#ifndef numtools_hh
#define numtools_hh

#include <valarray>

#include "fmins.hh"

namespace {
  std::valarray<double> gaussian_pdf(const std::valarray<double> &x, double mu, double sigma )
  {
    double x_length = x.max() - x.min();
    return x_length*exp(-(x*x-2*mu*x+mu*mu)/(2*sigma*sigma))/(sigma*sqrt(2*M_PI)*(x.size()-1));
  }

  std::valarray<double> linspace(double xmin, double xmax, size_t N )
  {
    std::valarray<double> x(N);
    for(size_t n=0; n<N; n++)
      x[n] = xmin + ((xmax - xmin) * n) / (N-1);
    return x;
  }
}

#endif /* numtools_hh */
