#ifndef params_hh
#define params_hh

#include <utility>
#include "conversion.hh"
#include "constants.hh"
#include "pair.hh"

namespace Params {

  /*
  struct Beam { ///< Beam parameters
    double sigmaz; ///< Bunch length [m]
    double espread; ///< Energy spread [GeV]
    double N_particles; ///< Nb of particles per bunch
    size_t N_bunches; ///< Nb of bunches per train
    double train_length; ///< train length [s]
    struct { ///< Initial beam emittances
      double x;
      double y;
    } emittance; 
    double get_peak_beam_current() const { return N_bunches * N_particles * ECHARGE / train_length; } ///< A
    double get_bunch_separation() const { return C_LIGHT * train_length / N_bunches; } ///< m
    double get_train_length() const { return train_length; } ///< s
  };
  */

  struct Cell {
    double frequency; ///< frequency [GHz]
    double ph;        ///< phase advance per cell ; 2pi/3
    double a;         ///< iris radius [m]   ; 0.35 < a/l < 0.70
    double g;         ///< gap length [m]    ; 0.55 < g/l < 0.90
    double l;         ///< structure period [m]
    Cell(double f = 11.994, double a_over_lambda = 0.15, double _ph = 2*M_PI/3 ) : frequency(f), ph(_ph)
    {
      double lambda = freq2lambda(frequency);
      a = a_over_lambda * lambda;
      l = ph / (2*M_PI) * lambda;
      g = l * 0.80;
    }
    double get_a_over_lambda() const { return a / freq2lambda(frequency); ; }
    void   set_a_over_lambda(double a_over_lambda ) { a = a_over_lambda * freq2lambda(frequency); ; }
    double W_long(double s /* m */ ) const ///< Longitudinal short-range wake. Return value is in V/pC/m 
    {
      double s0 = 0.41 * pow(a,1.8) * pow(g,1.6) / pow(l,2.4); // m
      return Z0 * C_LIGHT / M_PI / (a*a) * exp(-sqrt(s/s0)) * 1e-12;
    }
    double W_transv(double s /* m */  ) const ///< Transverse short-range wake. Return value is in V/pC/m^2
    {
      double s0 = 0.169 * pow(a,1.79) * pow(g,0.38) / pow(l,1.17); // m
      double sqrt_s_s0 = sqrt(s/s0);
      return 4 * Z0 * s0 * C_LIGHT / M_PI / (a*a*a*a) * (1 - (1 + sqrt_s_s0) * exp(-sqrt_s_s0)) * 1e-12;
    }
  };

  class AcceleratingStructure { ///< Accelerating structures parameters
    size_t Ncells; ///< Number of cells
    Cell cell; ///< Cell
  public:
    AcceleratingStructure() : Ncells(26), cell(11.994, 0.15, 2.*M_PI/3.) {}
    AcceleratingStructure(const Cell &c, size_t _Ncells ) : Ncells(_Ncells), cell(c) {}
    double max_gradient; ///< Gradient [GV/m]
    double get_frequency() const { return cell.frequency; }
    double get_length() const { return Ncells * cell.l; }
    void   set_length(double _l ) { Ncells = size_t(floor(_l / cell.l)); }
    double get_number_of_cells() const { return Ncells; }
    void   set_number_of_cells(size_t n ) { Ncells = n; }
    const Cell &get_cell() const { return cell; }
    void        set_cell(Cell c ) { cell = c; }
  };

  extern AcceleratingStructure Default_AcceleratingStructure;
}

#endif /* params_hh */
