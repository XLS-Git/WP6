#ifndef drift_hh
#define drift_hh

#include <utility>
#include <limits>
#include <cmath>

#include "element.hh"

class Drift : public Element {
protected:
  double length;
public:
  Drift(double l = 0.0, double momentum = -1.0 ) : Element(momentum), length(l) {}
  virtual ~Drift() {}
  virtual Drift *clone() { return new Drift(*this); }
  virtual void   set_length(double l ) { length = l; }
  virtual double get_length() const { return length; }
  virtual StaticMatrix<6,6> get_transfer_matrix(double mass = EMASS, double momentum = -1.0 ) const
  {
    if (momentum==-1.0) momentum = momentum0;
    double r56 = length*mass*mass/momentum/momentum; // equivalent to length/beta/beta/gamma/gamma
    return StaticMatrix<6,6>(1.0, length, 0.0, 0.0, 0.0, 0.0,
			     0.0, 1.0, 0.0, 0.0, 0.0, 0.0,
			     0.0, 0.0, 1.0, length, 0.0, 0.0,
			     0.0, 0.0, 0.0, 1.0, 0.0, 0.0,
			     0.0, 0.0, 0.0, 0.0, 1.0, r56,
			     0.0, 0.0, 0.0, 0.0, 0.0, 1.0);
  }
  virtual void track_z(Bunch1d &bunch )
  {
    double Etotal0 = sqrt(bunch.mass*bunch.mass + momentum0*momentum0);
    double beta0 = momentum0/Etotal0;
    if (beta0<0.999) {
      MatrixNd &data = bunch.data;
      for (size_t i=0; i<data.rows(); i++) {
	double momentum = data[i][1];
	double delta = (momentum - momentum0) / momentum0;
	double zp = 1+delta;
	// double zp = sqrt((1+delta)*(1+delta)-xp*xp-yp*yp); // 6d tracking
	data[i][0] += (1/beta0 - (1/beta0 + delta)/zp)*length;
      }
    }
  }
};

#endif /* drift_hh */
