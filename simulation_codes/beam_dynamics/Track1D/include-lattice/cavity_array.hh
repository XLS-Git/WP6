#ifndef cavity_array_hh
#define cavity_array_hh

#include "cavity.hh"

class CavityArray : public Element {
public:
  const Params::AcceleratingStructure params;
private:
  double voltage; ///< GV
  double phase; ///< rad
  bool do_wakefields;
  size_t size;
public:
  CavityArray(const Params::AcceleratingStructure &p = Params::Default_AcceleratingStructure, double momentum = -1.0 ) : Element(momentum), params(p), voltage(p.max_gradient * p.get_length()), phase(0.0), do_wakefields(true), size(0) {}
  virtual Element *clone() { return new CavityArray(*this); } 
  const Params::AcceleratingStructure &get_params() const { return params; }
  double get_energy_gain() const { return cos(phase)*voltage; }
  double get_frequency() const { return params.get_frequency(); }
  double get_voltage() const { return voltage; }
  void   set_voltage(double v ) { voltage = v; }
  double get_average_gradient() const { return voltage / get_length(); }
  double get_length() const { return get_size() * params.get_length(); }
  size_t get_size() const { return size == 0 ? size_t(ceil(voltage / params.max_gradient / params.get_length())) : size; }
  void resize(size_t n ) { size = n; }
  double get_phase() const { return phase; }
  double get_phased() const { return phase / M_PI * 180.0; }
  void   set_phase(double p ) { phase = p; }
  void   set_phased(double p ) { phase = p * M_PI / 180.0; }
  void   set_average_gradient(double G ) { voltage = std::min(G, params.max_gradient) * get_length(); }
  void   enable_wakefields() { do_wakefields = true; }
  void   disable_wakefields() { do_wakefields = false; }
  StaticMatrix<6,6> get_transfer_matrix(double mass = EMASS, double momentum = -1.0 ) const
  {
    if (fabs(get_energy_gain())<std::numeric_limits<double>::epsilon()) {
      return Drift(get_length(), momentum0).get_transfer_matrix(mass, momentum);
    }
    double _momentum = momentum == -1.0 ? momentum0 : momentum;
    StaticMatrix<6,6> R = Identity<6,6>();
    Cavity cavity(params);
    cavity.set_gradient(get_average_gradient());
    cavity.set_phase(phase);
    double _momentum0 = momentum0;
    for (size_t i=0; i<get_size(); i++) {
      cavity.set_reference_momentum(_momentum0);
      R = cavity.get_transfer_matrix(mass, _momentum) * R;
      double energy_gain = cavity.get_energy_gain();
      _momentum += energy_gain;
      _momentum0 += energy_gain;
    }
    return R;
  }
  void track_z(Bunch1d &bunch )
  {
    Cavity cavity(params);
    cavity.set_gradient(get_average_gradient());
    cavity.set_phase(phase);
    if (do_wakefields)
      cavity.enable_wakefields();
    else
      cavity.disable_wakefields();
    double _momentum0 = momentum0;
    for (size_t i=0; i<get_size(); i++) {
      cavity.set_reference_momentum(_momentum0);
      cavity.track_z(bunch);
      _momentum0 += cavity.get_energy_gain();
    }
  }
};

#endif /* cavity_array_hh */
