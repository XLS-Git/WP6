#ifndef lattice_hh
#define lattice_hh

#include <vector>

#include "element.hh"

class Lattice {
  std::vector<Element*> elements;
  bool check_reference_momentum(bool verbose = false ) const ///< true: everything is ok ; false: need to set_reference_momentum()
  {
    for (auto &element_ptr : elements) {
      if (element_ptr->get_reference_momentum() == -1.0) {
	if (verbose)
	  std::cerr << "error: first you need to run set_reference_momentum()\n";
	return false;
      }
    }
    return true;
  }
public:
  Lattice() {}
  Lattice(const Lattice &lattice ) {
    for (auto &element_ptr : lattice.elements) {
      elements.push_back(element_ptr->clone());
    }
  }
  ~Lattice() { for (auto &element_ptr : elements) delete element_ptr; }
  Lattice &operator=(const Lattice &lattice )
  {
    for (auto &element_ptr : elements) delete element_ptr;
    elements.clear();
    for (auto &element_ptr : lattice.elements) {
      elements.push_back(element_ptr->clone());
    }
    return *this;
  }
  template <class T> std::vector<T*> get() // picks all elements of type T (Qadrupole, Cavity )
  {
    std::vector<T*> ret;
    for (auto &element_ptr : elements)
      if (T *ptr = dynamic_cast<T*>(element_ptr))
	ret.push_back(ptr);
    return ret;
  }
  size_t size() const { return elements.size(); }
  void append(Element *element_ptr ) { elements.push_back(element_ptr->clone()); }
  void set_reference_momentum(double momentum0 = 0.0 )
  {
    for (auto &element_ptr : elements) {
      element_ptr->set_reference_momentum(momentum0);
      momentum0 += element_ptr->get_energy_gain();
    }
  }
  double get_length() const
  {
    double length = 0.0;
    for (auto &element_ptr : elements) {
      length += element_ptr->get_length();
    }
    return length;
  }
  double get_energy_gain() const {
    if (!check_reference_momentum(true)) return -1.0;
    double energy_gain = 0.0;
    for (auto &element_ptr : elements) {
      energy_gain += element_ptr->get_energy_gain();
    }
    return energy_gain;
  }
  StaticMatrix<6,6> get_transfer_matrix(double mass = EMASS, double momentum0 = 0.0 ) const
  {
    StaticMatrix<6,6> R = Identity<6,6>();
    if (!check_reference_momentum(true))
      return R;
    for (auto &element_ptr : elements) {
      R = element_ptr->get_transfer_matrix(mass, momentum0) * R;
      momentum0 += element_ptr->get_energy_gain();
    }
    return R;
  }
  Bunch1d track_z(Bunch1d bunch ) const
  {
    for (auto &element_ptr : elements) {
      element_ptr->track_z(bunch);
    }
    return bunch;
  }
  Element *operator[] (size_t i ) { return elements[i]; }
  const Element *operator[] (size_t i ) const { return elements[i]; }
  /*
  Pair<StaticVector<3>> transport_twiss(Pair<StaticVector<3>> t, std::ostream &stream ) const
  {
    double s = 0.0;
    stream << s << t.x << t.y << std::endl;
    for (auto &element_ptr : *this) {
      auto T = element_ptr->get_twiss_transfer_matrix();
      t.x = T.x * t.x;
      t.y = T.y * t.y;
      s += element_ptr->get_length();
      stream << s << t.x << t.y << std::endl;
    }
    return t;
  }
  Pair<StaticVector<3>> transport_twiss(Pair<StaticVector<3>> t, int N=-1 ) const
  {
    int n=1;
    for (auto &element_ptr : *this) {
      auto T = element_ptr->get_twiss_transfer_matrix();
      t.x = T.x * t.x;
      t.y = T.y * t.y;
      if (N!=-1 && n>=N)
	break;
      n++;
    }
    return t;
  }
  StaticMatrix<6,6> get_transfer_matrix(int N=-1) const
  {
    int n=1;
    StaticMatrix<6,6> m = Identity<6,6>();
    for (auto &element_ptr : *this) {
      const StaticMatrix<6,6> &M = element_ptr->get_transfer_matrix();
      m = M * m;
      if (N!=-1 && n>=N)
	break;
      n++;
    }
    return m;
  }
  Pair<double> get_beta_integral(Pair<StaticVector<3>> t ) const
  {
    Pair<double> result;
    for (auto &element_ptr : *this) {
      auto T = element_ptr->get_twiss_transfer_matrix();
      Pair<double> beta;
      beta.x = t.x[0] * 0.5;
      beta.y = t.y[0] * 0.5;
      t.x = T.x * t.x;
      t.y = T.y * t.y;
      beta.x += t.x[0] * 0.5;
      beta.y += t.y[0] * 0.5;
      double f = element_ptr->get_length() / element_ptr->get_reference_momentum();
      result.x += beta.x * f;
      result.y += beta.y * f;
    }
    return result;
  }
  */
};

#endif /* lattice_hh */
