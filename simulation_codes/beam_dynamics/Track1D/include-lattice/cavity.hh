#ifndef cavity_hh
#define cavity_hh

#include <valarray>
#include <utility>

#include "element.hh"
#include "drift.hh"
#include "params.hh"

class Cavity : public Element {
public:
  const Params::AcceleratingStructure params;
private:
  double gradient; // GV/m
  double phase; // rad
  bool do_wakefields;
  std::pair<std::valarray<double>, std::valarray<double> > wake_z_convolve(const Bunch1d &bunch, size_t Nslices ) const; 
public:
  Cavity(const Params::AcceleratingStructure &p = Params::Default_AcceleratingStructure, double momentum = -1.0 ) : Element(momentum), params(p), gradient(p.max_gradient), phase(0.0), do_wakefields(true) {}
  virtual Element *clone() { return new Cavity(*this); } 
  const Params::AcceleratingStructure &get_params() const { return params; }
  double get_energy_gain() const { return cos(phase)*gradient*get_length(); }
  double get_frequency() const { return params.get_frequency(); }
  double get_gradient() const { return gradient; }
  void   set_gradient(double g ) { gradient = std::min(g, params.max_gradient); }
  double get_length() const { return params.get_length(); }
  double get_phase() const { return phase; }
  double get_phased() const { return phase / M_PI * 180.0; }
  void   set_phase(double p ) { phase = p; }
  void   set_phased(double p ) { phase = p * M_PI / 180.0; }
  void   enable_wakefields() { do_wakefields = true; }
  void   disable_wakefields() { do_wakefields = false; }
  StaticMatrix<6,6> get_transfer_matrix(double mass = EMASS, double momentum = -1.0 ) const
  {
    double energy_gain = get_energy_gain();
    if (fabs(energy_gain)<std::numeric_limits<double>::epsilon()) {
      return Drift(get_length(), momentum0).get_transfer_matrix(mass, momentum);
    }
    double _momentum =  momentum == -1.0 ? momentum0 : momentum;
    double delta = energy_gain / _momentum;
    double sqrt1p_delta = sqrt(1.0 + delta);
    double length = get_length();
    StaticMatrix<6,6> A = Identity<6,6>();
    A[0][0] = A[2][2] = sqrt1p_delta;
    A[0][1] = A[2][3] = length * log1p(delta) * sqrt1p_delta / delta;
    A[1][1] = A[3][3] = 1.0 / sqrt1p_delta;
    StaticMatrix<6,6> E1 = Identity<6,6>();
    E1[1][0] = E1[3][2] = -0.5 * delta / length;
    StaticMatrix<6,6> E2 = Identity<6,6>();
    E2[1][0] = E2[3][2] = +0.5 * delta / length / (1.0 + delta);
    return E2 * A * E1;
  }
  void track_z(Bunch1d &bunch );

};

#endif /* cavity_hh */
