#ifndef quadrupole_hh
#define quadrupole_hh

#include "element.hh"

class Quadrupole : public Element {
  double length;   ///< quad length [GeV/m]
  double strength; ///< quad integrated strength [GeV/m]
public:
  Quadrupole(double l = 0.0, double s = 0.0, double momentum = -1.0 ) : Element(momentum), length(l), strength(s) {}
  virtual Element *clone() { return new Quadrupole(*this); } 
  void   set_K1(double k1 ) { strength = k1 * momentum0 * length; }
  double get_K1() const { return strength / momentum0 / length; }
  void   set_K1L(double K1L ) { strength = K1L * momentum0; }
  double get_K1L() const { return strength / momentum0; }
  double get_length() const { return length; }
  void   set_length(double l ) { length = l; }
  void   set_strength(double s ) { strength = s; }
  double get_strength() const { return strength; }
  StaticMatrix<6,6> get_transfer_matrix(double mass, double momentum = -1.0 ) const
  {
    if (fabs(strength)<std::numeric_limits<double>::epsilon())
      return Element::get_transfer_matrix(mass, momentum); 
    StaticMatrix<6,6> R(0.0);
    R[4][4]=1.0;
    R[5][5]=1.0;
    if (momentum==-1.0) momentum = momentum0;
    double K1L = strength / momentum;
    if (fabs(length)<std::numeric_limits<double>::epsilon()) {
      R[0][0]=1.0;
      R[1][0]=+K1L;
      R[1][1]=1.0;
      R[2][2]=1.0;
      R[3][2]=-K1L;
      R[3][3]=1.0;
    } else {
      double K1 = K1L / length;
      if (K1>0.0) {
	double ksqrt=sqrt(K1);
	double sh=sinh(ksqrt*length);
	double ch=cosh(ksqrt*length);
	R[0][0]=ch;
	R[0][1]=sh/ksqrt;
	R[1][0]=sh*ksqrt;
	R[1][1]=ch;
	double s=sin(ksqrt*length);
	double c=cos(ksqrt*length);
	R[2][2]=c;
	R[2][3]=s/ksqrt;
	R[3][2]=-s*ksqrt;
	R[3][3]=c;
      } else {
	double ksqrt=sqrt(-K1);
	double s=sin(ksqrt*length);
	double c=cos(ksqrt*length);
	R[0][0]=c;
	R[0][1]=s/ksqrt;
	R[1][0]=-s*ksqrt;
	R[1][1]=c;
	double sh=sinh(ksqrt*length);
	double ch=cosh(ksqrt*length);
	R[2][2]=ch;
	R[2][3]=sh/ksqrt;
	R[3][2]=sh*ksqrt;
	R[3][3]=ch;
      }
    }
    return R;
  }
  void track_z(Bunch1d &bunch )
  {
    Drift(length, momentum0).track_z(bunch);
  }
 
};

#endif /* quadrupole_hh */
