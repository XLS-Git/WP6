#ifndef chicane_hh
#define chicane_hh

#include <utility>
#include <limits>
#include <cmath>

#include "element.hh"

class Chicane : public Element {
protected:
  double r56;
  double t566;
  double u5666;
public:
  Chicane(double _r56, double _t566 = 0.0, double _u5666 = 0.0, double momentum = -1.0 ) : Element(momentum), r56(_r56), t566(_t566),
  u5666(_u5666) {}
  virtual ~Chicane() {}
  virtual Chicane *clone() { return new Chicane(*this); }
  virtual void   set_r56(double _r56 ) { r56 = _r56; }
  virtual void   set_t566(double _t566 ) { t566 = _t566; }
  virtual void   set_u5666(double _u5666 ) { u5666 = _u5666; }
  virtual double get_r56() const { return r56; }
  virtual double get_t566() const { return t566; }
  virtual double get_u5666() const { return u5666; }
  virtual StaticMatrix<6,6> get_transfer_matrix(double mass = EMASS, double momentum = -1.0 ) const
  {
    return StaticMatrix<6,6>(1.0, 0.0, 0.0, 0.0, 0.0, 0.0,
			     0.0, 1.0, 0.0, 0.0, 0.0, 0.0,
			     0.0, 0.0, 1.0, 0.0, 0.0, 0.0,
			     0.0, 0.0, 0.0, 1.0, 0.0, 0.0,
			     0.0, 0.0, 0.0, 0.0, 1.0, r56,
			     0.0, 0.0, 0.0, 0.0, 0.0, 1.0);
  }
  virtual void track_z(Bunch1d &bunch )
  {
    double ref_momentum = momentum0 == -1 ? bunch.get_mean_2() : momentum0;
    MatrixNd &data = bunch.data;
    for (size_t i=0; i<data.rows(); i++) {
      double momentum = data[i][1];
      double delta = (momentum - ref_momentum) / ref_momentum;
      data[i][0] += (r56 + (t566 + u5666 * delta) * delta) * delta * 1e6;
    }
  }
};

#endif /* chicane_hh */
