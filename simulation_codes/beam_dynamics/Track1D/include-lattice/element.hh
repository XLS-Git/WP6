#ifndef element_hh
#define element_hh

#include <utility>
#include <limits>
#include <cmath>

#include "static_matrix.hh"
#include "matrixnd.hh"
#include "bunch1d.hh"

class Element {
protected:
  double momentum0; // this is the reference momentm in GeV = P*c
public:
  Element(double momentum = -1.0 ) : momentum0(momentum) {}
  virtual ~Element() {}
  virtual Element *clone() = 0;
  virtual double get_length() const { return 0.0; }
  virtual void   set_reference_momentum(double momentum ) { momentum0 = momentum; }
  virtual double get_reference_momentum() const { return momentum0; }
  virtual double get_energy_gain() const { return 0.0; }
  virtual StaticMatrix<6,6> get_transfer_matrix(double mass = EMASS, double momentum = -1.0 ) const { return StaticMatrix<6,6>(0.0); } 
  virtual void track_z(Bunch1d &bunch ) = 0;

};

#endif /* element_hh */
