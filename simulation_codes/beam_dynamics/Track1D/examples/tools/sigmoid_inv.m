function x=sigmoid_inv(u, a, b)
  x=log((u-a)./(b-u));
end
