function X = relative_difference(X1, X0)
    X = abs(X1 - X0);
    if (min(abs(X0), abs(X1))) > 0.0
        X = abs(X1 - X0) / X0;
    end
end

