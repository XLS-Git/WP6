addpath('tools')
addpath('..')

  Beam.sigmaZ = 731;% um
  Beam.espread = 0.2; % percent
  Beam.energy = 0.116; % GeV

  N = 10000;
  Z = Beam.sigmaZ * randn(N,1);
  E = Beam.energy * (1 + Beam.espread * randn(N,1) / 100);

  Beam.mass = 0.00051099893;                         % [GeV/c/c]
  Beam.charge = 1.5603773e+09;                       % # of electrons 
  Beam.data = [ Z E ];
  DataIn=[ Z*1e-6  E]';
%*****accel up to  0.20038 GeV
    E0      =  0.20038;%  GeV
    f_S     = 3e+9; % Hz
    lamda_S = (3e+8)/f_S;                              % m
    kRF_S   = 2*pi/lamda_S;                              % 1/m              
    eV_S    = 0.207 ;  % [ 0.207  0.33 0.452]                  % GeV
    phi_S   = 20;  % 10:1:80 ;                                 
    R65_S   = (eV_S*kRF_S*sin(phi_S))/E0;        % 1/m      
    c2       = (R65_S.^2.*Beam.sigmaZ^2+Beam.espread^2);
    c1       = 2*R65_S.*Beam.sigmaZ^2;
    c0       =  Beam.sigmaZ^2-55^2;                
    R56     = roots([c2 c1 c0]);  %  -0.0105043,  -0.0090343
    R56     = mean(R56);
    T566= -1.5*R56;
    E_G=eV_S*cos(phi_S-kRF_S*Z*1e-6);
    E_S= E+E_G;
    DataOut_S=[ Z*1e-6 E_S]'; 
    d= (E_S-E0)/E0;
    DataOut_CH= [Z*1e-6+R56.*d+T566.*d.^2   E_S]'; % 
    plot(DataIn(1,:),DataIn(2,:), '*');
    hold on
    plot(DataOut_S(1,:),DataOut_S(2,:), 'r');
    hold on
    plot(DataOut_CH(1,:), DataOut_CH(2,:), 'c*');
   
     a = Beam.energy/E0;
    C=Beam.sigmaZ/55;
    h1= (1/C-1)/R56; 
    ss=a^2*R56^2*Beam.espread^2+(1+h1*R56)^2*Beam.sigmaZ^2
    sigmaZ_f=sqrt(ss)









