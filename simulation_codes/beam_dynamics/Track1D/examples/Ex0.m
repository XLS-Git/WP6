addpath('tools')
addpath('..')

%% setup the beam

Beam.sigmaZ = 700; % um
Beam.espread = 0.2; % percent
Beam.energy = 0.2; % GeV

N = 10000;
Z = Beam.sigmaZ * randn(N,1);
E = Beam.energy * (1 + Beam.espread * randn(N,1) / 100);

Beam.mass = 0.00051099893; % [GeV/c/c]
Beam.charge = 1.5603773e+09; % # of electrons 
Beam.data = [ Z E ];

% lattice
R56 = -0.106; % m
R65 = 9.4; % 1/m

TRF = [ 1 0 ; R65 0 ];
TCH = [ 1 R56 ; 0 1 ];

X0 = [ Z*1e-6 (E-Beam.energy) / Beam.energy ]';
X1 = TRF * X0;
X2 = TCH * X1;

% plot
clf
plot(X0(1,:), X0(2,:), '*');
hold on
plot(X1(1,:), X1(2,:), 'r*');
plot(X2(1,:), X2(2,:), 'g*');
