#! /opt/local/bin/octave -q --persist

addpath('tools')
addpath('..')

Track1D;

%% setup the beam

Beam.sigmaZ = 700; % um
Beam.espread = 0.2; % percent
Beam.energy = 0.2; % GeV

N = 10000;
Z = Beam.sigmaZ * randn(N,1);
E = Beam.energy * (1 + Beam.espread * randn(N,1) / 100);

Beam.mass = 0.00051099893; % [GeV/c/c]
Beam.charge = 1.5603773e+09; % # of electrons 
Beam.data = [ Z E ];
 
%% describe the RF structure
 
RF.Scell = Cell();
RF.Scell.frequency = 3.0; %2.8176;
RF.Scell.a = 0.0115;
RF.Scell.g = 0.0299;
RF.Scell.l = 0.0333;

ncells = 120;

RF.structure = AcceleratingStructure(RF.Scell, ncells);
RF.structure.max_gradient = 0.030; % GV/m

%% create the lattice

% setup RF
CAV = Cavity(RF.structure);
CAV.set_phase(pi/2);
CAV.set_gradient(0.030);

L1 = Lattice();
L1.append(CAV);
L1.set_reference_momentum(Beam.energy);

% setup chicane
R56 = -0.106;
T566 = -1.5 * R56;
BC = Chicane(R56, T566);
L2 = Lattice();
L2.append(BC);
L2.set_reference_momentum(Beam.energy);

% Tracking

B0 = Bunch1d(Beam.mass, Beam.charge, Beam.data);
B1 = L1.track_z(B0);
B2 = L2.track_z(B1);

clf
plot(B0.data(:,1), B0.data(:,2), '*');
hold on
plot(B1.data(:,1), B1.data(:,2), 'r*');
plot(B2.data(:,1), B2.data(:,2), 'g*');
