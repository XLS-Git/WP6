addpath('tools')
addpath('..')

%% setup the beam

 Beam.sigmaZ = 1000;% um
 Beam.espread = 0.2; % percent
 Beam.energy = 0.2; % GeV

  N = 10000;
  Z = Beam.sigmaZ * randn(N,1);
  E = Beam.energy * (1 + Beam.espread * randn(N,1) / 100);

  Beam.mass = 0.00051099893; % [GeV/c/c]
  Beam.charge = 1.5603773e+09; % # of electrons 
  Beam.data = [ Z E ];

% ************* lattice *************

%RF_S structure/ Phi & GeV configuration to accel up to 0.307 GeV --> [20 0.5]
%we cannot take the linear approximation as eV_S/Beam.energy = 2.5 > 1
  f_S = 3e+9; % Hz
  lamda_S = (3e+8)/f_S;  % m
  kRF_S = 2*pi/lamda_S % 1/m kRF_S =  62.832 
  eV_S = 0.5 ; % GeV
  phi_S = 20 ;   % deg
  R65_S = (eV_S*kRF_S*sin(phi_S))/Beam.energy % 1/m ??? R65_S = -1.8643
  R66_S = 1 - ((eV_S*cos(phi_S))/Beam.energy); % R66_S =  0.98674

%RF_X structure/ Phi & GeV conf to accelerate up to 2GeV       --> [122.1 0.36] 
  f_X= 12e+9; % Hz
  lamda_X =(3e+8)/f_X;  % m
  kRF_X = 2*pi/lamda_X % 1/m kRF_X =  251.33
  eV_X = 0.36;    % GeV
  phi_X = 122.1;  % deg
  R65_X = (eV_X*kRF_X*sin(phi_X))/Beam.energy % 1/m ???? R65_X =  185.34
  R66_X = 1 - ((eV_X*cos(phi_X))/Beam.energy) % R66_X =  2.6420

%Chicane represented by just a number

  R56= -0.0018;% m
  % R56= -0.0030419R56

  T566= (-1.5)*R56 % m T566 =  0.0027000

  %******test******

   % a = (-eV_S*kRF_S*sin(phi_S)- eV_X*kRF_X*sin(phi_X))/Beam.energy;
   % b = (-eV_S*(kRF_S^2)*cos(phi_S)- eV_X*(kRF_X^2)*cos(phi_X))/2*Beam.energy;
   % must_be_zero = b*R56 +(a^2)*T566    % must_be_zero =  288.21

    % is not : To 1st term of  must_be_zero: ans = -3.5889 -> 
   %            To 2nd : 
       % Better explained: delta =  = (a)*(z0-0) + (b)*(z0-0)^2  -->  z =(1+a*R56)*z0 + [b*R56+(a^2)*T566]*z0^2
		
    %		   value= 1+a*R56


 TRF_S = [ 1 0 ; R65_S R66_S ];
 TRF_X = [ 1 0 ; R65_X R66_X ];
  TCH = [ 1 R56 ; 0 1 ];

 X0 = [ Z*1e-6 (E-Beam.energy) / Beam.energy ]';
 X1 = TRF_S * X0;
 X2 = TRF_X * X1;
 X3 = TCH * X2;

 % plot
 clf
 plot(X0(1,:), X0(2,:), '*');
 hold on
 plot(X1(1,:), X1(2,:), 'r*');
 plot(X2(1,:), X2(2,:), 'g*');
 plot(X3(1,:), X3(2,:), 'y*');


