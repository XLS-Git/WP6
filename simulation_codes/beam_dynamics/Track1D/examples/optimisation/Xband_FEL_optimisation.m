#! /opt/local/bin/octave -q --persist

addpath('tools')
addpath('..')

Track1D;

%% Inizialization

global Beam RF Target

Beam   = setup_beam();
RF     = setup_RF();
Target = setup_target();

%% Define each element in the lattice

function State = X2State(X)
  global Target Beam
  X /= 2;
  State.BC1.R56      = sigmoid(X(1), -0.02, 0.02);
  State.BC2.R56      = sigmoid(X(2), -0.02, 0.02);
  State.S0.voltage   = sigmoid(X(3),  0.010, 0.500);
  State.X0.voltage   = sigmoid(X(4),      0, 0.500);
  State.X1.voltage   = sigmoid(X(5),      1, 4.5);
  State.S0.phase_deg = sigmoid(X(6),      0, 180);
  State.X0.phase_deg = sigmoid(X(7),      0, 180);
  State.X1.phase_deg = sigmoid(X(8),      0, 120);
  State.X2.phase_deg = sigmoid(X(9),      0, 40);
  State.X2.voltage   = (Target.energy - Beam.pc0 - ...
                        State.S0.voltage * cosd(State.S0.phase_deg) - ...
                        State.X0.voltage * cosd(State.X0.phase_deg) - ...
                        State.X1.voltage * cosd(State.X1.phase_deg)) / ...
		       cosd(State.X2.phase_deg);
  
endfunction

function [B0,B1,B2,B3] = track(State)
  global Beam RF Target
  Track1D;
  
  S0 = CavityArray(RF.Sstructure);
  S0.set_voltage(State.S0.voltage);
  S0.set_phase(deg2rad(State.S0.phase_deg));

  X0 = CavityArray(RF.Xstructure);
  X0.set_voltage(State.X0.voltage);
  X0.set_phase(deg2rad(State.X0.phase_deg));

  X1 = CavityArray(RF.Xstructure);
  X1.set_voltage(State.X1.voltage);
  X1.set_phase(deg2rad(State.X1.phase_deg));

  X2 = CavityArray(RF.Xstructure);
  X2.set_voltage(State.X2.voltage);
  X2.set_phase(deg2rad(State.X2.phase_deg));

  BC1 = Chicane(State.BC1.R56);
  BC2 = Chicane(State.BC2.R56);

  %% Defines the lattice

  L0 = Lattice();
  L0.append(S0); 
  L0.append(X0);
  L0.append(BC1);
  
  L1 = Lattice();
  L1.append(X1); 
  L1.append(BC2);
  
  L2 = Lattice();
  L2.append(X2);

  L0.set_reference_momentum(Beam.pc0);
  L1.set_reference_momentum(Beam.pc0 + L0.get_energy_gain());
  L2.set_reference_momentum(Beam.pc0 + L0.get_energy_gain() + L1.get_energy_gain());
  
  %% Tracking
  
  B0 = Bunch1d(Beam.mass0, Beam.charge, Beam.data);
  B1 = L0.track_z(B0);
  B2 = L1.track_z(B1);
  B3 = L2.track_z(B2);
end

function [M,B1,B2,B3,B4] = merit_function(X)
  global Target Beam
  Track1D;

  State = X2State(X);
  [B0,B1,B2,B3] = track(State);

  wgt_sigmaz    = 1e6;
  wgt_espread   = 1e2;
  wgt_energy    = 1e4;
  wgt_R56       = 0;

  intermediate_sigmaz = B1.get_std_1();
  intermediate_energy = B1.get_mean_2();
  
  disp("");
  final_sigmaz_um = B3.get_std_1()
  final_energy_GeV = B3.get_mean_2()
  final_espread_percent = B3.get_std_2() / final_energy_GeV * 100;
  final_slice_espread_percent = Slice_Espread(B3.data)
  final_longitudinal_emittance = final_sigmaz_um * B3.get_std_2();
  
  M = wgt_sigmaz * ...
      relative_difference(final_sigmaz_um, Target.sigmaz) ^ 2;
  
  M = M + wgt_sigmaz * ...
          relative_difference(intermediate_sigmaz, Target.intermediate_sigmaz) ^ 2;
  
  M = M + wgt_R56 * ...
          relative_difference(State.BC1.R56, 0.0) ^ 2;

  M = M + wgt_R56 * ...
          relative_difference(State.BC2.R56, 0.0) ^ 2;
  
  M = M + wgt_espread * ...
          relative_difference(final_slice_espread_percent, Target.slice_espread) ^ 2;

  M = M + 1000 * wgt_espread * ...
          relative_difference(final_longitudinal_emittance, Beam.emittance) ^ 2;
  
  M = M + wgt_energy * ...
          relative_difference(final_energy_GeV, Target.energy) ^ 2;

  M = M + wgt_energy * ...
          relative_difference(intermediate_energy, Target.intermediate_energy) ^ 2;
  
endfunction

%% Optimization
F_min = Inf;
X_min = [];

for i=1:10
  if i == 1
    X = zeros(9,1);
  else
    X = randn(9,1);
  endif
  [X,F] = fminsearch (@merit_function, X, optimset('TolFun', 1e-8, 'TolX', 1e-5, 'MaxIter', 100000));
  if F<F_min
    X_min = X;
    F_min = F;
    save -text optimum.dat X_min F_min i
  endif
endfor

%% Track the solution
[M,B0,B1,B2,B3] = merit_function(X_min);

%% Plots
figure(1); scatter(B0.data(1:50:end,1), B0.data(1:50:end,2));
figure(2); scatter(B1.data(1:50:end,1), B1.data(1:50:end,2));
figure(3); scatter(B2.data(1:50:end,1), B2.data(1:50:end,2));
figure(4); scatter(B3.data(1:50:end,1), B3.data(1:50:end,2));
