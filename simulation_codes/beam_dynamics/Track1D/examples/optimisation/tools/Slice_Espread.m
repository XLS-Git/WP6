function [dEE_percent, MASK] = Slice_Espread(Beam)
    num_of_particles = size(Beam, 1);

    percent = 0.20;
    Imin = floor(num_of_particles * (0.5 - percent / 2));
    Imax = ceil (num_of_particles * (0.5 + percent / 2));
    II = Imin:Imax;

    dEE_percent = std(Beam(II,2)) / mean(Beam(II,2)) * 100;

end
