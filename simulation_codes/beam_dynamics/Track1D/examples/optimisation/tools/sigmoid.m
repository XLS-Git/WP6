function s=sigmoid(x, a, b)
  s=a+(b-a)./(1+exp(-x));
end
