function Target = setup_target()
    Target.sigmaz              = 7;     % micron
    Target.energy              = 6.0;   % GeV
    Target.intermediate_sigmaz = 60;    % micron						 
    Target.intermediate_energy = 0.300; % GeV
    Target.slice_espread       = 0.02;  % percent
endfunction
