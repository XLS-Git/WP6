function RF = setup_RF()
    Track1D
    
    RF.Scell = Cell();
    RF.Scell.frequency = 2.8176;
    RF.Scell.a = 0.0115;
    RF.Scell.g = 0.0299;
    RF.Scell.l = 0.0333;

    RF.Xcell = Cell();
    RF.Xcell.frequency = 11.994;
    RF.Xcell.a = 0.0035;
    RF.Xcell.g = 0.00854;
    RF.Xcell.l = 0.010417;

    RF.Sstructure = AcceleratingStructure(RF.Scell, 120);
    RF.Sstructure.max_gradient = 20e-3; % GV/m

    RF.Xstructure = AcceleratingStructure(RF.Xcell, 72);
    RF.Xstructure.max_gradient = 67.5e-3; % GV/m

endfunction