addpath('tools')
addpath('..')

%% setup the beam

 Beam.sigmaZ = 1000;% um
 Beam.espread = 0.2; % percent
 Beam.energy = 0.2; % GeV

  N = 10000;
  Z = Beam.sigmaZ * randn(N,1);
  E = Beam.energy * (1 + Beam.espread * randn(N,1) / 100);

  Beam.mass = 0.00051099893; % [GeV/c/c]
  Beam.charge = 1.5603773e+09; % # of electrons 
  Beam.data = [ Z E ];


% ************* lattice *************

%RF_S structure/ Phi & GeV configuration to accel up to 0.307 GeV --> [20 0.5]
%we cannot take the linear approximation as eV_S/Beam.energy = 2.5 > 1
  f_S = 3e+9; % Hz
  lamda_S = (3e+8)/f_S;  % m
  kRF_S = 2*pi/lamda_S % 1/m kRF_S =  62.832 
    eV_S = 0.5 ; % GeV
  phi_S = 20 ;   % deg 20
  R65_S = (eV_S*kRF_S*sin(phi_S))/Beam.energy % 1/m ??? R65_S = -1.8643
  R66_S = 1 - ((eV_S*cos(phi_S))/Beam.energy); % R66_S =  0.98674

%***************RF_X structure/ Phi & GeV conf to accelerate up to 2GeV       --> [122.1 0.36] 
  f_X= 12e+9; % Hz
  lamda_X =(3e+8)/f_X;  % m
  kRF_X = 2*pi/lamda_X % 1/m kRF_X =  251.33
  eV_X = 0.36 ;    % GeV
  phi_X = 122.1 ;  % deg 108
  R65_X = (eV_X*kRF_X*sin(phi_X))/Beam.energy; % 1/m - R65_X =  185.34
  R66_X = 1 - ((eV_X*cos(phi_X))/Beam.energy); %       R66_X =  2.6420

%****************Chicane represented by just a number
 %    C = 14.28; % compression factor, sigmaZfinal = 70um
 %    R56 = -(1-C)/((-eV_S*kRF_S*sin(phi_S)- eV_X*kRF_X*sin(phi_X))/Beam.energy); 
 %   R56 = -1/((-eV_S*kRF_S*sin(phi_S)- eV_X*kRF_X*sin(phi_X))/Beam.energy);   %-0.036419
     R56= -0.0018;
     T566= (-1.5)*R56; % m T566 =  0.0027000

 %************** plot E[GeV] vs Z[um] **************

    DataInput=[ Z*1e-6  E]';
    DataOutput_S=[ Z*1e-6  E+eV_S*cos(phi_S-kRF_S*Z*1e-6)]';  
    E_afterX= E+eV_S*cos(phi_S-kRF_S*Z*1e-6)+eV_X*cos(phi_X-kRF_X*Z*1e-6); 
    DataOutput_SX=[ Z*1e-6  E_afterX]';
   % TCH = [ 1 R56 ; 0 1 ];
    DataOutput_BC= [Z*1e-6+R56.*((E.-E_afterX)./E_afterX)  E_afterX]'; % +T566*((E-E_X)/E_X).^2 
    plot(DataInput(1,:),DataInput(2,:), '*');
    DataInput(1,:)("um");
    DataInput(1,:)("GeV");
    hold on
    plot(DataOutput_S(1,:),DataOutput_S(2,:), 'r');
    
    hold on
    plot(DataOutput_SX(1,:), DataOutput_SX(2,:), 'g*');
    hold on
    plot(DataOutput_BC(1,:), DataOutput_BC(2,:), 'y*');


%******* Invest *******
  % c=(-kRF_S^2)*0.5*cos(20)/0.72;
  % d= 2*2*a/((kRF_X^2))*2.8;& % cos^-1(0.1983)= 78.55834277 deg




%*******Notes*********
   % Beam.dataOutput=sparse(Beam.data);
   % for row = 1:N 
   %  Beam.dataOutput(row,1)= Beam.data(row,1);
   %   Beam.dataOutput(row,2)= Beam.data(row,2)+eV_S*cos(phi_S-kRF_S*Beam.data(row,1))+eV_X*cos(phi_X-kRF_X*Beam.data(row,1));
   % endfor
   % i=1;
   % while i<N   % length(Beam.data)
   % Beam.dataOutput[]=[Beam.data(i,1) Beam.data(i,2)+eV_S*cos(phi_S-kRF_S*Beam.data(i,1))+eV_X*cos(phi_X-kRF_X*Beam.data(i,1))
   % i=i+1   
   % endwhile
						   
						 
 


