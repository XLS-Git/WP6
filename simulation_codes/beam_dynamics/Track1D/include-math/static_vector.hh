#ifndef vector_hh
#define vector_hh

#include <iostream>
#include <cstddef>
#include <cstdarg>
#include <cmath>

#include "stream.hh"

template <size_t N> class StaticVector {
	
  double data[N];
  
public:
  
  explicit StaticVector(double arg ) { for (size_t i=0; i<N; i++) data[i] = arg; }
  explicit StaticVector(const double *v )  { for (size_t i=0; i<N; i++) data[i] = v[i]; }
  StaticVector() {}
  StaticVector(double v0, double v1, ... ) 
  {
    data[0] = v0;	
    if (N >= 2) {
      data[1] = v1;
      if (N >= 3) {
	va_list ap;
	va_start(ap, v1);
	for (size_t i = 2; i < N; i++)
	  data[i] = va_arg(ap, double);
	va_end(ap);
      }
    }
  }
  
  inline void zero() { for (size_t i=0; i<N; i++) data[i]=0; }
  
  inline const double &operator [] (size_t i )	const	{ return data[i]; }
  inline double &operator [] (size_t i )	       	{ return data[i]; }
  
  friend double abs(const StaticVector &v )
  {
    double x = v.data[0];
    double sum = x * x;
    for (size_t i = 1; i < N; i++) {
      x = v.data[i];
      sum += x * x;
    }
    return sqrt(sum);
  }
  
  const StaticVector &operator += (const StaticVector &v )				{ for (size_t i = 0; i < N; i++) data[i] += v.data[i]; return *this; } 
  const StaticVector &operator -= (const StaticVector &v )				{ for (size_t i = 0; i < N; i++) data[i] -= v.data[i]; return *this; } 
  
  friend StaticVector operator + (const StaticVector &a, const StaticVector &b )	{ StaticVector result; for (size_t i = 0; i < N; i++) result.data[i] = a.data[i] + b.data[i]; return result; }
  friend StaticVector operator - (const StaticVector &a, const StaticVector &b )	{ StaticVector result; for (size_t i = 0; i < N; i++) result.data[i] = a.data[i] - b.data[i]; return result; }
  friend StaticVector operator - (const StaticVector &v )				{ StaticVector result; for (size_t i = 0; i < N; i++) result.data[i] = -v.data[i]; return result; }
  
  friend double operator * (const StaticVector &a, const StaticVector &b )		{ double result = a.data[0] * b.data[0]; for (size_t i = 1; i < N; i++) result += a.data[i] * b.data[i]; return result; }
  friend StaticVector operator * (const double &a, const StaticVector &b )		{ StaticVector result; for (size_t i = 0; i < N; i++) result.data[i] = a * b.data[i]; return result; }
  friend StaticVector operator * (const StaticVector &a, const double &b )		{ StaticVector result; for (size_t i = 0; i < N; i++) result.data[i] = a.data[i] * b; return result; }
  
  friend StaticVector operator / (const StaticVector &a, const double &b )		{ StaticVector result; for (size_t i = 0; i < N; i++) result.data[i] = a.data[i] / b; return result; }
  
  friend std::ostream &operator << (std::ostream &stream, const StaticVector &v )	{ for (size_t i = 0; i < N; i++) stream << ' ' << v[i]; return stream; }
  
  friend OStream &operator << (OStream &stream, const StaticVector &v )		{ if (stream) stream.write(v.data, N); return stream; }
  friend IStream &operator >> (IStream &stream, StaticVector &v )			{ if (stream) stream.read(v.data, N); return stream; }

};

#endif /* static_vector_hh */
