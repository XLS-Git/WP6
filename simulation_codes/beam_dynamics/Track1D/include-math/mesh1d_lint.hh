/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2021 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef mesh1d_lint_hh
#define mesh1d_lint_hh

#include "mesh1d.hh"

template <class T, class Alloc = std::allocator<T>>
class TMesh1d_LINT : public TMesh1d<T,Alloc> {
protected:
  using TMesh1d<T,Alloc>::nx;
public:
  using TMesh1d<T,Alloc>::TMesh1d;
  using TMesh1d<T,Alloc>::elem;
  using TMesh1d<T,Alloc>::operator=;

  template <class Alloc1> TMesh1d_LINT(const TMesh1d<T,Alloc1> &m ) : TMesh1d<T,Alloc>(m) {}

  inline T operator()(double x ) const // linear interpolation
  {
    if (x<0.0) return T{0.0};
    if (x>double(nx)-1.0) return T{0.0};
    double iinteger, ifrac = modf(x, &iinteger);
    size_t i = size_t(iinteger);
    auto i0 = elem(i);
    if (i+1<nx) { // X OK
      auto i1 = elem(i+1);
      return LINT(i0, i1, ifrac);
    }
    return i0;
  }

  inline T deriv(double x ) const // first derivative
  {
    if (x<0.0) return T{0.0};
    if (x>double(nx)-1.0) return T{0.0};
    double iinteger; modf(x, &iinteger);
    size_t i = size_t(iinteger);
    auto i0 = elem(i);
    if (i+1<nx) { // X OK
      auto i1 = elem(i+1);
      return LINT_deriv(i0, i1);
    }
    return i0;
  }

  inline T deriv2(double x ) const // second derivative
  {
    return 0.0;
  }

  inline T deriv3(double x ) const // third derivative
  {
    return 0.0;
  }

};

#endif /* mesh1d_lint_hh */
