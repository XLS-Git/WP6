#ifndef static_matrix_hh
#define static_matrix_hh

#include "static_vector.hh"

template <size_t M, size_t N>
class StaticMatrix {

  double data[M*N];

public:
  
  explicit StaticMatrix(double arg ) { for (size_t i=0; i<M*N; i++) data[i] = arg; }
  explicit StaticMatrix(const double m[][N] )
  {
    for (size_t i=0; i<M; i++) {
      for (size_t j=0; j<N; j++) {
	data[N*i+j]=m[i][j];
      }
    }
  }
  StaticMatrix(double a11, double a12, ... ) 
  {
    data[0]=a11;
    data[1]=a12;
    va_list ap;
    va_start(ap, a12);
    for (size_t j=2; j<N; j++) {
      data[j]=va_arg(ap, double);
    }
    for (size_t i=1; i<M; i++) {
      for (size_t j=0; j<N; j++) {
	data[N*i+j]=va_arg(ap, double);
      }
    }
    va_end(ap);
  }
  template <size_t I, size_t J>
  explicit StaticMatrix(const StaticMatrix<I,J> &m )
  {
    for (size_t i=0;i<(I<=M?I:M);i++) {
      for (size_t j=0;j<(J<=N?J:N);j++) {
        data[N*i+j]=m[i][j];
      }
    }
  }
  StaticMatrix() {}
  
  double *c_ptr() { return data; }
  const double *c_ptr() const { return data; }

  inline void zero() { for (size_t i=0; i<M*N; i++) data[i]=0.0; }
  
  inline void identity()
  {
    for (size_t i=0;i<M*N;i++)  data[i]=0.0;
    for (size_t i=0;i<(N<=M?N:M);i++) data[(N+1)*i]=1.0;
  }
  
  StaticMatrix<M-1,N-1> get_submatrix(size_t i, size_t j ) const
  {
    StaticMatrix<M-1, N-1> result;
    size_t __i=0;
    for (size_t _i=0; _i<M; _i++) {
      if (_i!=i) {
	size_t __j=0;
	for (size_t _j=0; _j<N; _j++) {
	  if (_j!=j) {
	    result[__i][__j]=(*this)[_i][_j];
	    __j++;
	  }
	}
	__i++;
      }
    }
    return result;
  }
  
  inline const StaticMatrix &operator = (const double m[][N] )
  {
    for (size_t i=0; i<M; i++) {
      for (size_t j=0; j<N; j++) {
	data[N*i+j] = m[i][j];
      }
    }
    return *this;
  }
  inline const StaticMatrix &operator += (const StaticMatrix &m )			{ if (&m != this)	for (size_t i = 0; i < M*N; i++)	*data[i] += m.data[i]; return *this; }
  inline const StaticMatrix &operator -= (const StaticMatrix &m )			{ if (&m != this)	for (size_t i = 0; i < M*N; i++)	*data[i] -= m.data[i]; return *this; }
  inline const StaticMatrix &operator *= (const StaticMatrix &m )			{ return *this = *this * m; }
  
  inline const double *operator [] (size_t i ) const { return &data[N*i]; }
  inline double *operator [] (size_t i ) { return &data[N*i]; }
  
  friend StaticMatrix operator * (const StaticMatrix &a, double b )			{ StaticMatrix<M,N> result;	for (size_t i = 0; i < M*N; i++)	result.data[i] = a.data[i] * b; return result; }
  friend StaticMatrix operator * (double b, const StaticMatrix &a )			{ StaticMatrix<M,N> result;	for (size_t i = 0; i < M*N; i++)	result.data[i] = a.data[i] * b; return result; }
  friend StaticMatrix operator / (const StaticMatrix &a, double b )			{ StaticMatrix<M,N> result;	for (size_t i = 0; i < M*N; i++)	result.data[i] = a.data[i] / b; return result; }
  friend StaticMatrix operator + (const StaticMatrix &a, const StaticMatrix &b )		{ StaticMatrix<M,N> result;	for (size_t i = 0; i < M*N; i++)	result.data[i] = a.data[i] + b.data[i]; return result; }
  friend StaticMatrix operator - (const StaticMatrix &a, const StaticMatrix &b )		{ StaticMatrix<M,N> result;	for (size_t i = 0; i < M*N; i++)	result.data[i] = a.data[i] - b.data[i]; return result; }
  friend StaticMatrix operator - (StaticMatrix a )					{ for (size_t i = 0; i < M*N; i++)	a.data[i] = -a.data[i]; return a; }
  
  template <size_t _I, size_t _J, size_t _K> 	
  friend StaticMatrix<_I,_K> operator * (const StaticMatrix<_I,_J> &a, const StaticMatrix<_J,_K> &b );
  
  friend StaticVector<M> operator * (const StaticMatrix &a, const StaticVector<N> &b )
  {
    StaticVector<M> r;
    for (size_t i = 0; i < M; i++) {
      r[i] = a.data[N*i] * b[0];
      for (size_t j = 1; j < N; j++)
	r[i] += a.data[N*i+j] * b[j];
    }
    return r;
  }
  
};

template <size_t M, size_t N> static inline StaticMatrix<N, M> transpose(const StaticMatrix<M,N> &a )
{
  StaticMatrix<N, M> result;
  for (size_t i = 0; i < M; i++)
    for (size_t j = 0; j < N; j++)
      result.data[M*j+i] = a.data[N*i+j];
  return result;
}

template <size_t M, size_t N, size_t O> static inline StaticMatrix<M,O> operator * (const StaticMatrix<M,N> &a, const StaticMatrix<N,O> &b )
{ 
  StaticMatrix<M, O> result(0.0);
  for (size_t i = 0; i < M; i++)
    for (size_t j = 0; j < O; j++) {
      result.data[O*i+j] = a.data[i*N] * b.data[j];
      for (size_t k = 1; k < N; k++)
	result.data[O*i+j] += a.data[N*i+k] * b.data[O*k+j];
    }
  return result;
}

template <size_t M, size_t N> static inline std::ostream &operator << (std::ostream &stream, const StaticMatrix<M,N> &m )
{ 
  stream << M << 'x' << N << ":\n";
  for (size_t i = 0; i < M; i++)
    stream << StaticVector<N>(m[i]) << std::endl;	
  return stream; 
}

template <size_t M, size_t N> static inline StaticMatrix<M,N> Identity()
{
  StaticMatrix<M,N> m;
  m.identity();
  return m;
}

#endif /* static_matrix_hh */
