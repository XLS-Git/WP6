# Track1D
Track1D is a small longitudinal tracking code developed at CERN for the optimization of bunch compressors and Free electron lasers (FELs). It implements wakefield effects and allows to automatically adjust the machine layout to fit the required beam parameters. 

Track1D was written as a fast C++ library, loadable from Octave or Python. This allows one to perform complex optimizations targeting arbitrary performance goals.


# How to compile and install Track1D
In order to run Track1D some binary packages and libraries are necessary:
* GSL - GNU Scientific Library
* FFTW Library
* Octave (optional)
* Python (optional)
## Installing the prerequisites
### On macOS
To compile Track1D on macOS, one option is to use MacPorts in order to retrieve the necessary packages:
1. Install MacPorts, https://www.macports.org/install.php
2. If you have it already, be sure everything is up do date
```
$ sudo port selfupdate
$ sudo port upgrade outdated
```
3. Install the necessary packages
```
$ sudo port install fftw-3 gsl clang
```
If you intend to use Track1D within Octave, you should also install this package:
```
$ sudo port install octave
```
If you intend to use Track1D within Python, you should make sure Python is installed on yoursystem, together with its library NumPy:
```
$ sudo port install py-numpy
```
4. Select the right compiler by setting the environment variables, e.g.
```
$ export CC=clang-mp-9.0
$ export CXX=clang++-mp-9.0
```
### On Ubuntu
It is good practice to update all packages before you install Track1D:
```
$ sudo apt-get update
$ sudo apt-get upgrade
```
In order to install the few necessary packages on Ubuntu, you must give the following command:
```
$ sudo apt install libgsl-dev fftw-dev git
```
If you intend to use Track1D within Octave, you should also install this package:
```
$ sudo apt install liboctave-dev
```
If you intend to use Track1D within Python, you should make sure Python is installed on your system, together with its library NumPy:
```
$ sudo apt install libpython-dev python-dev python-numpy
```

## Downloading Track1D
The best way to get Track1D is to clone the Git repository:
```
$ git clone ssh://git@gitlab.cern.ch:7999/XLS-Git/WP6.git
```
The code is in the subdirectory `simulation_codes/beam_dynamics/Track1D`.

## Compiling Track1D
In order to compile Track1D you first need to configure it for your system. First thing, you need to enter its directory:
```
$ cd Track1D
```
You can compile it with:
```
$ make
```
This will creates the files `Track1D.oct` to be used in Octave, and the files `RF_Track.py` and `_RF_Track.so` to be used in Python.

## Using Track1D
For using Track1D with Octave, you should start your Octave scripts with the command:
```
octave:1> addpath(’/Users/alatina/Codes/Track1D’);
```
where the base path `/Users/alatina`  is just an example.  You can add this command to the file `.octaverc` in your home directory to run it automatically whenever Octave is launched.
In Python, you should give the commands:
```
>>> import os,sys
>>> BIN = os.path.expanduser("/Users/alatina/Codes/Track1D")
>>> sys.path.append(BIN)
```
where the base path `/Users/alatina`  is just an example.

