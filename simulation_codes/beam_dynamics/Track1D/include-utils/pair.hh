#ifndef pair_hh
#define pair_hh

#include <iostream>

template <typename T> struct Pair {
  T x;
  T y;
  Pair() {}
  Pair(T u, T v ) : x(u), y(v) {} 
  friend T average(const Pair &t ) { return (t.x + t.y) * 0.5; }
  friend std::ostream &operator<<(std::ostream &stream, const Pair &p ) { return stream << "\n x = " << p.x << "\n y = " << p.y << std::endl; }
};

#endif /* pair_hh */
