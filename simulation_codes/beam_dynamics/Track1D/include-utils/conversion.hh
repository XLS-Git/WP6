#ifndef conversion_hh
#define conversion_hh

#include <gsl/gsl_math.h>
#include "constants.hh"

static inline double deg2rad(double x)
{
  return x*M_PI/180.0;
}

static inline double rad2deg(double x)
{
  return x*180.0/M_PI;
}

static inline double freq2lambda(double x ) // GHz to meters
{
  return C_LIGHT/(x*1e9);
}

static inline double lambda2freq(double x ) // meters to GHz
{
  return C_LIGHT/(x*1e9);
}

#endif /* conversion_hh */
