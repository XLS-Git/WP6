/* Track1D.i */

%{
#include "Track1D.hh"
#include "lattice.hh"
#include "params.hh"
#include "element.hh"
#include "drift.hh"
#include "quadrupole.hh"
#include "cavity.hh"
#include "cavity_array.hh"
#include "chicane.hh"
#include "bunch1d.hh"
%}

%include "std_vector.i"
%include "matrixnd.i"
%include "static_matrix_33.i"
%include "static_matrix_66.i"

%template(ElementPtr_vector) std::vector<Element*>;
%ignore Lattice::operator=; 

%include "Track1D.hh"
%include "mesh1d.i"
%include "params.hh"
%include "element.hh"
%include "drift.hh"
%include "cavity.hh"
%include "cavity_array.hh"
%include "quadrupole.hh"
%include "chicane.hh"
%include "lattice.hh"

%include "bunch1d.hh"
