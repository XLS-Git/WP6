#ifndef bunch1d_hh
#define bunch1d_hh

#include "matrixnd.hh"
#include "constants.hh"

struct Bunch1d {
  double mass;   ///< GeV/c^2
  double charge; ///< Number of particles
  MatrixNd data; ///< [ x x' ] or [ y y' ] or [ t P ] with P is the total momentum [ um GeV/c ]
public:
  Bunch1d(size_t N ) : mass(EMASS), charge(2e9), data(N,2) {}
  Bunch1d(double _mass, double _charge, size_t N ) : mass(_mass), charge(_charge), data(N,2) {}
  Bunch1d(double _mass, double _charge, const MatrixNd &x ) : mass(_mass), charge(_charge)
  {
    if (x.columns()==2) {
      data = x;
    } else {
      throw "initialization matrix must have 2 columns\n";
    }
  }
  size_t get_size() const { return data.rows(); }
  double get_charge() const { return charge; }
  void set_charge(double _charge ) { charge = _charge; }
  double get_mass() const { return mass; }
  void set_mass(double _mass ) { mass = _mass; }
  const MatrixNd &get_data() const { return data; }
  MatrixNd &get_data() { return data; }
  void set_data(const MatrixNd &x ) 
  {
    if (x.columns()==2) {
      data = x;
    } else {
      throw "RHS matrix must have 2 columns\n";
    }
  }
  double get_mean_1() const
  {
    double a=0.0;
    for (size_t i=0; i<data.rows(); i++) a += data[i][0];
    return a/data.rows();
  }
  double get_mean_2() const
  {
    double a=0.0;
    for (size_t i=0; i<data.rows(); i++) a += data[i][1];
    return a/data.rows();
  }
  double get_std_1() const
  {
    double m=get_mean_1();
    double a=0.0;
    for (size_t i=0; i<data.rows(); i++) {
      double x=data[i][0]-m;
      a += x*x;
    }
    return sqrt(a/data.rows());
  }
  double get_std_2() const
  {
    double m=get_mean_2();
    double a=0.0;
    for (size_t i=0; i<data.rows(); i++) {
      double x=data[i][1]-m;
      a += x*x;
    }
    return sqrt(a/data.rows());
  }
  MatrixNd get_cov() const
  {
    double m1=get_mean_1();
    double m2=get_mean_2();
    double c11=0.0;
    double c12=0.0;
    double c22=0.0;
    for (size_t i=0; i<data.rows(); i++) {
      double x1=data[i][0]-m1;
      double x2=data[i][1]-m2;
      c11 += x1*x1;
      c12 += x1*x2;
      c22 += x2*x2;
    }
    MatrixNd c(2,2);
    c[0][0] = c11/data.rows();
    c[0][1] = c[1][0] = c12/data.rows();
    c[1][1] = c22/data.rows();
    return c;
  }

};

#endif /* bunch1d_hh */
