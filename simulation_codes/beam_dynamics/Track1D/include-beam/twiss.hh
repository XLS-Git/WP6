#ifndef twiss_hh
#define twiss_hh

struct Twiss {
  double beta;
  double alpha;
};

#endif /* twiss_hh */
