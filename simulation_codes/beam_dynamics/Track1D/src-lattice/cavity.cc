#include <algorithm>
#include <cstdlib>

#include <gsl/gsl_fft_halfcomplex.h>
#include <gsl/gsl_fft_complex.h>
#include <gsl/gsl_fft_real.h>

#define REAL(z,i) ((z)[2*(i)])
#define IMAG(z,i) ((z)[2*(i)+1])

#include "cavity.hh"
#include "numtools.hh"

std::pair<std::valarray<double>, std::valarray<double> >
Cavity::wake_z_convolve(const Bunch1d &bunch, size_t Nslices ) const ///< returns an energy loss in GeV
{
  const size_t bunch_size = bunch.get_size();
  const double z_min = bunch.data[0][0];
  const double z_max = bunch.data[bunch_size-1][0];
  // number of slices for wakefield computation (must be a power of two)
  std::valarray<double> z = linspace(z_min, z_max, Nslices+1);
  // data space for the convolution (must be a power of two, and twice the length of the functions to convolve)
  const size_t Nslices_conv = 2*Nslices;
  double data_Wz_real[Nslices_conv];
  double data_dQ_real[Nslices_conv];
  for (size_t i=0; i<Nslices; i++)
    data_dQ_real[i] = 0.0;
  // computes the charge distribution integrals and prepares the arrays for the FFTs
  for (size_t i=0, slice=0; i<bunch_size; i++) {
    while (bunch.data[i][0]>z[slice+1] && slice<Nslices)
      slice++;
    data_dQ_real[slice] += 1;
  }
  const double bunch_charge = bunch.get_charge();
  const double bunch_charge_pC = bunch_charge * ECHARGE * 1e12;
  const double length = get_length();
  for (size_t i=0; i<Nslices; i++) {
    const double dz = (z[i] - z[0]) / 1e6; // um to m
    data_Wz_real[i] = length *  params.get_cell().W_long(dz);
    data_dQ_real[i] *= bunch_charge_pC / bunch_size;
  }
  data_Wz_real[0] *= 0.5;
  for (size_t i=Nslices; i<Nslices_conv; i++) {
    data_Wz_real[i] = 0.0;
    data_dQ_real[i] = 0.0;
  }
  // compute the convolution
  {
    // auxiliary arrays
    double aux_Wk[2*Nslices_conv];
    double aux_dQ[2*Nslices_conv];
    //////// Wz convolution
    gsl_fft_real_radix2_transform (data_Wz_real, 1, Nslices_conv);
    gsl_fft_real_radix2_transform (data_dQ_real, 1, Nslices_conv);
    gsl_fft_halfcomplex_radix2_unpack (data_Wz_real, aux_Wk, 1, Nslices_conv);
    gsl_fft_halfcomplex_radix2_unpack (data_dQ_real, aux_dQ, 1, Nslices_conv);
    for (size_t i=0; i<Nslices_conv; i++) {
      std::complex<double> Wk_i = std::complex<double>(REAL(aux_Wk,i), IMAG(aux_Wk,i));
      std::complex<double> dQ_i = std::complex<double>(REAL(aux_dQ,i), IMAG(aux_dQ,i));
      std::complex<double> t = Wk_i * dQ_i;
      REAL(aux_Wk,i) = t.real();
      IMAG(aux_Wk,i) = t.imag();
    }
    gsl_fft_complex_radix2_inverse (aux_Wk, 1, Nslices_conv);
    for (size_t i=0; i<Nslices; i++) data_Wz_real[i] = REAL(aux_Wk,i) / 1e9; // from e*V to e*GV
  }
  return std::pair<std::valarray<double>, std::valarray<double> >(z, std::valarray<double>(data_Wz_real, Nslices));
}

void Cavity::track_z(Bunch1d &bunch )
{

  const size_t bunch_size = bunch.get_size();
  const double length = get_length();
  const double k0 = 2.0*M_PI/(freq2lambda(params.get_frequency())*1e6); // rad/um
  if (do_wakefields) {
    bool need_sorting = false;
    for (size_t i=1; i<bunch_size; i++) {
      if (bunch.data[i][0] < bunch.data[i-1][0]) {
	need_sorting = true;
	break;
      }
    }
    if (need_sorting) {
      struct Particle {
	double t;
	double momentum;
	bool operator < (const Particle &p ) const { return t < p.t; }
      } *particles = new Particle[bunch_size];
      if (particles) {
	for (size_t i=0; i<bunch_size; i++) {
	  particles[i].t = bunch.data[i][0];
	  particles[i].momentum = bunch.data[i][1];
	}
	std::sort(particles, particles+bunch_size);
	for (size_t i=0; i<bunch_size; i++) {
	  bunch.data[i][0] = particles[i].t;
	  bunch.data[i][1] = particles[i].momentum;
	}
	delete []particles;
      } else {
	std::cerr << "not enough memory\n";
	abort();
      }
    }
    const size_t Nslices = 64;
    const auto &data = wake_z_convolve(bunch, Nslices);
    const std::valarray<double> &z = data.first;
    const std::valarray<double> &data_Wz = data.second;
    const double dz = z[1] - z[0];
    double dE = data_Wz[0];
    for (size_t i=0, slice=0; i<bunch_size; i++) {
      const double z_ = bunch.data[i][0];
      while (z_ > z[slice+1] && slice<Nslices)
	slice++;
      if (slice<Nslices-1) {
	double a = (z_ - z[slice]) / dz;
	dE = (1.0 - a) * data_Wz[slice] + a * data_Wz[slice+1];
      } else {
	dE = data_Wz[Nslices-1];
      }
      bunch.data[i][1] += cos(phase - k0 * z_) * gradient * length - dE;
    }
  } else {
    for (size_t i=0; i<bunch_size; i++) {
      bunch.data[i][1] += cos(phase - k0 * bunch.data[i][0]) * gradient * length;
    }
  }
}

