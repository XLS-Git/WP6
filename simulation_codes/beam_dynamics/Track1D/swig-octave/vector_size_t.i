/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2020 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#if defined(SWIGOCTAVE)

// Vector of indexes as input argument
%typemap(in) const std::vector<size_t> & {
  if ($input.is_real_type()) {
    if (($1 = new std::vector<size_t>($input.length()))) {
      // In Octave, we have decided to treat size_t as doubles instead of int32
      // Array<int> array = $input.int_vector_value();
      Matrix array = $input.matrix_value();
      for (int i=0; i<array.length(); i++)
        (*$1)[i] = size_t(array(i));
    }
  }
}

%typemap(freearg) const std::vector<size_t> & {
  if ($1) delete $1;
}

%typemap(typecheck) const std::vector<size_t> & {
  octave_value obj = $input;
  $1 = obj.is_real_type() ? 1 : 0;
}

%typemap(argout,noblock=1) const std::vector<size_t> & {
}

// Vector of indexes as output argument
%typemap(in, numinputs=0) std::vector<size_t> & (std::vector<size_t> temp ) {
  $1 = &temp;
}

%typemap(argout) std::vector<size_t> & {
  dim_vector dv;
  dv.resize(2);
  dv.elem(0)=1;
  dv.elem(1)=$1->size();
  // In Octave, we have decided to treat size_t as doubles instead of int32
  // uint32NDArray res(dv);
  Matrix res(dv);
  for (size_t i=0; i<$1->size(); i++)
    res(i) = double((*$1)[i]);
  $result->append(res);
}

%typemap(freearg,noblock=1) std::vector<size_t> & {
}

#endif
