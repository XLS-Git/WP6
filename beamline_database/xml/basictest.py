import xml.etree.ElementTree as ET

tree = ET.parse('beamline.xml')
root = tree.getroot()

sectors = root.findall('sector')

machine = 'xls_sx_1000Hz'
sector = 'xls_linac'
name = 'LN0_QD_F_V01'

element = root.findall("./element[@name='"+name+"']/")

length = float([ elem.attrib['design'] for elem in element if elem.tag == 'length' ][0])
data = {}
for elem in element:
    if elem.tag == 'quadrupole':
        data['type'] = 'Quadrupole'
        attributes = elem.findall("./machine[@name='"+machine+"']/")
        if not attributes:
            print("error: "+elem.tag+" '"+name+"' does not contain attributes for machine '"+machine+"'")
            exit(1)
        gradient = float([ attr.attrib['design'] for attr in attributes if attr.tag == 'gradient' ][0])
        data['strength'] = gradient * length / 0.299792458;

