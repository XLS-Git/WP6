import math
import sys

def find_attribute(attributes, tag, name, default):
    attrs = [ attr.attrib[name] for attr in attributes if attr.tag == tag ]
    if attrs:
        return float(attrs[0])
    else:
        return default

def export_to_PLACET(root, sector, machine):
    machine_name = machine.get('name')
    for element_ref in sector:
        name = element_ref.get('ref')
        length = [ L.get('design') for L in root.findall("./element[@name='"+name+"']/length[@design]") ]
        if length:
            length = float(length[0])
        else:
            length = 0.0
        data = {}
        for elem in root.findall("./element[@name='"+name+"']/"):
            attributes = elem.findall("./machine[@name='"+machine_name+"']/")
            if not attributes:
                #print("WARNING: "+elem.tag+" '"+name+"' does not contain specific attributes for machine '"+machine_name+"', using defaults.", file=sys.stderr)
                attributes = elem.findall('./*')
            if elem.tag == 'aperture':
                data['aperture_x'] = find_attribute(attributes, 'x_limit', 'design', 0.0)
                data['aperture_y'] = find_attribute(attributes, 'y_limit', 'design', 0.0)
            elif elem.tag == 'quadrupole':
                data['type'] = 'Quadrupole'
                data['strength'] = find_attribute(attributes, 'gradient', 'design', 0.0) * length / 0.299792458
            elif elem.tag == 'rf_cavity':
                data['type'] = 'Cavity'
                data['frequency'] = find_attribute(attributes, 'rf_freq', 'design', 0.0) / 1e9
                data['gradient']  = find_attribute(attributes, 'gradient', 'design', 0.0) / 1e9
                data['phase']     = find_attribute(attributes, 'phase0', 'design', 0.0) * 180.0 / math.pi
            elif elem.tag == 'bend':
                data['type'] = 'Sbend'
                data['angle'] = find_attribute(attributes, 'angle', 'design', 0.0) 
                data['E1']    = find_attribute(attributes, 'e1', 'design', 0.0) 
                data['E2']    = find_attribute(attributes, 'e2', 'design', 0.0) 
            elif not 'type' in data:
                data['type'] = 'Drift'
        print(data['type']+" -name "+name+" -length "+str(length), end = '')
        del data['type']
        for i in data:
            print(" -"+i+" "+str(data[i]), end='')
        print()

