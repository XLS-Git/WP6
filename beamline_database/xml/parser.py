#! /opt/local/bin/python

import argparse
import math
import sys
import PLACET
import xml.etree.ElementTree as ET

tree2 = ET.fromstring('<power design = "1.23" units = "MW" />')

parser = argparse.ArgumentParser(description='CompactLight XML Parser.')    
parser.add_argument('action', type=str, nargs=1, help='action to be undertaken: export | set | get | dump | list')
parser.add_argument('--filename', type=str, nargs=1, help='input XML file name', default='beamline.xml')
parser.add_argument('--machine', type=str, nargs=1, help='machine name')
parser.add_argument('--sector', type=str, nargs=1, help='sector name')
parser.add_argument('--element', type=str, nargs=1, help='element name')
parser.add_argument('--verbose', '-v', action='count', default=0)

#args = parser.parse_args()
#args = parser.parse_args('export --machine xls_sx_1000Hz --sector xls_linac'.split())
args, argument = parser.parse_known_args('append --sector xls_injector PIPPO'.split())
tree = ET.parse(args.filename)
root = tree.getroot()

## parser.py set --element LN0_QD_F_V01 --machine xls_sx_1000Hz '<power design = "1.23" units = "MW" />'

# Export PLACET | ELEGANT | GPT | RFT

# Dump CSV file by sector + machine + attribute(s) 

# Add | remove objects [add new entry in the XML file]
## parser.py add | remove [ element | sector | machine ] 

# Append and insert elements in a sector
## ## parser.py append --sector LN0_QD_F_V01 MYELEMENT

# Merge from XML file (e.g. result PLACET matching)

# [Internal Purge removes dead leaves, e.g. machine attributes that do not correspond to any <machine />]

def find_sector(root, name):
    sectors = root.findall('./sector')
    sector = [ x for x in sectors if x.get('name') == name ]
    if not sector:
        print("ERROR: sector '"+name+"' not found.", file=sys.stderr)
        exit(1)
    return sector[0]
print
def find_machine(root, name):
    machines = root.findall('./machine')
    machine = [ x for x in machines if x.get('name') == name ]
    if not machine:
        print("ERROR: machine '"+name+"' not found.", file=sys.stderr)
        exit(1)
    return machine[0]

def find_element(root, name):
    elements = root.findall('./element')
    element = [ x for x in elements if x.get('name') == name ]
    if not element:
        print("ERROR: element '"+name+"' not found.", file=sys.stderr)
        exit(1)
    return element[0]

print('CompactLight XML parser, version 0.0.1\n')

# Take action
if args.action[0] == 'list':
    print('The file \''+args.filename+'\' contains the following machines:')
    machines = root.findall('machine')
    for machine in machines:
        print(' *', machine.get('name'))
    print('\nand the following sectors:')
    sectors = root.findall('sector')
    for sector in sectors:
        print(' +-->', sector.get('name'))
    print(file=sys.stderr)
    exit(0)

if args.action[0] == 'export':
    if args.machine is None or args.sector is None:
        print('ERROR: you need to specify a sector and the machine it belongs to.\n', file=sys.stderr)
        exit(1)
    print('Exporting to PLACET format \''+args.sector[0]+'\'', file=sys.stderr)
    sector  = find_sector (root, args.sector[0])
    machine = find_machine(root, args.machine[0])
    PLACET.export_to_PLACET(root, sector, machine)
    exit(0)

if args.action[0] == 'append':
    if args.sector is None:
        print('ERROR: you need to specify a sector.\n', file=sys.stderr)
        exit(1)
    sector  = find_sector(root, args.sector[0])
    ref = ET.Element('element');
    ref.set('ref', argument[0])
    sector.insert(-1, ref)
    tree.write(args.filename)
    exit(0)
