proc save_elements_table { beamlinename } {

    puts "SAVING FOOTPRINT..."
    puts "SAVING ELEMENTS TABLE..."

    BeamlineSaveFootprint -beamline $beamlinename -file ${beamlinename}.footprint

    Octave {
        fid = fopen('${beamlinename}.footprint', 'r');
        F = textscan(fid, '%f %f %f %f %s %s %f %f %f %f', 'delimiter', ' ', 'HeaderLines', 11);
        fclose(fid);
    }
    
    Octave {
        global COLS_NAME % preceeded by "NAME" and "KEYWORD" and followed by "Type"
        COLS_NAME = { "S" ; "L" ;  "E0" ; "X" ; "Y" ; "Z" ; "PHI" ; "THETA" ; "PSI" ; "K1L" ; "K1SL" ; "K2L" ; "K2SL" ; "KS" ; "ANGLE" ; "E1" ; "E2" ; "STRENGTH" };
        ALL = placet_element_get_attributes("$beamlinename");
        TABLE_NAME = [];
        TABLE_KEY  = [];
        TABLE_DATA = [];
        TABLE_AUX  = [];
        function T = append_to_row(T, name, value)
	    global COLS_NAME
	    T(find(strcmp(COLS_NAME, name))) = value;
        end

        %% element by element
        sigma_quad = 100; % um, rms misalignment of quadrupoles
        last_quad_K1L = 0.0; % 1/m

        index = 1;    
        for i=1:size(ALL,1);
            THIS = ALL{i};
                T = zeros(1, size(COLS_NAME,1));
                T = append_to_row(T, "S", THIS.s - 0.5*THIS.length);
                T = append_to_row(T, "L", THIS.length);
                T = append_to_row(T, "E0", THIS.e0);
            switch THIS.type_name
                case "quadrupole"
                    strength = abs(THIS.strength / 0.299792458);
                    T = append_to_row(T, "K1L", THIS.strength / THIS.e0);
                    T = append_to_row(T, "STRENGTH", strength); % GV/c/m = (1 / 0.299792458) T
                    last_quad_K1L = THIS.strength / THIS.e0; % 1/m
                case "sbend"
                    T = append_to_row(T, "ANGLE", THIS.angle);
                    T = append_to_row(T, "E1", THIS.E1);
                    T = append_to_row(T, "E2", THIS.E2);
                    T = append_to_row(T, "STRENGTH", THIS.e0 * THIS.angle / 0.299792458); % GV/c = (1 / 0.299792458) T*m
                case "multipole"
                    THIS.type_name = "sextupole";
                    strength = abs(THIS.strength / 0.299792458);
                    T = append_to_row(T, "K2L", THIS.strength / THIS.e0);
                    T = append_to_row(T, "STRENGTH", strength); % GV/c/m^2 = (1 / 0.299792458) T/m
                case "bpm"
                case "dipole"
                    THIS.type_name = "kicker";
                    max_strength = 3*sigma_quad*abs(last_quad_K1L)*THIS.e0*1e3; % V, 3 * sigma_quad * last_quad_K1L * THIS.e0
                    T = append_to_row(T, "STRENGTH", max_strength / 299792458); % T*m
                case "cavity"
                    T = append_to_row(T, "STRENGTH", 1e3 * THIS.gradient * THIS.length); % MV
                    T = append_to_row(T, "ANGLE", THIS.phase / 180.0 * pi); % rad
                case "solenoid"
                    T = append_to_row(T, "KS", 0.29979246 * THIS.bz); % solenoid strength from T/(GeV/c) to 1/m
                    T = append_to_row(T, "STRENGTH", 2.9); % T
                case "drift"
                    if strfind(THIS.name, "MARKER")
                        THIS.type_name = "marker";
	            elseif strfind(THIS.name, "_VAC_SV")
                        THIS.type_name = "valve";
                    endif
            end

            %% for all types
            if ~strcmp(THIS.type_name, "drift")
                T = append_to_row(T, "X", F{1}(i+1)); % +1 to skip [BEGIN] marker
                T = append_to_row(T, "Y", F{2}(i+1)); % +1 to skip [BEGIN] marker
                T = append_to_row(T, "Z", F{3}(i+1)); % +1 to skip [BEGIN] marker
                T = append_to_row(T, "PHI", F{8}(i+1)); % +1 to skip [BEGIN] marker
                T = append_to_row(T, "THETA", F{9}(i+1)); % +1 to skip [BEGIN] marker
                T = append_to_row(T, "PSI", F{10}(i+1)); % +1 to skip [BEGIN] marker
                TABLE_DATA(index,:) = T;
                TABLE_NAME{index} = THIS.name;
                TABLE_KEY {index} = toupper(THIS.type_name);
                TABLE_AUX {index} = THIS.comment;
                index++;
            end
        end

        %% save on disk
        fid = fopen('${beamlinename}.table', 'w');
        if fid != -1
	    fprintf(fid, '\# XLS EXTENDED TABLE v0.1\n');
	    fprintf(fid, '\# Generated automatically by PLACET from lattice ''$beamlinename'' on %s', ctime(time));
            fprintf(fid, '* NAME\tKEYWORD\t%s\tAUX\n', strjoin(COLS_NAME, '\t'));
            fprintf(fid, '* <STRING> <STRING> [m] [m] [GeV] [m] [m] [m] [rad] [rad] [rad] [m^-1] [m^-1] [m^-2] [m^-2] [m^-1] [rad] [rad] [rad] <STRING> <STRING>\n');
            for i=1:size(TABLE_DATA,1)
                fprintf(fid, [ '%s\t%s\t%f\t' repmat('%f\t', 1, size(TABLE_DATA,2)-1) '%s\n' ], TABLE_NAME{i}, TABLE_KEY{i}, TABLE_DATA(i,:), TABLE_AUX{i});
            end
            fclose(fid);
        end
    }
}
