# XLS Extended Table v0.1


| COLUMNS  |                PROPERTY                 |   UNIT     |              DESCRIPTION               |
|----------|-----------------------------------------|------------|----------------------------------------|
|    1     |                  NAME                   | [STRING]   |                 name                   |
|    2     |                KEYWORD                  | [STRING]   |             element type               |
|    3     |                   S                     |     m      |            center along S              |
|    4     |                   L                     |     m      |                length                  |
|    5     |                   E                     |    GeV     |        beam energy at entrance         |
|   6-8    |                X, Y, Z                  |     m      | 3D position of the element center [*]  |
|  9-11    |            PHI, THETA, PSI              |    rad     |     3D orientation (Euler angles) [**] |
|  12-19   | K1L, K1SL, K2L, K2SL, KS, ANGLE, E1, E2 | [various]  |         beam optics quantities         |
|   20     |                STRENGTH                 | [various]  |          element strength [***]        |
|   21     |                  AUX                    | [STRING]   |  any auxiliary information associated  |

[*] In a Cartesian 3D reference system where the beam moves along the X direction, the dispersion plane is XY, and the 'height' axis is Z.

[**] It follows the same convention adopted by ROOT and Geant4. 

[***] Strength of the element, useful for power consumption estimates:
* for bending magnets: the integrated strength in Tm
* for quadrupoles: the integrated strength in T
* for structures (cavities): the integrated voltage in MV
* for kickers (dipole correctors) an estimate of the max strength, as integrated transverse voltage in Tm
* for sextupoles the integrated strength in T/m
* for solenoids the max field in T

The column delimiter is the TAB character.
