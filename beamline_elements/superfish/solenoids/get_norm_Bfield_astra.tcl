#!/bin/sh  
# \
exec tclsh "$0" "$@"

set fname0 [lindex $argv 0]
set fname2 [lindex $argv 1]
puts "entering -> $fname0"

cd $fname0


set fname "OUTSF7_LINE.TXT"
set lno [lindex [exec wc -l $fname] 0 ]

puts $lno



set fin [open $fname r]

set lineall {}
set emax -1e10;

for {set i 0} {$i < $lno} {incr i} {
    gets $fin line
    if {$i == 22} { set foutn0 [lindex $line 2] }
    
    if { $i > 37 && $i < [expr $lno-4] } {
#             set etmp [expr abs([lindex $line 3])]
            set etmp [expr  [lindex $line 3]]
            if {$etmp > $emax } {set emax $etmp}
            set lineall [lappend lineall $line]
           
    }

}


close $fin
set foutn [string map {.T35. .DAT} $foutn0]
puts "output file is $foutn maximum amplitude $emax"


set fou [open $foutn w]

foreach line  $lineall {
    set zn [expr [lindex $line 1]*1.e-2]
    set en [expr [lindex $line 3]/$emax]
    puts $fou [format "%12.8f  %12.8f" $zn $en ]
}

close $fou

exec mv $foutn ../




