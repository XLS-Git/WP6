#!/bin/sh  
# \
exec tclsh "$0" "$@"

set fname0 [lindex $argv 0]
set fname2 [lindex $argv 1]
puts "entering -> $fname0"

cd $fname0


set fname "OUTSF7.TXT"
set lno [lindex [exec wc -l $fname] 0 ]

puts $lno



set fin [open $fname r]

set lineall {}


for {set i 0} {$i < $lno} {incr i} {
    gets $fin line
    if {$i == 22} { set foutn0 [lindex $line 2] }
    if {$i == 30} { 
#         puts $line
        set nrs [lindex $line 4]
        set nzs [lindex $line 5]
        puts "$nrs $nzs"
    }
    
    if { $i > 33 && $i < [expr $lno-0]  } {
#             set etmp [expr abs([lindex $line 3])]
#             set etmp [expr  [lindex $line 3]]
#             if {$etmp > $emax } {set emax $etmp}
            set lineall [lappend lineall $line]
           
    }

}

# 
close $fin
set foutn [string map {.T35. .GDF} $foutn0]
puts "output file is $foutn"


set fou [open tmp w]

puts $fou [format "%15s %15s %15s %15s %15s %15s" "R" "Z" "Br" "Bz" "|B|" "A"  ]
#  puts $fou "      R          Z         Br         Bz        |B|          A "
# 
foreach line  $lineall {
#     puts $line
    set rn [expr [lindex $line 0]*1.0e-2]   ;# m
    set zn [expr [lindex $line 1]*1.0e-2]   ;# m
    set br [expr [lindex $line 2]*1.0]      ;# T
    set bz [expr [lindex $line 3]*1.0]      ;# T
    set ba [expr [lindex $line 4]*1.0]      ;# T
    set an [expr [lindex $line 5]*1.0e-2]   ;# Tm
    puts $fou [format "%15.8e %15.8e %15.8e %15.8e %15.8e %15.8e" $rn $zn $br $bz $ba $an ]
}

close $fou

exec -ignorestderr asci2gdf -o $foutn tmp

exec rm tmp

exec mv $foutn ../




