
  Echo Delete old files,if any,before starting.
  del  /q  *.txt *.t35 *.tbl *.sfo *.log *.pmi *.inf *.t7 *.qkp *.seg *00.CCL *01.CCL *01.ell *00.ELL *.rdp > nul
  start/w %SFdir%automesh xband_cavity_solenoid_20cm_30-05-19_v1.am
start/w %SFdir%poisson xband_cavity_solenoid_20cm_30-05-19_v1.T35
start/w %SFdir%WSFplot xband_cavity_solenoid_20cm_30-05-19_v1.T35 2
start/w %SFdir%sf7 xband_cavity_solenoid_20cm_30-05-19_v1.in7 xband_cavity_solenoid_20cm_30-05-19_v1.T35
start/w %SFdir%tablplot xband_cavity_solenoid_20cm_30-05-19_v11.TBL 2
Echo line plot finished
copy OUTSF7.TXT OUTSF7_LINE.TXT
move xband_cavity_solenoid_20cm_30-05-19_v1.in7 xband_cavity_solenoid_20cm_30-05-19_v1__.in7
move xband_cavity_solenoid_20cm_30-05-19_v1_.in7 xband_cavity_solenoid_20cm_30-05-19_v1.in7
start/w %SFdir%sf7 xband_cavity_solenoid_20cm_30-05-19_v1.in7 xband_cavity_solenoid_20cm_30-05-19_v1.T35
move xband_cavity_solenoid_20cm_30-05-19_v1.in7 xband_cavity_solenoid_20cm_30-05-19_v1_.in7
move xband_cavity_solenoid_20cm_30-05-19_v1__.in7 xband_cavity_solenoid_20cm_30-05-19_v1.in7
