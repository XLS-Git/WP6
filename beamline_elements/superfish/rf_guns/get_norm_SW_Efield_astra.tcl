#!/bin/sh  
# \
exec tclsh "$0" "$@"

set fname0 [lindex $argv 0]
set fname2 [lindex $argv 1]
puts "entering -> $fname0"

cd $fname0


set fname "OUTSF7_LINE.TXT"
set fnamesf "OUTFIS.TXT"
set lno [lindex [exec wc -l $fname] 0 ]

# puts $lno



set fin [open $fname r]

set lineall {}
set emax -1e10;

for {set i 0} {$i < $lno} {incr i} {
    gets $fin line
    if {$i == 23} { set foutn0 [lindex $line 3] }
    
    if { $i > 33 && $i < [expr $lno-1] } {
            set etmp [expr abs([lindex $line 2])]
#             set etmp [expr  [lindex $line 3]]
            if {$etmp > $emax } {set emax $etmp}
            set lineall [lappend lineall $line]     
    }

}
close $fin


set fin0 [open $fnamesf r]
while {[gets $fin0 line] >= 0}  {
#     puts $line
    set line0 [lindex $line 0]
    set line2 [lindex $line 2]
    set line3 [lindex $line 3]
    set line4 [lindex $line 4]
    if { $line0 == "FREQ" && $line2 == "RF" } {
        set cav_freq [lindex $line 1]
    }
}
close $fin0

# FREQ             2997.37903     RF cavity resonant frequency
    
#     if {$line0 == "FREQ" && $line2 == "RF" && $line3 == "cavity"  && $line3 == "resonant"} {


set foutn [string map {.T35 _ASTRA.DAT} $foutn0]
puts "output file is $foutn maximum amplitude $emax"
puts "cavity resonanat frequency =  $cav_freq  MHz"


set fou [open $foutn w]

foreach line  $lineall {
#     puts $line
    set zn [expr [lindex $line 0]*1.e-2]
    set en [expr [lindex $line 2]/$emax]
    puts $fou [format "%12.8f  %12.8f" $zn $en ]
}

close $fou

exec mv $foutn ../





