
Echo Delete old files, if any, before starting. 
del /q   *.txt *.t35 *.tbl *.sfo  *.log *.pmi *.inf *.t7 *.qkp *.seg *.AM *00.CCL *01.CCL *01.ell  *00.ELL *.rdp   >nul

Echo Run autofish.

start /w  %SFdir%autofish   X-band_gun_5.6cell.af

Echo make plot

start /w  %SFdir%WSFplot    X-band_gun_5.6cell.t35    3

Echo create axis field file

start /w  %SFdir%sf7        X-band_gun_5.6cell.IN7
start /w   %SFdir%tablplot  X-band_gun_5.6cell1.TBL 3

Echo line plot finished copying result
copy OUTSF7.TXT OUTSF7_LINE.TXT
move  X-band_gun_5.6cell.IN7  X-band_gun_5.6cell__.IN7
move   X-band_gun_5.6cell_.IN7  X-band_gun_5.6cell.IN7
start/w %SFdir%sf7  X-band_gun_5.6cell.IN7  
move  X-band_gun_5.6cell.IN7  X-band_gun_5.6cell_.IN7
move  X-band_gun_5.6cell__.IN7  X-band_gun_5.6cell.IN7
