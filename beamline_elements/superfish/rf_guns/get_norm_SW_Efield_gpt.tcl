#!/bin/sh  
# \
exec tclsh "$0" "$@"

set fname0 [lindex $argv 0]
set fname2 [lindex $argv 1]
puts "entering -> $fname0"

cd $fname0


set fname "OUTSF7.TXT"
set fnamesf "OUTFIS.TXT"

set lno [lindex [exec wc -l $fname] 0 ]
puts $lno



set fin [open $fname r]

set lineall {}


for {set i 0} {$i < $lno} {incr i} {
    gets $fin line
    if {$i == 23} { set foutn0 [lindex $line 3] }
    if {$i == 30} { 
        puts $line
        set nrs [lindex $line 4]
        set nzs [lindex $line 5]
        puts "$nrs $nzs"
    }
    
    if { $i > 33 && $i < [expr $lno-10]  } {
#             set etmp [expr abs([lindex $line 3])]
#             set etmp [expr  [lindex $line 3]]
#             if {$etmp > $emax } {set emax $etmp}
            set lineall [lappend lineall $line]
           
    }

}

close $fin

set fin0 [open $fnamesf r]
while {[gets $fin0 line] >= 0}  {
#     puts $line
    set line0 [lindex $line 0]
    set line2 [lindex $line 2]
    if { $line0 == "FREQ" && $line2 == "RF" } {
        set cav_freq [lindex $line 1]
    }
}
close $fin0

set foutnt [string map {.T35 GPT.DAT} $foutn0]
set foutn [string map {.T35 GPT.GDF} $foutn0]
puts "output file is $foutn"
puts "cavity resonanat frequency =  $cav_freq  MHz"



set fou [open $foutnt w]

puts $fou [format "%15s %15s %15s %15s %15s %15s" "Z" "R" "Ez" "Er" "|E|" "H"  ]

# 
foreach line  $lineall {
#     puts $line
    set zn [expr [lindex $line 0]*1.0e-2]   ;# m
    set rn [expr [lindex $line 1]*1.0e-2]   ;# m
    set ez [expr [lindex $line 2]*1.0e6]      ;# V/m
    set er [expr [lindex $line 3]*1.0e6]      ;# V/m
    set ea [expr [lindex $line 4]*1.0e6]      ;# V/m
    set hn [expr [lindex $line 5]*1.25664e-6]   ;# T
    puts $fou [format "%15.8e %15.8e %15.8e %15.8e %15.8e %15.8e" $zn $rn $ez $er $ea $hn ]
}

close $fou

exec -ignorestderr asci2gdf -o $foutn $foutnt

# exec rm tmp
exec mv $foutnt ../
exec mv $foutn ../




