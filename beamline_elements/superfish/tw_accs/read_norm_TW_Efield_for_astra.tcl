#!/bin/sh  
# \
exec tclsh "$0" "$@"

set cc 2.997924580e10  ;# cm/s

set fname0 [lindex $argv 0]
set nn  [lindex $argv 1]
set mm  [lindex $argv 2]
puts "entering -> $fname0"



cd $fname0

set fname0 [string map { "/" "" } $fname0]

set resltl [string map { " " "" } [concat  $fname0 "_l.SF7"]]
set resltc [string map { " " "" } [concat  $fname0 "_c.SF7"]]
set resltr [string map { " " "" } [concat  $fname0 "_r.SF7"]]
set reslt2l [string map { " " "" } [concat  $fname0 "_l.FIS"]]
set reslt2c [string map { " " "" } [concat  $fname0 "_c.FIS"]]
set reslt2r [string map { " " "" } [concat  $fname0 "_r.FIS"]]
set foutn  [string map { " " "" } [concat  $fname0 "_ASTRA.DAT"]]

puts $resltl
puts $resltc
puts $resltr
puts $reslt2l
puts $reslt2c
puts $reslt2r

set lnol [lindex [exec wc -l $resltl] 0 ]
set lnoc [lindex [exec wc -l $resltc] 0 ]
set lnor [lindex [exec wc -l $resltr] 0 ]



set fin0 [open $reslt2c r]
while {[gets $fin0 line] >= 0}  {
#     puts $line
    set line0 [lindex $line 0]
    set line2 [lindex $line 2]
    set line3 [lindex $line 3]
    set line4 [lindex $line 4]
    if { $line0 == "FREQ" && $line2 == "RF" } {
        set cav_freq [lindex $line 1]
    }
}
close $fin0

set phsl   [expr 360.*$nn/$mm]
set cell_l [expr $cc/($cav_freq*1e6)*$phsl/360.]
set tottwl [expr $cell_l*$mm*0.5]


puts $cav_freq
puts $cell_l
puts $tottwl



set finl [open $resltl r]

set linel {}
set emaxl -1e10;
set zminl 1e10
set zmaxl -1e10
set nil 0

for {set i 0} {$i < $lnol} {incr i} {
    gets $finl line
    if { $i > 33 && $i < [expr $lnol-1]  } {
            set etmp [expr abs([lindex $line 2])]
            set zz   [expr  [lindex $line 0] ]
            if {$etmp > $emaxl } {
                set emaxl $etmp
                set inx_maxl $nil
            }
            
            if {$zz < $zminl } { set zminl $zz }
            if {$zz > $zmaxl } { set zmaxl $zz }
            
            set linel [lappend linel $line]
            incr nil
    }

}
close $finl


set linelp {}
foreach ll $linel {
    set zz [lindex $ll 0]
    set ee [expr [lindex $ll 2]/$emaxl]
    set linelp [lappend linelp "$zz $ee"]
#     puts "$zz $ee"
}



set finc [open $resltc r]

set linec {}
set emaxc -1e10;
set zminc 1e10
set zmaxc -1e10
set nic 0

for {set i 0} {$i < $lnoc} {incr i} {
    gets $finc line
    if { $i > 33 && $i < [expr $lnoc-1]  } {
            set etmp [expr abs([lindex $line 2])]
            set zz   [expr  [lindex $line 0] ]
            if {$etmp > $emaxc } {
                set emaxc $etmp
                set inx_maxc $nic
            }
            
            if {$zz < $zminc } { set zminc $zz }
            if {$zz > $zmaxc } { set zmaxc $zz }
            
            set linec [lappend linec $line]
            incr nic
    }

}
close $finc


set linecp {}
foreach ll $linec {
    set zz [lindex $ll 0]
    set ee [expr [lindex $ll 2]/$emaxl]
    set linecp [lappend linecp "$zz $ee"]
#     puts "$zz $ee"
}



set finr [open $resltr r]

set liner {}
set emaxr -1e10;
set zminr 1e10
set zmaxr -1e10
set nic 0

for {set i 0} {$i < $lnor} {incr i} {
    gets $finr line
    if { $i > 33 && $i < [expr $lnor-1]  } {
            set etmp [expr abs([lindex $line 2])]
            set zz   [expr  [lindex $line 0] ]
            if {$etmp > $emaxr } {
                set emaxr $etmp
                set inx_maxr $nic
            }
            
            if {$zz < $zminr } { set zminr $zz }
            if {$zz > $zmaxr } { set zmaxr $zz }
            
            set liner [lappend liner $line]
            incr nir
    }

}
close $finc


set linerp {}
foreach ll $liner {
    set zz [lindex $ll 0]
    set ee [expr [lindex $ll 2]/$emaxl]
    set linerp [lappend linerp "$zz $ee"]
#     puts "$zz $ee"
}


# puts $zminl
# puts $zmaxl
# puts $zminc
# puts $zmaxc
# puts $zminr
# puts $zmaxr

set lineall $linelp
set linecp [lreplace $linecp 0 0 ]
set lineall [concat $lineall $linecp]
set linerp [lreplace $linerp 0 0 ]
set lineall [concat $lineall $linerp]


# foreach ll $lineall {
#     puts "$ll"
# }
# exit 


set fou [open $foutn w]

# set fou [open tmp w]

puts $fou [format "%15.8e %15.8e %3d %3d %12.8f " [expr $zminc*1e-2] [expr $zmaxc*1e-2] $nn $mm [expr $cav_freq*1e-3] ]
foreach line  $lineall {
#     puts $line
    set zn [expr [lindex $line 0]*1.e-2]
    set en [expr [lindex $line 1]]
    puts $fou [format "%15.8e %15.8e " $zn $en ]
}

close $fou

exec mv $foutn ../





