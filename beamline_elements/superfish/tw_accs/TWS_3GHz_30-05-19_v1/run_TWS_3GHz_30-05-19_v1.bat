 Echo automatic generated
Echo Delete old files, if any, before starting.
del  /q  *.txt *.t35 *.tbl *.sfo *.log *.pmi *.inf *.t7 *.qkp *.seg *00. CCL *01. CCL *01. ell *00. ELL *.rdp *.SF7 *.FIS > nul 
 
start /w %SFdir%autofish  TWS_3GHz_30-05-19_v1_l.af 
start /w %SFdir%WSFplot   TWS_3GHz_30-05-19_v1_l.T35 2  
start /w %SFdir%sf7       TWS_3GHz_30-05-19_v1_l.IN7
start /w %SFdir%tablplot  TWS_3GHz_30-05-19_v1_L1.TBL  2
copy  OUTSF7.TXT TWS_3GHz_30-05-19_v1_l.SF7
copy  OUTFIS.TXT TWS_3GHz_30-05-19_v1_l.FIS

start /w %SFdir%autofish  TWS_3GHz_30-05-19_v1_c.af
start /w %SFdir%WSFplot   TWS_3GHz_30-05-19_v1_c.T35 2 
start /w %SFdir%sf7       TWS_3GHz_30-05-19_v1_c.IN7
start /w %SFdir%tablplot  TWS_3GHz_30-05-19_v1_C1.TBL  2
copy  OUTSF7.TXT TWS_3GHz_30-05-19_v1_c.SF7
copy  OUTFIS.TXT TWS_3GHz_30-05-19_v1_c.FIS

start /w %SFdir%autofish  TWS_3GHz_30-05-19_v1_r.af
start /w %SFdir%WSFplot   TWS_3GHz_30-05-19_v1_r.T35 2 
start /w %SFdir%sf7       TWS_3GHz_30-05-19_v1_r.IN7
start /w %SFdir%tablplot  TWS_3GHz_30-05-19_v1_R1.TBL  2
copy  OUTSF7.TXT TWS_3GHz_30-05-19_v1_r.SF7
copy  OUTFIS.TXT TWS_3GHz_30-05-19_v1_r.FIS

del  /q  *.txt *.sfo *.log *.inf > nul 
 

