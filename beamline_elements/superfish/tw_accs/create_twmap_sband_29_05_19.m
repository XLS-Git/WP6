# #! /bin/octave -qf
# 
# 
# arg_list = argv ();
# 
# [nar ~]= size(arg_list);
# if (nar < 3);
#     printf("check the numer of arguments \nusaage:\n");
#     printf("octave create_twmap.m <filename> <no of cell for construction> <stepsize>\n\n");
#     quit
# endif
# 
# filename= arg_list{1};      #; (* axis field map of strcuture ASTRA form *)
# nconst  = arg_list{2};      #; (* number of cells to be constructed*)
# steps   = arg_list{3};      #; step size for field map

# 1;
# % file to generate distributions
# % 
# 

nconst  = 48;                                #; (* number of cells to be constructed*)
steps   = 0.0002;                             #; step size for field map
filename='TWS_3GHz_30-05-19_v1_ASTRA.DAT';     #; (* axis field map of strcuture ASTRA form *)


fid = fopen(filename, 'r');
pars= fscanf(fid, '%f', [1, 5]);
fld = fscanf(fid, '%f', [2, Inf]);
fclose(fid);
fld = fld';

cc     = 2.997924580e8;
incous = fld(1,1);                  #; (* start location of the field*)
incoue = pars(1);                   #; (* end position of initial coupler- tw cells start location *)
oucous = pars(2);                   #; (* tw cells end location start of out coupler cell*)
oucoue = fld(end,1);                #; (* total length -  end of outcoupler cell *)
nn     = pars(3);                   #; (* 2*pi*nn/mm *)
mm     = pars(4);                   #; (* 2*pi*nn/mm *)
freq   = pars(5)*1e9;               #; (* desgin frequency *)

lincou = incoue-incous;             #; (* length of initial coupler *)
loucou = oucoue-oucous;             #; (* length of output coupler *)
ltwsec = oucous-incoue;             #; (* length of tw section *)
ltwcel = ltwsec/mm;                 #; (* length of one tw cell *)
phsad  = ltwcel*freq/cc*2*pi;       #; (* phase advance per cell*)
ltwall = ltwcel*nconst;              #; (* length of one whole tw section after construction *)

phsadal= rem(ltwall*freq/cc*2*pi,2*pi);

printf("phase advance per cell is %f degree \n", phsad*180/pi)

# if (phsadal > 1e-3);
#     puts("check the phase advance of the cells\n")
#     exit()
# endif
    
# 
# #  calculate the coordinates
# 
inco_str   = 0;
inco_end   = lincou;
twse_str   = inco_end;
twse_end   = twse_str + ltwall;
ouco_str   = twse_end;
ouco_end   = ouco_str + loucou;

printf("total length of structure = %f m \n", ouco_end)

# split the sections 

[nl0 nc]=size(fld);
coupin = []; twsecc = []; coupou = [];

coupin= fld(fld(:,1) <= incoue,:);
twsecc= fld( fld(:,1) >= incoue & fld(:,1) <= oucous,:);
coupou= fld(fld(:,1) >= oucous,:);


[ntci nc]=size(coupin);
[ntws nc]=size(twsecc);
[ntco nc]=size(coupou);



#  since the tw section is symmetric we can do it i belive
#  it is some kind of smoothing for initial and coupler cells
for r=1:floor(ntws/2)
    tmp=(twsecc(r,2)+twsecc(ntws-r,2))*0.5;
    twsecc(r,2)=twsecc(ntws-r,2)=tmp;
endfor

# # normalize all fields to 1.0
coupin(:,2)=coupin(:,2)*1.0/coupin(end,2);
coupou(:,2)=coupou(:,2)*1.0/coupou(1,2);
twsecc(:,2)=twsecc(:,2)*1.0/twsecc(1,2);


# # shift all to zero later on it will be added 
coupin(:, 1)= coupin(:, 1) - incous;
twsecc(:, 1)= twsecc(:, 1) - incoue;
coupou(:, 1)= coupou(:, 1) - oucous;

swall = twsecc;

#  append tw cells 
for kk=1:floor(nconst/mm)+1
    zistart = swall(end,1);
    ztmp=twsecc(:,1)+ zistart;
    atmp=twsecc(:,2);
    tmp=[ztmp, atmp];
    swall=[swall; tmp];
endfor

# # remove similar ones
[~,id,~] = unique(swall(:,1), "last");
swall = swall(sort(id),:);
[ntws nc]=size(swall);




zint0=linspace(0, ltwall, ntws);
zintc=linspace(ltwcel,ltwall+ltwcel, ntws);


swall0=interp1(swall(:,1), swall(:,2),zint0,'spline');
swallc=interp1(swall(:,1), swall(:,2),zintc,'spline');

phase0=0;
twsect0=((swallc .- exp(-j*phsad)*swall0)*exp(-j*phase0));
coupin0=coupin(:,2)*exp(-j*phase0);
coupou0=coupou(:,2)*exp(-j*phase0);


retwsct=imag(twsect0);
recoupi=real(coupin0);
recoupo=real(coupou0);
factt=recoupi(end)/retwsct(1);
retwsct=(retwsct*factt)';

zcoi=coupin(:,1);
ztws=(zint0+zcoi(end))';
zcoo=coupou(:,1)+ztws(end);

imtwsct=(real(twsect0)*factt)';
imcoupi=imag(coupin0);
imcoupo=imag(coupou0);


allreal=[recoupi;retwsct(2:end);recoupo(2:end)];
allimag=[imcoupi;imtwsct(2:end);imcoupo(2:end)];
alliz  =[zcoi;ztws(2:end);zcoo(2:end)];

size(allreal);
size(allimag);
size(alliz);



zfinal=linspace(0,alliz(end),ceil(alliz(end)/steps)+1);
realfinal=interp1(alliz, allreal,zfinal,'spline');
imagfinal=interp1(alliz, allimag,zfinal,'spline');

allmap=[zfinal', realfinal', imagfinal'];



outname = strrep (filename, "ASTRA", "GPT")


fidd = fopen(outname, 'w');
fprintf(fidd, "        z        ReEz        ImEz \n") 
fprintf(fidd, "%9.6f %11.8f %11.8f \n", allmap.'  );
fclose(fidd);
    


clf;
plot(zfinal, realfinal,"r","linewidth", 2,zfinal, imagfinal,"b","linewidth", 2 )
legend ("RealEz", "ImagEz")
set(gca,'fontsize',20);

clear all

