Girder
SetReferenceEnergy  $e0
Drift      -name    "LN0_DR_V1" -length 0.614686613018 
Drift      -name    "LN0_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN0_QD_F_V01"  -length 0.08  -strength [expr 0.08*$e0*10.3784573339] -e0 $e0
Drift      -name    "LN0_DR_QD_ED" -length 0.05 
Drift      -name    "LN0_DR_V2" -length 0.4501427474 
Drift      -name    "LN0_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN0_QD_D_V02"  -length 0.08  -strength [expr 0.08*$e0*-12.1698626687] -e0 $e0
Drift      -name    "LN0_DR_QD_ED" -length 0.05 
Drift      -name    "LN0_DR_V3" -length 0.218700035812 
Drift      -name    "LN0_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN0_QD_F_V03"  -length 0.08  -strength [expr 0.08*$e0*4.6722875724] -e0 $e0
Drift      -name    "LN0_DR_QD_ED" -length 0.05 
Drift      -name    "LN0_DR_V4" -length 0.504441083662 
Drift      -name    "LN0_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN0_QD_D_V04"  -length 0.08  -strength [expr 0.08*$e0*-3.82020432982] -e0 $e0
Drift      -name    "LN0_DR_QD_ED" -length 0.05 
Drift      -name    "LN0_DR_V5" -length 2.08174601989 
Drift      -name    "LN0_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN0_QD_F0"  -length 0.08  -strength [expr 0.08*$e0*8.4] -e0 $e0
Drift      -name    "LN0_DR_QD_ED" -length 0.05 
Drift      -name    "LN0_DR_WP" -length 0.1 
Drift      -name    "LN0_DR_FL" -length 0.025 
Drift      -name    "LN0_DR_CCA_ED" -length 0.050198 
Cavity     -name    "LN0_CCA0" -length 1.899604  -gradient $LN0_CCA_GRD -phase $LN0_CCA_PHS 
set e0      [expr $e0 + 1.899604*$LN0_CCA_GRD*cos(3.14159265359*$LN0_CCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN0_DR_CCA_ED" -length 0.050198 
Drift      -name    "LN0_DR_FL" -length 0.025 
Drift      -name    "LN0_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN0_QD_D0"  -length 0.08  -strength [expr 0.08*$e0*-8.4] -e0 $e0
Drift      -name    "LN0_DR_QD_ED" -length 0.05 
Drift      -name    "LN0_DR_WP" -length 0.1 
Drift      -name    "LN0_DR_FL" -length 0.025 
Drift      -name    "LN0_DR_CCA_ED" -length 0.050198 
Cavity     -name    "LN0_CCA0" -length 1.899604  -gradient $LN0_CCA_GRD -phase $LN0_CCA_PHS 
set e0      [expr $e0 + 1.899604*$LN0_CCA_GRD*cos(3.14159265359*$LN0_CCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN0_DR_CCA_ED" -length 0.050198 
Drift      -name    "LN0_DR_FL" -length 0.025 
Drift      -name    "LN0_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN0_QD_F0"  -length 0.08  -strength [expr 0.08*$e0*8.4] -e0 $e0
Drift      -name    "LN0_DR_QD_ED" -length 0.05 
Drift      -name    "LN0_DR_WP" -length 0.1 
Drift      -name    "LN0_DR_FL" -length 0.025 
Drift      -name    "LN0_DR_CCA_ED" -length 0.050198 
Cavity     -name    "LN0_CCA0" -length 1.899604  -gradient $LN0_CCA_GRD -phase $LN0_CCA_PHS 
set e0      [expr $e0 + 1.899604*$LN0_CCA_GRD*cos(3.14159265359*$LN0_CCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN0_DR_CCA_ED" -length 0.050198 
Drift      -name    "LN0_DR_FL" -length 0.025 
Drift      -name    "LN0_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN0_QD_D0"  -length 0.08  -strength [expr 0.08*$e0*-8.4] -e0 $e0
Drift      -name    "LN0_DR_QD_ED" -length 0.05 
Drift      -name    "LN0_DR_WP" -length 0.1 
Drift      -name    "LN0_DR_LNZ_ED2" -length 0.6 
Drift      -name    "LN0_DR_FL" -length 0.025 
Drift      -name    "LN0_DR_KCA_ED" -length 0.02230105 
Cavity     -name    "LN0_KCA0" -length 0.7553979  -gradient $LN0_KCA_GRD -phase $LN0_KCA_PHS 
set e0      [expr $e0 + 0.7553979*$LN0_KCA_GRD*cos(3.14159265359*$LN0_KCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN0_DR_KCA_ED" -length 0.02230105 
Drift      -name    "LN0_DR_FL" -length 0.025 
Drift      -name    "LN0_DR_LNZ_ED2" -length 0.6 
Drift      -name    "LN0_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN0_QD_F0"  -length 0.08  -strength [expr 0.08*$e0*8.4] -e0 $e0
Drift      -name    "LN0_DR_QD_ED" -length 0.05 
Drift      -name    "LN0_DR_WP" -length 0.1 
Drift      -name    "LN0_DR_LNZ_ED2" -length 0.6 
Drift      -name    "LN0_DR_FL" -length 0.025 
Drift      -name    "LN0_DR_KCA_ED" -length 0.02230105 
Cavity     -name    "LN0_KCA0" -length 0.7553979  -gradient $LN0_KCA_GRD -phase $LN0_KCA_PHS 
set e0      [expr $e0 + 0.7553979*$LN0_KCA_GRD*cos(3.14159265359*$LN0_KCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN0_DR_KCA_ED" -length 0.02230105 
Drift      -name    "LN0_DR_FL" -length 0.025 
Drift      -name    "LN0_DR_LNZ_ED2" -length 0.6 
Drift      -name    "LN0_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN0_QD_D0"  -length 0.08  -strength [expr 0.08*$e0*-8.4] -e0 $e0
Drift      -name    "LN0_DR_QD_ED" -length 0.05 
Drift      -name    "BC1_D_V1" -length 3.17229112202 
Quadrupole -name    "BC1_Q_V1"  -length 0.08  -strength [expr 0.08*$e0*5.29281436004] -e0 $e0
Drift      -name    "BC1_D_V2" -length 1.43789671148 
Quadrupole -name    "BC1_Q_V2"  -length 0.08  -strength [expr 0.08*$e0*-9.26805152304] -e0 $e0
Drift      -name    "BC1_D_V3" -length 0.485186371723 
Quadrupole -name    "BC1_Q_V3"  -length 0.08  -strength [expr 0.08*$e0*13.2720486803] -e0 $e0
Drift      -name    "BC1_D_V4" -length 0.2000035292 
Quadrupole -name    "BC1_Q_V4"  -length 0.08  -strength [expr 0.08*$e0*-8.47727463588] -e0 $e0
Drift      -name    "BC1_D_V5" -length 0.20284930197 
Drift      -name    "BC1_BC_D_20" -length 0.5 
Sbend      -name    "BC1_BC_DIP1" -length 0.30009521  -angle 0.04363323  -E1 0 -E2 0.04363323 -hgap 0 -fint 0.5 -tilt 0 -e0 $e0 -six_dim 1 
Drift      -name    "BC1_BC_D_SIDE" -length 3.00285806 
Sbend      -name    "BC1_BC_DIP2" -length 0.30009521  -angle -0.04363323  -E1 -0.04363323 -E2 0 -hgap 0 -fint 0.5 -tilt 0 -e0 $e0 -six_dim 1 
Drift      -name    "BC1_BC_D_CENT" -length 1 
Sbend      -name    "BC1_BC_DIP3" -length 0.30009521  -angle -0.04363323  -E1 0 -E2 -0.04363323 -hgap 0 -fint 0.5 -tilt 0 -e0 $e0 -six_dim 1 
Drift      -name    "BC1_BC_D_SIDE" -length 3.00285806 
Sbend      -name    "BC1_BC_DIP4" -length 0.30009521  -angle 0.04363323  -E1 0.04363323 -E2 0 -hgap 0 -fint 0.5 -tilt 0 -e0 $e0 -six_dim 1 
Drift      -name    "BC1_BC_D_20" -length 0.5 
Drift      -name    "LN1_DR_V1" -length 0.324234168724 
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN1_QD_F_V01"  -length 0.08  -strength [expr 0.08*$e0*8.46660222115] -e0 $e0
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Drift      -name    "LN1_DR_V2" -length 1.07059051375 
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN1_QD_D_V02"  -length 0.08  -strength [expr 0.08*$e0*-10.4219944365] -e0 $e0
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Drift      -name    "LN1_DR_V3" -length 1.10255617571 
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN1_QD_F_V03"  -length 0.08  -strength [expr 0.08*$e0*10.3091860109] -e0 $e0
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Drift      -name    "LN1_DR_V4" -length 0.608998353803 
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN1_QD_D_V04"  -length 0.08  -strength [expr 0.08*$e0*-2.01201790485] -e0 $e0
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Drift      -name    "LN1_DR_V5" -length 1.68960197054 
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN1_QD_F0"  -length 0.08  -strength [expr 0.08*$e0*6.6] -e0 $e0
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Drift      -name    "LN1_DR_WP" -length 0.1 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_XCA_" -length 0.9164755 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_XCA_" -length 0.9164755 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN1_QD_D0"  -length 0.08  -strength [expr 0.08*$e0*-6.6] -e0 $e0
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Drift      -name    "LN1_DR_WP" -length 0.1 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_XCA_" -length 0.9164755 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_XCA_" -length 0.9164755 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN1_QD_F0"  -length 0.08  -strength [expr 0.08*$e0*6.6] -e0 $e0
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Drift      -name    "LN1_DR_WP" -length 0.1 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN1_QD_D0"  -length 0.08  -strength [expr 0.08*$e0*-6.6] -e0 $e0
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Drift      -name    "LN1_DR_WP" -length 0.1 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN1_QD_F0"  -length 0.08  -strength [expr 0.08*$e0*6.6] -e0 $e0
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Drift      -name    "LN1_DR_WP" -length 0.1 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN1_QD_D0"  -length 0.08  -strength [expr 0.08*$e0*-6.6] -e0 $e0
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Drift      -name    "LN1_DR_WP" -length 0.1 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN1_QD_F0"  -length 0.08  -strength [expr 0.08*$e0*6.6] -e0 $e0
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Drift      -name    "LN1_DR_WP" -length 0.1 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN1_QD_D0"  -length 0.08  -strength [expr 0.08*$e0*-6.6] -e0 $e0
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Drift      -name    "LN1_DR_WP" -length 0.1 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN1_QD_F0"  -length 0.08  -strength [expr 0.08*$e0*6.6] -e0 $e0
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Drift      -name    "BC2_D_V1" -length 0.96781492761 
Quadrupole -name    "BC2_Q_V1"  -length 0.08  -strength [expr 0.08*$e0*-0.084980639069] -e0 $e0
Drift      -name    "BC2_D_V2" -length 3.83280463233 
Quadrupole -name    "BC2_Q_V2"  -length 0.08  -strength [expr 0.08*$e0*-9.59827767409] -e0 $e0
Drift      -name    "BC2_D_V3" -length 0.428135381492 
Quadrupole -name    "BC2_Q_V3"  -length 0.08  -strength [expr 0.08*$e0*9.9148927828] -e0 $e0
Drift      -name    "BC2_D_V4" -length 0.100800035279 
Drift      -name    "BC2_BC_D_20" -length 0.5 
Sbend      -name    "BC2_BC_DIP1" -length 0.30006718  -angle 0.03665191  -E1 0 -E2 0.03665191 -hgap 0 -fint 0.5 -tilt 0 -e0 $e0 -six_dim 1 
Drift      -name    "BC2_BC_D_SIDE" -length 3.00201617 
Sbend      -name    "BC2_BC_DIP2" -length 0.30006718  -angle -0.03665191  -E1 -0.03665191 -E2 0 -hgap 0 -fint 0.5 -tilt 0 -e0 $e0 -six_dim 1 
Drift      -name    "BC2_BC_D_CENT" -length 1 
Sbend      -name    "BC2_BC_DIP3" -length 0.30006718  -angle -0.03665191  -E1 0 -E2 -0.03665191 -hgap 0 -fint 0.5 -tilt 0 -e0 $e0 -six_dim 1 
Drift      -name    "BC2_BC_D_SIDE" -length 3.00201617 
Sbend      -name    "BC2_BC_DIP4" -length 0.30006718  -angle 0.03665191  -E1 0.03665191 -E2 0 -hgap 0 -fint 0.5 -tilt 0 -e0 $e0 -six_dim 1 
Drift      -name    "BC2_BC_D_20" -length 0.5 
Drift      -name    "LN2_DR_DI_V1" -length 0.200000000039 
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN2_QD_F_DI_V01"  -length 0.08  -strength [expr 0.08*$e0*1.57156607187] -e0 $e0
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Drift      -name    "LN2_DR_DI_V2" -length 0.2 
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN2_QD_D_DI_V02"  -length 0.08  -strength [expr 0.08*$e0*4.60478075834] -e0 $e0
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Drift      -name    "LN2_DR_DI_V3" -length 0.302063992275 
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN2_QD_F_DI_V03"  -length 0.08  -strength [expr 0.08*$e0*-6.58481356314] -e0 $e0
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Drift      -name    "LN2_DR_DI_V4" -length 2.9999999998 
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN2_QD_D_DI_V04"  -length 0.08  -strength [expr 0.08*$e0*5.19014301974] -e0 $e0
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Drift      -name    "LN2_DR_DI_V5" -length 0.200000000117 
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN2_QD_D0_DI"  -length 0.08  -strength [expr 0.08*$e0*-7.8] -e0 $e0
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Drift      -name    "LN2_DR_DI_1" -length 0.3 
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN2_QD_F0_DI"  -length 0.08  -strength [expr 0.08*$e0*4] -e0 $e0
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Drift      -name    "LN2_DR_DI_2_0" -length 1 
Drift      -name    "LN2_DR_DI_2_0" -length 1 
Drift      -name    "LN2_DR_DI_2_0" -length 1 
Drift      -name    "LN2_DR_DI_2_0" -length 1 
Drift      -name    "LN2_DR_DI_2_0" -length 1 
Drift      -name    "LN2_DR_DI_2_0" -length 1 
Drift      -name    "LN2_DR_DI_2_0" -length 1 
Drift      -name    "LN2_DR_DI_2_0" -length 1 
Drift      -name    "LN2_DR_V1" -length 0.200004377725 
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN2_QD_F_V01"  -length 0.08  -strength [expr 0.08*$e0*8.49350732453] -e0 $e0
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Drift      -name    "LN2_DR_V2" -length 0.432036706941 
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN2_QD_D_V02"  -length 0.08  -strength [expr 0.08*$e0*-12.485520387] -e0 $e0
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Drift      -name    "LN2_DR_V3" -length 0.200000221272 
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN2_QD_F_V03"  -length 0.08  -strength [expr 0.08*$e0*6.05451613189] -e0 $e0
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Drift      -name    "LN2_DR_V4" -length 1.57255467532 
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN2_QD_D_V04"  -length 0.08  -strength [expr 0.08*$e0*-2.24106190165] -e0 $e0
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Drift      -name    "LN2_DR_V5" -length 2.7747946244 
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN2_QD_F0"  -length 0.08  -strength [expr 0.08*$e0*6.6] -e0 $e0
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Drift      -name    "LN2_DR_WP" -length 0.1 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_XCA_" -length 0.9164755 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_XCA_" -length 0.9164755 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN2_QD_D0"  -length 0.08  -strength [expr 0.08*$e0*-6.6] -e0 $e0
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Drift      -name    "LN2_DR_WP" -length 0.1 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_XCA_" -length 0.9164755 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_XCA_" -length 0.9164755 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN2_QD_F0"  -length 0.08  -strength [expr 0.08*$e0*6.6] -e0 $e0
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Drift      -name    "LN2_DR_WP" -length 0.1 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN2_QD_D0"  -length 0.08  -strength [expr 0.08*$e0*-6.6] -e0 $e0
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Drift      -name    "LN2_DR_WP" -length 0.1 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN2_QD_F0"  -length 0.08  -strength [expr 0.08*$e0*6.6] -e0 $e0
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Drift      -name    "LN2_DR_WP" -length 0.1 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN2_QD_D0"  -length 0.08  -strength [expr 0.08*$e0*-6.6] -e0 $e0
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Drift      -name    "LN2_DR_WP" -length 0.1 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "SBP_DR_QD_ED" -length 0.05 
Quadrupole -name    "SBP_QD_F0"  -length 0.08  -strength [expr 0.08*$e0*6.6] -e0 $e0
Drift      -name    "SBP_DR_QD_ED" -length 0.05 
Drift      -name    "SBP_DR_WP" -length 0.1 
Drift      -name    "SBP_DR_EDGE" -length 0.055 
Drift      -name    "SBP_DR_FL" -length 0.025 
Drift      -name    "SBP_DR_SCA_ED" -length 0.0997225 
Drift     -name    "SBP_SCA0" -length 1.800555 
Drift      -name    "SBP_DR_SCA_ED" -length 0.0997225 
Drift      -name    "SBP_DR_FL" -length 0.025 
Drift      -name    "SBP_DR_EDGE" -length 0.055 
Drift      -name    "SBP_DR_QD_ED" -length 0.05 
Quadrupole -name    "SBP_QD_D0"  -length 0.08  -strength [expr 0.08*$e0*-6.6] -e0 $e0
Drift      -name    "SBP_DR_QD_ED" -length 0.05 
Drift      -name    "SBP_DR_D0" -length 0.4 
Quadrupole -name    "SBP_QD_F1"  -length 0.08  -strength [expr 0.08*$e0*6.1] -e0 $e0
Drift      -name    "SBP_DR_D1" -length 0.48 
Quadrupole -name    "SBP_QD_F2"  -length 0.08  -strength [expr 0.08*$e0*-9.5] -e0 $e0
Drift      -name    "SBP_DR_D2" -length 0.4 
Quadrupole -name    "SBP_QD_F3"  -length 0.08  -strength [expr 0.08*$e0*8.21] -e0 $e0
Drift      -name    "SBP_DR_D3" -length 0.45 
Sbend      -name    "SEPTUMONOF" -length 1.50007616  -angle 0.03490659  -E1 0.017453295 -E2 0.017453295 -hgap 0 -fint 0.5 -tilt 0 -e0 $e0 -six_dim 1 
Drift      -name    "SBP_DR_D4" -length 1 
Drift      -name    "SBP_DR_D4" -length 1 
Drift      -name    "SBP_DR_D4" -length 1 
Drift      -name    "SBP_DR_D4" -length 1 
Drift      -name    "SBP_DR_D4" -length 1 
Drift      -name    "SBP_DR_V1" -length 0.243264138581 
Drift      -name    "SBP_DR_QD_ED" -length 0.05 
Quadrupole -name    "SBP_QD_V01"  -length 0.08  -strength [expr 0.08*$e0*-4.93600572828] -e0 $e0
Drift      -name    "SBP_DR_QD_ED" -length 0.05 
Drift      -name    "SBP_DR_V2" -length 0.417340425121 
Drift      -name    "SBP_DR_QD_ED" -length 0.05 
Quadrupole -name    "SBP_QD_V02"  -length 0.08  -strength [expr 0.08*$e0*11.0690333128] -e0 $e0
Drift      -name    "SBP_DR_QD_ED" -length 0.05 
Drift      -name    "SBP_DR_V3" -length 0.649880781443 
Drift      -name    "SBP_DR_QD_ED" -length 0.05 
Quadrupole -name    "SBP_QD_V03"  -length 0.08  -strength [expr 0.08*$e0*-6.01922358514] -e0 $e0
Drift      -name    "SBP_DR_QD_ED" -length 0.05 
Drift      -name    "SBP_DR_V4" -length 0.99995079232 
Sbend      -name    "SBP_DP1" -length 0.50000635  -angle 0.01745329  -E1 0.008726645 -E2 0.008726645 -hgap 0 -fint 0.5 -tilt 0 -e0 $e0 -six_dim 1 
Drift      -name    "SBP_DR_V5" -length 0.250000001606 
Drift      -name    "SBP_DR_QD_ED" -length 0.05 
Quadrupole -name    "SBP_QD_V05"  -length 0.08  -strength [expr 0.08*$e0*0.969034480652] -e0 $e0
Drift      -name    "SBP_DR_QD_ED" -length 0.05 
Drift      -name    "SBP_DR_V6" -length 1.09547797488 
Drift      -name    "SBP_DR_QD_ED" -length 0.05 
Quadrupole -name    "SBP_QD_V06"  -length 0.08  -strength [expr 0.08*$e0*-4.16426324165] -e0 $e0
Drift      -name    "SBP_DR_QD_ED" -length 0.05 
Drift      -name    "SBP_DR_V7" -length 1.1981196528 
Drift      -name    "SBP_DR_QD_ED" -length 0.05 
Quadrupole -name    "SBP_QD_V07"  -length 0.08  -strength [expr 0.08*$e0*4.19293212381] -e0 $e0
Drift      -name    "SBP_DR_QD_ED" -length 0.05 
Drift      -name    "SBP_DR_V8" -length 0.259323119173 
Drift      -name    "SBP_DR_QD_ED" -length 0.05 
Quadrupole -name    "SBP_QD_V08"  -length 0.08  -strength [expr 0.08*$e0*2.8507316376] -e0 $e0
Drift      -name    "SBP_DR_QD_ED" -length 0.05 
Drift      -name    "SBP_DR_V9" -length 0.722310745711 
Sbend      -name    "SBP_DP2" -length 0.50002539  -angle 0.03490659  -E1 0.017453295 -E2 0.017453295 -hgap 0 -fint 0.5 -tilt 0 -e0 $e0 -six_dim 1 
Drift      -name    "SBP_DR_V10" -length 2.06046203218 
Drift      -name    "SBP_DR_V11" -length 0.202649600903 
Quadrupole -name    "SBP_QD_V11"  -length 0.08  -strength [expr 0.08*$e0*6.72307404593] -e0 $e0
Drift      -name    "SBP_DR_V12" -length 0.908818922014 
Quadrupole -name    "SBP_QD_V12"  -length 0.08  -strength [expr 0.08*$e0*-7.76976851147] -e0 $e0
Drift      -name    "SBP_DR_V13" -length 2.10071192954 
Quadrupole -name    "SBP_QD_V13"  -length 0.08  -strength [expr 0.08*$e0*1.18121884393] -e0 $e0
Drift      -name    "SBP_DR_V14" -length 1.62020698236 
Quadrupole -name    "SBP_QD_F0_F"  -length 0.08  -strength [expr 0.08*$e0*5] -e0 $e0
Drift      -name    "SBP_DR_F0DO" -length 2.5 
Quadrupole -name    "SBP_QD_D0_F"  -length 0.08  -strength [expr 0.08*$e0*-5] -e0 $e0
Drift      -name    "SBP_DR_F0DO" -length 2.5 
Quadrupole -name    "SBP_QD_F0_F"  -length 0.08  -strength [expr 0.08*$e0*5] -e0 $e0
Drift      -name    "SBP_DR_V15" -length 0.904008915875 
Quadrupole -name    "SBP_QD_V15"  -length 0.08  -strength [expr 0.08*$e0*4.49944636738] -e0 $e0
Drift      -name    "SBP_DR_V16" -length 0.341599678912 
Quadrupole -name    "SBP_QD_V16"  -length 0.08  -strength [expr 0.08*$e0*1.96996646171] -e0 $e0
Drift      -name    "SBP_DR_V17" -length 0.200000046614 
Quadrupole -name    "SBP_QD_V17"  -length 0.08  -strength [expr 0.08*$e0*-7.98311702151] -e0 $e0
Drift      -name    "SBP_DR_V18" -length 0.241859387859 
Quadrupole -name    "SBP_QD_V18"  -length 0.08  -strength [expr 0.08*$e0*-1.73487313215] -e0 $e0
Drift      -name    "SBP_DR_V18A" -length 0.200000000339 
Sbend      -name    "SBP_DP3" -length 0.50003967  -angle -0.04363323  -E1 -0.021816615 -E2 -0.021816615 -hgap 0 -fint 0.5 -tilt 0 -e0 $e0 -six_dim 1 
Drift      -name    "SBP_DR_V19" -length 1.5 
Quadrupole -name    "SBP_QD_V19"  -length 0.08  -strength [expr 0.08*$e0*12.45] -e0 $e0
Drift      -name    "SBP_DR_V20" -length 0.5 
Quadrupole -name    "SBP_QD_V20"  -length 0.08  -strength [expr 0.08*$e0*-7] -e0 $e0
Drift      -name    "SBP_DR_V21" -length 0.5 
Drift      -name    "SBP_DR_V21" -length 0.5 
Quadrupole -name    "SBP_QD_V20"  -length 0.08  -strength [expr 0.08*$e0*-7] -e0 $e0
Drift      -name    "SBP_DR_V20" -length 0.5 
Quadrupole -name    "SBP_QD_V19"  -length 0.08  -strength [expr 0.08*$e0*12.45] -e0 $e0
Drift      -name    "SBP_DR_V19" -length 1.5 
Sbend      -name    "SBP_DP3" -length 0.50003967  -angle -0.04363323  -E1 -0.021816615 -E2 -0.021816615 -hgap 0 -fint 0.5 -tilt 0 -e0 $e0 -six_dim 1 
Drift      -name    "LN4_DR_V1" -length 0.200000000001 
Drift      -name    "LN4_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN4_QD_F_V01"  -length 0.08  -strength [expr 0.08*$e0*5.08753271047] -e0 $e0
Drift      -name    "LN4_DR_QD_ED" -length 0.05 
Drift      -name    "LN4_DR_V2" -length 0.200000242837 
Drift      -name    "LN4_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN4_QD_D_V02"  -length 0.08  -strength [expr 0.08*$e0*-13.1999987762] -e0 $e0
Drift      -name    "LN4_DR_QD_ED" -length 0.05 
Drift      -name    "LN4_DR_V3" -length 0.612042563058 
Drift      -name    "LN4_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN4_QD_F_V03"  -length 0.08  -strength [expr 0.08*$e0*8.27052799743] -e0 $e0
Drift      -name    "LN4_DR_QD_ED" -length 0.05 
Drift      -name    "LN4_DR_V4" -length 1.78701294271 
Drift      -name    "LN4_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN4_QD_D_V04"  -length 0.08  -strength [expr 0.08*$e0*-2.88986954083] -e0 $e0
Drift      -name    "LN4_DR_QD_ED" -length 0.05 
Drift      -name    "LN4_DR_V5" -length 0.2003222844 
Drift      -name    "LN4_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN4_QD_F0"  -length 0.08  -strength [expr 0.08*$e0*6.6] -e0 $e0
Drift      -name    "LN4_DR_QD_ED" -length 0.05 
Drift      -name    "LN4_DR_WP" -length 0.1 
Drift      -name    "LN4_DR_FL" -length 0.025 
Drift      -name    "LN4_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN4_DR_XCA_" -length 0.9164755 
Drift      -name    "LN4_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN4_DR_FL" -length 0.025 
Drift      -name    "LN4_DR_FL" -length 0.025 
Drift      -name    "LN4_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN4_DR_XCA_" -length 0.9164755 
Drift      -name    "LN4_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN4_DR_FL" -length 0.025 
Drift      -name    "LN4_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN4_QD_D0"  -length 0.08  -strength [expr 0.08*$e0*-6.6] -e0 $e0
Drift      -name    "LN4_DR_QD_ED" -length 0.05 
Drift      -name    "LN4_DR_WP" -length 0.1 
Drift      -name    "LN4_DR_FL" -length 0.025 
Drift      -name    "LN4_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN4_DR_XCA_" -length 0.9164755 
Drift      -name    "LN4_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN4_DR_FL" -length 0.025 
Drift      -name    "LN4_DR_FL" -length 0.025 
Drift      -name    "LN4_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN4_DR_XCA_" -length 0.9164755 
Drift      -name    "LN4_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN4_DR_FL" -length 0.025 
Drift      -name    "LN4_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN4_QD_F0"  -length 0.08  -strength [expr 0.08*$e0*6.6] -e0 $e0
Drift      -name    "LN4_DR_QD_ED" -length 0.05 
Drift      -name    "LN4_DR_WP" -length 0.1 
Drift      -name    "LN4_DR_FL" -length 0.025 
Drift      -name    "LN4_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN4_DR_XCA_" -length 0.9164755 
Drift      -name    "LN4_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN4_DR_FL" -length 0.025 
Drift      -name    "LN4_DR_FL" -length 0.025 
Drift      -name    "LN4_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN4_DR_XCA_" -length 0.9164755 
Drift      -name    "LN4_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN4_DR_FL" -length 0.025 
Drift      -name    "LN4_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN4_QD_D0"  -length 0.08  -strength [expr 0.08*$e0*-6.6] -e0 $e0
Drift      -name    "LN4_DR_QD_ED" -length 0.05 
Drift      -name    "LN4_DR_WP" -length 0.1 
Drift      -name    "LN4_DR_FL" -length 0.025 
Drift      -name    "LN4_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN4_DR_XCA_" -length 0.9164755 
Drift      -name    "LN4_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN4_DR_FL" -length 0.025 
Drift      -name    "LN4_DR_FL" -length 0.025 
Drift      -name    "LN4_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN4_DR_XCA_" -length 0.9164755 
Drift      -name    "LN4_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN4_DR_FL" -length 0.025 
Drift      -name    "LN4_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN4_QD_F0"  -length 0.08  -strength [expr 0.08*$e0*6.6] -e0 $e0
Drift      -name    "LN4_DR_QD_ED" -length 0.05 
Drift      -name    "LN4_DR_WP" -length 0.1 
Drift      -name    "LN4_DR_FL" -length 0.025 
Drift      -name    "LN4_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN4_XCA0" -length 0.9164755  -gradient $LN4_XCA_GRD -phase $LN4_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN4_XCA_GRD*cos(3.14159265359*$LN4_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN4_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN4_DR_FL" -length 0.025 
Drift      -name    "LN4_DR_FL" -length 0.025 
Drift      -name    "LN4_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN4_XCA0" -length 0.9164755  -gradient $LN4_XCA_GRD -phase $LN4_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN4_XCA_GRD*cos(3.14159265359*$LN4_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN4_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN4_DR_FL" -length 0.025 
Drift      -name    "LN4_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN4_QD_D0"  -length 0.08  -strength [expr 0.08*$e0*-6.6] -e0 $e0
Drift      -name    "LN4_DR_QD_ED" -length 0.05 
Drift      -name    "LN4_DR_WP" -length 0.1 
Drift      -name    "LN4_DR_FL" -length 0.025 
Drift      -name    "LN4_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN4_XCA0" -length 0.9164755  -gradient $LN4_XCA_GRD -phase $LN4_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN4_XCA_GRD*cos(3.14159265359*$LN4_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN4_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN4_DR_FL" -length 0.025 
Drift      -name    "LN4_DR_FL" -length 0.025 
Drift      -name    "LN4_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN4_XCA0" -length 0.9164755  -gradient $LN4_XCA_GRD -phase $LN4_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN4_XCA_GRD*cos(3.14159265359*$LN4_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN4_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN4_DR_FL" -length 0.025 
Drift      -name    "LN4_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN4_QD_F0"  -length 0.08  -strength [expr 0.08*$e0*6.6] -e0 $e0
Drift      -name    "LN4_DR_QD_ED" -length 0.05 
Drift      -name    "LN4_DR_WP" -length 0.1 
Drift      -name    "LN4_DR_FL" -length 0.025 
Drift      -name    "LN4_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN4_XCA0" -length 0.9164755  -gradient $LN4_XCA_GRD -phase $LN4_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN4_XCA_GRD*cos(3.14159265359*$LN4_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN4_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN4_DR_FL" -length 0.025 
Drift      -name    "LN4_DR_FL" -length 0.025 
Drift      -name    "LN4_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN4_XCA0" -length 0.9164755  -gradient $LN4_XCA_GRD -phase $LN4_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN4_XCA_GRD*cos(3.14159265359*$LN4_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN4_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN4_DR_FL" -length 0.025 
Drift      -name    "LN4_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN4_QD_D0"  -length 0.08  -strength [expr 0.08*$e0*-6.6] -e0 $e0
Drift      -name    "LN4_DR_QD_ED" -length 0.05 
Drift      -name    "LN4_DR_WP" -length 0.1 
Drift      -name    "LN4_DR_FL" -length 0.025 
Drift      -name    "LN4_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN4_XCA0" -length 0.9164755  -gradient $LN4_XCA_GRD -phase $LN4_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN4_XCA_GRD*cos(3.14159265359*$LN4_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN4_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN4_DR_FL" -length 0.025 
Drift      -name    "LN4_DR_FL" -length 0.025 
Drift      -name    "LN4_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN4_XCA0" -length 0.9164755  -gradient $LN4_XCA_GRD -phase $LN4_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN4_XCA_GRD*cos(3.14159265359*$LN4_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN4_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN4_DR_FL" -length 0.025 
Drift      -name    "LN4_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN4_QD_F0"  -length 0.08  -strength [expr 0.08*$e0*6.6] -e0 $e0
Drift      -name    "LN4_DR_QD_ED" -length 0.05 
Drift      -name    "LN4_DR_WP" -length 0.1 
Drift      -name    "LN4_DR_FL" -length 0.025 
Drift      -name    "LN4_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN4_DR_XCA_" -length 0.9164755 
Drift      -name    "LN4_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN4_DR_FL" -length 0.025 
Drift      -name    "LN4_DR_FL" -length 0.025 
Drift      -name    "LN4_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN4_DR_XCA_" -length 0.9164755 
Drift      -name    "LN4_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN4_DR_FL" -length 0.025 
Drift      -name    "LN4_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN4_QD_D0"  -length 0.08  -strength [expr 0.08*$e0*-6.6] -e0 $e0
Drift      -name    "LN4_DR_QD_ED" -length 0.05 
Drift      -name    "LN4_DR_WP" -length 0.1 
Drift      -name    "LN4_DR_FL" -length 0.025 
Drift      -name    "LN4_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN4_DR_XCA_" -length 0.9164755 
Drift      -name    "LN4_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN4_DR_FL" -length 0.025 
Drift      -name    "LN4_DR_FL" -length 0.025 
Drift      -name    "LN4_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN4_DR_XCA_" -length 0.9164755 
Drift      -name    "LN4_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN4_DR_FL" -length 0.025 
Drift      -name    "LN4_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN4_QD_F0"  -length 0.08  -strength [expr 0.08*$e0*6.6] -e0 $e0
Drift      -name    "LN4_DR_QD_ED" -length 0.05 
