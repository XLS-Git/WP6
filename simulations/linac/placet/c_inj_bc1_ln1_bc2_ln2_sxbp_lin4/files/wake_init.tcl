

SplineCreate   "xband_Wt" -file $xband_str(wake_tran_file)
SplineCreate   "xband_Wl" -file $xband_str(wake_long_file)
ShortRangeWake "Xband_SR_W" -type 2 -wx "xband_Wt" -wy "xband_Wt" -wz "xband_Wl"

SplineCreate   "kband_Wt" -file $kband_str(wake_tran_file)
SplineCreate   "kband_Wl" -file $kband_str(wake_long_file)
ShortRangeWake "Kband_SR_W" -type 2 -wx "kband_Wt" -wy "kband_Wt" -wz "kband_Wl"

SplineCreate   "cband_Wt" -file $cband_str(wake_tran_file)
SplineCreate   "cband_Wl" -file $cband_str(wake_long_file)
ShortRangeWake "Cband_SR_W" -type 2 -wx "cband_Wt" -wy "cband_Wt" -wz "cband_Wl"

SplineCreate   "sband_Wt" -file $sband_str(wake_tran_file)
SplineCreate   "sband_Wl" -file $sband_str(wake_long_file)
ShortRangeWake "Sband_SR_W" -type 2 -wx "sband_Wt" -wy "sband_Wt" -wz "sband_Wl"

# 
# 
# SplineCreate   "acc_cav_Wt_z" -file "acc_cav_Wt_zero.dat"
# SplineCreate   "acc_cav_Wl_z" -file "acc_cav_Wl_zero.dat"
# ShortRangeWake "acc_cav_SR_W_zero" -type 2 -wx "acc_cav_Wt_z" -wy "acc_cav_Wt_z" -wz "acc_cav_Wl_z"


#
# use this list to create long range fields 
# single bunch no effect
 WakeSet Wake_acc_cav_long_range {1. 1 0 0 }
#
# define structure
InjectorCavityDefine -name 0 -wakelong Wake_acc_cav_long_range 
