

source load_files.tcl

set beam0_indist cband_out_part.dat
set beamlinename xls_linac_sxr
set latfile xls_lattice_sxr.tcl


array set BeamDefine  [bunch_parameters $script_dir/$beam0_indist]
array set BeamDefine "name beam0 filename $beam0_indist n_slice 201 charge 0.46875e9"
set BeamDefine(n_macro) [expr int($BeamDefine(npart)/$BeamDefine(n_slice))]
lassign "$BeamDefine(beta_x) $BeamDefine(beta_y) $BeamDefine(alpha_x) $BeamDefine(alpha_y)" btx bty alx aly

set e0 $BeamDefine(energy)

puts [array get BeamDefine]



set LN0_CCA_GRD 40e-3
set LN0_CCA_PHS 20.0

set LN0_KCA_GRD 12e-3
set LN0_KCA_PHS [expr 180+$LN0_CCA_PHS-2.5]

set LN1_XCA_GRD 65e-3
set LN1_XCA_PHS 16.0

set LN2_XCA_GRD 65e-3
set LN2_XCA_PHS -20.0  


set LN4_XCA_GRD 65e-3
set LN4_XCA_PHS -10.0  

set b0_in beam0.in
set b0_ou beam0.ou

BeamlineNew
Girder
source $script_dir/$latfile

BeamlineSet -name $beamlinename
BeamlineUse -name $beamlinename

make_particle_beam_read BeamDefine $script_dir/$BeamDefine(filename)

Octave {

    global COLS_NAME % preceeded by "NAME" and "KEYWORD" and followed by "Type"
    COLS_NAME = { "S" ; "L" ; "E0" ; "K1L" ; "K1SL" ; "K2L" ; "K2SL" ; "KS" ; "ANGLE" ; "E1" ; "E2" ; "Strength" };
    ALL = placet_element_get_attributes("$beamlinename");
    TABLE_NAME = [];
    TABLE_KEY  = [];
    TABLE_DATA = [];
    TABLE_TYPE = [];
    function T = append_to_row(T, name, value)
        global COLS_NAME
        T(find(strcmp(COLS_NAME, name))) = value;
    end

    %% element by element
    sigma_quad = 10; % um, rms misalignment of quadrupoles
    last_quad_K1L = 0.0; % 1/m
    
    index = 1;    
    for i=1:size(ALL,1);
        THIS = ALL{i};
        T = zeros(1, size(COLS_NAME,1));
        T = append_to_row(T, "S", THIS.s);
        T = append_to_row(T, "L", THIS.length);
        T = append_to_row(T, "E0", THIS.e0);
        switch THIS.type_name
         case "quadrupole"
            strength = abs(THIS.strength / 0.299792458);
            T = append_to_row(T, "K1L", THIS.strength / THIS.e0);
            T = append_to_row(T, "Strength", strength); % GV/c/m = (1 / 0.299792458) T
            last_quad_K1L = THIS.strength / THIS.e0; % 1/m
         case "sbend"
            T = append_to_row(T, "ANGLE", THIS.angle);
            T = append_to_row(T, "E1", THIS.E1);
            T = append_to_row(T, "E2", THIS.E2);
            T = append_to_row(T, "Strength", THIS.e0 * THIS.angle / 0.299792458); % GV/c = (1 / 0.299792458) T*m
         case "multipole"
            THIS.type_name = "sextupole";
            strength = abs(THIS.strength / 0.299792458);
            T = append_to_row(T, "K2L", THIS.strength / THIS.e0);
            T = append_to_row(T, "Strength", strength); % GV/c/m^2 = (1 / 0.299792458) T/m
          case "bpm"
          case "dipole"
            THIS.type_name = "kicker";
            max_strength = 3*sigma_quad*abs(last_quad_K1L)*THIS.e0*1e3; % V, 3 * sigma_quad * last_quad_K1L * THIS.e0
            T = append_to_row(T, "Strength", max_strength / 299792458); % T*m
          case "cavity"
            T = append_to_row(T, "Strength", 1e3 * THIS.gradient * THIS.length); % MV
            T = append_to_row(T, "ANGLE", THIS.phase / 180.0 * pi); % rad
          case "solenoid"
            T = append_to_row(T, "KS", 0.29979246 * THIS.bz); % solenoid strength from T/(GeV/c) to 1/m
            T = append_to_row(T, "Strength", 2.9); % T
          case "drift"
            if strfind(THIS.name, "MARKER")
              THIS.type_name = "marker";
            endif
        end
        
        %% for all types
        switch THIS.type_name
          case { "quadrupole" , "sbend", "sextupole", "solenoid", "bpm" , "kicker" , "cavity", "marker" }
            TABLE_DATA(index,:) = T;
            TABLE_NAME{index} = THIS.name;
            TABLE_KEY {index} = toupper(THIS.type_name);
            TABLE_TYPE{index} = THIS.comment;
            index++;
        end
    end
    
    %% save on disk
    fid = fopen("elements_table.txt", 'w');
    if fid != -1
        fprintf(fid, '* NAME\tKEYWORD\t%s\n', strjoin (COLS_NAME, '\t'));
        fprintf(fid, '* <STR> <STR> [m] [m] [GeV] [m^-1] [m^-1] [m^-2] [m^-2] [m^-1] [rad] [rad] [rad] <STR>\n');
        for i=1:size(TABLE_DATA,1)
            fprintf(fid, [ '\"%s\"\t\"%s\"\t%.3f\t' repmat('%g\t', 1, size(TABLE_DATA,2)-1) '\n' ], TABLE_NAME{i}, TABLE_KEY{i}, TABLE_DATA(i,:));
        end
        fclose(fid);
    end
}

Octave {
    global beaminxdx = 1;
    global Beams = {};
}

proc octave_save {} {
    Octave {
      Beams{beaminxdx++} = placet_get_beam();
    }
} 


Octave {
# apply wakes for all structures
	ALLCAVITs = placet_get_number_list("$beamlinename", "cavity");
	placet_element_set_attribute("$beamlinename", ALLCAVITs, "six_dim", true);
    
    CCAVs = placet_get_name_number_list("$beamlinename", "*_CCA0");
    placet_element_set_attribute("$beamlinename", CCAVs, "short_range_wake",  "Cband_SR_W");
    placet_element_set_attribute("$beamlinename", CCAVs, "lambda", $cband_str(lambda));
    
    XCAVs = placet_get_name_number_list("$beamlinename", "*_XCA0");
    placet_element_set_attribute("$beamlinename", XCAVs, "short_range_wake", "Xband_SR_W");
    placet_element_set_attribute("$beamlinename", XCAVs, "lambda", $xband_str(lambda));
    
    KCAVs = placet_get_name_number_list("$beamlinename", "*_KCA0");
    placet_element_set_attribute("$beamlinename", KCAVs, "short_range_wake", "Kband_SR_W");
    placet_element_set_attribute("$beamlinename", KCAVs, "lambda", $kband_str(lambda));    
    
#  6d tracing in bunch compression
   SIs = placet_get_number_list("$beamlinename", "sbend");
   placet_element_set_attribute("$beamlinename", SIs, "six_dim", true);
   
#  set reference energy for first BC
   BC1Ds = placet_get_name_number_list("$beamlinename", "BC1_BC_DIP*");
   placet_element_set_attribute("$beamlinename", BC1Ds, "e0", 0.306547-5e-4);
   
#  set reference energy for second BC
   BC2Ds = placet_get_name_number_list("$beamlinename", "BC2_BC_DIP*");
   placet_element_set_attribute("$beamlinename", BC2Ds, "e0", 0.992685);
   

}


Octave {
    WLN0=  placet_get_name_number_list("$beamlinename", "LN0_*");
    WBC1=  placet_get_name_number_list("$beamlinename", "BC1_*");
    WLN1=  placet_get_name_number_list("$beamlinename", "LN1_*");
    WBC2=  placet_get_name_number_list("$beamlinename", "BC2_*");
    WLN2=  placet_get_name_number_list("$beamlinename", "LN2_*");
    WSBP=  placet_get_name_number_list("$beamlinename", "SBP_*");
    WLN4=  placet_get_name_number_list("$beamlinename", "LN4_*");
    watches=[WLN0(1) WLN0(end)];
    watches=[watches WBC1(end) WLN1(end) WBC2(end) WLN4(1) WLN4(end)];
    placet_element_set_attribute("$beamlinename", watches, "tclcall_exit", "octave_save");
  
}


Octave {
    
   %%  twiss function along beamline
	[s, beta_x, beta_y, alpha_x, alpha_y, mu_x, mu_y] = placet_evolve_beta_function("$beamlinename", $btx, $alx, $bty, $aly);
	beta_arr= [s  beta_x  beta_y  alpha_x alpha_y mu_x mu_y];
	save -text -ascii $beamlinename.twi beta_arr;
    
#     #   tracking default beam
# 	[emitt0,B01] = placet_test_no_correction("$beamlinename", "beam0", "None", "%s %E %dE %ex %ey %sz");
# 	save -text -ascii $b0_ou B01;
}

TestNoCorrection -beam $BeamDefine(name) -survey None -emitt_file $beamlinename.emt


Octave {
    [~,nb]=size(Beams);
    Tcl_SetVar("nbnch", nb);
   fnames={'inj_ou.dat', 'ln0_ou.dat', 'bc1_ou.dat', 'ln1_ou.dat', 'bc2_ou.dat', 'sbp_ou.dat', 'ln4_ou.dat'};
    pltnams={};
    
    for i=1:nb
        B=Beams{i};
        fnm=fnames{i};
        pltnams{i}=fnm;
        emean=mean(B(:,1));
        smean=mean(B(:,4));
        B(:,4)=B(:,4)-smean;
        sigz=std(B(:,4));
        printf("at position %s Emean=%f GeV, Sig_z=%f um, S_off=%f um\n",fnm,emean,sigz,smean) 
        save("-text", "-ascii",fnm,"B");
             
    endfor
    
    pltnams = strcat(pltnams,{' '});
    pltnams=cell2mat(pltnams);
    
    Tcl_SetVar("pltnams", pltnams);
    
}

set fl [lindex $pltnams end]
foreach fl $pltnams {
   set plot_phs  "'$fl' u 4:1 w p ti '$fl' "
   exec echo " set term x11 \n set key left \n set xlabel 'z (um)' \n set ylabel 'E (GeV)' \n  \
               pl $plot_phs " | gnuplot -persist 
}
set plot_twi  "'$beamlinename.twi' u 1:2 w l ti ' btx', '$beamlinename.twi' u 1:3 w l  ti 'bty'"

exec echo " set term x11 size 1200,600 \n \
            set xlabel 'distance (m)' \n set ylabel 'beta x,y (m)' \n set key  spacing 0.9 \n set xrange \[0:*\] \n \
            plot $plot_twi  " | gnuplot -persist

# exec echo " set term x11 size 800,600 \n set xlabel 'z (um)' \n set ylabel 'E (GeV)' \n  \
#             pl 	$plot_phs " | gnuplot -persist  

exit
