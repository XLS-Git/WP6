Girder
SetReferenceEnergy  $e0
Drift      -name    "LN0_DR_V0" -length 0.0 
Drift      -name    "LN0_DR_V1" -length 0.614686613018 
Drift      -name    "LN0_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN0_QD_F_V01"  -length 0.08  -strength [expr 0.08*$e0*10.3784573339] -e0 $e0
Bpm
Drift      -name    "LN0_DR_QD_ED" -length 0.05 
Drift      -name    "LN0_DR_V2" -length 0.4501427474 
Drift      -name    "LN0_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN0_QD_D_V02"  -length 0.08  -strength [expr 0.08*$e0*-12.1698626687] -e0 $e0
Bpm
Drift      -name    "LN0_DR_QD_ED" -length 0.05 
Drift      -name    "LN0_DR_V3" -length 0.218700035812 
Drift      -name    "LN0_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN0_QD_F_V03"  -length 0.08  -strength [expr 0.08*$e0*4.6722875724] -e0 $e0
Bpm
Drift      -name    "LN0_DR_QD_ED" -length 0.05 
Drift      -name    "LN0_DR_V4" -length 0.504441083662 
Drift      -name    "LN0_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN0_QD_D_V04"  -length 0.08  -strength [expr 0.08*$e0*-3.82020432982] -e0 $e0
Bpm
Drift      -name    "LN0_DR_QD_ED" -length 0.05 
Drift      -name    "LN0_DR_V5" -length 2.08174601989 
Drift      -name    "LN0_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN0_QD_F0"  -length 0.08  -strength [expr 0.08*$e0*8.4] -e0 $e0
Bpm
Drift      -name    "LN0_DR_QD_ED" -length 0.05 
Drift      -name    "LN0_DR_WP" -length 0.1 
Drift      -name    "LN0_DR_FL" -length 0.025 
Drift      -name    "LN0_DR_CCA_ED" -length 0.050198 
Cavity     -name    "LN0_CCA0" -length 1.899604  -gradient $LN0_CCA_GRD -phase $LN0_CCA_PHS 
set e0      [expr $e0 + 1.899604*$LN0_CCA_GRD*cos(3.14159265359*$LN0_CCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN0_DR_CCA_ED" -length 0.050198 
Drift      -name    "LN0_DR_FL" -length 0.025 
Drift      -name    "LN0_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN0_QD_D0"  -length 0.08  -strength [expr 0.08*$e0*-8.4] -e0 $e0
Bpm
Drift      -name    "LN0_DR_QD_ED" -length 0.05 
Drift      -name    "LN0_DR_WP" -length 0.1 
Drift      -name    "LN0_DR_FL" -length 0.025 
Drift      -name    "LN0_DR_CCA_ED" -length 0.050198 
Cavity     -name    "LN0_CCA0" -length 1.899604  -gradient $LN0_CCA_GRD -phase $LN0_CCA_PHS 
set e0      [expr $e0 + 1.899604*$LN0_CCA_GRD*cos(3.14159265359*$LN0_CCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN0_DR_CCA_ED" -length 0.050198 
Drift      -name    "LN0_DR_FL" -length 0.025 
Drift      -name    "LN0_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN0_QD_F0"  -length 0.08  -strength [expr 0.08*$e0*8.4] -e0 $e0
Bpm
Drift      -name    "LN0_DR_QD_ED" -length 0.05 
Drift      -name    "LN0_DR_WP" -length 0.1 
Drift      -name    "LN0_DR_FL" -length 0.025 
Drift      -name    "LN0_DR_CCA_ED" -length 0.050198 
Cavity     -name    "LN0_CCA0" -length 1.899604  -gradient $LN0_CCA_GRD -phase $LN0_CCA_PHS 
set e0      [expr $e0 + 1.899604*$LN0_CCA_GRD*cos(3.14159265359*$LN0_CCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN0_DR_CCA_ED" -length 0.050198 
Drift      -name    "LN0_DR_FL" -length 0.025 
Drift      -name    "LN0_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN0_QD_D0"  -length 0.08  -strength [expr 0.08*$e0*-8.4] -e0 $e0
Bpm
Drift      -name    "LN0_DR_QD_ED" -length 0.05 
Drift      -name    "LN0_DR_WP" -length 0.1 
Drift      -name    "LN0_DR_LNZ_ED2" -length 0.6 
Drift      -name    "LN0_DR_FL" -length 0.025 
Drift      -name    "LN0_DR_KCA_ED" -length 0.02230105 
Cavity     -name    "LN0_KCA0" -length 0.7553979  -gradient $LN0_KCA_GRD -phase $LN0_KCA_PHS 
set e0      [expr $e0 + 0.7553979*$LN0_KCA_GRD*cos(3.14159265359*$LN0_KCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN0_DR_KCA_ED" -length 0.02230105 
Drift      -name    "LN0_DR_FL" -length 0.025 
Drift      -name    "LN0_DR_LNZ_ED2" -length 0.6 
Drift      -name    "LN0_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN0_QD_F0"  -length 0.08  -strength [expr 0.08*$e0*8.4] -e0 $e0
Bpm
Drift      -name    "LN0_DR_QD_ED" -length 0.05 
Drift      -name    "LN0_DR_WP" -length 0.1 
Drift      -name    "LN0_DR_LNZ_ED2" -length 0.6 
Drift      -name    "LN0_DR_FL" -length 0.025 
Drift      -name    "LN0_DR_KCA_ED" -length 0.02230105 
Cavity     -name    "LN0_KCA0" -length 0.7553979  -gradient $LN0_KCA_GRD -phase $LN0_KCA_PHS 
set e0      [expr $e0 + 0.7553979*$LN0_KCA_GRD*cos(3.14159265359*$LN0_KCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN0_DR_KCA_ED" -length 0.02230105 
Drift      -name    "LN0_DR_FL" -length 0.025 
Drift      -name    "LN0_DR_LNZ_ED2" -length 0.6 
Drift      -name    "LN0_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN0_QD_D0"  -length 0.08  -strength [expr 0.08*$e0*-8.4] -e0 $e0
Bpm
Drift      -name    "LN0_DR_QD_ED" -length 0.05 
Drift      -name    "BC1_D_V1" -length 3.17229112202 
Dipole
Quadrupole -name    "BC1_Q_V1"  -length 0.08  -strength [expr 0.08*$e0*5.29281436004] -e0 $e0
Bpm
Drift      -name    "BC1_D_V2" -length 1.43789671148 
Dipole
Quadrupole -name    "BC1_Q_V2"  -length 0.08  -strength [expr 0.08*$e0*-9.26805152304] -e0 $e0
Bpm
Drift      -name    "BC1_D_V3" -length 0.485186371723 
Dipole
Quadrupole -name    "BC1_Q_V3"  -length 0.08  -strength [expr 0.08*$e0*13.2720486803] -e0 $e0
Bpm
Drift      -name    "BC1_D_V4" -length 0.2000035292 
Dipole
Quadrupole -name    "BC1_Q_V4"  -length 0.08  -strength [expr 0.08*$e0*-8.47727463588] -e0 $e0
Bpm
Drift      -name    "BC1_D_V5" -length 0.20284930197 
Drift      -name    "BC1_BC_D_20" -length 0.5
Sbend      -name    "BC1_BC_DIP1" -length 0.30009521  -angle 0.04363323  -E1 0 -E2 0.04363323 -hgap 0 -fint 0.5 -tilt 0 -e0 $e0 -six_dim 1 
Drift      -name    "BC1_BC_D_SIDE" -length 3.00285806 
Sbend      -name    "BC1_BC_DIP2" -length 0.30009521  -angle -0.04363323  -E1 -0.04363323 -E2 0 -hgap 0 -fint 0.5 -tilt 0 -e0 $e0 -six_dim 1 
Drift      -name    "BC1_BC_D_CENT" -length 1 
Sbend      -name    "BC1_BC_DIP3" -length 0.30009521  -angle -0.04363323  -E1 0 -E2 -0.04363323 -hgap 0 -fint 0.5 -tilt 0 -e0 $e0 -six_dim 1 
Drift      -name    "BC1_BC_D_SIDE" -length 3.00285806 
Sbend      -name    "BC1_BC_DIP4" -length 0.30009521  -angle 0.04363323  -E1 0.04363323 -E2 0 -hgap 0 -fint 0.5 -tilt 0 -e0 $e0 -six_dim 1 
Drift      -name    "BC1_BC_D_20" -length 0.5 
Drift      -name    "LN1_DR_V1" -length 0.324234168724 
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN1_QD_F_V01"  -length 0.08  -strength [expr 0.08*$e0*8.46660222115] -e0 $e0
Bpm
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Drift      -name    "LN1_DR_V2" -length 1.07059051375 
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN1_QD_D_V02"  -length 0.08  -strength [expr 0.08*$e0*-10.4219944365] -e0 $e0
Bpm
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Drift      -name    "LN1_DR_V3" -length 1.10255617571 
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN1_QD_F_V03"  -length 0.08  -strength [expr 0.08*$e0*10.3091860109] -e0 $e0
Bpm
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Drift      -name    "LN1_DR_V4" -length 0.608998353803 
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN1_QD_D_V04"  -length 0.08  -strength [expr 0.08*$e0*-2.01201790485] -e0 $e0
Bpm
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Drift      -name    "LN1_DR_V5" -length 1.68960197054 
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN1_QD_F0"  -length 0.08  -strength [expr 0.08*$e0*6.6] -e0 $e0
Bpm
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Drift      -name    "LN1_DR_WP" -length 0.1 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_XCA_" -length 0.9164755 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_XCA_" -length 0.9164755 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN1_QD_D0"  -length 0.08  -strength [expr 0.08*$e0*-6.6] -e0 $e0
Bpm
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Drift      -name    "LN1_DR_WP" -length 0.1 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_XCA_" -length 0.9164755 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_XCA_" -length 0.9164755 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN1_QD_F0"  -length 0.08  -strength [expr 0.08*$e0*6.6] -e0 $e0
Bpm
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Drift      -name    "LN1_DR_WP" -length 0.1 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN1_QD_D0"  -length 0.08  -strength [expr 0.08*$e0*-6.6] -e0 $e0
Bpm
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Drift      -name    "LN1_DR_WP" -length 0.1 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN1_QD_F0"  -length 0.08  -strength [expr 0.08*$e0*6.6] -e0 $e0
Bpm
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Drift      -name    "LN1_DR_WP" -length 0.1 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN1_QD_D0"  -length 0.08  -strength [expr 0.08*$e0*-6.6] -e0 $e0
Bpm
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Drift      -name    "LN1_DR_WP" -length 0.1 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN1_QD_F0"  -length 0.08  -strength [expr 0.08*$e0*6.6] -e0 $e0
Bpm
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Drift      -name    "LN1_DR_WP" -length 0.1 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN1_QD_D0"  -length 0.08  -strength [expr 0.08*$e0*-6.6] -e0 $e0
Bpm
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Drift      -name    "LN1_DR_WP" -length 0.1 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN1_QD_F0"  -length 0.08  -strength [expr 0.08*$e0*6.6] -e0 $e0
Bpm
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Drift      -name    "BC2_D_V1" -length 0.96781492761 
Dipole
Quadrupole -name    "BC2_Q_V1"  -length 0.08  -strength [expr 0.08*$e0*-0.084980639069] -e0 $e0
Bpm
Drift      -name    "BC2_D_V2" -length 3.83280463233 
Dipole
Quadrupole -name    "BC2_Q_V2"  -length 0.08  -strength [expr 0.08*$e0*-9.59827767409] -e0 $e0
Bpm
Drift      -name    "BC2_D_V3" -length 0.428135381492 
Dipole
Quadrupole -name    "BC2_Q_V3"  -length 0.08  -strength [expr 0.08*$e0*9.9148927828] -e0 $e0
Bpm
Drift      -name    "BC2_D_V4" -length 0.100800035279 
Drift      -name    "BC2_BC_D_20" -length 0.5 
Sbend      -name    "BC2_BC_DIP1" -length 0.30006718  -angle 0.03665191  -E1 0 -E2 0.03665191 -hgap 0 -fint 0.5 -tilt 0 -e0 $e0 -six_dim 1 
Drift      -name    "BC2_BC_D_SIDE" -length 3.00201617 
Sbend      -name    "BC2_BC_DIP2" -length 0.30006718  -angle -0.03665191  -E1 -0.03665191 -E2 0 -hgap 0 -fint 0.5 -tilt 0 -e0 $e0 -six_dim 1 
Drift      -name    "BC2_BC_D_CENT" -length 1 
Sbend      -name    "BC2_BC_DIP3" -length 0.30006718  -angle -0.03665191  -E1 0 -E2 -0.03665191 -hgap 0 -fint 0.5 -tilt 0 -e0 $e0 -six_dim 1 
Drift      -name    "BC2_BC_D_SIDE" -length 3.00201617 
Sbend      -name    "BC2_BC_DIP4" -length 0.30006718  -angle 0.03665191  -E1 0.03665191 -E2 0 -hgap 0 -fint 0.5 -tilt 0 -e0 $e0 -six_dim 1 
Drift      -name    "BC2_BC_D_20" -length 0.5 
Drift      -name    "LN2_DR_DI_V1" -length 0.200000000039 
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN2_QD_F_DI_V01"  -length 0.08  -strength [expr 0.08*$e0*1.57156607187] -e0 $e0
Bpm
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Drift      -name    "LN2_DR_DI_V2" -length 0.2 
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN2_QD_D_DI_V02"  -length 0.08  -strength [expr 0.08*$e0*4.60478075834] -e0 $e0
Bpm
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Drift      -name    "LN2_DR_DI_V3" -length 0.302063992275 
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN2_QD_F_DI_V03"  -length 0.08  -strength [expr 0.08*$e0*-6.58481356314] -e0 $e0
Bpm
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Drift      -name    "LN2_DR_DI_V4" -length 2.9999999998 
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN2_QD_D_DI_V04"  -length 0.08  -strength [expr 0.08*$e0*5.19014301974] -e0 $e0
Bpm
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Drift      -name    "LN2_DR_DI_V5" -length 0.200000000117 
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN2_QD_D0_DI"  -length 0.08  -strength [expr 0.08*$e0*-7.8] -e0 $e0
Bpm
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Drift      -name    "LN2_DR_DI_1" -length 0.3 
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN2_QD_F0_DI"  -length 0.08  -strength [expr 0.08*$e0*4] -e0 $e0
Bpm
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Drift      -name    "LN2_DR_DI_2_0" -length 1 
Drift      -name    "LN2_DR_DI_2_0" -length 1 
Drift      -name    "LN2_DR_DI_2_0" -length 1 
Drift      -name    "LN2_DR_DI_2_0" -length 1 
Drift      -name    "LN2_DR_DI_2_0" -length 1 
Drift      -name    "LN2_DR_DI_2_0" -length 1 
Drift      -name    "LN2_DR_DI_2_0" -length 1 
Drift      -name    "LN2_DR_DI_2_0" -length 1 
Drift      -name    "LN2_DR_V1" -length 0.200004377725 
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN2_QD_F_V01"  -length 0.08  -strength [expr 0.08*$e0*8.49350732453] -e0 $e0
Bpm
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Drift      -name    "LN2_DR_V2" -length 0.432036706941 
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN2_QD_D_V02"  -length 0.08  -strength [expr 0.08*$e0*-12.485520387] -e0 $e0
Bpm
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Drift      -name    "LN2_DR_V3" -length 0.200000221272 
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN2_QD_F_V03"  -length 0.08  -strength [expr 0.08*$e0*6.05451613189] -e0 $e0
Bpm
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Drift      -name    "LN2_DR_V4" -length 1.57255467532 
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN2_QD_D_V04"  -length 0.08  -strength [expr 0.08*$e0*-2.24106190165] -e0 $e0
Bpm
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Drift      -name    "LN2_DR_V5" -length 2.7747946244 
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN2_QD_F0"  -length 0.08  -strength [expr 0.08*$e0*6.6] -e0 $e0
Bpm
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Drift      -name    "LN2_DR_WP" -length 0.1 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_XCA_" -length 0.9164755 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_XCA_" -length 0.9164755 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN2_QD_D0"  -length 0.08  -strength [expr 0.08*$e0*-6.6] -e0 $e0
Bpm
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Drift      -name    "LN2_DR_WP" -length 0.1 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_XCA_" -length 0.9164755 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_XCA_" -length 0.9164755 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN2_QD_F0"  -length 0.08  -strength [expr 0.08*$e0*6.6] -e0 $e0
Bpm
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Drift      -name    "LN2_DR_WP" -length 0.1 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN2_QD_D0"  -length 0.08  -strength [expr 0.08*$e0*-6.6] -e0 $e0
Bpm
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Drift      -name    "LN2_DR_WP" -length 0.1 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN2_QD_F0"  -length 0.08  -strength [expr 0.08*$e0*6.6] -e0 $e0
Bpm
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Drift      -name    "LN2_DR_WP" -length 0.1 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN2_QD_D0"  -length 0.08  -strength [expr 0.08*$e0*-6.6] -e0 $e0
Bpm
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Drift      -name    "LN2_DR_WP" -length 0.1 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "SBP_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "SBP_QD_F0"  -length 0.08  -strength [expr 0.08*$e0*6.6] -e0 $e0
Bpm
Drift      -name    "SBP_DR_QD_ED" -length 0.05 
Drift      -name    "SBP_DR_WP" -length 0.1 
Drift      -name    "SBP_DR_EDGE" -length 0.055 
Drift      -name    "SBP_DR_FL" -length 0.025 
Drift      -name    "SBP_DR_SCA_ED" -length 0.0997225 
Drift      -name    "SBP_DR_SCA_" -length 1.800555 
Drift      -name    "SBP_DR_SCA_ED" -length 0.0997225 
Drift      -name    "SBP_DR_FL" -length 0.025 
Drift      -name    "SBP_DR_EDGE" -length 0.055 
Drift      -name    "SBP_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "SBP_QD_D0"  -length 0.08  -strength [expr 0.08*$e0*-6.6] -e0 $e0
Bpm
Drift      -name    "SBP_DR_QD_ED" -length 0.05 
Drift      -name    "SBP_DR_D0" -length 0.4 
Dipole
Quadrupole -name    "SBP_QD_F1"  -length 0.08  -strength [expr 0.08*$e0*6.1] -e0 $e0
Bpm
Drift      -name    "SBP_DR_D1" -length 0.48 
Dipole
Quadrupole -name    "SBP_QD_F2"  -length 0.08  -strength [expr 0.08*$e0*-9.5] -e0 $e0
Bpm
Drift      -name    "SBP_DR_D2" -length 0.4 
Dipole
Quadrupole -name    "SBP_QD_F3"  -length 0.08  -strength [expr 0.08*$e0*8.21] -e0 $e0
Bpm
Drift      -name    "SBP_DR_D3" -length 0.45 
Drift      -name    "SEPTUMONOF" -length 1.5 
Drift      -name    "SBP_DR_D4" -length 1 
Drift      -name    "SBP_DR_D4" -length 1 
Drift      -name    "SBP_DR_D4" -length 1 
Drift      -name    "SBP_DR_D4" -length 1 
Drift      -name    "SBP_DR_D4" -length 1 
Drift      -name    "LN3_DR_V1" -length 0.200009195366 
Drift      -name    "LN3_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN3_QD_F_V01"  -length 0.08  -strength [expr 0.08*$e0*11.9431120174] -e0 $e0
Bpm
Drift      -name    "LN3_DR_QD_ED" -length 0.05 
Drift      -name    "LN3_DR_V2" -length 0.83413602647 
Drift      -name    "LN3_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN3_QD_D_V02"  -length 0.08  -strength [expr 0.08*$e0*-12.7887055257] -e0 $e0
Bpm
Drift      -name    "LN3_DR_QD_ED" -length 0.05 
Drift      -name    "LN3_DR_V3" -length 0.991180103155 
Drift      -name    "LN3_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN3_QD_F_V03"  -length 0.08  -strength [expr 0.08*$e0*12.92235303] -e0 $e0
Bpm
Drift      -name    "LN3_DR_QD_ED" -length 0.05 
Drift      -name    "LN3_DR_V4" -length 2.75629479961 
Drift      -name    "LN3_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN3_QD_D_V04"  -length 0.08  -strength [expr 0.08*$e0*-2.03683669765] -e0 $e0
Bpm
Drift      -name    "LN3_DR_QD_ED" -length 0.05 
Drift      -name    "LN3_DR_V5" -length 1.95352905453 
Drift      -name    "LN3_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN3_QD_F0"  -length 0.08  -strength [expr 0.08*$e0*4.2] -e0 $e0
Bpm
Drift      -name    "LN3_DR_QD_ED" -length 0.05 
Drift      -name    "LN3_DR_WP" -length 0.1 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN3_QD_D0"  -length 0.08  -strength [expr 0.08*$e0*-4.2] -e0 $e0
Bpm
Drift      -name    "LN3_DR_QD_ED" -length 0.05 
Drift      -name    "LN3_DR_WP" -length 0.1 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN3_QD_F0"  -length 0.08  -strength [expr 0.08*$e0*4.2] -e0 $e0
Bpm
Drift      -name    "LN3_DR_QD_ED" -length 0.05 
Drift      -name    "LN3_DR_WP" -length 0.1 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN3_QD_D0"  -length 0.08  -strength [expr 0.08*$e0*-4.2] -e0 $e0
Bpm
Drift      -name    "LN3_DR_QD_ED" -length 0.05 
Drift      -name    "LN3_DR_WP" -length 0.1 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN3_QD_F0"  -length 0.08  -strength [expr 0.08*$e0*4.2] -e0 $e0
Bpm
Drift      -name    "LN3_DR_QD_ED" -length 0.05 
Drift      -name    "LN3_DR_WP" -length 0.1 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN3_QD_D0"  -length 0.08  -strength [expr 0.08*$e0*-4.2] -e0 $e0
Bpm
Drift      -name    "LN3_DR_QD_ED" -length 0.05 
Drift      -name    "LN3_DR_WP" -length 0.1 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN3_QD_F0"  -length 0.08  -strength [expr 0.08*$e0*4.2] -e0 $e0
Bpm
Drift      -name    "LN3_DR_QD_ED" -length 0.05 
Drift      -name    "LN3_DR_WP" -length 0.1 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN3_QD_D0"  -length 0.08  -strength [expr 0.08*$e0*-4.2] -e0 $e0
Bpm
Drift      -name    "LN3_DR_QD_ED" -length 0.05 
Drift      -name    "LN3_DR_WP" -length 0.1 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN3_QD_F0"  -length 0.08  -strength [expr 0.08*$e0*4.2] -e0 $e0
Bpm
Drift      -name    "LN3_DR_QD_ED" -length 0.05 
Drift      -name    "LN3_DR_WP" -length 0.1 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN3_QD_D0"  -length 0.08  -strength [expr 0.08*$e0*-4.2] -e0 $e0
Bpm
Drift      -name    "LN3_DR_QD_ED" -length 0.05 
Drift      -name    "LN3_DR_WP" -length 0.1 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN3_QD_F0"  -length 0.08  -strength [expr 0.08*$e0*4.2] -e0 $e0
Bpm
Drift      -name    "LN3_DR_QD_ED" -length 0.05 
Drift      -name    "LN3_DR_WP" -length 0.1 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN3_QD_D0"  -length 0.08  -strength [expr 0.08*$e0*-4.2] -e0 $e0
Bpm
Drift      -name    "LN3_DR_QD_ED" -length 0.05 
Drift      -name    "LN3_DR_WP" -length 0.1 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN3_QD_F0"  -length 0.08  -strength [expr 0.08*$e0*4.2] -e0 $e0
Bpm
Drift      -name    "LN3_DR_QD_ED" -length 0.05 
Drift      -name    "LN3_DR_WP" -length 0.1 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN3_QD_D0"  -length 0.08  -strength [expr 0.08*$e0*-4.2] -e0 $e0
Bpm
Drift      -name    "LN3_DR_QD_ED" -length 0.05 
Drift      -name    "LN3_DR_WP" -length 0.1 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN3_QD_F0"  -length 0.08  -strength [expr 0.08*$e0*4.2] -e0 $e0
Bpm
Drift      -name    "LN3_DR_QD_ED" -length 0.05 
Drift      -name    "LN3_DR_WP" -length 0.1 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN3_QD_D0"  -length 0.08  -strength [expr 0.08*$e0*-4.2] -e0 $e0
Bpm
Drift      -name    "LN3_DR_QD_ED" -length 0.05 
Drift      -name    "LN3_DR_WP" -length 0.1 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN3_QD_F0"  -length 0.08  -strength [expr 0.08*$e0*4.2] -e0 $e0
Bpm
Drift      -name    "LN3_DR_QD_ED" -length 0.05 
Drift      -name    "LN3_DR_WP" -length 0.1 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN3_QD_D0"  -length 0.08  -strength [expr 0.08*$e0*-4.2] -e0 $e0
Bpm
Drift      -name    "LN3_DR_QD_ED" -length 0.05 
Drift      -name    "LN3_DR_WP" -length 0.1 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN3_QD_F0"  -length 0.08  -strength [expr 0.08*$e0*4.2] -e0 $e0
Bpm
Drift      -name    "LN3_DR_QD_ED" -length 0.05 
Drift      -name    "LN3_DR_WP" -length 0.1 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_XCA_" -length 0.9164755 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_XCA_" -length 0.9164755 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_XCA_" -length 0.9164755 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_XCA_" -length 0.9164755 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN3_QD_D0"  -length 0.08  -strength [expr 0.08*$e0*-4.2] -e0 $e0
Bpm
Drift      -name    "LN3_DR_QD_ED" -length 0.05 
Drift      -name    "LN3_DR_WP" -length 0.1 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_XCA_" -length 0.9164755 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_XCA_" -length 0.9164755 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_XCA_" -length 0.9164755 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_XCA_" -length 0.9164755 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_FL" -length 0.025 
Drift      -name    "LN3_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN3_QD_F0"  -length 0.08  -strength [expr 0.08*$e0*4.2] -e0 $e0
Bpm
Drift      -name    "LN3_DR_QD_ED" -length 0.05 
