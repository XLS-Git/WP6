
# *****************************************************************************************
# structure parameters
# *****************************************************************************************

set clight  299792458.0
set pi [expr acos(-1.)]


# xband structure
set xfreq      11.9942
set xcell_l    [format " %.6e " [expr $clight/($xfreq*1e9)/3] ]
set xbore_r    3.5e-3
set xdia       [format " %.6e " 10.139e-3 ]
set xgap       [format " %.6e " [expr $xcell_l - 2.0e-3]]
set xno_of_cel 110
set xlambda    [format " %.6e " [expr  $clight/($xfreq*1e9)]]
set xactv_l    [format " %.6e " [expr  $xno_of_cel*$xcell_l ]]
set xtot_l     [format " %.6e " [expr  1.05]]
set xedge_l    [format " %.6e " [expr  0.5*($xtot_l-$xactv_l)]]
set xgrad      65e-3
set xvolt      [expr $xactv_l*$xgrad]
set xlngf      xwake_l.dat
set xtrsf      xwake_t.dat

array set xband_str   "freq $xfreq aper $xbore_r gap $xgap cell_l $xcell_l grad $xgrad volt $xvolt "
array set xband_str   "dia $xdia n_cell $xno_of_cel active_l $xactv_l edge_l $xedge_l"
array set xband_str   "total_l $xtot_l lambda $xlambda wake_long_file $xlngf wake_tran_file $xtrsf"


Octave {
    xband_str =struct('grad',$xband_str(grad),'freq',$xband_str(freq),'aper',$xband_str(aper), 'gap',$xband_str(gap),'cell_l', $xband_str(cell_l), 'cell_no',$xband_str(n_cell),'total_l',$xband_str(total_l),'active_l',$xband_str(active_l),'wake_long_file','$xband_str(wake_long_file)','wake_tran_file','$xband_str(wake_tran_file)');
}


# kband structure
set kfreq       [expr 3.0*$xfreq]
set kcell_l     [format " %.6e " [expr $clight/($kfreq*1e9)/3.0]]
set kbore_r     2.0e-3
set kdia        [format " %.6e " [expr $xdia/3.0]]
set kgap        [format " %.6e " [expr $kcell_l - 0.6e-3]]
set kno_of_cel  272
set klambda     [format " %.6e " [expr  $clight/($kfreq*1e9)]]
set kactv_l     [format " %.6e " [expr  $kno_of_cel*$kcell_l ]]
set ktot_l      [format " %.6e " [expr  0.8]]
set kedge_l     [format " %.6e " [expr  0.5*($ktot_l-$kactv_l)]]
set kgrad       25e-3
set kvolt       [expr $kactv_l*$kgrad]
set klngf       kwake_l.dat
set ktrsf       kwake_t.dat

array set kband_str   "freq $kfreq aper $kbore_r gap $kgap cell_l $kcell_l grad $kgrad volt $kvolt "
array set kband_str   "dia $kdia n_cell $kno_of_cel active_l $kactv_l edge_l $kedge_l"
array set kband_str   "total_l $ktot_l lambda $klambda wake_long_file $klngf wake_tran_file $ktrsf"


puts [array get kband_str]

Octave {
    kband_str =struct('grad',$kband_str(grad),'freq',$kband_str(freq),'aper',$kband_str(aper), 'gap',$kband_str(gap),'cell_l', $kband_str(cell_l), 'cell_no',$kband_str(n_cell),'total_l',$kband_str(total_l),'active_l',$kband_str(active_l),'wake_long_file','$kband_str(wake_long_file)','wake_tran_file','$kband_str(wake_tran_file)');
}



# sband deflecting structure
set sfreq      2.997
set scell_l    [format " %.6e " [expr $clight/($sfreq*1e9)/3.0] ]
set sbore_r    12e-3
set sdia       [format " %.6e " 44e-3 ]
set sgap       [format " %.6e " [expr $scell_l - 5e-3]]
set sno_of_cel 54
set slambda    [format " %.6e " [expr  $clight/($sfreq*1e9)]]
set sactv_l    [format " %.6e " [expr  $sno_of_cel*$scell_l ]]
set stot_l     [format " %.6e " [expr  2.0]]
set sedge_l    [format " %.6e " [expr  0.5*($stot_l-$sactv_l)]]
set sgrad      1e-3
set svolt      [expr $sactv_l*$sgrad]

set slngf       swake_l.dat
set strsf       swake_t.dat

array set sband_str   "freq $sfreq aper  $sbore_r gap $sgap cell_l $scell_l grad $sgrad volt $svolt "
array set sband_str   "dia  $sdia n_cell $sno_of_cel active_l $sactv_l edge_l $sedge_l"
array set sband_str   "total_l $stot_l lambda $slambda  wake_long_file $slngf wake_tran_file $strsf"


Octave {
    sband_str =struct('grad',$sband_str(grad),'freq',$sband_str(freq),'aper',$sband_str(aper), 'gap',$sband_str(gap),'cell_l', $sband_str(cell_l), 'cell_no',$sband_str(n_cell),'total_l',$sband_str(total_l),'active_l',$sband_str(active_l),'wake_long_file','$sband_str(wake_long_file)','wake_tran_file','$sband_str(wake_tran_file)');
}


# cband structure
set cfreq      5.9971
set ccell_l    [format " %.6e " [expr $clight/($cfreq*1e9)/3.0] ]
set cbore_r    6.3e-3
set cdia       [format " %.6e " 22.194e-3 ]
set cgap       [format " %.6e " [expr $ccell_l - 2.5e-3]]
set cno_of_cel 114
set clambda    [format " %.6e " [expr  $clight/($cfreq*1e9)]]
set cactv_l    [format " %.6e " [expr  $cno_of_cel*$ccell_l ]]
set ctot_l     [format " %.6e " [expr  2.0]]
set cedge_l    [format " %.6e " [expr  0.5*($ctot_l-$cactv_l)]]
set cgrad      40e-3
set cvolt      [expr $cactv_l*$cgrad]

set clngf       cwake_l.dat
set ctrsf       cwake_t.dat

array set cband_str   "freq $cfreq aper  $cbore_r gap $cgap cell_l $ccell_l grad $cgrad volt $cvolt "
array set cband_str   "dia  $cdia n_cell $cno_of_cel active_l $cactv_l edge_l $cedge_l"
array set cband_str   "total_l $ctot_l lambda $clambda wake_long_file $clngf wake_tran_file $ctrsf"

Octave {
    cband_str =struct('grad',$cband_str(grad),'freq',$cband_str(freq),'aper',$cband_str(aper), 'gap',$cband_str(gap),'cell_l', $cband_str(cell_l), 'cell_no',$cband_str(n_cell),'total_l',$cband_str(total_l),'active_l',$cband_str(active_l),'wake_long_file','$cband_str(wake_long_file)','wake_tran_file','$cband_str(wake_tran_file)');
}


