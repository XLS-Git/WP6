set config "softXrays"

source load_files.tcl

set beam0_indist cband_out_4ps_75pC_100k.part
set beamlinename xls_linac
set latfile xls_lat_bba.tcl






array set BeamDefine  [bunch_parameters $script_dir/$beam0_indist]
array set BeamDefine "name beam0 filename $beam0_indist n_slice 201 charge 0.46875e9"
set BeamDefine(n_macro) [expr int($BeamDefine(npart)/$BeamDefine(n_slice))]
lassign "$BeamDefine(beta_x) $BeamDefine(beta_y) $BeamDefine(alpha_x) $BeamDefine(alpha_y)" btx bty alx aly

set e0 $BeamDefine(energy)

puts [array get BeamDefine]

set LN0_XCA_GRD 65e-3
set LN1_XCA_GRD 65e-3
set LN2_XCA_GRD 65e-3
set LN0_KCA_GRD 20.30e-3

set LN0_XCA_PHS 20.0
set LN1_XCA_PHS 15.0
set LN2_XCA_PHS -15.0  ;# added phase due to large shift in time..
set LN0_KCA_PHS [expr 180+$LN0_XCA_PHS-2.5]

set b0_in beam0.in
set b0_ou beam0.ou

BeamlineNew
Girder
source $script_dir/$latfile

BeamlineSet -name $beamlinename
BeamlineUse -name $beamlinename

make_particle_beam_read BeamDefine $script_dir/$BeamDefine(filename)

Octave {
# apply wakes for all structures
    ALLCAVITs = placet_get_number_list("$beamlinename", "cavity");
    placet_element_set_attribute("$beamlinename", ALLCAVITs, "six_dim", true);
    
    XCAVs = placet_get_name_number_list("$beamlinename", "*_XCA0");
    placet_element_set_attribute("$beamlinename", XCAVs, "short_range_wake", "Xband_SR_W");
    placet_element_set_attribute("$beamlinename", XCAVs, "lambda", $xband_str(lambda));
    
    KCAVs = placet_get_name_number_list("$beamlinename", "*_KCA0");
    placet_element_set_attribute("$beamlinename", KCAVs, "short_range_wake", "Kband_SR_W");
    placet_element_set_attribute("$beamlinename", KCAVs, "lambda", $kband_str(lambda));    
    
#  6d tracing in bunch compression
   SIs = placet_get_number_list("$beamlinename", "sbend");
   placet_element_set_attribute("$beamlinename", SIs, "six_dim", true);
   
#  set reference energy for first BC
   BC1Ds = placet_get_name_number_list("$beamlinename", "BC1_BC_DIP*");
   placet_element_set_attribute("$beamlinename", BC1Ds, "e0", 0.278563-0.58e-4);
   
#  set reference energy for second BC
   BC2Ds = placet_get_name_number_list("$beamlinename", "BC2_BC_DIP*");
   placet_element_set_attribute("$beamlinename", BC2Ds, "e0", 1.197931);
}

Octave {

    function M = merit(X)
    LN0.XG = constrain(X(1),  0e-3, 30.4e-3); % GV/m
    LN1.XG = constrain(X(2),  0e-3, 30.4e-3); % GV/m
    LN2.XG = constrain(X(3),  0e-3, 30.4e-3); % GV/m
    LN0.KG = constrain(X(4),  0e-3, 20e-3); % GV/m
    LN0.XP = constrain(X(5),  0, 40); % deg
    LN0.KP = 180+LN0.XP-2.5;
    LN1.XP = constrain(X(6),  0, 40); % deg
    LN2.XP = constrain(X(7), -40, 0); % deg
    A1 = constrain(X(8), 0, 0.1); % rad
    A2 = constrain(X(9), 0, 0.1); % rad
    BC1_BC_DIP = placet_get_name_number_list("$beamlinename", "BC1_BC_DIP*");
    BC2_BC_DIP = placet_get_name_number_list("$beamlinename", "BC2_BC_DIP*");
    placet_element_set_attribute("$beamlinename", BC1_BC_DIP, "angle", [ A1 -A1 -A1 A1 ]);
    placet_element_set_attribute("$beamlinename", BC1_BC_DIP, "E1",    [ 0 -A1 0 A1 ]);
    placet_element_set_attribute("$beamlinename", BC1_BC_DIP, "E2",    [ A1 0 -A1 0 ]);
    placet_element_set_attribute("$beamlinename", BC2_BC_DIP, "angle", [ A2 -A2 -A2 A2 ]);
    placet_element_set_attribute("$beamlinename", BC2_BC_DIP, "E1",    [ 0 -A2 0 A2 ]);
    placet_element_set_attribute("$beamlinename", BC2_BC_DIP, "E2",    [ A2 0 -A2 0 ]);
    #
    I = placet_get_name_number_list("$beamlinename", "LN0_XCA0");
    placet_element_set_attribute("$beamlinename", I, "gradient", LN0.XG);
    placet_element_set_attribute("$beamlinename", I, "phase",    LN0.XP);
    # 
    I = placet_get_name_number_list("$beamlinename", "LN0_KCA0");
    placet_element_set_attribute("$beamlinename", I, "gradient", LN0.KG);
    placet_element_set_attribute("$beamlinename", I, "phase",    LN0.KP);
    #
    I = placet_get_name_number_list("$beamlinename", "LN1_XCA0");
    placet_element_set_attribute("$beamlinename", I, "gradient", LN1.XG);
    placet_element_set_attribute("$beamlinename", I, "phase",    LN1.XP);
    #
    I = placet_get_name_number_list("$beamlinename", "LN2_XCA0");
    placet_element_set_attribute("$beamlinename", I, "gradient", LN2.XG);
    placet_element_set_attribute("$beamlinename", I, "phase",    LN2.XP);
    Tcl_Eval("SetReferenceEnergy -beamline $beamlinename -beam $BeamDefine(name)");
    [E,B] = placet_test_no_correction("$beamlinename", "$BeamDefine(name)", "None");
    meanE = 0.97; % GeV target energy
    sigmaZ = 64.241241; % um target bunch length to get 0.35 kA average current
    _stdE = std(B(:,1))
    _meanE = mean(B(:,1))
    _sigmaZ = std(B(:,4))
    _emitt = placet_get_emittance(B)
    M = 100 * abs(_meanE - meanE) + 0.1 * abs(_sigmaZ - sigmaZ) + 5 * _stdE + 0.01 * sum(abs(E(:,2)) + abs(E(:,6))) / size(E,1)
    save -text params.dat LN0 LN1 LN2 A1 A2
    II = placet_get_name_number_list("$beamlinename", "*");
    STATE = placet_element_get_attributes("$beamlinename", II);
    save -text state.dat STATE
    %plot(E(:,1), E(:,2), 'r-');
    %drawnow;
    end
    
    X = fminsearch(@merit, zeros(9,1))

    load state.dat;
    II = placet_get_name_number_list("$beamlinename", "*");
    placet_element_set_attributes("$beamlinename", II, STATE);
    
    [E,B] = placet_test_no_correction("$beamlinename", "$BeamDefine(name)", "None");
    _stdE = std(B(:,1))
    _meanE = mean(B(:,1))
    _sigmaZ = std(B(:,4))
    _emitt = placet_get_emittance(B)

    save -text out_beam.dat B
}

BeamlineList -file xls_lat_SX.tcl

proc octave_save {} {
    Octave {
	Beams{beaminxdx++} = placet_get_beam();
    }
}

Octave {
    global beaminxdx = 1;
    global Beams = {};
    watches = placet_get_name_number_list("$beamlinename", "*_WA_OU*");
    placet_element_set_attribute("$beamlinename", watches, "tclcall_exit", "octave_save");
}

Octave {
    
    %%  twiss function along beamline
    [s, beta_x, beta_y, alpha_x, alpha_y, mu_x, mu_y] = placet_evolve_beta_function("$beamlinename", $btx, $alx, $bty, $aly);
    beta_arr= [s  beta_x  beta_y  alpha_x alpha_y mu_x mu_y];
    save -text -ascii $beamlinename.twi beta_arr;
    
    #     #   tracking default beam
    # 	[emitt0,B01] = placet_test_no_correction("$beamlinename", "beam0", "None", "%s %E %dE %ex %ey %sz");
    # 	save -text -ascii $b0_ou B01;
}

TestNoCorrection -beam $BeamDefine(name) -survey None -emitt_file $beamlinename.emt

Octave {
    [~,nb]=size(Beams);
    Tcl_SetVar("nbnch", nb);
    fnames={'inj_ou.dat', 'lnz_in.dat', 'ln0_ou.dat', 'bc1_ou.dat', 'ln1_ou.dat', 'bc2_ou.dat', 'ln2_ou.dat'};
    pltnams={};
    
    for i=1:nb
        B=Beams{i};
        fnm=fnames{i};
        pltnams{i}=fnm;
        emean=mean(B(:,1));
        smean=mean(B(:,4));
        B(:,4)=B(:,4)-smean;
        sigz=std(B(:,4));
        printf("at position %s Emean=%f GeV, Sig_z=%f um, S_off=%f um\n",fnm,emean,sigz,smean) 
        save("-text", "-ascii",fnm,"B");
             
    endfor
    
    pltnams = strcat(pltnams,{' '});
    pltnams=cell2mat(pltnams);
    
    Tcl_SetVar("pltnams", pltnams);
    
}

set fl [lindex $pltnams end]
# foreach fl $pltnams {
    set plot_phs  "'$fl' u 4:1 w p ti 'plct', '../eleouts/$fl' u 4:1 w p ti 'elgnt' "
    exec echo " set term x11 \n set key left \n set xlabel 'z (um)' \n set ylabel 'E (GeV)' \n  \
                pl $plot_phs " | gnuplot -persist 
# }
set plot_twi  "'$beamlinename.twi' u 1:2 w l ti ' btx', '$beamlinename.twi' u 1:3 w l  ti 'bty'"

exec echo " set term x11 size 1200,600 \n \
            set xlabel 'distance (m)' \n set ylabel 'beta x,y (m)' \n set key  spacing 0.9 \n set xrange \[0:*\] \n \
            plot $plot_twi  " | gnuplot -persist

# exec echo " set term x11 size 800,600 \n set xlabel 'z (um)' \n set ylabel 'E (GeV)' \n  \
#             pl 	$plot_phs " | gnuplot -persist  

# exit
