set config "hardXrays"

source load_files.tcl

set beam0_indist cband_out_4ps_75pC_100k.part
set beamlinename xls_linac
set latfile xls_lat.tcl


array set BeamDefine  [bunch_parameters $script_dir/$beam0_indist]
array set BeamDefine "name beam0 filename $beam0_indist n_slice 201 charge 0.46875e9"
set BeamDefine(n_macro) [expr int($BeamDefine(npart)/$BeamDefine(n_slice))]
lassign "$BeamDefine(beta_x) $BeamDefine(beta_y) $BeamDefine(alpha_x) $BeamDefine(alpha_y)" btx bty alx aly

set e0 $BeamDefine(energy)

puts [array get BeamDefine]


set LN0_XCA_GRD 65e-3
set LN0_XCA_PHS 20.0
set LN0_KCA_GRD 20.30e-3
set LN0_KCA_PHS [expr 180+$LN0_XCA_PHS-2.5]
set LN1_XCA_GRD 65e-3
set LN1_XCA_PHS 15.0
set LN2_XCA_GRD 65e-3
set LN2_XCA_PHS -15.0  ;# added phase due to large shift in time..


set b0_in beam0.in
set b0_ou beam0.ou

BeamlineNew
Girder
source $script_dir/$latfile

BeamlineSet -name $beamlinename
BeamlineUse -name $beamlinename

make_particle_beam_read BeamDefine $script_dir/$BeamDefine(filename)

Octave {
    global beaminxdx = 1;
    global Beams = {};
}

proc octave_save {} {
    Octave {
      Beams{beaminxdx++} = placet_get_beam();
    }
} 


Octave {
# apply wakes for all structures
    ALLCAVITs = placet_get_number_list("$beamlinename", "cavity");
    placet_element_set_attribute("$beamlinename", ALLCAVITs, "six_dim", true);
    
    XCAVs = placet_get_name_number_list("$beamlinename", "*_XCA0");
    placet_element_set_attribute("$beamlinename", XCAVs, "short_range_wake", "Xband_SR_W");
    placet_element_set_attribute("$beamlinename", XCAVs, "lambda", $xband_str(lambda));
    
    KCAVs = placet_get_name_number_list("$beamlinename", "*_KCA0");
    placet_element_set_attribute("$beamlinename", KCAVs, "short_range_wake", "Kband_SR_W");
    placet_element_set_attribute("$beamlinename", KCAVs, "lambda", $kband_str(lambda));    
    
#  6d tracing in bunch compression
   SIs = placet_get_number_list("$beamlinename", "sbend");
   placet_element_set_attribute("$beamlinename", SIs, "six_dim", true);
   
#  set reference energy for first BC
   BC1Ds = placet_get_name_number_list("$beamlinename", "BC1_BC_DIP*");
   placet_element_set_attribute("$beamlinename", BC1Ds, "e0", 0.278563-0.58e-4);
   
#  set reference energy for second BC
   BC2Ds = placet_get_name_number_list("$beamlinename", "BC2_BC_DIP*");
   placet_element_set_attribute("$beamlinename", BC2Ds, "e0", 1.197931);
   
   watches = placet_get_name_number_list("$beamlinename", "*_WA_OU*");

   placet_element_set_attribute("beamline", watches, "tclcall_exit", "octave_save");
}

Octave {
    
   %%  twiss function along beamline
	[s, beta_x, beta_y, alpha_x, alpha_y, mu_x, mu_y] = placet_evolve_beta_function("$beamlinename", $btx, $alx, $bty, $aly);
	beta_arr= [s  beta_x  beta_y  alpha_x alpha_y mu_x mu_y];
	save -text -ascii $beamlinename.twi beta_arr;
    
#     #   tracking default beam
# 	[emitt0,B01] = placet_test_no_correction("$beamlinename", "beam0", "None", "%s %E %dE %ex %ey %sz");
# 	save -text -ascii $b0_ou B01;
}

BeamlineSaveXML -beamline "$beamlinename" -file beamline.xml

TestNoCorrection -beam $BeamDefine(name) -survey None -emitt_file $beamlinename.emt


Octave {
    [~,nb]=size(Beams);
    Tcl_SetVar("nbnch", nb);
    fnames={'inj_ou.dat', 'lnz_in.dat', 'ln0_ou.dat', 'bc1_ou.dat', 'ln1_ou.dat', 'bc2_ou.dat', 'ln2_ou.dat'};
    pltnams={};
    
    for i=1:nb
        B=Beams{i};
        fnm=fnames{i};
        pltnams{i}=fnm;
        emean=mean(B(:,1));
        smean=mean(B(:,4));
        B(:,4)=B(:,4)-smean;
        sigz=std(B(:,4));
        printf("at position %s Emean=%f GeV, Sig_z=%f um, S_off=%f um\n",fnm,emean,sigz,smean) 
        save("-text", "-ascii",fnm,"B");
             
    endfor
    
    pltnams = strcat(pltnams,{' '});
    pltnams=cell2mat(pltnams);
    
    Tcl_SetVar("pltnams", pltnams);
    
}

set fl [lindex $pltnams end]
# foreach fl $pltnams {
    set plot_phs  "'$fl' u 4:1 w p ti 'plct', '../eleouts/$fl' u 4:1 w p ti 'elgnt' "
    exec echo " set term x11 \n set key left \n set xlabel 'z (um)' \n set ylabel 'E (GeV)' \n  \
                pl $plot_phs " | gnuplot -persist 
# }
set plot_twi  "'$beamlinename.twi' u 1:2 w l ti ' btx', '$beamlinename.twi' u 1:3 w l  ti 'bty'"

exec echo " set term x11 size 1200,600 \n \
            set xlabel 'distance (m)' \n set ylabel 'beta x,y (m)' \n set key  spacing 0.9 \n set xrange \[0:*\] \n \
            plot $plot_twi  " | gnuplot -persist

# exec echo " set term x11 size 800,600 \n set xlabel 'z (um)' \n set ylabel 'E (GeV)' \n  \
#             pl 	$plot_phs " | gnuplot -persist  

# exit
