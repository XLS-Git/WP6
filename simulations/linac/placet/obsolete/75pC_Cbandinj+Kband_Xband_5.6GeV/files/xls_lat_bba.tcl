Girder
SetReferenceEnergy  $e0
#   warnign CHARGE with name BNCH is unknown element 
Drift      -name    "INJ_WA_OU" -length 0 
Drift      -name    "LN0_DR_V1" -length 2.99080516272 
Drift      -name    "LN0_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN0_QD_F_V01"  -length 0.15  -strength [expr 0.15*$e0*8.8453778264] -e0 $e0
Bpm
Drift      -name    "LN0_DR_QD_ED" -length 0.05 
Drift      -name    "LN0_DR_V2" -length 0.660866117233 
Drift      -name    "LN0_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN0_QD_D_V02"  -length 0.15  -strength [expr 0.15*$e0*-7.84014351775] -e0 $e0
Bpm
Drift      -name    "LN0_DR_QD_ED" -length 0.05 
Drift      -name    "LN0_DR_V3" -length 0.988300054739 
Drift      -name    "LN0_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN0_QD_F_V03"  -length 0.15  -strength [expr 0.15*$e0*8.16413721629] -e0 $e0
Bpm
Drift      -name    "LN0_DR_QD_ED" -length 0.05 
Drift      -name    "LN0_DR_V4" -length 1.47523693706 
Drift      -name    "LN0_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN0_QD_D_V04"  -length 0.15  -strength [expr 0.15*$e0*-2.65674371765] -e0 $e0
Bpm
Drift      -name    "LN0_DR_QD_ED" -length 0.05 
Drift      -name    "LN0_DR_V5" -length 2.15642836819 
Drift      -name    "LN0_WA_M_OU" -length 0 
Drift      -name    "LN0_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN0_QD_F0"  -length 0.08  -strength [expr 0.08*$e0*9] -e0 $e0
Bpm
Drift      -name    "LN0_DR_QD_ED" -length 0.05 
Drift      -name    "LN0_DR_WP" -length 0.1 
Drift      -name    "LN0_DR_FL" -length 0.025 
Drift      -name    "LN0_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN0_XCA0" -length 0.9164755  -gradient $LN0_XCA_GRD -phase $LN0_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN0_XCA_GRD*cos(3.14159265359*$LN0_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN0_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN0_DR_FL" -length 0.025 
Drift      -name    "LN0_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN0_QD_D0"  -length 0.08  -strength [expr 0.08*$e0*-9] -e0 $e0
Bpm
Drift      -name    "LN0_DR_QD_ED" -length 0.05 
Drift      -name    "LN0_DR_WP" -length 0.1 
Drift      -name    "LN0_DR_FL" -length 0.025 
Drift      -name    "LN0_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN0_XCA0" -length 0.9164755  -gradient $LN0_XCA_GRD -phase $LN0_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN0_XCA_GRD*cos(3.14159265359*$LN0_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN0_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN0_DR_FL" -length 0.025 
Drift      -name    "LN0_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN0_QD_F0"  -length 0.08  -strength [expr 0.08*$e0*9] -e0 $e0
Bpm
Drift      -name    "LN0_DR_QD_ED" -length 0.05 
Drift      -name    "LN0_DR_WP" -length 0.1 
Drift      -name    "LN0_DR_FL" -length 0.025 
Drift      -name    "LN0_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN0_XCA0" -length 0.9164755  -gradient $LN0_XCA_GRD -phase $LN0_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN0_XCA_GRD*cos(3.14159265359*$LN0_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN0_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN0_DR_FL" -length 0.025 
Drift      -name    "LN0_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN0_QD_D0"  -length 0.08  -strength [expr 0.08*$e0*-9] -e0 $e0
Bpm
Drift      -name    "LN0_DR_QD_ED" -length 0.05 
Drift      -name    "LN0_DR_WP" -length 0.1 
Drift      -name    "LN0_DR_FL" -length 0.025 
Drift      -name    "LN0_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN0_XCA0" -length 0.9164755  -gradient $LN0_XCA_GRD -phase $LN0_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN0_XCA_GRD*cos(3.14159265359*$LN0_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN0_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN0_DR_FL" -length 0.025 
Drift      -name    "LN0_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN0_QD_F0"  -length 0.08  -strength [expr 0.08*$e0*9] -e0 $e0
Bpm
Drift      -name    "LN0_DR_QD_ED" -length 0.05 
Drift      -name    "LN0_WA_LNZ_IN" -length 0 
Drift      -name    "LN0_DR_WP" -length 0.1 
Drift      -name    "LN0_DR_KCA_ED2" -length 0.115 
Drift      -name    "LN0_DR_FL" -length 0.025 
Drift      -name    "LN0_DR_KCA_ED" -length 0.02230105 
Cavity     -name    "LN0_KCA0" -length 0.7553979  -gradient $LN0_KCA_GRD -phase $LN0_KCA_PHS 
set e0      [expr $e0 + 0.7553979*$LN0_KCA_GRD*cos(3.14159265359*$LN0_KCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN0_DR_KCA_ED" -length 0.02230105 
Drift      -name    "LN0_DR_FL" -length 0.025 
Drift      -name    "LN0_DR_KCA_ED2" -length 0.115 
Drift      -name    "LN0_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN0_QD_D0"  -length 0.08  -strength [expr 0.08*$e0*-9] -e0 $e0
Bpm
Drift      -name    "LN0_DR_QD_ED" -length 0.05 
Drift      -name    "LN0_DR_WP" -length 0.1 
Drift      -name    "LN0_DR_KCA_ED2" -length 0.115 
Drift      -name    "LN0_DR_FL" -length 0.025 
Drift      -name    "LN0_DR_KCA_ED" -length 0.02230105 
Cavity     -name    "LN0_KCA0" -length 0.7553979  -gradient $LN0_KCA_GRD -phase $LN0_KCA_PHS 
set e0      [expr $e0 + 0.7553979*$LN0_KCA_GRD*cos(3.14159265359*$LN0_KCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN0_DR_KCA_ED" -length 0.02230105 
Drift      -name    "LN0_DR_FL" -length 0.025 
Drift      -name    "LN0_DR_KCA_ED2" -length 0.115 
Drift      -name    "LN0_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN0_QD_F0"  -length 0.08  -strength [expr 0.08*$e0*9] -e0 $e0
Bpm
Drift      -name    "LN0_DR_QD_ED" -length 0.05 
Drift      -name    "LN0_WA_OU" -length 0 
Drift      -name    "BC1_D_V1" -length 0.158556598122 
Dipole
Quadrupole -name    "BC1_Q_V1"  -length 0.15  -strength [expr 0.15*$e0*1.59473163408] -e0 $e0
Bpm
Drift      -name    "BC1_D_V2" -length 3.11255698156 
Dipole
Quadrupole -name    "BC1_Q_V2"  -length 0.15  -strength [expr 0.15*$e0*-8.34628583665] -e0 $e0
Bpm
Drift      -name    "BC1_D_V3" -length 0.110219673179 
Dipole
Quadrupole -name    "BC1_Q_V3"  -length 0.15  -strength [expr 0.15*$e0*8.56153364918] -e0 $e0
Bpm
Drift      -name    "BC1_D_V4" -length 0.100000328915 
Drift      -name    "BC1_BC_D_20" -length 0.5 
Sbend      -name    "BC1_BC_DIP1" -length 0.30014173  -angle 0.05323254  -E1 0 -E2 0.05323254 -hgap 0 -fint 0.5 -tilt 0 -e0 $e0 -six_dim 1 
Drift      -name    "BC1_BC_D_SIDE" -length 3.00425558 
Sbend      -name    "BC1_BC_DIP2" -length 0.30014173  -angle -0.05323254  -E1 -0.05323254 -E2 0 -hgap 0 -fint 0.5 -tilt 0 -e0 $e0 -six_dim 1 
Drift      -name    "BC1_BC_D_CENT" -length 1 
Sbend      -name    "BC1_BC_DIP3" -length 0.30014173  -angle -0.05323254  -E1 0 -E2 -0.05323254 -hgap 0 -fint 0.5 -tilt 0 -e0 $e0 -six_dim 1 
Drift      -name    "BC1_BC_D_SIDE" -length 3.00425558 
Sbend      -name    "BC1_BC_DIP4" -length 0.30014173  -angle 0.05323254  -E1 0.05323254 -E2 0 -hgap 0 -fint 0.5 -tilt 0 -e0 $e0 -six_dim 1 
Drift      -name    "BC1_BC_D_20" -length 0.5 
Drift      -name    "BC1_WA_OU" -length 0 
Drift      -name    "LN1_DR_V1" -length 0.23147703299 
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN1_QD_F_V01"  -length 0.15  -strength [expr 0.15*$e0*7.96834253069] -e0 $e0
Bpm
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Drift      -name    "LN1_DR_V2" -length 0.402673564142 
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN1_QD_D_V02"  -length 0.15  -strength [expr 0.15*$e0*-9.94858930919] -e0 $e0
Bpm
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Drift      -name    "LN1_DR_V3" -length 0.600001872173 
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN1_QD_F_V03"  -length 0.15  -strength [expr 0.15*$e0*12.671062198] -e0 $e0
Bpm
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Drift      -name    "LN1_DR_V4" -length 0.290356783915 
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN1_QD_D_V04"  -length 0.15  -strength [expr 0.15*$e0*-1.56787179495] -e0 $e0
Bpm
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Drift      -name    "LN1_DR_V5" -length 1.58762491259 
Drift      -name    "LN1_WA_M_OU" -length 0 
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN1_QD_F0"  -length 0.08  -strength [expr 0.08*$e0*6.6] -e0 $e0
Bpm
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Drift      -name    "LN1_DR_WP" -length 0.1 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN1_QD_D0"  -length 0.08  -strength [expr 0.08*$e0*-6.6] -e0 $e0
Bpm
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Drift      -name    "LN1_DR_WP" -length 0.1 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN1_QD_F0"  -length 0.08  -strength [expr 0.08*$e0*6.6] -e0 $e0
Bpm
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Drift      -name    "LN1_DR_WP" -length 0.1 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN1_QD_D0"  -length 0.08  -strength [expr 0.08*$e0*-6.6] -e0 $e0
Bpm
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Drift      -name    "LN1_DR_WP" -length 0.1 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN1_QD_F0"  -length 0.08  -strength [expr 0.08*$e0*6.6] -e0 $e0
Bpm
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Drift      -name    "LN1_DR_WP" -length 0.1 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN1_QD_D0"  -length 0.08  -strength [expr 0.08*$e0*-6.6] -e0 $e0
Bpm
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Drift      -name    "LN1_DR_WP" -length 0.1 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN1_QD_F0"  -length 0.08  -strength [expr 0.08*$e0*6.6] -e0 $e0
Bpm
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Drift      -name    "LN1_DR_WP" -length 0.1 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN1_QD_D0"  -length 0.08  -strength [expr 0.08*$e0*-6.6] -e0 $e0
Bpm
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Drift      -name    "LN1_DR_WP" -length 0.1 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN1_QD_F0"  -length 0.08  -strength [expr 0.08*$e0*6.6] -e0 $e0
Bpm
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Drift      -name    "LN1_WA_OU" -length 0 
Drift      -name    "BC2_D_V1" -length 0.296855526635 
Dipole
Quadrupole -name    "BC2_Q_V1"  -length 0.15  -strength [expr 0.15*$e0*2.04245335941] -e0 $e0
Bpm
Drift      -name    "BC2_D_V2" -length 2.94082664181 
Dipole
Quadrupole -name    "BC2_Q_V2"  -length 0.15  -strength [expr 0.15*$e0*-8.8154757531] -e0 $e0
Bpm
Drift      -name    "BC2_D_V3" -length 0.115724044986 
Dipole
Quadrupole -name    "BC2_Q_V3"  -length 0.15  -strength [expr 0.15*$e0*9.1859802595] -e0 $e0
Bpm
Drift      -name    "BC2_D_V4" -length 0.118828549854 
Drift      -name    "BC2_BC_D_20" -length 0.5 
Sbend      -name    "BC2_BC_DIP1" -length 0.30005499  -angle 0.03316126  -E1 0 -E2 0.03316126 -hgap 0 -fint 0.5 -tilt 0 -e0 $e0 -six_dim 1 
Drift      -name    "BC2_BC_D_SIDE" -length 3.00165026 
Sbend      -name    "BC2_BC_DIP2" -length 0.30005499  -angle -0.03316126  -E1 -0.03316126 -E2 0 -hgap 0 -fint 0.5 -tilt 0 -e0 $e0 -six_dim 1 
Drift      -name    "BC2_BC_D_CENT" -length 1 
Sbend      -name    "BC2_BC_DIP3" -length 0.30005499  -angle -0.03316126  -E1 0 -E2 -0.03316126 -hgap 0 -fint 0.5 -tilt 0 -e0 $e0 -six_dim 1 
Drift      -name    "BC2_BC_D_SIDE" -length 3.00165026 
Sbend      -name    "BC2_BC_DIP4" -length 0.30005499  -angle 0.03316126  -E1 0.03316126 -E2 0 -hgap 0 -fint 0.5 -tilt 0 -e0 $e0 -six_dim 1 
Drift      -name    "BC2_BC_D_20" -length 0.5 
Drift      -name    "BC2_WA_OU" -length 0 
Drift      -name    "LN2_DR_V1" -length 0.213984979176 
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN2_QD_F_V01"  -length 0.15  -strength [expr 0.15*$e0*8.73168558154] -e0 $e0
Bpm
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Drift      -name    "LN2_DR_V2" -length 0.459127733902 
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN2_QD_D_V02"  -length 0.15  -strength [expr 0.15*$e0*-10.5807351699] -e0 $e0
Bpm
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Drift      -name    "LN2_DR_V3" -length 0.442035027619 
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN2_QD_F_V03"  -length 0.15  -strength [expr 0.15*$e0*11.9359025666] -e0 $e0
Bpm
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Drift      -name    "LN2_DR_V4" -length 1.86691647518 
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN2_QD_D_V04"  -length 0.15  -strength [expr 0.15*$e0*-1.25335822883] -e0 $e0
Bpm
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Drift      -name    "LN2_DR_V5" -length 2.42017854883 
Drift      -name    "LN2_WA_M_OU" -length 0 
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN2_QD_F0"  -length 0.08  -strength [expr 0.08*$e0*4.2] -e0 $e0
Bpm
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Drift      -name    "LN2_DR_WP" -length 0.1 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN2_QD_D0"  -length 0.08  -strength [expr 0.08*$e0*-4.2] -e0 $e0
Bpm
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Drift      -name    "LN2_DR_WP" -length 0.1 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN2_QD_F0"  -length 0.08  -strength [expr 0.08*$e0*4.2] -e0 $e0
Bpm
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Drift      -name    "LN2_DR_WP" -length 0.1 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN2_QD_D0"  -length 0.08  -strength [expr 0.08*$e0*-4.2] -e0 $e0
Bpm
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Drift      -name    "LN2_DR_WP" -length 0.1 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN2_QD_F0"  -length 0.08  -strength [expr 0.08*$e0*4.2] -e0 $e0
Bpm
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Drift      -name    "LN2_DR_WP" -length 0.1 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN2_QD_D0"  -length 0.08  -strength [expr 0.08*$e0*-4.2] -e0 $e0
Bpm
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Drift      -name    "LN2_DR_WP" -length 0.1 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN2_QD_F0"  -length 0.08  -strength [expr 0.08*$e0*4.2] -e0 $e0
Bpm
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Drift      -name    "LN2_DR_WP" -length 0.1 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN2_QD_D0"  -length 0.08  -strength [expr 0.08*$e0*-4.2] -e0 $e0
Bpm
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Drift      -name    "LN2_DR_WP" -length 0.1 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN2_QD_F0"  -length 0.08  -strength [expr 0.08*$e0*4.2] -e0 $e0
Bpm
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Drift      -name    "LN2_DR_WP" -length 0.1 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN2_QD_D0"  -length 0.08  -strength [expr 0.08*$e0*-4.2] -e0 $e0
Bpm
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Drift      -name    "LN2_DR_WP" -length 0.1 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN2_QD_F0"  -length 0.08  -strength [expr 0.08*$e0*4.2] -e0 $e0
Bpm
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Drift      -name    "LN2_DR_WP" -length 0.1 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN2_QD_D0"  -length 0.08  -strength [expr 0.08*$e0*-4.2] -e0 $e0
Bpm
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Drift      -name    "LN2_DR_WP" -length 0.1 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN2_QD_F0"  -length 0.08  -strength [expr 0.08*$e0*4.2] -e0 $e0
Bpm
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Drift      -name    "LN2_DR_WP" -length 0.1 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN2_QD_D0"  -length 0.08  -strength [expr 0.08*$e0*-4.2] -e0 $e0
Bpm
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Drift      -name    "LN2_DR_WP" -length 0.1 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN2_QD_F0"  -length 0.08  -strength [expr 0.08*$e0*4.2] -e0 $e0
Bpm
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Drift      -name    "LN2_DR_WP" -length 0.1 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN2_QD_D0"  -length 0.08  -strength [expr 0.08*$e0*-4.2] -e0 $e0
Bpm
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Drift      -name    "LN2_DR_WP" -length 0.1 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN2_QD_F0"  -length 0.08  -strength [expr 0.08*$e0*4.2] -e0 $e0
Bpm
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Drift      -name    "LN2_DR_WP" -length 0.1 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN2_QD_D0"  -length 0.08  -strength [expr 0.08*$e0*-4.2] -e0 $e0
Bpm
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Drift      -name    "LN2_DR_WP" -length 0.1 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN2_QD_F0"  -length 0.08  -strength [expr 0.08*$e0*4.2] -e0 $e0
Bpm
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Drift      -name    "LN2_DR_WP" -length 0.1 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_XCA_" -length 0.9164755 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_XCA_" -length 0.9164755 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_XCA_" -length 0.9164755 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_XCA_" -length 0.9164755 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN2_QD_D0"  -length 0.08  -strength [expr 0.08*$e0*-4.2] -e0 $e0
Bpm
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Drift      -name    "LN2_DR_WP" -length 0.1 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_XCA_" -length 0.9164755 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_XCA_" -length 0.9164755 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_XCA_" -length 0.9164755 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_XCA_" -length 0.9164755 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Dipole
Quadrupole -name    "LN2_QD_F0"  -length 0.08  -strength [expr 0.08*$e0*4.2] -e0 $e0
Bpm
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Drift      -name    "LN2_WA_OU" -length 0 
