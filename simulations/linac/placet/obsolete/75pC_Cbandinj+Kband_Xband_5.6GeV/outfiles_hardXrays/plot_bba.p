set term post eps enh solid colo 18

set xlabel 's [m]'
set ylabel '{/Symbol e}_x [mm.mrad]'

set log y
set out 'plot_emittx.eps'

plot\
 'xls_linac_emitt_no_n_100.dat' u 1:($4/10) ti 'no correction' w lp,\
 'xls_linac_emitt_simple_b_1_n_100.dat' u 1:($4/10) ti 'orbit correction' w lp,\
 'xls_linac_emitt_dfs_w_14_b_1_1_n_100.dat' u 1:($4/10) ti 'dispersion-free correction' w lp,\
 'xls_linac_emitt_wfs_w_14_14_b_1_1_1_c_0.9_n_100.dat' u 1:($4/10) ti 'wakefield-free correction' w lp

set ylabel '{/Symbol e}_y [mm.mrad]'
set out 'plot_emitty.eps'

plot\
 'xls_linac_emitt_no_n_100.dat' u 1:($5/10) ti 'no correction' w lp,\
 'xls_linac_emitt_simple_b_1_n_100.dat' u 1:($5/10) ti 'orbit correction' w lp,\
 'xls_linac_emitt_dfs_w_14_b_1_1_n_100.dat' u 1:($5/10) ti 'dispersion-free correction' w lp,\
 'xls_linac_emitt_wfs_w_14_14_b_1_1_1_c_0.9_n_100.dat' u 1:($5/10) ti 'wakefield-free correction' w lp

set out

set xlabel 's [m]'
set ylabel '{/Symbol b} [m]'

unset log y

set out 'plot_beta.eps'

plot\
 'xls_linac.twi' u 1:2 ti '{/Symbol b}_x' w lp,\
 'xls_linac.twi' u 1:3 ti '{/Symbol b}_y' w lp

set out

!for i in plot*.eps ; do epstopdf $i ; rm -f $i ; done
