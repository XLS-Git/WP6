set config "softXrays"

set sigma 100.0
set bpmres 5.0
set dphase1 10
set dphase2 10
set dcharge 0.9
set beta0 1
set beta1 1
set beta2 1
set wgt1 -1
set wgt2 -1
set nbins 1
set noverlap 0.0
set nm 100
set machine all

source load_files.tcl

set beam0_indist cband_out_4ps_75pC_100k.part
set beamlinename xls_linac
set latfile xls_lat_SX.tcl


array set BeamDefine  [bunch_parameters $script_dir/$beam0_indist]
array set BeamDefine "name beam0 filename $beam0_indist n_slice 201 charge 0.46875e9"
set BeamDefine(n_macro) [expr int($BeamDefine(npart)/$BeamDefine(n_slice))]
lassign "$BeamDefine(beta_x) $BeamDefine(beta_y) $BeamDefine(alpha_x) $BeamDefine(alpha_y)" btx bty alx aly

set e0 $BeamDefine(energy)

puts [array get BeamDefine]


set LN0_XCA_GRD 65e-3
set LN0_XCA_PHS 20.0
set LN0_KCA_GRD 20.30e-3
set LN0_KCA_PHS [expr 180+$LN0_XCA_PHS-2.5]
set LN1_XCA_GRD 65e-3
set LN1_XCA_PHS 15.0
set LN2_XCA_GRD 65e-3
set LN2_XCA_PHS -15.0  ;# added phase due to large shift in time..


set b0_in beam0.in
set b0_ou beam0.ou

BeamlineNew
Girder
source $script_dir/$latfile

BeamlineSet -name $beamlinename
BeamlineUse -name $beamlinename

make_particle_beam_read BeamDefine $script_dir/$BeamDefine(filename)

### BEAM1 for WFS

array set BeamDefine  [bunch_parameters $script_dir/$beam0_indist]
array set BeamDefine "name beam1 filename $beam0_indist n_slice 201 charge [expr 0.46875e9 * $dcharge]"
set BeamDefine(n_macro) [expr int($BeamDefine(npart)/$BeamDefine(n_slice))]
lassign "$BeamDefine(beta_x) $BeamDefine(beta_y) $BeamDefine(alpha_x) $BeamDefine(alpha_y)" btx bty alx aly

set e0 $BeamDefine(energy)

puts [array get BeamDefine]

make_particle_beam_read BeamDefine $script_dir/$BeamDefine(filename)

Octave {
    [E,B] = placet_test_no_correction("$beamlinename", "$BeamDefine(name)", "None");
    _stdE = std(B(:,1))
    _meanE = mean(B(:,1))
    _sigmaZ = std(B(:,4))
    _emitt = placet_get_emittance(B)
}

Octave {
    function reduce_energy()
      ACI1 = placet_get_name_number_list("$beamlinename", "LN0_XCA*");
      ACI2 = placet_get_name_number_list("$beamlinename", "LN1_XCA*");
      placet_element_vary_attribute("$beamlinename", ACI1, "phase", +$dphase1);
      placet_element_vary_attribute("$beamlinename", ACI2, "phase", +$dphase2);
    end

    function reset_energy()
      ACI1 = placet_get_name_number_list("$beamlinename", "LN0_XCA*");
      ACI2 = placet_get_name_number_list("$beamlinename", "LN1_XCA*");
      placet_element_vary_attribute("$beamlinename", ACI1, "phase", -$dphase1);
      placet_element_vary_attribute("$beamlinename", ACI2, "phase", -$dphase2);
    end
}

Octave {
    if exist('R_${beamlinename}_p_${dphase1}_${dphase2}_c_${dcharge}.dat', "file")
    load 'R_${beamlinename}_p_${dphase1}_${dphase2}_c_${dcharge}.dat';
    else
    Response.Cx = placet_get_number_list("$beamlinename", "dipole");
    Response.Cy = placet_get_number_list("$beamlinename", "dipole");
    Response.Bpms = placet_get_number_list("$beamlinename", "bpm");

    # picks all correctors preceeding the last bpm, and all bpms following the first corrector
    Response.Cx = Response.Cx(Response.Cx < Response.Bpms(end));
    Response.Cy = Response.Cy(Response.Cy < Response.Bpms(end));
    Response.Bpms = Response.Bpms(Response.Bpms > max(Response.Cx(1), Response.Cy(1)));

    # gets the energy at each corrector
    Response.Ex = placet_element_get_attribute("$beamlinename", Response.Cx, "e0");
    Response.Ey = placet_element_get_attribute("$beamlinename", Response.Cy, "e0");
    
    # compute response matrices
    placet_test_no_correction("$beamlinename", "beam0", "Zero");
    Response.B0 = placet_get_bpm_readings("$beamlinename", Response.Bpms);
    Response.R0x = placet_get_response_matrix_attribute ("$beamlinename", "beam0", Response.Bpms, "x", Response.Cx, "strength_x", "Zero");
    Response.R0y = placet_get_response_matrix_attribute ("$beamlinename", "beam0", Response.Bpms, "y", Response.Cy, "strength_y", "Zero");
    reduce_energy();
    placet_test_no_correction("$beamlinename", "beam0", "Zero");
    Response.B1 = placet_get_bpm_readings("$beamlinename", Response.Bpms) - Response.B0;
    Response.R1x = placet_get_response_matrix_attribute ("$beamlinename", "beam0", Response.Bpms, "x", Response.Cx, "strength_x", "Zero") - Response.R0x;
    Response.R1y = placet_get_response_matrix_attribute ("$beamlinename", "beam0", Response.Bpms, "y", Response.Cy, "strength_y", "Zero") - Response.R0y;
    reset_energy();
    placet_test_no_correction("$beamlinename", "beam1", "Zero");
    Response.B2 = placet_get_bpm_readings("$beamlinename", Response.Bpms) - Response.B0;
    Response.R2x = placet_get_response_matrix_attribute ("$beamlinename", "beam1", Response.Bpms, "x", Response.Cx, "strength_x", "Zero") - Response.R0x;
    Response.R2y = placet_get_response_matrix_attribute ("$beamlinename", "beam1", Response.Bpms, "y", Response.Cy, "strength_y", "Zero") - Response.R0y;
    save -text 'R_${beamlinename}_p_${dphase1}_${dphase2}_c_${dcharge}.dat' Response;
    end
}

proc my_survey {} {
    global beamlinename machine sigma bpmres
    Octave {
	randn("seed", $machine * 1e4);
	BI = placet_get_number_list("$beamlinename", "bpm");
	CI = placet_get_number_list("$beamlinename", "cavity");
	DI = placet_get_number_list("$beamlinename", "dipole");
	QI = placet_get_number_list("$beamlinename", "quadrupole");
	placet_element_set_attribute("$beamlinename", DI, "strength_x", 0.0);
	placet_element_set_attribute("$beamlinename", DI, "strength_y", 0.0);
	placet_element_set_attribute("$beamlinename", BI, "resolution", $bpmres);
	placet_element_set_attribute("$beamlinename", BI, "x", randn(size(BI)) * $sigma);
	placet_element_set_attribute("$beamlinename", BI, "y", randn(size(BI)) * $sigma);
	placet_element_set_attribute("$beamlinename", CI, "x", randn(size(CI)) * $sigma);
	placet_element_set_attribute("$beamlinename", CI, "y", randn(size(CI)) * $sigma);
	placet_element_set_attribute("$beamlinename", QI, "x", randn(size(QI)) * $sigma);
	placet_element_set_attribute("$beamlinename", QI, "y", randn(size(QI)) * $sigma);
    }
}

if { $machine == "all" } {
    set machine_start 1
    set machine_end $nm
} {
    set machine_start $machine
    set machine_end $machine
}

if { $wgt1 == -1 } {
    set wgt1 [expr round(sqrt(($bpmres * $bpmres + $sigma * $sigma) / (2 * $bpmres * $bpmres)))]
    puts "Using DFS theoretical weight = $wgt1"
}

if { $wgt2 == -1 } {
    set wgt2 [expr round(sqrt(($bpmres * $bpmres + $sigma * $sigma) / (2 * $bpmres * $bpmres)))]
    puts "Using WFS theoretical weight = $wgt2"
}

Octave {
    [E,B] = placet_test_no_correction("$beamlinename", "beam0", "Zero", "%s %E %dE %ex %ey %sz");
    E0 = zeros(size(E));
    E1 = zeros(size(E));
    E2 = zeros(size(E));
    E3 = zeros(size(E));
    EmittX_hist = zeros($nm, 4);
    EmittY_hist = zeros($nm, 4);
}

# Binning
Octave {
    nBpms = length(Response.Bpms);
    Blen = nBpms / ($nbins * (1 - $noverlap) + $noverlap);
    for i=1:$nbins
      Bmin = floor((i - 1) * Blen - (i - 1) * Blen * $noverlap) + 1;
      Bmax = floor((i)     * Blen - (i - 1) * Blen * $noverlap);
      Cxmin = find(Response.Cx < Response.Bpms(Bmin))(end);
      Cxmax = find(Response.Cx < Response.Bpms(Bmax))(end);
      Cymin = find(Response.Cy < Response.Bpms(Bmin))(end);
      Cymax = find(Response.Cy < Response.Bpms(Bmax))(end);
      Bins(i).Bpms = Bmin:Bmax;
      Bins(i).Cx = Cxmin:Cxmax;
      Bins(i).Cy = Cymin:Cymax;
    end
    printf("Each bin contains approx %g bpms.\n", round(Blen));
}

for {set machine $machine_start} {$machine <= $machine_end} {incr machine} {
    
    puts "MACHINE: $machine/$nm"
    
    my_survey
    
    Octave {
	disp("TRACKING...");
	[E,B] = placet_test_no_correction("$beamlinename", "beam0", "None", "%s %E %dE %ex %ey %sz");
	E0 = E0 .+ E;
	EmittX_hist($machine, 1) = E(end,4);
	EmittY_hist($machine, 1) = E(end,5);
	
	disp("1-TO-1 CORRECTION");
        for bin = 1:$nbins
	  Bin = Bins(bin);
	  nCx = length(Bin.Cx);
  	  nCy = length(Bin.Cy);
   	  R0x = Response.R0x(Bin.Bpms,Bin.Cx);
   	  R0y = Response.R0y(Bin.Bpms,Bin.Cy);
	  for i=1:3
	    B0 = placet_get_bpm_readings("$beamlinename", Response.Bpms);
	    B0 -= Response.B0;
   	    B0 = B0(Bin.Bpms,:);
            Cx = -[ R0x ; $beta0 * eye(nCx) ] \ [ B0(:,1) ; zeros(nCx,1) ];
	    Cy = -[ R0y ; $beta0 * eye(nCy) ] \ [ B0(:,2) ; zeros(nCy,1) ];
	    placet_element_vary_attribute("$beamlinename", Response.Cx(Bin.Cx), "strength_x", Cx);
	    placet_element_vary_attribute("$beamlinename", Response.Cy(Bin.Cy), "strength_y", Cy);
	    [E,B] = placet_test_no_correction("$beamlinename", "beam0", "None", 1, 0, Response.Bpms(Bin.Bpms(end)), "%s %E %dE %ex %ey %sz");
	  end
	end

	E1 = E1 .+ E;
	EmittX_hist($machine, 2) = E(end,4);
	EmittY_hist($machine, 2) = E(end,5);

	disp("DFS CORRECTION");
        for bin = 1:$nbins
	  Bin = Bins(bin);
	  nCx = length(Bin.Cx);
  	  nCy = length(Bin.Cy);
   	  R0x = Response.R0x(Bin.Bpms,Bin.Cx);
   	  R0y = Response.R0y(Bin.Bpms,Bin.Cy);
   	  R1x = Response.R1x(Bin.Bpms,Bin.Cx);
   	  R1y = Response.R1y(Bin.Bpms,Bin.Cy);
	  for i=1:3
	    B0 = placet_get_bpm_readings("$beamlinename", Response.Bpms);
	    reduce_energy();
	    placet_test_no_correction("$beamlinename", "beam0", "None", 1, 0, Response.Bpms(Bin.Bpms(end)));
	    reset_energy();
	    B1 = placet_get_bpm_readings("$beamlinename", Response.Bpms) - B0;
	    B0 -= Response.B0;
	    B1 -= Response.B1;
            B0 = B0(Bin.Bpms,:);
            B1 = B1(Bin.Bpms,:);
	    Cx = -[ R0x ; $wgt1 * R1x ; $beta1 * eye(nCx) ] \ [ B0(:,1) ; $wgt1 * B1(:,1) ; zeros(nCx,1) ];
	    Cy = -[ R0y ; $wgt1 * R1y ; $beta1 * eye(nCy) ] \ [ B0(:,2) ; $wgt1 * B1(:,2) ; zeros(nCy,1) ];
	    placet_element_vary_attribute("$beamlinename", Response.Cx(Bin.Cx), "strength_x", Cx);
	    placet_element_vary_attribute("$beamlinename", Response.Cy(Bin.Cy), "strength_y", Cy);
	    [E,B] = placet_test_no_correction("$beamlinename", "beam0", "None", 1, 0, Response.Bpms(Bin.Bpms(end)), "%s %E %dE %ex %ey %sz");
	  end
	end

	E2 = E2 .+ E;
	EmittX_hist($machine, 3) = E(end,4);
	EmittY_hist($machine, 3) = E(end,5);

	disp("WFS CORRECTION");
        for bin = 1:$nbins
	  Bin = Bins(bin);
	  nCx = length(Bin.Cx);
  	  nCy = length(Bin.Cy);
   	  R0x = Response.R0x(Bin.Bpms,Bin.Cx);
   	  R0y = Response.R0y(Bin.Bpms,Bin.Cy);
   	  R1x = Response.R1x(Bin.Bpms,Bin.Cx);
   	  R1y = Response.R1y(Bin.Bpms,Bin.Cy);
   	  R2x = Response.R2x(Bin.Bpms,Bin.Cx);
   	  R2y = Response.R2y(Bin.Bpms,Bin.Cy);
	  for i=1:3
	    B0 = placet_get_bpm_readings("$beamlinename", Response.Bpms);
	    % DFS
	    reduce_energy();
	    placet_test_no_correction("$beamlinename", "beam0", "None", 1, 0, Response.Bpms(Bin.Bpms(end)));
	    reset_energy();
	    B1 = placet_get_bpm_readings("$beamlinename", Response.Bpms) - B0;
            % WFS
	    placet_test_no_correction("$beamlinename", "beam1", "None", 1, 0, Response.Bpms(Bin.Bpms(end)));
	    B2 = placet_get_bpm_readings("$beamlinename", Response.Bpms) - B0;
	    %
	    B0 -= Response.B0;
	    B1 -= Response.B1;
	    B2 -= Response.B2;
            B0 = B0(Bin.Bpms,:);
            B1 = B1(Bin.Bpms,:);
            B2 = B2(Bin.Bpms,:);
	    Cx = -[ R0x ; $wgt1 * R1x ; $wgt2 * R2x ; $beta2 * eye(nCx) ] \ [ B0(:,1) ; $wgt1 * B1(:,1) ; $wgt2 * B2(:,1) ; zeros(nCx,1) ];
	    Cy = -[ R0y ; $wgt1 * R1y ; $wgt2 * R2y ; $beta2 * eye(nCy) ] \ [ B0(:,2) ; $wgt1 * B1(:,2) ; $wgt2 * B2(:,2) ; zeros(nCy,1) ];
	    placet_element_vary_attribute("$beamlinename", Response.Cx(Bin.Cx), "strength_x", Cx);
	    placet_element_vary_attribute("$beamlinename", Response.Cy(Bin.Cy), "strength_y", Cy);
	    [E,B] = placet_test_no_correction("$beamlinename", "beam0", "None", 1, 0, Response.Bpms(Bin.Bpms(end)), "%s %E %dE %ex %ey %sz");
	  end
	end

	E3 = E3 .+ E;
	EmittX_hist($machine, 4) = E(end,4);
	EmittY_hist($machine, 4) = E(end,5);
    }
}

set nm [expr $machine_end - $machine_start + 1]

Octave {
    E0 = E0 / $nm;
    E1 = E1 / $nm;
    E2 = E2 / $nm;
    E3 = E3 / $nm;
    save -text ${beamlinename}_emitt_no_n_${nm}.dat E0
    save -text ${beamlinename}_emitt_simple_b_${beta0}_n_${nm}.dat E1
    save -text ${beamlinename}_emitt_dfs_w_${wgt1}_b_${beta0}_${beta1}_n_${nm}.dat E2
    save -text ${beamlinename}_emitt_wfs_w_${wgt1}_${wgt2}_b_${beta0}_${beta1}_${beta2}_c_${dcharge}_n_${nm}.dat E3
    save -text ${beamlinename}_emittx_hist_w_${wgt1}_${wgt2}_b_${beta0}_${beta1}_${beta2}_c_${dcharge}_n_${nm}.dat EmittX_hist
    save -text ${beamlinename}_emitty_hist_w_${wgt1}_${wgt2}_b_${beta0}_${beta1}_${beta2}_c_${dcharge}_n_${nm}.dat EmittY_hist
}

