
# *****************************************************************************************
# structure parameters
# *****************************************************************************************

set clight  299792458.0
set pi [expr acos(-1.)]


# xband structure
set xfreq      11.9942
set xcell_l    [format " %.6e " [expr $clight/($xfreq*1e9)/3] ]
set xbore_r    3.5e-3
set xdia       [format " %.6e " 10.139e-3 ]
set xgap       [format " %.6e " [expr $xcell_l - 2.0e-3]]
set xno_of_cel 110
set xlambda    [format " %.6e " [expr  $clight/($xfreq*1e9)]]
set xactv_l    [format " %.6e " [expr  $xno_of_cel*$xcell_l ]]
set xtot_l     [format " %.6e " [expr  1.05]]
set xedge_l    [format " %.6e " [expr  0.5*($xtot_l-$xactv_l)]]
set xgrad      65e-3
set xvolt      [expr $xactv_l*$xgrad]
set xlngf      xwake_ldat
set xtrsf      xwake_tdat

array set xband_str   "freq $xfreq aper $xbore_r gap $xgap cell_l $xcell_l grad $xgrad volt $xvolt "
array set xband_str   "dia $xdia n_cell $xno_of_cel active_l $xactv_l edge_l $xedge_l"
array set xband_str   "total_l $xtot_l lambda $xlambda wake_long_file $xlngf wake_tran_file $xtrsf"


Octave {
    xband_str =struct('grad',$xband_str(grad),'freq',$xband_str(freq),'aper',$xband_str(aper), 'gap',$xband_str(gap),'cell_l', $xband_str(cell_l), 'cell_no',$xband_str(n_cell),'total_l',$xband_str(total_l),'active_l',$xband_str(active_l),'wake_long_file','$xband_str(wake_long_file)','wake_tran_file','$xband_str(wake_tran_file)');
}


# kband structure
set kfreq       [expr 3.0*$xfreq]
set kcell_l     [format " %.6e " [expr $clight/($kfreq*1e9)/3.0]]
set kbore_r     2.0e-3
set kdia        [format " %.6e " [expr $xdia/3.0]]
set kgap        [format " %.6e " [expr $kcell_l - 0.6e-3]]
set kno_of_cel  272
set klambda     [format " %.6e " [expr  $clight/($kfreq*1e9)]]
set kactv_l     [format " %.6e " [expr  $kno_of_cel*$kcell_l ]]
set ktot_l      [format " %.6e " [expr  0.8]]
set kedge_l     [format " %.6e " [expr  0.5*($ktot_l-$kactv_l)]]
set kgrad       25e-3
set kvolt       [expr $kactv_l*$kgrad]
set klngf      kwake_ldat
set ktrsf      kwake_tdat

array set kband_str   "freq $kfreq aper $kbore_r gap $kgap cell_l $kcell_l grad $kgrad volt $kvolt "
array set kband_str   "dia $kdia n_cell $kno_of_cel active_l $kactv_l edge_l $kedge_l"
array set kband_str   "total_l $ktot_l lambda $klambda wake_long_file $klngf wake_tran_file $ktrsf"


Octave {
    kband_str =struct('grad',$kband_str(grad),'freq',$kband_str(freq),'aper',$kband_str(aper), 'gap',$kband_str(gap),'cell_l', $kband_str(cell_l), 'cell_no',$kband_str(n_cell),'total_l',$kband_str(total_l),'active_l',$kband_str(active_l),'wake_long_file','$kband_str(wake_long_file)','wake_tran_file','$kband_str(wake_tran_file)');
}




