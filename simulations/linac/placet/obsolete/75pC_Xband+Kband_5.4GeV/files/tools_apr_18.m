1;
% file contains some functions used for manuplation of bunch file ...
% 

%----------------------------------------
%  smoothing 1d vector 3rd order
function smoothout=my_smooth(vector) 
  n=length(vector);
  tmp=[];
  for k=1:5
    tmp(1)=vector(1);
    a1=vector(1);
    a2=vector(2);
    a3=vector(3);
    tmp(2)=(a1+a2+a3)/3;
    
    for i=3:n-2
      sum=0;
      for j=1:5
	sum=sum+vector(i+j-3);
      end;
      tmp(i)=sum/5;
    end;
    a1=vector(n-2);
    a2=vector(n-1);
    a3=vector(n);
    tmp(n-1)=(a1+a2+a3)/3;
    tmp(n)=vector(n);
    vector=tmp;
   end;
  smoothout=vector;
end

%----------------------------------------
%  returns the histogram data for longitudinal coordinat
function hist=beam_histogram(beam,nslice) 
  B=load(beam);
  z=sort(B(:,4));
  minz=z(1);
  maxz=z(end);
  [count zl]=hist(z(2:end-1),nslice-2);
  zl=[minz zl maxz];
  count=[1 count 1];
  count=count./sum(count);
  count=my_smooth(count);
  hist=[zl' count'];   
end



%----------------------------------------
% calculates the variance of a vector
function suu=var1d(u) 
    um=mean(u);
    suu=sum((u-um).^2)/length(u);
endfunction


%----------------------------------------
% calculates the variance of two vector
function suv=var2d(u,v) 
    lu=length(u);
    lv=length(v);

    if (lu != lv)
        suv=0.0;
        return;
    end;

    um=mean(u);
    vm=mean(v);
    suv=sum((u-um).*(v-vm))/lu;
    
end


%----------------------------------------
% calculates the unprojected emittance of two vector
function emt=eps(u,v) 
    lu=length(u);
    lv=length(v);

    if (lu != lv)
        eps=0.0;
        return;
    end;
    
    ruu=var1d(u);
    rvv=var1d(v);
    ruv=var2d(u,v);
    emt=sqrt(ruu*rvv-ruv*ruv);
endfunction


%------------------------------------------------------------------------
%  this function calculates the transverse twiss parameters
function twissout=twisscalcfile(filein) 
   m0c2=0.00051099893;
   B=load(filein);
   [zs,id]=sort(B(:,4));
   B=B(id,:);
   e=B(:,1);
   x=B(:,2);
   y=B(:,3);
   z=B(:,4);
   xp=B(:,5);
   yp=B(:,6);

   e0=mean(e);
   g0=e0/m0c2+1;
   z0=mean(z);
   x0=mean(x);
   y0=mean(y);
   xp0=mean(xp);
   yp0=mean(yp);
   
   sxx=var1d(x);
   sxpxp=var1d(xp);
   sxxp=var2d(x,xp);
   epsx=sqrt(sxx*sxpxp-sxxp*sxxp);
   bx=sxx/epsx;
   ax=-sxxp/epsx;

   syy  =var1d(y);
   sypyp=var1d(yp);
   syyp =var2d(y,yp);
   epsy =sqrt(syy*sypyp-syyp*syyp);
   by   =syy/epsy;
   ay   =-syyp/epsy;

%     outpar=[x0,xp0,ax,bx,epsx,y0,yp0,ay,by,epsy,g0];
 
    pars=struct('x0', x0, 'xp0', xp0, 'alpha_x', ax, 'beta_x', bx, 'emit_x', epsx, 'y0', y0, 'yp0', yp0, 'alpha_y', ay, 'beta_y', by, 'emit_y', epsy);
   
   twissout=pars;
   
end


%------------------------------------------------------------------------
%  this function calculates the transverse twiss parameters
function [x0,xp0,ax,bx,epsx,y0,yp0,ay,by,epsy,g0]=twisscalcbeam(beam) 
   m0c2=0.00051099893;

   B=beam; 
   [zs,id]=sort(B(:,4));
   B=B(id,:);
   e=B(:,1);
   x=B(:,2);
   y=B(:,3);
   z=B(:,4);
   xp=B(:,5);
   yp=B(:,6);

   e0=mean(e);
   g0=e0/m0c2+1;
   z0=mean(z);
   x0=mean(x);
   y0=mean(y);
   xp0=mean(xp);
   yp0=mean(yp);
   
   sxx=var1d(x);
   sxpxp=var1d(xp);
   sxxp=var2d(x,xp);
   epsx=sqrt(sxx*sxpxp-sxxp*sxxp);
   bx=sxx/epsx;
   ax=-sxxp/epsx;

   syy  =var1d(y);
   sypyp=var1d(yp);
   syyp =var2d(y,yp);
   epsy =sqrt(syy*sypyp-syyp*syyp);
   by   =syy/epsy;
   ay   =-syyp/epsy;

   
end





%------------------------------------------------------------------------
%   this function normalizes the bunch by using phase advance at the stored position file_normed.ext
function normout=normalize_bunch(filein,mux,muy,fileout='normed') 
   m0c2=0.51099893e-3;
   B=load(filein);
   [zs,id]=sort(B(:,4));
   B=B(id,:);
   e=B(:,1);
   x=B(:,2);
   y=B(:,3);
   z=B(:,4);
   xp=B(:,5);
   yp=B(:,6);

   e0=mean(e);
   g0=e0/m0c2+1;
   z0=mean(z);
   x0=mean(x)
   y0=mean(y)
   xp0=mean(xp);
   yp0=mean(yp);
   
   sxx=var1d(x);
   sxpxp=var1d(xp);
   sxxp=var2d(x,xp);
   epsx=sqrt(sxx*sxpxp-sxxp*sxxp);
   bx=sxx/epsx
   ax=-sxxp/epsx;
   emtnx=epsx*g0*1e-6
   
   syy  =var1d(y);
   sypyp=var1d(yp);
   syyp =var2d(y,yp);
   epsy =sqrt(syy*sypyp-syyp*syyp);
   by   =syy/epsy
   ay   =-syyp/epsy;
   emtny=epsy*g0*1e-6
      
   gi=e/m0c2+1;
   
   txn=1.0 / sqrt(bx) * sqrt(gi) .* x  ;
   txpn= ax * txn + sqrt(bx) * sqrt(gi) .*  xp;    
      
   tyn=1.0/ sqrt(by) * sqrt(gi) .* y  ;
   typn= ay * tyn + sqrt(by) * sqrt(gi) .* yp;
   
   
   rx=sqrt(txn.*txn+txpn.*txpn);
   ry=sqrt(tyn.*tyn+typn.*typn);
    
   rx0=mean(rx)
   ry0=mean(ry)   
   
   tetx= atan2(txpn,txn) + mux;
   tety= atan2(typn,tyn) + muy;
   


   xn =rx.*cos(tetx);
   xpn=rx.*sin(tetx);
   
   yn =ry.*cos(tety);
   ypn=ry.*sin(tety);
    
   outdata0=[e,txn,tyn,z,txpn,typn];  
   outdata=[e,xn,yn,z,xpn,ypn];
   
   
   filecase=strcmp(fileout,'normed');

   if filecase >0 
    s=filein;
	npo=findstr(s,".");
	ext=s(1,(npo:end));
	name=s(1,(1:npo-1));
	fout=[name "_norm"];
	fout=[fout ext];
   else
    fout=fileout;
   end
   
   outfile0=sprintf('%s.%s',fout,"0");
   outfile=sprintf('%s',fout);
   
	fprintf('normalized_bunch outdata is %s\n\n',outfile);
%  	fid=fopen(fout,"wt");
%  	fprintf(fid,'%20.12e %20.12e %20.12e %20.12e %20.12e %20.12e\n',outdata');
%  	fclose(fid);

%      save -text -ascii outfile outdata;
    save("-text", "-ascii",outfile0,"outdata0");
    save("-text", "-ascii",outfile,"outdata");
   
   
end






%------------------------------------------------------------------------
%   this function normalizes the bunch by using default bunches
function normout=normalize_in_out2(nslice,beam0_in,beam0_ou,beam_in,beam_ou,out_n_in,out_n_ou ) 
   m0c2=0.51099893e-3;
   
%   initial reference bunch   
   B0in=load(beam0_in);
   [z0in,id0in]=sort(B0in(:,4));
   z0instp=unique(z0in,'first');
   B0in=B0in(id0in,:);
   
%     minz0in=min(B0in(:,4));
%     maxz0in=max(B0in(:,4));
%     dz0in=(maxz0in-minz0in)/nslice;
%     z0instp  =linspace(minz0in-dz0in*0.5,maxz0in+dz0in*0.5,nslice);
   
%   exit reference bunch   
   B0ou=load(beam0_ou);
   [z0ou,id0ou]=sort(B0ou(:,4));
   z0oustp=unique(z0ou,'first');
   B0ou=B0ou(id0ou,:);
%     minz0ou=min(B0ou(:,4));
%     maxz0ou=max(B0ou(:,4));
%     dz0ou=(maxz0ou-minz0ou)/nslice;
%     z0oustp  =linspace(minz0ou-dz0ou*0.5,maxz0ou+dz0ou*0.5,nslice);  
 
%   initial bunch for normalization
   Bin=load(beam_in);
   [zin,idin]=sort(Bin(:,4));
   zinstp=unique(zin,'first');
   Bin=Bin(idin,:);
%     minzin=min(Bin(:,4));
%     maxzin=max(Bin(:,4));
%     dzin=(maxzin-minzin)/nslice;
%     zinstp  =linspace(minzin-dzin*0.5,maxzin+dzin*0.5,nslice);  
   
%   exit  bunch  for normalization 
   Bou=load(beam_ou);
   [zou,idou]=sort(Bou(:,4));
   zoustp=unique(zou,'first');
   Bou=Bou(idou,:);
%     minzou=min(Bou(:,4));
%     maxzou=max(Bou(:,4));
%     dzou=(maxzou-minzou)/nslice;
%     zoustp  =linspace(minzou-dzou*.5,maxzou+dzou*.5,nslice);  
   
   [x00_in,xp00_in,ax0_in,bx0_in,epsx0_in,y00_in,yp00_in,ay0_in,by0_in,epsy0_in,g00_in]=twisscalcbeam(B0in);
   [x00_ou,xp00_ou,ax0_ou,bx0_ou,epsx0_ou,y00_ou,yp00_ou,ay0_ou,by0_ou,epsy0_ou,g00_ou]=twisscalcbeam(B0ou);

%     printf('%f %f %f %f %f %f\n', bx0_in, by0_in, ax0_in, ay0_in, epsx0_in, epsy0_in);
%     printf('%f %f %f %f %f %f\n', bx0_ou, by0_ou, ax0_ou, ay0_ou, epsx0_ou, epsy0_ou);
   
   
   
    resultin=[];
    resultou=[];
%  
    for j=1:nslice-1
%          
%  
%          
        indxin0j  = find( z0in == z0instp(j));
        indxou0j  = find( z0ou == z0oustp(j));
        indxinj   = find( zin  == zinstp(j));
        indxouj   = find( zou  == zoustp(j));

        kin0_srt=indxin0j(1);
        kin0_end=indxin0j(end);
        kou0_srt=indxou0j(1);
        kou0_end=indxou0j(end);
    
        kin_srt=indxinj(1);
        kin_end=indxinj(end);
        kou_srt=indxouj(1);
        kou_end=indxouj(end);
    
        B0in_j=B0in(kin0_srt:kin0_end,:);
        B0ou_j=B0ou(kou0_srt:kou0_end,:);    
        
        Bin_j=Bin(kin_srt:kin_end,:);
        Bou_j=Bou(kou_srt:kou_end,:);  
        
%          [sl_xin_j,sl_xpin_j,sl_alxin_j,sl_btxin_j,sl_epsxin_j,sl_yin_j,sl_ypin_j,sl_alyin_j,sl_btyin_j,sl_epsyin_j,sl_gin_j]=twisscalcbeam(B0in_j);
%          [sl_xou_j,sl_xpou_j,sl_alxou_j,sl_btxou_j,sl_epsxou_j,sl_you_j,sl_ypou_j,sl_alyou_j,sl_btyou_j,sl_epsyou_j,sl_gou_j]=twisscalcbeam(B0ou_j);


        sl_btxin_j=bx0_in;
        sl_alxin_j=ax0_in;
        sl_btyin_j=by0_in;
        sl_alyin_j=ay0_in;
        sl_gin_j=g00_in;
        
        sl_btxou_j=bx0_ou;
        sl_alxou_j=ax0_ou;
        sl_btyou_j=by0_ou;
        sl_alyou_j=ay0_ou;
        sl_gou_j=g00_ou;
        
        
%  %          printf ('betax  in out = %f <> %f <> %f \n', sl_btxin_j,sl_btxou_j, sl_btxin_j-sl_btxou_j);
%  %          printf ('betay  in out = %f <> %f <> %f \n', sl_btyin_j,sl_btyou_j, sl_btyin_j-sl_btyou_j);
%  %          printf ('alphax in out = %f <> %f <> %f \n', sl_alxin_j,sl_alxou_j, sl_alxin_j-sl_alxou_j);
%  %          printf ('alphay in out = %f <> %f <> %f \n', sl_alyin_j,sl_alyou_j, sl_alyin_j-sl_alyou_j);  
        
%  %    initial bunch
        ein_j=mean(Bin_j(:,1));
        gin_j=ein_j/m0c2+1;
        xin_j=mean(Bin_j(:,2));
        yin_j=mean(Bin_j(:,3));
        zin_j=mean(Bin_j(:,4));
        xpin_j=mean(Bin_j(:,5));
        ypin_j=mean(Bin_j(:,6));
        
%  %    exit bunch
        eou_j=mean(Bou_j(:,1));
        gou_j=eou_j/m0c2+1;
        xou_j=mean(Bou_j(:,2));
        you_j=mean(Bou_j(:,3));
        zou_j=mean(Bou_j(:,4));
        xpou_j=mean(Bou_j(:,5));
        ypou_j=mean(Bou_j(:,6));
        
 % %    initial bunch slice normalize       
                
        xin_jn = sqrt(sl_gin_j/sl_btxin_j)*xin_j;
        xpin_jn= xin_jn*sl_alxin_j + sqrt(sl_gin_j*sl_btxin_j)*xpin_j;
        
        yin_jn = sqrt(sl_gin_j/sl_btyin_j)*yin_j;
        ypin_jn= yin_jn*sl_alyin_j + sqrt(sl_gin_j*sl_btyin_j)*ypin_j;
        
        rxin_jn=sqrt(xin_jn.*xin_jn+xpin_jn.*xpin_jn);
        ryin_jn=sqrt(yin_jn.*yin_jn+ypin_jn.*ypin_jn);
        
        muxin_jn=atan2(xpin_jn,xin_jn);
        muyin_jn=atan2(ypin_jn,yin_jn);  
%          
 % %    exit bunch slice normalize       
                
        xou_jn = sqrt(sl_gou_j/sl_btxou_j)*xou_j;
        xpou_jn= xou_jn*sl_alxou_j + sqrt(sl_gou_j*sl_btxou_j)*xpou_j;
        
        you_jn = sqrt(sl_gou_j/sl_btyou_j)*you_j;
        ypou_jn= you_jn*sl_alyou_j + sqrt(sl_gou_j*sl_btyou_j)*ypou_j;
        
        rxou_jn=sqrt(xou_jn.*xou_jn+xpou_jn.*xpou_jn);
        ryou_jn=sqrt(you_jn.*you_jn+ypou_jn.*ypou_jn);
        
        muxou_jn=atan2(xpou_jn,xou_jn);
        muyou_jn=atan2(ypou_jn,you_jn);    
%          
        if (j==1)
            muxin_jn0=mean(muxin_jn);
            muyin_jn0=mean(muyin_jn);
            muxou_jn0=mean(muxou_jn);
            muyou_jn0=mean(muyou_jn);
        
            rxin_jn0=mean(rxin_jn);
            ryin_jn0=mean(ryin_jn);
            rxou_jn0=mean(rxou_jn);
            ryou_jn0=mean(ryou_jn);          

        end
        
        dmuxin_jn=muxin_jn-muxin_jn0;
        dmuyin_jn=muyin_jn-muyin_jn0;
        
        dmuxou_jn=muxou_jn-muxou_jn0;
        dmuyou_jn=muyou_jn-muyou_jn0;
        
        xnin  = rxin_jn.*cos(dmuxin_jn)/rxin_jn0;
        xpnin = rxin_jn.*sin(dmuxin_jn)/rxin_jn0;
        
        ynin  = ryin_jn.*cos(dmuyin_jn)/ryin_jn0;
        ypnin = ryin_jn.*sin(dmuyin_jn)/ryin_jn0;
        
        xnou  = rxou_jn.*cos(dmuxou_jn)/rxin_jn0;
        xpnou = rxou_jn.*sin(dmuxou_jn)/rxin_jn0;

        ynou  = ryou_jn.*cos(dmuyou_jn)/ryin_jn0;
        ypnou = ryou_jn.*sin(dmuyou_jn)/ryin_jn0;
        
        resultin=[resultin;ein_j,xnin,ynin,zin_j,xpnin,ypnin,rxin_jn/rxin_jn0,ryin_jn/ryin_jn0];
        resultou=[resultou;eou_j,xnou,ynou,zou_j,xpnou,ypnou,rxou_jn/rxin_jn0,ryou_jn/ryin_jn0];
%          
               
        
    end

        save("-text", "-ascii",out_n_in,"resultin");
        save("-text", "-ascii",out_n_ou,"resultou");


%      
%      resultin(:,2)=my_smooth(resultin(:,2));
%      resultou(:,2)=my_smooth(resultou(:,2));
%      resultin(:,3)=my_smooth(resultin(:,3));
%      resultou(:,3)=my_smooth(resultou(:,3));
%      resultin(:,5)=my_smooth(resultin(:,5));
%      resultou(:,5)=my_smooth(resultou(:,5));
%      resultin(:,6)=my_smooth(resultin(:,6));
%      resultou(:,6)=my_smooth(resultou(:,6));
%      resultin(:,7)=my_smooth(resultin(:,7));
%      resultou(:,7)=my_smooth(resultou(:,7));
%      resultin(:,8)=my_smooth(resultin(:,8));
%      resultou(:,8)=my_smooth(resultou(:,8));    
%      
%  
%          save("-text", "-ascii",out_n_in,"resultin");
%          save("-text", "-ascii",out_n_ou,"resultou");
endfunction





%------------------------------------------------------------------------
%   this function normalizes the bunch by using phase advance at the stored position 
function normout=normalize_in_out3(nslice,beam0_in,beam0_ou,beam_in,beam_ou,out_n_in,out_n_ou ) 
   m0c2=0.51099893e-3;
   
%   initial reference bunch   
   Bin0=load(beam0_in);
%     [zin0,id0in]=sort(Bin0(:,4));
%     Bin0=Bin0(id0in,:);
   [mxin0,mxpin0,malxin0,mbtxin0,mepsxin0,myin0,mypin0,malyin0,mbtyin0,mepsyin0,mgin0]=twisscalcbeam(Bin0);
   ein0=Bin0(:,1);
   gin0=ein0/m0c2+1;
   xin0=Bin0(:,2);
   yin0=Bin0(:,3);
   zin0=Bin0(:,4);
   xpin0=Bin0(:,5);
   ypin0=Bin0(:,6);
   
%   exit reference bunch   
   Bou0=load(beam0_ou);
%     [zou0,id0ou]=sort(Bou0(:,4));
%     Bou0=Bou0(id0ou,:);
   [mxou0,mxpou0,malxou0,mbtxou0,mepsxou0,myou0,mypou0,malyou0,mbtyou0,mepsyou0,mgou0]=twisscalcbeam(Bou0);
   eou0=Bou0(:,1);
   gou0=eou0/m0c2+1;
   xou0=Bou0(:,2);
   you0=Bou0(:,3);
   zou0=Bou0(:,4);
   xpou0=Bou0(:,5);
   ypou0=Bou0(:,6);
 
%   initial bunch  for normalization
   Bin=load(beam_in);
%     [zin,idin]=sort(Bin(:,4));
%     Bin=Bin(idin,:);
   ein=Bin(:,1);
   gin=ein/m0c2+1;
   xin=Bin(:,2);
   yin=Bin(:,3);
   zin=Bin(:,4);
   xpin=Bin(:,5);
   ypin=Bin(:,6);
   
%   exit  bunch  for normalization
   Bou=load(beam_ou);
%     [zou,idou]=sort(Bou(:,4));
%     Bou=Bou(idou,:);
   eou=Bou(:,1);
   gou=eou/m0c2+1;
   xou=Bou(:,2);
   you=Bou(:,3);
   zou=Bou(:,4);
   xpou=Bou(:,5);
   ypou=Bou(:,6);
    
   
   xin_n = sqrt(gin./mbtxin0).*xin;
   xpin_n=  xin_n.*malxin0 + sqrt(gin.*mbtxin0).*xpin;
   rxin=sqrt(xin_n.^2+xpin_n.^2);  
   dtetxin=atan2(xpin_n(1),xin_n(1));
   tetxin=atan2(xpin_n,xin_n);
   xin_n=cos(tetxin-dtetxin);
   xpin_n=sin(tetxin-dtetxin);
   
   yin_n = sqrt(gin./mbtyin0).*yin;
   ypin_n=  yin_n.*malyin0 + sqrt(gin.*mbtyin0).*ypin;
   
   xou_n = sqrt(gou./mbtxou0).*xou;
   xpou_n=  xou_n.*malxou0 + sqrt(gou.*mbtxou0).*xpou;
   rxou=sqrt(xou_n.^2+xpou_n.^2);
   tetxou=atan2(xpou_n,xou_n);
%  tetxou0=atan2(xpou_n(1),xou_n(1));
%  dtetx=atan2(xpou_n(1),xou_n(1))-atan2(xpin_n(1),xin_n(1));
   dtetxou=atan2(xpou_n(1),xou_n(1));
   
%     dtetx=tetxou-tetxin;
   
   xou_n=cos(tetxou-dtetxou).*rxou./rxin;
   xpou_n=sin(tetxou-dtetxou).*rxou./rxin;
   
   
   
   you_n = sqrt(gou./mbtyou0).*you;
   ypou_n=  you_n.*malyou0 + sqrt(gou.*mbtyou0).*ypou;
   
   Bin_n=[ein xin_n yin_n zin xpin_n ypin_n];
   Bou_n=[eou xou_n you_n zou xpou_n ypou_n];
   
    save("-text", "-ascii",out_n_in,"Bin_n");
    save("-text", "-ascii",out_n_ou,"Bou_n");
   


endfunction

