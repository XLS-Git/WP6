Girder
SetReferenceEnergy  $e0
#   warnign CHARGE with name BNCH is unknown element 
Drift      -name    "INJ_WA_OU" -length 0 
Drift      -name    "LN0_DR_V1" -length 0.52556389031 
Drift      -name    "LN0_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN0_QD_F_V01"  -length 0.15  -strength [expr 0.15*$e0*8.13469924498] -e0 $e0
Drift      -name    "LN0_DR_QD_ED" -length 0.05 
Drift      -name    "LN0_DR_V2" -length 0.535997067562 
Drift      -name    "LN0_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN0_QD_D_V02"  -length 0.15  -strength [expr 0.15*$e0*-8.55164706719] -e0 $e0
Drift      -name    "LN0_DR_QD_ED" -length 0.05 
Drift      -name    "LN0_DR_V3" -length 0.776180699293 
Drift      -name    "LN0_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN0_QD_F_V03"  -length 0.15  -strength [expr 0.15*$e0*6.46011761467] -e0 $e0
Drift      -name    "LN0_DR_QD_ED" -length 0.05 
Drift      -name    "LN0_DR_V4" -length 0.200037079595 
Drift      -name    "LN0_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN0_QD_D_V04"  -length 0.15  -strength [expr 0.15*$e0*-1.24510750441] -e0 $e0
Drift      -name    "LN0_DR_QD_ED" -length 0.05 
Drift      -name    "LN0_DR_V5" -length 1.90119087858 
Drift      -name    "LN0_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN0_QD_F0"  -length 0.08  -strength [expr 0.08*$e0*7] -e0 $e0
Drift      -name    "LN0_DR_QD_ED" -length 0.05 
Drift      -name    "LN0_DR_WP" -length 0.1 
Drift      -name    "LN0_DR_FL" -length 0.025 
Drift      -name    "LN0_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN0_XCA0" -length 0.9164755  -gradient $LN0_XCA_GRD -phase $LN0_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN0_XCA_GRD*cos(3.14159265359*$LN0_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN0_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN0_DR_FL" -length 0.025 
Drift      -name    "LN0_DR_FL" -length 0.025 
Drift      -name    "LN0_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN0_XCA0" -length 0.9164755  -gradient $LN0_XCA_GRD -phase $LN0_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN0_XCA_GRD*cos(3.14159265359*$LN0_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN0_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN0_DR_FL" -length 0.025 
Drift      -name    "LN0_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN0_QD_D0"  -length 0.08  -strength [expr 0.08*$e0*-7] -e0 $e0
Drift      -name    "LN0_DR_QD_ED" -length 0.05 
Drift      -name    "LN0_DR_WP" -length 0.1 
Drift      -name    "LN0_DR_FL" -length 0.025 
Drift      -name    "LN0_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN0_XCA0" -length 0.9164755  -gradient $LN0_XCA_GRD -phase $LN0_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN0_XCA_GRD*cos(3.14159265359*$LN0_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN0_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN0_DR_FL" -length 0.025 
Drift      -name    "LN0_DR_FL" -length 0.025 
Drift      -name    "LN0_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN0_XCA0" -length 0.9164755  -gradient $LN0_XCA_GRD -phase $LN0_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN0_XCA_GRD*cos(3.14159265359*$LN0_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN0_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN0_DR_FL" -length 0.025 
Drift      -name    "LN0_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN0_QD_F0"  -length 0.08  -strength [expr 0.08*$e0*7] -e0 $e0
Drift      -name    "LN0_DR_QD_ED" -length 0.05 
Drift      -name    "LN0_WA_OU_LNZ_IN" -length 0 
Drift      -name    "LN0_DR_WP" -length 0.1 
Drift      -name    "LN0_DR_KCA_ED2" -length 0.125 
Drift      -name    "LN0_DR_FL" -length 0.025 
Drift      -name    "LN0_DR_KCA_ED" -length 0.02230105 
Cavity     -name    "LN0_KCA0" -length 0.7553979  -gradient $LN0_KCA_GRD -phase $LN0_KCA_PHS 
set e0      [expr $e0 + 0.7553979*$LN0_KCA_GRD*cos(3.14159265359*$LN0_KCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN0_DR_KCA_ED" -length 0.02230105 
Drift      -name    "LN0_DR_FL" -length 0.025 
Drift      -name    "LN0_DR_KCA_ED2" -length 0.125 
Drift      -name    "LN0_DR_KCA_ED2" -length 0.125 
Drift      -name    "LN0_DR_FL" -length 0.025 
Drift      -name    "LN0_DR_KCA_ED" -length 0.02230105 
Cavity     -name    "LN0_KCA0" -length 0.7553979  -gradient $LN0_KCA_GRD -phase $LN0_KCA_PHS 
set e0      [expr $e0 + 0.7553979*$LN0_KCA_GRD*cos(3.14159265359*$LN0_KCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN0_DR_KCA_ED" -length 0.02230105 
Drift      -name    "LN0_DR_FL" -length 0.025 
Drift      -name    "LN0_DR_KCA_ED2" -length 0.125 
Drift      -name    "LN0_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN0_QD_D0"  -length 0.08  -strength [expr 0.08*$e0*-7] -e0 $e0
Drift      -name    "LN0_DR_QD_ED" -length 0.05 
Drift      -name    "LN0_WA_OU" -length 0 
Drift      -name    "BC1_D_V1" -length 1.53608722936 
Quadrupole -name    "BC1_Q_V1"  -length 0.15  -strength [expr 0.15*$e0*5.4610830135] -e0 $e0
Drift      -name    "BC1_D_V2" -length 3.39008089793 
Quadrupole -name    "BC1_Q_V2"  -length 0.15  -strength [expr 0.15*$e0*-8.07849191282] -e0 $e0
Drift      -name    "BC1_D_V3" -length 0.124499550959 
Quadrupole -name    "BC1_Q_V3"  -length 0.15  -strength [expr 0.15*$e0*8.52120422661] -e0 $e0
Drift      -name    "BC1_D_V4" -length 0.117041549323 
Drift      -name    "BC1_BC_D_20" -length 0.5 
Sbend      -name    "BC1_BC_DIP1" -length 0.30007107  -angle 0.03769911  -E1 0 -E2 0.03769911 -hgap 0 -fint 0.5 -tilt 0 -e0 $e0 -six_dim 1 
Drift      -name    "BC1_BC_D_SIDE" -length 3.0021331 
Sbend      -name    "BC1_BC_DIP2" -length 0.30007107  -angle -0.03769911  -E1 -0.03769911 -E2 0 -hgap 0 -fint 0.5 -tilt 0 -e0 $e0 -six_dim 1 
Drift      -name    "BC1_BC_D_CENT" -length 1 
Sbend      -name    "BC1_BC_DIP3" -length 0.30007107  -angle -0.03769911  -E1 0 -E2 -0.03769911 -hgap 0 -fint 0.5 -tilt 0 -e0 $e0 -six_dim 1 
Drift      -name    "BC1_BC_D_SIDE" -length 3.0021331 
Sbend      -name    "BC1_BC_DIP4" -length 0.30007107  -angle 0.03769911  -E1 0.03769911 -E2 0 -hgap 0 -fint 0.5 -tilt 0 -e0 $e0 -six_dim 1 
Drift      -name    "BC1_BC_D_20" -length 0.5 
Drift      -name    "BC1_WA_OU" -length 0 
Drift      -name    "LN1_DR_V1" -length 1.65046193093 
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN1_QD_F_V01"  -length 0.15  -strength [expr 0.15*$e0*8.03976715281] -e0 $e0
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Drift      -name    "LN1_DR_V2" -length 0.419380781516 
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN1_QD_D_V02"  -length 0.15  -strength [expr 0.15*$e0*-9.93448822638] -e0 $e0
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Drift      -name    "LN1_DR_V3" -length 0.605367446412 
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN1_QD_F_V03"  -length 0.15  -strength [expr 0.15*$e0*12.897472169] -e0 $e0
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Drift      -name    "LN1_DR_V4" -length 0.585387141999 
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN1_QD_D_V04"  -length 0.15  -strength [expr 0.15*$e0*-1.81558879443] -e0 $e0
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Drift      -name    "LN1_DR_V5" -length 1.76199229131 
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN1_QD_F0"  -length 0.08  -strength [expr 0.08*$e0*6.6] -e0 $e0
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Drift      -name    "LN1_DR_WP" -length 0.1 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN1_QD_D0"  -length 0.08  -strength [expr 0.08*$e0*-6.6] -e0 $e0
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Drift      -name    "LN1_DR_WP" -length 0.1 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN1_QD_F0"  -length 0.08  -strength [expr 0.08*$e0*6.6] -e0 $e0
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Drift      -name    "LN1_DR_WP" -length 0.1 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN1_QD_D0"  -length 0.08  -strength [expr 0.08*$e0*-6.6] -e0 $e0
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Drift      -name    "LN1_DR_WP" -length 0.1 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN1_QD_F0"  -length 0.08  -strength [expr 0.08*$e0*6.6] -e0 $e0
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Drift      -name    "LN1_DR_WP" -length 0.1 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN1_QD_D0"  -length 0.08  -strength [expr 0.08*$e0*-6.6] -e0 $e0
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Drift      -name    "LN1_DR_WP" -length 0.1 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN1_QD_F0"  -length 0.08  -strength [expr 0.08*$e0*6.6] -e0 $e0
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Drift      -name    "LN1_DR_WP" -length 0.1 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN1_QD_D0"  -length 0.08  -strength [expr 0.08*$e0*-6.6] -e0 $e0
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Drift      -name    "LN1_DR_WP" -length 0.1 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN1_DR_FL" -length 0.025 
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN1_QD_F0"  -length 0.08  -strength [expr 0.08*$e0*6.6] -e0 $e0
Drift      -name    "LN1_DR_QD_ED" -length 0.05 
Drift      -name    "LN1_WA_OU" -length 0 
Drift      -name    "BC2_D_V1" -length 0.294643845041 
Quadrupole -name    "BC2_Q_V1"  -length 0.15  -strength [expr 0.15*$e0*2.04106247144] -e0 $e0
Drift      -name    "BC2_D_V2" -length 2.93021349655 
Quadrupole -name    "BC2_Q_V2"  -length 0.15  -strength [expr 0.15*$e0*-8.82729093385] -e0 $e0
Drift      -name    "BC2_D_V3" -length 0.117696608419 
Quadrupole -name    "BC2_Q_V3"  -length 0.15  -strength [expr 0.15*$e0*9.18657020222] -e0 $e0
Drift      -name    "BC2_D_V4" -length 0.112611050866 
Drift      -name    "BC2_BC_D_20" -length 0.5 
Sbend      -name    "BC2_BC_DIP1" -length 0.30002985  -angle 0.02443461  -E1 0 -E2 0.02443461 -hgap 0 -fint 0.5 -tilt 0 -e0 $e0 -six_dim 1 
Drift      -name    "BC2_BC_D_SIDE" -length 3.0008958 
Sbend      -name    "BC2_BC_DIP2" -length 0.30002985  -angle -0.02443461  -E1 -0.02443461 -E2 0 -hgap 0 -fint 0.5 -tilt 0 -e0 $e0 -six_dim 1 
Drift      -name    "BC2_BC_D_CENT" -length 1 
Sbend      -name    "BC2_BC_DIP3" -length 0.30002985  -angle -0.02443461  -E1 0 -E2 -0.02443461 -hgap 0 -fint 0.5 -tilt 0 -e0 $e0 -six_dim 1 
Drift      -name    "BC2_BC_D_SIDE" -length 3.0008958 
Sbend      -name    "BC2_BC_DIP4" -length 0.30002985  -angle 0.02443461  -E1 0.02443461 -E2 0 -hgap 0 -fint 0.5 -tilt 0 -e0 $e0 -six_dim 1 
Drift      -name    "BC2_BC_D_20" -length 0.5 
Drift      -name    "BC2_WA_OU" -length 0 
Drift      -name    "LN2_DR_V1" -length 0.282346719679 
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN2_QD_F_V01"  -length 0.15  -strength [expr 0.15*$e0*8.29899082868] -e0 $e0
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Drift      -name    "LN2_DR_V2" -length 0.439556748509 
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN2_QD_D_V02"  -length 0.15  -strength [expr 0.15*$e0*-10.2452881858] -e0 $e0
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Drift      -name    "LN2_DR_V3" -length 0.514610215705 
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN2_QD_F_V03"  -length 0.15  -strength [expr 0.15*$e0*12.2142217393] -e0 $e0
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Drift      -name    "LN2_DR_V4" -length 1.34652620527 
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN2_QD_D_V04"  -length 0.15  -strength [expr 0.15*$e0*-2.06747059442] -e0 $e0
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Drift      -name    "LN2_DR_V5" -length 1.89647871396 
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN2_QD_F0"  -length 0.08  -strength [expr 0.08*$e0*6] -e0 $e0
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Drift      -name    "LN2_DR_WP" -length 0.1 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN2_QD_D0"  -length 0.08  -strength [expr 0.08*$e0*-6] -e0 $e0
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Drift      -name    "LN2_DR_WP" -length 0.1 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN2_QD_F0"  -length 0.08  -strength [expr 0.08*$e0*6] -e0 $e0
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Drift      -name    "LN2_DR_WP" -length 0.1 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN2_QD_D0"  -length 0.08  -strength [expr 0.08*$e0*-6] -e0 $e0
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Drift      -name    "LN2_DR_WP" -length 0.1 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN2_QD_F0"  -length 0.08  -strength [expr 0.08*$e0*6] -e0 $e0
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Drift      -name    "LN2_DR_WP" -length 0.1 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN2_QD_D0"  -length 0.08  -strength [expr 0.08*$e0*-6] -e0 $e0
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Drift      -name    "LN2_DR_WP" -length 0.1 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN2_QD_F0"  -length 0.08  -strength [expr 0.08*$e0*6] -e0 $e0
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Drift      -name    "LN2_DR_WP" -length 0.1 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN2_QD_D0"  -length 0.08  -strength [expr 0.08*$e0*-6] -e0 $e0
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Drift      -name    "LN2_DR_WP" -length 0.1 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN2_QD_F0"  -length 0.08  -strength [expr 0.08*$e0*6] -e0 $e0
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Drift      -name    "LN2_DR_WP" -length 0.1 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN2_QD_D0"  -length 0.08  -strength [expr 0.08*$e0*-6] -e0 $e0
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Drift      -name    "LN2_DR_WP" -length 0.1 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN2_QD_F0"  -length 0.08  -strength [expr 0.08*$e0*6] -e0 $e0
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Drift      -name    "LN2_DR_WP" -length 0.1 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN2_QD_D0"  -length 0.08  -strength [expr 0.08*$e0*-6] -e0 $e0
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Drift      -name    "LN2_DR_WP" -length 0.1 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN2_QD_F0"  -length 0.08  -strength [expr 0.08*$e0*6] -e0 $e0
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Drift      -name    "LN2_DR_WP" -length 0.1 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN2_QD_D0"  -length 0.08  -strength [expr 0.08*$e0*-6] -e0 $e0
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Drift      -name    "LN2_DR_WP" -length 0.1 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN2_QD_F0"  -length 0.08  -strength [expr 0.08*$e0*6] -e0 $e0
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Drift      -name    "LN2_DR_WP" -length 0.1 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN2_QD_D0"  -length 0.08  -strength [expr 0.08*$e0*-6] -e0 $e0
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Drift      -name    "LN2_DR_WP" -length 0.1 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN2_QD_F0"  -length 0.08  -strength [expr 0.08*$e0*6] -e0 $e0
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Drift      -name    "LN2_DR_WP" -length 0.1 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN2_QD_D0"  -length 0.08  -strength [expr 0.08*$e0*-6] -e0 $e0
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Drift      -name    "LN2_DR_WP" -length 0.1 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN2_QD_F0"  -length 0.08  -strength [expr 0.08*$e0*6] -e0 $e0
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Drift      -name    "LN2_DR_WP" -length 0.1 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN2_QD_D0"  -length 0.08  -strength [expr 0.08*$e0*-6] -e0 $e0
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Drift      -name    "LN2_DR_WP" -length 0.1 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN2_QD_F0"  -length 0.08  -strength [expr 0.08*$e0*6] -e0 $e0
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Drift      -name    "LN2_DR_WP" -length 0.1 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN2_QD_D0"  -length 0.08  -strength [expr 0.08*$e0*-6] -e0 $e0
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Drift      -name    "LN2_DR_WP" -length 0.1 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN2_QD_F0"  -length 0.08  -strength [expr 0.08*$e0*6] -e0 $e0
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Drift      -name    "LN2_DR_WP" -length 0.1 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN2_QD_D0"  -length 0.08  -strength [expr 0.08*$e0*-6] -e0 $e0
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Drift      -name    "LN2_DR_WP" -length 0.1 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN2_QD_F0"  -length 0.08  -strength [expr 0.08*$e0*6] -e0 $e0
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Drift      -name    "LN2_DR_WP" -length 0.1 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN2_QD_D0"  -length 0.08  -strength [expr 0.08*$e0*-6] -e0 $e0
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Drift      -name    "LN2_DR_WP" -length 0.1 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS 
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN2_QD_F0"  -length 0.08  -strength [expr 0.08*$e0*6] -e0 $e0
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Drift      -name    "LN2_DR_WP" -length 0.1 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN2_DR_XCA_" -length 0.9164755 
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN2_DR_XCA_" -length 0.9164755 
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN2_DR_XCA_" -length 0.9164755 
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN2_QD_D0"  -length 0.08  -strength [expr 0.08*$e0*-6] -e0 $e0
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Drift      -name    "LN2_DR_WP" -length 0.1 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN2_DR_XCA_" -length 0.9164755 
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN2_DR_XCA_" -length 0.9164755 
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN2_DR_XCA_" -length 0.9164755 
Drift      -name    "LN2_DR_XCA_ED" -length 0.06676225 
Drift      -name    "LN2_DR_FL" -length 0.025 
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Quadrupole -name    "LN2_QD_F0"  -length 0.08  -strength [expr 0.08*$e0*6] -e0 $e0
Drift      -name    "LN2_DR_QD_ED" -length 0.05 
Drift      -name    "LN2_WA_OU" -length 0 
