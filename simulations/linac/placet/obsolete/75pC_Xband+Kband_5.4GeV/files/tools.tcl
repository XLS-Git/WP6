#########################################################################################
#  some constands
#----------------------------------------------------------------------------------------
set clight 2.9979246e+08
set Z0 376.99112
set pi [expr acos(-1.)]
set mc2 0.51099893e-3


#########################################################################################
#  loading required libraries 
#----------------------------------------------------------------------------------------
package require math::statistics
package require math::fourier 
package require math::bigfloat
package require  math::special
package require Tcl 
package require struct
package require math::interpolate 

namespace import ::math::bigfloat::*




#########################################################################################
# procedure for normal (Gaussian) distribution with mean zero and standard deviation 1.0
#----------------------------------------------------------------------------------------
proc random {} {
     variable last
     if { [info exists last] } {
		  set retval $last
		  unset last
		  return $retval
     }
     set v1 [expr  2. * rand() - 1 ]
     set v2 [expr  2. * rand() - 1 ]
     set rsq [expr  $v1*$v1 + $v2*$v2 ]
     while { $rsq > 1. } {
		  set v1 [expr   2. * rand() - 1  ]
		  set v2 [expr   2. * rand() - 1  ]
		  set rsq [expr   $v1*$v1 + $v2*$v2  ]
     }
     set fac [expr   sqrt( -2. * log( $rsq ) / $rsq )  ]
     set last [expr   $v1 * $fac  ]
     return [expr   $v2 * $fac  ]
 } 
 

proc randomuni {} {
   return [expr -1.+ 2.*rand()]
}


 
#########################################################################################
# procedure for generating N real number with standard deviation of mu and mean sigma 
#----------------------------------------------------------------------------------------
proc randomN { args } {
	 
	 array set params [regsub -all "=" $args " "]
	 	 
	 if {[info exist params(n)]<1} {
		 puts "you have not given numbers default is taken to :=> n=1"
		 set n 1
	  } else {
		 set n $params(n)
	  }
	  
	 if {[info exist params(mu)]<1} {
		 puts "you have not given a standard deviation default is taken to :=> mu=0"
		 set mu 0.0
	  } else {
		 set mu $params(mu)
	  }
	  
	 if {[info exist params(sigma)]<1} {
		 puts "you have not given a mean default is taken to :=> sigma=1.0"
		 set sigma 1.0
	  } else {
		 set sigma $params(sigma)
	  }
	 
		set tmp {}
		for {set i 0} {$i < $n} {incr i} {
			set x [expr $mu+$sigma*[random]]
			lappend tmp $x
		}
     return $tmp
} 
 
 
#########################################################################################
# gets one column of vector returns two column histogram data with n slices
#----------------------------------------------------------------------------------------
proc histogramdata { args } {
# 	concat $args
	array set params [regsub -all "=" $args " "]
	
	if {[info exist params(L)]<1} {
		puts "you have not given a list of numbers Erorr"
		exit
	} else {
		set lst $params(L)
	}
	
	if {[info exist params(nslice)]<1} {
		puts "you have not define slice number default is taken to :=> nslice=51"
		set nslice 51
	} else {
		set nslice $params(nslice)
	}
	
	if {[info exist params(norm)]<1} {
		puts "you have not define normalizationr default is taken to :=> norm=1.0"
		set norm 1.0
	} else {
		set norm $params(norm)
	}
	
	set nl [llength $lst]
	
	if {$nl < $nslice } {
		puts " number of slices are bigger than number of slices reduced to nslice=length(L)"
		set nslice  $nl
	} else {
		set nslice $nslice
	}

	set minz [::math::statistics::min $lst] 
	set maxz [::math::statistics::max $lst] 
	set dz [expr ($maxz-$minz)/$nslice]
	
	set zstp {}
	set zst {}
	
	for {set i 0} {$i<[expr $nslice+4]} {incr i} {
		set zstpi [expr $minz+($i-1)*$dz]
		set zstpj [expr $minz+($i)*$dz]
		lappend zstp $zstpi
		lappend zst  [expr ($zstpi+$zstpj)/2]
	}
	
   set hst [lreplace [::math::statistics::histogram $zstp $lst] end end]
	
	set zh {}
	foreach z $zst h $hst {
		set hh [expr $norm*$h/$nl]
		lappend zh "$z $hh"
	}
	
	return $zh
   
}

#########################################################################################
# reads file and creaates histogram of longitudinal positions
#----------------------------------------------------------------------------------------
proc beamhistogram { args } {

    array set params [regsub -all "=" $args " "]
    
    if {[info exist params(file)]<1} {
	puts "you have not given a file to read Erorr"
	exit
    } else {
	set fin $params(file)
    }
    
    if {[info exist params(nslice)]<1} {
	puts "you have not define slice number default is taken to :=> nslice=101"
	set nslice 101
    } else {
	set nslice $params(nslice)
    }
    
    set ff [open $fin r]
    set lst {}
    while {[gets $ff line]>=0} {
	lappend lst [lindex $line 3]
    }
    close $ff
    

    
    set nl [llength $lst]
    
    if {$nl < $nslice } {
	puts " number of slices are bigger than number of slices reduced to nslice=length(L)"
	set nslice  $nl
    } else {
	set nslice $nslice
    }

    set minz [::math::statistics::min $lst] 
    set maxz [::math::statistics::max $lst] 
    set nslice2 [expr $nslice-2]
    set dz [expr ($maxz-$minz)/$nslice]
    
    set zst {}
    set hst {}
    set sum 1
    lappend zst $minz
    lappend hst 1.
    for {set i 1} {$i<[expr $nslice-1]} {incr i} {
	set zstpi [expr $minz+($i-0.5)*$dz]
	set zstpj [expr $minz+($i+0.5)*$dz]
	set zstp  [expr $minz+($i+0.0)*$dz]
	set amp 0
	foreach zl $lst {
	    if {$zl >= $zstpi && $zl < $zstpj } {
		incr amp
		incr sum
	    }
	}
	lappend zst $zstp
	lappend hst $amp
    }
    lappend zst $maxz
    lappend hst 1.
    incr sum
    
    set norm [expr 1./$sum]
    
    set zh {}
    foreach z $zst h $hst {
	set hh [expr $norm*$h]
	lappend zh "$z $hh"
    }
    
   return $zh
   
}

#########################################################################################
# checks if the number power of 2 if not adds required number importand for fourier transform
#----------------------------------------------------------------------------------------
proc check2n {x} {
	set k 0
	while {[expr pow(2,$k)] < $x} {
		incr k
	}
	set diff [expr pow(2,$k) - $x]
	
	puts "$x  [expr pow(2,$k)] $diff "
	
	return [expr int($diff)]
}

#########################################################################################
# checks if the number is even if not adds 1
#----------------------------------------------------------------------------------------
proc checkdobule {n} {
	set rest 	 [expr $n/2.]
	set integer  [expr floor($rest)]
	if {[expr $rest-$integer]==0} {
		set tmp $n
	} else {
		set tmp [expr $n+1]
	}
	return $tmp
}


#########################################################################################
#  Multiplies two array of complex numbers 
#----------------------------------------------------------------------------------------
proc complexmutiply {a_lst b_lst} {
	set tmp {}
	foreach f_wgt $a_lst f_wtr $b_lst {
		set a1 [lindex $f_wgt 0]
		set a2 [lindex $f_wgt 1]
		set b1 [lindex $f_wtr 0]
		set b2 [lindex $f_wtr 1]
		set convr [expr $a1*$b1-$a2*$b2]
		set convi [expr $a1*$b2+$a2*$b1]
		lappend tmp "$convr $convi"
	}
	return $tmp
}


proc smoothdata {zlist} {
	set zlist [lsort -real -index 0 $zlist]
	
 	for {set k 0} {$k<3} {incr k} {
# 		puts "smoothing histogram [expr $k +1]"
		set z {}
		set a {}
		set n [llength $zlist]
		foreach line $zlist {
			lappend z [lindex $line 0]
			lappend a [lindex $line 1]
		}
		set tmp {}
		
		
		lappend tmp "[lindex $z 0] [lindex $a 0]"   
		set a1 [lindex $a [expr 0]]
		set a2 [lindex $a [expr 1 ]]
		set a3 [lindex $a [expr 2]]
		set av [expr ($a1+$a2+$a3)/3.]
		lappend tmp "[lindex $z 1] $av"
		
		for {set i 2} {$i<[expr $n-2]} {incr i} {
			set sum 0.
			for {set j 0} {$j<5} {incr j} {
				set sum [expr $sum + [lindex $a [expr $i+$j-2]]]
			}
			set av [expr $sum/5.]
			lappend tmp "[lindex $z $i] $av"
		}
		
		set a1 [lindex $a [expr $n-3]]
		set a2 [lindex $a [expr $n-2 ]]
		set a3 [lindex $a [expr $n-1]]
		set av [expr ($a1+$a2+$a3)/3.]
		lappend tmp "[lindex $z $n-1] $av"

		
		lappend tmp "[lindex $z [expr $n-1]] [lindex $a [expr $n-1]]"
 		set zlist $tmp
 	}
	
	
	return $tmp
	
}
#########################################################################################
#  gauss function with mean sigma standard deviation 0. 
#----------------------------------------------------------------------------------------
proc gaussian {s sigma} {
	global pi
	return [expr 1./$sigma/sqrt(2.*$pi)*exp(-($s*$s)/2./$sigma/$sigma)]
}


proc plateau {s sigma} {
	global pi
	set ll [expr 3.333*$sigma] 
	set rt [expr $ll/10.]
	return [expr 1./$ll/(1.+exp(2./$rt*(2.*abs($s)-$ll)))]
}


proc parabola {s sigma} {
	global pi
	set zmax [expr sqrt(5.)*$sigma] 
	set zmin [expr -sqrt(5.)*$sigma] 
	
	if {$zmin<$s<$zmax} {
		set tmp [expr 3./4./$zmax*(1.-$s*$s/$zmax/$zmax)]
	} else {
		set tmp 0.0
	}
	return $tmp
}


proc uniform {s sigma} {
	global pi
	set fwhm [expr $sigma*2.*sqrt(3)]
	set zmax [expr $fwhm*0.5] 
	set zmin [expr -1.*$zmax] 
	
	if {$zmin<$s<$zmax} {
		set tmp [expr 1./$fwhm]
	} else {
		set tmp 0.0
	}
	return $tmp
}


proc gauss_sig1_norm { nmacro norm } {
	set sigma 1.0
	set sigma_cut [expr 10./3.]
	set minz [expr -$sigma_cut*$sigma]

	set dz [expr 2.*$sigma_cut*$sigma/($nmacro+1)]
	
	set sum 0.0
	set al  {}
	for {set i 0} {$i<[expr $nmacro]} {incr i } {
		set z   [expr $minz+($i+1)*$dz]
		set a   [expr [gaussian $z $sigma]]
		set sum [expr $sum+$a]
		lappend al  "$a "
	}
	
	set norm [expr $norm/$sum]
	
	set tmp {}
	
	foreach a $al {
		set a [expr $a*$norm]
		lappend tmp "$a"
	}

	return $tmp
}


proc uniform_sig1_norm { nmacro norm } {
	set sigma 1.0
	set sigma_cut [expr 2.*sqrt(3)]
	set minz [expr -$sigma_cut*$sigma]

	set dz [expr 2.*$sigma_cut*$sigma/($nmacro+1)]
	
	set sum 0.0
	set al  {}
	for {set i 0} {$i<[expr $nmacro]} {incr i } {
		set z   [expr $minz+$i*$dz]
		set a   [expr [uniform $z $sigma]]
		set sum [expr $sum+$a]
		lappend al  "$a "
	}
	
	set norm [expr $norm/$sum]
	
	set tmp {}
	
	foreach a $al {
		set a [expr $a*$norm]
		lappend tmp "$a"
	}

	return $tmp
}



proc gaussList { args } {
	array set params [regsub -all "=" $args " "]

	if {[info exist params(mu)]<1} {
		set mu 0.0
		puts "you have not given a standard deviation default is taken to :=> mu=$mu"
	} else {
		set mu $params(mu)
	}
	
	if {[info exist params(sigma)]<1} {
		set sigma 1.0
		puts "you have not given a mean default is taken to :=> sigma=$sigma"
	} else {
		set sigma $params(sigma)
	}
	
	if {[info exist params(sigma_cut)]<1} {
		set sigma_cut [expr 10./3.]
		puts "you have not define sigma_cut default is taken to :=> sigma_cut=$sigma_cut"
		
	} else {
		set sigma_cut $params(sigma_cut)
	}
	
	if {[info exist params(nslice)]<1} {
		set nslice 11
		puts "you have not given number of slices default is taken to :=> nslice=$nslice"
	} else {
		set nslice $params(nslice)
	}
	
	set minz [expr -$sigma_cut*$sigma]
	
	set dz [expr 2.*$sigma_cut*$sigma/($nslice)]
	
	set sum 0.0
	set al  {}
	set zl  {}
	for {set i 0} {$i<[expr $nslice]} {incr i } {
		set z   [expr $minz+($i+0.5)*$dz]
		set zz  [expr $z+$mu]
		set a   [expr [gaussian $z $sigma]]
		set sum [expr $sum+$a]
		lappend zl  "$zz "
		lappend al  "$a "
	}
	
	set norm [expr 1./$sum]
	
	set tmp {}
# 	lappend tmp  "[expr $minz+$mu-$dz] 0.0"
		foreach z  $zl a $al {
		set a [expr $a*$norm]
		lappend tmp "$z $a"
	}
# 	lappend tmp "[expr $zz+$dz] 0.0"
	return $tmp
}





proc plateauList { args } {
	array set params [regsub -all "=" $args " "]

	if {[info exist params(mu)]<1} {
		set mu 0.0
		puts "you have not given a standard deviation default is taken to :=> mu=$mu"
	} else {
		set mu $params(mu)
	}
	
	if {[info exist params(sigma)]<1} {
		set sigma 1.0
		puts "you have not given a mean default is taken to :=> sigma=$sigma"
	} else {
		set sigma $params(sigma)
	}
	
	if {[info exist params(sigma_cut)]<1} {
	   set sigma_cut [expr 10./3./sqrt(2)]
	   puts "the sigma_cut variable is fixed for this distribution :=> sigma_cut=$sigma_cut"
		
	} else {
		set sigma_cut [expr 10./3./sqrt(2)]
	}

	
	if {[info exist params(nslice)]<1} {
		set nslice 11
		puts "you have not given number of slices default is taken to :=> nslice=$nslice"
	} else {
		set nslice $params(nslice)
	}
	
	set minz [expr -$sigma_cut*$sigma]
	set dz  [expr 2*$sigma_cut*$sigma/($nslice)]
	
	set minz2 [expr -$sigma_cut*$sigma]
	while {[plateau $minz2 $sigma] < 2.e-7 } {
		set minz2 [expr $minz2+$dz]
	}

	set minz $minz2
	set dz [expr 2.*abs($minz)/$nslice]

	set sum 0.0
	set al  {}
	set zl  {}
	for {set i 0} {$i<[expr $nslice]} {incr i } {
		set z   [expr $minz+($i+0.5)*$dz]
		set zz  [expr $z+$mu]
		set a   [expr [plateau $z $sigma]]
		set sum [expr $sum+$a]
		lappend zl  "$zz "
		lappend al  "$a "
	}
	
	set norm [expr 1./$sum]
	
	set tmp {}
# 	lappend tmp  "[expr $minz+$mu-$dz] 0.0"
	foreach z  $zl a $al {
		set a [expr $a*$norm]
		lappend tmp "$z $a"
	}
# 	lappend tmp "[expr $zz+$dz] 0.0"
	return $tmp
}


proc parabolaList { args } {
	array set params [regsub -all "=" $args " "]

	if {[info exist params(mu)]<1} {
		set mu 0.0
		puts "you have not given a standard deviation default is taken to :=> mu=$mu"
	} else {
		set mu $params(mu)
	}
	
	if {[info exist params(sigma)]<1} {
		set sigma 1.0
		puts "you have not given a mean default is taken to :=> sigma=$sigma"
	} else {
		set sigma $params(sigma)
	}
	
	if {[info exist params(sigma_cut)]<1} {
		set sigma_cut [expr sqrt(5.)]
		puts "the sigma_cut variable is fixed for this distribution :=> sigma_cut=$sigma_cut"
		
	} else {
		set sigma_cut [expr sqrt(5.)]
	}
	
	if {[info exist params(nslice)]<1} {
		set nslice 11
		puts "you have not given number of slices default is taken to :=> nslice=$nslice"
	} else {
		set nslice $params(nslice)
	}
	

	set minz [expr -$sigma_cut*$sigma]
	set dz  [expr 2*$sigma_cut*$sigma/($nslice)]
	
	set sum 0.0
	set al  {}
	set zl  {}
	for {set i 0} {$i<[expr $nslice]} {incr i } {
		set z   [expr $minz+($i+0.5)*$dz]
		set zz  [expr $z+$mu]
		set a   [expr [parabola $z $sigma]]
		set sum [expr $sum+$a]
		lappend zl  "$zz "
		lappend al  "$a "
	}
	
	set norm [expr 1./$sum]
	
	set tmp {}
# 	lappend tmp  "[expr $minz+$mu-$dz] 0.0"
		foreach z  $zl a $al {
		set a [expr $a*$norm]
		lappend tmp "$z $a"
	}
# 	lappend tmp "[expr $zz+$dz] 0.0"
	return $tmp
}


proc uniformList { args } {
	array set params [regsub -all "=" $args " "]

	if {[info exist params(mu)]<1} {
		set mu 0.0
		puts "you have not given a standard deviation default is taken to :=> mu=$mu"
	} else {
		set mu $params(mu)
	}
	
	if {[info exist params(sigma)]<1} {
		set sigma 1.0
		puts "you have not given a mean default is taken to :=> sigma=$sigma"
	} else {
		set sigma $params(sigma)
	}
	
	if {[info exist params(sigma_cut)]<1} {
		set sigma_cut [expr 2.*sqrt(3)]
		puts "the sigma_cut variable is fixed for this distribution :=> sigma_cut=$sigma_cut"
		
	} else {
		set sigma_cut [expr 2.*sqrt(3)]
	}
	
	if {[info exist params(nslice)]<1} {
		set nslice 11
		puts "you have not given number of slices default is taken to :=> nslice=$nslice"
	} else {
		set nslice $params(nslice)
	}
	

	set minz [expr -0.5*$sigma_cut*$sigma]
	set dz  [expr $sigma_cut*$sigma/$nslice]

	set sum 0.0
	set al  {}
	set zl  {}
	for {set i 0} {$i<[expr $nslice]} {incr i } {
		set z   [expr $minz+($i+0.5)*$dz]
		set zz  [expr $z+$mu]
		set a   [expr [uniform $z $sigma]]
		set sum [expr $sum+$a]
		lappend zl  "$zz "
		lappend al  "$a "
	}
	
	set norm [expr 1./$sum]
	
	set tmp {}
# 	lappend tmp  "[expr $minz+$mu-$dz] 0.0"
		foreach z  $zl a $al {
		set a [expr $a*$norm]
		lappend tmp "$z $a"
	}
# 	lappend tmp "[expr $zz+$dz] 0.0"
	return $tmp
}


proc get_zero_amp { llist } {
	set zmin 1.e300
	set a0 0.0
	foreach line $llist {
		set z [expr abs([lindex $line 0])]
		set a [lindex $line 1]
		if {$z < $zmin} { 
			set zmin $z 
			set a0 $a
		} else { 
			set zmin $zmin
			set a0 $a0
		}
	}
	return "$zmin $a0"
}


proc mean {lst} {
	set sum1 [::math::statistics::mean $lst]
	return $sum1
}

proc rms {lst} {
	set sum1 [::math::statistics::var $lst]
	return $sum1
}



proc mean2 {lst1 lst2} {
	set ml1 [mean $lst1]
	set ml2 [mean $lst2]
	set product {}
	foreach l1 $lst1 l2 $lst2 {
		lappend product [expr ($l1-$ml1)*($l2-$ml2)]
	}
	return [::math::statistics::mean $product]
}

proc bunch_parameters {filein} {
	

  set elist   {}  ;# 0
  set xlist   {}  ;# 1
  set ylist   {}  ;# 2
  set zlist   {}  ;# 3
  set xplist  {}  ;# 4
  set yplist  {}  ;# 5
  
  set npart 0
  set fin [open $filein r]
  while {[gets $fin line] >= 0}  {
	  if {[llength $line ]==0 } {
		  incr nbunch
	  } else {
		  lappend elist   [lindex $line 0]
		  lappend xlist   [lindex $line 1]
		  lappend ylist   [lindex $line 2]
		  lappend zlist   [lindex $line 3]
		  lappend xplist  [lindex $line 4]
		  lappend yplist  [lindex $line 5]
		  incr npart
	  }
  }
  
  set emean [mean $elist]
  set gamma [expr $emean/0.5109989e-3]
  set sxx   [rms $xlist]
  set sxpxp [rms $xplist]
  set sxxp  [mean2 $xlist $xplist]
  set epsx  [expr sqrt($sxx*$sxpxp-$sxxp*$sxxp)]
  set emtx  [expr $epsx*$gamma*1e-5]
  set bx    [expr $sxx/$epsx]
  set ax	 [expr -$sxxp/$epsx]
  
  set syy   [rms $ylist]
  set sypyp [rms $yplist]
  set syyp  [mean2 $ylist $yplist]
  set epsy  [expr sqrt($syy*$sypyp-$syyp*$syyp)]
  set emty  [expr $epsy*$gamma*1e-5]
  set by    [expr $syy/$epsy]
  set ay	 [expr -$syyp/$epsy]
  
  set sz [expr sqrt([rms $zlist])]
  set de [expr sqrt([rms $elist])/$emean*100.]

  array set beampar {}
  set beampar(energy) $emean
  set beampar(sigma_z) $sz
  set beampar(e_spread) $de
  set beampar(beta_x) $bx
  set beampar(beta_y) $by
  set beampar(alpha_x) $ax
  set beampar(alpha_y) $ay
  set beampar(emitt_x) $emtx
  set beampar(emitt_y) $emty
  set beampar(npart) $npart
  return [array get beampar]
  
}


proc integrate_linac {twissfile } {
	
  set fin [open $twissfile r]

  set beta_x {}
  set beta_y {}
  set leng {}
  set ener {}
  set nline 0
  while {[gets $fin line] >= 0}  {
	if {$nline > 18 } {
	  lappend leng [lindex $line 1]
	  lappend ener [expr [lindex $line 2]*1e3]
	  lappend beta_x [expr [lindex $line 5]]
	  lappend beta_y [expr [lindex $line 9]]
	}
	incr nline
  }
  close $fin

  set int_x_ary {}
  set int_y_ary {}
  set int_x 0.0
  set int_y 0.0
  set phase_x 0.0
  set phase_y 0.0

  set totline [llength $leng]

  for {set i 0} {$i< [expr $totline-1]} {incr i} {
	set s_ii [lindex $leng [expr $i]]
	set s_jj [lindex $leng [expr $i+1]]
	set beta_xii [lindex $beta_x [expr $i]]
	set beta_xjj [lindex $beta_x [expr $i+1]]
	set beta_yii [lindex $beta_y [expr $i]]
	set beta_yjj [lindex $beta_y [expr $i+1]]
	set ener_ii  [lindex $ener [expr $i]]
	set ener_jj  [lindex $ener [expr $i+1]]

	set phase_x [expr $phase_x + 0.5*($s_jj-$s_ii)*(1./$beta_xii +1./$beta_xjj)]
	set phase_y [expr $phase_y + 0.5*($s_jj-$s_ii)*(1./$beta_yii +1./$beta_yjj)]
		

	if { [expr ($ener_jj + $ener_ii)*0.5] <= $ener_ii } {
	  set beta_xii 0
	  set beta_xjj 0
	  set beta_yii 0
	  set beta_yjj 0
	} else {
	  set beta_xii $beta_xii
	  set beta_xjj $beta_xjj
	  set beta_yii $beta_yii
	  set beta_yjj $beta_yjj
	}

	set int_x [expr $int_x + 0.5*($s_jj-$s_ii)*($beta_xii/$ener_ii + $beta_xjj/$ener_jj)]
	set int_y [expr $int_y + 0.5*($s_jj-$s_ii)*($beta_yii/$ener_ii + $beta_yjj/$ener_jj)]

  }


  array set lin_integrate {}
  set lin_integrate(phase_advance_x) $phase_x
  set lin_integrate(phase_advance_y) $phase_y
  set lin_integrate(beta_over_energy_x) $int_x
  set lin_integrate(beta_over_energy_y) $int_y

  return [array get lin_integrate]

} 

proc get_max_amplification {filein} {
	
  set max_axn -1e10
  set max_ayn -1e10

  set fin [open $filein r]
  set nline 0
  while {[gets $fin line] >= 0}  {
    if {$nline > 0 } {
      set xn  [lindex $line 1]
      set yn  [lindex $line 2]
      set xpn [lindex $line 4]
      set ypn [lindex $line 5]
      set axn [expr sqrt($xn*$xn+$xpn*$xpn)]
      set ayn [expr sqrt($yn*$yn+$ypn*$ypn)]
      
      if { $axn > $max_axn } {set max_axn $axn} else { set max_axn $max_axn}
      if { $ayn > $max_ayn } {set max_ayn $ayn} else { set max_ayn $max_ayn}
    }
    incr nline
   
  }
  close $fin
  return [format "%12.8f %12.8f " $max_axn $max_ayn]
}
   
proc ccos { deg } {
    set pi [expr acos(-1.0)]
    return [expr cos($deg*$pi/180.)]
}
