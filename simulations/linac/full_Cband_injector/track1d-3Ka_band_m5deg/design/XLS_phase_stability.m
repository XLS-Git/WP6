addpath('../files');
Track1D;

%% read in the particle distribution
M = load('../files/cband_out_part.dat.gz');
T = M(:,4); % um/c
P = M(:,1); % GeV/c

% store some bunch information 
Beam.charge_pC = 75; % pC
Beam.sigmaZ = std(T); % um/c
Beam.sigmaP = std(P); % GeV/c, uncorrelated
Beam.mass = 0.00051099893 % [GeV/c^2]

global B0
B0 = Bunch1d(Beam.mass, Beam.charge_pC * 6241509.343260179, [ T P ]);     

%% load the default solution
source('XLS_layout.dat');

%% print out machine info
XLS

%% main
T = [];
for XLS.Ka_PhiD = linspace(174, 186, 17)
    Machine = setup_machine_HX(XLS);
    
    L = Lattice();
    L.append(Machine.L0); 
    L.append(Machine.Ka); 
    B1 = L.track_z(B0);
    
    L = Lattice();
    L.append(Machine.BC1);
    B2 = L.track_z(B1);
    
    % set <Z> = 0
    D = B2.data;
    D(:,1) -= XLS.BC1_z1;
    B2.set_data(D);
    
    L = Lattice();
    L.append(Machine.L1);
    B3 = L.track_z(B2);
    
    L = Lattice();
    L.append(Machine.BC2);
    B4 = L.track_z(B3);
    
    % set <Z> = 0
    D = B4.data;
    D(:,1) -= XLS.BC2_z1;
    B4.set_data(D);
    
    L = Lattice();
    L.append(Machine.L2);
    L.append(Machine.L3);
    B5 = L.track_z(B4);
    
    Z = B5.data(:,1); % um
    P = B5.data(:,2);
    
    T = [ T ; XLS.Ka_PhiD mean(Z) std(Z) mean(P) std(P) ];
    
end

H_axis = T(:,1) - 180;
H_label = '\Delta\phi_{Ka} [deg]';

figure(1); % , 'visible', 'off');
clf 
subplot(2,2,1); hold on
plot(H_axis, T(:,2) * 3.33564095198152);
plot(H_axis,  zeros(size(T(:,1))) * XLS.Out_sigmaZ * 3.33564095198152, ':k')
plot(H_axis, -ones(size(T(:,1))) * XLS.Out_sigmaZ * 3.33564095198152, ':k')
xlabel(H_label);
ylabel('arrival time [fs]');
grid on

subplot(2,2,2); hold on
plot(H_axis, (T(:,3)-XLS.Out_sigmaZ)/XLS.Out_sigmaZ * 100);
plot(H_axis,  ones(size(T(:,1))) * 5, ':k')
plot(H_axis, -ones(size(T(:,1))) * 5, ':k')
xlabel(H_label);
ylabel('\Delta{C}/C [%]');
axis([ -1 1 -7 7 ]);
grid on

subplot(2,2,3); hold on
plot(H_axis, (T(:,4) - XLS.Out_meanE) / XLS.Out_meanE * 1e2);
plot(H_axis,  ones(size(T(:,1))) * 0.05, ':k')
plot(H_axis, -ones(size(T(:,1))) * 0.05, ':k')
xlabel(H_label);
ylabel('relative energy variation [%]');
grid on

subplot(2,2,4);
plot(H_axis, T(:,5) * 1e3);
xlabel(H_label);
ylabel('projected energy spread [MeV]');
grid on

print -dpng XLS_Ka_phase.png