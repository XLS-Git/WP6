addpath('../files');
Track1D;

%% read in the particle distribution
M = load('../files/cband_out_part.dat.gz');
T = M(:,4); % um/c
P = M(:,1); % GeV/c

% store some bunch information 
Beam.charge_pC = 75; % pC
Beam.sigmaZ = std(T); % um/c
Beam.sigmaP = std(P); % GeV/c, uncorrelated
Beam.mass = 0.00051099893 % [GeV/c^2]

global B0
B0 = Bunch1d(Beam.mass, Beam.charge_pC * 6241509.343260179, [ T P ]);     

%% load the default solution
source('XLS_layout.dat');

%% print out machine info
XLS

%% main
Machine = setup_machine_HX(XLS);

L = Lattice();
L.append(Machine.L0); 
L.append(Machine.Ka); 
B1 = L.track_z(B0);

L = Lattice();
L.append(Machine.BC1);
B2 = L.track_z(B1);

% set <Z> = 0
XLS.BC1_z1 = mean(B2.data(:,1));
D = B2.data;
D(:,1) -= XLS.BC1_z1;
B2.set_data(D);

L = Lattice();
L.append(Machine.L1);
B3 = L.track_z(B2);

L = Lattice();
L.append(Machine.BC2);
B4 = L.track_z(B3);

% set <Z> = 0
XLS.BC2_z1 = mean(B4.data(:,1));
D = B4.data;
D(:,1) -= XLS.BC2_z1;
B4.set_data(D);

L = Lattice();
L.append(Machine.L2);
L.append(Machine.L3);
B5 = L.track_z(B4);

M1 = B1.data;
M2 = B2.data;
M3 = B3.data;
M4 = B4.data;
M5 = B5.data;
save -text -zip XLS_beams.dat.gz M1 M2 M3 M4 M5

XLS.Out_sigmaZ = std(B5.data(:,1));
XLS.Out_sigmaE = std(B5.data(:,2));
XLS.Out_meanE = mean(B5.data(:,2));

fid = fopen('XLS_layout.dat', 'w');
fields = fieldnames(XLS);
for i = 1:length(fields)
    fprintf(fid, 'XLS.%s = %.15g;\n', fields{i}, eval( [ 'XLS.' fields{i} ]))
end
fclose(fid);

figure('visible','off');
clf
hold on
scatter(B0.data(:,1), 1e2*(B0.data(:,2)-mean(B0.data(:,2)))/mean(B0.data(:,2)));
scatter(B2.data(:,1), 1e2*(B2.data(:,2)-mean(B2.data(:,2)))/mean(B2.data(:,2)));
scatter(B5.data(:,1), 1e2*(B5.data(:,2)-mean(B5.data(:,2)))/mean(B5.data(:,2)));
xlabel('t [um/c]')
ylabel('dP/P [%]')
print -dpng XLS_plot.png

pkg load plot
clf
subplot(2,3,1);
%scatter(B1.data(:,1)/1e3, 1e3*B1.data(:,2));
[H,Xa,Ya] = hist2d([ B1.data(:,1)/1e3 1e3*B1.data(:,2) ],64,64);
h = pcolor(Xa, Ya, H);
set(h, 'EdgeColor', 'none');
axis([ Xa(1) Xa(end) Ya(1) Ya(end) ]);
title('after Linac-0 and K-band')
xlabel('\Delta{t} [mm/c]')
ylabel('P [MeV/c]')

%figure(2)
subplot(2,3,2);
%scatter(B2.data(:,1), 1e3*B2.data(:,2));
[H,Xa,Ya] = hist2d([ B2.data(:,1) 1e3*B2.data(:,2) ],64,64);
h = pcolor(Xa, Ya, H);
set(h, 'EdgeColor', 'none');
axis([ Xa(1) Xa(end) Ya(1) Ya(end) ]);
title('after BC1')
xlabel('\Delta{t} [um/c]')
ylabel('P [MeV/c]')

%figure(3)
subplot(2,3,3);
%scatter(B3.data(:,1), B3.data(:,2));
[H,Xa,Ya] = hist2d([ B3.data(:,1) 1e3*B3.data(:,2) ],64,64);
h = pcolor(Xa, Ya, H);
set(h, 'EdgeColor', 'none');
axis([ Xa(1) Xa(end) Ya(1) Ya(end) ]);
title('after Linac-1')
xlabel('\Delta{t} [um/c]')
ylabel('P [GeV/c]')

%figure(4)
subplot(2,3,4);
%scatter(B4.data(:,1), B4.data(:,2));
[H,Xa,Ya] = hist2d([ B4.data(:,1) B4.data(:,2) ],64,64);
h = pcolor(Xa, Ya, H);
set(h, 'EdgeColor', 'none');
axis([ Xa(1) Xa(end) Ya(1) Ya(end) ]);
title('after BC2')
xlabel('\Delta{t} [um/c]')
ylabel('P [GeV/c]')

%figure(5)
mean_P = mean(B5.data(:,2));
subplot(2,3,5);
%scatter(B5.data(:,1), (B5.data(:,2) - mean_P) / mean_P * 100);
[H,Xa,Ya] = hist2d([ B5.data(:,1) (B5.data(:,2) - mean_P) / mean_P * 100 ],64,64);
h = pcolor(Xa, Ya, H);
set(h, 'EdgeColor', 'none');
axis([ Xa(1) Xa(end) Ya(1) Ya(end) ]);
title(sprintf('after Linac-2 (<P>=%.2g GeV/c)', mean_P))
xlabel('\Delta{t} [um/c]')
ylabel('\delta_P [%]')

%figure(6);
subplot(2,3,6);
[H,X] = hist((B5.data(:,1) - mean(B5.data(:,1))) / 0.29979246, 40, Beam.charge_pC); % pC, fs
H /= X(2) - X(1); % kA = pC/fs
plot(X,H);
title('Current')
ylabel('I_{peak} [kA]')
xlabel('\Delta{t} [fs]')
drawnow;

print -dpng tmp.png;
system('pngtopnm tmp.png | pnmcrop | pnmtopng -compression=9 > XLS_track.png && rm -f tmp.png')

%% print out some bunch info  
bc1_sigmaZ = std(B2.data(:,1));
bc1_meanE = mean(B2.data(:,2));
bc2_sigmaZ = std(B5.data(:,1));
bc2_meanE = mean(B4.data(:,2));
bc2_sigmaE = std(B4.data(:,2));
bc2_sigmaE_E_percent = bc2_sigmaE / bc2_meanE * 100;

END_meanE = mean(B5.data(:,2));
END_sigmaE = std(B5.data(:,2));
END_sigmaE_E_percent = END_sigmaE / bc2_meanE * 100;

% Sliced quantities
Z = sort(B5.data(:,1));
Z_ = linspace(0, 100, length(Z));
Z_min = spline(Z_, Z, 47.5);
Z_max = spline(Z_, Z, 52.5);
M = Z>=Z_min & Z<Z_max;
bc2_meanE_slice = mean(B4.data(M,2));
bc2_sigmaE_slice = std(B4.data(M,2));
bc2_sigmaE_E_slice_percent = bc2_sigmaE_slice / bc2_meanE_slice * 100;
END_meanE_slice = mean(B5.data(M,2));
END_sigmaE_slice = std(B5.data(M,2));
END_sigmaE_E_slice_percent = END_sigmaE_slice / END_meanE_slice * 100;

printf('BC1_sigmaZ = %.2g um\n', bc1_sigmaZ);
printf('BC1_meanE = %.2f MeV\n\n', 1e3*bc1_meanE);
printf('BC2_sigmaZ = %.2g um\n', bc2_sigmaZ);
printf('BC2_meanE = %.2f GeV\n', bc2_meanE);
printf('BC2_slice_sigmaE = %.2g GeV\n', bc2_sigmaE_slice);
printf('BC2_slice_rel_Espread = %2.1g %%\n\n', bc2_sigmaE_E_slice_percent);
printf('END_sigmaZ = %.2g um\n', bc2_sigmaZ);
printf('END_meanE = %.2f GeV\n', END_meanE);
printf('END_slice_sigmaE = %.2g GeV\n', END_sigmaE_slice);
printf('END_slice_rel_Espread = %2.1g %%\n\n', END_sigmaE_E_slice_percent);

%{
T_fs = (B5.data(:,1) - mean(B5.data(:,1))) / 0.29979246; % fs
E_GeV = B5.data(:,2); % GeV
Q_pC = Beam.charge / length(T_fs) * ones(size(T_fs)); % pC

T = [ T_fs E_GeV Q_pC ];

save -text XLS_beam.dat T;

[H,X] = hist((B5.data(:,1) - mean(B5.data(:,1))) / 0.29979246, 40, Beam.charge); % pC, fs
H /= X(2) - X(1); % kA = pC/fs

T = [ X H ];
save -text XLS_current.dat T;
%}
