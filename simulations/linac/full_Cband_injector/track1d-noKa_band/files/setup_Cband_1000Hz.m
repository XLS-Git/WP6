function C = setup_Cband_1000Hz()
    Track1D;
    
    clight = 0.29979246; % m/ns
    
    ## C-band
    C.Ccell = Cell();
    C.Ccell.frequency = 5.712; % GHz
    C.Ccell.a = 0.0066; % m
    C.Ccell.l = clight / C.Ccell.frequency / 3; % m, 2*pi/3 phase advance
    C.Ccell.g = C.Ccell.l - 0.0025; % m
    
    C.Cstructure = AcceleratingStructure(C.Ccell, 115);
    C.Cstructure.max_gradient = 15e-3; % GV/m

endfunction
