function K = setup_Kband_100Hz()
    Track1D;
    
    clight = 0.29979246; % m/ns
    
    ## Latest K-band structure for CompactLight
    K.Kcell = Cell();
    K.Kcell.frequency = 3 * 11.9942; % GHz
    lambda_K = clight / K.Kcell.frequency; % m
    K.Kcell.a = 0.002; % m
    K.Kcell.l = lambda_K / 2; % m, pi/2 phase advance
    K.Kcell.g = K.Kcell.l - 0.000667; % m
    
    K.Kstructure = AcceleratingStructure(K.Kcell, 19);
    K.Kstructure.max_gradient = 0.2047732424416445; % GV/m (16.2 MV
                                                    % integrated voltage)

endfunction
