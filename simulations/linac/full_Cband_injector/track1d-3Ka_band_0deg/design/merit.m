function [M,B5,B2] = merit(X)
    Track1D;
    
    global XLS B0
    
    energy0_GeV = mean(B0.data(:,2));
    
    XLS.L0_PhiD    = constrain(X(1), 0, 90); % degrees
    XLS.L0_Voltage = (0.3 - energy0_GeV) / cosd(XLS.L0_PhiD); % GV
    XLS.Ka_PhiD    = 180; % degrees
    XLS.Ka_Voltage = constrain(X(2), 0, 0.017); % GV
    XLS.BC1_Pref   = constrain(X(3), 0.280, 0.320); % GeV/c
    XLS.BC1_R56    = constrain(X(4), -0.009, 0); % m
    XLS.BC1_T566   = -3/2 * XLS.BC1_R56;
    XLS.L1_PhiD    = constrain(X(5), 0, 90); % degrees
    XLS.L1_Voltage = constrain(X(6), 0, 0.7 + 0.234); % GV
    XLS.BC2_Pref   = constrain(X(7), 0.3, 1.5); % GeV/c
    XLS.BC2_R56    = constrain(X(8), -0.004, 0); % m
    XLS.BC2_T566   = -3/2 * XLS.BC2_R56; % m
    XLS.L2_PhiD    = constrain(X(9), -50, 50); % degrees
    XLS.L2_Voltage = constrain(X(10), 0, 0.45 + 0.234); % GV
    XLS.L3_PhiD    = constrain(X(11), -50, 50); % degrees
    XLS.L3_Voltage = constrain(X(12), 0, 4.2 + 0.234); % GV

    Machine = setup_machine_HX(XLS);

    [B5,~,B3,B2,~] = track(Machine, B0);

    sigmaz1_um = std(B2.data(:,1));
    energy1_GeV = mean(B2.data(:,2));
    energy2_GeV = mean(B3.data(:,2));
    sigmaz3_um = std(B5.data(:,1));
    energy3_GeV = mean(B5.data(:,2));
    skew = skewness(B5.data(:,1));
    kurt = kurtosis(B5.data(:,1));

    M = 1000 * (sigmaz1_um / 22 - 1)**2 + ...
        10000 * (energy1_GeV / 0.3 - 1)**2 + ...
        1000 * (sigmaz3_um / 1 - 1)**2 + ...
        10000 * (energy3_GeV / 5.5 - 1)**2 + ...
        10 * (kurt / 1.8 - 1)**2 + ...
        1000 * abs(skew) + ...
	      1000 * abs(XLS.BC1_R56) + ...	
	      1000 * abs(XLS.BC2_R56);
    
     %disp([ M sigmaz1_um sigmaz3_um energy1_GeV energy2_GeV energy3_GeV kurt ]);
    
end

