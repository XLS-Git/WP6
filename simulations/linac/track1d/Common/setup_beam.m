function B0 = setup_beam(Q_pC)
    Track1D;
    
    if nargin == 0
        Q_pC = 75;
    endif
    
    M = importdata('../Common/new_oncrest_check_CSRON_300MeV_Xband_Hrep_stracheck.w1.asci', ' ', 45).data;
    T = M(1:10:end,7) * 299792458e6; % um/c
    P = M(1:10:end,6) * 0.00051099893; % GeV/c
    
    % store some bunch information 
    Beam.charge_pC = Q_pC; % pC
    Beam.sigmaZ = std(T); % um/c
    Beam.sigmaP = std(P); % GeV/c, uncorrelated
    Beam.mass = 0.00051099893 % [GeV/c^2]

    B0 = Bunch1d(Beam.mass, Beam.charge_pC * 6241509.343260179, [ T P ]);