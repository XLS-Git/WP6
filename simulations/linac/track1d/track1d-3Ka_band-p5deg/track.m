function [B5,B4,B3,B2,B1] = track(Machine, B0)
    Track1D;
    
    L = Lattice();
    L.append(Machine.L0); 
    L.append(Machine.Ka); 
    B1 = L.track_z(B0);
    
    L = Lattice();
    L.append(Machine.BC1);
    B2 = L.track_z(B1);
    
    % set <Z> = 0
    D = B2.data;
    D(:,1) -= mean(D(:,1));
    B2.set_data(D);

    L = Lattice();
    L.append(Machine.L1);
    B3 = L.track_z(B2);

    L = Lattice();
    L.append(Machine.BC2);
    B4 = L.track_z(B3);

    % set <Z> = 0
    D = B4.data;
    D(:,1) -= mean(D(:,1));
    B4.set_data(D);

    L = Lattice();
    L.append(Machine.L2);
    L.append(Machine.L3);
    B5 = L.track_z(B4);

end
