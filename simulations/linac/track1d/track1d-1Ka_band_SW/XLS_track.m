addpath('../Common');
Track1D;

global B0
B0 = setup_beam();
Q_pC = 75; % bunch charge

%% load the default solution
source('XLS_layout.dat');

%% print out machine info
XLS

%% main
Machine = setup_machine_HX(XLS);

L = Lattice();
L.append(Machine.L0); 
L.append(Machine.Ka); 
B1 = L.track_z(B0);

L = Lattice();
L.append(Machine.BC1);
B2 = L.track_z(B1);

% set <Z> = 0
D = B2.data;
D(:,1) -= XLS.BC1_z1;
B2.set_data(D);

L = Lattice();
L.append(Machine.L1);
B3 = L.track_z(B2);

L = Lattice();
L.append(Machine.BC2);
B4 = L.track_z(B3);

% set <Z> = 0
D = B4.data;
D(:,1) -= XLS.BC2_z1;
B4.set_data(D);

L = Lattice();
L.append(Machine.L2);
L.append(Machine.L3);
B5 = L.track_z(B4);

M1 = B1.data;
M2 = B2.data;
M3 = B3.data;
M4 = B4.data;
M5 = B5.data;
save -text -zip XLS_beams.dat.gz M1 M2 M3 M4 M5

graphics_toolkit gnuplot
figure('visible','off');
subplot(2,3,1);
scatter(B1.data(:,1)/1e3, 1e3*B1.data(:,2));
title('after Linac-0 and K-band')
xlabel('\Delta{t} [mm/c]')
ylabel('P [MeV/c]')

%figure(2)
subplot(2,3,2);
scatter(B2.data(:,1), 1e3*B2.data(:,2));
title('after BC1')
xlabel('\Delta{t} [um/c]')
ylabel('P [MeV/c]')

%figure(3)
subplot(2,3,3);
scatter(B3.data(:,1), B3.data(:,2));
title('after Linac-1')
xlabel('\Delta{t} [um/c]')
ylabel('P [GeV/c]')

%figure(4)
subplot(2,3,4);
scatter(B4.data(:,1), B4.data(:,2));
title('after BC2')
xlabel('\Delta{t} [um/c]')
ylabel('P [GeV/c]')

%figure(5)
mean_P = mean(B5.data(:,2));
subplot(2,3,5);
scatter(B5.data(:,1), (B5.data(:,2) - mean_P) / mean_P * 100);
title(sprintf('after Linac-2 (<P>=%.2g GeV/c)', mean_P))
xlabel('\Delta{t} [um/c]')
ylabel('\delta_P [%]')
legend(sprintf('<P> = %.2g', mean_P)); 

%figure(6);
subplot(2,3,6);
[H,X] = hist((B5.data(:,1) - mean(B5.data(:,1))) / 0.29979246, 40, Q_pC); % pC, fs
H /= X(2) - X(1); % kA = pC/fs
plot(X,H);
title('Current')
ylabel('I_{peak} [kA]')
xlabel('\Delta{t} [fs]')
drawnow;

print -dpng tmp.png;
system('pngtopnm tmp.png | pnmcrop | pnmtopng -compression=9 > XLS_track.png && rm -f tmp.png')

%% print out some bunch info  
bc1_sigmaZ = std(B2.data(:,1));
bc1_meanE = mean(B2.data(:,2));
bc2_sigmaZ = std(B5.data(:,1));
bc2_meanE = mean(B4.data(:,2));
bc2_sigmaE = std(B4.data(:,2));
bc2_sigmaE_E_percent = bc2_sigmaE / bc2_meanE * 100;

END_meanE = mean(B5.data(:,2));
END_sigmaE = std(B5.data(:,2));
END_sigmaE_E_percent = END_sigmaE / bc2_meanE * 100;

% Sliced quantities
Z = sort(B5.data(:,1));
Z_ = linspace(0, 100, length(Z));
Z_min = spline(Z_, Z, 47.5);
Z_max = spline(Z_, Z, 52.5);
M = Z>=Z_min & Z<Z_max;
bc2_meanE_slice = mean(B4.data(M,2));
bc2_sigmaE_slice = std(B4.data(M,2));
bc2_sigmaE_E_slice_percent = bc2_sigmaE_slice / bc2_meanE_slice * 100;
END_meanE_slice = mean(B5.data(M,2));
END_sigmaE_slice = std(B5.data(M,2));
END_sigmaE_E_slice_percent = END_sigmaE_slice / END_meanE_slice * 100;

printf('BC1_sigmaZ = %.2g um\n', bc1_sigmaZ);
printf('BC1_meanE = %.2f MeV\n\n', 1e3*bc1_meanE);
printf('BC2_sigmaZ = %.2g um\n', bc2_sigmaZ);
printf('BC2_meanE = %.2f GeV\n', bc2_meanE);
printf('BC2_slice_sigmaE = %.2g GeV\n', bc2_sigmaE_slice);
printf('BC2_slice_rel_Espread = %2.1g %%\n\n', bc2_sigmaE_E_slice_percent);
printf('END_sigmaZ = %.2g um\n', bc2_sigmaZ);
printf('END_meanE = %.2f GeV\n', END_meanE);
printf('END_slice_sigmaE = %.2g GeV\n', END_sigmaE_slice);
printf('END_slice_rel_Espread = %2.1g %%\n\n', END_sigmaE_E_slice_percent);

%{
T_fs = (B5.data(:,1) - mean(B5.data(:,1))) / 0.29979246; % fs
E_GeV = B5.data(:,2); % GeV
Q_pC_ = Q_pC / length(T_fs) * ones(size(T_fs)); % pC

T = [ T_fs E_GeV Q_pC_ ];

save -text XLS_beam.dat T;

[H,X] = hist((B5.data(:,1) - mean(B5.data(:,1))) / 0.29979246, 40, Q_pC); % pC, fs
H /= X(2) - X(1); % kA = pC/fs

T = [ X H ];
save -text XLS_current.dat T;
%}
