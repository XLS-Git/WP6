function M = setup_machine_HX(XLS)
    Track1D;

    M.Xband_L0 = setup_Xband_1000Hz ();
    M.Kband = setup_Kband_TW_100Hz ();
    M.Xband = setup_Xband_100Hz ();
    
    M.L0 = CavityArray(M.Xband_L0.Xstructure);
    M.L0.set_voltage (XLS.L0_Voltage);
    M.L0.set_phased  (XLS.L0_PhiD);
    M.L0.resize(8); % Anna Giribono, Glasgow virtual
    
    M.Ka = CavityArray(M.Kband.Kstructure);
    M.Ka.set_voltage(XLS.Ka_Voltage);
    M.Ka.set_phased (XLS.Ka_PhiD);
    M.Ka.resize(4); % 

    M.BC1 = Chicane(XLS.BC1_R56, XLS.BC1_T566, 0.0, XLS.BC1_Pref);

    M.L1 = CavityArray(M.Xband.Xstructure);
    M.L1.set_voltage (XLS.L1_Voltage);
    M.L1.set_phased  (XLS.L1_PhiD);
    M.L1.resize(12); % Avni

    M.BC2 = Chicane(XLS.BC2_R56, XLS.BC2_T566, 0.0, XLS.BC2_Pref);

    M.L2 = CavityArray(M.Xband.Xstructure);
    M.L2.set_voltage (XLS.L2_Voltage);
    M.L2.set_phased  (XLS.L2_PhiD);
    M.L2.resize(8); % Avni
    %M.L2.resize(80); % Avni

    M.L3 = CavityArray(M.Xband.Xstructure);
    M.L3.set_voltage (XLS.L3_Voltage);
    M.L3.set_phased  (XLS.L3_PhiD);
    M.L3.resize(72); % Avni
