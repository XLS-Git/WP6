AST.x = load('noSC/XLS_benchmark.Xemit.001');
AST.z = load('noSC/XLS_benchmark.Zemit.001');
RFT   = load('noSC/RF_Track.txt');

clf
subplot (2, 2, 1);
hold on
plot(AST.x(:,1), AST.x(:,6), '-');
plot(RFT(:,2)/1e3, RFT(:,11),  '-');
%legend('ASTRA', 'RF-Track');
xlabel('S [m]');
ylabel('\epsilon_{nx,y} [mm.mrad]')
axis([ 0 8.7 ])


subplot (2, 2, 2);
hold on
plot(AST.x    (:,1), AST.x    (:,4), '-');
plot(RFT(:,2)/1e3,   RFT(:,5),  '-');
legend('ASTRA', 'RF-Track');
xlabel('S [m]');
ylabel('\sigma_{x,y} [mm]')
axis([ 0 8.7 ])

subplot (2, 2, 3);
hold on
plot(AST.z    (:,1), AST.z    (:,3), '-');
plot(RFT(:,2)/1e3,   RFT(:,3),       '-');
%legend('ASTRA', 'RF-Track');
xlabel('S [m]');
ylabel('<K> [MeV]')
axis([ 0 8.7 ])

subplot (2, 2, 4);
hold on
plot(AST.z    (:,1), AST.z    (:,4), '-');
plot(RFT(:,2)/1e3,   RFT(:,7),       '-');
%legend('ASTRA', 'RF-Track');
xlabel('S [m]');
ylabel('\sigma_z [mm]')
axis([ 0 8.7 ])

print -dpng -r200 plot_noSC.png