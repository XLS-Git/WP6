AST.x = load('SC/XLS_benchmark.Xemit.001');
AST.z = load('SC/XLS_benchmark.Zemit.001');
AST_FFT.x = load('SC/XLS_benchmark.Xemit_FFT.001');
AST_FFT.z = load('SC/XLS_benchmark.Zemit_FFT.001');
TST = load('SC/Tstep.txt');
RFT = load('SC/RF_Track.txt');

clf
subplot (2, 2, 1);
hold on
plot(AST.x(:,1), AST.x(:,6), '-');
plot(AST_FFT.x(:,1), AST_FFT.x(:,6), '-');
plot(TST(:,2)/1e2, TST(:,6),   '-');
plot(RFT(:,2)/1e3, RFT(:,11),  '-');
%legend('ASTRA', 'ASTRA, LSPCH3D=T', 'TStep', 'RF-Track');
xlabel('S [m]');
ylabel('\epsilon_{nx} [mm.mrad]')
axis([ 0 8.7 ])


subplot (2, 2, 2);
hold on
plot(AST.x    (:,1), AST.x    (:,4), '-');
plot(AST_FFT.x(:,1), AST_FFT.x(:,4), '-');
plot(TST(:,2)/1e2,   TST(:,9),       '-');
plot(RFT(:,2)/1e3,   RFT(:,5),  '-');
legend('ASTRA', 'ASTRA, LSPCH3D=T ', 'TStep', 'RF-Track');
xlabel('S [m]');
ylabel('\sigma_{x} [mm]')
axis([ 0 8.7 ])

subplot (2, 2, 3);
hold on
plot(AST.z    (:,1), AST.z    (:,3), '-');
plot(AST_FFT.z(:,1), AST_FFT.z(:,3), '-');
plot(TST(:,2)/1e2,   TST(:,12),      '-');
plot(RFT(:,2)/1e3,   RFT(:,3),       '-');
%legend('ASTRA', 'ASTRA, LSPCH3D=T', 'TStep', 'RF-Track');
xlabel('S [m]');
ylabel('<K> [MeV]')
axis([ 0 8.7 ])

subplot (2, 2, 4);
hold on
plot(AST.z    (:,1), AST.z    (:,4), '-');
plot(AST_FFT.z(:,1), AST_FFT.z(:,4), '-');
plot(TST(:,2)/1e2,   TST(:,11),      '-');
plot(RFT(:,2)/1e3,   RFT(:,7),       '-');
%legend('ASTRA', 'ASTRA, LSPCH3D=T', 'TStep', 'RF-Track');
xlabel('S [m]');
ylabel('\sigma_z [mm]')
axis([ 0 8.7 ])

print -dpng -r200 plot_SC.png