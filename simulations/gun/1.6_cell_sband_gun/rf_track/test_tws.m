addpath([ pwd '/scripts' ]);

%% Load RF-Track
RF_Track;

T_period = 104.96935; % 1 / 2.856 mm/c

if false
    B = load_beam('data/XLS_benchmark.ini');
    P0 = Bunch6dT(B(1,:)); % reference particle
    B0 = Bunch6dT(B(1:10:end,:)); % beam
    
    O = TrackingOptions();
    O.odeint_algorithm = 'rkf45'; % pick your favorite algorithm, 'rk2', 'rkf45', 'leapfrog', 'analytic'
    O.odeint_epsabs = 1e-10; % required accuracy
    O.dt_mm = 10; % mm/c, integration step size
    O.t_max_mm = 0.0; % mm/c, tracks until t_max
    O.tt_dt_mm = 0; % mm/c tabulate the phase space every tt_dt_mm
    O.sc_dt_mm = 0; % mm/c tabulate the phase space every tt_dt_mm
    O.verbosity = 0; % 0..2
    O.open_boundaries = true; % it tracks until V.set_s1(), (default value: true)
    
    T = [];
    for phid=-90:90
        TWS1 = load_tws(-27.2e6, phid);
        V = Volume();
        V.add(TWS1.RFT, 0, 0, 0);
        P1 = V.track(P0, O);
        T = [ T ; phid P1.get_info().mean_K ];
        disp([ phid P1.get_info().mean_K ] )
    end
else
    TWS1 = load_tws(-1, 0);
    V = Volume();
    V.add(TWS1.RFT, 0, 0, 0);
    
    S_axis = linspace(0, 300, 500); % mm
    T_axis = linspace(0, T_period, 100)(1:end-1); % mm/c
    
    t0 = 1234;
    t_ = 0;
    for n=1:28
        for t = T_axis
            Ez = [];
            for s = S_axis
                [E,B] = V.get_field(0, 0, s, t0 + t_ + t);
                Ez = [ Ez ; E(3) ];
            end
            figure(1);
            plot(S_axis, Ez, 'b*-', t_ + t, 0, 'r*.');
            drawnow;
        end
        t_ += T_period;
    end
end