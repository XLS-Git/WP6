RF_Track;

global S Ez

T = load('fields/TWS.dat');
T(isnan(T)) = 0;
S = T(:,1)';
Ez = T(:,2);

freq = 2.856e9; % Hz
l_tw_cell = RF_Track.clight / freq / 3;

M  = S<0.05248467 | S>0.157454;
S = S(M);
Ez = Ez(M);

S = linspace(0, S(end) - l_tw_cell, length(S));

Ez /= max(Ez);


function M = merit(X)
global S Ez
RF_Track;
freq = 2.856e9; % Hz
SW = SW_Structure(X, freq, S(end), -1);       % half SW, entrance coupler
SW.set_t0(0.0);
SW.set_phid(90);
E_ = [];
for s=S*1e3
    [E,B] = SW.get_field(0,0,s,0);
    E_ = [ E_ ; E(3) ];
end
plot(S, E_, 'b-', S, Ez, 'r-');
drawnow;
M = norm(max(Ez, 0.25).*(E_ .- Ez), 1)
endfunction


X_opt = fminsearch(@merit, [ 0 0 0 0 0 0 0 0 0 0 0 ], optimset("TolFun", 1e-21, ...
                                                  'MaxFunEvals', 1e10));