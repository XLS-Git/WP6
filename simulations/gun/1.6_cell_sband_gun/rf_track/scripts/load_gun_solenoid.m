function Gun_Solenoid = load_gun_solenoid()
RF_Track;
T = load('fields/ELI_SolGun_3000G_2D_2v.poi');
S = T(:,1); % m
S0 = min(S); % m
S1 = max(S); % m
Bz = T(:,2); % T
Bz = Bz / max(Bz) * 0.315; % T
% smooth
new_N = 200;
new_S = linspace(S0, S1, new_N);
Gun_Solenoid.Bz = interp1(S(:), Bz(:), new_S, 'spline');
Gun_Solenoid.S0 = S0;
Gun_Solenoid.dS = range(new_S) / (new_N-1); % m
Gun_Solenoid.RFT = Static_Magnetic_FieldMap_1d_LINT(Gun_Solenoid.Bz, Gun_Solenoid.dS);

