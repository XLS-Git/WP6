function Gun = load_gun(phid)
RF_Track;
T = load('fields/2015Gfield.dat');
S = T(:,1); % m
L = range(S); % m
S0 = min(S); % m
S1 = max(S); % m
freq = 2.856e9; % Hz
Ez = T(:,2) / max(abs(T(:,2))) * -120e6; % V/m
dS = range(S) / (length(S)-1); % m
Gun.RFT = RF_FieldMap_1d(Ez, dS, L, freq, -1);
Gun.RFT.set_t0(0.0);
Gun.RFT.set_smooth(5);
Gun.RFT.set_phid(phid);