function TWS = load_tws(maxE, phid) % V/m degree
RF_Track;
freq = 2.856e9; % Hz

T = load('fields/TWS_RFT.dat');
S = T(:,1);
Ez = maxE*complex(T(:,2), T(:,3));

L_TW_cell = RF_Track.clight / freq / 3;
L_TW = 84 * L_TW_cell;
L_SW = 1.748602173202614e-01; % m

TWS.RFT = RF_FieldMap_1d_LINT(Ez, mean(diff(S)), S(end), freq, -1);
TWS.RFT.set_t0(0.0);
TWS.RFT.set_phid(phid);

TWS.S0 = -L_SW / 2; % m

return
TWS = Drift(L_TW);
TWS.set_static_Efield(0,0,maxE);

L = Lattice();
L.append(Drift(L_SW/2));
L.append(TWS);
L.append(Drift(L_SW/2));

TWS.RFT = L;

return

freq = 2.856e9; % Hz
n_cells = 84;

L_SW = 1.748602173202614e-01; % m

A_TW = [  0.0013221226239678355 -0.019243915862753889 0.29326128415666941 ...
          0.75002581163937343 -0.026626797415463262 0.0012616787911237499 ...
          8.3563612959562393e-06 ];

A_SW = [  6.192020020706116e-01  -6.953816923003616e-04 ...
          -3.279125409035794e-01   1.043714164141073e-04 ...
          8.257347316504823e-02  -1.849214043458953e-04 ...
          8.669595750563971e-03   2.327131602259179e-04 ...
          -1.597554108422184e-02  -4.434041973047716e-04 ...
          5.789529950425815e-03 ];

SWL = SW_Structure(maxE * A_SW, freq, L_SW, -0.5); % half SW, entrance coupler
SWL.set_t0(0.0);
SWL.set_phid(phid);

ph_adv = 2*pi/3; % radian, phase advance per cell
TW = TW_Structure(maxE * A_TW, -3, freq, ph_adv, n_cells);
TW.set_t0(0.0);
TW.set_phid(phid);

SWR = SW_Structure(maxE * A_SW, freq, L_SW, +0.5); % half SW, exit coupler
SWR.set_t0(0.0);
SWR.set_phid(phid);

TWS.RFT = Lattice();
TWS.RFT.append(SWL);
TWS.RFT.append(TW);
TWS.RFT.append(SWR);
