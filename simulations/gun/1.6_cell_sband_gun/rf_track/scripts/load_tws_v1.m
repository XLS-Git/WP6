function TWS = load_tws(maxE, phid) % V/m degree
RF_Track;
freq = 2.856e9; % Hz
L_SW = 1.748602173202614e-01; % m

A_TW = [  0.0013221226239678355 -0.019243915862753889 0.29326128415666941 ...
          0.75002581163937343 -0.026626797415463262 0.0012616787911237499 8.3563612959562393e-06 ];
A_SW = [  0.6192020020706116 -0.00069538169230036158 -0.32791254090357935 ...
          0.00010437141641410729 0.082573473165048233 - ...
          0.00018492140434589528 0.0086695957505639708 ...
          0.00023271316022591791 -0.015975541084221844 - ...
          0.00044340419730477159 0.0057895299504258152 ];

SWL = SW_Structure(maxE * A_SW, freq, L_SW, -0.5); % half SW, entrance coupler
SWL.set_t0(0.0);
SWL.set_phid(phid);

ph_adv = 2*pi/3; % radian, phase advance per cell
TW = TW_Structure(maxE * A_TW, -3, freq, ph_adv, 84);
TW.set_t0(0.0);
TW.set_phid(phid);

SWR = SW_Structure(maxE * A_SW, freq, L_SW, +0.5); % half SW, exit coupler
SWR.set_t0(0.0);
SWR.set_phid(phid);

TWS.RFT = Lattice();
TWS.RFT.append(SWL);
TWS.RFT.append(TW);
TWS.RFT.append(SWR);

return

%% input coupler
T = load('fields/SW_1DL_INFNSLAC.dat');
T(isnan(T)) = 0;
S = T(:,1);
L = range(T(:,1));
Ez = T(:,2) / max(abs(T(:,2))) * maxE; % MV/m
%% traveling wave
T = load('fields/TW_1D_INFNSLAC.dat');
T(isnan(T)) = 0;
S = [ S ; S(end) + T(:,1) ];
L += range(T(:,1));
E = complex(T(:,2), -T(:,3));
Ez = [ Ez ; E / max(real(E)) * maxE ];

%% output coupler
T = load('fields/SW_1DR_INFNSLAC.dat');
S = [ S ; S(end) + T(:,1) ];
L += range(T(:,1));
Ez = [ Ez ; T(:,2) / max(abs(T(:,2))) * maxE ]; % MV/m

new_N = 1000;
new_S = linspace(0, L, new_N);
TWS.Ez = interp1(S(:), Ez(:), new_S, 'linear');
TWS.dS = range(new_S) / (new_N-1); % m
TWS.RFT = RF_FieldMap_1d(TWS.Ez, TWS.dS, L, freq, -1);
TWS.RFT.set_t0(0.0);
TWS.RFT.set_phid(phid);