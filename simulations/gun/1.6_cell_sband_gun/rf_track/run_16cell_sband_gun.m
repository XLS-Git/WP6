addpath([ pwd '/scripts' ]);
system('mkdir -p results');

%% Load RF-Track
RF_Track;

%% Load the beam and define B0
global B0 P0
B = load_beam('data/XLS_benchmark.ini');
t0 = B(1,10);

P0 = Bunch6dT(B(1,:)); % reference particle
B0 = Bunch6dT(B); % beam
P0.t = t0;

global Gun  Gun_Solenoid 
Gun          = load_gun(57.2-3.75579399);
Gun_Solenoid = load_gun_solenoid();
TWS1         = load_tws(-27.2e6*1.0245, 324.09-0.79715-60.152467);
TWS2         = load_tws(-33.3e6*1.0050, 71.259-60.152467);

%% Define Volume
V = Volume();
V.add(Gun.RFT, 0, 0, 0);
V.add(Gun_Solenoid.RFT, 0, 0, 0.204 + Gun_Solenoid.S0);
V.add(TWS1.RFT, 0, 0, 1.5);
V.add(TWS2.RFT, 0, 0, 5.1);
V.set_s0(0.0); % entrance plane
V.set_s1(8.7); % exit plane
V.set_aperture(0.5, 0.5);

%% perfor tracking in two parts, first the low energy part with
%% smaller step size
SC = SpaceCharge_PIC_FreeSpace(32,32,32);
SC.set_smooth(2.0); % set smooth factor
RF_Track.SC_engine = SC;

T_emission = range(B0.get_phase_space('%t0')) % mm/c

O = TrackingOptions();
O.odeint_algorithm = 'rkf45'; % pick your favorite algorithm, 'rk2', 'rkf45', 'leapfrog', 'analytic'
O.odeint_epsabs = 1e-12; % required accuracy
O.dt_mm = 1; % mm/c, integration step size
O.t_max_mm = T_emission * 100; % mm/c, tracks until t_max
O.sc_dt_mm = T_emission / 10; % mm/c compute space-charge step every sc_dt_mm 
O.tt_dt_mm = 10; % mm/c tabulate the phase space every tt_dt_mm
O.wp_dt_mm = 0; % mm/c save the phase space every tt_dt_mm
O.verbosity = 1; % 0..2
O.open_boundaries = true; % it tracks until V.set_s1(), (default value: true)

tic
B1 = V.track(B0, O);
toc

% extract transport table for multi particle
T = V.get_transport_table(['%t %mean_S %mean_K %mean_Pz %sigma_X %sigma_Y %sigma_Z %emitt_x %emitt_y %emitt_z %emitt_4d %sigma_E']);
save -text results/T.txt T

if true 
    O.t_max_mm = 8000; % mm/c, tracks until t_max
    O.sc_dt_mm = 1; % mm/c tabulate the phase space every tt_dt_mm

    tic
    B1 = V.track(B1, O);
    toc

    % extract transport table for multi particle
    T = [ T ; V.get_transport_table(['%t %mean_S %mean_K %mean_Pz %sigma_X %sigma_Y %sigma_Z %emitt_x %emitt_y %emitt_z %emitt_4d %sigma_E']) ];
    save -text results/T.txt T
end

%B1 = V.get_bunch_at_s1();
A0 = B0.get_phase_space('%x %Px %y %Py %Z %Pz');
save -text results/input_beam_rfgun.dat A0
A1 = B1.get_phase_space('%x %Px %y %Py %Z %Pz');
save -text results/output_beam_rfgun.dat A1

zr=T(:,2)*1e-3;
E0r=T(:,3);
Pzr=T(:,4);
stdxr=T(:,5);
stdyr=T(:,6);
stdzr=T(:,7);
emtxr=T(:,8);
emtyr=T(:,9);
emtzr=T(:,10);
emt4dr=T(:,11);

%{
AXemit = load('astra/46_cell_xband_gun_astra.Xemit.001');
AZemit = load('astra/46_cell_xband_gun_astra.Zemit.001');
Aref   = load('astra/46_cell_xband_gun_astra.ref.001');
Abeam  = load('astra/46_cell_xband_gun_astra.0400.001');
Abeam(2:end,1:7) += repmat(Abeam(1,1:7), size(Abeam,1)-1,1); % ASTRA convention               
%}

figure(1)
clf
hold on
%plot(zr, Pzr,'b.-', Aref(:,1), Aref(:,3), 'r.-');
plot(zr, Pzr,'b.-');
legend('RF-Track', 'ASTRA', "location", 'southeast');
xlabel('z [m]');
ylabel('Pz [MeV/c]');
print -dpng results/plot_pz.png


figure(2)
clf
hold on
%plot(zr, E0r,'b.-', AZemit(:,1), AZemit(:,3), 'r.-');
plot(zr, E0r,'b.-');
legend('RF-Track', 'ASTRA', "location", 'southeast');
xlabel('z [m]');
ylabel('E [MeV]');
print -dpng results/plot_energy.png




figure(3)
clf
hold on
%plot(zr, stdxr,'b.-', AXemit(:,1), AXemit(:,4), 'r.-');
plot(zr, stdxr,'b.-');
legend('RF-Track', 'ASTRA', "location", 'northwest');
xlabel('z [m]');
ylabel('sigma_x [mm]');
print -dpng results/plot_sigmax.png


figure(4)
clf
hold on
%plot(zr, stdzr,'b.-', AZemit(:,1), AZemit(:,4), 'r.-');
plot(zr, stdzr,'b.-');
legend('RF-Track', 'ASTRA', "location", 'southeast');
xlabel('z [m]');
ylabel('sigma_z [mm]');
print -dpng results/plot_sigmaz.png

figure(5)
clf
hold on
%plot(zr, emtxr,'b.-', AXemit(:,1), AXemit(:,6), 'r.-');
plot(zr, emt4dr,'b.-');
legend('RF-Track', 'ASTRA', "location", 'northeast');
xlabel('z [m]');
ylabel('emit_{4d} [mm.mrad]');
print -dpng results/plot_emtx.png


figure(6)
clf
hold on
%plot(A1(:,1),A1(:,2),'b.', Abeam(:,1)*1e3, Abeam(:,4)/1e6, 'r.');
plot(A1(:,1),A1(:,2),'b.');
legend('RF-Track', 'ASTRA', "location", 'southeast');
xlabel('x [mm]');
ylabel('P_x [MeV/c]');
print -dpng results/plot_phase.png

figure(7)
clf
hold on
%plot(A1(:,5),A1(:,6),'b.', Abeam(:,3)*1e3 - 400, Abeam(:,6)/1e6, 'r.');
plot(A1(:,5),A1(:,6),'b.');
legend('RF-Track','ASTRA', "location", 'northwest');
xlabel('z [mm]');
ylabel('P_z [MeV/c]');
print -dpng results/plot_phase.png
