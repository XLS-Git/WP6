global S Ez

T = load('fields/TWS.dat');
T(isnan(T)) = 0;
S = 1e3*T(:,1)';
Ez = T(:,2);

M = S>1e3*0.05248467 & S<1e3*0.157454;
S = S(M);
Ez = Ez(M);
S -= 1e3*0.05248467;

function M = merit(X)
global S Ez
RF_Track;
freq = 2.856e9; % Hz
ph_adv = 2*pi/3; % radian, phase advance per cell
n_cells = 3; % number of cells, negative sign indicates a start from the beginning of the cell
TW = TW_Structure(X, -3, freq, ph_adv, n_cells);
TW.set_t0(0.0);
TW.set_phid(90);
E_ = [];
for s=S
    [E,B] = TW.get_field(0,0,s,0);
    E_ = [ E_ ; E(3) ];
end
plot(S, E_, 'b-', S, Ez, 'r-');
drawnow;
M = norm(E_ .- Ez, 1)
endfunction


X_opt = fminsearch(@merit, [ 0 0 0 0 0 0 0 ], optimset("TolFun", 1e-21, ...
                                                  'MaxFunEvals', 1e10));