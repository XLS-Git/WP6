addpath([ pwd '/scripts' ]);
system('mkdir -p results');

%% Load RF-Track
RF_Track;
%RF_Track.number_of_threads = 1;

%% Load the beam and define B0
global B0 P0
B = load_beam('data/XLS_benchmark.ini');
P0 = Bunch6dT(B(1,:)); % reference particle
B0 = Bunch6dT(B(1:10:end,:)); % beam

function E = energy(X)
    global P0 B0
    RF_Track;
    Gun          = load_gun(57.2-3.75579399);
    Gun_Solenoid = load_gun_solenoid();
    TWS1         = load_tws(-27.2e6*1.0245, 324.09-0.79715-60.152467);
    TWS2         = load_tws(-33.3e6*1.0050, 71.259-60.152467);
        
		%% Define Volume
		V = Volume();
		V.add(Gun.RFT, 0, 0, 0.0);
		V.add(Gun_Solenoid.RFT, 0, 0, 0.204 + Gun_Solenoid.S0);
		V.add(TWS1.RFT, 0, 0, 1.5);
		V.add(TWS2.RFT, 0, 0, 5.1);
		V.set_s0(0.0); % entrance plane
		V.set_s1(8.7); % exit plane
		V.set_aperture(0.5, 0.5);

    %% perform tracking in two parts, first the low energy part with
    %% smaller step size
    O = TrackingOptions();
    O.odeint_algorithm = 'rkf45'; % pick your favorite algorithm, 'rk2', 'rkf45', 'leapfrog', 'analytic'
    O.odeint_epsabs = 1e-10; % required accuracy
    O.dt_mm = 0.1; % mm/c, integration step size
    O.t_max_mm = 0.0; % mm/c, tracks until t_max
    O.tt_dt_mm = 0; % mm/c tabulate the phase space every tt_dt_mm
    O.sc_dt_mm = 0; % mm/c tabulate the phase space every tt_dt_mm
    O.verbosity = 0; % 0..2
    O.open_boundaries = true; % it tracks until V.set_s1(), (default value: true)

    P1 = V.track(P0, O).get_info();
    B1 = V.track(P0, O).get_info(); % tracks the reference particle to set the
                                    % arrival time at 
    mean_S = B1.mean_S
    E = [ B1.mean_K B1.sigma_Z, B1.emitt_4d ]

endfunction

if false
    T = [];
    for phid=0:5:360
        T = [ T ; phid energy(phid) ];
    end
    figure(1); plot(T(:,1), T(:,2));
    figure(2); plot(T(:,1), T(:,3));
    figure(3); plot(T(:,1), T(:,4));
else
    fminbnd (@(X) abs(139.184-energy(X)(1)), 0.9, 1.2)
end
return

tic
P1 = V.track(P0, O); % tracks the reference particle to set the arrival time at structures
%%B1 = V.track(B0, O);
toc

return

% extract transport table for multi particle
T = V.get_transport_table(['%t %mean_S %mean_K %mean_Pz %sigma_X %sigma_Y %sigma_Z %emitt_x %emitt_y %emitt_z']);
save -text results/T.txt T


% B1 = V.get_bunch_at_s1();
A0 = B1.get_phase_space('%x %Px %y %Py %Z %Pz');
save -text results/output_beam_rfgun.dat A0

zr=T(:,2)*1e-3;
E0r=T(:,3);
Pzr=T(:,4);
stdxr=T(:,5);
stdyr=T(:,6);
stdzr=T(:,7);
emtxr=T(:,8);
emtyr=T(:,9);
emtzr=T(:,10);

%{
AXemit = load('astra/46_cell_xband_gun_astra.Xemit.001');
AZemit = load('astra/46_cell_xband_gun_astra.Zemit.001');
Aref   = load('astra/46_cell_xband_gun_astra.ref.001');
Abeam  = load('astra/46_cell_xband_gun_astra.0400.001');
Abeam(2:end,1:7) += repmat(Abeam(1,1:7), size(Abeam,1)-1,1); % ASTRA convention               
%}

figure(1)
clf
hold on
%plot(zr, Pzr,'b.-', Aref(:,1), Aref(:,3), 'r.-');
plot(zr, Pzr,'b.-');
legend('RF-Track', 'ASTRA', "location", 'southeast');
xlabel('z [m]');
ylabel('Pz [MeV/c]');
print -dpng results/plot_pz.png


figure(2)
clf
hold on
%plot(zr, E0r,'b.-', AZemit(:,1), AZemit(:,3), 'r.-');
plot(zr, E0r,'b.-');
legend('RF-Track', 'ASTRA', "location", 'southeast');
xlabel('z [m]');
ylabel('E [MeV]');
print -dpng results/plot_energy.png




figure(3)
clf
hold on
%plot(zr, stdxr,'b.-', AXemit(:,1), AXemit(:,4), 'r.-');
plot(zr, stdxr,'b.-');
legend('RF-Track', 'ASTRA', "location", 'northwest');
xlabel('z [m]');
ylabel('sigma_x [mm]');
print -dpng results/plot_sigmax.png


figure(4)
clf
hold on
%plot(zr, stdzr,'b.-', AZemit(:,1), AZemit(:,4), 'r.-');
plot(zr, stdzr,'b.-');
legend('RF-Track', 'ASTRA', "location", 'southeast');
xlabel('z [m]');
ylabel('sigma_z [mm]');
print -dpng results/plot_sigmaz.png

figure(5)
clf
hold on
%plot(zr, emtxr,'b.-', AXemit(:,1), AXemit(:,6), 'r.-');
plot(zr, emtxr,'b.-');
legend('RF-Track', 'ASTRA', "location", 'northeast');
xlabel('z [m]');
ylabel('emit_x [mm.mrad]');
print -dpng results/plot_emtx.png


figure(6)
clf
hold on
%plot(A0(:,1),A0(:,2),'b.', Abeam(:,1)*1e3, Abeam(:,4)/1e6, 'r.');
plot(A0(:,1),A0(:,2),'b.');
legend('RF-Track', 'ASTRA', "location", 'southeast');
xlabel('x [mm]');
ylabel('P_x [MeV/c]');
print -dpng results/plot_phase.png

figure(7)
clf
hold on
%plot(A0(:,5),A0(:,6),'b.', Abeam(:,3)*1e3 - 400, Abeam(:,6)/1e6, 'r.');
plot(A0(:,5),A0(:,6),'b.');
legend('RF-Track','ASTRA', "location", 'northwest');
xlabel('z [mm]');
ylabel('P_z [MeV/c]');
print -dpng results/plot_phase.png
