
# # # 

proc plota {namee} {
    
    set flroot [file rootname $namee]
    puts $flroot
    set fl [open "$flroot.pl.001" w]
    
puts $fl "
set term qt enhanced size 1500, 400 font 'Times , 14' 
set grid lt 0
set multiplot layout 1, 2
set xlabel ' Distance (m)' font 'Times , 14' offset 0,0.5
set key top center

set ytics nomirror
set y2tics
set tics out
set autoscale  y
set autoscale y2

set ylabel ' E (MeV)' font 'Times , 14'  
set y2label '\{/Symbol s\}_\{z\} (mm)' font 'Times , 14'  offset -2,0
set yrange \[0:*\]
set y2range \[0:*\]
pl '$flroot.Zemit.001' u 1:3 w l lw 3 lc 'blue' ti 'E', '$flroot.Zemit.001' u 1:4 w l lw 3 lc 'black' dt 2 axes x1y2 ti '\{/Symbol s\}_\{z\}'

set ylabel ' \{/Symbol s\}_\{x\} (\{/Symbol m\}mrad)' font 'Times , 14'  
set y2label ' \{/Symbol e\}_\{x\} (mm)' font 'Times , 14'  offset -2,0
set yrange \[0:*\]
set y2range \[0:*\]
pl '$flroot.Xemit.001' u 1:4 w l lw 3 lc 'blue' ti '\{/Symbol s\}_\{x\}', '$flroot.Xemit.001' u 1:6 w l lw 3 lc 'black' dt 2 axes x1y2 ti '\{/Symbol e\}_\{x\}'

unset multiplot 

"
    
close $fl
    
exec gnuplot -persist $flroot.pl.001 
    

}
    
    
proc color {foreground text} {
    return [exec tput setaf $foreground]$text[exec tput sgr0]
}



proc print_ellapsed_time { t0  } {

set t1 [clock clicks -milliseconds] 
set dt_usec_all [expr $t1-$t0]
set dt_sec_all [expr int($dt_usec_all/1000)]
set dt_min_all [expr int($dt_sec_all/60)]
set dt_hou_all [expr int($dt_min_all/60)]
set dt_day_all [expr int($dt_hou_all/24)]

set dt_use [expr $dt_usec_all-$dt_sec_all*1000]
set dt_sec [expr $dt_sec_all-$dt_min_all*60]
set dt_min [expr $dt_min_all-$dt_hou_all*60]
set dt_hou [expr $dt_hou_all-$dt_day_all*24]

puts [color 2 "******************************************************"]
puts [color 2 [format   "**           %2d h %2d m %2d s %3d µs for run          **" $dt_hou $dt_min $dt_sec $dt_use ]]
puts [color 2 "******************************************************"]
}





