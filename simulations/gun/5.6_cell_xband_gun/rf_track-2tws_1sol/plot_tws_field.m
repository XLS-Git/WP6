addpath([ pwd '/scripts' ]);

%% Load RF-Track
RF_Track;

freq = 11.99206070e9; % Hz
T_period = RF_Track.s / freq; % mm/c

TWS = load_tws(90, 1e-6);

%% plot Ez along the structure, in time
T_axis = linspace(0, T_period, 80); % mm/c
Za = linspace(0, 100, 100); % mm
%%Za = TWS.get_length()*1e3 + linspace(-100, 0, 100); % mm

for t=T_axis
    E_TW = [];
    B_TW = [];
    for z=Za
        [E,B] = TWS.get_field(0, 1, z, t);
        E_TW = [ E_TW E ];
        B_TW = [ B_TW B ];
    end
    subplot(2,1,1);
    plot(Za, E_TW(3,:));
    title(sprintf('t = %2.f%% of period', t*100/T_period));
    xlabel('Z [mm]');
    ylabel('E_z [V/m]');
    legend('Fourier');
    axis([ Za(1) Za(end) -1.2 1.2 ]);
    drawnow;
    subplot(2,1,2);
    plot(Za, B_TW(1,:));
    xlabel('Z [mm]');
    ylabel('B_x [T]');
    legend('Fourier');
    axis([ Za(1) Za(end) ]);
    drawnow;
end



