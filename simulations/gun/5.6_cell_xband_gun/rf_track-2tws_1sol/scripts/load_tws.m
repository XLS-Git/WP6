function TWS = load_tws(phid,grad)
    RF_Track;
    %%
    Ncell = 108; % number of cells
    freq = 11.99206070e9; % Hz
    lambda = RF_Track.clight / freq; % m
    ph = 2*pi/3;
    
    load scripts/A_SW.dat
    load scripts/A_TW.dat
    
    A_SW = A_SW * grad * 1e6; % V/m
    A_TW = A_TW * grad * 1e6; % V/m
    
    CI = SW_Structure(A_SW, freq, lambda, -0.5);
    TW = TW_Structure(A_TW, first_index, freq, ph, Ncell);
    CO = SW_Structure(A_SW, freq, lambda, 0.5);

    CI.set_phid(phid-90);
    TW.set_phid(phid-90);
    CO.set_phid(phid-90);
    
    L = Lattice();
    L.append(CI);
    L.append(TW);
    L.append(CO);
    
    TWS = Volume();
    TWS.add(L, 0, 0, 0);
    
endfunction
