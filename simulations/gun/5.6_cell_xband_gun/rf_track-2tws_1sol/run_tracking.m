addpath([ pwd '/scripts' ]);

system('mkdir -p results');

%% Load RF-Track
RF_Track;
RF_Track.number_of_threads = 8;

%% Setup space-charge
SC = SpaceCharge_PIC_FreeSpace(32,32,32);
SC.set_smooth(0.0); % set smooth factor
SC.set_mirror(0.0); % set position of cathode
RF_Track.SC_engine = SC;

%% Load the beam and define B0
global B0t P0t
%B = load_beam('../astra/part_10k_2ps_0.25mm.dat.gz');
B = load_beam('../astra/part_100k.dat.gz');
B0t = Bunch6dT(B); % beam
P0t = Bunch6dT(B(1,:)); % reference particle

global setup
setup.gun_phid = 190 - 90; % deg
setup.gun_grad = 200; % MV/m
setup.gun_sol_Bmax = 0.480; % T
setup.lin_phid = -17.3; % deg
setup.lin_grad = 65 * (91.85 - 7.5914) / (89.6 - 7.5914); % MV/m
setup.lin_sol_Bmax = 0.3 * 1.25; % T
setup.gun_sol_align = 0; % urad, misalignment

function [B1,T] = track()
    RF_Track;
    global B0t P0t setup
    %% setup volume
    V = setup_volume(setup);
    %% tracking of the refernece particle
    O = TrackingOptions();
    O.odeint_algorithm = 'analytic'; % pick your favorite algorithm, 'rk2', 'rkf45', 'leapfrog', 'analytic'
    O.odeint_epsabs = 1e-10; % required accuracy
    O.dt_mm = 0.01 * RF_Track.ps; % mm/c, integration step size
    O.open_boundaries = false;
    % track reference particle
    P1t = V.track(P0t, O);
    %% tracking
    tic
    tt_fmt_str = '%mean_S %mean_K %sigma_X %sigma_Y %sigma_Z %emitt_x %emitt_y %emitt_4d %sigma_E';
    O.t_max_mm = 20 * RF_Track.ps; % mm/c, tracks until t_max
    O.tt_dt_mm = 1; % mm/c, tabulate beam properties every tt_dt_mm 
    O.sc_dt_mm = 0.01 * RF_Track.ps; % mm/c, compute space-charge step every sc_dt_mm 
    O.dt_mm = 1 * RF_Track.ps; % mm/c, integration step size
    O.verbosity = 0;
    %% part 1
    B1t = V.track(B0t, O);
    B1t.save('B1t.dat');
    % extract transport table
    T = V.get_transport_table(tt_fmt_str);
    %% part 2
    O.t_max_mm = Inf; % mm/c, tracks until t_max
    O.tt_dt_mm = 5; % mm/c, tabulate beam properties every tt_dt_mm 
    O.sc_dt_mm = 0.1; % mm/c, compute space-charge step every sc_dt_mm 
    B1t = V.track(B1t, O);
    % extend transport table
    T = [ T ; V.get_transport_table(tt_fmt_str) ];
    save -text results/track.dat T
    toc
    B1 = V.get_bunch_at_s1();
end

[B1,T] = track();

%% Save input / output distributions
A0 = B0t.get_phase_space('%x %Px %y %Py %t0 %Pz');
A1 = B1.get_phase_space('%x %xp %y %yp %dt %P %K');

A1(:,5) -= mean(A1(:,5));

save -text results/input_beam_RFGun.dat A0
save -text results/output_beam_RFGun.dat A1

%% Do a lot of plots
if 1
V = setup_volume(setup);
S_axis = linspace(0, 8, 2000); % m
Br = [];
Bz = [];
Er = [];
Ez = [];
for S = S_axis
    [E0_,B0_] = V.get_field(0, 0, S*1e3, 0);
    [E1_,B1_] = V.get_field(5, 0, S*1e3, 0);
    [E2_,B2_] = V.get_field(10, 0, S*1e3, 0);
    Br = [ Br ; B0_(1) B1_(1) B2_(1) ];
    Bz = [ Bz ; B0_(3) B1_(3) B2_(3) ];
    Er = [ Er ; E0_(1) E1_(1) E2_(1) ];
    Ez = [ Ez ; E0_(3) E1_(3) E2_(3) ];
end

figure(1)
clf
hold on
plot(S_axis, Er(:,3) / 1e6, 'b-');
plot(S_axis, Er(:,2) / 1e6, 'r-');
plot(S_axis, Er(:,1) / 1e6, 'g-');
legend('r=2 mm ', 'r=1 mm ', 'r=0 mm ');
xlabel('S [m]');
ylabel('E_r [MV/m]');
print -dpng results/plot_Er.png

clf
hold on
plot(S_axis, Ez(:,3) / 1e6, 'b-');
plot(S_axis, Ez(:,2) / 1e6, 'r-');
plot(S_axis, Ez(:,1) / 1e6, 'g-');
legend('r=2 mm ', 'r=1 mm ', 'r=0 mm ');
xlabel('S [m]');
ylabel('E_z [MV/m]');
print -dpng results/plot_Ez.png

clf
hold on
plot(S_axis, Br(:,3) / 1e-3, 'b-');
plot(S_axis, Br(:,2) / 2e-3, 'r-');
plot(S_axis, Br(:,1) / 3e-3, 'g-');
legend('r=2 mm ', 'r=1 mm ', 'r=0 mm ');
xlabel('S [m]');
ylabel('dB_r / dr [T/m]');
print -dpng results/plot_Br.png

clf
hold on
plot(S_axis, Bz(:,3), 'b-');
plot(S_axis, Bz(:,2), 'r-');
plot(S_axis, Bz(:,1), 'g-');
legend('r=2 mm ', 'r=1 mm ', 'r=0 mm ');
xlabel('S [m]');
ylabel('B_z [T]');
print -dpng results/plot_Bz.png

figure(2)
clf
scatter(A1(:,1), A1(:,2))
xlabel('x [mm]');
ylabel('x'' [mrad]');
print -dpng results/plot_xxp.png

figure(3)
clf
scatter(A1(:,3), A1(:,4))
xlabel('y [mm]');
ylabel('y'' [mrad]');
print -dpng results/plot_yyp.png

figure(4)
clf
A1(:,5) -= mean(A1(:,5));
scatter(A1(:,5) / RF_Track.ns, A1(:,6))
xlabel('dt [ns]');
ylabel('P [MeV/c]');
print -dpng results/plot_dtP.png
endif

load results/track.dat

figure(5)
plot(T(:,1)/1e3, T(:,6), T(:,1)/1e3, T(:,7), T(:,1)/1e3, T(:,8));
legend('emitt_x ', 'emitt_y ', 'emitt_{4d} ');
xlabel('S [m]');
ylabel('emitt [mm.mrad]');
print -dpng results/plot_emitt.png

figure(6)
plot(T(:,1)/1e3, T(:,3), T(:,1)/1e3, T(:,4));
legend('\sigma_x ', '\sigma_y ');
xlabel('S [m]');
ylabel('\sigma [mm]');
print -dpng results/plot_sigma.png

figure(8)
ax = plotyy(T(:,1)/1e3, T(:,5), T(:,1)/1e3, T(:,2));
xlabel('S [m]');
ylabel(ax(1), '\sigma_z [mm] ');
ylabel(ax(2), 'E_{kinetic} [MeV] ');
print -dpng results/plot_E_sigma_Z.png

clf
plot(T(:,1)/1e3, T(:,8));
xlabel('S [m]');
ylabel('emitt_{4d} [mm.mrad]');
print -dpng results/plot_emitt_4d.png

