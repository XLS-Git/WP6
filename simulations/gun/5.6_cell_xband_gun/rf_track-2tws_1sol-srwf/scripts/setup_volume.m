function V = setup_volume(setup)
    RF_Track;
    % gun
    Gun = load_gun(setup.gun_phid, setup.gun_grad); 
    % gun solenoid
    [Sol, Sol_S] = load_gun_solenoid(setup.gun_sol_Bmax); 
    % linac traveling wave
    TWL = load_tws_1d(setup.lin_phid, setup.lin_grad);
    TWL_dS = 1.1; % m
    TWL_S = 0.568; % m
    % linac traveling wave - solenoid
    [TWS, TWS_S] = load_tws_solenoid(setup.lin_sol_Bmax);
    % volume
    V = Volume();
    V.add(Gun, 0, 0, 0);
    V.add(Sol, 0, 0, Sol_S, 0, 0, setup.gun_sol_align/1e6);
    for i=1:2
        V.add(TWL, 0, 0, TWL_S + (i-1) * TWL_dS);
    end
    for i=1:1
        V.add(TWS, 0, 0, TWL_S + TWS_S + (i-1) * TWL_dS);
        V.add(TWS, 0, 0, TWL_S + TWS_S + (i-1) * TWL_dS + 0.3);
        V.add(TWS, 0, 0, TWL_S + TWS_S + (i-1) * TWL_dS + 0.6);
    end
endfunction
