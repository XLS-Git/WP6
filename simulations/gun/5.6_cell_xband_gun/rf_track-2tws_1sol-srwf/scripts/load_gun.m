function Gun = load_gun(phid, grad)
    RF_Track;
    T = load('../astra/X-BAND_GUN_5.6CELL.DAT');
    S = T(:,1); % m
    L = range(S); % m
    S0 = min(S); % m
    S1 = max(S); % m
    freq = 11.994e9; % Hz
    Ez = T(:,2) * grad * 1e6; % V/m
    Gun = RF_FieldMap_1d(Ez, S(2) - S(1), L, freq, +1);
    Gun.set_phid(phid);
    Gun.set_smooth(10);
endfunction

