function [Solenoid,S0] = load_tws_solenoid(Bmax)
    RF_Track;
    T = load('../astra/XBAND_CAVITY_SOLENOID_20CM_30-05-19_V1.DAT');
    S = T(:,1); % m
    S0 = min(T(:,1)); % m
    Bz = T(:,2) * Bmax; % T
    Solenoid = Static_Magnetic_FieldMap_1d(Bz, S(2) - S(1));
    Solenoid.set_smooth(8);
endfunction
