grad = 65; % MV/m
phid = 0; % deg

RF_Track;
%%
Ncell = 3; % number of cells
freq = 11.99206070e9; % Hz
lambda = RF_Track.clight / freq; % m
%%    
S = fileread('../astra/TWS_XBAND_120DEG.DAT');
T = strsplit(S, '\n');
L = str2num(T{1}); % first line of the superfish file
A = str2num([ T{2:end} ]); % all the rest of the file
D = transpose(reshape(A, 2, length(A) / 2)); % into matrix form
I0 = find(D(:,1) <= L(1))(end); % from (to be included)
I1 = find(D(:,1) >= L(2))(1); % to (to be included)
%%
IN  =  D(1:I0,2);
OUT = D(I1:end,2); % full structure
SW  = [ IN ; OUT ];
%%
Lco_i = L(1); % m, input coupler length (standing wave)
Lco_f = D(end,1) - L(2); % m, output coupler length (standing wave)
Lcell = lambda * L(3) / L(4); % m
ph = 2*pi * L(3) / L(4); % rad, phase advance per cell
dS = D(2,1) - D(1,1); % m, mesh step
Ltw = Ncell * Lcell; % m, length of the traveling wave
Lstr = Lco_i + Ltw + Lco_f; % m, total structure length
Z = 0:dS:Lstr; % prepare mesh
M = Z>=Lco_i & Z<=(Ltw+Lco_f); % pick the mesh points in the traveling wave part
Z0 = Lco_i + rem(Z(M) - Lco_i, lambda);
Zp = Lco_i + rem(Z(M) - Lco_i + Lcell, lambda);
% from the standing wave computes the traveling wave
SW0 = interp1(D(:,1), D(:,2), Z0(:));
SWp = interp1(D(:,1), D(:,2), Zp(:));
TW  = sqrt(-1) * (SWp - SW0 * exp(sqrt(-1)*ph)) / (2*sin(ph));
TW *= IN(end) / real(TW(1)); % normalize to 1
ALL = [ IN ; TW(2:end-1) ; OUT ]; % full structure
ALL = [ IN(1:end-1) ; TW ; OUT(2:end) ]; % full structure
figure(1); plot(real(ALL))
figure(2); plot(imag(ALL))
TW_ = TW;

%% fit

T_period = RF_Track.s / freq; % mm/c

clear SF SW TW
global SF SW TW

if 1
    global SF SW TW

    SF = RF_FieldMap_1d([ IN ; OUT ], dS, -1, freq, +1);
    SF.set_smooth(5);
    SF.set_t0(0.0);
    
    SW = SW_Structure(1, freq, lambda, -1);
    SW.set_t0(-T_period / 4);

    function M = merit_SW(X)
        RF_Track;
        global SF SW iteration
        SW.set_coefficients(X);
        freq = 11.99206070e9; % Hz
        T_period = RF_Track.s / freq; % mm/c
        T_axis = 0; %linspace(0, T_period, 80); % mm/c
        Z_axis = linspace(0, min(SF.get_length(), SW.get_length())*1e3, 80)(2:end-1); % mm
        M = 0;
        for t=T_axis
            E_SW = [];
            B_SW = [];
            E_SF = [];
            B_SF = [];
            for z=Z_axis
                [E,B] = SF.get_field(0, 0.1, z, t);
                E_SF = [ E_SF E(3) ];
                B_SF = [ B_SF B(1) ];
                [E,B] = SW.get_field(0, 0.1, z, t);
                E_SW = [ E_SW E(3) ];
                B_SW = [ B_SW B(1) ];
            end
            M += norm(E_SW .- E_SF);
        end
        if rem(iteration++, 50) == 0
            figure(3)
            subplot(2,1,1)
            plot(Z_axis, E_SW, Z_axis, E_SF);
            title(sprintf('t = %2.f%% of period', t*100/T_period));
            xlabel('Z [mm]');
            ylabel('E_z [V/m]');
            subplot(2,1,2)
            plot(Z_axis, B_SW, Z_axis, B_SF);
            title(sprintf('t = %2.f%% of period', t*100/T_period));
            xlabel('Z [mm]');
            ylabel('B_x [T]');
            drawnow;
        end
    endfunction
    
    global iteration
    iteration = 0;
    
    %load A_SW.dat
    A_SW = fminsearch(@merit_SW, zeros(12, 1))
    save -text A_SW.dat A_SW
    
    global first_index;
    first_index = -8;
    
    function M = merit_TW(X)
        RF_Track;
        global SF TW iteration first_index
        TW.set_coefficients(X, first_index);
        freq = 11.99206070e9; % Hz
        T_period = RF_Track.s / freq; % mm/c
        T_axis = 0; %linspace(0, T_period, 80); % mm/c
        Z_axis = linspace(0, min(SF.get_length(), TW.get_length())*1e3, 80)(2:end-1); % mm
        M = 0;
        for t=T_axis
            E_TW = [];
            B_TW = [];
            E_SF = [];
            B_SF = [];
            for z=Z_axis
                [E,B] = SF.get_field(0, 0.1, z, t);
                E_SF = [ E_SF E(3) ];
                B_SF = [ B_SF B(1) ];
                [E,B] = TW.get_field(0, 0.1, z, t);
                E_TW = [ E_TW E(3) ];
                B_TW = [ B_TW B(1) ];
            end
            M += norm(E_TW .- E_SF);
        end
        if rem(iteration++, 100) == 0
            figure(4)
            subplot(2,1,1)
            plot(Z_axis, E_TW, Z_axis, E_SF);
            title(sprintf('t = %2.f%% of period', t*100/T_period));
            xlabel('Z [mm]');
            ylabel('E_z [V/m]');
            subplot(2,1,2)
            plot(Z_axis, B_TW, Z_axis, B_SF);
            title(sprintf('t = %2.f%% of period', t*100/T_period));
            xlabel('Z [mm]');
            ylabel('B_x [T]');
            drawnow;
        end
    endfunction

    SF = RF_FieldMap_1d(TW_, dS, -1, freq, +1);
    SF.set_t0(0.0);

    TW = TW_Structure(1, first_index, freq, ph, 3);
    TW.set_t0(-T_period / 4);

    global iteration
    iteration = 0;
    
    A_TW = fminsearch(@merit_TW, zeros(2*abs(first_index)+1, 1))
    save -text A_TW.dat A_TW first_index
    
else
    load A_SW.dat
    load A_TW.dat
end
    
SF = RF_FieldMap_1d(ALL, dS, -1, freq, +1);
SF.set_t0(0.0);
SF.set_smooth(5);

%% create standing and traveling wave structure
l_cell = lambda; % m, length of the full SW cell
n_cells = -0.5; % number of cells              
                % a negative sign indicates a start from the beginning of the cell
                % a positive sign indicates a start from the middle of the cell

CI = SW_Structure(A_SW, freq, l_cell, -0.5);
TW = TW_Structure(A_TW, first_index, freq, ph, 3);
CO = SW_Structure(A_SW, freq, l_cell, 0.5);
%CI.set_t0(-T_period / 4);
%TW.set_t0(-T_period / 4);
%CO.set_t0(-T_period / 4);
CI.set_phid(90.0)
TW.set_phid(90.0)
CO.set_phid(90.0)


AS = Lattice();
AS.append(CI);
AS.append(TW);
AS.append(CO);

V = Volume();
V.add(AS, 0, 0, 0);

%% plot Ez along the structure, in time
T_axis = linspace(0, T_period, 80); % mm/c
Z_axis = linspace(0, V.get_length()*1e3, 100); % mm
for t=T_axis
    E_TW = [];
    B_TW = [];
    E_SF = [];
    B_SF = [];
    for z=Z_axis
        [E,B] = V.get_field(0, 0, z, t);
        E_TW = [ E_TW E ];
        B_TW = [ B_TW B ];
        [E,B] = SF.get_field(0, 0, z, t);
        E_SF = [ E_SF E ];
        B_SF = [ B_SF B ];
    end
    figure(5)
    clf ; hold on
    plot(Z_axis, E_TW(3,:));
    plot(Z_axis, E_SF(3,:));
    title(sprintf('t = %2.f%% of period', t*100/T_period));
    xlabel('Z [mm]');
    ylabel('E_z [V/m]');
    legend('Fourier', '1D extrapolation');
    axis([ 0 Z_axis(end) -1.2 1.2 ]);
    drawnow;
    figure(6)
    clf ; hold on
    plot(Z_axis, B_TW(1,:));
    plot(Z_axis, B_SF(1,:));
    title(sprintf('t = %2.f%% of period', t*100/T_period));
    xlabel('Z [mm]');
    ylabel('B_x [T]');
    legend('Fourier', '1D extrapolation');
    axis([ 0 Z_axis(end) ]);
    drawnow;
end



