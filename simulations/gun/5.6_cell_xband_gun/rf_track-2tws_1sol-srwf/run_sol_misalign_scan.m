addpath([ pwd '/scripts' ]);

system('mkdir -p results');

%% Load RF-Track
RF_Track;

%% Setup space-charge
SC = SpaceCharge_PIC_FreeSpace(32,32,32);
SC.set_smooth(0.0); % set smooth factor
SC.set_mirror(0.0); % set position of cathode
RF_Track.SC_engine = SC;

%% Load the beam and define B0
global B0t P0t
%B = load_beam('../astra/part_10k_2ps_0.25mm.dat.gz');
B = load_beam('../astra/part_100k.dat.gz');
B0t = Bunch6dT(B); % beam
P0t = Bunch6dT(B(1,:)); % reference particle

global setup
setup.gun_phid = 190 - 90; % deg
setup.gun_grad = 200; % MV/m
setup.gun_sol_Bmax = 0.480; % T
setup.lin_phid = -17.3; % deg
setup.lin_grad = 65 * (91.85 - 7.5914) / (89.6 - 7.5914); % MV/m
setup.lin_sol_Bmax = 0.3; % T
setup.gun_sol_align = str2num(argv(){1}) % urad

function [B1,T] = track()
    RF_Track;
    global B0t P0t setup
    %% setup volume
    V = setup_volume(setup);
    %% tracking of the refernece particle
    O = TrackingOptions();
    O.odeint_algorithm = 'analytic'; % pick your favorite algorithm, 'rk2', 'rkf45', 'leapfrog', 'analytic'
    O.odeint_epsabs = 1e-10; % required accuracy
    O.dt_mm = 0.01 * RF_Track.ps; % mm/c, integration step size
    P1t = V.track(P0t, O);
    %% tracking - part 1
    tic
    tt_fmt_str = '%mean_S %mean_K %sigma_X %sigma_Y %sigma_Z %emitt_x %emitt_y %emitt_4d %sigma_E %mean_X %mean_Y %N';
    O.t_max_mm = 20 * RF_Track.ps; % mm/c, tracks until t_max
    O.tt_dt_mm = 1; % mm/c, tabulate beam properties every tt_dt_mm 
    O.sc_dt_mm = 0.001 * RF_Track.ps; % mm/c, compute space-charge step every sc_dt_mm 
    O.dt_mm = 1 * RF_Track.ps; % mm/c, integration step size
    O.verbosity = 0;
    B1t = V.track(B0t, O);
    % extract transport table
    T = V.get_transport_table(tt_fmt_str);
    %% part 2
    O.t_max_mm = Inf; % mm/c, tracks until t_max
    O.tt_dt_mm = 5; % mm/c, tabulate beam properties every tt_dt_mm 
    O.sc_dt_mm = 0.1; % mm/c, compute space-charge step every sc_dt_mm 
    B1t = V.track(B1t, O);
    % extend transport table
    T = [ T ; V.get_transport_table(tt_fmt_str) ];
    save(sprintf('results/track_%d.dat',int32(setup.gun_sol_align)),'-text','T');
    toc
    B1 = V.get_bunch_at_s1();
end

[B1,T] = track();
