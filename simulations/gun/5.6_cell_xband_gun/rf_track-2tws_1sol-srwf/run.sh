#!/bin/bash
source /cvmfs/clicbp.cern.ch/x86_64-centos7-gcc8-opt/setup.sh

cd ~/rf_track-2tws_1sol-srwf

octave-cli -q run_sol_misalign_scan.m $1
