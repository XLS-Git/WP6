addpath([ pwd '/scripts' ]);
system('mkdir -p results');

%% Load RF-Track
RF_Track;

%% Load the beam and define B0
global B0
B = load_beam('for_andrea/part_10k_2ps_0.25mm.dat');
B0 = Bunch6dT(B);

%% Load the field map of the elements
global Gun TWS Gun_Solenoid TWS_Solenoid
Gun          = load_gun();
TWS          = load_tws();
Gun_Solenoid = load_gun_solenoid();
TWS_Solenoid = load_tws_solenoid();

% load optimum saved by run_rfgun_optimise_rf_phases.m
load results/optimum.dat
Gun.RFT.set_phid(Optimum(1));
TWS.RFT.set_phid(Optimum(2));

%% Define Volume
V = Volume();
V.add(Gun.RFT, 0, 0, 0.0);
V.add(TWS.RFT, 0, 0, 0.7);
V.add(Gun_Solenoid.RFT, 0, 0, 0.0 + Gun_Solenoid.S0);
V.add(TWS_Solenoid.RFT, 0, 0, 0.7 + TWS_Solenoid.S0);
V.add(TWS_Solenoid.RFT, 0, 0, 1.0 + TWS_Solenoid.S0);
V.add(TWS_Solenoid.RFT, 0, 0, 1.3 + TWS_Solenoid.S0);
V.set_s0(0.0); % entrance plane
V.set_s1(1.8); % exit plane

%% track in the low energy part
RF_Track.SC_engine = SpaceCharge_PIC_FreeSpace(32,32,64);
RF_Track.SC_engine.set_mirror(0.0); % set cathode position for mirror charges

%% perfor tracking in two parts, first the low energy part with
%% smaller step size
O = TrackingOptions();
O.open_boundaries = false; % it tracks until V.set_s1(), (default value: true)
O.odeint_algorithm = 'rk2'; % pick your favorite algorithm, 'rk2', 'rkf45', 'leapfrog', 'analytic'
O.dt_mm = 0.01; % mm/c, integration step size
O.tt_dt_mm = 10; % mm/c tabulate the phase space every tt_dt_mm
O.sc_dt_mm = 0.1; % mm/c, apply one step of space charge every 0.1 mm/c
O.t_max_mm = 150.0; % mm/c, tracks until t_max
O.verbosity = 0; % 0..2

% Track B0 -> B1 with dt_mm = 0.01 m/c
tic
B1 = V.track(B0, O);
toc

% extract transport table
T = V.get_transport_table(['%t %emitt_x %emitt_y %emitt_4d %sigma_X %sigma_Y %rmax %sigma_Px %sigma_Py %sigma_Z %mean_K']);
save -text results/T.txt T

%% track until the end, with larger step size
O.dt_mm = 0.1; % mm/c, increase the integration step
O.sc_dt_mm = 10.0; % mm/c, apply one step of space charge every 10 mm/c
O.t_max_mm = 0.0; % keep tracking until all particles have left the volume

% Track B1 -> B2 with increased step size
tic
B2 = V.track(B1, O);
toc

% extend transport table
T = [ T ; V.get_transport_table(['%t %emitt_x %emitt_y %emitt_4d %sigma_X %sigma_Y %rmax %sigma_Px %sigma_Py %sigma_Z %mean_K']) ];
save -text results/T.txt T

%% Save input / output distributions
A0 = B0.get_phase_space('%x %Px %y %Py %t0 %Pz');
save -text results/input_beam_RFGun.dat A0

% if open_boundaries = false, it gets the bunch at location s1
if O.open_boundaries
    A2 = B2.get_phase_space('%x %Px %y %Py %dt %Pz');
else 
    B2 = V.get_bunch_at_s1();
    A2 = B2.get_phase_space('%x %Px %y %Py %dt %Pz');
end
save -text results/output_beam_RFGun.dat A2


%% Do a lot of plots
S_axis = linspace(V.get_s0(), V.get_s1(), 1000); % m
Br = [];
Bz = [];
Er = [];
Ez = [];
for S = S_axis
    [E0_,B0_] = V.get_field(0, 0, S*1e3, 0);
    [E1_,B1_] = V.get_field(5, 0, S*1e3, 0);
    [E2_,B2_] = V.get_field(10, 0, S*1e3, 0);
    Br = [ Br ; B0_(1) B1_(1) B2_(1) ];
    Bz = [ Bz ; B0_(3) B1_(3) B2_(3) ];
    Er = [ Er ; E0_(1) E1_(1) E2_(1) ];
    Ez = [ Ez ; E0_(3) E1_(3) E2_(3) ];
end

figure(1)
clf
hold on
plot(S_axis, Er(:,1) / 1e6, 'b-');
plot(S_axis, Er(:,2) / 1e6, 'r-');
plot(S_axis, Er(:,3) / 1e6, 'g-');
legend('r=0 mm ', 'r=1 mm ', 'r=2 mm ');
xlabel('S [m]');
ylabel('E_r [MV/m]');
print -dpng results/plot_Er.png

clf
hold on
plot(S_axis, Ez(:,1) / 1e6, 'b-');
plot(S_axis, Ez(:,2) / 1e6, 'r-');
plot(S_axis, Ez(:,3) / 1e6, 'g-');
legend('r=0 mm ', 'r=1 mm ', 'r=2 mm ');
xlabel('S [m]');
ylabel('E_z [MV/m]');
print -dpng results/plot_Ez.png

clf
hold on
plot(S_axis, Br(:,1) / 1e-3, 'b-');
plot(S_axis, Br(:,2) / 2e-3, 'r-');
plot(S_axis, Br(:,3) / 3e-3, 'g-');
legend('r=0 mm ', 'r=1 mm ', 'r=2 mm ');
xlabel('S [m]');
ylabel('dB_r / dr [T/m]');
print -dpng results/plot_Br.png

clf
hold on
plot(S_axis, Bz(:,1), 'b-');
plot(S_axis, Bz(:,2), 'r-');
plot(S_axis, Bz(:,3), 'g-');
legend('r=0 mm ', 'r=1 mm ', 'r=2 mm ');
xlabel('S [m]');
ylabel('B_z [T]');
print -dpng results/plot_Bz.png

figure(2)
clf
hold on
scatter(B2.get_phase_space('%x'), B2.get_phase_space('%Px'))
xlabel('x [mm]');
ylabel('P_x [MeV/c]');
print -dpng results/plot_xPx.png

figure(3)
clf
hold on
scatter(B2.get_phase_space('%y'), B2.get_phase_space('%Py'))
xlabel('y [mm]');
ylabel('P_y [MeV/c]');
print -dpng results/plot_yPy.png

figure(4)
clf
hold on
scatter(B2.get_phase_space('%dt') / RF_Track.ns, B2.get_phase_space('%Pz'))
xlabel('dt [ns]');
ylabel('P_z [MeV/c]');
print -dpng results/plot_dtPz.png

figure(5)
clf
hold on
plot(T(:,1), T(:,2), T(:,1), T(:,3), T(:,1), T(:,4));
legend('emitt_x ', 'emitt_y ', 'emitt_{4d} ');
xlabel('t [mm/c]');
ylabel('emitt [mm.mrad]');
print -dpng results/plot_emitt.png

figure(6)
clf
hold on
plot(T(:,1), T(:,5), T(:,1), T(:,6), T(:,1), T(:,7));
legend('\sigma_x ', '\sigma_y ', 'Envelope ');
xlabel('t [mm/c]');
ylabel('mm');
print -dpng results/plot_sigma.png

figure(7)
clf
hold on
Pz_ref = B2.get_phase_space("%Pz")(1);
plot(T(:,1), 1e3*T(:,8)/Pz_ref, T(:,1), 1e3*T(:,9)/Pz_ref);
legend('\sigma_{xp} ', '\sigma_{yp} ');
xlabel('t [mm/c]');
ylabel('mrad');
print -dpng results/plot_sigma_prime.png

figure(8)
clf
ax = plotyy(T(:,1), T(:,10), T(:,1), T(:,11));
xlabel('t [mm/c]');
ylabel(ax(1), '\sigma_z [mm] ');
ylabel(ax(2), 'E_{kinetic} [MeV] ');
print -dpng results/plot_E_sigma_Z.png

clf
hold on
plot(T(:,1), T(:,4));
xlabel('t [mm/c]');
ylabel('emitt_{4d} [mm.mrad]');
print -dpng results/plot_emitt_4d.png

