%% Load the beam
function B = load_beam(filename)
RF_Track;

T = load(filename);
T(2:end,[ 3 6 7 ]) += repmat(T(1,[ 3 6 7 ]), size(T,1)-1,1); % ASTRA convention

% create a 10-column matrix for Bunch6dT
B = zeros(size(T,1), 10);
B(:,1) = T(:,1) * 1e3; % mm
B(:,2) = T(:,4) / 1e6; % MeV/c
B(:,3) = T(:,2) * 1e3; % mm
B(:,4) = T(:,5) / 1e6; % MeV/c
B(:,5) = T(:,3) * 1e3; % mm
B(:,6) = T(:,6) / 1e6; % MeV/c
B(:,7) = RF_Track.electronmass; % MeV/c^2
B(:,8) = -1; % e^+
B(:,9) = abs(T(:,8)) * RF_Track.nC; % e^+
B(:,10) = T(:,7) * RF_Track.ns; % mm/c
