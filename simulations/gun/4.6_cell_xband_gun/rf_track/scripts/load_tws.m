function TWS = load_tws()
RF_Track;
T = load('for_andrea/xband_108cell_120deg.txt').allmap;
T(isnan(T)) = 0;
TWS.S = T(:,1);
TWS.L = range(T(:,1));
TWS.Ez = complex(T(:,2), T(:,3));
TWS.freq = 11.9917e9; % Hz
% smooth
new_N = 800;
new_S = linspace(TWS.S(1), TWS.S(end), new_N);
TWS.Ez = interp1(TWS.S(:), TWS.Ez(:), new_S, 'spline');
TWS.dS = range(new_S) / (new_N-1); % m
TWS.RFT = RF_FieldMap_1d(TWS.Ez, TWS.dS, TWS.L, TWS.freq, -1);
TWS.RFT.set_t0(0.0);
TWS.RFT.set_phid(249.97);

