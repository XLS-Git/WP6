function Gun_Solenoid = load_gun_solenoid()
RF_Track;
T = load('for_andrea/XBAND_GUN_SOL.DAT');
Gun_Solenoid.S = T(:,1); % m
Gun_Solenoid.S0 = min(T(:,1)); % m
Gun_Solenoid.S1 = max(T(:,1)); % m
Gun_Solenoid.Bz = T(:,2) * 0.482; % T
% smooth
new_N = 800;
new_S = linspace(Gun_Solenoid.S0, Gun_Solenoid.S1, new_N);
Gun_Solenoid.Bz = interp1(Gun_Solenoid.S(:), Gun_Solenoid.Bz(:), new_S, 'spline');
Gun_Solenoid.dS = range(new_S) / (new_N-1); % m
Gun_Solenoid.RFT = Static_Magnetic_FieldMap_1d_LINT(Gun_Solenoid.Bz, Gun_Solenoid.dS);

