function Gun = load_gun()
RF_Track;
T = load('for_andrea/X-BAND_GUN_4.6CELL.DAT');
Gun.S = T(:,1); % m
Gun.L = range(Gun.S); % m
Gun.S0 = min(Gun.S); % m
Gun.S1 = max(Gun.S); % m
Gun.freq = 11.9917e9; % Hz
Gun.Ez = T(:,2) * 250e6; % V/m
% smooth
new_N = 200;
new_S = linspace(Gun.S0, Gun.S1, new_N);
Gun.Ez = interp1(Gun.S(:), Gun.Ez(:), new_S, 'spline');
Gun.dS = range(new_S) / (new_N-1); % m
Gun.RFT = RF_FieldMap_1d(Gun.Ez, Gun.dS, Gun.L, Gun.freq, -1);
Gun.RFT.set_t0(0.0);
Gun.RFT.set_phid(287);

