function TWS_Solenoid = load_tws_solenoid()
RF_Track;
T = load('for_andrea/XBAND_CAV_SOL.DAT');
TWS_Solenoid.S = T(:,1); % m
TWS_Solenoid.S0 = min(T(:,1)); % m
TWS_Solenoid.S1 = max(T(:,1)); % m
TWS_Solenoid.Bz = T(:,2) * 0.3; % T
TWS_Solenoid.Bz(1) = 0.0;
% smooth
new_N = 800;
new_S = linspace(TWS_Solenoid.S0, TWS_Solenoid.S1, new_N);
TWS_Solenoid.Bz = interp1(TWS_Solenoid.S(:), TWS_Solenoid.Bz(:), new_S, 'spline');
TWS_Solenoid.dS = range(new_S) / (new_N-1); % m
TWS_Solenoid.RFT = Static_Magnetic_FieldMap_1d_LINT(TWS_Solenoid.Bz, TWS_Solenoid.dS);
