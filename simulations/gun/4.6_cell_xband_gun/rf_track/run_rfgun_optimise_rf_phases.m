addpath([ pwd '/scripts' ]);
system('mkdir -p results');

%% Load RF-Track
RF_Track;

%% Load the beam and define B0
global B0
B = load_beam('for_andrea/part_10k_2ps_0.25mm.dat');
B0 = Bunch6dT(B);

%% Load the field map of the elements
global Gun TWS Gun_Solenoid TWS_Solenoid
Gun          = load_gun();
TWS          = load_tws();
Gun_Solenoid = load_gun_solenoid();
TWS_Solenoid = load_tws_solenoid();

%% track particles as a function of RF phases
function B2 = track(phid1, phid2)
    global Gun TWS Gun_Solenoid TWS_Solenoid B0
    RF_Track;
    Gun.RFT.set_phid(phid1);
    TWS.RFT.set_phid(phid2);

    %% Define Volume
    V = Volume();
    V.add(Gun.RFT, 0, 0, 0.0);
    V.add(TWS.RFT, 0, 0, 0.7);
    V.add(Gun_Solenoid.RFT, 0, 0, 0.0 + Gun_Solenoid.S0);
    V.add(TWS_Solenoid.RFT, 0, 0, 0.7 + TWS_Solenoid.S0);
    V.add(TWS_Solenoid.RFT, 0, 0, 1.0 + TWS_Solenoid.S0);
    V.add(TWS_Solenoid.RFT, 0, 0, 1.3 + TWS_Solenoid.S0);
    V.set_s0(0.0); % entrance plane
    V.set_s1(1.8); % exit plane
    
    %% set the space charge routines
    RF_Track.SC_engine = SpaceCharge_PIC_FreeSpace(32,32,64);
    RF_Track.SC_engine.set_mirror(0.0);
    
    %% set the tracking options for the first part of tracking
    O = TrackingOptions();
    O.open_boundaries = false; % it tracks until V.set_s1()
    O.odeint_algorithm = 'leapfrog'; % pick your favorite algorithm, 'rk2', 'rkf45', 'leapfrog', 'analytic'
    O.dt_mm = 0.01; % mm/c, integration step size
    O.t_max_mm = 150.0; % mm/c, tracks until t_max
    O.sc_dt_mm = 0.1; % mm/c, integration step size
    
    %% track in the low energy part
    B1 = V.track(B0, O);
    
    %% set the tracking options for the second part of tracking
    % Track B1 -> B2 with increased step size
    O.dt_mm = 0.1; % mm/c, increase the integration step
    O.sc_dt_mm = 10.0; % mm/c, integration step size
    O.t_max_mm = 0.0; % keeps tracking until all particles have left the volume
    V.track(B1, O);

    %% track in the second part
    B2 = V.get_bunch_at_s1();
endfunction

%% finds the phase for minimum emittance
function M = merit_function(X)
    RF_Track;
    phid1 = constrain(X(1), 180, 260);
    phid2 = constrain(X(2), 0, 90);
    %% perform tracking
    B = track(phid1, phid2);
    %% get bunch info
    I = B.get_info();
    %% display the relevant quantities
    disp([ phid1 phid2 I.emitt_4d I.sigma_t I.mean_E ]);
    %% merit function: asks for low emittance,
    % short rms bunch length, and high energy
    M = I.emitt_4d + I.sigma_t - 0.001 * I.mean_E; 
endfunction

%% perform rf phases optimisation
X = fminsearch(@merit_function, [ 0 0 ], optimset("TolX", 0.1));
Optimum = [ Gun.RFT.get_phid() TWS.RFT.get_phid() ]
save -text results/optimum.dat Optimum