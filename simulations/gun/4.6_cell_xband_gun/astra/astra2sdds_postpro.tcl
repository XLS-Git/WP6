#!/bin/sh  
# \
exec tclsh "$0" "$@"

set fname [lindex $argv 0]

if {[llength $argv] < 1 } {
    puts "no input file is specified \nusage astra2sdds_postpro.tcl <astra_run> outputfile"
} elseif {[llength $argv] > 1 } {
    set fname2  [lindex $argv 1]
 } else {
     set fname2 $fname
 }
set fname2 $fname2.sdds

puts "astra($fname) -> sddsout($fname2)"


set  lfile [glob -type f $fname.Log.*]

# set yfile [glob -type f $fname.Yemit.*]
# set zfile [glob -type f $fname.Zemit.*]
# set rfile [glob -type f $fname.ref.*]
# set cfile [glob -type f $fname.Cathode.*]
puts "$lfile"


set fl [open $lfile r]
set i 0
set j 1e10
set outnames {}
set positions {}
while {[gets $fl linel]>=0 } {
#  End of Input deck
    set end1  [lindex $linel 0]
    set end2  [lindex $linel 1]
    set end3  [lindex $linel 2]
    
    if {$end1=="ZPHASE" } {
        set zphase [string map {, ""} $end3]
    }
    
    if { $end1=="End" && $end2=="of" && $end3=="Input" } {
        set j  [expr $i+1]
        set je [expr $i+$zphase+2]

    }
    if { $i > $j && $i < $je } {
        lappend outnames $end1
    }
    
    incr i
    
}

puts "$j $je"

set kk 0
set inputlst {}
foreach on $outnames {    
    puts $on
    exec astra2commonsdds $on utmp.sdds
    exec sddsconvert utmp.sdds tmp.$kk.sdds -binary
    

    lappend inputlst tmp.$kk.sdds
    incr kk
    
}

    eval  exec sddscombine  $inputlst  tmpt.00.sdds -overwrite
      
      
exec rm -f utmp.sdds
eval exec rm $inputlst
exec mv tmpt.00.sdds $fname2

exit


 

 
