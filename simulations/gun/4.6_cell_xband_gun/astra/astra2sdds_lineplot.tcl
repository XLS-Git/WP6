#!/bin/sh  
# \
exec tclsh "$0" "$@"

set fname [lindex $argv 0]

if {[llength $argv] < 1 } {
    puts "no input file is specified \nusage astra2sdds_lineplot.tcl <astra_run> outputfile"
} elseif {[llength $argv] > 1 } {
    set fname2  [lindex $argv 1]
 } else {
     set fname2 $fname
 }
set fname2 $fname2.sdds

puts "astra($fname) -> sddsout($fname2)"


set xfile [glob -type f $fname.Xemit.*]
set yfile [glob -type f $fname.Yemit.*]
set zfile [glob -type f $fname.Zemit.*]
set rfile [glob -type f $fname.ref.*]
set cfile [glob -type f $fname.Cathode.*]
# puts "$xfile $yfile $zfile"



proc readfiles { xfile yfile zfile rfile cfile outfile} {
#     global xfile yfile zfile
    set m0c2 510998.910e-6 ;# MeV
    set cc    299792458. ;# m/s
    set mev 1.e-6
    
    set fx [open $xfile r]
    set fy [open $yfile r]
    set fz [open $zfile r]
    set fr [open $rfile r]
    set fc [open $cfile r]
    
# time Q avgx avgy avgz avgG stdx stdy stdz stdG nemixrms nemiyrms numpar avgp
   set zz     {}   ;# m
   set timee  {}   ;# ns
   set avgx   {}   ;# mm
   set avgy   {}   ;# mm
   set avgz   {}   ;# m
   set avgg   {}   ;# unitless
   set avge   {}   ;# MeV
   
   set stdx   {}   ;# mm
   set stdy   {}   ;# mm
   set stdz   {}   ;# mm
   
   set stdg   {}   ;# unitless
   set stde   {}   ;# keV
   
   set avgp   {}   ;# MeV/c
   set pref   {}   ;# MeV/c   
   
   set nemixrms {} ;# mm mrad
   set nemiyrms {} ;# mm mrad
   set nemizrms {} ;# keV mm
   set nemizrms2 {} ;# keV ns
   
   set qemitt   {} ;

   
   set lcharge 0
   set temt {}
   set zemt {}
   set qemt {}
   
   while {[gets $fc linec]>=0 } {
        set z [lindex $linec 0]    ;# m
        set t [lindex $linec 1]    ;# ns
        set q [lindex $linec 4]    ;# nC
        lappend temt $t
        lappend zemt $z
        lappend qemt $q
        
        incr lcharge
   }
   
    set nline 0
   	while {[gets $fx linex]>=0 && [gets $fy liney]>=0 && [gets $fz linez]>=0  } {
#         puts $linex
        
        set z [lindex $linex 0]    ;# m
        set t [lindex $linex 1]    ;# ns
        
        set avx [lindex $linex 2]  ;# mm
        set stx [lindex $linex 3]  ;# mm
        set emx [lindex $linex 5]  ;# mm mrad
        
        set avy [lindex $liney 2]  ;# mm
        set sty [lindex $liney 3]  ;# mm
        set emy [lindex $liney 5]  ;# mm mrad
        
        set avE [lindex $linez 2]  ;# Mev
        set stz [lindex $linez 3]  ;# mm
        set stE [lindex $linez 4]  ;# keV
        set emz [lindex $linez 5]  ;# keV mm
        
        set avG [expr $avE/$m0c2 +1.0]
        set stG [expr $stE*1e-3/$m0c2 ]        
        set avB [expr sqrt(1-1/$avG/$avG)]
        set avp [expr sqrt($avE*$avE+2.0*$avE*$m0c2)] ;# MeV/c
        
        set emz2 [expr $emz*1e-3/($avB*$cc)*1e9]  ;# keV ns
        
        
        
        lappend zz $z
        lappend timee $t
        lappend avgx   $avx
        lappend avgy   $avy
        lappend avgz   $z
        lappend avgg   $avG
        lappend avge   $avE
        
        lappend stdx   $stx
        lappend stdy   $sty
        lappend stdz   $stz ;# mm
        
        lappend stdg   $stG   ;# unitless
        lappend stde   $stE   ;# keV
        
        lappend avgp   $avp   ;# MeV/c
        
        
        lappend nemixrms $emx ;# mm mrad
        lappend nemiyrms $emy ;# mm mrad
        lappend nemizrms $emz ;# keV mm
        lappend nemizrms2 $emz2 ;# keV ns   
        incr nline
    }

    close $fx
    close $fy
    close $fz
    
    set fo  [open tmp.dat w]
   
    puts $fo "SDDS1
&parameter name=Description, format_string=%s, type=string, &end
&column name=z, units=m, type=double,  &end
&column name=time, units=ns, type=double,  &end
&column name=avgx, units=mm, type=double,  &end
&column name=avgy, units=mm, type=double,  &end
&column name=avgz, units=m, type=double,  &end
&column name=avgG, units=unitless, type=double,  &end
&column name=avgE, units=MeV, type=double,  &end
&column name=stdx, units=mm, type=double,  &end
&column name=stdy, units=mm, type=double,  &end
&column name=stdz, units=mm, type=double,  &end
&column name=stdG, units=unitless, type=double,  &end
&column name=stdE, units=keV, type=double,  &end
&column name=avgp, units=MeV/c, type=double,  &end
&column name=nemixrms, units=mm.mrad, type=double,  &end
&column name=nemiyrms, units=mm.mrad, type=double,  &end
&column name=nemizrms, units=keV.ns, type=double,  &end
&column name=nemizrms2, units=keV.mm, type=double,  &end
&data mode=ascii, &end
! page number 1"
puts $fo "astra2sdds"
puts $fo "    $nline"
# 
foreach  z $zz t $timee x0 $avgx  y0 $avgy  z0 $avgz   g0 $avgg  e0 $avge  xx $stdx  yy $stdy zz $stdz  gg $stdg   ee $stde  p0 $avgp emx $nemixrms emy $nemiyrms emz  $nemizrms emz2 $nemizrms2 {
		puts $fo [format "%16.9e %16.9e %16.9e %16.9e %16.9e %16.9e %16.9e %16.9e %16.9e %16.9e %16.9e %16.9e %16.9e %16.9e %16.9e %16.9e %16.9e "  $z $t $x0 $y0 $z0 $g0 $e0 $xx $yy $zz $gg $ee $p0 $emx $emy $emz2 $emz ]   
	}

	close $fo
	
	exec sddsconvert tmp.dat $outfile -binary
	exec rm tmp.dat
    
    
}


readfiles $xfile $yfile $zfile $rfile $cfile $fname2

exit




proc readfiles {indist outdist} {
   set fin [open $indist r]

   set m0c2 510998.910 ;# eV
   set c    299792458. ;# m/s
   set mev 1.e-6
   set x_l    {}
   set y_l    {}
   set z_l    {}
   set xp_l   {}
   set yp_l   {}
   set t_l   {}
   set pz_l   {}
  
	

	set i 0
	set npart 0
	set qtot 0.
	while {[gets $fin line] >= 0} {

		if { $i < 1 } {
			set x0  [lindex $line 0]
			set y0  [lindex $line 1]
			set z0  [lindex $line 2]
			set xp0 [expr [lindex $line 3]/$m0c2]
			set yp0 [expr [lindex $line 4]/$m0c2]
			set pz0 [expr [lindex $line 5]/$m0c2]
			set w0  [expr (sqrt(($pz0*$m0c2)**2+$m0c2**2)-$m0c2)]
			set g  [expr $w0/$m0c2+1]
			set v  [expr sqrt(1.-1./$g/$g)*$c]
			set w0  [expr $w0*$mev]
# 			set t  [expr $z/$v +$t0]
			set t0  [expr [lindex $line 6]*1.e-9]
			set qtot [expr $qtot+[lindex $line 7]]

			lappend x_l   [expr $x0]
			lappend y_l   [expr $y0]
			lappend z_l   [expr $z0]
			lappend xp_l  [expr $xp0/$g]
			lappend yp_l  [expr $yp0/$g]
			lappend w_l   [expr $w0]
			lappend t_l   [expr $t0]
			lappend pz_l  [expr $pz0]
			incr npart
		} else {
			if {[lindex $line 9] > -2} {
				set x  [expr [lindex $line 0]+$x0]
				set y  [expr [lindex $line 1]+$y0]
				set z  [lindex $line 2]
				set xp [expr [lindex $line 3]/$m0c2+$xp0]
				set yp [expr [lindex $line 4]/$m0c2+$yp0]
				set pz [expr [lindex $line 5]/$m0c2+$pz0]
				set w  [expr (sqrt(($pz*$m0c2)**2+$m0c2**2)-$m0c2)]
				set g  [expr $w/$m0c2+1]
				set v  [expr sqrt(1.-1./$g/$g)*$c]
				set w  [expr $w*$mev]
				set t  [expr $z/$v +$t0]
				set qtot [expr $qtot+[lindex $line 7]]
				
				lappend x_l  [expr $x]
				lappend y_l  [expr $y]
				lappend z_l  [expr $z]
				lappend xp_l [expr $xp/$g]
				lappend yp_l [expr $yp/$g]
				lappend w_l   [expr $w]
				lappend t_l  [expr $t]
				lappend pz_l  [expr $pz]
				incr npart
		}
	}
		incr i
	}
	close $fin
	
proc mymean { lst } {
	set u0 0.
	set nl 0
   foreach u $lst {
		set  u0 [expr $u0+$u]
		incr nl
	}
	return [expr $u0/$nl]	 
}

proc myvar { lst1 lst2 } {
	set u0 0.
	set nl 0
   foreach u1 $lst1 u2 $lst2  {
		set  u0 [expr $u0+$u1*$u2]
		incr nl
	}
	return [expr $u0/$nl]	 
}


   set fo  [open tmp.dat w]
   
   puts $fo "SDDS1
&parameter name=Description, format_string=%s, type=string, &end
&parameter name=pCentral, symbol=\"p\$bcen\$n\", units=\"m\$be\$nc\", description=\"Reference beta*gamma\", type=double, &end
&parameter name=Charge, units=C, description=\"Beam charge\", type=double, &end
&parameter name=Particles, description=\"Number of particles\", type=long, &end
&parameter name=PassCentralTime, units=s, type=double, &end
&column name=x, units=m, type=double,  &end
&column name=xp, type=double,  &end
&column name=y, units=m, type=double,  &end
&column name=yp, type=double,  &end
&column name=t, units=s, type=double,  &end
&column name=p, units=\"m\$be\$nc\", type=double,  &end
&column name=dt, units=s, type=double,  &end
&data mode=ascii, &end
! page number 1"
puts $fo "astra2elegant"
puts $fo [format "%16.9e"  [expr $pz0]]
puts $fo [format "%16.9e"  [expr -1.*$qtot*1.e-9]]
puts $fo " $npart"
puts $fo [format "%16.9e"  [expr 0]]    
puts $fo "          $npart"
# 
   foreach x $x_l xp $xp_l y $y_l yp $yp_l t $t_l pz $pz_l {
		puts $fo [format "%16.9e %16.9e %16.9e %16.9e %16.9e %16.9e %16.9e"  $x $xp $y $yp $t $pz [expr $t-$t0]]   
		
	}

	close $fo
	
	exec sddsconvert tmp.dat $outdist -binary
	exec rm tmp.dat
}


compute_single_bunch_params $fname $fname2
 

 
