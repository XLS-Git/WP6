addpath([ pwd '/scripts' ]);

%% Load RF-Track
RF_Track;

%% Setup space-charge
SC = SpaceCharge_PIC_FreeSpace(32,32,32);
SC.set_smooth(1.0); % set smooth factor
SC.set_mirror(0.0); % set position of cathode
RF_Track.SC_engine = SC;

%% Load the beam and define B0t
global B0t P0t
B = load_beam('../ASTRA/XLS_distr_0.256_10k.ini');
B0t = Bunch6dT(B); % beam
P0t = Bunch6dT(B(1,:)); % reference particle

%% setup parameters
global setup
setup.gun_phid = 100; % deg
setup.gun_grad = 160; % MV/m
setup.gun_sol_bmax = 0.3990 * str2num(argv{1}) / 100; % T
setup.tws_phid = [ 3.9 3.2621 -1.7552 -3.4594 ]; % deg
setup.tws_grad = 19.52; % MV/m
setup.tws_sol_bmax = 0.059;

function [B1,T] = track()
    RF_Track;
    global B0t P0t setup
    %% setup volume
    V = setup_volume(setup);
    V.set_s1(1.5);
    %% tracking of the refernece particle to set the arrival time at the RF elements
    O = TrackingOptions();
    O.odeint_algorithm = 'analytic'; % pick your favorite algorithm, 'rk2', 'rkf45', 'leapfrog', 'analytic'
    O.odeint_epsabs = 1e-10; % required accuracy
    O.dt_mm = 0.01 * RF_Track.ps; % mm/c, integration step size
    P1t = V.track(P0t, O);
    %% tracking
    % part 1
    tt_fmt_str = '%mean_S %mean_K %sigma_X %sigma_Y %sigma_Z %emitt_x %emitt_y %emitt_4d %sigma_E';
    O.dt_mm = 1 * RF_Track.ps; % mm/c, integration step size
    O.t_max_mm = 40 * RF_Track.ps; % mm/c, tracks until t_max
    O.sc_dt_mm = 0.01 * RF_Track.ps; % mm/c, compute space-charge step every sc_dt_mm 
    O.tt_dt_mm = 50; % mm/c, tabulate beam properties every tt_dt_mm 
    B1t = V.track(B0t, O);
    T = V.get_transport_table(tt_fmt_str);
    %%part 2
    O.t_max_mm = Inf; % mm/c, tracks until t_max
    O.sc_dt_mm = 1; % mm/c, compute space-charge step every sc_dt_mm 
    B1t = V.track(B1t, O);
    B1 = V.get_bunch_at_s1();
    T = [ T ; V.get_transport_table(tt_fmt_str) ];
end

[B1,T] = track();
M1 = B1.get_phase_space();
I1 = B1.get_info();
M = [ I1.emitt_4d,
      I1.sigma_x,
      I1.sigma_t,
      I1.mean_K,
      skewness(M1(:,5)),
      skewness(M1(:,6)),
      kurtosis(M1(:,5)),
      kurtosis(M1(:,6))
    ];

system('mkdir -p scan2');
save('-text', '-zip', sprintf('scan2/M_scan2_%s.dat.gz', argv{1}), 'M', 'M1', 'T');
