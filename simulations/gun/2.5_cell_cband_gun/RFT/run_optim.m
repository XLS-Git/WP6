addpath([ pwd '/scripts' ]);

system('mkdir -p results');

%% Load RF-Track
RF_Track;
RF_Track.number_of_threads = 10;

%% Setup space-charge
SC = SpaceCharge_PIC_FreeSpace(32,32,32);
SC.set_smooth(0.0); % set smooth factor
SC.set_mirror(0.0); % set position of cathode
RF_Track.SC_engine = SC;

%% input file
global setup
setup.distribution_10k = '../ASTRA/XLS_distr_0.220_10k.ini';
setup.distribution_50k = '../ASTRA/XLS_distr_0.220_50k.ini';

%% Load the beam and define B0t
global B0t P0t
B = load_beam(setup.distribution_10k);
B0t = Bunch6dT(B); % beam
P0t = Bunch6dT(B(1,:)); % reference particle

%% setup parameters
function B1 = track()
    RF_Track;
    global B0t P0t setup
    %% setup volume
    V = setup_volume(setup);
    %% tracking of the refernece particle to set the arrival time at the RF elements
    O = TrackingOptions();
    O.odeint_algorithm = 'analytic'; % pick your favorite algorithm, 'rk2', 'rkf45', 'leapfrog', 'analytic'
    O.odeint_epsabs = 1e-10; % required accuracy
    O.dt_mm = 0.01 * RF_Track.ps; % mm/c, integration step size
    P1t = V.track(P0t, O);
    %% tracking
    % part 1
    O.dt_mm = 1 * RF_Track.ps; % mm/c, integration step size
    O.t_max_mm = 15 * RF_Track.ps; % mm/c, tracks until t_max
    O.sc_dt_mm = 0.01 * RF_Track.ps; % mm/c, compute space-charge step every sc_dt_mm 
    B1t = V.track(B0t, O);
    % part 2
    O.t_max_mm = Inf; % mm/c, tracks until t_max
    O.sc_dt_mm = 1; % mm/c, compute space-charge step every sc_dt_mm 
    B1t = V.track(B1t, O);
    B1 = V.get_bunch_at_s1();
end

function M = merit(X)
    RF_Track;
    X /= 10;
    global setup convergence
    setup.gun_phid = constrain(X(1), 98, 140); % deg
    setup.gun_grad = constrain(X(2), 150, 190); % MV/m
    setup.gun_sol_bmax = constrain(X(3), 0.30, 0.50); % T
    setup.tws_phid = constrain(X(4), 0, 20); % deg
    setup.tws_grad = 19.52; % MV/m
    setup.tws_pos = constrain(X(5), 1.4, 1.6); % m
    setup.tws_sol_bmax = constrain(X(6), 0.040, 0.060); % T
    save -text results/setup.dat setup
    B1 = track();
    M1 = B1.get_phase_space();
    emitt_4d = B1.get_info().emitt_4d;
    M = 200*max(emitt_4d / 0.2 - 1, 0.0)**2 + ...
        10*max(std(M1(:,5)) / 0.340 - 1, 0.0)**2 + ...
        10*min(mean(M1(:,6)) / 90 - 1, 0.0)**2 + ...
        skewness(M1(:,5))**2 + ...
        skewness(M1(:,6))**2
    convergence = [ convergence ; M ];
    save -text results/convergence.dat convergence
end

global convergence
convergence = [];
X = fminsearch(@merit, zeros(1,6));

merit(X)
save -text results/setup.dat setup
