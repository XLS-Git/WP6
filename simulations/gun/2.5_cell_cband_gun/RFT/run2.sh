#!/bin/bash
source /cvmfs/clicbp.cern.ch/x86_64-centos7-gcc10-opt/setup.sh

cd ~/2.5_cell_cband_gun/RFT

octave-cli run_optim_condor.m 5ps_0.18mm $1
