function Gun = load_gun(phid, grad)
    RF_Track;
    T = load('../ASTRA/C_Band_2_5_cells_gun.dat');
    S = T(:,1); % m
    L = range(S); % m
    freq = 5.712e9; % Hz
    T(:,2) /= max(abs(T(:,2))); % normalize to 1
    Ez = T(:,2) * grad * 1e6; % V/m
    Gun = RF_FieldMap_1d(Ez, S(2) - S(1), L, freq, +1);
    Gun.set_phid(phid);
    Gun.set_smooth(3);
endfunction

