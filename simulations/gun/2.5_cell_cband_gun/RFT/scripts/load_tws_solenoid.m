function [Solenoid,S0] = load_tws_solenoid(Bmax)
    RF_Track;
    T = load('../ASTRA/C_Band_TW2mlong_SOL.poi');
    S = T(:,1); % m
    S0 = min(T(:,1)); % m
    Bz = T(:,2) * Bmax; % T
    Solenoid = Static_Magnetic_FieldMap_1d(Bz, S(2) - S(1));
    Solenoid.set_smooth(5);
endfunction
