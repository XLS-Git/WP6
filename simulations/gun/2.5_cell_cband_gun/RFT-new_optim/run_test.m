addpath([ pwd '/scripts' ]);

%% Load RF-Track
RF_Track;

%% Setup space-charge
SC = SpaceCharge_PIC_FreeSpace(32,32,32);
SC.set_smooth(1.0); % set smooth factor
SC.set_mirror(0.0); % set position of cathode
RF_Track.SC_engine = SC;

%% Load the beam and define B0
global B0t P0t
B = load_beam('../ASTRA/XLS_distr_0.128_10k.ini');
B0t = Bunch6dT(B); % beam
P0t = Bunch6dT(B(1,:)); % reference particle

function [M,setup] = track(X)
    RF_Track;
    global B0t P0t
    setup.gun_phid = 180 + X(1); % deg
    setup.gun_grad = 160; % MV/m
    setup.gun_sol_bmax = 0.3990; % T
    setup.tws_phid = X(2:5); %%% [ -1.11 -5.3 -0.3 -0.3 ]; % deg
    setup.tws_grad = 19.52; % MV/m
    setup.tws_sol_bmax = 0.059;
    V = setup_volume(setup);
    %% tracking of the refernece particle
    O = TrackingOptions();
    O.odeint_algorithm = 'rk2'; % pick your favorite algorithm, 'rk2', 'rkf45', 'leapfrog', 'analytic'
    O.odeint_epsabs = 1e-10; % required accuracy
    O.dt_mm = 0.01 * RF_Track.ps; % mm/c, integration step size
    P1t = V.track(P0t, O);
    M = P1t.get_phase_space('%K')
end

X = fminsearch(@(X) abs(120-track(X)), zeros(5,1));
[~,setup] = track(X)