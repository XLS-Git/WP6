#!/bin/bash
source /cvmfs/clicbp.cern.ch/x86_64-centos7-gcc8-opt/setup.sh

cd ~/2.5_cell_cband_gun/RFT-new_optim

octave-cli -q run_optim_condor.m 220 $1
