addpath([ pwd '/scripts' ]);

%% Load RF-Track
RF_Track;
RF_Track.number_of_threads = 8;

%% Setup space-charge
SC = SpaceCharge_PIC_FreeSpace(32,32,32);
SC.set_smooth(1.0); % set smooth factor
SC.set_mirror(0.0); % set position of cathode
RF_Track.SC_engine = SC;

%% setup parameters
global setup
load results/setup.dat

%% Load the beam and define B0t
global B0t P0t
B = load_beam(setup.distribution_50k);
B0t = Bunch6dT(B); % beam
P0t = Bunch6dT(B(1,:)); % reference particle

function [B1,T] = track()
    RF_Track;
    global B0t P0t setup
    %% setup volume
    V = setup_volume(setup);
    %% tracking of the refernece particle to set the arrival time at the RF elements
    O = TrackingOptions();
    O.odeint_algorithm = 'rkf45'; % pick your favorite algorithm, 'rk2', 'rkf45', 'leapfrog', 'analytic'
    O.odeint_epsabs = 1e-10; % required accuracy
    O.dt_mm = 0.005 * RF_Track.ps; % mm/c, integration step size
    P1t = V.track(P0t, O);
    %% tracking
    tic
    % part 1
    tt_fmt_str = '%mean_S %mean_K %sigma_X %sigma_Y %sigma_Z %emitt_x %emitt_y %emitt_4d %sigma_E';
    O.t_max_mm = 100 * RF_Track.ps; % mm/c, tracks until t_max
    O.tt_dt_mm = 1; % mm/c, tabulate beam properties every tt_dt_mm 
    O.sc_dt_mm = 0.01 * RF_Track.ps; % mm/c, compute space-charge step every sc_dt_mm 
    O.dt_mm = 1 * RF_Track.ps; % mm/c, integration step size
    O.verbosity = 1;
    B1t = V.track(B0t, O);
    % extract transport table
    T = V.get_transport_table(tt_fmt_str);
    save -text results/track.dat T
    % part 2
    O.t_max_mm = Inf; % mm/c, tracks until t_max
    O.tt_dt_mm = 10; % mm/c, tabulate beam properties every tt_dt_mm 
    O.sc_dt_mm = 1; % mm/c, compute space-charge step every sc_dt_mm 
    B1t = V.track(B1t, O);
    % extend transport table
    T = [ T ; V.get_transport_table(tt_fmt_str) ];
    save -text results/track.dat T
    toc
    B1 = V.get_bunch_at_s1();
end

[B1,T] = track();

%% Save input / output distributions
A0 = B0t.get_phase_space('%x %Px %y %Py %t0 %Pz');
A1 = B1.get_phase_space();
save -text results/input_beam_RFGun.dat A0
save -text results/output_beam_RFGun.dat A1

figure(2)
clf
scatter(A1(:,1), A1(:,2))
xlabel('x [mm]');
ylabel('x'' [mrad]');
print -dpng results/plot_xxp.png

figure(3)
clf
scatter(A1(:,3), A1(:,4))
xlabel('y [mm]');
ylabel('y'' [mrad]');
print -dpng results/plot_yyp.png

figure(4)
clf
A1(:,5) -= mean(A1(:,5));
scatter(A1(:,5) / RF_Track.ns, A1(:,6))
xlabel('dt [ns]');
ylabel('P [MeV/c]');
print -dpng results/plot_dtP.png
	
figure(5)
plot(T(:,1)/1e3, T(:,6), T(:,1)/1e3, T(:,7), T(:,1)/1e3, T(:,8));
legend('emitt_x ', 'emitt_y ', 'emitt_{4d} ');
xlabel('S [m]');
ylabel('emitt [mm.mrad]');
print -dpng results/plot_emitt.png

figure(6)
plot(T(:,1)/1e3, T(:,3), T(:,1)/1e3, T(:,4));
legend('\sigma_x ', '\sigma_y ');
xlabel('S [m]');
ylabel('\sigma [mm]');
print -dpng results/plot_sigma.png

figure(8)
ax = plotyy(T(:,1)/1e3, T(:,5)*1e3, T(:,1)/1e3, T(:,2));
xlabel('S [m]');
ylabel(ax(1), '\sigma_z [{\mu}m] ');
ylabel(ax(2), 'E_{kinetic} [MeV] ');
print -dpng results/plot_E_sigma_Z.png

clf
plot(T(:,1)/1e3, T(:,8));
xlabel('S [m]');
ylabel('emitt_{4d} [mm.mrad]');
print -dpng results/plot_emitt_4d.png

