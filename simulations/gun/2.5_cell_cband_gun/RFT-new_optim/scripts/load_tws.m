function TWS = load_tws(phid,grad)
    RF_Track;
    Ncell = 114; % number of cells
    freq = 5.712e9; % Hz
    lambda = RF_Track.clight / freq; % m
    %%
    S = fileread('../ASTRA/TWS_Cband.dat');
    T = strsplit(S, '\n');
    L = str2num(T{1}); % first line of the superfish file
    A = str2num([ T{2:end} ]); % all the rest of the file
    D = transpose(reshape(A, 2, length(A) / 2)); % into matrix form
    I0 = find(D(:,1) <= L(1))(end); % from (to be included)
    I1 = find(D(:,1) >= L(2))(1); % to (to be included)
    IN =  D(1:I0,2);
    OUT = D(I1:end,2); % full structure
    Lco_i = L(1); % m, input coupler length (standing wave)
    Lco_f = D(end,1) - L(2); % m, output coupler length (standing wave)
    Lcell = lambda * L(3) / L(4); % m
    ph = 2*pi * L(3) / L(4); % rad, phase advance per cell
    dS = D(2,1) - D(1,1); % m, mesh step
    Ltw = Ncell * Lcell; % m, length of the traveling wave
    Lstr = Lco_i + Ltw + Lco_f; % m, total structure length
    Z = 0:dS:Lstr; % prepare mesh
    M = Z>=Lco_i & Z<=(Ltw+Lco_f); % pick the mesh points in the traveling wave part
    Z0 = Lco_i + rem(Z(M) - Lco_i, lambda);
    Zp = Lco_i + rem(Z(M) - Lco_i + Lcell, lambda);
    % from the standing wave computes the traveling wave
    SW0 = interp1(D(:,1), D(:,2), Z0(:));
    SWp = interp1(D(:,1), D(:,2), Zp(:));
    TW  = sqrt(-1) * (SWp - SW0 * exp(sqrt(-1)*ph)) / (2*sin(ph));
    TW /= max(abs(TW)); % normalize to 1
    ALL = [ IN(1:end-1) ; TW ; OUT(2:end) ]; % full structure
    Ez = grad * 1e6 * ALL; % V/m
    TWS = RF_FieldMap_1d(Ez, dS, Lstr, freq, +1);
    TWS.set_phid(phid);
    TWS.set_smooth(20);
endfunction
