function [Solenoid,S0] = load_gun_solenoid(Bmax)
    RF_Track;
    T = load('../ASTRA/C_Band_2_5_cells_gun_SOL.poi');
    S = T(:,1); % m
    S0 = min(T(:,1)); % m
    T(:,2) /= max(abs(T(:,2)));
    Bz = T(:,2) * Bmax; % T
    Solenoid = Static_Magnetic_FieldMap_1d(Bz, S(2) - S(1));
    Solenoid.set_smooth(10);
endfunction

