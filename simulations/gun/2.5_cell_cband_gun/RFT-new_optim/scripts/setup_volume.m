function V = setup_volume(setup)
    RF_Track;
    % gun
    Gun = load_gun(setup.gun_phid, setup.gun_grad); 
    % gun solenoid
    [Sol, Sol_S] = load_gun_solenoid(setup.gun_sol_bmax);
    % linac traveling wave - solenoid
    [TWS, TWS_S] = load_tws_solenoid(setup.tws_sol_bmax);
    % volume
    V = Volume();
    V.add(Gun, 0, 0, 0);
    V.add(Sol, 0, 0, 0.165 + Sol_S);
    V.add(TWS, 0, 0, setup.tws_pos - 1.5 + 2.52 + TWS_S);
    % add linac traveling wave structures
    tws_cpos = setup.tws_pos - 1.5 + [ 1.5 3.95 6.34 8.735 ]; % m
    TWL = load_tws(setup.tws_phid, setup.tws_grad);
    for i=1:4
        V.add(TWL, 0, 0, tws_cpos(i));
    end
    V.set_aperture(0.1)
endfunction
