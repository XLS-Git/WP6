phi_axis = 91:150;

TABLE = zeros(length(phi_axis), 1+8+1);

for i=1:length(phi_axis)
    phi = phi_axis(i);
    TABLE(i,1) = phi;
    try
        load(sprintf('scan1/M_scan1_%d.dat.gz', phi));
        TABLE(i,2:end) = [ M' size(M1,1) ];
        %{
        figure(1)
        clf
        subplot(2,2,1)
        scatter(M1(:,1), M1(:,2))
        title(sprintf('phi = %d', phi));
        subplot(2,2,2)
        scatter(M1(:,5), M1(:,6))
        title(sprintf('N = %d', size(M1,1)));
        subplot(2,2,3)
        scatter(M1(:,3), M1(:,4))
        subplot(2,2,4)
        hist(M1(:,6),100);
        title(sprintf('sigma_z = %g mm/c', std(M1(:,5))));
        drawnow;
        pause(0.1)
        %}
    end
end

save -text scan1/reduce.dat TABLE

figure(1)
clf ; hold on
semilogy(TABLE(:,1), TABLE(:,2) / 0.2, 'linewidth', 2);
semilogy(TABLE(:,1), TABLE(:,3) / 0.3, 'linewidth', 2);
semilogy(TABLE(:,1), TABLE(:,4) / 0.2, 'linewidth', 2);
semilogy(TABLE(:,1), TABLE(:,end) / 50000, 'linewidth', 2);
legend('emitt_{4d} / 0.2 mm.mrad   ', ...
       '\sigma_r / 0.3 mm', ...
       '\sigma_t / 200 {\mu}m/c', ...
       'N / N_{total}');
xlabel('\phi_{gun} [deg]')
grid
print -dpng scan1/plot_1a.png

clf ; hold on
plot(TABLE(:,1), TABLE(:,5), 'linewidth', 2);
legend('mean K / MeV');
grid
print -dpng scan1/plot_1b.png

clf ; hold on
plot(TABLE(:,1), TABLE(:,6), 'linewidth', 2);
plot(TABLE(:,1), TABLE(:,7), 'linewidth', 2);
legend('skewness(t)', ... 
       'skewness(P)');
grid
print -dpng scan1/plot_1c.png

clf ; hold on
plot(TABLE(:,1), TABLE(:,8), 'linewidth', 2);
plot(TABLE(:,1), TABLE(:,9), 'linewidth', 2);
legend('kurtosis(t)', ... 
       'kurtosis(P)');
grid
print -dpng scan1/plot_1d.png