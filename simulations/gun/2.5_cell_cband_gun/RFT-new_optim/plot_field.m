addpath([ pwd '/scripts' ]);

system('mkdir -p results');

%% Load RF-Track
RF_Track;
RF_Track.number_of_threads = 8;

%% Setup space-charge
SC = SpaceCharge_PIC_FreeSpace(32,32,32);
SC.set_smooth(1.0); % set smooth factor
SC.set_mirror(0.0); % set position of cathode
RF_Track.SC_engine = SC;

%% Load the beam and define B0t
global B0t P0t
B = load_beam('../ASTRA/XLS_distr_0.220_50k.ini');
B0t = Bunch6dT(B); % beam
P0t = Bunch6dT(B(1,:)); % reference particle

%% setup parameters
global setup
load setup_sigx220.dat

%% Do a lot of plots
V = setup_volume(setup);
S_axis = linspace(0, 11, 1100); % m
Br = [];
Bz = [];
Er = [];
Ez = [];
for S = S_axis
    [E0_,B0_] = V.get_field(0, 0, S*1e3, 0);
    [E1_,B1_] = V.get_field(5, 0, S*1e3, 0);
    [E2_,B2_] = V.get_field(10, 0, S*1e3, 0);
    Br = [ Br ; B0_(1) B1_(1) B2_(1) ];
    Bz = [ Bz ; B0_(3) B1_(3) B2_(3) ];
    Er = [ Er ; E0_(1) E1_(1) E2_(1) ];
    Ez = [ Ez ; E0_(3) E1_(3) E2_(3) ];
end

figure(1)
clf
hold on
plot(S_axis, Er(:,3) / 1e6, 'b-');
plot(S_axis, Er(:,2) / 1e6, 'r-');
plot(S_axis, Er(:,1) / 1e6, 'g-');
legend('r=2 mm ', 'r=1 mm ', 'r=0 mm ');
xlabel('S [m]');
ylabel('E_r [MV/m]');
print -dpng results/plot_Er.png

clf
hold on
plot(S_axis, Ez(:,3) / 1e6, 'b-');
plot(S_axis, Ez(:,2) / 1e6, 'r-');
plot(S_axis, Ez(:,1) / 1e6, 'g-');
legend('r=2 mm ', 'r=1 mm ', 'r=0 mm ');
xlabel('S [m]');
ylabel('E_z [MV/m]');
print -dpng results/plot_Ez.png

clf
hold on
plot(S_axis, Br(:,3) / 1e-3, 'b-');
plot(S_axis, Br(:,2) / 2e-3, 'r-');
plot(S_axis, Br(:,1) / 3e-3, 'g-');
legend('r=2 mm ', 'r=1 mm ', 'r=0 mm ');
xlabel('S [m]');
ylabel('dB_r / dr [T/m]');
print -dpng results/plot_Br.png

clf
hold on
plot(S_axis, Bz(:,3), 'b-');
plot(S_axis, Bz(:,2), 'r-');
plot(S_axis, Bz(:,1), 'g-');
legend('r=2 mm ', 'r=1 mm ', 'r=0 mm ');
xlabel('S [m]');
ylabel('B_z [T]');
print -dpng results/plot_Bz.png

