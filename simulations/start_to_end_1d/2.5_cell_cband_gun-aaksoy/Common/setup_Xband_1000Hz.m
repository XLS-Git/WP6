function X = setup_Xband_1000Hz()
    Track1D;
    
    clight = 0.29979246; % m/ns
    
    ## Latest Frascati's X-band structure for CompactLight
    X.Xcell = Cell();
    X.Xcell.frequency = 11.9942; % GHz
    lambda_X = clight / X.Xcell.frequency; % m
    X.Xcell.a = 0.0035; % m
    X.Xcell.l = lambda_X / 3; % m, 2*pi/3 phase advance
    X.Xcell.g = X.Xcell.l - 0.002; % m
    X.Xcell.ncell = 110; % m
    
    X.Xstructure = AcceleratingStructure(X.Xcell, X.Xcell.ncell);
    X.Xstructure.max_gradient = 100.0e-3; % GV/m

    
endfunction
