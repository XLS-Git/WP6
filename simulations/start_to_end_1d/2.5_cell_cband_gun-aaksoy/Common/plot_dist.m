
function plot_dist(beam,out)
    global Q_pC ;

    
    pkg load plot;
    
    beam=beam;

    c=299792458 ;

    zz=beam(:,1);
    ee=beam(:,2);
%      tt=beam(:,1)*1e-3/c;

    sigz=std(zz);
    sige=std(ee);

    zmim=min(zz)-sigz*0.5;
    zmax=max(zz)+sigz*0.5;
    emin=min(ee)-sige*0.5;
    emax=max(ee)+sige*0.5;
%      tmim=min(tt);
%      tmax=max(tt);
    np=151;
    nslc=51;
    zbin=linspace(zmim,zmax,np);
    ebin=linspace(emin,emax,np);
    zbin2=linspace(zmim,zmax,nslc);
    ebin2=linspace(emin,emax,nslc);
%      tbin=linspace(tmim,tmax,nslc);

    [cnt,zl,el]=hist2d(beam,zbin,ebin);

    [hz,zll] = hist(zz,zbin2);
    [he,ell] = hist(ee,ebin2);
%      [ht,tll] = hist(tt,tbin);
    
    thno=Q_pC/sum(hz);
    dt=(zll(2)-zll(1))*1e-6/c*1e12;
    
    hz=hz*thno/dt*1e-3;
    
    hz=smoothing(hz);
    

    sigz=std(zz);
    sige=std(ee);
    z0=mean(zz);
    e0=mean(ee);
    de=sige/e0*100;


    clf

%      plt=figure(1, 'position',[0,0,800,600]);

    f = figure(1,'visible','off');

    subplot (2,2,1, "align");
    plot(zll,hz);
    ylabel ('I (kA)');
    xlim([ zl(1) zl(end) ]);
    set(gca, "fontsize", 16);

    subplot (2,2,2, "align");
    strn=cstrcat ("name  ","  ", out);
    stre=cstrcat ("E     =  ", num2str(e0)," GeV");
    strz=cstrcat ("sigz =  ", num2str(sigz)," um");
    strde=cstrcat ("dE   =  ", num2str(de)," %");
    plot(0,0);
       text(-0.8,0.4,strn,"fontsize", 20);
    text(-0.8,0.2,stre,"fontsize", 20);
    text(-0.8,0,strz,"fontsize", 20);
    text(-0.8,-0.2,strde,"fontsize", 20);
    axis off;
    
    
    subplot (2,2,3, "align");
    h = imagesc(zl,el,cnt);
    axis([ zl(1) zl(end) el(1) el(end) ]);
    myColorMap = jet(256);
    myColorMap(1,:) = 1;
    colormap(myColorMap);
    xlabel ('z (mm)');
    ylabel ('E (GeV)');
    set(gca,'YDir','normal');
    grid on;
    set(gca, "fontsize", 16);

    subplot (2,2,4, "align");
    plot(he,ell);
    xlabel ('N (#)');
    ylim ( [ el(1) el(end) ]);
    set(gca, "fontsize", 16);
    set(gca,'xticklabel',[]);

    print (1,'-S800,600', strcat (out,".png"));

end


