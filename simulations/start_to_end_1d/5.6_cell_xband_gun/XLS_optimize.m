addpath('../Common');

system('mkdir -p results');

Track1D;

global B0
B0 = setup_beam();

%% load a good solution
global XLS

%% Optimization
function [F_,X_] = func(idx)
    O = optimset('TolFun', 1e-10, 'TolX', 1e-10, 'MaxFunEvals', 100000, 'MaxIter', 100000);
    randn("seed", idx*12345); X0 = randn(1,12);
    [X_, F_] = fminsearch (@(X) merit(X,X0), zeros(1,12), O);
    F_
end

F_min = Inf;
X_min = [];
N = 100;

if 1
    pkg load parallel
    [F,X] = pararrayfun(nproc, 'func', 1:N, 'UniformOutput', false);
    for i=1:N
        if F{i}<F_min
            X_min = X{i};
            F_min = F{i};
            randn("seed", i*12345);
            X0 = randn(1,12);
        end
    end
    save -text results/all.dat F X X0
else
    for i=1:N
        [F,X] = func(i);
        if F<F_min
            X_min = X;
            F_min = F;
            randn("seed", i*12345);
            X0 = randn(1,12);
            save -text results/optimum_tmp.dat XLS X_min F_min
        end
    end
end
save -text results/optimum.dat XLS X_min F_min X0

[~,B5,B2] = merit(X_min,X0);

B0_sigmaz_um = std(B0.data(:,1))
B0_energy_GeV = mean(B0.data(:,2))
B2_sigmaz_um = std(B2.data(:,1))
B2_energy_GeV = mean(B2.data(:,2))
B5_sigmaz_um = std(B5.data(:,1))
B5_energy_GeV = mean(B5.data(:,2))

if 1 % compute delays from BC1 and BC2
    Machine = setup_machine_HX(XLS);

    L = Lattice();
    L.append(Machine.L0); 
    L.append(Machine.Ka); 
    B1 = L.track_z(B0);

    L = Lattice();
    L.append(Machine.BC1);
    B2 = L.track_z(B1);

    % set <Z> = 0
    XLS.BC1_z1 = mean(B2.data(:,1));
    D = B2.data;
    D(:,1) -= XLS.BC1_z1;
    B2.set_data(D);

    L = Lattice();
    L.append(Machine.L1);
    B3 = L.track_z(B2);

    L = Lattice();
    L.append(Machine.BC2);
    B4 = L.track_z(B3);

    % set <Z> = 0
    XLS.BC2_z1 = mean(B4.data(:,1));
    D = B4.data;
    D(:,1) -= XLS.BC2_z1;
    B4.set_data(D);

    L = Lattice();
    L.append(Machine.L2);
    L.append(Machine.L3);
    B5 = L.track_z(B4);
end

XLS.Out_sigmaZ = std(B5.data(:,1));
XLS.Out_sigmaE = std(B5.data(:,2));
XLS.Out_meanE = mean(B5.data(:,2));

fid = fopen('results/XLS_layout.dat', 'w');
fields = fieldnames(XLS);
for i = 1:length(fields)
    fprintf(fid, 'XLS.%s = %.15g;\n', fields{i}, eval( [ 'XLS.' fields{i} ]))
end
fclose(fid);

clf
hold on
scatter(B0.data(:,1), 1e2*(B0.data(:,2)-mean(B0.data(:,2)))/mean(B0.data(:,2)));
scatter(B2.data(:,1), 1e2*(B2.data(:,2)-mean(B2.data(:,2)))/mean(B2.data(:,2)));
scatter(B5.data(:,1), 1e2*(B5.data(:,2)-mean(B5.data(:,2)))/mean(B5.data(:,2)));
xlabel('t [um/c]')
ylabel('dP/P [%]')
print -dpng results/XLS_plot.png

XLS_track
XLS_charge_stability
XLS_phase_stability
XLS_voltage_stability
