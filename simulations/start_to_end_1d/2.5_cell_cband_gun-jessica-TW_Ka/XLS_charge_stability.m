addpath('../Common');
Track1D;

%% load the default solution
source('results/XLS_layout.dat');

%% print out machine info
XLS

%% main

T = [];
for scale_factor = linspace(0.9, 1.1, 11)
    
    B0 = setup_beam(75 * scale_factor); % pC
    
    Machine = setup_machine_HX(XLS);
    
    L = Lattice();
    L.append(Machine.L0); 
    L.append(Machine.Ka); 
    B1 = L.track_z(B0);
    
    L = Lattice();
    L.append(Machine.BC1);
    B2 = L.track_z(B1);
    
    % set <Z> = 0
    D = B2.data;
    D(:,1) -= XLS.BC1_z1;
    B2.set_data(D);
    
    L = Lattice();
    L.append(Machine.L1);
    B3 = L.track_z(B2);
    
    L = Lattice();
    L.append(Machine.BC2);
    B4 = L.track_z(B3);
    
    % set <Z> = 0
    D = B4.data;
    D(:,1) -= XLS.BC2_z1;
    B4.set_data(D);
    
    L = Lattice();
    L.append(Machine.L2);
    L.append(Machine.L3);
    B5 = L.track_z(B4);
    
    Z = B5.data(:,1); % um
    P = B5.data(:,2);
    
    T = [ T ; scale_factor mean(Z) std(Z) mean(P) std(P) ];
    
end

H_axis = T(:,1)*100;
H_label = 'scale factor [%]';

figure(1); % , 'visible', 'off');
clf 
subplot(2,2,1); hold on
plot(H_axis, T(:,2) * 3.33564095198152);
plot(H_axis,  ones(size(T(:,1))) * XLS.Out_sigmaZ * 3.33564095198152 / 2, ':k')
plot(H_axis, -ones(size(T(:,1))) * XLS.Out_sigmaZ * 3.33564095198152 / 2, ':k')
xlabel(H_label);
ylabel('arrival time [fs]');
grid on

subplot(2,2,2); hold on
plot(H_axis, (T(:,3)-XLS.Out_sigmaZ)/XLS.Out_sigmaZ * 100);
plot(H_axis,  ones(size(T(:,1))) * 5, ':k')
plot(H_axis, -ones(size(T(:,1))) * 5, ':k')
xlabel(H_label);
ylabel('\Delta{C}/C [%]');
axis([ 98 102 -10 10 ]);
grid on

subplot(2,2,3); hold on
plot(H_axis, (T(:,4) - XLS.Out_meanE) / XLS.Out_meanE * 1e2);
plot(H_axis,  ones(size(T(:,1))) * 0.05, ':k')
plot(H_axis, -ones(size(T(:,1))) * 0.05, ':k')
xlabel(H_label);
ylabel('relative energy variation [%]');
grid on

subplot(2,2,4);
plot(H_axis, T(:,5) * 1e3);
xlabel(H_label);
ylabel('projected energy spread [MeV]');
grid on

print -dpng results/XLS_Ka_charge.png