function B0 = setup_beam(Q_pC)
    Track1D;
    
    if nargin == 0
        Q_pC = 75;
    endif
    
    M = load('pp_lin_TW_Ka_1150.ini.gz');
    M = M(:, [ 3 6 ]); % Z Pz
    M(2:end,:) += M(1,:);
    
    T = -M(:,1) * 1e6; % um/c
    P = M(:,2) / 1e9; % GeV/c
    
    T -= mean(T);
    
    % store some bunch information 
    Beam.charge_pC = Q_pC; % pC
    Beam.sigmaZ = std(T); % um/c
    Beam.sigmaP = std(P); % GeV/c, uncorrelated
    Beam.mass = 0.00051099893 % [GeV/c^2]

    B0 = Bunch1d(Beam.mass, Beam.charge_pC * 6241509.343260179, [ T P ]);
