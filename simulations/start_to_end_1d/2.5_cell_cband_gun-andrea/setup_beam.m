function B0 = setup_beam(Q_pC)
    Track1D;
    
    if nargin == 0
        Q_pC = 75;
    endif
    
    load('output_beam_RFGun.dat.gz');
    T = A1(:,5) * 1e3; % um/c
    P = A1(:,6) / 1e3; % GeV/c
    
    T -= mean(T);
    
    % store some bunch information 
    Beam.charge_pC = Q_pC; % pC
    Beam.sigmaZ = std(T); % um/c
    Beam.sigmaP = std(P); % GeV/c, uncorrelated
    Beam.mass = 0.00051099893 % [GeV/c^2]

    B0 = Bunch1d(Beam.mass, Beam.charge_pC * 6241509.343260179, [ T P ]);
