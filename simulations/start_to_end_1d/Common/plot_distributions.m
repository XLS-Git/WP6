M = importdata('new_oncrest_check_CSRON_300MeV_Xband_Hrep_stracheck.w1.asci', ' ', 45).data;
T = M(:,7) * 299792458e3; % mm/c
P = M(:,6) * 0.51099893; % MeV/c

sigma_z = std(T)

pkg load plot

clf
subplot(2,2,1);

[H,Xa,Ya] = hist2d([ T P ], linspace(-1,1,64), 64);
h = pcolor(Xa,Ya,H);
set(h, 'EdgeColor', 'none');
axis([ Xa(1) Xa(end) Ya(1) Ya(end) ]);
xlabel('mm [mrad]');
ylabel('P [MeV/c]');

subplot(2,2,2)
[H,Xa] = hist(P,64);
plot(Xa,H/1e6)
xlabel('P [MeV/c]');

subplot(2,2,3)
[H,Xa] = hist(T,linspace(-1,1,64));
plot(Xa,H/1e6)
xlabel('z [mm]');

print -dpng plot_dist.png

figure(2)
P_ = polyfit(T,P,20);

[H,Xa,Ya] = hist2d([ T P ], linspace(-1,1,64), 64);
clf
subplot(1,2,1);

[H,Xa,Ya] = hist2d([ T P-polyval(P_,T) ], 64, 64);
h = pcolor(Xa,Ya,H);
set(h, 'EdgeColor', 'none');
axis([ Xa(1) Xa(end) Ya(1) Ya(end) ]);
xlabel('mm [mrad]');
ylabel('\Delta P [MeV/c]');

subplot(1,2,2)
[H,Xa] = hist(P-polyval(P_,T),64);
plot(Xa,H/1e6)
xlabel('\Delta P [MeV/c]');

sigma_P_unc = std(P-polyval(P_,T))
print -dpng plot_P_unc.png
