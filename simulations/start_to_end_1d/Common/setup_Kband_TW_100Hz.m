function K = setup_Kband_TW_100Hz()
    Track1D;
    
    clight = 0.29979246; % m/ns
    
    ## Latest K-band structure for CompactLight
    K.Kcell = Cell();
    K.Kcell.frequency = 3 * 11.9942; % GHz
    lambda_K = clight / K.Kcell.frequency; % m
    K.Kcell.a = 0.002; % m
    K.Kcell.l = lambda_K / 3; % m, 2*pi/3 phase advance
    K.Kcell.g = K.Kcell.l - 0.0006; % m
    
    K.Kstructure = AcceleratingStructure(K.Kcell, 36);
    K.Kstructure.max_gradient = 0.0567; % GV/m

endfunction
