

source load_files.tcl

set beam0_indist cband_out_4ps_0.18mm_100k_120MeV_4.dat
set beamlinename xls_linac_hxr
set latfile xls_hxr_lattice_mine.tcl


array set BeamDefine  [bunch_parameters $script_dir/$beam0_indist]
array set BeamDefine "name beam0 filename $beam0_indist n_slice 201 charge 0.46875e9"
set BeamDefine(n_macro) [expr int($BeamDefine(npart)/$BeamDefine(n_slice))]
lassign "$BeamDefine(beta_x) $BeamDefine(beta_y) $BeamDefine(alpha_x) $BeamDefine(alpha_y)" btx bty alx aly

set e0 $BeamDefine(energy)

puts [array get BeamDefine]



set LN0_XCA_GRD 30e-3
set LN0_XCA_PHS 18.5

set LN0_KCA_GRD 32.0e-3
set LN0_KCA_PHS [expr 180+2]

set  BC1_XCA_VLT 0.0
set  BC1_XCA_PHS 0.0

set LN1_XCA_GRD 65e-3
set LN1_XCA_PHS 17.0

set LN2_XCA_GRD 26e-3
set LN2_XCA_PHS -20.0  

set LN2_XCA_GRD_DI 0.0
set LN2_XCA_PHS_DI 0.0  
set LN2_XCA_VLT_DI 0.0

set LN2_SCA_VLT 0.0
set LN2_SCA_PHS 0.0

set LN3_XCA_GRD $LN2_XCA_GRD
set LN3_XCA_PHS $LN2_XCA_PHS

set LN3_SCA_VLT 0.0
set LN3_SCA_PHS 0.0

set LN4_XCA_GRD 65e-3
set LN4_XCA_PHS -10.0  

set b0_in beam0.in
set b0_ou beam0.ou

# BeamlineNew
Girder
source $script_dir/$latfile

BeamlineSet -name $beamlinename
BeamlineUse -name $beamlinename

source ../../../../../beamline_database/placet/save_elements_table.tcl 

save_elements_table $beamlinename
