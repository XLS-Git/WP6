set term post eps enh solid colo 18 lw 2

set xlabel 's [m]'
set ylabel 'E [GeV]'

set key left

set out 'plot_energy.eps'

plot\
 'outfiles/xls_linac_hxr_emitt_no_n_100.dat' u 1:2 ti 'hard X-rays @ 100 Hz' w lp lw 2

set ylabel '{/Symbol s}_z [{/Symbol m}m]'

set key right

set out 'plot_sigmaz.eps'

plot\
 'outfiles/xls_linac_hxr_emitt_no_n_100.dat' u 1:6 ti 'hard X-rays @ 100 Hz' w lp lw 2

set out

set xlabel 's [m]'
set ylabel '{/Symbol b} [m]'

unset log y

set out 'plot_beta.eps'

plot\
 'outfiles/xls_linac_hxr.twi' u 1:2 ti '{/Symbol b}_x' w l lw 2,\
 'outfiles/xls_linac_hxr.twi' u 1:3 ti '{/Symbol b}_y' w l lw 2

set out

!for i in plot*.eps ; do epstopdf $i ; rm -f $i ; done

