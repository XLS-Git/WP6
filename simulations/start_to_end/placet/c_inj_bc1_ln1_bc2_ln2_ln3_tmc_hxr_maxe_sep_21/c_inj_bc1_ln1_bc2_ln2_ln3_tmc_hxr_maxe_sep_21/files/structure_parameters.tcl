


# *****************************************************************************************
# structure parameters
# *****************************************************************************************

set clight  299792458.0
set pi [expr acos(-1.)]


# sband deflecting structure
set sdfreq      2.997e0
set sdcell_l    [format " %.6e " [expr $clight/$sdfreq/3.0/1e9] ]
set sdbore_r    12e-3
set sddia       [format " %.6e " 44e-3 ]
set sdgap       [format " %.6e " [expr $sdcell_l - 5e-3]]
set sdno_of_cel 16
set sdlambda    [format " %.6e " [expr  $clight/$sdfreq/1e9]]
set sdactv_l    [format " %.6e " [expr  $sdno_of_cel*$sdcell_l ]]
set sdtot_l     [format " %.6e " [expr  0.7]]
set sdedge_l    [format " %.6e " [expr  0.5*($sdtot_l-$sdactv_l)]]
set sdgrad      1e-3
set sdvolt      [expr $sdactv_l*$sdgrad]
set sdtrwkfact  1.0
set sdlnwkfact  1.0
set sdlngf      sdwake_l.dat
set sdtrsf      sdwake_t.dat

array set sdband_str   "freq $sdfreq aper  $sdbore_r gap $sdgap cell_l $sdcell_l grad $sdgrad volt $sdvolt "
array set sdband_str   "dia  $sddia n_cell $sdno_of_cel active_l $sdactv_l edge_l $sdedge_l"
array set sdband_str   "total_l $sdtot_l lambda $sdlambda wake_long_file $sdlngf wake_tran_file $sdtrsf trwakefact $sdtrwkfact lnwakefact $sdlnwkfact"


Octave {
    sdband_str =struct('grad',$sdband_str(grad),'freq',$sdband_str(freq),'aper',$sdband_str(aper), 'gap',$sdband_str(gap),'cell_l', $sdband_str(cell_l), 'cell_no',$sdband_str(n_cell),'total_l',$sdband_str(total_l),'active_l',$sdband_str(active_l), 'wlfact', $sdband_str(lnwakefact), 'wtfact', $sdband_str(trwakefact), 'wake_long_file','$sdband_str(wake_long_file)','wake_tran_file','$sdband_str(wake_tran_file)');
}


# cband structure
set cfreq      5.9971
set ccell_l    [format " %.6e " [expr $clight/$cfreq/3.0/1e9] ]
set cbore_r    6.3e-3
set cdia       [format " %.6e " 22.194e-3 ]
set cgap       [format " %.6e " [expr $ccell_l - 2.5e-3]]
set cno_of_cel 114
set clambda    [format " %.6e " [expr  $clight/$cfreq/1e9]]
set cactv_l    [format " %.6e " [expr  $cno_of_cel*$ccell_l ]]
set ctot_l     [format " %.6e " [expr  2.0]]
set cedge_l    [format " %.6e " [expr  0.5*($ctot_l-$cactv_l)]]
set cgrad      40e-3
set cvolt      [expr $cactv_l*$cgrad]
set ctrwkfact  1.0
set clnwkfact  0.95
set clngf      cwake_l.dat
set ctrsf      cwake_t.dat

array set cband_str   "freq $cfreq aper  $cbore_r gap $cgap cell_l $ccell_l grad $cgrad volt $cvolt "
array set cband_str   "dia  $cdia n_cell $cno_of_cel active_l $cactv_l edge_l $cedge_l"
array set cband_str   "total_l $ctot_l lambda $clambda wake_long_file $clngf wake_tran_file $ctrsf trwakefact $ctrwkfact lnwakefact $clnwkfact"


Octave {
    cband_str =struct('grad',$cband_str(grad),'freq',$cband_str(freq),'aper',$cband_str(aper), 'gap',$cband_str(gap),'cell_l', $cband_str(cell_l), 'cell_no',$cband_str(n_cell),'total_l',$cband_str(total_l),'active_l',$cband_str(active_l), 'wlfact', $cband_str(lnwakefact), 'wtfact', $cband_str(trwakefact), 'wake_long_file','$cband_str(wake_long_file)','wake_tran_file','$cband_str(wake_tran_file)');
}



# xband structure
set xfreq      11.9942e0
set xcell_l    [format " %.6e " [expr $clight/$xfreq/3.0/1e9] ]
set xbore_r    3.5e-3
set xdia       [format " %.6e " 10.139e-3 ]
set xgap       [format " %.6e " [expr $xcell_l - 2.0e-3]]
set xno_of_cel 110
set xlambda    [format " %.6e " [expr  $clight/$xfreq/1e9]]
set xactv_l    [format " %.6e " [expr  $xno_of_cel*$xcell_l ]]
set xtot_l     [format " %.6e " [expr  1.03]]
set xedge_l    [format " %.6e " [expr  0.5*($xtot_l-$xactv_l)]]
set xgrad      65e-3
set xvolt      [expr $xactv_l*$xgrad]
set xtrwkfact  1.0
set xlnwkfact  1.0
set xlngf      xwake_l.dat
set xtrsf      xwake_t.dat


array set xband_str   "freq $xfreq aper $xbore_r gap $xgap cell_l $xcell_l grad $xgrad volt $xvolt "
array set xband_str   "dia $xdia n_cell $xno_of_cel active_l $xactv_l edge_l $xedge_l"
array set xband_str   "total_l $xtot_l lambda $xlambda wake_long_file $xlngf wake_tran_file $xtrsf trwakefact $xtrwkfact lnwakefact $xlnwkfact"

Octave {
    xband_str =struct('grad',$xband_str(grad),'freq',$xband_str(freq),'aper',$xband_str(aper), 'gap',$xband_str(gap),'cell_l', $xband_str(cell_l), 'cell_no',$xband_str(n_cell),'total_l',$xband_str(total_l),'active_l',$xband_str(active_l),  'wlfact', $xband_str(lnwakefact), 'wtfact', $xband_str(trwakefact), 'wake_long_file','$xband_str(wake_long_file)','wake_tran_file','$xband_str(wake_tran_file)');
}


# kband structure
set kfreq       [expr 3.0*$xfreq]
set kcell_l     [format " %.6e " [expr $clight/$kfreq/3.0/1e9]]
set kbore_r     2.0e-3
set kdia        [format " %.6e " [expr $xdia/3.0]]
set kgap        [format " %.6e " [expr $kcell_l - 0.6e-3]]
set kno_of_cel  110
set klambda     [format " %.6e " [expr  $clight/$kfreq/1e9]]
set kactv_l     [format " %.6e " [expr  $kno_of_cel*$kcell_l ]]
set ktot_l      [format " %.6e " [expr  0.35]]
set kedge_l     [format " %.6e " [expr  0.5*($ktot_l-$kactv_l)]]
set kgrad       15e-3
set kvolt       [expr $kactv_l*$kgrad]
set ktrwkfact  1.0
set klnwkfact  1.0
set klngf      kwake_l.dat
set ktrsf      kwake_t.dat


array set kband_str   "freq $kfreq aper $kbore_r gap $kgap cell_l $kcell_l grad $kgrad volt $kvolt "
array set kband_str   "dia $kdia n_cell $kno_of_cel active_l $kactv_l edge_l $kedge_l"
array set kband_str   "total_l $ktot_l lambda $klambda wake_long_file $klngf wake_tran_file $ktrsf trwakefact $ktrwkfact lnwakefact $klnwkfact"

Octave {
    kband_str =struct('grad',$kband_str(grad),'freq',$kband_str(freq),'aper',$kband_str(aper), 'gap',$kband_str(gap),'cell_l', $kband_str(cell_l), 'cell_no',$kband_str(n_cell),'total_l',$kband_str(total_l),'active_l',$kband_str(active_l),  'wlfact', $kband_str(lnwakefact), 'wtfact', $kband_str(trwakefact), 'wake_long_file','$kband_str(wake_long_file)','wake_tran_file','$kband_str(wake_tran_file)');
}


# xband deflecting structure
set xdfreq      11.9942e0
set xdcell_l    [format " %.6e " [expr $clight/$xdfreq/3.0/1e9] ]
set xdbore_r    4.0e-3
set xddia       [format " %.6e " 10.0e-3 ]
set xdgap       [format " %.6e " [expr $xdcell_l - 2.6e-3]]
set xdno_of_cel 120
set xdlambda    [format " %.6e " [expr  $clight/$xdfreq/1e9]]
set xdactv_l    [format " %.6e " [expr  $xdno_of_cel*$xdcell_l ]]
set xdtot_l     [format " %.6e " [expr  1.2]]
set xdedge_l    [format " %.6e " [expr  0.5*($xdtot_l-$xdactv_l)]]
set xdgrad      20e-3
set xdvolt      [expr $xactv_l*$xgrad]
set xdtrwkfact  1.0
set xdlnwkfact  1.0

set xdlngf      xdwake_l.dat
set xdtrsf      xdwake_t.dat


array set xdband_str   "freq $xdfreq aper $xdbore_r gap $xdgap cell_l $xdcell_l grad $xdgrad volt $xdvolt "
array set xdband_str   "dia $xddia n_cell $xdno_of_cel active_l $xdactv_l edge_l $xdedge_l"
array set xdband_str   "total_l $xdtot_l lambda $xdlambda wake_long_file $xdlngf wake_tran_file $xdtrsf trwakefact $xdtrwkfact lnwakefact $xdlnwkfact"


Octave {
    xdband_str =struct('grad',$xdband_str(grad),'freq',$xdband_str(freq),'aper',$xdband_str(aper), 'gap',$xdband_str(gap),'cell_l', $xdband_str(cell_l), 'cell_no',$xdband_str(n_cell),'total_l',$xdband_str(total_l),'active_l',$xdband_str(active_l), 'wlfact', $xdband_str(lnwakefact), 'wtfact', $xdband_str(trwakefact), 'wake_long_file','$xdband_str(wake_long_file)','wake_tran_file','$xdband_str(wake_tran_file)');
}



