Girder
SetReferenceEnergy  $e0
Drift      -name    "INJ_WA_OU" -length 0 
Drift      -name    "LN0_DR_V1" -length 0.230985127288 
Drift      -name    "LN0_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN0_QD_V01"  -length 0.04  -strength [expr 0.04*$e0*10.0895578662] -e0 $e0
Dipole     -name    "LN0_COR" -length 0 
Bpm        -name    "LN0_BPM" -length 0 
Quadrupole -name    "LN0_QD_V01"  -length 0.04  -strength [expr 0.04*$e0*10.0895578662] -e0 $e0
Drift      -name    "LN0_DR_QD_ED" -length 0.0425 
Drift      -name    "LN0_DR_V2" -length 0.200000450255 
Drift      -name    "LN0_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN0_QD_V02"  -length 0.04  -strength [expr 0.04*$e0*-11.2523402074] -e0 $e0
Dipole     -name    "LN0_COR" -length 0 
Bpm        -name    "LN0_BPM" -length 0 
Quadrupole -name    "LN0_QD_V02"  -length 0.04  -strength [expr 0.04*$e0*-11.2523402074] -e0 $e0
Drift      -name    "LN0_DR_QD_ED" -length 0.0425 
Drift      -name    "LN0_DR_V3" -length 1.88139530032 
Drift      -name    "LN0_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN0_QD_V03"  -length 0.04  -strength [expr 0.04*$e0*5.55804058826] -e0 $e0
Dipole     -name    "LN0_COR" -length 0 
Bpm        -name    "LN0_BPM" -length 0 
Quadrupole -name    "LN0_QD_V03"  -length 0.04  -strength [expr 0.04*$e0*5.55804058826] -e0 $e0
Drift      -name    "LN0_DR_QD_ED" -length 0.0425 
Drift      -name    "LN0_DR_V4" -length 0.20136262021 
Drift      -name    "LN0_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN0_QD_V04"  -length 0.04  -strength [expr 0.04*$e0*-2.64219688195] -e0 $e0
Dipole     -name    "LN0_COR" -length 0 
Bpm        -name    "LN0_BPM" -length 0 
Quadrupole -name    "LN0_QD_V04"  -length 0.04  -strength [expr 0.04*$e0*-2.64219688195] -e0 $e0
Drift      -name    "LN0_DR_QD_ED" -length 0.0425 
Drift      -name    "LN0_DR_V5" -length 1.96871495047 
Drift      -name    "LN0_WA_M_OU" -length 0 
Drift      -name    "LN0_DR_20" -length 0.2 
Sbend      -name    "LN0_DP_DIP1" -length 0.1  -angle -0.08726646  -E1 0 -E2 -0.08726646 -hgap 0 -fint 0.5 -tilt 0 -e0 $e0 -six_dim 1 
Drift      -name    "LN0_DR_20" -length 0.2 
Sbend      -name    "LN0_DP_DIP2" -length 0.1  -angle 0.08726646  -E1 0.08726646 -E2 0 -hgap 0 -fint 0.5 -tilt 0 -e0 $e0 -six_dim 1 
Drift      -name    "LN0_DR_20" -length 0.2 
Bpm        -name    "LN0_BPM_LH" -length 0 
Drift      -name    "LN0_DR_20" -length 0.2 
Drift      -name    "LN0_UN_UND" -length 0.24 
Drift      -name    "LN0_DR_20" -length 0.2 
Bpm        -name    "LN0_BPM_LH" -length 0 
Drift      -name    "LN0_DR_VS" -length 0.1 
Drift      -name    "LN0_DR_10" -length 0.1 
Sbend      -name    "LN0_DP_DIP3" -length 0.1  -angle 0.08726646  -E1 0 -E2 0.08726646 -hgap 0 -fint 0.5 -tilt 0 -e0 $e0 -six_dim 1 
Drift      -name    "LN0_DR_20" -length 0.2 
Sbend      -name    "LN0_DP_DIP4" -length 0.1  -angle -0.08726646  -E1 -0.08726646 -E2 0 -hgap 0 -fint 0.5 -tilt 0 -e0 $e0 -six_dim 1 
Drift      -name    "LN0_DR_20" -length 0.2 
Drift      -name    "LN0_WA_L_OU" -length 0 
Drift      -name    "LN0_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN0_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*8.4] -e0 $e0
Dipole     -name    "LN0_COR" -length 0 
Bpm        -name    "LN0_BPM" -length 0 
Quadrupole -name    "LN0_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*8.4] -e0 $e0
Drift      -name    "LN0_DR_QD_ED" -length 0.0425 
Drift      -name    "LN0_DR_WP" -length 0.1 
Drift      -name    "LN0_DR_BL" -length 0.05 
Drift      -name    "LN0_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN0_XCA0" -length 0.9164755  -gradient $LN0_XCA_GRD -phase $LN0_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN0_XCA_GRD*cos(3.14159265359*$LN0_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN0_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN0_DR_BL" -length 0.05 
Drift      -name    "LN0_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN0_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-8.4] -e0 $e0
Dipole     -name    "LN0_COR" -length 0 
Bpm        -name    "LN0_BPM" -length 0 
Quadrupole -name    "LN0_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-8.4] -e0 $e0
Drift      -name    "LN0_DR_QD_ED" -length 0.0425 
Drift      -name    "LN0_DR_VS" -length 0.1 
Drift      -name    "LN0_DR_BL" -length 0.05 
Drift      -name    "LN0_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN0_XCA0" -length 0.9164755  -gradient $LN0_XCA_GRD -phase $LN0_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN0_XCA_GRD*cos(3.14159265359*$LN0_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN0_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN0_DR_BL" -length 0.05 
Drift      -name    "LN0_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN0_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*8.4] -e0 $e0
Dipole     -name    "LN0_COR" -length 0 
Bpm        -name    "LN0_BPM" -length 0 
Quadrupole -name    "LN0_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*8.4] -e0 $e0
Drift      -name    "LN0_DR_QD_ED" -length 0.0425 
Drift      -name    "LN0_DR_VS" -length 0.1 
Drift      -name    "LN0_DR_BL" -length 0.05 
Drift      -name    "LN0_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN0_XCA0" -length 0.9164755  -gradient $LN0_XCA_GRD -phase $LN0_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN0_XCA_GRD*cos(3.14159265359*$LN0_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN0_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN0_DR_BL" -length 0.05 
Drift      -name    "LN0_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN0_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-8.4] -e0 $e0
Dipole     -name    "LN0_COR" -length 0 
Bpm        -name    "LN0_BPM" -length 0 
Quadrupole -name    "LN0_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-8.4] -e0 $e0
Drift      -name    "LN0_DR_QD_ED" -length 0.0425 
Drift      -name    "LN0_DR_VS" -length 0.1 
Drift      -name    "LN0_DR_BL" -length 0.05 
Drift      -name    "LN0_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN0_XCA0" -length 0.9164755  -gradient $LN0_XCA_GRD -phase $LN0_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN0_XCA_GRD*cos(3.14159265359*$LN0_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN0_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN0_DR_BL" -length 0.05 
Drift      -name    "LN0_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN0_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*8.4] -e0 $e0
Dipole     -name    "LN0_COR" -length 0 
Bpm        -name    "LN0_BPM" -length 0 
Quadrupole -name    "LN0_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*8.4] -e0 $e0
Drift      -name    "LN0_DR_QD_ED" -length 0.0425 
Drift      -name    "LN0_DR_WP" -length 0.1 
Drift      -name    "LN0_DR_BL" -length 0.05 
Drift      -name    "LN0_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN0_XCA0" -length 0.9164755  -gradient $LN0_XCA_GRD -phase $LN0_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN0_XCA_GRD*cos(3.14159265359*$LN0_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN0_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN0_DR_BL" -length 0.05 
Drift      -name    "LN0_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN0_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-8.4] -e0 $e0
Dipole     -name    "LN0_COR" -length 0 
Bpm        -name    "LN0_BPM" -length 0 
Quadrupole -name    "LN0_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-8.4] -e0 $e0
Drift      -name    "LN0_DR_QD_ED" -length 0.0425 
Drift      -name    "LN0_DR_VS" -length 0.1 
Drift      -name    "LN0_DR_BL" -length 0.05 
Drift      -name    "LN0_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN0_XCA0" -length 0.9164755  -gradient $LN0_XCA_GRD -phase $LN0_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN0_XCA_GRD*cos(3.14159265359*$LN0_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN0_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN0_DR_BL" -length 0.05 
Drift      -name    "LN0_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN0_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*8.4] -e0 $e0
Dipole     -name    "LN0_COR" -length 0 
Bpm        -name    "LN0_BPM" -length 0 
Quadrupole -name    "LN0_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*8.4] -e0 $e0
Drift      -name    "LN0_DR_QD_ED" -length 0.0425 
Drift      -name    "LN0_DR_VS" -length 0.1 
Drift      -name    "LN0_DR_BL" -length 0.05 
Drift      -name    "LN0_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN0_XCA0" -length 0.9164755  -gradient $LN0_XCA_GRD -phase $LN0_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN0_XCA_GRD*cos(3.14159265359*$LN0_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN0_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN0_DR_BL" -length 0.05 
Drift      -name    "LN0_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN0_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-8.4] -e0 $e0
Dipole     -name    "LN0_COR" -length 0 
Bpm        -name    "LN0_BPM" -length 0 
Quadrupole -name    "LN0_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-8.4] -e0 $e0
Drift      -name    "LN0_DR_QD_ED" -length 0.0425 
Drift      -name    "LN0_DR_VS" -length 0.1 
Drift      -name    "LN0_DR_BL" -length 0.05 
Drift      -name    "LN0_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN0_XCA0" -length 0.9164755  -gradient $LN0_XCA_GRD -phase $LN0_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN0_XCA_GRD*cos(3.14159265359*$LN0_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN0_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN0_DR_BL" -length 0.05 
Drift      -name    "LN0_WA_LNZ_IN" -length 0 
Drift      -name    "LN0_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN0_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*8.4] -e0 $e0
Dipole     -name    "LN0_COR" -length 0 
Bpm        -name    "LN0_BPM" -length 0 
Quadrupole -name    "LN0_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*8.4] -e0 $e0
Drift      -name    "LN0_DR_QD_ED" -length 0.0425 
Drift      -name    "LN0_DR_VS" -length 0.1 
Drift      -name    "LN0_DR_LNZ_ED2" -length 0.14 
Drift      -name    "LN0_DR_BL" -length 0.05 
Drift      -name    "LN0_DR_KCA_ED" -length 0.0222541 
Cavity     -name    "LN0_KCA0" -length 0.3054918  -gradient $LN0_KCA_GRD -phase $LN0_KCA_PHS -frequency 35.9826
set e0      [expr $e0 + 0.3054918*$LN0_KCA_GRD*cos(3.14159265359*$LN0_KCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN0_DR_KCA_ED" -length 0.0222541 
Drift      -name    "LN0_DR_BL" -length 0.05 
Drift      -name    "LN0_DR_KCA_ED" -length 0.0222541 
Cavity     -name    "LN0_KCA0" -length 0.3054918  -gradient $LN0_KCA_GRD -phase $LN0_KCA_PHS -frequency 35.9826
set e0      [expr $e0 + 0.3054918*$LN0_KCA_GRD*cos(3.14159265359*$LN0_KCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN0_DR_KCA_ED" -length 0.0222541 
Drift      -name    "LN0_DR_BL" -length 0.05 
Drift      -name    "LN0_DR_LNZ_ED2" -length 0.14 
Drift      -name    "LN0_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN0_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-8.4] -e0 $e0
Dipole     -name    "LN0_COR" -length 0 
Bpm        -name    "LN0_BPM" -length 0 
Quadrupole -name    "LN0_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-8.4] -e0 $e0
Drift      -name    "LN0_DR_QD_ED" -length 0.0425 
Drift      -name    "LN0_DR_VS" -length 0.1 
Drift      -name    "LN0_DR_LNZ_ED2" -length 0.14 
Drift      -name    "LN0_DR_BL" -length 0.05 
Drift      -name    "LN0_DR_KCA_ED" -length 0.0222541 
Cavity     -name    "LN0_KCA0" -length 0.3054918  -gradient $LN0_KCA_GRD -phase $LN0_KCA_PHS -frequency 35.9826
set e0      [expr $e0 + 0.3054918*$LN0_KCA_GRD*cos(3.14159265359*$LN0_KCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN0_DR_KCA_ED" -length 0.0222541 
Drift      -name    "LN0_DR_BL" -length 0.05 
Drift      -name    "LN0_DR_KCA_ED" -length 0.0222541 
Drift      -name    "LN0_DR_KCA0" -length 0.3054918 
Drift      -name    "LN0_DR_KCA_ED" -length 0.0222541 
Drift      -name    "LN0_DR_BL" -length 0.05 
Drift      -name    "LN0_DR_LNZ_ED2" -length 0.14 
Drift      -name    "LN0_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN0_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*8.4] -e0 $e0
Dipole     -name    "LN0_COR" -length 0 
Bpm        -name    "LN0_BPM" -length 0 
Quadrupole -name    "LN0_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*8.4] -e0 $e0
Drift      -name    "LN0_DR_QD_ED" -length 0.0425 
Drift      -name    "LN0_DR_DR" -length 0.1 
Drift      -name    "LN0_DR_BL" -length 0.05 
Drift      -name    "LN0_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN0_DR_XCA0H" -length 0.45823775 
Drift      -name    "LN0_WA_OU" -length 0 
Drift      -name    "BC1_DR_V1" -length 0.2 
Drift      -name    "BC1_DR_QD_ED" -length 0.0425 
Quadrupole -name    "BC1_QD_V01"  -length 0.04  -strength [expr 0.04*$e0*-0.563621871838] -e0 $e0
Dipole     -name    "BC1_COR" -length 0 
Bpm        -name    "BC1_BPM" -length 0 
Quadrupole -name    "BC1_QD_V01"  -length 0.04  -strength [expr 0.04*$e0*-0.563621871838] -e0 $e0
Drift      -name    "BC1_DR_QD_ED" -length 0.0425 
Drift      -name    "BC1_DR_V2" -length 4.38565793515 
Drift      -name    "BC1_DR_QD_ED" -length 0.0425 
Quadrupole -name    "BC1_QD_V02"  -length 0.04  -strength [expr 0.04*$e0*-11.2852803274] -e0 $e0
Dipole     -name    "BC1_COR" -length 0 
Bpm        -name    "BC1_BPM" -length 0 
Quadrupole -name    "BC1_QD_V02"  -length 0.04  -strength [expr 0.04*$e0*-11.2852803274] -e0 $e0
Drift      -name    "BC1_DR_QD_ED" -length 0.0425 
Drift      -name    "BC1_DR_V3" -length 0.2 
Drift      -name    "BC1_DR_QD_ED" -length 0.0425 
Quadrupole -name    "BC1_QD_V03"  -length 0.04  -strength [expr 0.04*$e0*12.7647141217] -e0 $e0
Dipole     -name    "BC1_COR" -length 0 
Bpm        -name    "BC1_BPM" -length 0 
Quadrupole -name    "BC1_QD_V03"  -length 0.04  -strength [expr 0.04*$e0*12.7647141217] -e0 $e0
Drift      -name    "BC1_DR_QD_ED" -length 0.0425 
Drift      -name    "BC1_DR_V4" -length 0.2 
Drift      -name    "BC1_DR_QD_ED" -length 0.0425 
Quadrupole -name    "BC1_QD_V04"  -length 0.04  -strength [expr 0.04*$e0*-1.28396240659] -e0 $e0
Dipole     -name    "BC1_COR" -length 0 
Bpm        -name    "BC1_BPM" -length 0 
Quadrupole -name    "BC1_QD_V04"  -length 0.04  -strength [expr 0.04*$e0*-1.28396240659] -e0 $e0
Drift      -name    "BC1_DR_QD_ED" -length 0.0425 
Drift      -name    "BC1_DR_V5" -length 0.2 
Drift      -name    "BC1_DR_20" -length 0.5 
Sbend      -name    "BC1_DP_DIP1" -length 0.30013439  -angle -0.05183628  -E1 0 -E2 -0.05183628 -hgap 0 -fint 0.5 -tilt 0 -e0 $e0 -six_dim 1 
Drift      -name    "BC1_DR_SIDE" -length 3.00403502 
Sbend      -name    "BC1_DP_DIP2" -length 0.30013439  -angle 0.05183628  -E1 0.05183628 -E2 0 -hgap 0 -fint 0.5 -tilt 0 -e0 $e0 -six_dim 1 
Drift      -name    "BC1_DR_CENT" -length 0.5 
Drift      -name    "BC1_DR_CENT" -length 0.5 
Sbend      -name    "BC1_DP_DIP3" -length 0.30013439  -angle 0.05183628  -E1 0 -E2 0.05183628 -hgap 0 -fint 0.5 -tilt 0 -e0 $e0 -six_dim 1 
Drift      -name    "BC1_DR_SIDE" -length 3.00403502 
Sbend      -name    "BC1_DP_DIP4" -length 0.30013439  -angle -0.05183628  -E1 -0.05183628 -E2 0 -hgap 0 -fint 0.5 -tilt 0 -e0 $e0 -six_dim 1 
Drift      -name    "BC1_DR_20" -length 0.5 
Drift      -name    "BC1_WA_OU" -length 0 
Drift      -name    "BC1_DR_DI_V1" -length 0.200035322359 
Drift      -name    "BC1_DR_QD_ED" -length 0.0425 
Quadrupole -name    "BC1_QD_DI_V01"  -length 0.04  -strength [expr 0.04*$e0*-8.25275384527] -e0 $e0
Dipole     -name    "BC1_COR_DI" -length 0 
Bpm        -name    "BC1_BPM_DI" -length 0 
Quadrupole -name    "BC1_QD_DI_V01"  -length 0.04  -strength [expr 0.04*$e0*-8.25275384527] -e0 $e0
Drift      -name    "BC1_DR_QD_ED" -length 0.0425 
Drift      -name    "BC1_DR_DI_V2" -length 0.200745896403 
Drift      -name    "BC1_DR_QD_ED" -length 0.0425 
Quadrupole -name    "BC1_QD_DI_V02"  -length 0.04  -strength [expr 0.04*$e0*11.8195496816] -e0 $e0
Dipole     -name    "BC1_COR_DI" -length 0 
Bpm        -name    "BC1_BPM_DI" -length 0 
Quadrupole -name    "BC1_QD_DI_V02"  -length 0.04  -strength [expr 0.04*$e0*11.8195496816] -e0 $e0
Drift      -name    "BC1_DR_QD_ED" -length 0.0425 
Drift      -name    "BC1_DR_DI_V3" -length 1.89413461691 
Drift      -name    "BC1_DR_QD_ED" -length 0.0425 
Quadrupole -name    "BC1_QD_DI_V03"  -length 0.04  -strength [expr 0.04*$e0*-5.84683366148] -e0 $e0
Dipole     -name    "BC1_COR_DI" -length 0 
Bpm        -name    "BC1_BPM_DI" -length 0 
Quadrupole -name    "BC1_QD_DI_V03"  -length 0.04  -strength [expr 0.04*$e0*-5.84683366148] -e0 $e0
Drift      -name    "BC1_DR_QD_ED" -length 0.0425 
Drift      -name    "BC1_DR_DI_V5" -length 0.2 
Drift      -name    "BC1_WA_M_OU_DI" -length 0 
Drift      -name    "BC1_DR_DI" -length 0.25 
Drift      -name    "BC1_DR_VS" -length 0.1 
Drift      -name    "BC1_DR_BL" -length 0.05 
Drift      -name    "BC1_DR_XCA_ED" -length 0.1001043 
CrabCavity -name    "BC1_XCA0" -length 0.9997914  -voltage $BC1_XCA_VLT -phase $BC1_XCA_PHS -frequency 11.9942
Drift      -name    "BC1_DR_XCA_ED" -length 0.1001043 
Drift      -name    "BC1_DR_BL" -length 0.05 
Drift      -name    "BC1_DR_DI" -length 0.25 
Drift      -name    "BC1_DR_DI" -length 0.25 
Drift      -name    "BC1_DR_DI_QD_ED" -length 0.0425 
Quadrupole -name    "BC1_QD_DI_FH"  -length 0.04  -strength [expr 0.04*$e0*8] -e0 $e0
Dipole     -name    "BC1_COR_DI" -length 0 
Bpm        -name    "BC1_BPM_DI" -length 0 
Quadrupole -name    "BC1_QD_DI_FH"  -length 0.04  -strength [expr 0.04*$e0*8] -e0 $e0
Drift      -name    "BC1_DR_DI_QD_ED" -length 0.0425 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_DI_VS" -length 0.1 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_DI_QD_ED" -length 0.0425 
Quadrupole -name    "BC1_QD_DI_DH"  -length 0.04  -strength [expr 0.04*$e0*-8] -e0 $e0
Dipole     -name    "BC1_COR_DI" -length 0 
Bpm        -name    "BC1_BPM_DI" -length 0 
Quadrupole -name    "BC1_QD_DI_DH"  -length 0.04  -strength [expr 0.04*$e0*-8] -e0 $e0
Drift      -name    "BC1_DR_DI_QD_ED" -length 0.0425 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_DI_VS" -length 0.1 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_DI_QD_ED" -length 0.0425 
Quadrupole -name    "BC1_QD_DI_FH"  -length 0.04  -strength [expr 0.04*$e0*8] -e0 $e0
Dipole     -name    "BC1_COR_DI" -length 0 
Bpm        -name    "BC1_BPM_DI" -length 0 
Quadrupole -name    "BC1_QD_DI_FH"  -length 0.04  -strength [expr 0.04*$e0*8] -e0 $e0
Drift      -name    "BC1_DR_DI_QD_ED" -length 0.0425 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_DI_VS" -length 0.1 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_DI_QD_ED" -length 0.0425 
Quadrupole -name    "BC1_QD_DI_DH"  -length 0.04  -strength [expr 0.04*$e0*-8] -e0 $e0
Dipole     -name    "BC1_COR_DI" -length 0 
Bpm        -name    "BC1_BPM_DI" -length 0 
Quadrupole -name    "BC1_QD_DI_DH"  -length 0.04  -strength [expr 0.04*$e0*-8] -e0 $e0
Drift      -name    "BC1_DR_DI_QD_ED" -length 0.0425 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_DI_VS" -length 0.1 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_DI_QD_ED" -length 0.0425 
Quadrupole -name    "BC1_QD_DI_FH"  -length 0.04  -strength [expr 0.04*$e0*8] -e0 $e0
Dipole     -name    "BC1_COR_DI" -length 0 
Bpm        -name    "BC1_BPM_DI" -length 0 
Quadrupole -name    "BC1_QD_DI_FH"  -length 0.04  -strength [expr 0.04*$e0*8] -e0 $e0
Drift      -name    "BC1_DR_DI_QD_ED" -length 0.0425 
Drift      -name    "BC1_DR_DI" -length 0.25 
Sbend      -name    "BC1_DP_DI" -length 0.25  -angle 0  -E1 0 -E2 0 -hgap 0 -fint 0.5 -tilt 0 -e0 $e0 -six_dim 1 
Drift      -name    "BC1_DR_DI" -length 0.25 
Drift      -name    "BC1_DR_DI" -length 0.25 
Drift      -name    "BC1_DR_DI" -length 0.25 
Drift      -name    "BC1_WA_IN_DI" -length 0 
Drift      -name    "LN1_DR_V1" -length 0.200662494052 
Drift      -name    "LN1_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN1_QD_V01"  -length 0.04  -strength [expr 0.04*$e0*5.48046970618] -e0 $e0
Dipole     -name    "LN1_COR" -length 0 
Bpm        -name    "LN1_BPM" -length 0 
Quadrupole -name    "LN1_QD_V01"  -length 0.04  -strength [expr 0.04*$e0*5.48046970618] -e0 $e0
Drift      -name    "LN1_DR_QD_ED" -length 0.0425 
Drift      -name    "LN1_DR_V2" -length 0.230983008858 
Drift      -name    "LN1_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN1_QD_V02"  -length 0.04  -strength [expr 0.04*$e0*-10.8241684484] -e0 $e0
Dipole     -name    "LN1_COR" -length 0 
Bpm        -name    "LN1_BPM" -length 0 
Quadrupole -name    "LN1_QD_V02"  -length 0.04  -strength [expr 0.04*$e0*-10.8241684484] -e0 $e0
Drift      -name    "LN1_DR_QD_ED" -length 0.0425 
Drift      -name    "LN1_DR_V3" -length 1.98847631967 
Drift      -name    "LN1_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN1_QD_V03"  -length 0.04  -strength [expr 0.04*$e0*1.02132067689] -e0 $e0
Dipole     -name    "LN1_COR" -length 0 
Bpm        -name    "LN1_BPM" -length 0 
Quadrupole -name    "LN1_QD_V03"  -length 0.04  -strength [expr 0.04*$e0*1.02132067689] -e0 $e0
Drift      -name    "LN1_DR_QD_ED" -length 0.0425 
Drift      -name    "LN1_DR_V4" -length 0.210637980905 
Drift      -name    "LN1_WA_M_OU" -length 0 
Drift      -name    "LN1_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN1_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*6.6] -e0 $e0
Dipole     -name    "LN1_COR" -length 0 
Bpm        -name    "LN1_BPM" -length 0 
Quadrupole -name    "LN1_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*6.6] -e0 $e0
Drift      -name    "LN1_DR_QD_ED" -length 0.0425 
Drift      -name    "LN1_DR_WP" -length 0.1 
Drift      -name    "LN1_DR_BL" -length 0.05 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_BL" -length 0.05 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_BL" -length 0.05 
Drift      -name    "LN1_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN1_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-6.6] -e0 $e0
Dipole     -name    "LN1_COR" -length 0 
Bpm        -name    "LN1_BPM" -length 0 
Quadrupole -name    "LN1_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-6.6] -e0 $e0
Drift      -name    "LN1_DR_QD_ED" -length 0.0425 
Drift      -name    "LN1_DR_VS" -length 0.1 
Drift      -name    "LN1_DR_BL" -length 0.05 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_BL" -length 0.05 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_BL" -length 0.05 
Drift      -name    "LN1_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN1_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*6.6] -e0 $e0
Dipole     -name    "LN1_COR" -length 0 
Bpm        -name    "LN1_BPM" -length 0 
Quadrupole -name    "LN1_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*6.6] -e0 $e0
Drift      -name    "LN1_DR_QD_ED" -length 0.0425 
Drift      -name    "LN1_DR_WP" -length 0.1 
Drift      -name    "LN1_DR_BL" -length 0.05 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_BL" -length 0.05 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_BL" -length 0.05 
Drift      -name    "LN1_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN1_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-6.6] -e0 $e0
Dipole     -name    "LN1_COR" -length 0 
Bpm        -name    "LN1_BPM" -length 0 
Quadrupole -name    "LN1_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-6.6] -e0 $e0
Drift      -name    "LN1_DR_QD_ED" -length 0.0425 
Drift      -name    "LN1_DR_VS" -length 0.1 
Drift      -name    "LN1_DR_BL" -length 0.05 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_BL" -length 0.05 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_BL" -length 0.05 
Drift      -name    "LN1_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN1_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*6.6] -e0 $e0
Dipole     -name    "LN1_COR" -length 0 
Bpm        -name    "LN1_BPM" -length 0 
Quadrupole -name    "LN1_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*6.6] -e0 $e0
Drift      -name    "LN1_DR_QD_ED" -length 0.0425 
Drift      -name    "LN1_DR_WP" -length 0.1 
Drift      -name    "LN1_DR_BL" -length 0.05 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_BL" -length 0.05 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_BL" -length 0.05 
Drift      -name    "LN1_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN1_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-6.6] -e0 $e0
Dipole     -name    "LN1_COR" -length 0 
Bpm        -name    "LN1_BPM" -length 0 
Quadrupole -name    "LN1_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-6.6] -e0 $e0
Drift      -name    "LN1_DR_QD_ED" -length 0.0425 
Drift      -name    "LN1_DR_VS" -length 0.1 
Drift      -name    "LN1_DR_BL" -length 0.05 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_BL" -length 0.05 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_BL" -length 0.05 
Drift      -name    "LN1_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN1_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*6.6] -e0 $e0
Dipole     -name    "LN1_COR" -length 0 
Bpm        -name    "LN1_BPM" -length 0 
Quadrupole -name    "LN1_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*6.6] -e0 $e0
Drift      -name    "LN1_DR_QD_ED" -length 0.0425 
Drift      -name    "LN1_DR_DR" -length 0.1 
Drift      -name    "LN1_DR_BL" -length 0.05 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_XCA_" -length 0.9164755 
Drift      -name    "LN1_WA_OU" -length 0 
Drift      -name    "BC2_DR_V1" -length 0.2 
Drift      -name    "BC2_DR_QD_ED" -length 0.0425 
Quadrupole -name    "BC2_QD_V01"  -length 0.04  -strength [expr 0.04*$e0*-0.58189942611] -e0 $e0
Dipole     -name    "BC2_COR" -length 0 
Bpm        -name    "BC2_BPM" -length 0 
Quadrupole -name    "BC2_QD_V01"  -length 0.04  -strength [expr 0.04*$e0*-0.58189942611] -e0 $e0
Drift      -name    "BC2_DR_QD_ED" -length 0.0425 
Drift      -name    "BC2_DR_V2" -length 4.75621168775 
Drift      -name    "BC2_DR_QD_ED" -length 0.0425 
Quadrupole -name    "BC2_QD_V02"  -length 0.04  -strength [expr 0.04*$e0*-11.186630386] -e0 $e0
Dipole     -name    "BC2_COR" -length 0 
Bpm        -name    "BC2_BPM" -length 0 
Quadrupole -name    "BC2_QD_V02"  -length 0.04  -strength [expr 0.04*$e0*-11.186630386] -e0 $e0
Drift      -name    "BC2_DR_QD_ED" -length 0.0425 
Drift      -name    "BC2_DR_V3" -length 0.2 
Drift      -name    "BC2_DR_QD_ED" -length 0.0425 
Quadrupole -name    "BC2_QD_V03"  -length 0.04  -strength [expr 0.04*$e0*11.5634459652] -e0 $e0
Dipole     -name    "BC2_COR" -length 0 
Bpm        -name    "BC2_BPM" -length 0 
Quadrupole -name    "BC2_QD_V03"  -length 0.04  -strength [expr 0.04*$e0*11.5634459652] -e0 $e0
Drift      -name    "BC2_DR_QD_ED" -length 0.0425 
Drift      -name    "BC2_DR_V4" -length 0.2 
Drift      -name    "BC2_DR_20" -length 0.5 
Sbend      -name    "BC2_DP_DIP1" -length 0.30002985  -angle -0.02443461  -E1 0 -E2 -0.02443461 -hgap 0 -fint 0.5 -tilt 0 -e0 $e0 -six_dim 1 
Drift      -name    "BC2_DR_SIDE" -length 3.0008958 
Sbend      -name    "BC2_DP_DIP2" -length 0.30002985  -angle 0.02443461  -E1 0.02443461 -E2 0 -hgap 0 -fint 0.5 -tilt 0 -e0 $e0 -six_dim 1 
Drift      -name    "BC2_DR_CENT" -length 0.5 
Sbend      -name    "BC2_DP_DIP3" -length 0.30002985  -angle 0.02443461  -E1 0 -E2 0.02443461 -hgap 0 -fint 0.5 -tilt 0 -e0 $e0 -six_dim 1 
Drift      -name    "BC2_DR_SIDE" -length 3.0008958 
Sbend      -name    "BC2_DP_DIP4" -length 0.30002985  -angle -0.02443461  -E1 -0.02443461 -E2 0 -hgap 0 -fint 0.5 -tilt 0 -e0 $e0 -six_dim 1 
Drift      -name    "BC2_DR_20" -length 0.5 
Drift      -name    "BC2_WA_OU" -length 0 
Drift      -name    "LN2_DR_V1" -length 0.200000070841 
Drift      -name    "LN2_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN2_QD_V01"  -length 0.04  -strength [expr 0.04*$e0*10.9917061443] -e0 $e0
Dipole     -name    "LN2_COR" -length 0 
Bpm        -name    "LN2_BPM" -length 0 
Quadrupole -name    "LN2_QD_V01"  -length 0.04  -strength [expr 0.04*$e0*10.9917061443] -e0 $e0
Drift      -name    "LN2_DR_QD_ED" -length 0.0425 
Drift      -name    "LN2_DR_V2" -length 0.200000042372 
Drift      -name    "LN2_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN2_QD_V02"  -length 0.04  -strength [expr 0.04*$e0*-7.48826941018] -e0 $e0
Dipole     -name    "LN2_COR" -length 0 
Bpm        -name    "LN2_BPM" -length 0 
Quadrupole -name    "LN2_QD_V02"  -length 0.04  -strength [expr 0.04*$e0*-7.48826941018] -e0 $e0
Drift      -name    "LN2_DR_QD_ED" -length 0.0425 
Drift      -name    "LN2_DR_V3" -length 0.598866268645 
Drift      -name    "LN2_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN2_QD_V03"  -length 0.04  -strength [expr 0.04*$e0*-4.48659406481] -e0 $e0
Dipole     -name    "LN2_COR" -length 0 
Bpm        -name    "LN2_BPM" -length 0 
Quadrupole -name    "LN2_QD_V03"  -length 0.04  -strength [expr 0.04*$e0*-4.48659406481] -e0 $e0
Drift      -name    "LN2_DR_QD_ED" -length 0.0425 
Drift      -name    "LN2_DR_V4" -length 2.99999999981 
Drift      -name    "LN2_WA_M_OU" -length 0 
Drift      -name    "LN2_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN2_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*6.7] -e0 $e0
Dipole     -name    "LN2_COR" -length 0 
Bpm        -name    "LN2_BPM" -length 0 
Quadrupole -name    "LN2_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*6.7] -e0 $e0
Drift      -name    "LN2_DR_QD_ED" -length 0.0425 
Drift      -name    "LN2_DR_WP" -length 0.1 
Drift      -name    "LN2_DR_BL" -length 0.05 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_BL" -length 0.05 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_BL" -length 0.05 
Drift      -name    "LN2_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN2_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-6.7] -e0 $e0
Dipole     -name    "LN2_COR" -length 0 
Bpm        -name    "LN2_BPM" -length 0 
Quadrupole -name    "LN2_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-6.7] -e0 $e0
Drift      -name    "LN2_DR_QD_ED" -length 0.0425 
Drift      -name    "LN2_DR_VS" -length 0.1 
Drift      -name    "LN2_DR_BL" -length 0.05 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_BL" -length 0.05 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_BL" -length 0.05 
Drift      -name    "LN2_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN2_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*6.7] -e0 $e0
Dipole     -name    "LN2_COR" -length 0 
Bpm        -name    "LN2_BPM" -length 0 
Quadrupole -name    "LN2_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*6.7] -e0 $e0
Drift      -name    "LN2_DR_QD_ED" -length 0.0425 
Drift      -name    "LN2_DR_WP" -length 0.1 
Drift      -name    "LN2_DR_BL" -length 0.05 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_BL" -length 0.05 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_BL" -length 0.05 
Drift      -name    "LN2_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN2_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-6.7] -e0 $e0
Dipole     -name    "LN2_COR" -length 0 
Bpm        -name    "LN2_BPM" -length 0 
Quadrupole -name    "LN2_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-6.7] -e0 $e0
Drift      -name    "LN2_DR_QD_ED" -length 0.0425 
Drift      -name    "LN2_DR_VS" -length 0.1 
Drift      -name    "LN2_DR_BL" -length 0.05 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_BL" -length 0.05 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_BL" -length 0.05 
Drift      -name    "LN2_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN2_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*6.7] -e0 $e0
Dipole     -name    "LN2_COR" -length 0 
Bpm        -name    "LN2_BPM" -length 0 
Quadrupole -name    "LN2_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*6.7] -e0 $e0
Drift      -name    "LN2_DR_QD_ED" -length 0.0425 
Drift      -name    "LN2_DR_DR" -length 0.1 
Drift      -name    "LN2_DR_BL" -length 0.05 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_XCA_" -length 0.9164755 
Drift      -name    "LN2_WA_OU" -length 0 
Drift      -name    "LN2_DR_BP_V1" -length 0.760784928325 
Drift      -name    "LN2_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN2_QD_BP_V01"  -length 0.04  -strength [expr 0.04*$e0*-0.529803108903] -e0 $e0
Dipole     -name    "LN2_COR" -length 0 
Bpm        -name    "LN2_BPM" -length 0 
Quadrupole -name    "LN2_QD_BP_V01"  -length 0.04  -strength [expr 0.04*$e0*-0.529803108903] -e0 $e0
Drift      -name    "LN2_DR_QD_ED" -length 0.0425 
Drift      -name    "LN2_DR_BP_V2" -length 2.99774644361 
Drift      -name    "LN2_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN2_QD_BP_V02"  -length 0.04  -strength [expr 0.04*$e0*-11.5467727281] -e0 $e0
Dipole     -name    "LN2_COR" -length 0 
Bpm        -name    "LN2_BPM" -length 0 
Quadrupole -name    "LN2_QD_BP_V02"  -length 0.04  -strength [expr 0.04*$e0*-11.5467727281] -e0 $e0
Drift      -name    "LN2_DR_QD_ED" -length 0.0425 
Drift      -name    "LN2_DR_BP_V3" -length 0.255073428798 
Drift      -name    "LN2_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN2_QD_BP_V03"  -length 0.04  -strength [expr 0.04*$e0*12.1171350324] -e0 $e0
Dipole     -name    "LN2_COR" -length 0 
Bpm        -name    "LN2_BPM" -length 0 
Quadrupole -name    "LN2_QD_BP_V03"  -length 0.04  -strength [expr 0.04*$e0*12.1171350324] -e0 $e0
Drift      -name    "LN2_DR_QD_ED" -length 0.0425 
Drift      -name    "LN2_DR_BP_V4" -length 0.286997194396 
Drift      -name    "LN2_DR_BP_0" -length 0.2 
Drift      -name    "LN2_DR_BP_BL" -length 0.05 
Drift      -name    "LN2_DR_SCA_ED" -length 0.08325105 
CrabCavity -name    "LN2_SCA0" -length 0.5334979  -voltage $LN2_SCA_VLT -phase $LN2_SCA_PHS -frequency 2.997
Drift      -name    "LN2_DR_SCA_ED" -length 0.08325105 
Drift      -name    "LN2_DR_BP_BL" -length 0.05 
Drift      -name    "LN2_DR_BP_0" -length 0.2 
Drift      -name    "LN2_DR_BP" -length 0.4 
Drift      -name    "LN2_DR_BP" -length 0.4 
Drift      -name    "LN2_DR_BP_VS" -length 0.1 
Drift      -name    "LN2_DP_SEPM" -length 0.5 
Drift      -name    "LN2_DR_BP" -length 0.4 
Drift      -name    "LN2_DR_BP" -length 0.4 
Drift      -name    "LN2_DR_BP" -length 0.4 
Drift      -name    "LN2_DR_BP" -length 0.4 
Drift      -name    "LN2_DR_BP" -length 0.4 
Drift      -name    "LN2_DR_BP_VS" -length 0.1 
Drift      -name    "LN2_DR_BP" -length 0.4 
Drift      -name    "LN2_BP_WA_OU" -length 0 
Drift      -name    "LN3_DR_V1" -length 2.29586925695 
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN3_QD_V01"  -length 0.04  -strength [expr 0.04*$e0*7.57641062673] -e0 $e0
Dipole     -name    "LN3_COR" -length 0 
Bpm        -name    "LN3_BPM" -length 0 
Quadrupole -name    "LN3_QD_V01"  -length 0.04  -strength [expr 0.04*$e0*7.57641062673] -e0 $e0
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Drift      -name    "LN3_DR_V2" -length 0.317824586709 
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN3_QD_V02"  -length 0.04  -strength [expr 0.04*$e0*-7.97656880144] -e0 $e0
Dipole     -name    "LN3_COR" -length 0 
Bpm        -name    "LN3_BPM" -length 0 
Quadrupole -name    "LN3_QD_V02"  -length 0.04  -strength [expr 0.04*$e0*-7.97656880144] -e0 $e0
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Drift      -name    "LN3_DR_V3" -length 0.687018787542 
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN3_QD_V03"  -length 0.04  -strength [expr 0.04*$e0*-1.72551673483] -e0 $e0
Dipole     -name    "LN3_COR" -length 0 
Bpm        -name    "LN3_BPM" -length 0 
Quadrupole -name    "LN3_QD_V03"  -length 0.04  -strength [expr 0.04*$e0*-1.72551673483] -e0 $e0
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Drift      -name    "LN3_DR_V4" -length 2.99901471456 
Drift      -name    "LN3_WA_M_OU" -length 0 
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN3_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*4.2] -e0 $e0
Dipole     -name    "LN3_COR" -length 0 
Bpm        -name    "LN3_BPM" -length 0 
Quadrupole -name    "LN3_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*4.2] -e0 $e0
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Drift      -name    "LN3_DR_WP" -length 0.1 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN3_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-4.2] -e0 $e0
Dipole     -name    "LN3_COR" -length 0 
Bpm        -name    "LN3_BPM" -length 0 
Quadrupole -name    "LN3_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-4.2] -e0 $e0
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Drift      -name    "LN3_DR_VS" -length 0.1 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN3_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*4.2] -e0 $e0
Dipole     -name    "LN3_COR" -length 0 
Bpm        -name    "LN3_BPM" -length 0 
Quadrupole -name    "LN3_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*4.2] -e0 $e0
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Drift      -name    "LN3_DR_WP" -length 0.1 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN3_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-4.2] -e0 $e0
Dipole     -name    "LN3_COR" -length 0 
Bpm        -name    "LN3_BPM" -length 0 
Quadrupole -name    "LN3_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-4.2] -e0 $e0
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Drift      -name    "LN3_DR_VS" -length 0.1 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN3_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*4.2] -e0 $e0
Dipole     -name    "LN3_COR" -length 0 
Bpm        -name    "LN3_BPM" -length 0 
Quadrupole -name    "LN3_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*4.2] -e0 $e0
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Drift      -name    "LN3_DR_WP" -length 0.1 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN3_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-4.2] -e0 $e0
Dipole     -name    "LN3_COR" -length 0 
Bpm        -name    "LN3_BPM" -length 0 
Quadrupole -name    "LN3_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-4.2] -e0 $e0
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Drift      -name    "LN3_DR_VS" -length 0.1 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN3_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*4.2] -e0 $e0
Dipole     -name    "LN3_COR" -length 0 
Bpm        -name    "LN3_BPM" -length 0 
Quadrupole -name    "LN3_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*4.2] -e0 $e0
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Drift      -name    "LN3_DR_WP" -length 0.1 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN3_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-4.2] -e0 $e0
Dipole     -name    "LN3_COR" -length 0 
Bpm        -name    "LN3_BPM" -length 0 
Quadrupole -name    "LN3_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-4.2] -e0 $e0
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Drift      -name    "LN3_DR_VS" -length 0.1 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN3_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*4.2] -e0 $e0
Dipole     -name    "LN3_COR" -length 0 
Bpm        -name    "LN3_BPM" -length 0 
Quadrupole -name    "LN3_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*4.2] -e0 $e0
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Drift      -name    "LN3_DR_WP" -length 0.1 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN3_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-4.2] -e0 $e0
Dipole     -name    "LN3_COR" -length 0 
Bpm        -name    "LN3_BPM" -length 0 
Quadrupole -name    "LN3_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-4.2] -e0 $e0
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Drift      -name    "LN3_DR_VS" -length 0.1 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN3_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*4.2] -e0 $e0
Dipole     -name    "LN3_COR" -length 0 
Bpm        -name    "LN3_BPM" -length 0 
Quadrupole -name    "LN3_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*4.2] -e0 $e0
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Drift      -name    "LN3_DR_WP" -length 0.1 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN3_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-4.2] -e0 $e0
Dipole     -name    "LN3_COR" -length 0 
Bpm        -name    "LN3_BPM" -length 0 
Quadrupole -name    "LN3_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-4.2] -e0 $e0
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Drift      -name    "LN3_DR_VS" -length 0.1 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN3_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*4.2] -e0 $e0
Dipole     -name    "LN3_COR" -length 0 
Bpm        -name    "LN3_BPM" -length 0 
Quadrupole -name    "LN3_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*4.2] -e0 $e0
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Drift      -name    "LN3_DR_WP" -length 0.1 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN3_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-4.2] -e0 $e0
Dipole     -name    "LN3_COR" -length 0 
Bpm        -name    "LN3_BPM" -length 0 
Quadrupole -name    "LN3_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-4.2] -e0 $e0
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Drift      -name    "LN3_DR_VS" -length 0.1 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN3_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*4.2] -e0 $e0
Dipole     -name    "LN3_COR" -length 0 
Bpm        -name    "LN3_BPM" -length 0 
Quadrupole -name    "LN3_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*4.2] -e0 $e0
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Drift      -name    "LN3_DR_WP" -length 0.1 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN3_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-4.2] -e0 $e0
Dipole     -name    "LN3_COR" -length 0 
Bpm        -name    "LN3_BPM" -length 0 
Quadrupole -name    "LN3_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-4.2] -e0 $e0
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Drift      -name    "LN3_DR_VS" -length 0.1 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN3_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*4.2] -e0 $e0
Dipole     -name    "LN3_COR" -length 0 
Bpm        -name    "LN3_BPM" -length 0 
Quadrupole -name    "LN3_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*4.2] -e0 $e0
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Drift      -name    "LN3_DR_WP" -length 0.1 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN3_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-4.2] -e0 $e0
Dipole     -name    "LN3_COR" -length 0 
Bpm        -name    "LN3_BPM" -length 0 
Quadrupole -name    "LN3_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-4.2] -e0 $e0
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Drift      -name    "LN3_DR_VS" -length 0.1 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN3_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*4.2] -e0 $e0
Dipole     -name    "LN3_COR" -length 0 
Bpm        -name    "LN3_BPM" -length 0 
Quadrupole -name    "LN3_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*4.2] -e0 $e0
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Drift      -name    "LN3_DR_DR" -length 0.1 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_XCA_" -length 0.9164755 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_XCA_" -length 0.9164755 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_WA_OU" -length 0 
Drift      -name    "LN3_DR_BP_V1" -length 0.847210078795 
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN3_QD_BP_V01"  -length 0.04  -strength [expr 0.04*$e0*-0.992107722422] -e0 $e0
Dipole     -name    "LN3_COR" -length 0 
Bpm        -name    "LN3_BPM" -length 0 
Quadrupole -name    "LN3_QD_BP_V01"  -length 0.04  -strength [expr 0.04*$e0*-0.992107722422] -e0 $e0
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Drift      -name    "LN3_DR_BP_V2" -length 2.47130196978 
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN3_QD_BP_V02"  -length 0.04  -strength [expr 0.04*$e0*-7.83956144838] -e0 $e0
Dipole     -name    "LN3_COR" -length 0 
Bpm        -name    "LN3_BPM" -length 0 
Quadrupole -name    "LN3_QD_BP_V02"  -length 0.04  -strength [expr 0.04*$e0*-7.83956144838] -e0 $e0
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Drift      -name    "LN3_DR_BP_V3" -length 0.599604414246 
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN3_QD_BP_V03"  -length 0.04  -strength [expr 0.04*$e0*8.34886727201] -e0 $e0
Dipole     -name    "LN3_COR" -length 0 
Bpm        -name    "LN3_BPM" -length 0 
Quadrupole -name    "LN3_QD_BP_V03"  -length 0.04  -strength [expr 0.04*$e0*8.34886727201] -e0 $e0
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Drift      -name    "LN3_DR_BP_V4" -length 0.200000002441 
Drift      -name    "LN3_DR_BP_0" -length 0.2 
Drift      -name    "LN3_DR_BP_BL" -length 0.05 
Drift      -name    "LN3_DR_SCA_ED" -length 0.08325105 
CrabCavity -name    "LN3_SCA0" -length 0.5334979  -voltage $LN3_SCA_VLT -phase $LN3_SCA_PHS -frequency 2.997
Drift      -name    "LN3_DR_SCA_ED" -length 0.08325105 
Drift      -name    "LN3_DR_BP_BL" -length 0.05 
Drift      -name    "LN3_DR_BP_0" -length 0.2 
Drift      -name    "LN3_DR_BP" -length 0.4 
Drift      -name    "LN3_DR_BP" -length 0.4 
Drift      -name    "LN3_DR_BP_VS" -length 0.1 
Drift      -name    "LN3_DP_SEPM" -length 0.5 
Drift      -name    "LN3_DR_BP" -length 0.4 
Drift      -name    "LN3_DR_BP" -length 0.4 
Drift      -name    "LN3_DR_BP" -length 0.4 
Drift      -name    "LN3_DR_BP" -length 0.4 
Drift      -name    "LN3_DR_BP" -length 0.4 
Drift      -name    "LN3_DR_BP_VS" -length 0.1 
Drift      -name    "LN3_DR_BP" -length 0.4 
Drift      -name    "LN3_BP_WA_OU" -length 0 
Drift      -name    "TMC_DR_V1" -length 3.57838328995 
Drift      -name    "TMC_DR_QD_ED" -length 0.0425 
Quadrupole -name    "TMC_QD_V01"  -length 0.04  -strength [expr 0.04*$e0*3.22100658971] -e0 $e0
Dipole     -name    "TMC_COR" -length 0 
Bpm        -name    "TMC_BPM" -length 0 
Quadrupole -name    "TMC_QD_V01"  -length 0.04  -strength [expr 0.04*$e0*3.22100658971] -e0 $e0
Drift      -name    "TMC_DR_QD_ED" -length 0.0425 
Drift      -name    "TMC_DR_V2" -length 0.932504124506 
Drift      -name    "TMC_DR_QD_ED" -length 0.0425 
Quadrupole -name    "TMC_QD_V02"  -length 0.04  -strength [expr 0.04*$e0*-12] -e0 $e0
Dipole     -name    "TMC_COR" -length 0 
Bpm        -name    "TMC_BPM" -length 0 
Quadrupole -name    "TMC_QD_V02"  -length 0.04  -strength [expr 0.04*$e0*-12] -e0 $e0
Drift      -name    "TMC_DR_QD_ED" -length 0.0425 
Drift      -name    "TMC_DR_V3" -length 0.2 
Drift      -name    "TMC_DR_QD_ED" -length 0.0425 
Quadrupole -name    "TMC_QD_V03"  -length 0.04  -strength [expr 0.04*$e0*9.55003445987] -e0 $e0
Dipole     -name    "TMC_COR" -length 0 
Bpm        -name    "TMC_BPM" -length 0 
Quadrupole -name    "TMC_QD_V03"  -length 0.04  -strength [expr 0.04*$e0*9.55003445987] -e0 $e0
Drift      -name    "TMC_DR_QD_ED" -length 0.0425 
Drift      -name    "TMC_DR_V4" -length 0.2 
Drift      -name    "TMC_DR_20" -length 0.5 
Sbend      -name    "TMC_DP_DIP1" -length 0.30000381  -angle -0.00872665  -E1 0 -E2 -0.00872665 -hgap 0 -fint 0.5 -tilt 0 -e0 $e0 -six_dim 1 
Drift      -name    "TMC_DR_SIDE" -length 2.5000952 
Sbend      -name    "TMC_DP_DIP2" -length 0.30000381  -angle 0.00872665  -E1 0.00872665 -E2 0 -hgap 0 -fint 0.5 -tilt 0 -e0 $e0 -six_dim 1 
Drift      -name    "TMC_DR_CENT" -length 0.5 
Sbend      -name    "TMC_DP_DIP3" -length 0.30000381  -angle 0.00872665  -E1 0 -E2 0.00872665 -hgap 0 -fint 0.5 -tilt 0 -e0 $e0 -six_dim 1 
Drift      -name    "TMC_DR_SIDE" -length 2.5000952 
Sbend      -name    "TMC_DP_DIP4" -length 0.30000381  -angle -0.00872665  -E1 -0.00872665 -E2 0 -hgap 0 -fint 0.5 -tilt 0 -e0 $e0 -six_dim 1 
Drift      -name    "TMC_DR_20" -length 0.5 
Drift      -name    "TMC_WA_OU" -length 0 
Drift      -name    "HXR_DR_V1" -length 0.911285087526 
Drift      -name    "HXR_DR_QD_ED" -length 0.0425 
Quadrupole -name    "HXR_QD_V01"  -length 0.04  -strength [expr 0.04*$e0*3.07479100319] -e0 $e0
Dipole     -name    "HXR_COR" -length 0 
Bpm        -name    "HXR_BPM" -length 0 
Quadrupole -name    "HXR_QD_V01"  -length 0.04  -strength [expr 0.04*$e0*3.07479100319] -e0 $e0
Drift      -name    "HXR_DR_QD_ED" -length 0.0425 
Drift      -name    "HXR_DR_V2" -length 0.470491483096 
Drift      -name    "HXR_DR_QD_ED" -length 0.0425 
Quadrupole -name    "HXR_QD_V02"  -length 0.04  -strength [expr 0.04*$e0*-9.96329880075] -e0 $e0
Dipole     -name    "HXR_COR" -length 0 
Bpm        -name    "HXR_BPM" -length 0 
Quadrupole -name    "HXR_QD_V02"  -length 0.04  -strength [expr 0.04*$e0*-9.96329880075] -e0 $e0
Drift      -name    "HXR_DR_QD_ED" -length 0.0425 
Drift      -name    "HXR_DR_V3" -length 0.200000637541 
Drift      -name    "HXR_DR_QD_ED" -length 0.0425 
Quadrupole -name    "HXR_QD_V03"  -length 0.04  -strength [expr 0.04*$e0*3.62732423925] -e0 $e0
Dipole     -name    "HXR_COR" -length 0 
Bpm        -name    "HXR_BPM" -length 0 
Quadrupole -name    "HXR_QD_V03"  -length 0.04  -strength [expr 0.04*$e0*3.62732423925] -e0 $e0
Drift      -name    "HXR_DR_QD_ED" -length 0.0425 
Drift      -name    "HXR_DR_V4" -length 1.58916552756 
Drift      -name    "HXR_WA_M_OU" -length 0 
Drift      -name    "HXR_DR_QD_ED" -length 0.0425 
Quadrupole -name    "HXR_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*5] -e0 $e0
Dipole     -name    "HXR_COR" -length 0 
Bpm        -name    "HXR_BPM" -length 0 
Quadrupole -name    "HXR_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*5] -e0 $e0
Drift      -name    "HXR_DR_QD_ED" -length 0.0425 
Drift      -name    "HXR_DR_WP" -length 0.1 
Drift      -name    "HXR_DR_BL" -length 0.05 
Drift      -name    "HXR_DR_WIG_ED" -length 0.2 
Drift      -name    "HXR_DR_WIG" -length 3 
Drift      -name    "HXR_DR_WIG_ED" -length 0.2 
Drift      -name    "HXR_DR_BL" -length 0.05 
Drift      -name    "HXR_DR_QD_ED" -length 0.0425 
Quadrupole -name    "HXR_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-5] -e0 $e0
Dipole     -name    "HXR_COR" -length 0 
Bpm        -name    "HXR_BPM" -length 0 
Quadrupole -name    "HXR_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-5] -e0 $e0
Drift      -name    "HXR_DR_QD_ED" -length 0.0425 
Drift      -name    "HXR_DR_VS" -length 0.1 
Drift      -name    "HXR_DR_BL" -length 0.05 
Drift      -name    "HXR_DR_WIG_ED" -length 0.2 
Drift      -name    "HXR_DR_WIG" -length 3 
Drift      -name    "HXR_DR_WIG_ED" -length 0.2 
Drift      -name    "HXR_DR_BL" -length 0.05 
Drift      -name    "HXR_WA_OU" -length 0 
