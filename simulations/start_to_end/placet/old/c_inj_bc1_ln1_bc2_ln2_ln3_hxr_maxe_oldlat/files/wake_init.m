1;
% file contains related short range wakefield ...
% 


function p = pow(x,a)
    p = x**a;
end

function Wt = W_transv(s,structure) % V/pC/m/m
    Z0=120*pi;
    clight=2.9979246e8;
    a=structure.aper;
    g=structure.gap;
    l=structure.cell_l;
    s0 = 0.169 * pow(a,1.79) * pow(g,0.38) / pow(l,1.17);
    sqrt_s_s0 = sqrt(s*1e-6/s0);
    Wt = 4 * Z0 * s0 * clight / pi / pow(a,4) * (1 - (1 + sqrt_s_s0) .* exp(-sqrt_s_s0)) / 1e12;
end

function Wl = W_long(s,structure) % V/pC/m
    Z0=120*pi;
    clight=2.9979246e8;
    a=structure.aper;
    g=structure.gap;
    l=structure.cell_l;
    s0 = 0.41 * pow(a,1.8) * pow(g,1.6) / pow(l,2.4);
    Wl = Z0 * clight / pi / (a*a) * exp(-sqrt(s*1e-6/s0)) / 1e12;
endfunction



function vlong = V_long (charge,z_list,structure)
%  # unit to store in the file is MV/pCm
    nslice=length(z_list);
    e_pC = 1.6021765e-07;
    zz=z_list(:,1);  % it is in microns should be converteed to meter for functions above
    zzm=zz;
    wgt=z_list(:,2);
    
    Wz = W_long(zzm-zzm(1), structure);
    
    Wz(1) = Wz(1) / 2; % fundamental theorem of beam loading
    wgt = fliplr(wgt); % flips to have the head toward z<0
    %% convolution through FFT
    nn = nslice + nslice - 1;
    Wz(nn) = 0; % padding
    wgt(nn) = 0; % padding
    dE = real( ifft( fft(Wz) .* fft(wgt) ));
    %% convolution
    %dE = conv(Wz, NN);
    %% flips lr the first nslices of the result in order to have the head toward z>0 again
    dE = fliplr(dE(1:nslice));
    dE =dE.*(-1e-6*e_pC*charge);
    vlong=[zz,dE];
end


function vzero = get_vlong_zero (z_list)
%  # unit to store in the file is MV/pCm
    nslice=length(z_list);
    zz =z_list(:,1); 
    wgt=z_list(:,2);
    [zl indx]=sort(abs(zz)); 
    vzero=wgt(indx(1));   
end


function vtrans = V_trans (charge,z_list,structure)
%  # unit to store in the file is MV/pCm^2
    nslice=length(z_list);
    e_pC = 1.6021765e-07;
    zz=z_list(:,1);  % it is in microns should be converteed to meter for functions above
    zzm=zz;
    zzm_tmp=zeros(0,1);
    tmp=zeros(0,1);
%      zu=zeros(0,1);
    for i=1:nslice
	zzm_tmp=[zzm_tmp, zzm(i)];
	z0=zzm(i);
	nj=length(zzm_tmp);
	for j=1:nj
	    z=zzm_tmp(j);
	    dz=z0-z;
%    # multiply charge with factor for pC and MV
	    kick=e_pC*1e-6*charge*W_transv(dz,structure);
	    tmp=[tmp;kick];
%  	    zu=[zu;z0];
	endfor
    endfor
%      all=[zu tmp];

  vtrans=tmp;
endfunction

function calc_beamdat(outfile,charge,z_list,structure)
    format long ;
    nslice=length(z_list);
    wgt=z_list(:,2);
    long =  V_long(charge,z_list,structure);
    transv= V_trans(charge,z_list,structure);
    vzero=get_vlong_zero (long);
    long2=[long wgt];
    
    fid=fopen(outfile,"wt");
    fprintf(fid,"%d\n",nslice);
    fprintf(fid,"%g\n",vzero);
    fprintf(fid,"%g %g %g\n",long2');
    fprintf(fid,"%g %g\n",transv);
    fclose(fid);
   
endfunction


   
   
## short wake files

nnslice = 4096    ; #  
sigma  = nnslice/2 ; # [um]

ZZ  = linspace(0, sigma, nnslice);
zrzs= zeros(1,nnslice);
Zm  = ZZ.*1e-6;


%  xband wake
Wtx = W_transv(ZZ,xband_str);
Wlx = W_long(ZZ,xband_str);

filetrx = fopen(xband_str.wake_tran_file, 'w');
filelnx = fopen(xband_str.wake_long_file, 'w');

fprintf(filetrx, ' %.15g %.15g\n', [ Zm ; Wtx ] );
fprintf(filelnx, ' %.15g %.15g\n', [ Zm ; Wlx ] );

fclose(filetrx);
fclose(filelnx);

%  kband wake
Wtk = W_transv(ZZ,kband_str);
Wlk = W_long(ZZ,kband_str);

filetrk = fopen(kband_str.wake_tran_file, 'w');
filelnk = fopen(kband_str.wake_long_file, 'w');

fprintf(filetrk, ' %.15g %.15g\n', [ Zm ; Wtk ] );
fprintf(filelnk, ' %.15g %.15g\n', [ Zm ; Wlk ] );

fclose(filetrk);
fclose(filelnk);

%  cband wake
Wtk = W_transv(ZZ,cband_str);
Wlk = W_long(ZZ,cband_str);

filetrk = fopen(cband_str.wake_tran_file, 'w');
filelnk = fopen(cband_str.wake_long_file, 'w');

fprintf(filetrk, ' %.15g %.15g\n', [ Zm ; Wtk ] );
fprintf(filelnk, ' %.15g %.15g\n', [ Zm ; Wlk ] );

fclose(filetrk);
fclose(filelnk);

%  sband def wake
Wtk = W_transv(ZZ,sdband_str);
Wlk = W_long(ZZ,sdband_str);

filetrk = fopen(sdband_str.wake_tran_file, 'w');
filelnk = fopen(sdband_str.wake_long_file, 'w');

fprintf(filetrk, ' %.15g %.15g\n', [ Zm ; Wtk ] );
fprintf(filelnk, ' %.15g %.15g\n', [ Zm ; Wlk ] );

fclose(filetrk);
fclose(filelnk);



%  xband def wake
Wtk = W_transv(ZZ,xdband_str);
Wlk = W_long(ZZ,xdband_str);

filetrk = fopen(xdband_str.wake_tran_file, 'w');
filelnk = fopen(xdband_str.wake_long_file, 'w');

fprintf(filetrk, ' %.15g %.15g\n', [ Zm ; Wtk ] );
fprintf(filelnk, ' %.15g %.15g\n', [ Zm ; Wlk ] );

fclose(filetrk);
fclose(filelnk);


%  zero wake
filetr_z = fopen('acc_cav_Wt_zero.dat', 'w');
fileln_z = fopen('acc_cav_Wl_zero.dat', 'w');


fprintf(filetr_z, ' %.15g %.15g\n', [ Zm ; zrzs ] );
fprintf(fileln_z, ' %.15g %.15g\n', [ Zm ; zrzs ] );


fclose(filetr_z);
fclose(fileln_z);


