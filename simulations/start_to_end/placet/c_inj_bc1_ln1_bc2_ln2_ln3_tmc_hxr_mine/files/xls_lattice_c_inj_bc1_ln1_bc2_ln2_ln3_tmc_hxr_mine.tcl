Girder
SetReferenceEnergy  $e0
Drift      -name    "LN0_DR_V1" -length 0.200000000113 
Drift      -name    "LN0_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN0_QD_V01"  -length 0.04  -strength [expr 0.04*$e0*-8.29703379056] -e0 $e0
Dipole     -name    "LN0_COR" -length 0 
Bpm        -name    "LN0_BPM" -length 0 
Quadrupole -name    "LN0_QD_V01"  -length 0.04  -strength [expr 0.04*$e0*-8.29703379056] -e0 $e0
Drift      -name    "LN0_DR_QD_ED" -length 0.0425 
Drift      -name    "LN0_DR_V2" -length 0.357344724475 
Drift      -name    "LN0_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN0_QD_V02"  -length 0.04  -strength [expr 0.04*$e0*10.3560458224] -e0 $e0
Dipole     -name    "LN0_COR" -length 0 
Bpm        -name    "LN0_BPM" -length 0 
Quadrupole -name    "LN0_QD_V02"  -length 0.04  -strength [expr 0.04*$e0*10.3560458224] -e0 $e0
Drift      -name    "LN0_DR_QD_ED" -length 0.0425 
Drift      -name    "LN0_DR_V3" -length 0.997310976817 
Drift      -name    "LN0_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN0_QD_V03"  -length 0.04  -strength [expr 0.04*$e0*-0.823202280099] -e0 $e0
Dipole     -name    "LN0_COR" -length 0 
Bpm        -name    "LN0_BPM" -length 0 
Quadrupole -name    "LN0_QD_V03"  -length 0.04  -strength [expr 0.04*$e0*-0.823202280099] -e0 $e0
Drift      -name    "LN0_DR_QD_ED" -length 0.0425 
Drift      -name    "LN0_DR_V4" -length 1.4219526072 
Drift      -name    "LN0_DR_20" -length 0.2 
Sbend      -name    "LN0_DP_DIP1" -length 0.1  -angle -0.08726646  -E1 0 -E2 -0.08726646 -hgap 0 -fint 0.5 -tilt 1.57079632679 -e0 $e0 -six_dim 1 
Drift      -name    "LN0_DR_20_C" -length 0.2 
Sbend      -name    "LN0_DP_DIP2" -length 0.1  -angle 0.08726646  -E1 0.08726646 -E2 0 -hgap 0 -fint 0.5 -tilt 1.57079632679 -e0 $e0 -six_dim 1 
Drift      -name    "LN0_DR_20_C" -length 0.2 
Bpm        -name    "LN0_BPM_LH" -length 0 
Drift      -name    "LN0_DR_20" -length 0.2 
Drift      -name    "LN0_UN_UND" -length 0.24 
Drift      -name    "LN0_DR_20" -length 0.2 
Bpm        -name    "LN0_BPM_LH" -length 0 
Drift      -name    "LN0_DR_VS" -length 0.1 
Drift      -name    "LN0_DR_10" -length 0.1 
Sbend      -name    "LN0_DP_DIP3" -length 0.1  -angle 0.08726646  -E1 0 -E2 0.08726646 -hgap 0 -fint 0.5 -tilt 1.57079632679 -e0 $e0 -six_dim 1 
Drift      -name    "LN0_DR_20_C" -length 0.2 
Sbend      -name    "LN0_DP_DIP4" -length 0.1  -angle -0.08726646  -E1 -0.08726646 -E2 0 -hgap 0 -fint 0.5 -tilt 1.57079632679 -e0 $e0 -six_dim 1 
Drift      -name    "LN0_DR_20_C" -length 0.2 
Drift      -name    "LN0_DR_VS" -length 0.1 
Drift      -name    "LN0_DR_V5" -length 0.66354339613 
Drift      -name    "LN0_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN0_QD_V04"  -length 0.04  -strength [expr 0.04*$e0*-7.00753275047] -e0 $e0
Dipole     -name    "LN0_COR" -length 0 
Bpm        -name    "LN0_BPM" -length 0 
Quadrupole -name    "LN0_QD_V04"  -length 0.04  -strength [expr 0.04*$e0*-7.00753275047] -e0 $e0
Drift      -name    "LN0_DR_QD_ED" -length 0.0425 
Drift      -name    "LN0_DR_V6" -length 0.259874163938 
Drift      -name    "LN0_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN0_QD_V05"  -length 0.04  -strength [expr 0.04*$e0*14.1297638401] -e0 $e0
Dipole     -name    "LN0_COR" -length 0 
Bpm        -name    "LN0_BPM" -length 0 
Quadrupole -name    "LN0_QD_V05"  -length 0.04  -strength [expr 0.04*$e0*14.1297638401] -e0 $e0
Drift      -name    "LN0_DR_QD_ED" -length 0.0425 
Drift      -name    "LN0_DR_V7" -length 1.41975407698 
Drift      -name    "LN0_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN0_QD_V06"  -length 0.04  -strength [expr 0.04*$e0*-6.07664540011] -e0 $e0
Dipole     -name    "LN0_COR" -length 0 
Bpm        -name    "LN0_BPM" -length 0 
Quadrupole -name    "LN0_QD_V06"  -length 0.04  -strength [expr 0.04*$e0*-6.07664540011] -e0 $e0
Drift      -name    "LN0_DR_QD_ED" -length 0.0425 
Drift      -name    "LN0_DR_V8" -length 0.200002236253 
Drift      -name    "LN0_DR_VS" -length 0.1 
Drift      -name    "LN0_DR_BL" -length 0.05 
Drift      -name    "LN0_DR_XCA_ED_DI" -length 0.1001043 
CrabCavity -name    "LN0_XCA0_DI" -length 0.9997914  -voltage $LN0_XCA_VLT -phase $LN0_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + $LN0_XCA_VLT*sin(3.14159265359*$LN0_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN0_DR_XCA_ED_DI" -length 0.1001043 
Drift      -name    "LN0_DR_BL" -length 0.05 
Drift      -name    "LN0_TDS_DR" -length 0.25 
Drift      -name    "LN0_TDS_DR" -length 0.25 
Drift      -name    "LN0_TDS_DR" -length 0.25 
Drift      -name    "LN0_TDS_DR" -length 0.25 
Drift      -name    "LN0_TDS_DR" -length 0.25 
Drift      -name    "LN0_TDS_DR" -length 0.25 
Drift      -name    "LN0_TDS_DR" -length 0.25 
Drift      -name    "LN0_TDS_DR" -length 0.25 
Sbend      -name    "LN0_DP_TDS" -length 0.1  -angle 0  -E1 0 -E2 0 -hgap 0 -fint 0.5 -tilt 0 -e0 $e0 -six_dim 1 
Drift      -name    "LN0_DR_20" -length 0.2 
Drift      -name    "LN0_DR_VS" -length 0.1 
Drift      -name    "LN0_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN0_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*7] -e0 $e0
Dipole     -name    "LN0_COR" -length 0 
Bpm        -name    "LN0_BPM" -length 0 
Quadrupole -name    "LN0_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*7] -e0 $e0
Drift      -name    "LN0_DR_QD_ED" -length 0.0425 
Drift      -name    "LN0_DR_SV" -length 0.1 
Drift      -name    "LN0_DR_BL" -length 0.05 
Drift      -name    "LN0_DR_CCA_ED" -length 0.050198 
Cavity     -name    "LN0_CCA0" -length 1.899604  -gradient $LN0_CCA_GRD -phase $LN0_CCA_PHS -frequency 5.9971
set e0      [expr $e0 + 1.899604*$LN0_CCA_GRD*cos(3.14159265359*$LN0_CCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN0_DR_CCA_ED" -length 0.050198 
Drift      -name    "LN0_DR_BL" -length 0.05 
Drift      -name    "LN0_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN0_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-7] -e0 $e0
Dipole     -name    "LN0_COR" -length 0 
Bpm        -name    "LN0_BPM" -length 0 
Quadrupole -name    "LN0_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-7] -e0 $e0
Drift      -name    "LN0_DR_QD_ED" -length 0.0425 
Drift      -name    "LN0_DR_VS" -length 0.1 
Drift      -name    "LN0_DR_BL" -length 0.05 
Drift      -name    "LN0_DR_CCA_ED" -length 0.050198 
Cavity     -name    "LN0_CCA0" -length 1.899604  -gradient $LN0_CCA_GRD -phase $LN0_CCA_PHS -frequency 5.9971
set e0      [expr $e0 + 1.899604*$LN0_CCA_GRD*cos(3.14159265359*$LN0_CCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN0_DR_CCA_ED" -length 0.050198 
Drift      -name    "LN0_DR_BL" -length 0.05 
Drift      -name    "LN0_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN0_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*7] -e0 $e0
Dipole     -name    "LN0_COR" -length 0 
Bpm        -name    "LN0_BPM" -length 0 
Quadrupole -name    "LN0_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*7] -e0 $e0
Drift      -name    "LN0_DR_QD_ED" -length 0.0425 
Drift      -name    "LN0_DR_VS" -length 0.1 
Drift      -name    "LN0_DR_BL" -length 0.05 
Drift      -name    "LN0_DR_CCA_ED" -length 0.050198 
Cavity     -name    "LN0_CCA0" -length 1.899604  -gradient $LN0_CCA_GRD -phase $LN0_CCA_PHS -frequency 5.9971
set e0      [expr $e0 + 1.899604*$LN0_CCA_GRD*cos(3.14159265359*$LN0_CCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN0_DR_CCA_ED" -length 0.050198 
Drift      -name    "LN0_DR_BL" -length 0.05 
Drift      -name    "LN0_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN0_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-7] -e0 $e0
Dipole     -name    "LN0_COR" -length 0 
Bpm        -name    "LN0_BPM" -length 0 
Quadrupole -name    "LN0_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-7] -e0 $e0
Drift      -name    "LN0_DR_QD_ED" -length 0.0425 
Drift      -name    "LN0_DR_VS" -length 0.1 
Drift      -name    "LN0_DR_BL" -length 0.05 
Drift      -name    "LN0_DR_CCA_ED" -length 0.050198 
Cavity     -name    "LN0_CCA0" -length 1.899604  -gradient $LN0_CCA_GRD -phase $LN0_CCA_PHS -frequency 5.9971
set e0      [expr $e0 + 1.899604*$LN0_CCA_GRD*cos(3.14159265359*$LN0_CCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN0_DR_CCA_ED" -length 0.050198 
Drift      -name    "LN0_DR_BL" -length 0.05 
Drift      -name    "LN0_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN0_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*7] -e0 $e0
Dipole     -name    "LN0_COR" -length 0 
Bpm        -name    "LN0_BPM" -length 0 
Quadrupole -name    "LN0_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*7] -e0 $e0
Drift      -name    "LN0_DR_QD_ED" -length 0.0425 
Drift      -name    "LN0_DR_VS" -length 0.1 
Drift      -name    "LN0_DR_BL" -length 0.05 
Drift      -name    "LN0_DR_CCA_ED" -length 0.050198 
Cavity     -name    "LN0_CCA0" -length 1.899604  -gradient $LN0_CCA_GRD -phase $LN0_CCA_PHS -frequency 5.9971
set e0      [expr $e0 + 1.899604*$LN0_CCA_GRD*cos(3.14159265359*$LN0_CCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN0_DR_CCA_ED" -length 0.050198 
Drift      -name    "LN0_DR_BL" -length 0.05 
Drift      -name    "LN0_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN0_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-7] -e0 $e0
Dipole     -name    "LN0_COR" -length 0 
Bpm        -name    "LN0_BPM" -length 0 
Quadrupole -name    "LN0_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-7] -e0 $e0
Drift      -name    "LN0_DR_QD_ED" -length 0.0425 
Drift      -name    "LN0_DR_VS" -length 0.1 
Drift      -name    "LN0_DR_BL" -length 0.05 
Drift      -name    "LN0_DR_CCA_ED" -length 0.050198 
Cavity     -name    "LN0_CCA0" -length 1.899604  -gradient $LN0_CCA_GRD -phase $LN0_CCA_PHS -frequency 5.9971
set e0      [expr $e0 + 1.899604*$LN0_CCA_GRD*cos(3.14159265359*$LN0_CCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN0_DR_CCA_ED" -length 0.050198 
Drift      -name    "LN0_DR_BL" -length 0.05 
Drift      -name    "LN0_WA_LNZ_IN2" -length 0 
Drift      -name    "LN0_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN0_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*7] -e0 $e0
Dipole     -name    "LN0_COR" -length 0 
Bpm        -name    "LN0_BPM" -length 0 
Quadrupole -name    "LN0_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*7] -e0 $e0
Drift      -name    "LN0_DR_QD_ED" -length 0.0425 
Drift      -name    "LN0_DR_VS" -length 0.1 
Drift      -name    "LN0_DR_LNZ_ED2" -length 0.625 
Drift      -name    "LN0_DR_BL" -length 0.05 
Drift      -name    "LN0_DR_KCA_ED" -length 0.0222541 
Cavity     -name    "LN0_KCA0" -length 0.3054918  -gradient $LN0_KCA_GRD -phase $LN0_KCA_PHS -frequency 35.9826
set e0      [expr $e0 + 0.3054918*$LN0_KCA_GRD*cos(3.14159265359*$LN0_KCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN0_DR_KCA_ED" -length 0.0222541 
Drift      -name    "LN0_DR_BL" -length 0.05 
Drift      -name    "LN0_DR_KCA_ED" -length 0.0222541 
Drift      -name    "LN0_DR_KCA0" -length 0.3054918 
Drift      -name    "LN0_DR_KCA_ED" -length 0.0222541 
Drift      -name    "LN0_DR_BL" -length 0.05 
Drift      -name    "LN0_DR_LNZ_ED2" -length 0.625 
Drift      -name    "LN0_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN0_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-7] -e0 $e0
Dipole     -name    "LN0_COR" -length 0 
Bpm        -name    "LN0_BPM" -length 0 
Quadrupole -name    "LN0_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-7] -e0 $e0
Drift      -name    "LN0_DR_QD_ED" -length 0.0425 
Drift      -name    "LN0_DR_DR" -length 0.1 
Drift      -name    "LN0_DR_BL" -length 0.05 
Drift      -name    "LN0_DR_CCA_ED" -length 0.050198 
Drift      -name    "LN0_DR_CCA0H" -length 0.349802 
Drift      -name    "LN0_DR_A" -length 0.2 
Drift      -name    "LN0_DR_VS" -length 0.1 
Drift      -name    "LN0_DR_A" -length 0.2 
Drift      -name    "LN0_DR_CT" -length 0.1 
Drift      -name    "LN0_WA_OU2" -length 0 
Drift      -name    "BC1_DR_V1" -length 0.251871515721 
Drift      -name    "BC1_DR_QD_ED" -length 0.0425 
Quadrupole -name    "BC1_QD_V01"  -length 0.04  -strength [expr 0.04*$e0*2.07336557017] -e0 $e0
Dipole     -name    "BC1_COR" -length 0 
Bpm        -name    "BC1_BPM" -length 0 
Quadrupole -name    "BC1_QD_V01"  -length 0.04  -strength [expr 0.04*$e0*2.07336557017] -e0 $e0
Drift      -name    "BC1_DR_QD_ED" -length 0.0425 
Drift      -name    "BC1_DR_V2" -length 6.55758040293 
Drift      -name    "BC1_DR_QD_ED" -length 0.0425 
Quadrupole -name    "BC1_QD_V02"  -length 0.04  -strength [expr 0.04*$e0*-9.20998959515] -e0 $e0
Dipole     -name    "BC1_COR" -length 0 
Bpm        -name    "BC1_BPM" -length 0 
Quadrupole -name    "BC1_QD_V02"  -length 0.04  -strength [expr 0.04*$e0*-9.20998959515] -e0 $e0
Drift      -name    "BC1_DR_QD_ED" -length 0.0425 
Drift      -name    "BC1_DR_V3" -length 0.204017339675 
Drift      -name    "BC1_DR_QD_ED" -length 0.0425 
Quadrupole -name    "BC1_QD_V03"  -length 0.04  -strength [expr 0.04*$e0*3.27187611302] -e0 $e0
Dipole     -name    "BC1_COR" -length 0 
Bpm        -name    "BC1_BPM" -length 0 
Quadrupole -name    "BC1_QD_V03"  -length 0.04  -strength [expr 0.04*$e0*3.27187611302] -e0 $e0
Drift      -name    "BC1_DR_QD_ED" -length 0.0425 
Drift      -name    "BC1_DR_V4" -length 0.200006716844 
Drift      -name    "BC1_DR_QD_ED" -length 0.0425 
Quadrupole -name    "BC1_QD_V04"  -length 0.04  -strength [expr 0.04*$e0*5.44371731052] -e0 $e0
Dipole     -name    "BC1_COR" -length 0 
Bpm        -name    "BC1_BPM" -length 0 
Quadrupole -name    "BC1_QD_V04"  -length 0.04  -strength [expr 0.04*$e0*5.44371731052] -e0 $e0
Drift      -name    "BC1_DR_QD_ED" -length 0.0425 
Drift      -name    "BC1_DR_V5" -length 0.201084758257 
Drift      -name    "BC1_DR_20" -length 0.5 
Sbend      -name    "BC1_DP_DIP1" -length 0.50031199  -angle -0.06117379  -E1 0 -E2 -0.06117379 -hgap 0 -fint 0.5 -tilt 0 -e0 $e0 -six_dim 1 
Drift      -name    "BC1_DR_SIDE_C" -length 0.5 
Drift      -name    "BC1_DR_SIDE" -length 2.75609062 
Sbend      -name    "BC1_DP_DIP2" -length 0.50031199  -angle 0.06117379  -E1 0.06117379 -E2 0 -hgap 0 -fint 0.5 -tilt 0 -e0 $e0 -six_dim 1 
Drift      -name    "BC1_DR_CENT" -length 0.35 
Bpm        -name    "BC1_BPM" -length 0 
Drift      -name    "BC1_DR_CENT" -length 0.35 
Sbend      -name    "BC1_DP_DIP3" -length 0.50031199  -angle 0.06117379  -E1 0 -E2 0.06117379 -hgap 0 -fint 0.5 -tilt 0 -e0 $e0 -six_dim 1 
Drift      -name    "BC1_DR_SIDE_C" -length 0.5 
Drift      -name    "BC1_DR_SIDE" -length 2.75609062 
Sbend      -name    "BC1_DP_DIP4" -length 0.50031199  -angle -0.06117379  -E1 -0.06117379 -E2 0 -hgap 0 -fint 0.5 -tilt 0 -e0 $e0 -six_dim 1 
Drift      -name    "BC1_DR_20_C" -length 0.5 
Drift      -name    "BC1_WA_OU2" -length 0 
Drift      -name    "BC1_DR_CT" -length 0.1 
Drift      -name    "BC1_DR_DI_V1" -length 0.200239191952 
Drift      -name    "BC1_DR_QD_ED" -length 0.0425 
Quadrupole -name    "BC1_QD_DI_V01"  -length 0.04  -strength [expr 0.04*$e0*-8.75952630911] -e0 $e0
Dipole     -name    "BC1_COR_DI" -length 0 
Bpm        -name    "BC1_BPM_DI" -length 0 
Quadrupole -name    "BC1_QD_DI_V01"  -length 0.04  -strength [expr 0.04*$e0*-8.75952630911] -e0 $e0
Drift      -name    "BC1_DR_QD_ED" -length 0.0425 
Drift      -name    "BC1_DR_DI_V2" -length 0.450886481385 
Drift      -name    "BC1_DR_QD_ED" -length 0.0425 
Quadrupole -name    "BC1_QD_DI_V02"  -length 0.04  -strength [expr 0.04*$e0*13.5941617757] -e0 $e0
Dipole     -name    "BC1_COR_DI" -length 0 
Bpm        -name    "BC1_BPM_DI" -length 0 
Quadrupole -name    "BC1_QD_DI_V02"  -length 0.04  -strength [expr 0.04*$e0*13.5941617757] -e0 $e0
Drift      -name    "BC1_DR_QD_ED" -length 0.0425 
Drift      -name    "BC1_DR_DI_V3" -length 1.1674752749 
Drift      -name    "BC1_DR_QD_ED" -length 0.0425 
Quadrupole -name    "BC1_QD_DI_V03"  -length 0.04  -strength [expr 0.04*$e0*-4.9529779053] -e0 $e0
Dipole     -name    "BC1_COR_DI" -length 0 
Bpm        -name    "BC1_BPM_DI" -length 0 
Quadrupole -name    "BC1_QD_DI_V03"  -length 0.04  -strength [expr 0.04*$e0*-4.9529779053] -e0 $e0
Drift      -name    "BC1_DR_QD_ED" -length 0.0425 
Drift      -name    "BC1_DR_DI_V5" -length 0.2 
Drift      -name    "BC1_DR_DI" -length 0.25 
Drift      -name    "BC1_DR_VS" -length 0.1 
Drift      -name    "BC1_DR_BL" -length 0.05 
Drift      -name    "BC1_DR_XCA_ED_DI" -length 0.1001043 
CrabCavity -name    "BC1_XCA0_DI" -length 0.9997914  -voltage $BC1_XCA_VLT -phase $BC1_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + $BC1_XCA_VLT*sin(3.14159265359*$BC1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "BC1_DR_XCA_ED_DI" -length 0.1001043 
Drift      -name    "BC1_DR_BL" -length 0.05 
Drift      -name    "BC1_DR_VS" -length 0.1 
Drift      -name    "BC1_DR_DI" -length 0.25 
Drift      -name    "BC1_DR_DI" -length 0.25 
Drift      -name    "BC1_DR_DI_QD_ED" -length 0.0425 
Quadrupole -name    "BC1_QD_DI_FH"  -length 0.04  -strength [expr 0.04*$e0*8] -e0 $e0
Dipole     -name    "BC1_COR_DI" -length 0 
Bpm        -name    "BC1_BPM_DI" -length 0 
Quadrupole -name    "BC1_QD_DI_FH"  -length 0.04  -strength [expr 0.04*$e0*8] -e0 $e0
Drift      -name    "BC1_DR_DI_QD_ED" -length 0.0425 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_DI_VS" -length 0.1 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_DI_QD_ED" -length 0.0425 
Quadrupole -name    "BC1_QD_DI_DH"  -length 0.04  -strength [expr 0.04*$e0*-8] -e0 $e0
Dipole     -name    "BC1_COR_DI" -length 0 
Bpm        -name    "BC1_BPM_DI" -length 0 
Quadrupole -name    "BC1_QD_DI_DH"  -length 0.04  -strength [expr 0.04*$e0*-8] -e0 $e0
Drift      -name    "BC1_DR_DI_QD_ED" -length 0.0425 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_DI_VS" -length 0.1 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_DI_QD_ED" -length 0.0425 
Quadrupole -name    "BC1_QD_DI_FH"  -length 0.04  -strength [expr 0.04*$e0*8] -e0 $e0
Dipole     -name    "BC1_COR_DI" -length 0 
Bpm        -name    "BC1_BPM_DI" -length 0 
Quadrupole -name    "BC1_QD_DI_FH"  -length 0.04  -strength [expr 0.04*$e0*8] -e0 $e0
Drift      -name    "BC1_DR_DI_QD_ED" -length 0.0425 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_DI_VS" -length 0.1 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_DI_QD_ED" -length 0.0425 
Quadrupole -name    "BC1_QD_DI_DH"  -length 0.04  -strength [expr 0.04*$e0*-8] -e0 $e0
Dipole     -name    "BC1_COR_DI" -length 0 
Bpm        -name    "BC1_BPM_DI" -length 0 
Quadrupole -name    "BC1_QD_DI_DH"  -length 0.04  -strength [expr 0.04*$e0*-8] -e0 $e0
Drift      -name    "BC1_DR_DI_QD_ED" -length 0.0425 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_DI_VS" -length 0.1 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_DI_QD_ED" -length 0.0425 
Quadrupole -name    "BC1_QD_DI_FH"  -length 0.04  -strength [expr 0.04*$e0*8] -e0 $e0
Dipole     -name    "BC1_COR_DI" -length 0 
Bpm        -name    "BC1_BPM_DI" -length 0 
Quadrupole -name    "BC1_QD_DI_FH"  -length 0.04  -strength [expr 0.04*$e0*8] -e0 $e0
Drift      -name    "BC1_DR_DI_QD_ED" -length 0.0425 
Drift      -name    "BC1_DR_DI" -length 0.25 
Sbend      -name    "BC1_DP_DI" -length 0.25  -angle 0  -E1 0 -E2 0 -hgap 0 -fint 0.5 -tilt 0 -e0 $e0 -six_dim 1 
Drift      -name    "BC1_DR_DI" -length 0.25 
Drift      -name    "BC1_DR_DI" -length 0.25 
Drift      -name    "BC1_DR_DI" -length 0.25 
Drift      -name    "BC1_WA_OU_DI2" -length 0 
Drift      -name    "LN1_DR_V1" -length 0.200084370588 
Drift      -name    "LN1_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN1_QD_V01"  -length 0.04  -strength [expr 0.04*$e0*7.92533010902] -e0 $e0
Dipole     -name    "LN1_COR" -length 0 
Bpm        -name    "LN1_BPM" -length 0 
Quadrupole -name    "LN1_QD_V01"  -length 0.04  -strength [expr 0.04*$e0*7.92533010902] -e0 $e0
Drift      -name    "LN1_DR_QD_ED" -length 0.0425 
Drift      -name    "LN1_DR_V2" -length 0.230210634207 
Drift      -name    "LN1_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN1_QD_V02"  -length 0.04  -strength [expr 0.04*$e0*-12.3823450851] -e0 $e0
Dipole     -name    "LN1_COR" -length 0 
Bpm        -name    "LN1_BPM" -length 0 
Quadrupole -name    "LN1_QD_V02"  -length 0.04  -strength [expr 0.04*$e0*-12.3823450851] -e0 $e0
Drift      -name    "LN1_DR_QD_ED" -length 0.0425 
Drift      -name    "LN1_DR_V3" -length 1.91737871107 
Drift      -name    "LN1_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN1_QD_V03"  -length 0.04  -strength [expr 0.04*$e0*1.39352653803] -e0 $e0
Dipole     -name    "LN1_COR" -length 0 
Bpm        -name    "LN1_BPM" -length 0 
Quadrupole -name    "LN1_QD_V03"  -length 0.04  -strength [expr 0.04*$e0*1.39352653803] -e0 $e0
Drift      -name    "LN1_DR_QD_ED" -length 0.0425 
Drift      -name    "LN1_DR_V4" -length 0.200000011427 
Drift      -name    "LN1_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN1_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*6.6] -e0 $e0
Dipole     -name    "LN1_COR" -length 0 
Bpm        -name    "LN1_BPM" -length 0 
Quadrupole -name    "LN1_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*6.6] -e0 $e0
Drift      -name    "LN1_DR_QD_ED" -length 0.0425 
Drift      -name    "LN1_DR_SV" -length 0.1 
Drift      -name    "LN1_DR_BL" -length 0.05 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_BL" -length 0.05 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_BL" -length 0.05 
Drift      -name    "LN1_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN1_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-6.6] -e0 $e0
Dipole     -name    "LN1_COR" -length 0 
Bpm        -name    "LN1_BPM" -length 0 
Quadrupole -name    "LN1_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-6.6] -e0 $e0
Drift      -name    "LN1_DR_QD_ED" -length 0.0425 
Drift      -name    "LN1_DR_VS" -length 0.1 
Drift      -name    "LN1_DR_BL" -length 0.05 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_BL" -length 0.05 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_BL" -length 0.05 
Drift      -name    "LN1_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN1_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*6.6] -e0 $e0
Dipole     -name    "LN1_COR" -length 0 
Bpm        -name    "LN1_BPM" -length 0 
Quadrupole -name    "LN1_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*6.6] -e0 $e0
Drift      -name    "LN1_DR_QD_ED" -length 0.0425 
Drift      -name    "LN1_DR_SV" -length 0.1 
Drift      -name    "LN1_DR_BL" -length 0.05 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_BL" -length 0.05 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_BL" -length 0.05 
Drift      -name    "LN1_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN1_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-6.6] -e0 $e0
Dipole     -name    "LN1_COR" -length 0 
Bpm        -name    "LN1_BPM" -length 0 
Quadrupole -name    "LN1_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-6.6] -e0 $e0
Drift      -name    "LN1_DR_QD_ED" -length 0.0425 
Drift      -name    "LN1_DR_VS" -length 0.1 
Drift      -name    "LN1_DR_BL" -length 0.05 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_BL" -length 0.05 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_BL" -length 0.05 
Drift      -name    "LN1_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN1_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*6.6] -e0 $e0
Dipole     -name    "LN1_COR" -length 0 
Bpm        -name    "LN1_BPM" -length 0 
Quadrupole -name    "LN1_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*6.6] -e0 $e0
Drift      -name    "LN1_DR_QD_ED" -length 0.0425 
Drift      -name    "LN1_DR_SV" -length 0.1 
Drift      -name    "LN1_DR_BL" -length 0.05 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_BL" -length 0.05 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_BL" -length 0.05 
Drift      -name    "LN1_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN1_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-6.6] -e0 $e0
Dipole     -name    "LN1_COR" -length 0 
Bpm        -name    "LN1_BPM" -length 0 
Quadrupole -name    "LN1_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-6.6] -e0 $e0
Drift      -name    "LN1_DR_QD_ED" -length 0.0425 
Drift      -name    "LN1_DR_VS" -length 0.1 
Drift      -name    "LN1_DR_BL" -length 0.05 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_BL" -length 0.05 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_BL" -length 0.05 
Drift      -name    "LN1_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN1_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*6.6] -e0 $e0
Dipole     -name    "LN1_COR" -length 0 
Bpm        -name    "LN1_BPM" -length 0 
Quadrupole -name    "LN1_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*6.6] -e0 $e0
Drift      -name    "LN1_DR_QD_ED" -length 0.0425 
Drift      -name    "LN1_DR_SV" -length 0.1 
Drift      -name    "LN1_DR_BL" -length 0.05 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_BL" -length 0.05 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_BL" -length 0.05 
Drift      -name    "LN1_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN1_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-6.6] -e0 $e0
Dipole     -name    "LN1_COR" -length 0 
Bpm        -name    "LN1_BPM" -length 0 
Quadrupole -name    "LN1_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-6.6] -e0 $e0
Drift      -name    "LN1_DR_QD_ED" -length 0.0425 
Drift      -name    "LN1_DR_VS" -length 0.1 
Drift      -name    "LN1_DR_BL" -length 0.05 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_BL" -length 0.05 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_BL" -length 0.05 
Drift      -name    "LN1_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN1_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*6.6] -e0 $e0
Dipole     -name    "LN1_COR" -length 0 
Bpm        -name    "LN1_BPM" -length 0 
Quadrupole -name    "LN1_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*6.6] -e0 $e0
Drift      -name    "LN1_DR_QD_ED" -length 0.0425 
Drift      -name    "LN1_DR_DR" -length 0.1 
Drift      -name    "LN1_DR_BL" -length 0.05 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_XCA_" -length 0.9164755 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_BL" -length 0.05 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_XCA_" -length 0.9164755 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_BL" -length 0.05 
Drift      -name    "LN1_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN1_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-6.6] -e0 $e0
Dipole     -name    "LN1_COR" -length 0 
Bpm        -name    "LN1_BPM" -length 0 
Quadrupole -name    "LN1_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-6.6] -e0 $e0
Drift      -name    "LN1_DR_QD_ED" -length 0.0425 
Drift      -name    "LN1_DR_DR" -length 0.1 
Drift      -name    "LN1_DR_BL" -length 0.05 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_XCA_A" -length 0.8164755 
Drift      -name    "LN1_DR_VS" -length 0.1 
Drift      -name    "LN1_WA_OU2" -length 0 
Drift      -name    "BC2_DR_V1" -length 0.948789460833 
Drift      -name    "BC2_DR_QD_ED" -length 0.0425 
Quadrupole -name    "BC2_QD_V01"  -length 0.04  -strength [expr 0.04*$e0*0.346290548625] -e0 $e0
Dipole     -name    "BC2_COR" -length 0 
Bpm        -name    "BC2_BPM" -length 0 
Quadrupole -name    "BC2_QD_V01"  -length 0.04  -strength [expr 0.04*$e0*0.346290548625] -e0 $e0
Drift      -name    "BC2_DR_QD_ED" -length 0.0425 
Drift      -name    "BC2_DR_V2" -length 5.32659442836 
Drift      -name    "BC2_DR_QD_ED" -length 0.0425 
Quadrupole -name    "BC2_QD_V02"  -length 0.04  -strength [expr 0.04*$e0*-11.2656972499] -e0 $e0
Dipole     -name    "BC2_COR" -length 0 
Bpm        -name    "BC2_BPM" -length 0 
Quadrupole -name    "BC2_QD_V02"  -length 0.04  -strength [expr 0.04*$e0*-11.2656972499] -e0 $e0
Drift      -name    "BC2_DR_QD_ED" -length 0.0425 
Drift      -name    "BC2_DR_V3" -length 0.203873836569 
Drift      -name    "BC2_DR_QD_ED" -length 0.0425 
Quadrupole -name    "BC2_QD_V03"  -length 0.04  -strength [expr 0.04*$e0*10.5381281712] -e0 $e0
Dipole     -name    "BC2_COR" -length 0 
Bpm        -name    "BC2_BPM" -length 0 
Quadrupole -name    "BC2_QD_V03"  -length 0.04  -strength [expr 0.04*$e0*10.5381281712] -e0 $e0
Drift      -name    "BC2_DR_QD_ED" -length 0.0425 
Drift      -name    "BC2_DR_V4" -length 0.2 
Drift      -name    "BC2_DR_20" -length 0.5 
Sbend      -name    "BC2_DP_DIP1" -length 0.60005635  -angle -0.02373648  -E1 0 -E2 -0.02373648 -hgap 0 -fint 0.5 -tilt 0 -e0 $e0 -six_dim 1 
Drift      -name    "BC2_DR_SIDE_C" -length 0.5 
Drift      -name    "BC2_DR_SIDE" -length 3.20104257 
Sbend      -name    "BC2_DP_DIP2" -length 0.60005635  -angle 0.02373648  -E1 0.02373648 -E2 0 -hgap 0 -fint 0.5 -tilt 0 -e0 $e0 -six_dim 1 
Drift      -name    "BC2_DR_CENT_C" -length 0.25 
Bpm        -name    "BC2_BPM" -length 0 
Drift      -name    "BC2_DR_CENT" -length 0.25 
Sbend      -name    "BC2_DP_DIP3" -length 0.60005635  -angle 0.02373648  -E1 0 -E2 0.02373648 -hgap 0 -fint 0.5 -tilt 0 -e0 $e0 -six_dim 1 
Drift      -name    "BC2_DR_SIDE_C" -length 0.5 
Drift      -name    "BC2_DR_SIDE" -length 3.20104257 
Sbend      -name    "BC2_DP_DIP4" -length 0.60005635  -angle -0.02373648  -E1 -0.02373648 -E2 0 -hgap 0 -fint 0.5 -tilt 0 -e0 $e0 -six_dim 1 
Drift      -name    "BC2_DR_20_C" -length 0.5 
Drift      -name    "BC2_WA_OU2" -length 0 
Drift      -name    "LN2_DR_CT" -length 0.1 
Drift      -name    "LN2_DR_V1" -length 2.03137296374 
Drift      -name    "LN2_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN2_QD_V01"  -length 0.04  -strength [expr 0.04*$e0*14] -e0 $e0
Dipole     -name    "LN2_COR" -length 0 
Bpm        -name    "LN2_BPM" -length 0 
Quadrupole -name    "LN2_QD_V01"  -length 0.04  -strength [expr 0.04*$e0*14] -e0 $e0
Drift      -name    "LN2_DR_QD_ED" -length 0.0425 
Drift      -name    "LN2_DR_V2" -length 0.552946789152 
Drift      -name    "LN2_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN2_QD_V02"  -length 0.04  -strength [expr 0.04*$e0*-12.9567508912] -e0 $e0
Dipole     -name    "LN2_COR" -length 0 
Bpm        -name    "LN2_BPM" -length 0 
Quadrupole -name    "LN2_QD_V02"  -length 0.04  -strength [expr 0.04*$e0*-12.9567508912] -e0 $e0
Drift      -name    "LN2_DR_QD_ED" -length 0.0425 
Drift      -name    "LN2_DR_V3" -length 1.45717412229 
Drift      -name    "LN2_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN2_QD_V03"  -length 0.04  -strength [expr 0.04*$e0*8.99442961345] -e0 $e0
Dipole     -name    "LN2_COR" -length 0 
Bpm        -name    "LN2_BPM" -length 0 
Quadrupole -name    "LN2_QD_V03"  -length 0.04  -strength [expr 0.04*$e0*8.99442961345] -e0 $e0
Drift      -name    "LN2_DR_QD_ED" -length 0.0425 
Drift      -name    "LN2_DR_V4" -length 2.99994373837 
Drift      -name    "LN2_DR_VS" -length 0.1 
Drift      -name    "LN2_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN2_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*6.7] -e0 $e0
Dipole     -name    "LN2_COR" -length 0 
Bpm        -name    "LN2_BPM" -length 0 
Quadrupole -name    "LN2_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*6.7] -e0 $e0
Drift      -name    "LN2_DR_QD_ED" -length 0.0425 
Drift      -name    "LN2_DR_SV" -length 0.1 
Drift      -name    "LN2_DR_BL" -length 0.05 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_BL" -length 0.05 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_BL" -length 0.05 
Drift      -name    "LN2_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN2_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-6.7] -e0 $e0
Dipole     -name    "LN2_COR" -length 0 
Bpm        -name    "LN2_BPM" -length 0 
Quadrupole -name    "LN2_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-6.7] -e0 $e0
Drift      -name    "LN2_DR_QD_ED" -length 0.0425 
Drift      -name    "LN2_DR_VS" -length 0.1 
Drift      -name    "LN2_DR_BL" -length 0.05 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_BL" -length 0.05 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_BL" -length 0.05 
Drift      -name    "LN2_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN2_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*6.7] -e0 $e0
Dipole     -name    "LN2_COR" -length 0 
Bpm        -name    "LN2_BPM" -length 0 
Quadrupole -name    "LN2_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*6.7] -e0 $e0
Drift      -name    "LN2_DR_QD_ED" -length 0.0425 
Drift      -name    "LN2_DR_SV" -length 0.1 
Drift      -name    "LN2_DR_BL" -length 0.05 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_BL" -length 0.05 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_BL" -length 0.05 
Drift      -name    "LN2_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN2_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-6.7] -e0 $e0
Dipole     -name    "LN2_COR" -length 0 
Bpm        -name    "LN2_BPM" -length 0 
Quadrupole -name    "LN2_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-6.7] -e0 $e0
Drift      -name    "LN2_DR_QD_ED" -length 0.0425 
Drift      -name    "LN2_DR_VS" -length 0.1 
Drift      -name    "LN2_DR_BL" -length 0.05 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_BL" -length 0.05 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_BL" -length 0.05 
Drift      -name    "LN2_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN2_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*6.7] -e0 $e0
Dipole     -name    "LN2_COR" -length 0 
Bpm        -name    "LN2_BPM" -length 0 
Quadrupole -name    "LN2_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*6.7] -e0 $e0
Drift      -name    "LN2_DR_QD_ED" -length 0.0425 
Drift      -name    "LN2_WA_OU2" -length 0 
Drift      -name    "LN2_DR_DI_V1" -length 1.08694034025 
Drift      -name    "LN2_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN2_QD_DI_V01"  -length 0.04  -strength [expr 0.04*$e0*-2.69016348491] -e0 $e0
Dipole     -name    "LN2_COR_DI" -length 0 
Bpm        -name    "LN2_BPM_DI" -length 0 
Quadrupole -name    "LN2_QD_DI_V01"  -length 0.04  -strength [expr 0.04*$e0*-2.69016348491] -e0 $e0
Drift      -name    "LN2_DR_QD_ED" -length 0.0425 
Drift      -name    "LN2_DR_DI_V2" -length 1.01311154351 
Drift      -name    "LN2_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN2_QD_DI_V02"  -length 0.04  -strength [expr 0.04*$e0*10.0995580776] -e0 $e0
Dipole     -name    "LN2_COR_DI" -length 0 
Bpm        -name    "LN2_BPM_DI" -length 0 
Quadrupole -name    "LN2_QD_DI_V02"  -length 0.04  -strength [expr 0.04*$e0*10.0995580776] -e0 $e0
Drift      -name    "LN2_DR_QD_ED" -length 0.0425 
Drift      -name    "LN2_DR_DI_V3" -length 0.210195703151 
Drift      -name    "LN2_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN2_QD_DI_V03"  -length 0.04  -strength [expr 0.04*$e0*-12.0941843422] -e0 $e0
Dipole     -name    "LN2_COR_DI" -length 0 
Bpm        -name    "LN2_BPM_DI" -length 0 
Quadrupole -name    "LN2_QD_DI_V03"  -length 0.04  -strength [expr 0.04*$e0*-12.0941843422] -e0 $e0
Drift      -name    "LN2_DR_QD_ED" -length 0.0425 
Drift      -name    "LN2_DR_DI_V5" -length 0.2 
Drift      -name    "LN2_DR_VS" -length 0.1 
Drift      -name    "LN2_DR_DI" -length 0.25 
Drift      -name    "LN2_DR_VS_DI" -length 0.1 
Drift      -name    "LN2_DR_BL_DI" -length 0.05 
Drift      -name    "LN2_DR_XCA_ED_DI" -length 0.1001043 
CrabCavity -name    "LN2_XCA0_DI" -length 0.9997914  -voltage $LN2_XCA_VLT_DI -phase $LN2_XCA_PHS_DI -frequency 11.9942
set e0      [expr $e0 + $LN2_XCA_VLT_DI*sin(3.14159265359*$LN2_XCA_PHS_DI/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED_DI" -length 0.1001043 
Drift      -name    "LN2_DR_BL_DI" -length 0.05 
Drift      -name    "LN2_DR_DI" -length 0.25 
Drift      -name    "LN2_DR_DI" -length 0.25 
Drift      -name    "LN2_DR_DI_QD_ED" -length 0.0425 
Quadrupole -name    "LN2_QD_DI_FH"  -length 0.04  -strength [expr 0.04*$e0*8] -e0 $e0
Dipole     -name    "LN2_COR_DI" -length 0 
Bpm        -name    "LN2_BPM_DI" -length 0 
Quadrupole -name    "LN2_QD_DI_FH"  -length 0.04  -strength [expr 0.04*$e0*8] -e0 $e0
Drift      -name    "LN2_DR_DI_QD_ED" -length 0.0425 
Drift      -name    "LN2_DR_FODO" -length 0.15 
Drift      -name    "LN2_DR_FODO" -length 0.15 
Drift      -name    "LN2_DR_FODO" -length 0.15 
Drift      -name    "LN2_DR_FODO" -length 0.15 
Drift      -name    "LN2_DR_DI_VS" -length 0.1 
Drift      -name    "LN2_DR_FODO" -length 0.15 
Drift      -name    "LN2_DR_FODO" -length 0.15 
Drift      -name    "LN2_DR_FODO" -length 0.15 
Drift      -name    "LN2_DR_FODO" -length 0.15 
Drift      -name    "LN2_DR_DI_QD_ED" -length 0.0425 
Quadrupole -name    "LN2_QD_DI_DH"  -length 0.04  -strength [expr 0.04*$e0*-8] -e0 $e0
Dipole     -name    "LN2_COR_DI" -length 0 
Bpm        -name    "LN2_BPM_DI" -length 0 
Quadrupole -name    "LN2_QD_DI_DH"  -length 0.04  -strength [expr 0.04*$e0*-8] -e0 $e0
Drift      -name    "LN2_DR_DI_QD_ED" -length 0.0425 
Drift      -name    "LN2_DR_FODO" -length 0.15 
Drift      -name    "LN2_DR_FODO" -length 0.15 
Drift      -name    "LN2_DR_FODO" -length 0.15 
Drift      -name    "LN2_DR_FODO" -length 0.15 
Drift      -name    "LN2_DR_DI_VS" -length 0.1 
Drift      -name    "LN2_DR_FODO" -length 0.15 
Drift      -name    "LN2_DR_FODO" -length 0.15 
Drift      -name    "LN2_DR_FODO" -length 0.15 
Drift      -name    "LN2_DR_FODO" -length 0.15 
Drift      -name    "LN2_DR_DI_QD_ED" -length 0.0425 
Quadrupole -name    "LN2_QD_DI_FH"  -length 0.04  -strength [expr 0.04*$e0*8] -e0 $e0
Dipole     -name    "LN2_COR_DI" -length 0 
Bpm        -name    "LN2_BPM_DI" -length 0 
Quadrupole -name    "LN2_QD_DI_FH"  -length 0.04  -strength [expr 0.04*$e0*8] -e0 $e0
Drift      -name    "LN2_DR_DI_QD_ED" -length 0.0425 
Drift      -name    "LN2_DR_FODO" -length 0.15 
Drift      -name    "LN2_DR_FODO" -length 0.15 
Drift      -name    "LN2_DR_FODO" -length 0.15 
Drift      -name    "LN2_DR_FODO" -length 0.15 
Drift      -name    "LN2_DR_DI_VS" -length 0.1 
Drift      -name    "LN2_DR_FODO" -length 0.15 
Drift      -name    "LN2_DR_FODO" -length 0.15 
Drift      -name    "LN2_DR_FODO" -length 0.15 
Drift      -name    "LN2_DR_FODO" -length 0.15 
Drift      -name    "LN2_DR_DI_QD_ED" -length 0.0425 
Quadrupole -name    "LN2_QD_DI_DH"  -length 0.04  -strength [expr 0.04*$e0*-8] -e0 $e0
Dipole     -name    "LN2_COR_DI" -length 0 
Bpm        -name    "LN2_BPM_DI" -length 0 
Quadrupole -name    "LN2_QD_DI_DH"  -length 0.04  -strength [expr 0.04*$e0*-8] -e0 $e0
Drift      -name    "LN2_DR_DI_QD_ED" -length 0.0425 
Drift      -name    "LN2_DR_FODO" -length 0.15 
Drift      -name    "LN2_DR_FODO" -length 0.15 
Drift      -name    "LN2_DR_FODO" -length 0.15 
Drift      -name    "LN2_DR_FODO" -length 0.15 
Drift      -name    "LN2_DR_DI_VS" -length 0.1 
Drift      -name    "LN2_DR_FODO" -length 0.15 
Drift      -name    "LN2_DR_FODO" -length 0.15 
Drift      -name    "LN2_DR_FODO" -length 0.15 
Drift      -name    "LN2_DR_FODO" -length 0.15 
Drift      -name    "LN2_DR_DI_QD_ED" -length 0.0425 
Quadrupole -name    "LN2_QD_DI_FH"  -length 0.04  -strength [expr 0.04*$e0*8] -e0 $e0
Dipole     -name    "LN2_COR_DI" -length 0 
Bpm        -name    "LN2_BPM_DI" -length 0 
Quadrupole -name    "LN2_QD_DI_FH"  -length 0.04  -strength [expr 0.04*$e0*8] -e0 $e0
Drift      -name    "LN2_DR_DI_QD_ED" -length 0.0425 
Drift      -name    "LN2_DR_FODO" -length 0.15 
Drift      -name    "LN2_DR_FODO" -length 0.15 
Drift      -name    "LN2_DR_FODO" -length 0.15 
Drift      -name    "LN2_DR_FODO" -length 0.15 
Drift      -name    "LN2_DR_DI_VS" -length 0.1 
Sbend      -name    "LN2_DP_DI" -length 0.15  -angle 0  -E1 0 -E2 0 -hgap 0 -fint 0.5 -tilt 0 -e0 $e0 -six_dim 1 
Drift      -name    "LN2_DR_FODO" -length 0.15 
Drift      -name    "LN2_DR_FODO" -length 0.15 
Drift      -name    "LN2_DR_FODO" -length 0.15 
Drift      -name    "LN2_DR_DI_QD_ED" -length 0.0425 
Quadrupole -name    "LN2_QD_DI_DH"  -length 0.04  -strength [expr 0.04*$e0*-8] -e0 $e0
Dipole     -name    "LN2_COR_DI" -length 0 
Bpm        -name    "LN2_BPM_DI" -length 0 
Quadrupole -name    "LN2_QD_DI_DH"  -length 0.04  -strength [expr 0.04*$e0*-8] -e0 $e0
Drift      -name    "LN2_DR_DI_QD_ED" -length 0.0425 
Drift      -name    "LN2_DR_DI" -length 0.25 
# Drift      -name    "LN2_WA_OU_DI2" -length 0 
Drift      -name    "LN2_DR_BP_V1" -length 2.86795412417 
Drift      -name    "LN2_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN2_QD_BP_V01"  -length 0.04  -strength [expr 0.04*$e0*-0.267513738089] -e0 $e0
Dipole     -name    "LN2_COR" -length 0 
Bpm        -name    "LN2_BPM" -length 0 
Quadrupole -name    "LN2_QD_BP_V01"  -length 0.04  -strength [expr 0.04*$e0*-0.267513738089] -e0 $e0
Drift      -name    "LN2_DR_QD_ED" -length 0.0425 
Drift      -name    "LN2_DR_BP_V2" -length 2.99999855297 
Drift      -name    "LN2_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN2_QD_BP_V02"  -length 0.04  -strength [expr 0.04*$e0*10.1785114794] -e0 $e0
Dipole     -name    "LN2_COR" -length 0 
Bpm        -name    "LN2_BPM" -length 0 
Quadrupole -name    "LN2_QD_BP_V02"  -length 0.04  -strength [expr 0.04*$e0*10.1785114794] -e0 $e0
Drift      -name    "LN2_DR_QD_ED" -length 0.0425 
Drift      -name    "LN2_DR_BP_V3" -length 0.447636787193 
Drift      -name    "LN2_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN2_QD_BP_V03"  -length 0.04  -strength [expr 0.04*$e0*-9.64926390603] -e0 $e0
Dipole     -name    "LN2_COR" -length 0 
Bpm        -name    "LN2_BPM" -length 0 
Quadrupole -name    "LN2_QD_BP_V03"  -length 0.04  -strength [expr 0.04*$e0*-9.64926390603] -e0 $e0
Drift      -name    "LN2_DR_QD_ED" -length 0.0425 
Drift      -name    "LN2_DR_BP_V4" -length 0.207756077703 
Drift      -name    "LN2_DR_BP_0" -length 0.2 
Drift      -name    "LN2_DR_BP_BL" -length 0.05 
Drift      -name    "LN2_DR_SCA_ED" -length 0.08325105 
CrabCavity -name    "LN2_SCA0" -length 0.5334979  -voltage $LN2_SCA_VLT -phase $LN2_SCA_PHS -frequency 2.997
set e0      [expr $e0 + $LN2_SCA_VLT*sin(3.14159265359*$LN2_SCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_SCA_ED" -length 0.08325105 
Drift      -name    "LN2_DR_BP_BL" -length 0.05 
Drift      -name    "LN2_DR_BP_0" -length 0.2 
Drift      -name    "LN2_DR_BP" -length 0.4 
Drift      -name    "LN2_DR_BP" -length 0.4 
Drift      -name    "LN2_DR_BP_VS" -length 0.1 
Drift      -name    "LN2_DR_BP_DR" -length 0.1 
# Drift      -name    "LN2_WA_BP_OU2" -length 0 
Drift      -name    "LN2_DP_SEPM" -length 0.5 
Drift      -name    "LN2_DR_BP" -length 0.4 
Drift      -name    "LN2_DR_BP" -length 0.4 
Drift      -name    "LN2_DR_BP" -length 0.4 
Drift      -name    "LN2_DR_BP" -length 0.4 
Drift      -name    "LN2_DR_BP" -length 0.4 
Drift      -name    "LN2_DR_BP_VS" -length 0.1 
Drift      -name    "LN2_DR_BP" -length 0.4 
Drift      -name    "LN2_WA_BP_OU2_" -length 0 
Drift      -name    "LN3_DR_CT" -length 0.1 
Drift      -name    "LN3_DR_V1" -length 0.111008512413 
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN3_QD_V01"  -length 0.04  -strength [expr 0.04*$e0*-7.04951168053] -e0 $e0
Dipole     -name    "LN3_COR" -length 0 
Bpm        -name    "LN3_BPM" -length 0 
Quadrupole -name    "LN3_QD_V01"  -length 0.04  -strength [expr 0.04*$e0*-7.04951168053] -e0 $e0
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Drift      -name    "LN3_DR_V2" -length 0.100000001776 
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN3_QD_V02"  -length 0.04  -strength [expr 0.04*$e0*11.7854733657] -e0 $e0
Dipole     -name    "LN3_COR" -length 0 
Bpm        -name    "LN3_BPM" -length 0 
Quadrupole -name    "LN3_QD_V02"  -length 0.04  -strength [expr 0.04*$e0*11.7854733657] -e0 $e0
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Drift      -name    "LN3_DR_V3" -length 0.927088651771 
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN3_QD_V03"  -length 0.04  -strength [expr 0.04*$e0*-5.42557338402] -e0 $e0
Dipole     -name    "LN3_COR" -length 0 
Bpm        -name    "LN3_BPM" -length 0 
Quadrupole -name    "LN3_QD_V03"  -length 0.04  -strength [expr 0.04*$e0*-5.42557338402] -e0 $e0
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Drift      -name    "LN3_DR_V4" -length 0.100000003751 
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN3_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*4.2] -e0 $e0
Dipole     -name    "LN3_COR" -length 0 
Bpm        -name    "LN3_BPM" -length 0 
Quadrupole -name    "LN3_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*4.2] -e0 $e0
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Drift      -name    "LN3_DR_SV" -length 0.1 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN3_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-4.2] -e0 $e0
Dipole     -name    "LN3_COR" -length 0 
Bpm        -name    "LN3_BPM" -length 0 
Quadrupole -name    "LN3_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-4.2] -e0 $e0
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Drift      -name    "LN3_DR_VS" -length 0.1 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN3_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*4.2] -e0 $e0
Dipole     -name    "LN3_COR" -length 0 
Bpm        -name    "LN3_BPM" -length 0 
Quadrupole -name    "LN3_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*4.2] -e0 $e0
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Drift      -name    "LN3_DR_SV" -length 0.1 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN3_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-4.2] -e0 $e0
Dipole     -name    "LN3_COR" -length 0 
Bpm        -name    "LN3_BPM" -length 0 
Quadrupole -name    "LN3_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-4.2] -e0 $e0
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Drift      -name    "LN3_DR_VS" -length 0.1 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN3_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*4.2] -e0 $e0
Dipole     -name    "LN3_COR" -length 0 
Bpm        -name    "LN3_BPM" -length 0 
Quadrupole -name    "LN3_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*4.2] -e0 $e0
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Drift      -name    "LN3_DR_SV" -length 0.1 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN3_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-4.2] -e0 $e0
Dipole     -name    "LN3_COR" -length 0 
Bpm        -name    "LN3_BPM" -length 0 
Quadrupole -name    "LN3_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-4.2] -e0 $e0
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Drift      -name    "LN3_DR_VS" -length 0.1 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN3_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*4.2] -e0 $e0
Dipole     -name    "LN3_COR" -length 0 
Bpm        -name    "LN3_BPM" -length 0 
Quadrupole -name    "LN3_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*4.2] -e0 $e0
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Drift      -name    "LN3_DR_SV" -length 0.1 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN3_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-4.2] -e0 $e0
Dipole     -name    "LN3_COR" -length 0 
Bpm        -name    "LN3_BPM" -length 0 
Quadrupole -name    "LN3_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-4.2] -e0 $e0
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Drift      -name    "LN3_DR_VS" -length 0.1 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN3_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*4.2] -e0 $e0
Dipole     -name    "LN3_COR" -length 0 
Bpm        -name    "LN3_BPM" -length 0 
Quadrupole -name    "LN3_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*4.2] -e0 $e0
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Drift      -name    "LN3_DR_SV" -length 0.1 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN3_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-4.2] -e0 $e0
Dipole     -name    "LN3_COR" -length 0 
Bpm        -name    "LN3_BPM" -length 0 
Quadrupole -name    "LN3_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-4.2] -e0 $e0
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Drift      -name    "LN3_DR_VS" -length 0.1 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN3_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*4.2] -e0 $e0
Dipole     -name    "LN3_COR" -length 0 
Bpm        -name    "LN3_BPM" -length 0 
Quadrupole -name    "LN3_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*4.2] -e0 $e0
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Drift      -name    "LN3_DR_SV" -length 0.1 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN3_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-4.2] -e0 $e0
Dipole     -name    "LN3_COR" -length 0 
Bpm        -name    "LN3_BPM" -length 0 
Quadrupole -name    "LN3_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-4.2] -e0 $e0
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Drift      -name    "LN3_DR_VS" -length 0.1 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN3_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*4.2] -e0 $e0
Dipole     -name    "LN3_COR" -length 0 
Bpm        -name    "LN3_BPM" -length 0 
Quadrupole -name    "LN3_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*4.2] -e0 $e0
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Drift      -name    "LN3_DR_SV" -length 0.1 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN3_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-4.2] -e0 $e0
Dipole     -name    "LN3_COR" -length 0 
Bpm        -name    "LN3_BPM" -length 0 
Quadrupole -name    "LN3_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-4.2] -e0 $e0
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Drift      -name    "LN3_DR_VS" -length 0.1 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN3_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*4.2] -e0 $e0
Dipole     -name    "LN3_COR" -length 0 
Bpm        -name    "LN3_BPM" -length 0 
Quadrupole -name    "LN3_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*4.2] -e0 $e0
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Drift      -name    "LN3_DR_SV" -length 0.1 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN3_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-4.2] -e0 $e0
Dipole     -name    "LN3_COR" -length 0 
Bpm        -name    "LN3_BPM" -length 0 
Quadrupole -name    "LN3_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-4.2] -e0 $e0
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Drift      -name    "LN3_DR_VS" -length 0.1 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN3_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*4.2] -e0 $e0
Dipole     -name    "LN3_COR" -length 0 
Bpm        -name    "LN3_BPM" -length 0 
Quadrupole -name    "LN3_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*4.2] -e0 $e0
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Drift      -name    "LN3_DR_SV" -length 0.1 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN3_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-4.2] -e0 $e0
Dipole     -name    "LN3_COR" -length 0 
Bpm        -name    "LN3_BPM" -length 0 
Quadrupole -name    "LN3_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-4.2] -e0 $e0
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Drift      -name    "LN3_DR_DR" -length 0.1 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_XCA_" -length 0.9164755 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_XCA_" -length 0.9164755 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_CT" -length 0.1 
Drift      -name    "LN3_WA_OU2" -length 0 
Drift      -name    "LN3_DR_BP_V1" -length 0.761339103917 
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN3_QD_BP_V01"  -length 0.04  -strength [expr 0.04*$e0*-8.84842991536] -e0 $e0
Dipole     -name    "LN3_COR" -length 0 
Bpm        -name    "LN3_BPM" -length 0 
Quadrupole -name    "LN3_QD_BP_V01"  -length 0.04  -strength [expr 0.04*$e0*-8.84842991536] -e0 $e0
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Drift      -name    "LN3_DR_BP_V2" -length 1.75034314293 
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN3_QD_BP_V02"  -length 0.04  -strength [expr 0.04*$e0*10.3429006414] -e0 $e0
Dipole     -name    "LN3_COR" -length 0 
Bpm        -name    "LN3_BPM" -length 0 
Quadrupole -name    "LN3_QD_BP_V02"  -length 0.04  -strength [expr 0.04*$e0*10.3429006414] -e0 $e0
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Drift      -name    "LN3_DR_BP_V3" -length 0.952352093322 
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN3_QD_BP_V03"  -length 0.04  -strength [expr 0.04*$e0*-9.12979503093] -e0 $e0
Dipole     -name    "LN3_COR" -length 0 
Bpm        -name    "LN3_BPM" -length 0 
Quadrupole -name    "LN3_QD_BP_V03"  -length 0.04  -strength [expr 0.04*$e0*-9.12979503093] -e0 $e0
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Drift      -name    "LN3_DR_BP_V4" -length 0.544004323931 
Drift      -name    "LN3_DR_BP_0" -length 0.2 
Drift      -name    "LN3_DR_BP_BL" -length 0.05 
Drift      -name    "LN3_DR_SCA_ED" -length 0.08325105 
CrabCavity -name    "LN3_SCA0" -length 0.5334979  -voltage $LN3_SCA_VLT -phase $LN3_SCA_PHS -frequency 2.997
set e0      [expr $e0 + $LN3_SCA_VLT*sin(3.14159265359*$LN3_SCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_SCA_ED" -length 0.08325105 
Drift      -name    "LN3_DR_BP_BL" -length 0.05 
Drift      -name    "LN3_DR_BP_0" -length 0.2 
Drift      -name    "LN3_DR_BP" -length 0.4 
Drift      -name    "LN3_DR_BP_VS" -length 0.1 
Drift      -name    "LN3_DR_BP" -length 0.4 
Drift      -name    "LN3_BP_WA_OU2" -length 0 
Drift      -name    "LN3_DP_SEPM" -length 0.45 
Drift      -name    "LN3_DR_BP" -length 0.4 
Drift      -name    "LN3_DR_BP" -length 0.4 
Drift      -name    "LN3_DR_BP" -length 0.4 
Drift      -name    "LN3_DR_BP" -length 0.4 
Drift      -name    "LN3_DR_BP" -length 0.4 
Drift      -name    "LN3_DR_BP_VS" -length 0.1 
Drift      -name    "LN3_DR_BP" -length 0.4 
# Drift      -name    "LN3_WA_BP_OU2_" -length 0 
Drift      -name    "TMC_DR_V1" -length 0.437110315839 
Drift      -name    "TMC_DR_QD_ED" -length 0.0425 
Quadrupole -name    "TMC_QD_V01"  -length 0.04  -strength [expr 0.04*$e0*3.73090682896] -e0 $e0
Dipole     -name    "TMC_COR" -length 0 
Bpm        -name    "TMC_BPM" -length 0 
Quadrupole -name    "TMC_QD_V01"  -length 0.04  -strength [expr 0.04*$e0*3.73090682896] -e0 $e0
Drift      -name    "TMC_DR_QD_ED" -length 0.0425 
Drift      -name    "TMC_DR_V2" -length 7.42850346066 
Drift      -name    "TMC_DR_QD_ED" -length 0.0425 
Quadrupole -name    "TMC_QD_V02"  -length 0.04  -strength [expr 0.04*$e0*-8.70689791285] -e0 $e0
Dipole     -name    "TMC_COR" -length 0 
Bpm        -name    "TMC_BPM" -length 0 
Quadrupole -name    "TMC_QD_V02"  -length 0.04  -strength [expr 0.04*$e0*-8.70689791285] -e0 $e0
Drift      -name    "TMC_DR_QD_ED" -length 0.0425 
Drift      -name    "TMC_DR_V3" -length 0.200155335728 
Drift      -name    "TMC_DR_QD_ED" -length 0.0425 
Quadrupole -name    "TMC_QD_V03"  -length 0.04  -strength [expr 0.04*$e0*8.4224185994] -e0 $e0
Dipole     -name    "TMC_COR" -length 0 
Bpm        -name    "TMC_BPM" -length 0 
Quadrupole -name    "TMC_QD_V03"  -length 0.04  -strength [expr 0.04*$e0*8.4224185994] -e0 $e0
Drift      -name    "TMC_DR_QD_ED" -length 0.0425 
Drift      -name    "TMC_DR_V4" -length 0.2 
Drift      -name    "TMC_DR_20" -length 0.5 
Sbend      -name    "TMC_DP_DIP1" -length 0.50000057  -angle -0.00261799  -E1 0 -E2 -0.00261799 -hgap 0 -fint 0.5 -tilt 0 -e0 $e0 -six_dim 1 
Drift      -name    "TMC_DR_SIDE_C" -length 0.5 
Drift      -name    "TMC_DR_SIDE" -length 3.00001199 
Sbend      -name    "TMC_DP_DIP2" -length 0.50000057  -angle 0.00261799  -E1 0.00261799 -E2 0 -hgap 0 -fint 0.5 -tilt 0 -e0 $e0 -six_dim 1 
Drift      -name    "TMC_DR_CENT_C" -length 0.25 
Bpm        -name    "TMC_BPM" -length 0 
Drift      -name    "TMC_DR_CENT" -length 0.5 
Sbend      -name    "TMC_DP_DIP3" -length 0.50000057  -angle 0.00261799  -E1 0 -E2 0.00261799 -hgap 0 -fint 0.5 -tilt 0 -e0 $e0 -six_dim 1 
Drift      -name    "TMC_DR_SIDE_C" -length 0.5 
Drift      -name    "TMC_DR_SIDE" -length 3.00001199 
Sbend      -name    "TMC_DP_DIP4" -length 0.50000057  -angle -0.00261799  -E1 -0.00261799 -E2 0 -hgap 0 -fint 0.5 -tilt 0 -e0 $e0 -six_dim 1 
Drift      -name    "TMC_DR_20_C" -length 0.5 
Drift      -name    "TMC_WA_OU2" -length 0 
Drift      -name    "HXR_DR_V1" -length 1.26432916436 
Drift      -name    "HXR_DR_QD_ED" -length 0.05 
Quadrupole -name    "HXR_QD_V01"  -length 0.05  -strength [expr 0.05*$e0*-4.45354202597] -e0 $e0
Dipole     -name    "HXR_COR" -length 0 
Bpm        -name    "HXR_BPM" -length 0 
Quadrupole -name    "HXR_QD_V01"  -length 0.05  -strength [expr 0.05*$e0*-4.45354202597] -e0 $e0
Drift      -name    "HXR_DR_QD_ED" -length 0.05 
Drift      -name    "HXR_DR_V2" -length 0.947359172562 
Drift      -name    "HXR_DR_QD_ED" -length 0.05 
Quadrupole -name    "HXR_QD_V02"  -length 0.05  -strength [expr 0.05*$e0*5.0967016693] -e0 $e0
Dipole     -name    "HXR_COR" -length 0 
Bpm        -name    "HXR_BPM" -length 0 
Quadrupole -name    "HXR_QD_V02"  -length 0.05  -strength [expr 0.05*$e0*5.0967016693] -e0 $e0
Drift      -name    "HXR_DR_QD_ED" -length 0.05 
Drift      -name    "HXR_DR_V3" -length 2.99980659233 
Drift      -name    "HXR_DR_QD_ED" -length 0.05 
Quadrupole -name    "HXR_QD_V03"  -length 0.05  -strength [expr 0.05*$e0*-4.53023027368] -e0 $e0
Dipole     -name    "HXR_COR" -length 0 
Bpm        -name    "HXR_BPM" -length 0 
Quadrupole -name    "HXR_QD_V03"  -length 0.05  -strength [expr 0.05*$e0*-4.53023027368] -e0 $e0
Drift      -name    "HXR_DR_QD_ED" -length 0.05 
Drift      -name    "HXR_DR_V4" -length 0.512266295703 
Drift      -name    "HXR_DR_CT" -length 0.1 
# Drift      -name    "HXR_WA_M_OU2" -length 0 
Drift      -name    "HXR_DR_QD_ED" -length 0.05 
Quadrupole -name    "HXR_QD_FH"  -length 0.05  -strength [expr 0.05*$e0*5] -e0 $e0
Dipole     -name    "HXR_COR" -length 0 
Bpm        -name    "HXR_BPM" -length 0 
Quadrupole -name    "HXR_QD_FH"  -length 0.05  -strength [expr 0.05*$e0*5] -e0 $e0
Drift      -name    "HXR_DR_QD_ED" -length 0.05 
Drift      -name    "HXR_DR_SV" -length 0.1 
Drift      -name    "HXR_DR_BL" -length 0.05 
Drift      -name    "HXR_DR_WIG_ED" -length 0.1 
Drift     -name    "HXR_WIG_0" -length 1.768 
Drift      -name    "HXR_DR_WIG_ED" -length 0.1 
Drift      -name    "HXR_DR_BL" -length 0.05 
Drift      -name    "HXR_DR_QD_ED" -length 0.05 
Quadrupole -name    "HXR_QD_DH"  -length 0.05  -strength [expr 0.05*$e0*-5] -e0 $e0
Dipole     -name    "HXR_COR" -length 0 
Bpm        -name    "HXR_BPM" -length 0 
Quadrupole -name    "HXR_QD_DH"  -length 0.05  -strength [expr 0.05*$e0*-5] -e0 $e0
Drift      -name    "HXR_DR_QD_ED" -length 0.05 
Drift      -name    "HXR_DR_VS" -length 0.1 
Drift      -name    "HXR_DR_BL" -length 0.05 
Drift      -name    "HXR_DR_WIG_ED" -length 0.1 
Drift     -name    "HXR_WIG_0" -length 1.768 
Drift      -name    "HXR_DR_WIG_ED" -length 0.1 
Drift      -name    "HXR_DR_BL" -length 0.05 
Drift      -name    "HXR_WA_OU2" -length 0 
