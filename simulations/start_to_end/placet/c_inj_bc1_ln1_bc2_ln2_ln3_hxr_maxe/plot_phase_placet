#!/usr/bin/env python

# This file a script for ploting the phase spaces of a bunch distribution
#
# Copyright (C) 2013 Avni AKSOY             
#
# here additional definitions can be given..
# \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\////////////////////////////////

import string
import sys,os
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import matplotlib 
import numpy
import itertools
from matplotlib.ticker import NullFormatter
from numpy import*
from matplotlib import rc, rcParams



narg=len(sys.argv)

if narg != 3:
    print "@Usage: parmela_plot.py indist plotfile"
    sys.exit()
    
indist = sys.argv[1] 
plot_file = sys.argv[2] 

rc('text',usetex=True)
rc('font', family='serif', serif='Times New Roman')

matplotlib.rc('xtick', labelsize=14) 
matplotlib.rc('ytick', labelsize=14) 



nameee=indist
f = loadtxt(nameee) 

def mymean(l):
    s = 0
    for e in l:
        s += e
    s=s/len(l)
    return (s)

def mysigma(l1, l2):
    s = 0
    for e1,e2 in itertools.izip(l1,l2):
        s += e1*e2
    s=s/len(l1)
    return (s)

cc=299792458    ;#m/s
qq=75e-12       ;# C bunch charge

w = f[:,0]     ;# GeV
x = f[:,1]     ;# um
y = f[:,2]     ;# um
z = f[:,3]     ;# um
t = z/cc*1e9   ;# fs
xp =f[:,4]     ;# urad
yp =f[:,5]     ;# urad
size=len(f)    ;# npart
qpp=qq/size    ;# charge per particle

xmin  = x.min()
xmax  = x.max()
xpmin = xp.min()
xpmax = xp.max()
ymin  = y.min()
ymax  = y.max()
ypmin = yp.min()
ypmax = yp.max()
zmin = z.min()
zmax = z.max()
wmin = w.min()
wmax = w.max()
tmax = t.max()
tmin = t.min()


x0 =mymean(x)	;#um
xp0=mymean(xp)	;#urad
y0 =mymean(y)	;#um
yp0=mymean(yp)	;#urad
z0 =mymean(z)	;#um
w0 =mymean(w)	;#urad
t0 =mymean(t)	;#ps

gm=w0/0.511e-3+1
bt=sqrt(1.-1./gm/gm)
gmbt=gm*bt

sig_xx  =mysigma(x-x0,x-x0)     ;# um^2
sig_xpxp=mysigma(xp-xp0,xp-xp0) ;# urad^2
sig_xxp =mysigma(x-x0,xp-xp0)   ;# um urad

sig_yy  =mysigma(y-y0,y-y0)     ;# um^2    
sig_ypyp=mysigma(yp-yp0,yp-yp0) ;# urad^2
sig_yyp =mysigma(y-y0,yp-yp0)   ;# um urad

sig_zz=mysigma(z-z0,z-z0)       ;# um^2
sig_tt=mysigma(t-t0,t-t0)       ;# fs^2
sig_ww=mysigma(w-w0,w-w0)       ;# GeV^2
sig_zw=mysigma(z-z0,w-w0)       ;# um GeV
sig_tw=mysigma(t-t0,w-w0)       ;# fs GeV

emt_x =sqrt( sig_xx * sig_xpxp - sig_xxp * sig_xxp )*1e-6 ;# mm mrad
emt_y =sqrt( sig_yy * sig_ypyp - sig_yyp * sig_yyp )*1e-6 ;# mm mrad    
emt_z =sqrt( sig_zz * sig_ww - sig_zw * sig_zw )*1e3       ;# mm MeV
emt_t =sqrt( sig_tt * sig_ww - sig_tw * sig_tw )*1e3     ;# ps keV

emt_xn="{0:.3f}".format(emt_x*gmbt)             ;# mm mrad
emt_yn="{0:.3f}".format(emt_y*gmbt)             ;# mm mrad
emt_z ="{0:.3f}".format(emt_z)                  ;# mm MeV
emt_t ="{0:.3f}".format(emt_t)                  ;# ps keV
e0    ="{0:.3f}".format(w0)                     ;# GeV
sig_x ="{0:.3f}".format(sqrt(sig_xx))           ;# um
sig_y ="{0:.3f}".format(sqrt(sig_yy))           ;# um
sig_z ="{0:.3f}".format(sqrt(sig_zz))           ;# um
sig_t ="{0:.3f}".format(sqrt(sig_tt))           ;# fd
sig_w ="{0:.3f}".format(sqrt(sig_ww)*1000)      ;# MeV
dE    ="{0:.3f}".format(sqrt(sig_ww)*100/w0)    ;# %

#print(emt_xn,emt_yn )
#print(emt_z,emt_t)
#print(e0)
#print(sig_x,sig_y )
#print(sig_z,sig_t)
#print(sig_w,dE)
#print(size,qpp)
#exit()

nullfmt   = NullFormatter()         

#definitions for the axes 
axspace_0=0.1
axspace=0.12
pltspace=0.005
pltwidth=pltheight= 0.25
histheight=0.13

rect_scatter_zw = [axspace_0, axspace_0, pltwidth, pltheight]
rect_hist_z     = [axspace_0, axspace_0+pltheight+pltspace, pltwidth, histheight]
rect_hist_w     = [axspace_0+pltwidth+pltspace,axspace_0, histheight, pltheight]

rect_scatter_xy = [axspace_0, axspace_0+axspace+pltspace+pltspace+pltheight+histheight, pltwidth, pltheight]
rect_hist_x = [axspace_0, axspace_0+axspace+pltspace+pltspace+pltheight+histheight+pltspace+pltheight, pltwidth, histheight]
rect_hist_y = [axspace_0+pltwidth+pltspace,axspace_0+axspace+pltspace+pltspace+pltheight+histheight, histheight, pltheight]

rect_scatter_xxp = [axspace_0+pltwidth+pltspace+histheight+pltspace+axspace_0, axspace_0+axspace+pltspace+pltspace+pltheight+histheight, pltwidth, pltheight]
rect_hist_x2 = [axspace_0+pltwidth+pltspace+histheight+pltspace+axspace_0, axspace_0+axspace+pltspace+pltspace+pltheight+histheight+pltspace+pltheight, pltwidth, histheight]
rect_hist_xp = [axspace_0+pltwidth+pltspace+histheight+pltspace+axspace_0+pltwidth+pltspace,axspace_0+axspace+pltspace+pltspace+pltheight+histheight, histheight, pltheight]



table_pos= [axspace_0+pltwidth+pltspace+histheight+pltspace+axspace/2, 0, pltwidth+histheight, pltheight+histheight]


plt.figure(1, figsize=(10,9))

xyScatter = plt.axes(rect_scatter_xy)
xyHistx = plt.axes(rect_hist_x)
xyHisty = plt.axes(rect_hist_y)

xxpScatter = plt.axes(rect_scatter_xxp)
xxpHistx = plt.axes(rect_hist_x2)
xxpHistxp = plt.axes(rect_hist_xp)

zwScatter = plt.axes(rect_scatter_zw)
zwHistz = plt.axes(rect_hist_z)
zwHistw = plt.axes(rect_hist_w)

tableplt=plt.axes(table_pos,frameon=False)
tableplt.axes.get_yaxis().set_visible(False)
tableplt.axes.get_xaxis().set_visible(False)

# no labels
xyHistx.xaxis.set_major_formatter(nullfmt)
xyHisty.yaxis.set_major_formatter(nullfmt)
xyHistx.yaxis.set_major_formatter(nullfmt)
xyHisty.xaxis.set_major_formatter(nullfmt)

xxpHistx.xaxis.set_major_formatter(nullfmt)
xxpHistxp.yaxis.set_major_formatter(nullfmt)
xxpHistx.yaxis.set_major_formatter(nullfmt)
xxpHistxp.xaxis.set_major_formatter(nullfmt)

zwHistz.xaxis.set_major_formatter(nullfmt)
zwHistw.yaxis.set_major_formatter(nullfmt)
#zwHistz.yaxis.set_major_formatter(nullfmt)
zwHistw.xaxis.set_major_formatter(nullfmt)

tableplt.xaxis.set_major_formatter(nullfmt)
tableplt.yaxis.set_major_formatter(nullfmt)

# the scatter plots:
scatterbin=150
xyScatter.hexbin(x, y,cmap=cm.jet,mincnt=1,bins=scatterbin)
xxpScatter.hexbin(x, xp,cmap=cm.jet,mincnt=1,bins=scatterbin)
#zwScatter.hexbin(z, w,cmap=cm.jet,mincnt=1,bins=scatterbin)
zwScatter.hexbin(t, w,cmap=cm.jet,mincnt=1,bins=scatterbin)




xyScatter.set_xlabel(r'$\mathit{x}$ ($\mu$m)', fontsize=16)
xyScatter.set_ylabel(r'$\mathit{y}$ ($\mu$m)', fontsize=16)
xxpScatter.set_xlabel(r'$\mathit{x}$ ($\mu$m)', fontsize=16)
xxpScatter.set_ylabel(r'$\mathit{x^\prime}$ ~($\mu$rad)', fontsize=16)
#zwScatter.set_xlabel(r'$\mathit{z}$ ($\mu$m)', fontsize=16)

zwScatter.set_xlabel(r'$\mathit{z}$ (fs)', fontsize=16)
zwScatter.set_ylabel(r'$\mathit{E}$ (GeV)', fontsize=16)

binstep=100

binwidthx=(xmax-xmin)/binstep
xmax = np.max( [np.max(np.fabs(x))] )
limx = ( int(xmax/binwidthx) + 1) * binwidthx

binwidthy=(ymax-ymin)/binstep
ymax = np.max( [np.max(np.fabs(y))] )
limy = ( int(ymax/binwidthy) + 1) * binwidthy

binwidthxp=(xpmax-xpmin)/binstep
xpmax = np.max( [np.max(np.fabs(xp))] )
limxp = ( int(xpmax/binwidthxp) + 1) * binwidthxp

#binwidthz=(zmax-zmin)/binstep
#zmax = np.max( [np.max(np.fabs(z))] )
#limz = ( int(zmax/binwidthz) + 1) * binwidthz

binwidtht=(tmax-tmin)/binstep
tmax = np.max( [np.max(np.fabs(t))] )
limt = ( int(tmax/binwidtht) + 1) * binwidtht

binwidthw=(wmax-wmin)/binstep
wmax = np.max( [np.max(np.fabs(w))] )
limw = ( int(wmax/binwidthw) + 1) * binwidthw

xyScatter.set_xlim( (xmin, xmax) )
xyScatter.set_ylim( (ymin, ymax) )

xxpScatter.set_xlim( (xmin, xmax) )
xxpScatter.set_ylim( (xpmin, xpmax) )

#zwScatter.set_xlim( (zmin, zmax) )
zwScatter.set_xlim( (tmin, tmax) )
zwScatter.set_ylim( (wmin, wmax) )

binsx = np.arange(-limx, limx + binwidthx, binwidthx)
binsy = np.arange(-limy, limy + binwidthy, binwidthy)
binsxp = np.arange(-limxp, limxp + binwidthxp, binwidthxp)
#binsz = np.arange(-limz, limz + binwidthz, binwidthz)
binst = np.arange(-limt, limt + binwidtht, binwidtht)
binsw = np.arange(-limw, limw + binwidthw, binwidthw)

lww=1.
xyHistx.hist(x, bins=binsx, histtype='step',linewidth=lww, color='blue')
xyHisty.hist(y, bins=binsy, orientation='horizontal', histtype='step',linewidth=lww, color='blue')

xxpHistx.hist(x, bins=binsx, histtype='step',linewidth=lww, color='blue')
xxpHistxp.hist(xp, bins=binsxp, orientation='horizontal', histtype='step',linewidth=lww, color='blue')

ampt,binst=np.histogram(t, bins=binst)

binstr=[]
for i in range(len(binst)-1):
    tmp=(binst[i]+binst[i+1])/2
    binstr.append(tmp)

#print(ampz)
dt=binstr[5]-binstr[4]   ;#fs

amptcur=ampt*qpp/(dt*1e-15)/1000;
peakcur=np.max(amptcur)
Ip    ="{0:.3f}".format(peakcur)    ;# %

#if np.max(amptcur) > 0.1 

zwHistz.set_ylabel(r'$\mathit{I}$ (kA)', fontsize=16)

#yticks(np.arange(0, 1, step=0.2))

#zwHistz.hist(z, bins=binsz, histtype='step',linewidth=lww, color='blue')
zwHistz.step(binstr, amptcur , where='mid',linewidth=lww, color='blue')
zwHistw.hist(w, bins=binsw, orientation='horizontal', histtype='step',linewidth=lww, color='blue')

xyHistx.set_xlim( xyScatter.get_xlim() )
xyHisty.set_ylim( xyScatter.get_ylim() )

xxpHistx.set_xlim( xxpScatter.get_xlim() )
xxpHistxp.set_ylim( xxpScatter.get_ylim() )

zwHistz.set_xlim( zwScatter.get_xlim() )
zwHistw.set_ylim( zwScatter.get_ylim() )



table = ( r'\begin{tabular}{l c c }'
        r'$\mathbf{Parameter}$ & $\mathbf{Unit}$ & $\mathbf{Value}$ \\[-2pt]'
        r'$E$  & GeV & %s \\[-2pt]'
        r'$\varepsilon_{n,x}$  & $\mu$m.rad & %s \\[-2pt]'
        r'$\varepsilon_{n,y}$  & $\mu$m.rad & %s \\[-2pt]'
        r'$\varepsilon_{z}$  & keV.ps & %s \\[-2pt]'
        r'$I_{max}$  & kA & %s \\[-2pt]'
        r'$\sigma_{x}$  & $\mu$m & %s  \\[-2pt] '
        r'$\sigma_{y}$  & $\mu$m & %s \\[-2pt] '
        r'$\sigma_{z}$  & $\mu$m & %s \\[-2pt] '
        r'$\sigma_{t}$  & fs & %s \\[-2pt] '
        r'$\sigma_{E}$  & MeV & %s \\[-2pt] '
        r'$\Delta E/E$  & \%% & %s '
        r'\end{tabular}' % (e0, emt_xn, emt_yn, emt_t,Ip, sig_x, sig_y, sig_z, sig_t,sig_w, dE) )
plt.text(0.1,0.05,table,size=16, fontweight='bold')

fig1 = plt.gcf()
#plt.suptitle('x (mm) - x\' (mrad)',fontsize=24,horizontalalignment = 'center')
plt.show()
plt.draw()
fig1.savefig(plot_file,dpi=150)




exit()
