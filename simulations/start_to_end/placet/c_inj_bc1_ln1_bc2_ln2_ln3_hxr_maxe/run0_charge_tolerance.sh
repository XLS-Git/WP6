rm -f rel_charge.dat
for rel_charge in 96 96 98 100 102 104 ; do

    placet run0_charge_tolerance.tcl rel_charge $rel_charge 
    
done

octave <<EOF
    T = load('rel_charge.dat');	
    REF = find(T(:,1) == 100)
    	
    DI_I_percent = T(REF,13)  / T(end,13) * 100
    DT_T_percent_fs = T(REF,5) / (0.299792458)
    DE_E_percent = T(end,9)  / T(REF,9) * 100
    dE_E_percent = T(end,10)  / T(REF,10) * 100
    deproj_eprog_percent = sqrt(prod(T(end,11:12))  / prod(T(REF,11:12))) * 100

    T(:,2:4) = T(:,2:4) ./ T(REF,2:4);
    T(:,5) = T(:,5) / 0.299792458;
    T(:,6:end) = T(:,6:end) ./ T(REF,6:end);

    save -text rel_charge_rel.dat
EOF

gnuplot plot_charge_tolerance.p
