

source load_files.tcl

set beam0_indist cband_out_4ps_0.18mm_100k_120MeV_4.dat
set beamlinename xls_linac_hxr
set latfile xls_lattice.tcl

set charge [expr 75e-12/1.6e-19]

array set BeamDefine  [bunch_parameters $script_dir/$beam0_indist]
array set BeamDefine "name beam0 filename $beam0_indist n_slice 201 charge $charge"
set BeamDefine(n_macro) [expr int($BeamDefine(npart)/$BeamDefine(n_slice))]
lassign "$BeamDefine(beta_x) $BeamDefine(beta_y) $BeamDefine(alpha_x) $BeamDefine(alpha_y)" btx bty alx aly

set e0 $BeamDefine(energy)

puts [array get BeamDefine]

set LN0_XCA_VLT 0.0
set LN0_XCA_PHS 0.0

set LN0_CCA_GRD 15e-3
set LN0_CCA_PHS 25.0

set LN0_KCA_GRD [expr 8e-3/$kband_str(active_l)]
set LN0_KCA_PHS [expr 180-5]

set  BC1_XCA_VLT 0.0
set  BC1_XCA_PHS 0.0

set LN1_XCA_GRD 65e-3
set LN1_XCA_PHS 33.0

set LN2_XCA_GRD 65e-3
set LN2_XCA_PHS -10.0  

set LN2_XCA_VLT 0.0
set LN2_XCA_PHS 0.0

set LN2_SCA_VLT 0.0
set LN2_SCA_PHS 0.0

set LN3_XCA_GRD 65e-3
set LN3_XCA_PHS $LN2_XCA_PHS

set LN3_SCA_VLT 0.0
set LN3_SCA_PHS 0.0

set LN4_XCA_GRD 65e-3
set LN4_XCA_PHS -10.0  

set b0_in beam0.in
set b0_ou beam0.ou





# BeamlineNew
Girder
source $script_dir/$latfile

BeamlineSet -name $beamlinename
BeamlineUse -name $beamlinename

source ../../../../../beamline_database/placet/save_elements_table.tcl 

save_elements_table $beamlinename
