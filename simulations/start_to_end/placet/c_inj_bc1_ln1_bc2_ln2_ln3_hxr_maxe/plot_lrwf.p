set term pdfcairo font "Arial,14" lw 2

set xlabel 'Kick [V/pC/m/mm]'
set ylabel 'Amplification factor'

set out 'plot_lrwf.pdf'

# X-band
# kick is in keV
# kV / 0.916475473701178 m / 75 pC / mm = 14.54848898409336 V/pC/m/mm

# C-band
# kick is in keV
# kV / 1.899604 m / 75 pC / mm = 7.019006768428226 V/pC/m/mm

# Ka-band
# kick is in keV
# kV / 0.3054918 m / 75 pC / mm = 43.64547046216407 V/pC/m/mm


set key left

plot [0:60]\
 1.15 ti 'threshold' w l lw 1 lt 8,\
 'outfiles_lrwf/xls_linac_hxr_xlrwf.act' u ($1*14.54848898409336):2 ti 'X-band - X axis' w l lw 2,\
 'outfiles_lrwf/xls_linac_hxr_ylrwf.act' u ($1*14.54848898409336):2 ti 'X-band - Y axis'  w l lw 2
# 'outfiles_lrwf_cband/xls_linac_hxr_xlrwf.act' u ($1*7.019006768428226):2 ti 'C-band - X axis' w l lw 2,\
# 'outfiles_lrwf_cband/xls_linac_hxr_ylrwf.act' u ($1*7.019006768428226):2 ti 'C-band - Y axis'  w l lw 2,\
# 'outfiles_lrwf_kaband/xls_linac_hxr_xlrwf.act' u ($1*43.64547046216407):2 ti 'Ka-band - X axis' w l lw 2,\
# 'outfiles_lrwf_kaband/xls_linac_hxr_ylrwf.act' u ($1*43.64547046216407):2 ti 'Ka-band - Y axis'  w l lw 2, \

