set tranverse_kick 1 ; # keV/c


source load_files.tcl

set beam0_indist cband_out_4ps_0.18mm_100k_120MeV_4.dat
set beamlinename xls_linac_hxr
set latfile xls_lattice.tcl

set charge [expr 75e-12/1.6e-19]

array set BeamDefine  [bunch_parameters $script_dir/$beam0_indist]
array set BeamDefine "name beam0 filename $beam0_indist n_slice 201 charge $charge"
set BeamDefine(n_macro) [expr int($BeamDefine(npart)/$BeamDefine(n_slice))]
lassign "$BeamDefine(beta_x) $BeamDefine(beta_y) $BeamDefine(alpha_x) $BeamDefine(alpha_y)" btx bty alx aly

set e0 $BeamDefine(energy)

puts [array get BeamDefine]

set LN0_XCA_VLT 0.0
set LN0_XCA_PHS 0.0

set LN0_CCA_GRD 15e-3
set LN0_CCA_PHS 25.0

set LN0_KCA_GRD [expr 8e-3/$kband_str(active_l)]
set LN0_KCA_PHS [expr 180-5]

set  BC1_XCA_VLT 0.0
set  BC1_XCA_PHS 0.0

set LN1_XCA_GRD 65e-3
set LN1_XCA_PHS 33.0

set LN2_XCA_GRD 65e-3
set LN2_XCA_PHS -10.0  

set LN2_XCA_VLT 0.0
set LN2_XCA_PHS 0.0

set LN2_SCA_VLT 0.0
set LN2_SCA_PHS 0.0

set LN3_XCA_GRD 65e-3
set LN3_XCA_PHS $LN2_XCA_PHS

set LN3_SCA_VLT 0.0
set LN3_SCA_PHS 0.0

set LN4_XCA_GRD 65e-3
set LN4_XCA_PHS -10.0  

set b0_in beam0.in
set b0_ou beam0.ou





# BeamlineNew
Girder
source $script_dir/$latfile

BeamlineSet -name $beamlinename
# BeamlineUse -name $beamlinename

make_particle_beam_read BeamDefine $script_dir/$BeamDefine(filename)

Octave {
    global beaminxdx = 1;
    global Beams = {};
}

proc octave_save {} {
    Octave {
      Beams{beaminxdx++} = placet_get_beam();
    }
} 


Octave {
# apply wakes for all structures
    ALLCAVITs = placet_get_number_list("$beamlinename", "cavity");
    placet_element_set_attribute("$beamlinename", ALLCAVITs, "six_dim", true);
    
    LN0XDCs = placet_get_name_number_list("$beamlinename", "LN0_XCA0_DI");
    placet_element_set_attribute("$beamlinename", LN0XDCs, "short_range_wake",  "Xdband_SR_W");
    placet_element_set_attribute("$beamlinename", LN0XDCs, "lambda", $xdband_str(lambda));
    
    LN0CCs = placet_get_name_number_list("$beamlinename", "LN0_CCA0");
    placet_element_set_attribute("$beamlinename", LN0CCs, "short_range_wake",  "Cband_SR_W");
    placet_element_set_attribute("$beamlinename", LN0CCs, "lambda", $cband_str(lambda));
     
    LN0KCs = placet_get_name_number_list("$beamlinename", "LN0_KCA0");
    placet_element_set_attribute("$beamlinename", LN0KCs, "short_range_wake",  "Kband_SR_W");
    placet_element_set_attribute("$beamlinename", LN0KCs, "lambda", $kband_str(lambda));
   
    BC1XCs = placet_get_name_number_list("$beamlinename", "BC1_XCA0");
    placet_element_set_attribute("$beamlinename", BC1XCs, "short_range_wake",  "Xdband_SR_W");
    placet_element_set_attribute("$beamlinename", BC1XCs, "lambda", $xdband_str(lambda));
    
    LN1XCs = placet_get_name_number_list("$beamlinename", "LN1_XCA0");
    placet_element_set_attribute("$beamlinename", LN1XCs, "short_range_wake",  "Xband_SR_W");
    placet_element_set_attribute("$beamlinename", LN1XCs, "lambda", $xband_str(lambda));
    
    LN2XCs = placet_get_name_number_list("$beamlinename", "LN2_XCA0");
    placet_element_set_attribute("$beamlinename", LN2XCs, "short_range_wake",  "Xband_SR_W");
    placet_element_set_attribute("$beamlinename", LN2XCs, "lambda", $xband_str(lambda));
    
    LN2XDCs = placet_get_name_number_list("$beamlinename", "LN2_XCA0_DI");
    placet_element_set_attribute("$beamlinename", LN2XDCs, "short_range_wake",  "Xdband_SR_W");
    placet_element_set_attribute("$beamlinename", LN2XDCs, "lambda", $xdband_str(lambda));    
    
    LN2SCs = placet_get_name_number_list("$beamlinename", "LN2_SCA0");
    placet_element_set_attribute("$beamlinename", LN2SCs, "short_range_wake",  "Sdband_SR_W");
    placet_element_set_attribute("$beamlinename", LN2SCs, "lambda", $sdband_str(lambda));
    
    LN3XCs = placet_get_name_number_list("$beamlinename", "LN3_XCA0");
    placet_element_set_attribute("$beamlinename", LN3XCs, "short_range_wake",  "Xband_SR_W");
    placet_element_set_attribute("$beamlinename", LN3XCs, "lambda", $xband_str(lambda));
    
    LN3SCs = placet_get_name_number_list("$beamlinename", "LN3_SCA0");
    placet_element_set_attribute("$beamlinename", LN3SCs, "short_range_wake",  "Sdband_SR_W");
    placet_element_set_attribute("$beamlinename", LN3SCs, "lambda", $sdband_str(lambda));
#     
 
    
#  6d tracking in bunch compression
   SIs = placet_get_number_list("$beamlinename", "sbend");
   placet_element_set_attribute("$beamlinename", SIs, "six_dim", true);
   placet_element_set_attribute("$beamlinename", SIs, "csr", true);
   placet_element_set_attribute("$beamlinename", SIs, "csr_charge", 75e-12);
   placet_element_set_attribute("$beamlinename", SIs, "csr_nbins", 50);
   placet_element_set_attribute("$beamlinename", SIs, "csr_nsectors", 10);
   placet_element_set_attribute("$beamlinename", SIs, "csr_filterorder", 1);
   placet_element_set_attribute("$beamlinename", SIs, "csr_nhalffilter", 2);

   QDs = placet_get_number_list("$beamlinename", "quadrupole");
   placet_element_set_attribute("$beamlinename", QDs, "six_dim", true);

#  set reference energy for first BC
   BC1Ds = placet_get_name_number_list("$beamlinename", "BC1_DP_DIP*");
   placet_element_set_attribute("$beamlinename", BC1Ds, "e0", 0.271562-0.2e-4);
   
#  set reference energy for second BC
   BC2Ds = placet_get_name_number_list("$beamlinename", "BC2_DP_DIP*");
   placet_element_set_attribute("$beamlinename", BC2Ds, "e0", 1.069679);
   
   #  set reference energy for second BC
   TMCDs = placet_get_name_number_list("$beamlinename", "TMC_DP_DIP*");
   placet_element_set_attribute("$beamlinename", TMCDs, "e0", 5.520727);
}

## Consistent dipole kick

Octave {
    B0 = placet_get_beam("beam0");
    stdX  = std(B0(:,2));
    stdXp = std(B0(:,5));
    A0_ = stdX * stdXp * pi / 100 * mean(B0(:,1)); % initial area
    T = [];
}

for {set tranverse_kick 0} {$tranverse_kick <= 10} {set tranverse_kick [ expr $tranverse_kick + 1 ] } { 
    Octave {
	AIn = placet_get_name_number_list("$beamlinename", "LN0_CCA0*");
	AIt = placet_get_number_list("$beamlinename", "cavity");
	AI = intersect(AIn,AIt);

	Angles = linspace(0, 360, 32)(1:end-1);
	A0 = [];
	A1 = [];
	for angle = Angles
	  offsetX  = stdX  * 0.1 * cosd(angle);
	  offsetXp = stdXp * 0.1 * sind(angle);
	  B = B0;
	  B(:,2) = B(:,2) + offsetX;
	  B(:,5) = B(:,5) + offsetXp;
	  placet_set_beam("beam0", B);
	  # first bunch
	  placet_element_set_attribute("$beamlinename", AI, 'dipole_kick_x', 0.0);
	  [E_,B_] = placet_test_no_correction("$beamlinename", "beam0", "None", "%s %E %dE %ex %ey %sz");
	  A0 = [ A0 ; mean(B_(:,2)) mean(B_(:,5)) * mean(B_(:,1)) ];
	  DX = placet_element_get_attribute("$beamlinename", AI, "bpm_reading_x") / 1e3; % mm
	  # second bunch
	  KX = $tranverse_kick * DX;
	  placet_element_set_attribute("$beamlinename", AI, 'dipole_kick_x', KX);
	  [E_,B_] = placet_test_no_correction("$beamlinename", "beam0", "None", "%s %E %dE %ex %ey %sz");
	  A1 = [ A1 ; mean(B_(:,2)) mean(B_(:,5)) * mean(B_(:,1)) ];
	end
	placet_element_set_attribute("$beamlinename", AI, 'dipole_kick_x', 0.0);
	T = [ T ; $tranverse_kick polyarea(A1(:,1), A1(:,2)) / polyarea(A0(:,1), A0(:,2)) polyarea(A0(:,1), A0(:,2)) / A0_ ];
    }
}

Octave {
    save -text ${beamlinename}_xlrwf.act T
    placet_set_beam("beam0", B0);
}

Octave {
    B0 = placet_get_beam("beam0");
    stdY  = std(B0(:,3));
    stdYp = std(B0(:,6));
    A0_ = stdY * stdYp * pi / 100 * mean(B0(:,1)); % initial area
    T = [];
}

for {set tranverse_kick 0} {$tranverse_kick <= 10} {set tranverse_kick [ expr $tranverse_kick + 1 ] } { 
    Octave {
	AIn = placet_get_name_number_list("$beamlinename", "LN0_CCA0*");
	AIt = placet_get_number_list("$beamlinename", "cavity");
	AI = intersect(AIn,AIt);

	Angles = linspace(0, 360, 32)(1:end-1);
	A0 = [];
	A1 = [];
	for angle = Angles
	  offsetY  = stdY  * 0.1 * cosd(angle);
	  offsetYp = stdYp * 0.1 * sind(angle);
	  B = B0;
	  B(:,3) = B(:,3) + offsetY;
	  B(:,6) = B(:,6) + offsetYp;
	  placet_set_beam("beam0", B);
	  # first bunch
	  placet_element_set_attribute("$beamlinename", AI, 'dipole_kick_y', 0.0);
	  [E_,B_] = placet_test_no_correction("$beamlinename", "beam0", "None", "%s %E %dE %ex %ey %sz");
	  A0 = [ A0 ; mean(B_(:,3)) mean(B_(:,6)) * mean(B_(:,1)) ];
	  DY = placet_element_get_attribute("$beamlinename", AI, "bpm_reading_y") / 1e3; % mm
	  # second bunch
	  KY = $tranverse_kick * DY;
	  placet_element_set_attribute("$beamlinename", AI, 'dipole_kick_y', KY);
	  [E_,B_] = placet_test_no_correction("$beamlinename", "beam0", "None", "%s %E %dE %ex %ey %sz");
	  A1 = [ A1 ; mean(B_(:,3)) mean(B_(:,6)) * mean(B_(:,1)) ];
	end
	placet_element_set_attribute("$beamlinename", AI, 'dipole_kick_y', 0.0);
	T = [ T ; $tranverse_kick polyarea(A1(:,1), A1(:,2)) / polyarea(A0(:,1), A0(:,2)) polyarea(A0(:,1), A0(:,2)) / A0_ ];
    }
}

Octave {
    save -text ${beamlinename}_ylrwf.act T
    placet_set_beam("beam0", B0);
}
