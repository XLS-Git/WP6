C = load('outfiles_lrwf_cband/xls_linac_hxr_xlrwf.act').T;
X = load('outfiles_lrwf/xls_linac_hxr_xlrwf.act').T;

% X-band
% kick is in keV
% kV / 0.916475473701178 m / 75 pC / mm = 14.54848898409336 V/pC/m/mm

% C-band
% kick is in keV
% kV / 1.899604 m / 75 pC / mm = 7.019006768428226 V/pC/m/mm

C(:,1) *= 7.019006768428226;
X(:,1) *= 14.54848898409336;

PC = polyfit(C(:,1), C(:,2), 2);
PX = polyfit(X(:,1), X(:,2), 2);

K = linspace(0, 60, 100);

figure(1)
clf ; hold on
plot([ 0 60 ], [ 1.15 1.15 ], '-', 'linewidth', 1, 'color', [ 0 0 0 ]);
plot(K, polyval(PC, K), 'linewidth', 2, 'color', [ 0 0 1 ]);
plot(K, polyval(PX, K), 'linewidth', 2, 'color', [ 1 0 0 ]);
legend('threshold', 'C-band', 'X-band');
xlabel('Kick [V/pC/m/mm]');
ylabel('Amplification factor');

print -dpng plot_lrwf.png
