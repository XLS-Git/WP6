
set term qt enhanced size 1500, 800 font 'Times , 18' 


set grid lt 0
# set multiplot layout 1, 2
set xlabel ' Distance (m)' font 'Times , 14' offset 0,0.5
set key top center

set ytics nomirror
set y2tics
set tics out
set autoscale  y
set autoscale y2


set y2label 'E (MeV)' font 'Times , 14'  
set ylabel '{/Symbol b}_{x,y} (m)' font 'Times , 14' 
set yrange [0:*]
set y2range [0:*]


pl './outfiles/xls_linac_hxr.twi' u 1:2 w l lw 3 lc 'blue' ti 'btx',  './outfiles/xls_linac_hxr.twi' u 1:3 w l lw 3 lc 'red' ti 'bty', \
        './outfiles/beamlinename.ener' u 1:2 w l lw 3 lc 'black' dt 2 axes x1y2 ti 'E'
# 
# set plot_twi  "'$beamlinename.twi' u 1:2 w l ti ' btx', '$beamlinename.twi' u 1:3 w l  ti 'bty'"
# 
# exec echo " set term x11 size 1200,600 \n \
#             set xlabel 'distance (m)' \n set ylabel 'beta x,y (m)' \n set key  spacing 0.9 \n set xrange \[0:*\] \n \
#             plot $plot_twi  " | gnuplot -persist
