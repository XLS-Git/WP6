
SplineCreate   "cband_Wt" -file $cband_str(wake_tran_file)
SplineCreate   "cband_Wl" -file $cband_str(wake_long_file)
ShortRangeWake "Cband_SR_W" -type 2 -wx "cband_Wt" -wy "cband_Wt" -wz "cband_Wl"

SplineCreate   "xband_Wt" -file $xband_str(wake_tran_file)
SplineCreate   "xband_Wl" -file $xband_str(wake_long_file)
ShortRangeWake "Xband_SR_W" -type 2 -wx "xband_Wt" -wy "xband_Wt" -wz "xband_Wl"

SplineCreate   "kband_Wt" -file $kband_str(wake_tran_file)
SplineCreate   "kband_Wl" -file $kband_str(wake_long_file)
ShortRangeWake "Kband_SR_W" -type 2 -wx "kband_Wt" -wy "kband_Wt" -wz "kband_Wl"

SplineCreate   "sdband_Wt" -file $sdband_str(wake_tran_file)
SplineCreate   "sdband_Wl" -file $sdband_str(wake_long_file)
ShortRangeWake "Sdband_SR_W" -type 2 -wx "sdband_Wt" -wy "sdband_Wt" -wz "sdband_Wl"


SplineCreate   "xdband_Wt" -file $xdband_str(wake_tran_file)
SplineCreate   "xdband_Wl" -file $xdband_str(wake_long_file)
ShortRangeWake "Xdband_SR_W" -type 2 -wx "xdband_Wt" -wy "xdband_Wt" -wz "xdband_Wl"

# 
# 
# SplineCreate   "acc_cav_Wt_z" -file "acc_cav_Wt_zero.dat"
# SplineCreate   "acc_cav_Wl_z" -file "acc_cav_Wl_zero.dat"
# ShortRangeWake "acc_cav_SR_W_zero" -type 2 -wx "acc_cav_Wt_z" -wy "acc_cav_Wt_z" -wz "acc_cav_Wl_z"


#
# use this list to create long range fields 
# single bunch no effect
 WakeSet Wake_acc_cav_long_range {1. 1 0 0 }
#
# define structure
InjectorCavityDefine -name 0 -wakelong Wake_acc_cav_long_range 
