set TIME_start [clock clicks -milliseconds] 

if {[file exist outfiles]} {
	exec rm -rf outfiles
	exec mkdir -p outfiles
} else {
	exec mkdir -p outfiles
}

set  indx 0 
cd   outfiles
set  script_dir "../files"


source $script_dir/structure_pars.tcl
source $script_dir/srw_calculation.tcl
source ../parameters_v0.tcl


puts $ln0_kvolt
source $script_dir/create_bunch.tcl
source $script_dir/match_aug_19.tcl
source $script_dir/modify_output2.tcl

# 
set  track_sxrbpl 1
set  track_hbp1 1
set  track_hbp2 1
set  track_hbp3 1
# # set  track_hxrl 1
# # 
set  track_bunch 1


set csr_flag 1
set isr_flag 1

# exit



set e0 [expr (sqrt($p0*$p0 + 1.0)-1)*$mc2]

puts "\nreference momentum $p0"
puts "reference energy $e0\n"

# array set BeamDefine [array get match_L1]
array set BeamDefine "sigma_z [expr $st*$clight] n_slice 201 emitt_x $emtx emitt_y $emty energy $e0 p0 $p0"
array set BeamDefine "e_spread [expr $de*100] n_macro 201 type uniform charge 75e-12 filename $indist_name"
array set BeamDefine "beta_x $btx beta_y $bty alpha_x $alx alpha_y $aly "
set BeamDefine(npart) [expr 100000]

# #target twiss parameters for septum magnet
# lassign {1.0 19.980 0.565 3.495} btx bty alpx alpy

# lassign {0.8582676 18.239825 2.42235 5.131099} btxs btys alpxs alpys
lassign {1.061496 12.7680276 1.5177  4.44889} btxs btys alpxs alpys
# lassign { 1.11460 9.74763 0.82368 3.27529} btxs btys alpxs alpys
# lassign {2.1 13.75 1.55 4.45} btxs btys alpxs alpys
lassign {1.19443817  18.69324075 3.7451895 3.47167929} btxs2 btys2 alpxs2 alpys2

set ksext 800
 
puts [array get BeamDefine]

after 100


# exit

# the bunch is created with defined parameters
exec cp $script_dir/$BeamDefine(filename) .

set indx 9
source $script_dir/srw_calculation.tcl
source $script_dir/create_beamline.tcl
source $script_dir/create_elegant_file.tcl


set ncpu [expr int([exec nproc]-8)]
puts "number of cores to be used in calculation $ncpu"


puts $watches
puts $watches2




foreach flem $elegant_match flet $elegant_track {
    
    puts $flem
    puts $flet
    exec elegant $flem >/dev/tty
#     after 1000
    exec mpirun -host localhost --oversubscribe  -np $ncpu Pelegant $flet >/dev/tty
#     after 1000

}

print_ellapsed_time $TIME_start



foreach fl $watches {
    plotdist $fl
}

exec sddsplot [lindex $watches 1] -col=x,y -gra=dot,var -leg=parameter=Description  [lindex $watches 2] -col=x,y -gra=dot,var -leg=parameter=Description &

# exec sddsplot -gra=dot,var -leg=parameter=Description [lindex $watches 1] -col=dt,x   [lindex $watches 2] -col=dt,x  &
# # 
set form "format=\%.8e"
lassign [eval exec sddsanalyzebeam [lindex $watches 1] -pipe=out -nowarning | sddsprintout -pipe \
	  -col=betac?,$form -col=alphac?,$form  -nolabel -notitle] btx0 bty0 alpx0 alpy0 
	  
lassign [eval exec sddsanalyzebeam [lindex $watches 2] -pipe=out -nowarning | sddsprintout -pipe \
	  -col=betac?,$form -col=alphac?,$form  -nolabel -notitle] btx1 bty1 alpx1 alpy1
# 

# 
set alxav [expr  ($alpx0 + $alpx1)*.5]
set alyav [expr  ($alpy0 + $alpy1)*.5]
set btxav [expr  ($btx0 - $btx1)*.5]
set btyav [expr  ($bty0 - $bty1)*.5]

# puts "{$btxav $btyav $alxav $alyav}"


set alxav [expr  $alpx0+ $alxav*.5]
set alyav [expr  $alpy0+ $alyav*.5]
set btxav [expr  $btx0 + $btxav*.5]
set btyav [expr  $bty0 + $btyav*.5]

puts "{$btxav $btyav $alxav $alyav}"


puts "alxav [expr  ($alpx0 + $alpx1)]"
puts "alyav [expr  ($alpy0 + $alpy1)]"
puts "btxav [expr  ($btx0 - $btx1)]"
puts "btyav [expr  ($bty0 - $bty1)]"

# foreach fl $twissfiles {
#     plottwiss $fl
# }

foreach fl $sigmafiles {
    plotsigma $fl
}
# 
set flrfl [lindex  $flrfiles end end]
puts $flrfl
set magf [file rootname $flrfl].mag
set matf [file rootname $flrfl].mat    
set twif [file rootname $flrfl].twi
exec sddsplot $flrfl -col=s,X &
exec sddsplot $matf -col=s,R56 &
exec sddsplot $twif -col=s,psix &
# 
# set psi1 [eval exec sddsprocess $twif -pipe=out -match=column,ElementName=FITT_HBP_1 -nowarning | sdds2stream -pipe -col=psix ]
# set psi2 [eval exec sddsprocess $twif -pipe=out -match=column,ElementName=FITT_HBP_2 -nowarning | sdds2stream -pipe -col=psix ]
# puts [expr $psi2-$psi1]
# set psi1 [eval exec sddsprocess $twif -pipe=out -match=column,ElementName=FITT_HBP_0 -nowarning | sdds2stream -pipe -col=psix ]
# set psi2 [eval exec sddsprocess $twif -pipe=out -match=column,ElementName=FITT_HBP_PI -nowarning | sdds2stream -pipe -col=psix ]
# puts [expr $psi2-$psi1]

# set alx [exec sddsprintout  [lindex $sigmafiles end end] -col=alphaxBeam -notitle -nolabel ]
# set aly [exec sddsprintout  [lindex $sigmafiles end end] -col=alphayBeam -notitle -nolabel ]
# set btx [exec sddsprintout  [lindex $sigmafiles end end] -col=betaxBeam -notitle -nolabel ]
# set bty [exec sddsprintout  [lindex $sigmafiles end end] -col=betayBeam -notitle -nolabel ]
# set emtx [exec sddsprintout  [lindex $sigmafiles end end] -col=ecnx -notitle -nolabel ]
# set emtx2 [exec sddsprintout  [lindex $sigmafiles end end] -col=enx -notitle -nolabel ]
# set emtx3 [exec sddsprintout  [lindex $sigmafiles end end] -col=ecx -notitle -nolabel ]
# set emty [exec sddsprintout  [lindex $sigmafiles end end] -col=ecny -notitle -nolabel ]
# foreach ax $alx bx $btx ex $emtx ex2 $emtx2 ex3 $emtx3 {
#     puts "$ax $bx $ex $ex2 $ex3"
# }


# set flrz1 [lindex [exec sddsprintout  [lindex $flrfiles 1] -col=Z -notitle -nolabel ] end end]
# set flrz2 [lindex [exec sddsprintout  [lindex $flrfiles 2] -col=Z -notitle -nolabel ] end end]
# set flrx1 [lindex [exec sddsprintout  [lindex $flrfiles 1] -col=X -notitle -nolabel ] end end]
# set flrx2 [lindex [exec sddsprintout  [lindex $flrfiles 2] -col=X -notitle -nolabel ] end end]
# 
# # set pl1 [expr 
# 
# puts "$flrz1 [expr $flrz2*cos(2*$hbp_swi_angle_rd)]"
# puts "$flrx1 [expr $flrz2*sin(2*$hbp_swi_angle_rd)]"
# puts [expr $flrz2*sin(2*$hbp_swi_angle_rd)+2*$flrx1]
# puts [expr $flrz2*cos(2*$hbp_swi_angle_rd)+2*$flrz1]

# puts "t1 [expr ($flrz2*cos(2*$hbp_swi_angle_rd)+2*$flrz1)/3e8]"
# puts "t2 [expr (




if {[info exists track_bunch] } {
    
     exec mpiexec -host localhost --oversubscribe  -np $ncpu Pelegant $filename.all.trck.ele >&@stdout

foreach fl $watches2 {
    plotdist $fl 0
}
     
exec sddsplot -thick=2 -graph=line,vary,thick=2  \
              -column=s,(Sx,Sy) -yscale=id=1 $filename.all.$indx.sig -legend \
              -column=s,(enx,eny) -yscale=id=2 $filename.all.$indx.sig -legend \
              -column=s,Profile -overlay=xmode=norm,yfact=0.04 -graph=line,type=0 $filename.all.$indx.mag -nolabel \
              -device=gpng,onwhite -output=$filename.all.$indx.sig.png  & 
            
exec sddsplot -thick=2 -graph=line,vary,thick=2  \
              -column=s,(betaxBeam,betayBeam) -yscale=id=1 $filename.all.$indx.sig -legend \
              -column=s,(ecnx,ecny) -yscale=id=2 $filename.all.$indx.sig -legend \
              -column=s,Profile -overlay=xmode=norm,yfact=0.04 -graph=line,type=0 $filename.all.$indx.mag -nolabel \
              -device=gpng,onwhite -output=$filename.all.$indx.beta.png  & 
              
# modifytwiss $filename.all.$indx.twi


# # exec sddsplot   -thick=2 -graph=line,vary,thick=2 \
# #                 -column=s,beta? -yscale=id=1 $filename.all.$indx.twi -leg \
# #                 -column=s,E -yscale=id=2 $filename.all.$indx.twi -legend \
# #                 -column=s,Profile -overlay=xmode=norm,yfact=0.04 -graph=line,type=0 $filename.all.$indx.mag -nolabel &
# #                 -device=gpng,onwhite  -output=$filename.all.$indx.twi.png  
# # 		
# # exec sddsplot   -thick=2 -graph=line,vary,thick=2 \
# #                 -column=s,(etax) -yscale=id=1 $filename.all.$indx.twi -legend \
# #                 -column=s,(etaxp) -yscale=id=2 $filename.all.$indx.twi -legend \
# #                 -column=s,Profile -overlay=xmode=norm,yfact=0.04 -graph=line,type=0 $filename.all.$indx.mag -nolabel &
# #                 -device=gpng,onwhite -output=$filename.all.$indx.eta.png  &

                  
exec sddsplot   -thick=2 -graph=line,vary,thick=2 \
                -column=s,(X,Y) -yscale=id=1 $filename.all.$indx.flr -legend \
                -column=s,R56 -yscale=id=2 $filename.all.$indx.mtr -legend \
                -column=s,Profile -overlay=xmode=norm,yfact=0.04 -graph=line,type=0 $filename.all.$indx.mag -nolabel \
                -device=gpng,onwhite -output=$filename.all.$indx.r56.png  

}



print_ellapsed_time $TIME_start


exit



