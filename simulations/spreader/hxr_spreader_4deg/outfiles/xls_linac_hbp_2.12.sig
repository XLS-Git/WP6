SDDS1
!# little-endian
&description text="sigma matrix--input: xls_linac_hbp_2.track.ele  lattice: xls_linac.12.lte", contents="sigma matrix", &end
&parameter name=Step, description="Simulation step", type=long, &end
&parameter name=SVNVersion, description="SVN version number", type=string, fixed_value=unknown, &end
&column name=s, units=m, description=Distance, type=double,  &end
&column name=ElementName, description="Element name", format_string=%10s, type=string,  &end
&column name=ElementOccurence, description="Occurence of element", format_string=%6ld, type=long,  &end
&column name=ElementType, description="Element-type name", format_string=%10s, type=string,  &end
&column name=s1, symbol="$gs$r$b1$n", units=m, description="sqrt(<x*x>)", type=double,  &end
&column name=s12, symbol="$gs$r$b12$n", units=m, description="<x*xp'>", type=double,  &end
&column name=s13, symbol="$gs$r$b13$n", units="m$a2$n", description="<x*y>", type=double,  &end
&column name=s14, symbol="$gs$r$b14$n", units=m, description="<x*y'>", type=double,  &end
&column name=s15, symbol="$gs$r$b15$n", units="m$a2$n", description="<x*s>", type=double,  &end
&column name=s16, symbol="$gs$r$b16$n", units=m, description="<x*delta>", type=double,  &end
&column name=s17, symbol="$gs$r$b17$n", units="m*s", description="<x*t>", type=double,  &end
&column name=s2, symbol="$gs$r$b2$n", description="sqrt(<x'*x'>)", type=double,  &end
&column name=s23, symbol="$gs$r$b23$n", units=m, description="<x'*y>", type=double,  &end
&column name=s24, symbol="$gs$r$b24$n", description="<x'*y'>", type=double,  &end
&column name=s25, symbol="$gs$r$b25$n", units=m, description="<x'*s>", type=double,  &end
&column name=s26, symbol="$gs$r$b26$n", description="<x'*delta>", type=double,  &end
&column name=s27, symbol="$gs$r$b27$n", units=s, description="<x'*t>", type=double,  &end
&column name=s3, symbol="$gs$r$b3$n", units=m, description="sqrt(<y*y>)", type=double,  &end
&column name=s34, symbol="$gs$r$b34$n", units=m, description="<y*y'>", type=double,  &end
&column name=s35, symbol="$gs$r$b35$n", units="m$a2$n", description="<y*s>", type=double,  &end
&column name=s36, symbol="$gs$r$b36$n", units=m, description="<y*delta>", type=double,  &end
&column name=s37, symbol="$gs$r$b37$n", units="m*s", description="<y*t>", type=double,  &end
&column name=s4, symbol="$gs$r$b4$n", description="sqrt(<y'*y')>", type=double,  &end
&column name=s45, symbol="$gs$r$b45$n", units=m, description="<y'*s>", type=double,  &end
&column name=s46, symbol="$gs$r$b46$n", description="<y'*delta>", type=double,  &end
&column name=s47, symbol="$gs$r$b47$n", units=s, description="<y'*t>", type=double,  &end
&column name=s5, symbol="$gs$r$b5$n", units=m, description="sqrt(<s*s>)", type=double,  &end
&column name=s56, symbol="$gs$r$b56$n", units=m, description="<s*delta>", type=double,  &end
&column name=s57, symbol="$gs$r$b57$n", units="m*s", description="<s*t>", type=double,  &end
&column name=s6, symbol="$gs$r$b6$n", description="sqrt(<delta*delta>)", type=double,  &end
&column name=s67, symbol="$gs$r$b67$n", units=s, description="<delta*t>", type=double,  &end
&column name=s7, symbol="$gs$r$b7$n", description="sqrt(<t*t>)", type=double,  &end
&column name=ma1, symbol="max$sb$ex$sb$e", units=m, description="maximum absolute value of x", type=double,  &end
&column name=ma2, symbol="max$sb$ex'$sb$e", description="maximum absolute value of x'", type=double,  &end
&column name=ma3, symbol="max$sb$ey$sb$e", units=m, description="maximum absolute value of y", type=double,  &end
&column name=ma4, symbol="max$sb$ey'$sb$e", description="maximum absolute value of y'", type=double,  &end
&column name=ma5, symbol="max$sb$e$gD$rs$sb$e", units=m, description="maximum absolute deviation of s", type=double,  &end
&column name=ma6, symbol="max$sb$e$gd$r$sb$e", description="maximum absolute value of delta", type=double,  &end
&column name=ma7, symbol="max$sb$e$gD$rt$sb$e", units=s, description="maximum absolute deviation of t", type=double,  &end
&column name=minimum1, symbol="x$bmin$n", units=m, type=double,  &end
&column name=minimum2, symbol="x'$bmin$n", units=m, type=double,  &end
&column name=minimum3, symbol="y$bmin$n", units=m, type=double,  &end
&column name=minimum4, symbol="y'$bmin$n", units=m, type=double,  &end
&column name=minimum5, symbol="$gD$rs$bmin$n", units=m, type=double,  &end
&column name=minimum6, symbol="$gd$r$bmin$n", units=m, type=double,  &end
&column name=minimum7, symbol="t$bmin$n", units=s, type=double,  &end
&column name=maximum1, symbol="x$bmax$n", units=m, type=double,  &end
&column name=maximum2, symbol="x'$bmax$n", units=m, type=double,  &end
&column name=maximum3, symbol="y$bmax$n", units=m, type=double,  &end
&column name=maximum4, symbol="y'$bmax$n", units=m, type=double,  &end
&column name=maximum5, symbol="$gD$rs$bmax$n", units=m, type=double,  &end
&column name=maximum6, symbol="$gd$r$bmax$n", units=m, type=double,  &end
&column name=maximum7, symbol="t$bmax$n", units=s, type=double,  &end
&column name=Sx, symbol="$gs$r$bx$n", units=m, description=sqrt(<(x-<x>)^2>), type=double,  &end
&column name=Sxp, symbol="$gs$r$bx'$n", description=sqrt(<(x'-<x'>)^2>), type=double,  &end
&column name=Sy, symbol="$gs$r$by$n", units=m, description=sqrt(<(y-<y>)^2>), type=double,  &end
&column name=Syp, symbol="$gs$r$by'$n", description=sqrt(<(y'-<y'>)^2>), type=double,  &end
&column name=Ss, symbol="$gs$r$bs$n", units=m, description=sqrt(<(s-<s>)^2>), type=double,  &end
&column name=Sdelta, symbol="$gs$bd$n$r", description=sqrt(<(delta-<delta>)^2>), type=double,  &end
&column name=St, symbol="$gs$r$bt$n", units=s, description=sqrt(<(t-<t>)^2>), type=double,  &end
&column name=ex, symbol="$ge$r$bx$n", units=m, description="geometric horizontal emittance", type=double,  &end
&column name=enx, symbol="$ge$r$bx,n$n", units=m, description="normalized horizontal emittance", type=double,  &end
&column name=ecx, symbol="$ge$r$bx,c$n", units=m, description="geometric horizontal emittance less dispersive contributions", type=double,  &end
&column name=ecnx, symbol="$ge$r$bx,cn$n", units=m, description="normalized horizontal emittance less dispersive contributions", type=double,  &end
&column name=ey, symbol="$ge$r$by$n", units=m, description="geometric vertical emittance", type=double,  &end
&column name=eny, symbol="$ge$r$by,n$n", units=m, description="normalized vertical emittance", type=double,  &end
&column name=ecy, symbol="$ge$r$by,c$n", units=m, description="geometric vertical emittance less dispersive contributions", type=double,  &end
&column name=ecny, symbol="$ge$r$by,cn$n", units=m, description="normalized vertical emittance less dispersive contributions", type=double,  &end
&column name=betaxBeam, symbol="$gb$r$bx,beam$n", units=m, description="betax for the beam, excluding dispersive contributions", type=double,  &end
&column name=alphaxBeam, symbol="$ga$r$bx,beam$n", description="alphax for the beam, excluding dispersive contributions", type=double,  &end
&column name=betayBeam, symbol="$gb$r$by,beam$n", units=m, description="betay for the beam, excluding dispersive contributions", type=double,  &end
&column name=alphayBeam, symbol="$ga$r$by,beam$n", description="alphay for the beam, excluding dispersive contributions", type=double,  &end
&data mode=binary, &end
,                 _BEG_      MARK�W�+�>�>�x즛�=>�zk=������d=<t�7��]���h��(>�\�m隻�>_�QR�>h8I �3����s�t�ш%�E�=�V��&� ������;���s�f�>[C�0�X�=���ї+=ԍ��z;��
rp�R;�
v����>6;=_���AHũVs���x �(��͵ˈ�>-S�,�Tw����;=�����8?Xh�_=d'��y����<�L5�,�?��H�%?�M��y!?�h e�?   *���>r>�!�j?   �J�!=w�a��v�Ԧ��e��M��y!��h e��   ��.ɾr>�!�j�   D����L5�,�?��H�%?w��v?��AF�?   *���>�$��i?   �J�!=�W�+�>�>�>_�QR�>���s�f�>�
v����>�͵ˈ�>=�����8?�y����<�u*.�c�=`?��̫�>��o��=�&�ׁ��>��p��z�=�n��IT�>����z�='�1IT�>�qt��?��)����柘�w)@��U�/��           BNCH      CHARGE�W�+�>�>�x즛�=>�zk=������d=<t�7��]���h��(>�\�m隻�>_�QR�>h8I �3����s�t�ш%�E�=�V��&� ������;���s�f�>[C�0�X�=���ї+=ԍ��z;��
rp�R;�
v����>6;=_���AHũVs���x �(��͵ˈ�>-S�,�Tw����;=�����8?Xh�_=d'��y����<�L5�,�?��H�%?�M��y!?�h e�?   *���>r>�!�j?   �J�!=w�a��v�Ԧ��e��M��y!��h e��   ��.ɾr>�!�j�   D����L5�,�?��H�%?w��v?��AF�?   *���>�$��i?   �J�!=�W�+�>�>�>_�QR�>���s�f�>�
v����>�͵ˈ�>=�����8?�y����<�u*.�c�=`?��̫�>��o��=�&�ׁ��>��p��z�=�n��IT�>����z�='�1IT�>�qt��?��)����柘�w)@��U�/��333333�?   HBP_DR_BP_C      CSRDRIFTk?�JL�>7f�����=��W�qsb�;��	Q=�e�#�`=p"�]��=�����;�>_�QR�>~��G�����s�t�X�f�E�=�V��&� �s��A_��;G�}�pE�>.��$�=�I̚�'=���"����(Q;�
v����>K���AHũVs��2�"(�22ʈ�>��* �꽯ŧ=���;=�����8?�O�)d'�?�����<� n��i?��H�%?!��,[�#?�h e�?   �6��>r>�!�j?   8��!=�&�_�n�Ԧ��e�!��,[�#��h e��   l�.ɾr>�!�j�   p���� n��i?��H�%?�qK� ?��AF�?   �6��>�$��i?   8��!=k?�JL�>�>_�QR�>G�}�pE�>�
v����>22ʈ�>=�����8??�����<�u*.�c�=^?��̫�>��o��=�&�ׁ��>��p��z�=�n��IT�>����z�='�1IT�>��+�o�@�(פPd����a/@�jF�����33333�?	   HBP_DR_V3      DRIF�?�JL�>�f�����=��W�qsb����	Q=�f�#�`=�"�]��=�����;�>_�QR�>���G�����s�t�X�f�E�=�V��&� �s��A_��;W�}�pE�>.��$�=�I̚�'=��"����(Q;�
v����>K���AHũVs��2�"(�22ʈ�>��* �꽯ŧ=���;=�����8?�O�)d'�?�����<!n��i?��H�%?-��,[�#?�h e�?   �6��>r>�!�j?   8��!=�&�_�n�Ԧ��e�-��,[�#��h e��   l�.ɾr>�!�j�   p���!n��i?��H�%?�qK� ?��AF�?   �6��>�$��i?   8��!=�?�JL�>�>_�QR�>W�}�pE�>�
v����>22ʈ�>=�����8??�����<�u*.�c�=a?��̫�>��o��=�&�ׁ��>��p��z�=�n��IT�>����z�='�1IT�>��+�o�@�(פPd�ٵ�a/@�jF����TR����?   HBP_DR_QD_ED      DRIF_�Zm��>F9�h"�=���@��i�����K=�}�ie=Y	�3�ɽm�6�Т;�>_�QR�>�t�����s�t��۔5�E�=�V��&� �zF*0Y��;�@�tʭ�>�љHe�=�*�K�=	�_�G����d�Q;�
v����>� s���AHũVs�H?^��(��l�Ɉ�>�#!o�꽶�W����;=�����8?��L'd'��u@����<e��?��H�%?����)$?�h e�?   ?��>r>�!�j?   !=2�B�)��Ԧ��e�����)$��h e��   d�.ɾr>�!�j�   ����e��?��H�%?��ѣ�E ?��AF�?   ?��>�$��i?   !=_�Zm��>�>_�QR�>�@�tʭ�>�
v����>�l�Ɉ�>=�����8?�u@����<�u*.�c�=`?��̫�>��o��=�&�ׁ��>}�p��z�=�n��IT�>鵽��z�=�&�1IT�>�F�b�@!��r0q�1V��/@s�������G�z�?
   HBP_QD_V02      QUAD"殎�>+�I��=dc;]�-p����Z=�Ch�Ri= zT�ὸ�g���;�U��>[��ol����z��{�G=C}�
b��=��`k�� �P]����;:L�	���>)Q[��=K�=M��Ӆ��3��v�P;_,KZʻ�>�"�mp# �����V_a�d��;��<��7��Ɉ�>������I��ƚ��;=�����8?�_.$d'�.������<�{<��?H�%��?�иͶ7$?r>�p���>    I��>r>�!�j?   Ց�!=Q;v�N�m��иͶ7$�r>�p���   �.ɾr>�!�j�   `����{<��?H�%��?o{��m ?�i�UH�>    I��>�$��i?   Ց�!="殎�>�U��>:L�	���>_,KZʻ�>�7��Ɉ�>=�����8?.������<n��}c�=�̺|���>�ǥz�=��~���>O:���}�=�4o�V�>s��p�}�=�oAV�>����a@D���f�
�b2L��20@w�ç�6���G�z�?   HBP_COR      KICKER"殎�>+�I��=dc;]�-p����Z=�Ch�Ri= zT�ὸ�g���;�U��>[��ol����z��{�G=C}�
b��=��`k�� �P]����;:L�	���>)Q[��=K�=M��Ӆ��3��v�P;_,KZʻ�>�"�mp# �����V_a�d��;��<��7��Ɉ�>������I��ƚ��;=�����8?�_.$d'�.������<�{<��?H�%��?�иͶ7$?r>�p���>    I��>r>�!�j?   Ց�!=Q;v�N�m��иͶ7$�r>�p���   �.ɾr>�!�j�   `����{<��?H�%��?o{��m ?�i�UH�>    I��>�$��i?   Ց�!="殎�>�U��>:L�	���>_,KZʻ�>�7��Ɉ�>=�����8?.������<n��}c�=�̺|���>�ǥz�=��~���>O:���}�=�4o�V�>s��p�}�=�oAV�>����a@D���f�
�b2L��20@w�ç�6���G�z�?   HBP_BPM      MONI"殎�>+�I��=dc;]�-p����Z=�Ch�Ri= zT�ὸ�g���;�U��>[��ol����z��{�G=C}�
b��=��`k�� �P]����;:L�	���>)Q[��=K�=M��Ӆ��3��v�P;_,KZʻ�>�"�mp# �����V_a�d��;��<��7��Ɉ�>������I��ƚ��;=�����8?�_.$d'�.������<�{<��?H�%��?�иͶ7$?r>�p���>    I��>r>�!�j?   Ց�!=Q;v�N�m��иͶ7$�r>�p���   �.ɾr>�!�j�   `����{<��?H�%��?o{��m ?�i�UH�>    I��>�$��i?   Ց�!="殎�>�U��>:L�	���>_,KZʻ�>�7��Ɉ�>=�����8?.������<n��}c�=�̺|���>�ǥz�=��~���>O:���}�=�4o�V�>s��p�}�=�oAV�>����a@D���f�
�b2L��20@w�ç�6�rףp=
�?
   HBP_QD_V02      QUAD�8*���>FNaNq�=�]|�e�s���4bbg=��2u��m=��,�/��Ĥ\�Ǫ;.��xA�>�7�"闽+�1
 �|=�0��k�=#=�?F%!��6h��`�;3,ݽS��>��x�$ƽ�-i~]='��N䅽^S�ssP;O�B�9�>B&`	����]Ak@=�.2<�F�����Ɉ�>�:K���ik���;=�����8?�"1 d'�.�]r���<�]��a�?�l��?��=07$?fj#�O��>   BW��>r>�!�j?   ���!=��P�O-�D�|&���=07$�fj#�O���   H�.ɾr>�!�j�   ����]��a�?�l��?
��5_l ?Dh�3E�>   BW��>�$��i?   ���!=�8*���>.��xA�>3,ݽS��>O�B�9�>����Ɉ�>=�����8?.�]r���<�ɜVc�=un� k��>��}��=��F�]��>'����=1n�!�W�>��{���=�� ]�W�>{ڟ��@�%�4�
�ޱ���0@���@*�(\���?   HBP_DR_QD_ED      DRIF"�W�	�>���uj�=��ܓ�w��g�r��i=]���rq=�jMr���~Ă�B�;.��xA�>2�mR����+�1
 �|=]s���k�=#=�?F%!� vI~�`�;)(ؖ�>Q�K���ŽU�pM:�=�M �؅��8kA��O;O�B�9�>���`	����]Ak@='���*�F�b��Ɉ�>MtZ ��>x(ՙ��;=�����8?G_\d'�=�����<
����?�l��?�hK��$?fj#�O��>   #i��>r>�!�j?   ���!=>WMY��D�|&��hK��$�fj#�O���   Ĩ.ɾr>�!�j�   ����
����?�l��?SOK��T ?Dh�3E�>   #i��>�$��i?   ���!="�W�	�>.��xA�>)(ؖ�>O�B�9�>b��Ɉ�>=�����8?=�����<��ɜVc�=on� k��>��}��=p�F�]��>'����=,n�!�W�>��{���=�� ]�W�>�M���
@Yʵɏ"�Z{�Vo�/@���(��@*�(\���?   FITT_HBP_AA      MARK"�W�	�>���uj�=��ܓ�w��g�r��i=]���rq=�jMr���~Ă�B�;.��xA�>2�mR����+�1
 �|=]s���k�=#=�?F%!� vI~�`�;)(ؖ�>Q�K���ŽU�pM:�=�M �؅��8kA��O;O�B�9�>���`	����]Ak@='���*�F�b��Ɉ�>MtZ ��>x(ՙ��;=�����8?G_\d'�=�����<
����?�l��?�hK��$?fj#�O��>   #i��>r>�!�j?   ���!=>WMY��D�|&��hK��$�fj#�O���   Ĩ.ɾr>�!�j�   ����
����?�l��?SOK��T ?Dh�3E�>   #i��>�$��i?   ���!="�W�	�>.��xA�>)(ؖ�>O�B�9�>b��Ɉ�>=�����8?=�����<��ɜVc�=on� k��>��}��=p�F�]��>'����=,n�!�W�>��{���=�� ]�W�>�M���
@Yʵɏ"�Z{�Vo�/@���(��@�M���?	   HBP_DR_V4      DRIF�$����>bs�&��=��W\�H����_	_��=�~Q{P�=< �3w)�������;.��xA�>�[3�~��+�1
 �|=���g�i�=#=�?F%!��;i �^�;^�g�	�>Q���Ú���b��M�<�Q��x��Y�G$��;O�B�9�>D�B�4^	����]Ak@=WA,��F����Ѻ��>_1��+��b���;=�����8?��c'��������<����0?�l��?ѧ1�ݦ ?fj#�O��>   B���>r>�!�j?   ���!=��^�%�D�|&�ѧ1�ݦ �fj#�O���   �Y.ɾr>�!�j�   �S������0?�l��?q�[E��?Dh�3E�>   B���>�$��i?   ���!=�$����>.��xA�>^�g�	�>O�B�9�>���Ѻ��>=�����8?�������<�ɜVc�=�n� k��>�}��=�F�]��>'����=4n�!�W�>��{���=�� ]�W�>ݎ-;@ʥ;�0S*�0�QY�!@�qMP�#@���;��?   HBP_DR_QD_ED      DRIF<|�*�h�>�fx�p)�=��cjS��V�y,�=z�oLB��=N�1*�h^;[%�;.��xA�>@��(d~��+�1
 �|=&7\�i�=#=�?F%!��~�^�;n��E���>��_7��c�c�6�<�@��|m����3#h�:O�B�9�>��!^	����]Ak@=���g��F���%^���>�j��%�1\t̆��;=�����8?�\j~c'�K?n���<�-#�if0?�l��?�5?�׊ ?fj#�O��>   $���>r>�!�j?   ���!=�˹l<�%�D�|&��5?�׊ �fj#�O���   W.ɾr>�!�j�   �Q���-#�if0?�l��?dL:�?Dh�3E�>   $���>�$��i?   ���!=<|�*�h�>.��xA�>n��E���>O�B�9�>��%^���>=�����8?K?n���<�ɜVc�=Vn� k��>^�}��=9�F�]��>'����=3n�!�W�>��{���=�� ]�W�>R��9�5<@����6�*��JK4��!@3�vs�� @>7F�6�?
   HBP_QD_V03      QUAD/�u�p��>Q�y����=B(t-X��"�8%I~=Hf��&�=��z�0�*�hb}�%��;8��ʧ��>�_��j�z¥[S�R=A����~=��9(�X����0Ԃ�;f8xض�>�󹦒��=�^Hue���R�8����e���.0� �>�*y�_	��d�e��b���P��F���0���>��r�"�KXk����;=�����8? �U�{c'�VS�P���<D�V�!�0?��)=�?��+@� ?�" b���>    ���>r>�!�j?   ���!=��O�%�;0�3����+@� ��" b���   $V.ɾr>�!�j�   HQ��D�V�!�0?��)=�?Q����? �*���>    ���>�$��i?   ���!=/�u�p��>8��ʧ��>f8xض�>�.0� �>��0���>=�����8?VS�P���<��!d�=��BAu��>yH4�=1q6,j��>p'�m�~�=����V�>�HG�~�=mٵ9�V�>�r�hh�<@�dYܝ����V��}!@�ȕVr׿>7F�6�?   HBP_COR      KICKER/�u�p��>Q�y����=B(t-X��"�8%I~=Hf��&�=��z�0�*�hb}�%��;8��ʧ��>�_��j�z¥[S�R=A����~=��9(�X����0Ԃ�;f8xض�>�󹦒��=�^Hue���R�8����e���.0� �>�*y�_	��d�e��b���P��F���0���>��r�"�KXk����;=�����8? �U�{c'�VS�P���<D�V�!�0?��)=�?��+@� ?�" b���>    ���>r>�!�j?   ���!=��O�%�;0�3����+@� ��" b���   $V.ɾr>�!�j�   HQ��D�V�!�0?��)=�?Q����? �*���>    ���>�$��i?   ���!=/�u�p��>8��ʧ��>f8xض�>�.0� �>��0���>=�����8?VS�P���<��!d�=��BAu��>yH4�=1q6,j��>p'�m�~�=����V�>�HG�~�=mٵ9�V�>�r�hh�<@�dYܝ����V��}!@�ȕVr׿>7F�6�?   HBP_BPM      MONI/�u�p��>Q�y����=B(t-X��"�8%I~=Hf��&�=��z�0�*�hb}�%��;8��ʧ��>�_��j�z¥[S�R=A����~=��9(�X����0Ԃ�;f8xض�>�󹦒��=�^Hue���R�8����e���.0� �>�*y�_	��d�e��b���P��F���0���>��r�"�KXk����;=�����8? �U�{c'�VS�P���<D�V�!�0?��)=�?��+@� ?�" b���>    ���>r>�!�j?   ���!=��O�%�;0�3����+@� ��" b���   $V.ɾr>�!�j�   HQ��D�V�!�0?��)=�?Q����? �*���>    ���>�$��i?   ���!=/�u�p��>8��ʧ��>f8xض�>�.0� �>��0���>=�����8?VS�P���<��!d�=��BAu��>yH4�=1q6,j��>p'�m�~�=����V�>�HG�~�=mٵ9�V�>�r�hh�<@�dYܝ����V��}!@�ȕVr׿�vtP���?
   HBP_QD_V03      QUAD������>
��ͽI��i�[��,~�o�[=�l�Q�=�O�}�*��0�<��;=\z$���>6"Ұ 	a���A[]�X������A=��߽�Z��H\�;������>�E��!�=N��<Єȼ�쟲̈́��&������3KM�>�Zj�	�u{*�t��ǌ���F�ym`2���>$�|"�6㝡���;=�����8?EN��{c'���]���<�c�h&�0?�A$���?br��I� ?@c_���
?   ��>r>�!�j?   �!=1?��0�%��A$����br��I� �@c_���
�   V.ɾr>�!�j�   hQ���c�h&�0?�֜	 �>)�����?�%�H~>?   ��>�$��i?   �!=������>=\z$���>������>��3KM�>ym`2���>=�����8?��]���<��?��d�=
�����>3-25`�=��	���>�8T6}�=ጣ��U�>ٟy35}�=�wd#�U�>��(*=@l
(�@J����!@�]i�����U�ވ�?   HBP_DR_QD_ED      DRIF�J�X���>��ؙlͽ8�V�`���?tYO�Z=�*U�V�=w�E�B+�纘VH��;=\z$���>�A
<��a���A[]�X�9GO��A=��߽�ZǓ�^�;�c��1�>"��.Cu�=Xd�2�Լ�%y?��D�3����3KM�>]�nt�	�u{*�t���<���F�Fos#���>��m"꽨=]����;=�����8?J��{c'�:P\���<k��a�|0?�A$���?[a��� ?@c_���
?   ����>r>�!�j?   È!=�HQ/�%��A$����[a��� �@c_���
�   |U.ɾr>�!�j�   Q��k��a�|0?�֜	 �>*|M?�%�H~>?   ����>�$��i?   È!=�J�X���>=\z$���>�c��1�>��3KM�>Fos#���>=�����8?:P\���<��?��d�=�����>=-25`�=��	���>�8T6}�=挣��U�>�y35}�=�wd#�U�>����<@+���^z@C�~�>"@�^�w�H���ׁs�@	   HBP_DR_V5      DRIF��B>���>��m�<��%&>�U���c{3HA�t�~|��ʛ=7���D52��5�����;=\z$���>�J��%����A[]�X�2�2�ԋA=��߽𽎑)�S�;^®ŷ�>Q#�6��=�}��b-��T��h��K�qǐMj���3KM�>����	�u{*�t�v�P���F�A�����>������*�����;=�����8?��΀c'�ǎ�����<�x�DA?�A$���?_bXK�7?@c_���
?   6���>r>�!�j?   ��!=P �\��A$����_bXK�7�@c_���
�   �.ɾr>�!�j�   h+���x�DA?�֜	 �>У��2?�%�H~>?   6���>�$��i?   ��!=��B>���>=\z$���>^®ŷ�>��3KM�>A�����>=�����8?ǎ�����<��?��d�=�����>6-25`�=��	���>K9T6}�=����U�>3�y35}�=�wd#�U�>|���y%@�n�Zu��?[�N�}�K@�B�<8��-����@   HBP_DR_QD_ED      DRIF����Q��>�.���Ĺ��4�W��	�ύ/u���٥Л=c�s��@2�-ƨW6��;=\z$���>��AG����A[]�X����!ՋA=��߽�<o�U�;�m�x���>rC�u��=Rɰ�z�-�i��}����!Ү)�j���3KM�>)�����	�u{*�t�����F�E������>"��N��=�n����;=�����8?Xx[ڀc'��+�����<
�R�?�A$���?��[I��7?@c_���
?   ����>r>�!�j?   \�!=��/4@8��A$������[I��7�@c_���
�   �.ɾr>�!�j�   +��
�R�?�֜	 �>��n��2?�%�H~>?   ����>�$��i?   \�!=����Q��>=\z$���>�m�x���>��3KM�>E������>=�����8?�+�����<��?��d�=�����>8-25`�=��	���>9T6}�=��U�>�y35}�=�wd#�U�>Hn�.U�$@����a�?f
���IL@��le�	�c�@
   HBP_QD_V04      QUAD��F��>��O؂��=�!�WR��
ޙ8��_=�b�a�=��z��W2��ޛ�g�;z��'���>���x��Kj�>����G#q=��.S �edHwξ�;�5KU���>�S��pq1�6�M���-�.�J��,������´j���/�M�>  �zj�J�xK�D���P@b1��P	����>hh0,��=�����;=�����8?�؁!�c'��<J����<rPBU�?�FPu��> g����7?�lI���>   
���>r>�!�j?   ��!=�c6s)���u,�2�� g����7��sF�gԾ   t.ɾr>�!�j�   �*��rPBU�?�FPu��>��1�?�2?�lI���>   
���>�$��i?   ��!=��F��>z��'���>�5KU���>��/�M�>�P	����>=�����8?�<J����<���e:e�=P6��孞>?*4a��=��1$�>�|8��=y�hX�>��MP7��=�_��gX�>8V�ax�$@��#ľ�r?0���mL@�)����r?	�c�@   HBP_COR      KICKER��F��>��O؂��=�!�WR��
ޙ8��_=�b�a�=��z��W2��ޛ�g�;z��'���>���x��Kj�>����G#q=��.S �edHwξ�;�5KU���>�S��pq1�6�M���-�.�J��,������´j���/�M�>  �zj�J�xK�D���P@b1��P	����>hh0,��=�����;=�����8?�؁!�c'��<J����<rPBU�?�FPu��> g����7?�lI���>   
���>r>�!�j?   ��!=�c6s)���u,�2�� g����7��sF�gԾ   t.ɾr>�!�j�   �*��rPBU�?�FPu��>��1�?�2?�lI���>   
���>�$��i?   ��!=��F��>z��'���>�5KU���>��/�M�>�P	����>=�����8?�<J����<���e:e�=P6��孞>?*4a��=��1$�>�|8��=y�hX�>��MP7��=�_��gX�>8V�ax�$@��#ľ�r?0���mL@�)����r?	�c�@   HBP_BPM      MONI��F��>��O؂��=�!�WR��
ޙ8��_=�b�a�=��z��W2��ޛ�g�;z��'���>���x��Kj�>����G#q=��.S �edHwξ�;�5KU���>�S��pq1�6�M���-�.�J��,������´j���/�M�>  �zj�J�xK�D���P@b1��P	����>hh0,��=�����;=�����8?�؁!�c'��<J����<rPBU�?�FPu��> g����7?�lI���>   
���>r>�!�j?   ��!=�c6s)���u,�2�� g����7��sF�gԾ   t.ɾr>�!�j�   �*��rPBU�?�FPu��>��1�?�2?�lI���>   
���>�$��i?   ��!=��F��>z��'���>�5KU���>��/�M�>�P	����>=�����8?�<J����<���e:e�=P6��孞>?*4a��=��1$�>�|8��=y�hX�>��MP7��=�_��gX�>8V�ax�$@��#ľ�r?0���mL@�)����r?2�%�C@
   HBP_QD_V04      QUAD��	��>�{x��=N~�J������؂=`>w(�=��Kf�2�+U4��?�;Y2S��>���-ȗ���{dk=��7#�=������\�0 ۼ;T�&���>y��zܽ����3�-��K3i�$��vL)H��j��F��V�>_�H�F�<����uo=]�6�V�&;�92����>�RO���FP_����;=�����8?��V�c'�	&�����<�J�K�?��pQ?;�R���7?�)9��?   '���>r>�!�j?   ��!=*���@��-�����;�R���7��`
��+�   H.ɾr>�!�j�   �*���J�K�?��pQ?I+ģ�2?�)9��?   '���>�$��i?   ��!=��	��>Y2S��>T�&���>�F��V�>�92����>=�����8?	&�����<>Le�=i��?��>��$9��=�I;��>��wX��=��[�>%KZ?W��=0�@E[�>��3"��$@�\g��;��4��
BL@�ʉ0gf@8gDio@   HBP_DR_QD_ED      DRIF��e���>
Y	}%)�=�u��髽B�9�#�=��8���=/�2b�2��b]�y��;Y2S��>i؜좗���{dk=/\��"�=��������F�
ۼ;��x����>S>7˼�۽�g�-��e�����A`�j��F��V�>�N�"@F�<����uo=�ˇQ��&;h���>ԡ�^�꽟^ ���;=�����8?��r<�c'���#����<�'��)?��pQ?+�#"�7?�)9��?   ����>r>�!�j?   )�!=B5�|n��-�����+�#"�7��`
��+�   H.ɾr>�!�j�   +���'��)?��pQ?oq���2?�)9��?   ����>�$��i?   )�!=��e���>Y2S��>��x����>�F��V�>h���>=�����8?��#����<;Le�=i��?��>��$9��=�I;��>[��wX��=B�[�>�KZ?W��=u�@E[�>I|Q�V %@y߾�l���� ��K@�H}�'9@7Ҵ��)&@	   HBP_DR_V5      DRIF�pZ�?4R��6�=O���������=NH�@�=�x�D�F��v��3��;Y2S��>���+ͤ����{dk=V�#\��=�������>	�ڼ;Xs�R~'�><d�"qŽK��/��&�|{<D���{ᇡ�Yd��F��V�>�G	�`Y�<����uo=qUZ���&;G�#f���>�1����2goO���;=�����8?�V�vc'��x�����<>
R'�/?��pQ?�3~�$?�)9��?   �$��>r>�!�j?   /�!=�쮂ZO,��-������3~�$��`
��+�   �.ɾr>�!�j�   �@��>
R'�/?��pQ?���� ?�)9��?   �$��>�$��i?   /�!=�pZ�?Y2S��>Xs�R~'�>�F��V�>G�#f���>=�����8?�x�����<8Le�=
i��?��>q�$9��=zI;��>���wX��= �[�>)KZ?W��=2�@E[�>�h��<@�f��P�j� V$)"@����>@-��?&@   HBP_DR_QD_ED      DRIFݺ:���?�>[�T�=����Ũ���PH�=���� 4�=����F����Zq�;Y2S��>ձzULZ����{dk=�q����=��������E��ڼ;��U����>�/��Ž�S�S�&���}{\��1��	]Jd��F��V�>��,-�Y�<����uo=a���&;I��_���>Ty���/P���;=�����8?��evc'��z����<k�[��/?��pQ?��Փ�>$?�)9��?   ^%��>r>�!�j?   j/�!=�[��Jv,��-�������Փ�>$��`
��+�   �.ɾr>�!�j�   �@��k�[��/?��pQ?Z;�N��?�)9��?   ^%��>�$��i?   j/�!=ݺ:���?Y2S��>��U����>�F��V�>I��_���>=�����8?�z����<>Le�=i��?��>��$9��=�I;��>��wX��=	�[�>6KZ?W��=;�@E[�>��4��<@(>�1i����驫!@�����@A�X�T&@
   HBP_QD_V03      QUAD(��K�??�&�[��b��_�n��{֡�mK�=����K1�=e�Q�D�F�q���^�;'z��R!�>TM�7��v=���(T'`�\��A��H��G�>�ӿ�c»�P�/(��>��g���60ں&���y�K��YA�*Yd���k�I;�>*�Ć@���3#E�M���<A�V2W���>w�f��꽦U�L���;=�����8?�Y?wvc'�'㟩���<�oO���/?jV��a?�9�A�$? �j���>   &��>r>�!�j?   0�!=�)��q,��*!�?��9�A�$� �%���   �.ɾr>�!�j�   (A���oO���/?jV��a?x�%��h? �j���>   &��>�$��i?   0�!=(��K�?'z��R!�>�P�/(��>��k�I;�>V2W���>=�����8?'㟩���<E8�;a�= �턨��>�I��3�=O癏>Mm6���=�j��Z�>�U�����=7I�Z�>?��	�<@�|��@-q,&#j!@c�	���?A�X�T&@   HBP_COR      KICKER(��K�??�&�[��b��_�n��{֡�mK�=����K1�=e�Q�D�F�q���^�;'z��R!�>TM�7��v=���(T'`�\��A��H��G�>�ӿ�c»�P�/(��>��g���60ں&���y�K��YA�*Yd���k�I;�>*�Ć@���3#E�M���<A�V2W���>w�f��꽦U�L���;=�����8?�Y?wvc'�'㟩���<�oO���/?jV��a?�9�A�$? �j���>   &��>r>�!�j?   0�!=�)��q,��*!�?��9�A�$� �%���   �.ɾr>�!�j�   (A���oO���/?jV��a?x�%��h? �j���>   &��>�$��i?   0�!=(��K�?'z��R!�>�P�/(��>��k�I;�>V2W���>=�����8?'㟩���<E8�;a�= �턨��>�I��3�=O癏>Mm6���=�j��Z�>�U�����=7I�Z�>?��	�<@�|��@-q,&#j!@c�	���?A�X�T&@   HBP_BPM      MONI(��K�??�&�[��b��_�n��{֡�mK�=����K1�=e�Q�D�F�q���^�;'z��R!�>TM�7��v=���(T'`�\��A��H��G�>�ӿ�c»�P�/(��>��g���60ں&���y�K��YA�*Yd���k�I;�>*�Ć@���3#E�M���<A�V2W���>w�f��꽦U�L���;=�����8?�Y?wvc'�'㟩���<�oO���/?jV��a?�9�A�$? �j���>   &��>r>�!�j?   0�!=�)��q,��*!�?��9�A�$� �%���   �.ɾr>�!�j�   (A���oO���/?jV��a?x�%��h? �j���>   &��>�$��i?   0�!=(��K�?'z��R!�>�P�/(��>��k�I;�>V2W���>=�����8?'㟩���<E8�;a�= �턨��>�I��3�=O癏>Mm6���=�j��Z�>�U�����=7I�Z�>?��	�<@�|��@-q,&#j!@c�	���?UW�ۊh&@
   HBP_QD_V03      QUAD��s��?ꛞ�;���������p���d��T�p��={�ǙF��>V3s��;�Kw�3��>9�؇W�=��4�e4=3��Hn��������3>y6�.ٻ���>	��>�����=+6~�&'��9 �i�����٢d�ho(��>�l3����,{�T�u��X�+T�򍉗���>u( p��$������;=�����8?�3#/sc'��e����<N��Hl/?OD܈y�?�@#H�8$?�?5K�v?   �.��>r>�!�j?   �7�!=B���],�OD܈y���@#H�8$��?5K�v�   H.ɾr>�!�j�   �B��N��Hl/?ᇩ���?cV�?���^I ?   �.��>�$��i?   �7�!=��s��?�Kw�3��>���>	��>ho(��>򍉗���>=�����8?�e����<;��]�=���C��>*����=M��j⒏>��u�Ӄ�=K}s,Z�>��҃�=�`>bZ�>!��{�<@����*@E��ˍ!@������ �K��jM~&@   HBP_DR_QD_ED      DRIF��y�,?�{"z(Q��``Ҍ����"�<%�d��1�	�d�=�.L(.F�?�`�'�;�Kw�3��>��(��Z�=��4�e4=�*qn��������3>ᒟB ٻ=�6�T��>ϡ��\�=ɦ=iׇ'��Ȑ����s�?�e�ho(��>��A���,{�T�u�Z����+T��
�,���>�3��3������;=�����8?5a[ lc'�FV����<R�[�.?OD܈y�?�t�8s$?�?5K�v?   �?��>r>�!�j?   G�!=䔗V.�+�OD܈y���t�8s$��?5K�v�   �.ɾr>�!�j�   F��R�[�.?ᇩ���?4L	d��?���^I ?   �?��>�$��i?   G�!=��y�,?�Kw�3��>=�6�T��>ho(��>�
�,���>=�����8?FV����<��]�=����C��>����=w��j⒏>��u�Ӄ�=M}s,Z�>��҃�=�`>bZ�>���:@����;*@q\:��!@���t �WB>��,)@	   HBP_DR_V4      DRIF��.�>1!L���'��m��:��	�Pa��A���=N�j �1��&+���;�Kw�3��>tF��gȓ=��4�e4=�P��k��������3>�)DR}ٻ�"���>蠐X���=�gt��S3�,�&̋�������Mq�ho(��>aC�̈�,{�T�u�G@���,T��P�Ĉ�>�K��[)���;=�����8?ҫ�b�b'�*�Wo���<L�10?OD܈y�?����L�+?�?5K�v?   �U��>r>�!�j?   2'�!=�Yv����OD܈y������L�+��?5K�v�   �.ɾr>�!�j�   쬐�L�10?ᇩ���?ֲ*�wi%?���^I ?   �U��>�$��i?   2'�!=��.�>�Kw�3��>�"���>ho(��>�P�Ĉ�>=�����8?*�Wo���<���]�=����C��>F����=r��j⒏>��u�Ӄ�=K}s,Z�>��҃�=�`>bZ�>_�u�
@�y��@(q�O�/@�,��WB>��,)@   FITT_HBP_BB      MARK��.�>1!L���'��m��:��	�Pa��A���=N�j �1��&+���;�Kw�3��>tF��gȓ=��4�e4=�P��k��������3>�)DR}ٻ�"���>蠐X���=�gt��S3�,�&̋�������Mq�ho(��>aC�̈�,{�T�u�G@���,T��P�Ĉ�>�K��[)���;=�����8?ҫ�b�b'�*�Wo���<L�10?OD܈y�?����L�+?�?5K�v?   �U��>r>�!�j?   2'�!=�Yv����OD܈y������L�+��?5K�v�   �.ɾr>�!�j�   쬐�L�10?ᇩ���?ֲ*�wi%?���^I ?   �U��>�$��i?   2'�!=��.�>�Kw�3��>�"���>ho(��>�P�Ĉ�>=�����8?*�Wo���<���]�=����C��>F����=r��j⒏>��u�Ӄ�=K}s,Z�>��҃�=�`>bZ�>_�u�
@�y��@(q�O�/@�,��Mk�w�B)@   HBP_DR_QD_ED	      DRIF�5��>��+��L�_�������TH$5a�r�;Ls�=�����0��V�sL�;�Kw�3��>ۿ���˓=��4�e4=�O64�k��������3>xڄ�C}ٻ��E��>�r����=3�/�3�|�}��痽��*�v�q�ho(��>q�zKֈ�,{�T�u�:�?O�,T���&/ň�>i�~h��L�����;=�����8?`�14�b'���-���<�͵l?OD܈y�?LoJ���+?�?5K�v?   �f��>r>�!�j?   i6�!=(���OD܈y��LoJ���+��?5K�v�   ��.ɾr>�!�j�   0����͵l?ᇩ���?�V�sĕ%?���^I ?   �f��>�$��i?   i6�!=�5��>�Kw�3��>��E��>ho(��>��&/ň�>=�����8?��-���<��]�=����C��>c����=���j⒏>��u�Ӄ�=K}s,Z�>��҃�=�`>bZ�>��(Z��@`���@�Z�0@��W{���a�XW)@
   HBP_QD_V02      QUAD��m�>�*�$FN�R2p?����d�dB͗l=�M��m�=��%�H0�(�.��;�Vf�z�>`�P��Ջ=e>Ң�m�����ø��OE���L/>�u�`Ի��Ý��>�E�gj魽a2��3����5����i�$ʡq�<�<�tе>�*&���编n��RF=��0�m%��ɔ�ň�>��\ύ�[YS���;=�����8?[4�|b'�^�+f���<J���7?�v�?o-�5�+?������>   Rt��>r>�!�j?   �B�!=���O��ޏ��o-�5�+�<�TŤ��    �.ɾr>�!�j�   ����J���7?�v�?�ұ��%?������>   Rt��>�$��i?   �B�!=��m�>�Vf�z�>��Ý��><�<�tе>�ɔ�ň�>=�����8?^�+f���<pA�]�=&���⣞>��*z�= �����>Q�[k^��=q4,yZ�>�SH5]��=���SxZ�>�).X>@rN-O��
@fNR��0@ј,�6�?a�XW)@   HBP_COR      KICKER��m�>�*�$FN�R2p?����d�dB͗l=�M��m�=��%�H0�(�.��;�Vf�z�>`�P��Ջ=e>Ң�m�����ø��OE���L/>�u�`Ի��Ý��>�E�gj魽a2��3����5����i�$ʡq�<�<�tе>�*&���编n��RF=��0�m%��ɔ�ň�>��\ύ�[YS���;=�����8?[4�|b'�^�+f���<J���7?�v�?o-�5�+?������>   Rt��>r>�!�j?   �B�!=���O��ޏ��o-�5�+�<�TŤ��    �.ɾr>�!�j�   ����J���7?�v�?�ұ��%?������>   Rt��>�$��i?   �B�!=��m�>�Vf�z�>��Ý��><�<�tе>�ɔ�ň�>=�����8?^�+f���<pA�]�=&���⣞>��*z�= �����>Q�[k^��=q4,yZ�>�SH5]��=���SxZ�>�).X>@rN-O��
@fNR��0@ј,�6�?a�XW)@   HBP_BPM      MONI��m�>�*�$FN�R2p?����d�dB͗l=�M��m�=��%�H0�(�.��;�Vf�z�>`�P��Ջ=e>Ң�m�����ø��OE���L/>�u�`Ի��Ý��>�E�gj魽a2��3����5����i�$ʡq�<�<�tе>�*&���编n��RF=��0�m%��ɔ�ň�>��\ύ�[YS���;=�����8?[4�|b'�^�+f���<J���7?�v�?o-�5�+?������>   Rt��>r>�!�j?   �B�!=���O��ޏ��o-�5�+�<�TŤ��    �.ɾr>�!�j�   ����J���7?�v�?�ұ��%?������>   Rt��>�$��i?   �B�!=��m�>�Vf�z�>��Ý��><�<�tе>�ɔ�ň�>=�����8?^�+f���<pA�]�=&���⣞>��*z�= �����>Q�[k^��=q4,yZ�>�SH5]��=���SxZ�>�).X>@rN-O��
@fNR��0@ј,�6�?u�):�k)@
   HBP_QD_V02      QUAD��.\n�>�\��=Ὧ�چH��\�2qV�=YI����=���#�y/�]|/�:f�;ぶ_=��>F��d�7�=����ks��3�?����9��W'>Ts1��λ�4����>�(��fPҽ�ђ�3����qٗ�F�]g>�q��xJa���>�����=�Jg�f�z=!�H�d�M;�L��ň�>�6��ڟ͒���;=�����8?�<	�xb'��HL����<��0��?�.�S�3?p�G�+?Dļ��?   �}��>r>�!�j?   K�!=�T�GS1��B=V��p�G�+����|�
�   ��.ɾr>�!�j�   \�����0��?�.�S�3?4�r�2{%?Dļ��?   �}��>�$��i?   K�!=��.\n�>ぶ_=��>�4����>�xJa���>�L��ň�>=�����8?�HL����<�Ep�^�=��@^u��>#ޜ��=$�Mx��>�I�턭=V "�Z�>z���섭=q��U�Z�>���e�^@���GZ@�9(8:�/@f�P�g�@k���T�)@   HBP_DR_QD_ED
      DRIF�L��т�>���c���*�Lc���E�p���=>��q�=�A$��{.�3;/���;ぶ_=��>\<��
�=����ks��L%�?����9��W'>3 ���λo?g/�>��X4{ҽ4"���s3��W\O-�����K-�iq��xJa���>�[/(�=�Jg�f�z=
�w+��M;�\��ň�>ȏ[����︟��;=�����8?X�E�ub'�$�����<�� A	?�.�S�3?;DP��c+?Dļ��?   J���>r>�!�j?   �R�!=g�����B=V��;DP��c+����|�
�    �.ɾr>�!�j�   ������ A	?�.�S�3?=9�,�2%?Dļ��?   J���>�$��i?   �R�!=�L��т�>ぶ_=��>o?g/�>�xJa���>�\��ň�>=�����8?$�����<�Ep�^�=��@^u��>8ޜ��=?�Mx��>�I�턭=? "�Z�>W���섭=Z��U�Z�>�Gf&�@��M@D��~��.@ՕA��@n���T�)@	   HBP_DR_V3      DRIF�L��т�>j��c����Lc���5�p���="��q�=�A$��{.�;/���;ぶ_=��>=<��
�=����ks��L%�?����9��W'>3 ���λ�n?g/�>��X4{ҽ,"���s3��W\O-�����K-�iq��xJa���>�[/(�=�Jg�f�z=
�w+��M;�\��ň�>ȏ[����︟��;=�����8?X�E�ub'�$�����<�� A	?�.�S�3?-DP��c+?Dļ��?   J���>r>�!�j?   �R�!=N�����B=V��-DP��c+����|�
�    �.ɾr>�!�j�   ������ A	?�.�S�3?29�,�2%?Dļ��?   J���>�$��i?   �R�!=�L��т�>ぶ_=��>�n?g/�>�xJa���>�\��ň�>=�����8?$�����<�Ep�^�=��@^u��>Mޜ��=[�Mx��>�I�턭=K "�Z�>h���섭=e��U�Z�>�Gf&�@ǯ�M@��~��.@��A��@�c�*@	   HBP_DR_BP      DRIFB����,�>�OBB_@׽{ ʓL������o}=@� �bq�=���~�z'�[��"�3�;ぶ_=��>�+��z�y=����ks���ve?����9��W'>�sg�λ�J�.�Q�>R?t�8Eн�
�	z52��e�c�������2Mp��xJa���>��Aڰ�=�Jg�f�z=�GRf{�M;�yafƈ�>U��g�gaƠ��;=�����8?�ͱ�]b'���ˍ���<!SE��/?�.�S�3?�Y���(?Dļ��?   �>r>�!�j?   ���!=2ABD�$��B=V���Y���(����|�
�   ș.ɾr>�!�j�   ؾ��!SE��/?�.�S�3?a��ls3#?Dļ��?   �>�$��i?   ���!=B����,�>ぶ_=��>�J�.�Q�>�xJa���>�yafƈ�>=�����8?��ˍ���<�Ep�^�=��@^u��><ޜ��=D�Mx��>�I�턭=? "�Z�>W���섭=Z��U�Z�>M����?�oW���?����H)@�2�A�@�c�*@   HBP_CNT      CENTERB����,�>�OBB_@׽{ ʓL������o}=@� �bq�=���~�z'�[��"�3�;ぶ_=��>�+��z�y=����ks���ve?����9��W'>�sg�λ�J�.�Q�>R?t�8Eн�
�	z52��e�c�������2Mp��xJa���>��Aڰ�=�Jg�f�z=�GRf{�M;�yafƈ�>U��g�gaƠ��;=�����8?�ͱ�]b'���ˍ���<�*Wk�0??���3?2�����(?�%{��?   �>r>�!�j?   ���!=J��R"�f24����2�����(��W&�|�
�   ș.ɾr>�!�j�   ؾ���*Wk�0??���3?��s3#?�%{��?   �>�$��i?   ���!=B����,�>ぶ_=��>�J�.�Q�>�xJa���>�yafƈ�>=�����8?��ˍ���<�Ep�^�=��@^u��><ޜ��=D�Mx��>�I�턭=? "�Z�>W���섭=Z��U�Z�>M����?�oW���?����H)@�2�A�@�c�*@
   HBP_WA_OU2      WATCHB����,�>�OBB_@׽{ ʓL������o}=@� �bq�=���~�z'�[��"�3�;ぶ_=��>�+��z�y=����ks���ve?����9��W'>�sg�λ�J�.�Q�>R?t�8Eн�
�	z52��e�c�������2Mp��xJa���>��Aڰ�=�Jg�f�z=�GRf{�M;�yafƈ�>U��g�gaƠ��;=�����8?�ͱ�]b'���ˍ���<�*Wk�0??���3?2�����(?�%{��?   �>r>�!�j?   ���!=J��R"�f24����2�����(��W&�|�
�   ș.ɾr>�!�j�   ؾ���*Wk�0??���3?��s3#?�%{��?   �>�$��i?   ���!=B����,�>ぶ_=��>�J�.�Q�>�xJa���>�yafƈ�>=�����8?��ˍ���<�Ep�^�=��@^u��><ޜ��=D�Mx��>�I�턭=? "�Z�>W���섭=Z��U�Z�>M����?�oW���?����H)@�2�A�@