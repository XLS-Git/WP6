SDDS1
!# little-endian
&description text="sigma matrix--input: xls_linac_ln3_bp.track.ele  lattice: xls_linac.10.lte", contents="sigma matrix", &end
&parameter name=Step, description="Simulation step", type=long, &end
&parameter name=SVNVersion, description="SVN version number", type=string, fixed_value=unknown, &end
&column name=s, units=m, description=Distance, type=double,  &end
&column name=ElementName, description="Element name", format_string=%10s, type=string,  &end
&column name=ElementOccurence, description="Occurence of element", format_string=%6ld, type=long,  &end
&column name=ElementType, description="Element-type name", format_string=%10s, type=string,  &end
&column name=s1, symbol="$gs$r$b1$n", units=m, description="sqrt(<x*x>)", type=double,  &end
&column name=s12, symbol="$gs$r$b12$n", units=m, description="<x*xp'>", type=double,  &end
&column name=s13, symbol="$gs$r$b13$n", units="m$a2$n", description="<x*y>", type=double,  &end
&column name=s14, symbol="$gs$r$b14$n", units=m, description="<x*y'>", type=double,  &end
&column name=s15, symbol="$gs$r$b15$n", units="m$a2$n", description="<x*s>", type=double,  &end
&column name=s16, symbol="$gs$r$b16$n", units=m, description="<x*delta>", type=double,  &end
&column name=s17, symbol="$gs$r$b17$n", units="m*s", description="<x*t>", type=double,  &end
&column name=s2, symbol="$gs$r$b2$n", description="sqrt(<x'*x'>)", type=double,  &end
&column name=s23, symbol="$gs$r$b23$n", units=m, description="<x'*y>", type=double,  &end
&column name=s24, symbol="$gs$r$b24$n", description="<x'*y'>", type=double,  &end
&column name=s25, symbol="$gs$r$b25$n", units=m, description="<x'*s>", type=double,  &end
&column name=s26, symbol="$gs$r$b26$n", description="<x'*delta>", type=double,  &end
&column name=s27, symbol="$gs$r$b27$n", units=s, description="<x'*t>", type=double,  &end
&column name=s3, symbol="$gs$r$b3$n", units=m, description="sqrt(<y*y>)", type=double,  &end
&column name=s34, symbol="$gs$r$b34$n", units=m, description="<y*y'>", type=double,  &end
&column name=s35, symbol="$gs$r$b35$n", units="m$a2$n", description="<y*s>", type=double,  &end
&column name=s36, symbol="$gs$r$b36$n", units=m, description="<y*delta>", type=double,  &end
&column name=s37, symbol="$gs$r$b37$n", units="m*s", description="<y*t>", type=double,  &end
&column name=s4, symbol="$gs$r$b4$n", description="sqrt(<y'*y')>", type=double,  &end
&column name=s45, symbol="$gs$r$b45$n", units=m, description="<y'*s>", type=double,  &end
&column name=s46, symbol="$gs$r$b46$n", description="<y'*delta>", type=double,  &end
&column name=s47, symbol="$gs$r$b47$n", units=s, description="<y'*t>", type=double,  &end
&column name=s5, symbol="$gs$r$b5$n", units=m, description="sqrt(<s*s>)", type=double,  &end
&column name=s56, symbol="$gs$r$b56$n", units=m, description="<s*delta>", type=double,  &end
&column name=s57, symbol="$gs$r$b57$n", units="m*s", description="<s*t>", type=double,  &end
&column name=s6, symbol="$gs$r$b6$n", description="sqrt(<delta*delta>)", type=double,  &end
&column name=s67, symbol="$gs$r$b67$n", units=s, description="<delta*t>", type=double,  &end
&column name=s7, symbol="$gs$r$b7$n", description="sqrt(<t*t>)", type=double,  &end
&column name=ma1, symbol="max$sb$ex$sb$e", units=m, description="maximum absolute value of x", type=double,  &end
&column name=ma2, symbol="max$sb$ex'$sb$e", description="maximum absolute value of x'", type=double,  &end
&column name=ma3, symbol="max$sb$ey$sb$e", units=m, description="maximum absolute value of y", type=double,  &end
&column name=ma4, symbol="max$sb$ey'$sb$e", description="maximum absolute value of y'", type=double,  &end
&column name=ma5, symbol="max$sb$e$gD$rs$sb$e", units=m, description="maximum absolute deviation of s", type=double,  &end
&column name=ma6, symbol="max$sb$e$gd$r$sb$e", description="maximum absolute value of delta", type=double,  &end
&column name=ma7, symbol="max$sb$e$gD$rt$sb$e", units=s, description="maximum absolute deviation of t", type=double,  &end
&column name=minimum1, symbol="x$bmin$n", units=m, type=double,  &end
&column name=minimum2, symbol="x'$bmin$n", units=m, type=double,  &end
&column name=minimum3, symbol="y$bmin$n", units=m, type=double,  &end
&column name=minimum4, symbol="y'$bmin$n", units=m, type=double,  &end
&column name=minimum5, symbol="$gD$rs$bmin$n", units=m, type=double,  &end
&column name=minimum6, symbol="$gd$r$bmin$n", units=m, type=double,  &end
&column name=minimum7, symbol="t$bmin$n", units=s, type=double,  &end
&column name=maximum1, symbol="x$bmax$n", units=m, type=double,  &end
&column name=maximum2, symbol="x'$bmax$n", units=m, type=double,  &end
&column name=maximum3, symbol="y$bmax$n", units=m, type=double,  &end
&column name=maximum4, symbol="y'$bmax$n", units=m, type=double,  &end
&column name=maximum5, symbol="$gD$rs$bmax$n", units=m, type=double,  &end
&column name=maximum6, symbol="$gd$r$bmax$n", units=m, type=double,  &end
&column name=maximum7, symbol="t$bmax$n", units=s, type=double,  &end
&column name=Sx, symbol="$gs$r$bx$n", units=m, description=sqrt(<(x-<x>)^2>), type=double,  &end
&column name=Sxp, symbol="$gs$r$bx'$n", description=sqrt(<(x'-<x'>)^2>), type=double,  &end
&column name=Sy, symbol="$gs$r$by$n", units=m, description=sqrt(<(y-<y>)^2>), type=double,  &end
&column name=Syp, symbol="$gs$r$by'$n", description=sqrt(<(y'-<y'>)^2>), type=double,  &end
&column name=Ss, symbol="$gs$r$bs$n", units=m, description=sqrt(<(s-<s>)^2>), type=double,  &end
&column name=Sdelta, symbol="$gs$bd$n$r", description=sqrt(<(delta-<delta>)^2>), type=double,  &end
&column name=St, symbol="$gs$r$bt$n", units=s, description=sqrt(<(t-<t>)^2>), type=double,  &end
&column name=ex, symbol="$ge$r$bx$n", units=m, description="geometric horizontal emittance", type=double,  &end
&column name=enx, symbol="$ge$r$bx,n$n", units=m, description="normalized horizontal emittance", type=double,  &end
&column name=ecx, symbol="$ge$r$bx,c$n", units=m, description="geometric horizontal emittance less dispersive contributions", type=double,  &end
&column name=ecnx, symbol="$ge$r$bx,cn$n", units=m, description="normalized horizontal emittance less dispersive contributions", type=double,  &end
&column name=ey, symbol="$ge$r$by$n", units=m, description="geometric vertical emittance", type=double,  &end
&column name=eny, symbol="$ge$r$by,n$n", units=m, description="normalized vertical emittance", type=double,  &end
&column name=ecy, symbol="$ge$r$by,c$n", units=m, description="geometric vertical emittance less dispersive contributions", type=double,  &end
&column name=ecny, symbol="$ge$r$by,cn$n", units=m, description="normalized vertical emittance less dispersive contributions", type=double,  &end
&column name=betaxBeam, symbol="$gb$r$bx,beam$n", units=m, description="betax for the beam, excluding dispersive contributions", type=double,  &end
&column name=alphaxBeam, symbol="$ga$r$bx,beam$n", description="alphax for the beam, excluding dispersive contributions", type=double,  &end
&column name=betayBeam, symbol="$gb$r$by,beam$n", units=m, description="betay for the beam, excluding dispersive contributions", type=double,  &end
&column name=alphayBeam, symbol="$ga$r$by,beam$n", description="alphay for the beam, excluding dispersive contributions", type=double,  &end
&data mode=binary, &end
%                 _BEG_      MARK�����>-Ky%h��=�qi��=��pg�A=��D-���`�'����=7NG>�ٻX_)�x�>��1�]9�Q��@`<_=^L���o��ot���Z�O�A������mQ�>ybv�w��
���v�(���F�����Y�)f���E�D�>�Aլ�=�\��R|Q;F_��Ӄ�>�[w��=702���;�F�0B�+?�G:zDO<&�;�)��<�[���?:O08e�?��l�\�?n��C-??   ���>In��0_l?   �?< =�[�������$R7����l�\���%jE���   Ho~Ǿ-�|3�e�   D9
����,�K?:O08e�?H,�Q��?n��C-??   ���>In��0_l?   �?< =�����>X_)�x�>���mQ�>��E�D�>F_��Ӄ�>�F�0B�+?&�;�)��<��63X�=bF�xױ�>?��jE�=
[,�>��V���=|m@��_�>E�6{臭=_z��_�>�Q�@�R��Ʃ�����m=F@ ����X�?           BNCH      CHARGE�����>-Ky%h��=�qi��=��pg�A=��D-���`�'����=7NG>�ٻX_)�x�>��1�]9�Q��@`<_=^L���o��ot���Z�O�A������mQ�>ybv�w��
���v�(���F�����Y�)f���E�D�>�Aլ�=�\��R|Q;F_��Ӄ�>�[w��=702���;�F�0B�+?�G:zDO<&�;�)��<�[���?:O08e�?��l�\�?n��C-??   ���>In��0_l?   �?< =�[�������$R7����l�\���%jE���   Ho~Ǿ-�|3�e�   D9
����,�K?:O08e�?H,�Q��?n��C-??   ���>In��0_l?   �?< =�����>X_)�x�>���mQ�>��E�D�>F_��Ӄ�>�F�0B�+?&�;�)��<��63X�=bF�xױ�>?��jE�=
[,�>��V���=|m@��_�>E�6{臭=_z��_�>�Q�@�R��Ʃ�����m=F@ ����X�?�rt��?   LN3_DR_BP_V1      DRIF�"�J��>"~�+*��=z�V2�=���mq=�)�a᡽#{�mC�=[�V���X_)�x�>''(U%1k=Q��@`<_=��BK��o��ot���dMEF���:�M`���>�'T�����S����a��Ȣ���!o��%VF���E�D�> F�=�\��0��Q;r&��؃�>rٔ�H��=�/�����;�F�0B�+?# =\O<Od��-��<Ę[Q��"?:O08e�?£ �=?n��C-??   � �>In��0_l?   S�< =,�@c������$R7��ڮ�9���%jE���   �\~Ǿ-�|3�e�   �*
�Ę[Q��"?:O08e�?£ �=?n��C-??   � �>In��0_l?   S�< =�"�J��>X_)�x�>:�M`���>��E�D�>r&��؃�>�F�0B�+?Od��-��<��63X�=^F�xױ�><��jE�=[,�>��V���={m@��_�>C�6{臭=_z��_�>�\pC/@'	2�:��&�H�@��ķ��?e�U��?   LN3_DR_QD_ED      DRIF�aVO��>Sb����=�i���=��"��q=�W������ �V���=�%gs�X_)�x�>��5��k=Q��@`<_=m�Yj��o��ot����{"_F���a�?���>�|,sN���4Zh#W�t�m�?�����F%��D���E�D�>����=�\� y5�Q;�|ك�>�D��I��=a,�����;�F�0B�+?Fc7�\O<y�[�-��<όy1��"?:O08e�?�J�q��?n��C-??   . �>In��0_l?   ߇< =%�ͼ������$R7��b��EN��%jE���   d\~Ǿ-�|3�e�   @*
�όy1��"?:O08e�?�J�q��?n��C-??   . �>In��0_l?   ߇< =�aVO��>X_)�x�>a�?���>��E�D�>�|ك�>�F�0B�+?y�[�-��<��63X�=bF�xױ�>?��jE�=
[,�>��V���={m@��_�>C�6{臭=_z��_�>a��E�}/@D�E=��>���@y�]�U��?��Iw6 @   LN3_QD_BP_V01      QUAD��P�4�>u�����=l�kٚ=����m��Ei�'���.�K��=3 iM�A�-Z*\Pq�>�Ƽ�Ʌ�={��`672�?]��<��P��˜��=Rrm� ˻'�eB2�>��Ji-��1s-�����n��n��6��@neC����F?�>v6S䐆=4�e	�o=,�$9�*T;�= fك�>G@AM��=���B���;�F�0B�+?@�_O<�w�8.��<(�oFt@#?�a� ,�?Rj�Et?<�3��?   P �>In��0_l?   ��< =L�(�?��x��Y�������|�Y��   <Z~Ǿ-�|3�e�   `(
�(�oFt@#?�a� ,�?Rj�Et?<�3��?   P �>In��0_l?   ��< =��P�4�>-Z*\Pq�>'�eB2�>���F?�>�= fك�>�F�0B�+?�w�8.��<��U�Y�=�v�MƳ�>b�{vF�=A��ᐚ�>�"R�^��=��`q_�> �_�V��=�u:l_�><̜r�(0@#�?����]�8@|#�Sl��?��Iw6 @   LN3_COR      KICKER��P�4�>u�����=l�kٚ=����m��Ei�'���.�K��=3 iM�A�-Z*\Pq�>�Ƽ�Ʌ�={��`672�?]��<��P��˜��=Rrm� ˻'�eB2�>��Ji-��1s-�����n��n��6��@neC����F?�>v6S䐆=4�e	�o=,�$9�*T;�= fك�>G@AM��=���B���;�F�0B�+?@�_O<�w�8.��<(�oFt@#?�a� ,�?Rj�Et?<�3��?   P �>In��0_l?   ��< =L�(�?��x��Y�������|�Y��   <Z~Ǿ-�|3�e�   `(
�(�oFt@#?�a� ,�?Rj�Et?<�3��?   P �>In��0_l?   ��< =��P�4�>-Z*\Pq�>'�eB2�>���F?�>�= fك�>�F�0B�+?�w�8.��<��U�Y�=�v�MƳ�>b�{vF�=A��ᐚ�>�"R�^��=��`q_�> �_�V��=�u:l_�><̜r�(0@#�?����]�8@|#�Sl��?��Iw6 @   LN3_BPM      MONI��P�4�>u�����=l�kٚ=����m��Ei�'���.�K��=3 iM�A�-Z*\Pq�>�Ƽ�Ʌ�={��`672�?]��<��P��˜��=Rrm� ˻'�eB2�>��Ji-��1s-�����n��n��6��@neC����F?�>v6S䐆=4�e	�o=,�$9�*T;�= fك�>G@AM��=���B���;�F�0B�+?@�_O<�w�8.��<(�oFt@#?�a� ,�?Rj�Et?<�3��?   P �>In��0_l?   ��< =L�(�?��x��Y�������|�Y��   <Z~Ǿ-�|3�e�   `(
�(�oFt@#?�a� ,�?Rj�Et?<�3��?   P �>In��0_l?   ��< =��P�4�>-Z*\Pq�>'�eB2�>���F?�>�= fك�>�F�0B�+?�w�8.��<��U�Y�=�v�MƳ�>b�{vF�=A��ᐚ�>�"R�^��=��`q_�> �_�V��=�u:l_�><̜r�(0@#�?����]�8@|#�Sl��?�Mh�b� @   LN3_QD_BP_V01      QUAD2��ǐ��>ݎ�K���=xr�.�*�= q��5j��P��=���ޜ���=ؙ
���1/+[s��>��7���=�1�w�~��~s�)n���U(�X�=]�P��׻H/J����>�L+�-o����Yu��%L���s�(���A��,ASs�>�|f�G�=m�9Lg�=d�ׇW;Yq�Vڃ�>���W��=�܊���;�F�0B�+?}�UhO<�q/��<�����#?h�$R�Z ?LY�r�?Q�~ w�
?   �' �>In��0_l?   Ж< =8#�^ �˜u7�Y��t3V���:[�,	�   PQ~Ǿ-�|3�e�   h 
������#?h�$R�Z ?LY�r�?Q�~ w�
?   �' �>In��0_l?   Ж< =2��ǐ��>1/+[s��>H/J����>�,ASs�>Yq�Vڃ�>�F�0B�+?�q/��<1�*=[�=��b@Ե�>�mU�G�=3nFK}��>��('ֆ�=�D�_�>p�PΆ�=���_�>堫2��0@$��7**��h��F@�MJ���?����l� @   LN3_DR_QD_ED      DRIF�k� +�>�|$4�!�=e�P{�=�ؐ�)���[�"���\��=�g�@�!�1/+[s��>�Y3ߪ��=�1�w�~�>Ps�)n���U(�X�=������׻;��5���>����K6���i�ǎ����D��0�T:@k?��,ASs�>�D<�G�=m�9Lg�=�n�W;�%�ۃ�>k�;^i��=��U����;�F�0B�+?P0awO<g%W0��<9Jr9�z$?h�$R�Z ?�^�&��?Q�~ w�
?   �: �>In��0_l?   �< =����� �˜u7�Y�;�����
��:[�,	�   �B~Ǿ-�|3�e�   0
�9Jr9�z$?h�$R�Z ?�^�&��?Q�~ w�
?   �: �>In��0_l?   �< =�k� +�>1/+[s��>;��5���>�,ASs�>�%�ۃ�>�F�0B�+?g%W0��<x1�*=[�=i�b@Ե�>U�mU�G�=�nFK}��>��('ֆ�=�D�_�>p�PΆ�=���_�>��l��2@)-4��+�;�e� @��B�R�?d���ە@   LN3_DR_BP_V2      DRIF��z?�	?�����B>g��Oqn=���5������LS���ozm�j|�=B��>��1/+[s��>��Q�pa=�1�w�~�o�ݗ6n���U(�X�=�o ��׻6.�h�>`
��2H�=�'��w)=�a^ǭ�v=�Dk	hf;�,ASs�>�ρ�N�=m�9Lg�=�[_|�W;cl��)��>60,���=瘏JA��;�F�0B�+?+x�R<�q�u��<��*��<?h�$R�Z ?���e?Q�~ w�
?   E$�>In��0_l?   �J@ =�~��)6�˜u7�Y��W����:[�,	�   L%{Ǿ-�|3�e�   �K���*��<?h�$R�Z ?���e?Q�~ w�
?   E$�>In��0_l?   �J@ =��z?�	?1/+[s��>6.�h�>�,ASs�>cl��)��>�F�0B�+?�q�u��<�1�*=[�=��b@Ե�>��mU�G�==oFK}��>��('ֆ�=�D�_�>p�PΆ�=���_�>q�F㽞`@x��rYB�45X�\�@UNU�"��P�a�@   LN3_DR_QD_ED      DRIF ��8E
?��UÈ>]γ6��d=�����L���9��������`���=o �.�Y��1/+[s��>8'��*�]=�1�w�~�	���6n���U(�X�=��W��׻8��]@��>�R�
���=m�1�c�)=�٭;Px=�z��L�f;�,ASs�>K�W�N�=m�9Lg�=9���W;\��k+��>�$L�"��=� �FC��;�F�0B�+?8��R<,��v��<��N�J=?h�$R�Z ?�q���?Q�~ w�
?   HX$�>In��0_l?   �[@ =�@�K�m6�˜u7�Y�o�>y����:[�,	�   |{Ǿ-�|3�e�   X>���N�J=?h�$R�Z ?�q���?Q�~ w�
?   HX$�>In��0_l?   �[@ = ��8E
?1/+[s��>8��]@��>�,ASs�>\��k+��>�F�0B�+?,��v��<�1�*=[�=��b@Ե�>��mU�G�==oFK}��>��('ֆ�=�D�_�>p�PΆ�=���_�>�3��%a@_����B����@��ˮ��yd8�V�@   LN3_QD_BP_V02      QUAD0x�\�_
?�N�Q��#���uW=)눞	���d�Z	�����g��=���Qn���_�!h
�>�\?�J�P=2�אJj=�^P��ǀ=���u������ļ�;%T ��>Ix.�=�=�; �D*=��+<�y=F�^R�g;��4RX�>��r�e�%=�ug�=����Q�c;�%,��>e�6F)��=��BD��;�F�0B�+?#� O�R<ߴơw��<�&0K�i=?��˅?� ?��u�?�䆁��?   a$�>In��0_l?   �c@ =���v�6���˅?� �jk���� ,��   0{Ǿ-�|3�e�   l;��&0K�i=?��z���>��u�?�䆁��?   a$�>In��0_l?   �c@ =0x�\�_
?�_�!h
�>%T ��>��4RX�>�%,��>�F�0B�+?ߴơw��<���1K�=6�كƠ�>����8�=D��_���>p�U5v��=Ę��_�>�M�bn��=�P$�{_�>���qQ2a@����$@�%�X�@�����q�yd8�V�@   LN3_COR      KICKER0x�\�_
?�N�Q��#���uW=)눞	���d�Z	�����g��=���Qn���_�!h
�>�\?�J�P=2�אJj=�^P��ǀ=���u������ļ�;%T ��>Ix.�=�=�; �D*=��+<�y=F�^R�g;��4RX�>��r�e�%=�ug�=����Q�c;�%,��>e�6F)��=��BD��;�F�0B�+?#� O�R<ߴơw��<�&0K�i=?��˅?� ?��u�?�䆁��?   a$�>In��0_l?   �c@ =���v�6���˅?� �jk���� ,��   0{Ǿ-�|3�e�   l;��&0K�i=?��z���>��u�?�䆁��?   a$�>In��0_l?   �c@ =0x�\�_
?�_�!h
�>%T ��>��4RX�>�%,��>�F�0B�+?ߴơw��<���1K�=6�كƠ�>����8�=D��_���>p�U5v��=Ę��_�>�M�bn��=�P$�{_�>���qQ2a@����$@�%�X�@�����q�yd8�V�@   LN3_BPM      MONI0x�\�_
?�N�Q��#���uW=)눞	���d�Z	�����g��=���Qn���_�!h
�>�\?�J�P=2�אJj=�^P��ǀ=���u������ļ�;%T ��>Ix.�=�=�; �D*=��+<�y=F�^R�g;��4RX�>��r�e�%=�ug�=����Q�c;�%,��>e�6F)��=��BD��;�F�0B�+?#� O�R<ߴơw��<�&0K�i=?��˅?� ?��u�?�䆁��?   a$�>In��0_l?   �c@ =���v�6���˅?� �jk���� ,��   0{Ǿ-�|3�e�   l;��&0K�i=?��z���>��u�?�䆁��?   a$�>In��0_l?   �c@ =0x�\�_
?�_�!h
�>%T ��>��4RX�>�%,��>�F�0B�+?ߴơw��<���1K�=6�كƠ�>����8�=D��_���>p�U5v��=Ę��_�>�M�bn��=�P$�{_�>���qQ2a@����$@�%�X�@�����q���ǐL@   LN3_QD_BP_V02      QUADq��,q
?���)*�B�4I5=���g���V	˸���,�Tq�=��G�>3����2(�C�>��րɁQ=T�[.q��=���q���=�7�AJν>=<�K�;�OLp��>o%<.���=��KS+=����{=h�3wh;N"�,�L�>=DS�$,/=�=�,F�=�ѵ���k;��.��>�4��>��=����F��;�F�0B�+?�U���R<	��Ny��<KVc =?�=66�(?Ϭ����?I���T ?   �v$�>In��0_l?   Bw@ =��~��N6��=66�(����o���Xp�Y��   H{Ǿ-�|3�e�   /�KVc =?U�%
#?Ϭ����?I���T ?   �v$�>In��0_l?   Bw@ =q��,q
?��2(�C�>�OLp��>N"�,�L�>��.��>�F�0B�+?	��Ny��<6sO?O=�=�	�Ύ��>���+�=����w�>cZ*��=����_�>H�F�"��=	�F��_�>7`��`@������L@�-ݻz@�܄�ܘ�����>@   LN3_DR_QD_ED      DRIFP3. W�	?}:�T���̱���J��E��n����=R�U���ќ����=p�J�����2(�C�>v�=���Z=T�[.q��=|�@����=�7�AJν��K�;�+�/��>�w{���=ty�Ey�,=���E��}=\8�ߦi;N"�,�L�>0�<1,/=�=�,F�=E�U���k;�sP�1��>wM�o��= ��3L��;�F�0B�+?�}m�R<tmu�|��<�[9ٚ<?�=66�(?��rd�0?I���T ?   ��$�>In��0_l?   e�@ =������5��=66�(�B�$�U��Xp�Y��   �zǾ-�|3�e�   ���[9ٚ<?U�%
#?��rd�0?I���T ?   ��$�>In��0_l?   e�@ =P3. W�	?��2(�C�>�+�/��>N"�,�L�>�sP�1��>�F�0B�+?tmu�|��<�lO?O=�=��Ύ��>�����+�=D���w�>GcZ*��=@����_�>��F�"��=6�F��_�>\�+�}J`@�sʷIJL@ �p�@%��DJ�e�g-�H@   LN3_DR_BP_V3      DRIF\TT<�>��pQ' ��dw��,�������\%]'���ѲO
�o�=��W��E㻍�2(�C�>;C.d �=T�[.q��=[da���=�7�AJν#up�K�;}�R�n]�>��`��A�=X����@=О��1�=W_t�di~;N"�,�L�>	�i�-/=�=�,F�=�0���k;��$0���>"9�+ �=L��d���;�F�0B�+?�Mƾ�W<d�f����<���z<H*?�=66�(?�K����1?I���T ?   9*�>In��0_l?   ��E =���k�T$��=66�(�I�p,+�Xp�Y��   ��vǾ-�|3�e�   �1����z<H*?U�%
#?�K����1?I���T ?   9*�>In��0_l?   ��E =\TT<�>��2(�C�>}�R�n]�>N"�,�L�>��$0���>�F�0B�+?d�f����<%hO?O=�=O��Ύ��>ק���+�=���w�>dZ*��=����_�>ŒF�"��=�F��_�>�Kǡ��:@�a�x�9@�o|c|B@����z�1�Q�Lyt@   LN3_DR_QD_ED      DRIFq����I�>x�	΋���%�m�^����ߕ̈́�r��z����Z�/��%�=�؃��s⻍�2(�C�>��4����=T�[.q��=[	���=�7�AJνh�	�K�;۩+���>Mѯ�3��=�8��PA=�@��=�,=�H;N"�,�L�>
q�Э-/=�=�,F�=�L\��k;�{���>��LQ�=�4����;�F�0B�+?ğ74'X<ʂ� ���<M�A(=)?�=66�(?m�͠�.2?I���T ?   3E*�>In��0_l?   ӬE =�(��#��=66�(�W���+�Xp�Y��   DjvǾ-�|3�e�   ��M�A(=)?U�%
#?m�͠�.2?I���T ?   3E*�>In��0_l?   ӬE =q����I�>��2(�C�>۩+���>N"�,�L�>�{���>�F�0B�+?ʂ� ���<�hO?O=�=X��Ύ��>�����+�=���w�>UbZ*��=�����_�>��F�"��=��F��_�>Oq5g�8@�pY�!�8@b|���=C@hׁ�1�z/�o�@   LN3_QD_BP_V03      QUAD�K>���>\L@-z(�����!���i���-x���7ֲ᣽爍@��=���`���M�v���>�TÆY�=v���\�t=N'5�*��=��x*(ǽ�O
@��;� �d��>���{S�=���}ւA=_�� ��=+~4R-[;��7��>��2�=�Ź �Er=��,(�L;w�k���>���p�=��I����;�F�0B�+?��SBBX<�B����<�/�Pl(?��֗I ?8ܸ-c2?OPv� ?   ja*�>In��0_l?   +�E =4~?��"���֗I ���f��+�[���i��   �PvǾ-�|3�e�   ����/�Pl(?9�.�?8ܸ-c2?OPv� ?   ja*�>In��0_l?   +�E =�K>���>�M�v���>� �d��>��7��>w�k���>�F�0B�+?�B����<㎕m�?�=�kÛ��>.�?�.�=�q�e�z�>1|��<��=ޜ
rk[�>�?�#5��=\��Kf[�>�.��_ 7@�&gU�/@O��ۏ�C@���ba��z/�o�@   LN3_COR      KICKER�K>���>\L@-z(�����!���i���-x���7ֲ᣽爍@��=���`���M�v���>�TÆY�=v���\�t=N'5�*��=��x*(ǽ�O
@��;� �d��>���{S�=���}ւA=_�� ��=+~4R-[;��7��>��2�=�Ź �Er=��,(�L;w�k���>���p�=��I����;�F�0B�+?��SBBX<�B����<�/�Pl(?��֗I ?8ܸ-c2?OPv� ?   ja*�>In��0_l?   +�E =4~?��"���֗I ���f��+�[���i��   �PvǾ-�|3�e�   ����/�Pl(?9�.�?8ܸ-c2?OPv� ?   ja*�>In��0_l?   +�E =�K>���>�M�v���>� �d��>��7��>w�k���>�F�0B�+?�B����<㎕m�?�=�kÛ��>.�?�.�=�q�e�z�>1|��<��=ޜ
rk[�>�?�#5��=\��Kf[�>�.��_ 7@�&gU�/@O��ۏ�C@���ba��z/�o�@   LN3_BPM      MONI�K>���>\L@-z(�����!���i���-x���7ֲ᣽爍@��=���`���M�v���>�TÆY�=v���\�t=N'5�*��=��x*(ǽ�O
@��;� �d��>���{S�=���}ւA=_�� ��=+~4R-[;��7��>��2�=�Ź �Er=��,(�L;w�k���>���p�=��I����;�F�0B�+?��SBBX<�B����<�/�Pl(?��֗I ?8ܸ-c2?OPv� ?   ja*�>In��0_l?   +�E =4~?��"���֗I ���f��+�[���i��   �PvǾ-�|3�e�   ����/�Pl(?9�.�?8ܸ-c2?OPv� ?   ja*�>In��0_l?   +�E =�K>���>�M�v���>� �d��>��7��>w�k���>�F�0B�+?�B����<㎕m�?�=�kÛ��>.�?�.�=�q�e�z�>1|��<��=ޜ
rk[�>�?�#5��=\��Kf[�>�.��_ 7@�&gU�/@O��ۏ�C@���ba����>�d�@   LN3_QD_BP_V03      QUAD�1B���>�X����[�+�Ɔ�m��Ƿ�`��,ngk���1ڰJ�=�7_tc�R�oG��>yD�/��=�^X3��L�0��^R��=]�דT��݂9m��;���
�>O�0Jݽ�x��yA=,*Yc�=⫆�uJ;�f�I��>��`�����J�U!�k�c�DV�[[�p�� ���>��nT{�=�a�����;�F�0B�+?�
�KX<��C����<.U8F��'?'>�(}?��؄Y2?�5�\�?   ,j*�>In��0_l?   �E =�pX׌�"�'>�(}��j��+��5�\��   �FvǾ-�|3�e�   ���.U8F��'?,�.%g?��؄Y2?��d��j?   ,j*�>In��0_l?   �E =�1B���>R�oG��>���
�>�f�I��>p�� ���>�F�0B�+?��C����<�x�A�=� B���>���70�=�XE�`}�>X����z�=*�GW�>>�g��z�=Q �W�>�6@#��~[| @yiݮ��C@�x7_�@������@   LN3_DR_QD_ED      DRIF}��4���>�H�xx��A�酽z��%e8a�w�5����++��=m"�ӏ�R�oG��>ux�D���=�^X3��L�Ac/JR��=]�דT��.Z�m��;�2n"��>���I�ݽ��/��OA=:����=ל�� ;�f�I��>R������J�U!�k�kh:��[[�R�񥭄�>��т�=�����;�F�0B�+?KRX<tb"���<Wx�*&�'?'>�(}?1y��".2?�5�\�?   �o*�>In��0_l?   $�E =<��M"�'>�(}��y�\�+��5�\��   �AvǾ-�|3�e�   D��Wx�*&�'?,�.%g?1y��".2?��d��j?   �o*�>In��0_l?   $�E =}��4���>R�oG��>�2n"��>�f�I��>R�񥭄�>�F�0B�+?tb"���<ux�A�=�� B���>���70�=�XE�`}�>X����z�=*�GW�>>�g��z�=Q �W�>~�%�)U5@Lo��?8 @�!�YC@���gn�@��;@   LN3_DR_BP_V4      DRIFK�5�,�>��]N��OrЋ��tͪb�����g���j��T�=̽�r+߻R�oG��>Zk�0�4�=�^X3��L�O���Q��=]�דT��
-�ml��;��[$��>��j�
�۽a=yZ�@=DX��A�=�8Mˡ};�f�I��>�P������J�U!�k�ϝk��[[�~�����>����=I�	����;�F�0B�+?
+�oX<���D���<TVJ��%?'>�(}?�0*]�a1?�5�\�?   ��*�>In��0_l?   ��E =毇�!�'>�(}�8].��h*��5�\��   �)vǾ-�|3�e�   ���TVJ��%?,�.%g?�0*]�a1?��d��j?   ��*�>In��0_l?   ��E =K�5�,�>R�oG��>��[$��>�f�I��>~�����>�F�0B�+?���D���<ux�A�=�� B���>���70�=�XE�`}�>{����z�=A�GW�>`�g��z�=g �W�>�k��m62@)��5�@̾�y\�A@���pr6@�n�o��@   LN3_DR_BP_0      DRIFG�ә�>�+���Nὕ�R0|�8��]d�~���9}���{f� �=��5�2ܻR�oG��>Ń�vZ؇=�^X3��L�E��Q��=]�דT��6w�k��;��N�q��>�����ڽ�+z��?=CB�１�=7~FR�C|;�f�I��>�~r䱎��J�U!�k��jU\[��hu����>%[�S��=&��7���;�F�0B�+?���'�X<�>Jg���<�n��H$?'>�(}?�����0?�5�\�?   �*�>In��0_l?   �F =ٟ�6���'>�(}��6�!0)��5�\��   �vǾ-�|3�e�   ����n��H$?,�.%g?�����0?��d��j?   �*�>In��0_l?   �F =G�ә�>R�oG��>��N�q��>�f�I��>�hu����>�F�0B�+?�>Jg���<�x�A�=� B���>���70�=�XE�`}�>5����z�=�GW�>�g��z�=: �W�>J�4%Q�.@�k1�2n@� �S@@��͛�@��;@   LN3_DR_BP_BL      DRIFru��(5�>�	��ད�%��Pz�͜�yd�Z�ਨ��Fe�M�~�=�&���sۻR�oG��>�	,�6��=�^X3��L�A*oQ��=]�דT����}�k��;��>�M��>��˟Tڽk�[	�/?=����e�=a�~H$�{;�f�I��>���j����J�U!�k��Q�\[�wj�%���>���#��=������;�F�0B�+?snl�X<҆�����<@��04�#?'>�(}?/�Cڴb0?�5�\�?   ��*�>In��0_l?   �F =�/�8�/�'>�(}��8���(��5�\��   �vǾ-�|3�e�   T��@��04�#?,�.%g?/�Cڴb0?��d��j?   ��*�>In��0_l?   �F =ru��(5�>R�oG��>��>�M��>�f�I��>wj�%���>�F�0B�+?҆�����<�x�A�=4� B���>���70�=�XE�`}�>X����z�=*�GW�>>�g��z�=Q �W�>s#؟6U-@�@��@��=j��?@Op%$&�@AWf@   LN3_DR_SCA_ED      DRIF������>��?i;Aཽ��3�<w�ɂU��e�Т���F��-%��Ƶ=���T�6ڻR�oG��>a����=�^X3��L��d�FQ��=]�דT�����Vk��;r)�o�J�>�Ԅv��ٽA�O �>=y���l�=��sYfZ{;�f�I��>�]�����J�U!�k���\[�JA\*���>s�����=��l���;�F�0B�+?��>��X<��Z����<�,�~/#?'>�(}?~)/�0?�5�\�?   n�*�>In��0_l?   �F =6��K/�'>�(}����7�_(��5�\��   �vǾ-�|3�e�   ����,�~/#?,�.%g?~)/�0?��d��j?   n�*�>In��0_l?   �F =������>R�oG��>r)�o�J�>�f�I��>JA\*���>�F�0B�+?��Z����<\x�A�=�� B���>���70�=�XE�`}�>{����z�=A�GW�>`�g��z�=g �W�>n�/O%+@�ث�@��sJO�>@"<|�/
@� [�Z6@   LN3_SCA0      RFDF|�K W��>��`�;ؽ������R�a(eZ�h���PL�j����}2h�=����GһR�oG��>�X��ƣ�=�^X3��L����BP��=]�דT���&ۗi��;L�A��>�`�C�ֽ��#�y:=��n�F�=<��i�w;�f�I��>���d���J�U!�k�G�� E\[��@^����>0!��>�=�mB��;�F�0B�+?Z��X<�΄���<HD#�?'>�(}?R�
aO�+?�5�\�?   )�*�>In��0_l?   RF =�mim���'>�(}�QL�.%��5�\��   ��uǾ-�|3�e�   �z�HD#�?,�.%g?R�
aO�+?��d��j?   )�*�>In��0_l?   RF =|�K W��>R�oG��>L�A��>�f�I��>�@^����>�F�0B�+?�΄���<ux�A�=�� B���>���70�=�XE�`}�>G����z�=�GW�>,�g��z�=E �W�>- s^]@�E�Q;@%U��N�7@�^-��@� [�Z6@   LN3_SZW      WAKE|�K W��>���$=ؽ������R��:�H\�h��#d[�j�������V�=W(��GһK*��I��>���ȣ�="����L��"0P��={I։�	��ȁ��j��;L�A��>���E�ֽ\�0N�y:=��!{.�=P���i�w;|!�VK��>v3����SI��Fk�o��RF\[�%TՊ���>��\��=Z}�y��;�'��ճ+?@^W��<��Ą���<HD#�?�,�*}?R�
aO�+?Ui�?   ��*�>���al?   RF =�mim����,�*}�QL�.%�Ui��   ��uǾ���e�   �z�HD#�?���O'g?R�
aO�+?�\R�j?   ��*�>���al?   RF =|�K W��>K*��I��>L�A��>|!�VK��>%TՊ���>�'��ճ+?��Ą���<s����A�=�������>N�@��/�=�r7�|�>�F$��z�=I��
W�>#��Ίz�=����W�>|q�]@���x�@��y�M�7@�f��@� [�Z6@   LN3_STW      TRWAKE|�K W��> o%=ؽ������R��U-H\�h��#d[�j�������V�=W(��Gһh�v�I��>X�.ȣ�=a���L��:�3P��=Uu��	�����j��;L�A��>�%��E�ֽ\�0N�y:=��!{.�=P���i�w;�)�VK��>0����Y�b��Fk�"*~ZF\[�%TՊ���>��\��=Z}�y��;�'��ճ+?@^W��<��Ą���<HD#�?H4ɨ*}?R�
aO�+?�ci�?   ��*�>���al?   RF =�mim���H4ɨ*}�QL�.%��ci��   ��uǾ���e�   �z�HD#�?�ԣP'g?R�
aO�+?��O�j?   ��*�>���al?   RF =|�K W��>h�v�I��>L�A��>�)�VK��>%TՊ���>�'��ճ+?��Ą���<�~)��A�=�����>�����/�=���:�|�>�k$��z�=���
W�>*��Ίz�=�а�W�>EE}m�]@��lv�@�wy�M�7@�f��@�n�o��@   LN3_DR_SCA_ED      DRIFp�_mw�>�'1�s�ֽ/��n�?�`J-z�i�5יJ����7����=˯��ѻh�v�I��>���@}�=a���L�*P��=Uu��	��E�]�j��;��c�M�>?*B ֽ��<0�9=1�X"��=lU�̩"w;�)�VK��>��kr���Y�b��Fk��M\[�{U����>��� ��=�����;�'��ճ+?�u�l��<+�[h���<��x�3?H4ɨ*}?E�o
Z0+?�ci�?   �+�>���al?   �[F =H�6w��H4ɨ*}���ʦ�$��ci��   ��uǾ���e�   �q���x�3?�ԣP'g?E�o
Z0+?��O�j?   �+�>���al?   �[F =p�_mw�>h�v�I��>��c�M�>�)�VK��>{U����>�'��ճ+?+�[h���<�~)��A�=�����>�����/�=���:�|�>*l$��z�=Ż�
W�>p��Ίz�=�а�W�>?�!�F@h�_�
@@�w�6@��ƽ�@��;@   LN3_DR_BP_BL      DRIF[�����>���-�ս�C*��W�&?;�	�i�v��+4����l��x=<��$�Lлh�v�I��>f�f�=a���L�I}��O��=Uu��	��彼mj��;O���>��_�սךT�ku9=5���,��=��yC �v;�)�VK��>Y������Y�b��Fk��r�Q\[���+���>.�2�(��=a���;�'��ճ+?PMڮ��<il����<�����`?H4ɨ*}?�縢F�*?�ci�?   Y+�>���al?   �aF =�OB'r)�H4ɨ*}���F��M$��ci��   ��uǾ���e�   �l������`?�ԣP'g?�縢F�*?��O�j?   Y+�>���al?   �aF =[�����>h�v�I��>O���>�)�VK��>��+���>�'��ճ+?il����<�~)��A�=������>�����/�=���:�|�>l$��z�=���
W�>^��Ίz�=�а�W�>
��%܀@b��Xtj@@W�"6@���B�@�n�o��@   LN3_DR_BP_0      DRIF��a���>e'�ҽ�����PW=C �HXk�G/�1�Í��΀���s}��˦ʻh�v�I��>:/�ތ	�=a���L���v�O��=Uu��	���qk�i��;#A�f��>���֟Խ?��W�7=�n���=�[��lu;�)�VK��>:����Y�b��Fk���
9a\[��䖝���>�9L��=D����;�'��ճ+?�R����<1Ҽ���<����M?H4ɨ*}?���1)?�ci�?   |&+�>���al?   �yF =�c�]��H4ɨ*}���5�e#��ci��   ��uǾ���e�   8W�����M?�ԣP'g?���1)?��O�j?   |&+�>���al?   �yF =��a���>h�v�I��>#A�f��>�)�VK��>�䖝���>�'��ճ+?1Ҽ���<�~)��A�=�����>�����/�=���:�|�>�k$��z�=���
W�><��Ίz�=�а�W�>�^̯	@^�W���@�C���3@��fDkc@��;@	   LN3_DR_BP      DRIF.�i��>��$̽qȑw�?m=�b�@��m�ޑ��̓��Z�L�����ƺ��h�v�I��>]E�~�=a���L��t�N��=Uu��	��X��h��;:ߩ��M�>����ҽ!�p$��5=F�^���=z�=r�_s;�)�VK��>>�GT(��Y�b��Fk��҃y\[�H�IH���>f�Հ��=�NN���;�'��ճ+?�uEB��<w��G���<�X�5�!?H4ɨ*}?!����&?�ci�?   �M+�>���al?   d�F =0F��?%�H4ɨ*}����@!��ci��   �tuǾ���e�   L7��X�5�!?�ԣP'g?!����&?��O�j?   �M+�>���al?   d�F =.�i��>h�v�I��>:ߩ��M�>�)�VK��>H�IH���>�'��ճ+?w��G���<�~)��A�=������>�����/�=���:�|�>l$��z�=���
W�>^��Ίz�=�а�W�>¯f�jo@�Wu�O@�w�͞0@��&�n@hY	4%@   LN3_DR_BP_VS      DRIF���o �>�D��Ƚ���Tiq=yЉ��<n�>��\�z��+�Z粽��ͅ߂��h�v�I��>��gmP�=a���L�7^��N��=Uu��	��l��wh��;	zG�|W�>*T�~�8ҽ�
Q*�4={�v*z��=�����r;�)�VK��>J[�k1��Y�b��Fk������\[���5��>��kp���=�.s���;�'��ճ+?��*���<(�0Y���<D��W�{?H4ɨ*}?Ջ'�]&?�ci�?   �Z+�>���al?   L�F =nUI�j��H4ɨ*}��GyM� ��ci��   �huǾ���e�   �,�D��W�{?�ԣP'g?Ջ'�]&?��O�j?   �Z+�>���al?   L�F =���o �>h�v�I��>	zG�|W�>�)�VK��>��5��>�'��ճ+?(�0Y���<�~)��A�=�����>�����/�=���:�|�>l$��z�=���
W�>M��Ίz�=�а�W�>b�4�.9@�7���@���:/@?�f���@�F�3, @	   LN3_DR_BP      DRIF�;gK��>gd���@�e9�Vy=���4p�uC�#�j����E+ ��T�\��U��h�v�I��>��-͕ń=a���L���<N��=Uu��	��6u�|g��;�HP[w�>��f�vkн��?K��2=�ˠ��=5�F�c�p;�)�VK��>KKG�L��Y�b��Fk���ۙ\[����+Ƅ�>��?Ǜ�=��՗��;�'��ճ+?�2�Q"�<�N���<���}�	?H4ɨ*}?���W�#?�ci�?   ��+�>���al?   ��F =���!.��H4ɨ*}�;)������ci��   �DuǾ���e�   �����}�	?�ԣP'g?���W�#?��O�j?   ��+�>���al?   ��F =�;gK��>h�v�I��>�HP[w�>�)�VK��>���+Ƅ�>�'��ճ+?�N���<�~)��A�=�����>�����/�=���:�|�>l$��z�=���
W�>M��Ίz�=�а�W�>~Ӿ�u/�?A�9M��?������)@��&���@�F�3, @   LN3_BP_WA_OU      WATCH�;gK��>gd���@�e9�Vy=���4p�uC�#�j����E+ ��T�\��U��h�v�I��>��-͕ń=a���L���<N��=Uu��	��6u�|g��;�HP[w�>��f�vkн��?K��2=�ˠ��=5�F�c�p;�)�VK��>KKG�L��Y�b��Fk���ۙ\[����+Ƅ�>��?Ǜ�=��՗��;�'��ճ+?�2�Q"�<�N���<���}�	?H4ɨ*}?���W�#?�ci�?   ��+�>���al?   ��F =���!.��H4ɨ*}�;)������ci��   �DuǾ���e�   �����}�	?�ԣP'g?���W�#?��O�j?   ��+�>���al?   ��F =�;gK��>h�v�I��>�HP[w�>�)�VK��>���+Ƅ�>�'��ճ+?�N���<�~)��A�=�����>�����/�=���:�|�>l$��z�=���
W�>M��Ίz�=�а�W�>~Ӿ�u/�?A�9M��?������)@��&���@