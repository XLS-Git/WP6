SDDS1
!# little-endian
&description text="sigma matrix--input: xls_linac_hbp_3.track.ele  lattice: xls_linac.13.lte", contents="sigma matrix", &end
&parameter name=Step, description="Simulation step", type=long, &end
&parameter name=SVNVersion, description="SVN version number", type=string, fixed_value=unknown, &end
&column name=s, units=m, description=Distance, type=double,  &end
&column name=ElementName, description="Element name", format_string=%10s, type=string,  &end
&column name=ElementOccurence, description="Occurence of element", format_string=%6ld, type=long,  &end
&column name=ElementType, description="Element-type name", format_string=%10s, type=string,  &end
&column name=s1, symbol="$gs$r$b1$n", units=m, description="sqrt(<x*x>)", type=double,  &end
&column name=s12, symbol="$gs$r$b12$n", units=m, description="<x*xp'>", type=double,  &end
&column name=s13, symbol="$gs$r$b13$n", units="m$a2$n", description="<x*y>", type=double,  &end
&column name=s14, symbol="$gs$r$b14$n", units=m, description="<x*y'>", type=double,  &end
&column name=s15, symbol="$gs$r$b15$n", units="m$a2$n", description="<x*s>", type=double,  &end
&column name=s16, symbol="$gs$r$b16$n", units=m, description="<x*delta>", type=double,  &end
&column name=s17, symbol="$gs$r$b17$n", units="m*s", description="<x*t>", type=double,  &end
&column name=s2, symbol="$gs$r$b2$n", description="sqrt(<x'*x'>)", type=double,  &end
&column name=s23, symbol="$gs$r$b23$n", units=m, description="<x'*y>", type=double,  &end
&column name=s24, symbol="$gs$r$b24$n", description="<x'*y'>", type=double,  &end
&column name=s25, symbol="$gs$r$b25$n", units=m, description="<x'*s>", type=double,  &end
&column name=s26, symbol="$gs$r$b26$n", description="<x'*delta>", type=double,  &end
&column name=s27, symbol="$gs$r$b27$n", units=s, description="<x'*t>", type=double,  &end
&column name=s3, symbol="$gs$r$b3$n", units=m, description="sqrt(<y*y>)", type=double,  &end
&column name=s34, symbol="$gs$r$b34$n", units=m, description="<y*y'>", type=double,  &end
&column name=s35, symbol="$gs$r$b35$n", units="m$a2$n", description="<y*s>", type=double,  &end
&column name=s36, symbol="$gs$r$b36$n", units=m, description="<y*delta>", type=double,  &end
&column name=s37, symbol="$gs$r$b37$n", units="m*s", description="<y*t>", type=double,  &end
&column name=s4, symbol="$gs$r$b4$n", description="sqrt(<y'*y')>", type=double,  &end
&column name=s45, symbol="$gs$r$b45$n", units=m, description="<y'*s>", type=double,  &end
&column name=s46, symbol="$gs$r$b46$n", description="<y'*delta>", type=double,  &end
&column name=s47, symbol="$gs$r$b47$n", units=s, description="<y'*t>", type=double,  &end
&column name=s5, symbol="$gs$r$b5$n", units=m, description="sqrt(<s*s>)", type=double,  &end
&column name=s56, symbol="$gs$r$b56$n", units=m, description="<s*delta>", type=double,  &end
&column name=s57, symbol="$gs$r$b57$n", units="m*s", description="<s*t>", type=double,  &end
&column name=s6, symbol="$gs$r$b6$n", description="sqrt(<delta*delta>)", type=double,  &end
&column name=s67, symbol="$gs$r$b67$n", units=s, description="<delta*t>", type=double,  &end
&column name=s7, symbol="$gs$r$b7$n", description="sqrt(<t*t>)", type=double,  &end
&column name=ma1, symbol="max$sb$ex$sb$e", units=m, description="maximum absolute value of x", type=double,  &end
&column name=ma2, symbol="max$sb$ex'$sb$e", description="maximum absolute value of x'", type=double,  &end
&column name=ma3, symbol="max$sb$ey$sb$e", units=m, description="maximum absolute value of y", type=double,  &end
&column name=ma4, symbol="max$sb$ey'$sb$e", description="maximum absolute value of y'", type=double,  &end
&column name=ma5, symbol="max$sb$e$gD$rs$sb$e", units=m, description="maximum absolute deviation of s", type=double,  &end
&column name=ma6, symbol="max$sb$e$gd$r$sb$e", description="maximum absolute value of delta", type=double,  &end
&column name=ma7, symbol="max$sb$e$gD$rt$sb$e", units=s, description="maximum absolute deviation of t", type=double,  &end
&column name=minimum1, symbol="x$bmin$n", units=m, type=double,  &end
&column name=minimum2, symbol="x'$bmin$n", units=m, type=double,  &end
&column name=minimum3, symbol="y$bmin$n", units=m, type=double,  &end
&column name=minimum4, symbol="y'$bmin$n", units=m, type=double,  &end
&column name=minimum5, symbol="$gD$rs$bmin$n", units=m, type=double,  &end
&column name=minimum6, symbol="$gd$r$bmin$n", units=m, type=double,  &end
&column name=minimum7, symbol="t$bmin$n", units=s, type=double,  &end
&column name=maximum1, symbol="x$bmax$n", units=m, type=double,  &end
&column name=maximum2, symbol="x'$bmax$n", units=m, type=double,  &end
&column name=maximum3, symbol="y$bmax$n", units=m, type=double,  &end
&column name=maximum4, symbol="y'$bmax$n", units=m, type=double,  &end
&column name=maximum5, symbol="$gD$rs$bmax$n", units=m, type=double,  &end
&column name=maximum6, symbol="$gd$r$bmax$n", units=m, type=double,  &end
&column name=maximum7, symbol="t$bmax$n", units=s, type=double,  &end
&column name=Sx, symbol="$gs$r$bx$n", units=m, description=sqrt(<(x-<x>)^2>), type=double,  &end
&column name=Sxp, symbol="$gs$r$bx'$n", description=sqrt(<(x'-<x'>)^2>), type=double,  &end
&column name=Sy, symbol="$gs$r$by$n", units=m, description=sqrt(<(y-<y>)^2>), type=double,  &end
&column name=Syp, symbol="$gs$r$by'$n", description=sqrt(<(y'-<y'>)^2>), type=double,  &end
&column name=Ss, symbol="$gs$r$bs$n", units=m, description=sqrt(<(s-<s>)^2>), type=double,  &end
&column name=Sdelta, symbol="$gs$bd$n$r", description=sqrt(<(delta-<delta>)^2>), type=double,  &end
&column name=St, symbol="$gs$r$bt$n", units=s, description=sqrt(<(t-<t>)^2>), type=double,  &end
&column name=ex, symbol="$ge$r$bx$n", units=m, description="geometric horizontal emittance", type=double,  &end
&column name=enx, symbol="$ge$r$bx,n$n", units=m, description="normalized horizontal emittance", type=double,  &end
&column name=ecx, symbol="$ge$r$bx,c$n", units=m, description="geometric horizontal emittance less dispersive contributions", type=double,  &end
&column name=ecnx, symbol="$ge$r$bx,cn$n", units=m, description="normalized horizontal emittance less dispersive contributions", type=double,  &end
&column name=ey, symbol="$ge$r$by$n", units=m, description="geometric vertical emittance", type=double,  &end
&column name=eny, symbol="$ge$r$by,n$n", units=m, description="normalized vertical emittance", type=double,  &end
&column name=ecy, symbol="$ge$r$by,c$n", units=m, description="geometric vertical emittance less dispersive contributions", type=double,  &end
&column name=ecny, symbol="$ge$r$by,cn$n", units=m, description="normalized vertical emittance less dispersive contributions", type=double,  &end
&column name=betaxBeam, symbol="$gb$r$bx,beam$n", units=m, description="betax for the beam, excluding dispersive contributions", type=double,  &end
&column name=alphaxBeam, symbol="$ga$r$bx,beam$n", description="alphax for the beam, excluding dispersive contributions", type=double,  &end
&column name=betayBeam, symbol="$gb$r$by,beam$n", units=m, description="betay for the beam, excluding dispersive contributions", type=double,  &end
&column name=alphayBeam, symbol="$ga$r$by,beam$n", description="alphay for the beam, excluding dispersive contributions", type=double,  &end
&data mode=binary, &end
"                 _BEG_      MARKB����,�>�OBB_@׽{ ʓL������o}=Kl�bq�=���~�z'�c��"�3�;ぶ_=��>�+��z�y=����ks�'X�ve?����9��W'>��rg�λ�J�.�Q�>R?t�8Eн�(�	z52��e�c���� ��2Mp��xJa���>�Wܰ�=�Jg�f�z=o�`{�M;o�`fƈ�>j��g꽖�ZƠ��;=�����8?n���]b'���Í���<�*Wk�0??���3?2�����(?�%{��?   �>r>�!�j?   ���!=J��R"�f24����2�����(��W&�|�
�   ��.ɾr>�!�j�   Ծ���*Wk�0??���3?��s3#?�%{��?   �>�$��i?   ���!=B����,�>ぶ_=��>�J�.�Q�>�xJa���>o�`fƈ�>=�����8?��Í���<�Ep�^�=��@^u��><ޜ��=D�Mx��>�I�턭=? "�Z�>W���섭=Z��U�Z�>M����?�oW���?����H)@�2�A�@           BNCH      CHARGEB����,�>�OBB_@׽{ ʓL������o}=Kl�bq�=���~�z'�c��"�3�;ぶ_=��>�+��z�y=����ks�'X�ve?����9��W'>��rg�λ�J�.�Q�>R?t�8Eн�(�	z52��e�c���� ��2Mp��xJa���>�Wܰ�=�Jg�f�z=o�`{�M;o�`fƈ�>j��g꽖�ZƠ��;=�����8?n���]b'���Í���<�*Wk�0??���3?2�����(?�%{��?   �>r>�!�j?   ���!=J��R"�f24����2�����(��W&�|�
�   ��.ɾr>�!�j�   Ծ���*Wk�0??���3?��s3#?�%{��?   �>�$��i?   ���!=B����,�>ぶ_=��>�J�.�Q�>�xJa���>o�`fƈ�>=�����8?��Í���<�Ep�^�=��@^u��><ޜ��=D�Mx��>�I�턭=? "�Z�>W���섭=Z��U�Z�>M����?�oW���?����H)@�2�A�@        
   FITT_HBP_5      MARKB����,�>�OBB_@׽{ ʓL������o}=Kl�bq�=���~�z'�c��"�3�;ぶ_=��>�+��z�y=����ks�'X�ve?����9��W'>��rg�λ�J�.�Q�>R?t�8Eн�(�	z52��e�c���� ��2Mp��xJa���>�Wܰ�=�Jg�f�z=o�`{�M;o�`fƈ�>j��g꽖�ZƠ��;=�����8?n���]b'���Í���<�*Wk�0??���3?2�����(?�%{��?   �>r>�!�j?   ���!=J��R"�f24����2�����(��W&�|�
�   ��.ɾr>�!�j�   Ծ���*Wk�0??���3?��s3#?�%{��?   �>�$��i?   ���!=B����,�>ぶ_=��>�J�.�Q�>�xJa���>o�`fƈ�>=�����8?��Í���<�Ep�^�=��@^u��><ޜ��=D�Mx��>�I�턭=? "�Z�>W���섭=Z��U�Z�>M����?�oW���?����H)@�2�A�@ã	�17�?   HBP_DP_SEPM2   	   CSRCSBEND�5���>�� �Խ��0?���wE���w=_.4�:�=e?���E]��J��;vgV���>�zY�Mr=�5.���r�'X� ���#t7#�C>	��#��C�w�>�8@�-�̽�Z�&.H��3��|v�P�(|����r;�ݶ��>*(����2=�w~��]=8v#3Q�p;�$��ܹ>7�vdc#����8C���;�2��=<?����c�4�~�| *�<y�4�E?0}�:t�$?A�魀O&?�vr���?   �"Q�>��� �k?   c�g =r-��a�0}�:t�$�A�魀O&�����
�   ج;Ͼ��� �k�   ���y�4�E?9"�vW?�+� �3!?�vr���?   �"Q�>I�)B��i?   c�g =�5���>vgV���>��C�w�>r;�ݶ��>�$��ܹ>�2��=<?~�| *�<L������=v��R5B�>Q?��i�=��O����>��=��pJ Z�>�2��=��< Z�>���@�r�?��PI��?�f���G$@���)d@ã	�17�?   HBP_DP_SEPM2   	   CSRCSBEND��]���>Y	ȽB��=��<�����=r=��k?s����qw[:(>���Φл���?��ӻ�j=E?��{;s�� Wl����� \Z�qS>YF-Z����W#���>]���_ɽ�H�{Y�P����@�=�R���3��r�C�Ê�>�����>=�B�>H}��0�ϡn{;!T�����>���*q���60�\�;�Gi���??�@�ﶚ8��0��<��?��h	?��*<�1?�~L��#?�G�X�?   ��b�>��@A-l?   ��$=��?��h	���*<�1��~L��#�Jy,<e�
�   �t�Ͼ��@A-l�   �GI�UlY��> �,?�lT�g?�G�X�?   ��b�>m�k�hNj?   ��$=��]���>���?�W#���>r�C�Ê�>!T�����>�Gi���??�0��<��$40E�=��Nx؇�>
i�6��=	ˢɀ!�>*.�턭=Y�mY�>/{K~섭=��)mY�>�X��	��?��B�AӿA{�ä�@�]�m�@\=�����?   HBP_DR_BP_C      CSRDRIFTģ�a�Y�>8C��"�>&�lh����=czh=J��c=��������A>��n�������?S�X�I^=E?��{;s��~%��~�IE�S>G#��$����S����>}�>�q�Ž�%��"M�����=8sm����r�C�Ê�>��O��>=�g)N/�v�e��Z;p{;��h���>u'T���. ^�;SЫ>@?& ^�Y�7� -�����<��B7� ?��*<�1?�;(�;;!?�G�X�?   ��b�>�	u�2l?   p�$=��B7� ���*<�1��;(�;;!�Jy,<e�
�   �Q�Ͼ�	u�2l�   �BJ�}ܠ ~�? �,?�2X`i?�G�X�?   ��b�>?��%j?   p�$=ģ�a�Y�>���?�S����>r�C�Ê�>��h���>SЫ>@? -�����<}�$40E�=J�㏇�>(�_Px~�=5�`�G̎>@.�턭=����!Y�>�G��섭=L�!Y�>���V�?ˁ���Q�#@�naU�@{k�25�?	   HBP_DR_BP      DRIF���ġD�>B�RZ�>?l��~�Z�L��Y=IX�[����I��M>�TN�1������?�Ĕ�n�;=E?��{;s�E��{��~�IE�S>��������Ţ1�>��hn�6½Z�Y�1�H�լŐ�Z�=�weC���r�C�Ê�>��TV�>=�g)N/�v�11���q{;�j�Ѩ��>�]��.���4mȸ�_�;SЫ>@?Y����7��Z�����<�J̒�(?��*<�1?{���b?�G�X�?   @c�>�	u�2l?   *�$=�J̒�(���*<�1�{���b�Jy,<e�
�    h�Ͼ�	u�2l�   �=K�rYs�+0#? �,?�[�j?�G�X�?   @c�>?��%j?   *�$=���ġD�>���?�Ţ1�>r�C�Ê�>�j�Ѩ��>SЫ>@?�Z�����<}�$40E�=J�㏇�>��_Px~�={�`�G̎>7.�턭={���!Y�>G��섭=L�!Y�>Q���]��?8LA��� ������@%u�vľ@H8�[��?	   HBP_DR_BP      DRIFP䑑?~biP�X>�#{*1�}��	�Tc&=�k"�9��Fs;(�T>Y � f�����?�MM'P�E?��{;s��[4���~�IE�S>�WQ����8�㍊��>��%U�D���ku�C�I����=��Eہ�r�C�Ê�>3jR��>=�g)N/�v��WVns{;@j l���>���\������`�;SЫ>@?�>H*��7�[H����<����1?��*<�1?6k�[O?�G�X�?   �c�>�	u�2l?   ��$=����1���*<�1�6k�[O�Jy,<e�
�   @~�Ͼ�	u�2l�   �8L��D��+? �,?
>�_(l?�G�X�?   �c�>?��%j?   ��$=P䑑?���?8�㍊��>r�C�Ê�>@j l���>SЫ>@?[H����<4�$40E�=�㏇�>o�_Px~�=_�`�G̎>9.�턭=}���!Y�>�G��섭=L�!Y�>��,p�
@�[.�4���f�f@#�
�g��?�(���?	   HBP_DR_BP      DRIF@�6��?x��&�� >EJ3ZP���=y�GT��'�|��Ľ�щ�Z>?ѐ*=�����?��$��c�E?��{;s��E��(��~�IE�S>(	����qb����>�zͥ��j����>�#���[y=�V���y{�r�C�Ê�>t����>=�g)N/�v���9�u{;�f8���>*fP����+B�Zb�;SЫ>@? �{��7�!N;���<Q n{��6?��*<�1?��v3[;?�G�X�?   �2c�>�	u�2l?   �%=Q n{��6���*<�1���v3[;�Jy,<e�
�   ���Ͼ�	u�2l�   �3M���O�1? �,?�
��?�G�X�?   �2c�>?��%j?   �%=@�6��?���?qb����>r�C�Ê�>�f8���>SЫ>@?!N;���<�$40E�=^�㏇�>��_Px~�=̃`�G̎>7.�턭={���!Y�>G��섭=L�!Y�>����@C������<cQ�� @�/�F��?�7ԍ� @	   HBP_DR_V6      DRIFuh ԾR?a�reHd$>L�{!��.����d������7ɽ.��J�`>���1�����?�W�b>Fn�E?��{;s�uVZ��~�IE�S>������K5�0��>�����������r�.6�n}i���r=˦���s�r�C�Ê�>�<f9��>=�g)N/�v��N��v{;Ugp勡�>�zpUf����}�c�;SЫ>@?=(I�8��̉Ot��<!L�N�;?��*<�1?�����?�G�X�?   �Dc�>�	u�2l?   �7%=!L�N�;���*<�1�������Jy,<e�
�   ���Ͼ�	u�2l�   �N��m�b�5? �,?�B(��y?�G�X�?   �Dc�>?��%j?   �7%=uh ԾR?���?K5�0��>r�C�Ê�>Ugp勡�>SЫ>@?�̉Ot��<�$40E�=��㏇�>`Px~�=��`�G̎>6.�턭={���!Y�>~G��섭=L�!Y�>��N�T@Q�s��(�jU^���?#�M{���?x�D��� @   HBP_DR_QD_ED      DRIF���{�?X�$�_�$>��"�s���R����nf��5
��ɽ�.���`>t��p5����?�R�`��o�E?��{;s�א|����~�IE�S>�i�����U뛔���>	��6�����̳��4��}}��q=�p��ɱr�r�C�Ê�>3pbЫ>=�g)N/�v�)rp�v{;�����>}�}������oa}�c�;SЫ>@?+�Ō�8��O����<�p����<?��*<�1?-LL�t�?�G�X�?   �Gc�>�	u�2l?   �<%=�p����<���*<�1�-LL�t��Jy,<e�
�   �Ͼ�	u�2l�   X?N���A�x6? �,?*���=X?�G�X�?   �Gc�>?��%j?   �<%=���{�?���?U뛔���>r�C�Ê�>�����>SЫ>@?�O����<�$40E�=^�㏇�>��_Px~�='�`�G̎>6.�턭={���!Y�>~G��섭=L�!Y�>��L�} @��J�٭�`���j�?�!՛d�?ʓcP�G@
   HBP_QD_V05      QUAD"�!?ϱ�(�>F�};��.)p�aw�#�1�[ʽfB�]�`>�1��M����:��>T�#!U/U��7E��Vq�K	��$�����yQD>N�ý���<�C�|�>��{	�a��K0R	z�3��de�:	q=]���O�q�qt�~���>�_O�u�8=tv����q��s���Dv;ڎ�|á�>�7��P���ؚ��c�;SЫ>@?�8P%�8�F�S���<λ�0=?�Tư(#?��{��r
?�%1'
?   �Hc�>�	u�2l?   �>%=λ�0=��Tư(#���{��r
�T8ZP���   0ѕϾ�	u�2l�   �QN� ��#�6?��DX�&?�%w��d?�%1'
?   �Hc�>?��%j?   �>%="�!?��:��><�C�|�>qt�~���>ڎ�|á�>SЫ>@?F�S���<�_i) @�=��n)��>�ƅ�&z�=5#0X�Ǝ>� 7&��=�a/4GY�>�5��%��=BJ�FY�>|��D!@��S�_����7j�?�]�k��?ʓcP�G@   HBP_COR      KICKER"�!?ϱ�(�>F�};��.)p�aw�#�1�[ʽfB�]�`>�1��M����:��>T�#!U/U��7E��Vq�K	��$�����yQD>N�ý���<�C�|�>��{	�a��K0R	z�3��de�:	q=]���O�q�qt�~���>�_O�u�8=tv����q��s���Dv;ڎ�|á�>�7��P���ؚ��c�;SЫ>@?�8P%�8�F�S���<λ�0=?�Tư(#?��{��r
?�%1'
?   �Hc�>�	u�2l?   �>%=λ�0=��Tư(#���{��r
�T8ZP���   0ѕϾ�	u�2l�   �QN� ��#�6?��DX�&?�%w��d?�%1'
?   �Hc�>?��%j?   �>%="�!?��:��><�C�|�>qt�~���>ڎ�|á�>SЫ>@?F�S���<�_i) @�=��n)��>�ƅ�&z�=5#0X�Ǝ>� 7&��=�a/4GY�>�5��%��=BJ�FY�>|��D!@��S�_����7j�?�]�k��?ʓcP�G@   HBP_BPM      MONI"�!?ϱ�(�>F�};��.)p�aw�#�1�[ʽfB�]�`>�1��M����:��>T�#!U/U��7E��Vq�K	��$�����yQD>N�ý���<�C�|�>��{	�a��K0R	z�3��de�:	q=]���O�q�qt�~���>�_O�u�8=tv����q��s���Dv;ڎ�|á�>�7��P���ؚ��c�;SЫ>@?�8P%�8�F�S���<λ�0=?�Tư(#?��{��r
?�%1'
?   �Hc�>�	u�2l?   �>%=λ�0=��Tư(#���{��r
�T8ZP���   0ѕϾ�	u�2l�   �QN� ��#�6?��DX�&?�%w��d?�%1'
?   �Hc�>?��%j?   �>%="�!?��:��><�C�|�>qt�~���>ڎ�|á�>SЫ>@?F�S���<�_i) @�=��n)��>�ƅ�&z�=5#0X�Ǝ>� 7&��=�a/4GY�>�5��%��=BJ�FY�>|��D!@��S�_����7j�?�]�k��?L��ș@
   HBP_QD_V05      QUAD����(?��=~��=s���hф���
jՁ�v�1�ʽt�����`>��%e������' �>��>��W=�S }��a�zd��p���CaO >�o��̬�f�XH�@�>pUսg��Y^�[G�2�
�W�kp=��C���p�-%Bw��>����Y3=2��j�j���P�Sq;���3ơ�>�#�[���k����c�;SЫ>@?4�u�8��ب��<hhyL=?x� ����>)��Ҍ	?Kr�6�?   ^Hc�>�	u�2l?   p>%=hhyL=�x� �����)��Ҍ	���c���   8ҕϾ�	u�2l�   �RN��d�7?��'����>Q�b�?Kr�6�?   ^Hc�>?��%j?   p>%=����(?���' �>f�XH�@�>-%Bw��>���3ơ�>SЫ>@?�ب��<z�	{;�=f��g�>�`��
v�=���;��>\T�^��=�T�kY�>C��]��=���kY�>�ĘzuV!@��8e(¿���{��?��4c��?�����@   HBP_DR_QD_ED      DRIF�-�*�+?�86b���=p�՝eu����j����!ލ�ʽ��my�`>|Z�������' �>[�ڂ�V=�S }��a�=���p���CaO >?�̬����Vq �>�V
���� �j�2���%K�o=ӟ��>,p�-%Bw��>�Dq�Y3=2��j�j����5�Sq;r��ơ�>��J�[�����е�c�;SЫ>@?YF^��8���b̨��<�Qۨ�V=?x� ����>��U��?Kr�6�?   �Hc�>�	u�2l?   �>%=�Qۨ�V=�x� �������U�����c���   �ЕϾ�	u�2l�   RN�Խ�4�7?��'����>�t�?Kr�6�?   �Hc�>?��%j?   �>%=�-�*�+?���' �>���Vq �>-%Bw��>r��ơ�>SЫ>@?��b̨��<z�	{;�=f��g�>�`��
v�=���;��>[T�^��=�T�kY�>B��]��=���kY�>�j�Ȱ\!@���s��¿�L;Y���?�45��?�O��f@	   HBP_DR_V7      DRIF�1@K@?�ĜnC%�=�� ws����5�`���%m�ʽ��$��`>wvr������' �>��3FyF=�S }��a�-�UH�p���CaO >�dT�̬�񟎟���>�t��x=!V��8(�gx�_�g=˒�r�e�-%Bw��>��Y3=2��j�j��#�|�Sq;~R�ġ�>�PcY����x���c�;SЫ>@?r
r"�8���y���<����r�=?x� ����>�����?Kr�6�?   <Jc�>�	u�2l?   jB%=����r�=�x� �����!��}T���c���   �ǕϾ�	u�2l�   �KN�/�)v8k7?��'����>�����?Kr�6�?   <Jc�>?��%j?   jB%=�1@K@?���' �>񟎟���>-%Bw��>~R�ġ�>SЫ>@?��y���<x�	{;�=d��g�>�`��
v�=���;��>]T�^��=�T�kY�>D��]��=���kY�>g���B�!@0JIUGǿ��c�[;�?�t��ݺ��A�1X�@	   HBP_DR_V7      DRIFSJuU?�P{�o�=��)��A���V ���0Z5���ʽ2i\ܘa>�BV}����' �>��2���<�S }��a�L���p���CaO >o�D̬�������>���@=�=hpije�����÷{_=�(uu�V�-%Bw��>���Y3=2��j�j������Sq;P��á�>;"U%W���yG���c�;SЫ>@?O���8�(��%���<���/�=?x� ����>{�.·?Kr�6�?   �Kc�>�	u�2l?   �E%=���/�=�x� �������O����c���   Ⱦ�Ͼ�	u�2l�   �EN���w��7?��'����>{�.·?Kr�6�?   �Kc�>?��%j?   �E%=SJuU?���' �>������>-%Bw��>P��á�>SЫ>@?(��%���<z�	{;�=f��g�>�`��
v�=���;��>\T�^��=�T�kY�>C��]��=���kY�>D޲8�!@��
���˿� �J��?
����0ٿ��ob3@   HBP_DR_QD_ED      DRIF��}�X?���z�=���`����T� 섽�7���ʽ�B�^a>�Apb����' �>y��P���S }��a�����p���CaO >oz��̬�F��@�I�>��d�E��=���<Z�9����E]=��q	�S�-%Bw��>S���Y3=2��j�j�/i��Sq;�D�á�>%��V����/�q�c�;SЫ>@?�BE��8�m����<;ِ,��=?x� ����>I�QJ��?Kr�6�?   Lc�>�	u�2l?   dF%=;ِ,��=�x� ������G@�����c���   ���Ͼ�	u�2l�   �DN�٠L���7?��'����>I�QJ��?Kr�6�?   Lc�>?��%j?   dF%=��}�X?���' �>F��@�I�>-%Bw��>�D�á�>SЫ>@?m����<y�	{;�=e��g�>�`��
v�=���;��>]T�^��=�T�kY�>D��]��=���kY�>������!@��YɁ�̿�SS�F?�?�<k��ۿ�:�M�@
   HBP_QD_V05      QUAD�2[A?	7���pV��R~�Cѝ��w�g�ʽ�v���`>L̊����h�����>�i�Ժs=j�=�&a=D$���=�ǰ��~B���]���;���ݡ�>���_��=��o&1_�@}��Y[=��zsP�Ú)p��>�?�1=g@3�f����ap;�b�`ơ�>n��c�����g�c�;SЫ>@?�V��8��-}����<�5���=?����k�?LO�`?��k�0_ ?   �Mc�>�	u�2l?   I%=�5���=�ޤ6���N�
��O���k�0_ �   ���Ͼ�	u�2l�   �HN��׽��7?����k�?LO�`?�}/�F��>   �Mc�>?��%j?   I%=�2[A?h�����>���ݡ�>Ú)p��>�b�`ơ�>SЫ>@?�-}����<�$�-'7�=�7m�\~�>J�2dr�='�Qkr��>���y��=�f��}Y�>8���x��=�F@R}Y�>WGyˬ�!@Qw��}@d`̓�?��Ì��:�M�@   HBP_COR      KICKER�2[A?	7���pV��R~�Cѝ��w�g�ʽ�v���`>L̊����h�����>�i�Ժs=j�=�&a=D$���=�ǰ��~B���]���;���ݡ�>���_��=��o&1_�@}��Y[=��zsP�Ú)p��>�?�1=g@3�f����ap;�b�`ơ�>n��c�����g�c�;SЫ>@?�V��8��-}����<�5���=?����k�?LO�`?��k�0_ ?   �Mc�>�	u�2l?   I%=�5���=�ޤ6���N�
��O���k�0_ �   ���Ͼ�	u�2l�   �HN��׽��7?����k�?LO�`?�}/�F��>   �Mc�>?��%j?   I%=�2[A?h�����>���ݡ�>Ú)p��>�b�`ơ�>SЫ>@?�-}����<�$�-'7�=�7m�\~�>J�2dr�='�Qkr��>���y��=�f��}Y�>8���x��=�F@R}Y�>WGyˬ�!@Qw��}@d`̓�?��Ì��:�M�@   HBP_BPM      MONI�2[A?	7���pV��R~�Cѝ��w�g�ʽ�v���`>L̊����h�����>�i�Ժs=j�=�&a=D$���=�ǰ��~B���]���;���ݡ�>���_��=��o&1_�@}��Y[=��zsP�Ú)p��>�?�1=g@3�f����ap;�b�`ơ�>n��c�����g�c�;SЫ>@?�V��8��-}����<�5���=?����k�?LO�`?��k�0_ ?   �Mc�>�	u�2l?   I%=�5���=�ޤ6���N�
��O���k�0_ �   ���Ͼ�	u�2l�   �HN��׽��7?����k�?LO�`?�}/�F��>   �Mc�>?��%j?   I%=�2[A?h�����>���ݡ�>Ú)p��>�b�`ơ�>SЫ>@?�-}����<�$�-'7�=�7m�\~�>J�2dr�='�Qkr��>���y��=�f��}Y�>8���x��=�F@R}Y�>WGyˬ�!@Qw��}@d`̓�?��Ì�ZVYy9�@
   HBP_QD_V05      QUAD��Q8�?���0�T$�t������fs����q�N�Nʽ�L.�۶`>�c�D��e��0?��h��݃=�<����=�<�xx��=D��]�R�΂9�|�;c��{�1�>�ʆO%�='����1�T/s��Y=Z1����K�>���
B�>���x�0=��l"!bb�9�t�m;n��ڡ�>{x�����wU�d�;SЫ>@?��M�J8��&]`���<%��O_=?�Ŀ0?ir��?w�p_�?   Sc�>�	u�2l?   S%=%��O_=��sw�A�(���
�����:���   @ޕϾ�	u�2l�   pbN����͟M7?�Ŀ0?ir��?w�p_�?   Sc�>?��%j?   S%=��Q8�?e��0?c��{�1�>>���
B�>n��ڡ�>SЫ>@?�&]`���<~�t��3�=n�,_|�>d!�-'o�=�
�3��>�\ꑅ�=	emˍY�>�݈����=5S���Y�>)�;N"!@�T��X�@� �=�m�?[4%�����1�ɶC.@   HBP_DR_QD_ED      DRIF\����?��䎮�#��ή�\�NX\Χ����R�iŮɽjnP`>Wsf���e��0?p�+龍=�<����=�C^99��=D��]�R����;O}�;Ƅ�0��>�/Z^X�=u���}	��sh,X=�v���F�>���
B�>�����0=��l"!bb�?aj�e�m;WC�����>1�Y����sTOd�;SЫ>@?�>��8��嫞���<�[�#�<?�Ŀ0?�4��	?w�p_�?   \c�>�	u�2l?   c%=�[�#�<��sw�A�(�*��fĖ��:���   ��Ͼ�	u�2l�   ��N�q�9T �6?�Ŀ0?�4��	?w�p_�?   \c�>?��%j?   c%=\����?e��0?Ƅ�0��>>���
B�>WC�����>SЫ>@?�嫞���<��t��3�=-�,_|�>�-'o�=/�
�3��>�\ꑅ�=emˍY�>�݈����=3S���Y�>#��{9V @��� �@i�TUi?�?Qp;ї��H/B0�e
@	   HBP_DR_V6      DRIFQ�R�?f
5�� �STғ�]�KK��0����ŽXLB�zc[>-n��f_�e��0?M�w0�Ή=�<����=���!��=D��]�R�U�D���;�s����>|�E��+�=�o2�_u�<�~���K=�� 5;>���
B�>*�9hp�0=��l"!bb���C���m;O��碹>�#^�`���H����e�;SЫ>@?%|v8�Ɗ���<pX�=�H8?�Ŀ0?-�[�@+?w�p_�?   ��c�>�	u�2l?   d�%=pX�=�H8��sw�A�(�Q������:���   (Z�Ͼ�	u�2l�   ��O����^�Q3?�Ŀ0?-�[�@+?w�p_�?   ��c�>?��%j?   d�%=Q�R�?e��0?�s����>>���
B�>O��碹>SЫ>@?Ɗ���<��t��3�=-�,_|�>���-'o�=��
�3��>�\ꑅ�=emˍY�>�݈����=3S���Y�>�7���B@������@��{li@.Q{�Z�������+�@	   HBP_DR_BP      DRIF�*L?^��{�g�*�j><�����和���9s�.��)���U>sD�y����e��0?%�AYb�=�<����=�]�r��=D��]�R����|��;X
��>���Z�R�=zm=M�=�5QJ��'=�:��iRW;>���
B�>�]��)�0=��l"!bb��[	a:n;�P壹>�U����s��g�;SЫ>@?ƿ�`8���̒��<�����3?�Ŀ0?���#|�?w�p_�?   ��c�>�	u�2l?   N<&=�����3��sw�A�(�E۶����:���   ���Ͼ�	u�2l�   ��P��D�p�(/?�Ŀ0?���#|�?w�p_�?   ��c�>?��%j?   N<&=�*L?e��0?X
��>>���
B�>�P壹>SЫ>@?��̒��<��t��3�=-�,_|�>X�-'o�=�
�3��>�\ꑅ�=emˍY�>�݈����=1S���Y�>>p�`�@#d��X@� X��@E��PI�����2@	   HBP_DR_BP      DRIF;U��T ?��
�������,���R%Mu4���Ju?ꁹ������P>dο�|���e��0?]R�){�=�<����=��/Ù�=D��]�R�~f*�A��;�c��>��ؼ��=$Qrp�!'=����&@��-5P�d;>���
B�>��6��0=��l"!bb��!���n;��B�⤹>χ�����^�xh�;SЫ>@?�0E"J8�]#2�v��<�I�|��-?�Ŀ0?��O�W3?w�p_�?   vd�>�	u�2l?   6�&=�I�|��-��sw�A�(��<Te���:���   H#�Ͼ�	u�2l�   �:R�t�#�'?�Ŀ0?��O�W3?w�p_�?   vd�>?��%j?   6�&=;U��T ?e��0?�c��>>���
B�>��B�⤹>SЫ>@?]#2�v��<��t��3�=-�,_|�>,��-'o�=�
�3��>�\ꑅ�=
emˍY�>�݈����=5S���Y�>�S��N@�1mf�@���@�m�J�s�=��1|�@	   HBP_DR_BP      DRIF'���f7�>�=�2	��P������d�k���1�:�������\��D>r~�(���e��0?8$��D�=�<����=�6����=D��]�R��/@��;j?��>@�����=�����0=X
��*S����k�m;>���
B�>F�=���0=��l"!bb���x9�n;�6��ॹ>ĹwZu����|��i�;SЫ>@?X��'48��c�[��<r�D��A$?�Ŀ0?̗��3�?w�p_�?   4Rd�>�	u�2l?   "'=r�D��A$��sw�A�(�}������:���   ���Ͼ�	u�2l�   �{S�̎�n3 ?�Ŀ0?̗��3�?w�p_�?   4Rd�>?��%j?   "'='���f7�>e��0?j?��>>���
B�>�6��ॹ>SЫ>@?�c�[��<��t��3�=-�,_|�>���-'o�=��
�3��>�\ꑅ�=�dmˍY�>�݈����=+S���Y�>��@�G=�?�ױ=�^�?{RG|c�@�PeD��p��d��@	   HBP_DR_BP      DRIF��0��>PT�������4�6↽�K����w��\g�����|}�2>�Q	��ۻe��0?��:��=�<����='�Ded��=D��]�R���nA̔�;;����>� ��9�=m�u�@�5=!�g�"^��.�Cns;>���
B�>�Y�V�0=��l"!bb�b���<n;^��ަ�>��*�������wSk�;SЫ>@?x>s-8��p�o?��<�ŏ��?�Ŀ0?ރ��?w�p_�?   ��d�>�	u�2l?   
�'=�ŏ����sw�A�(��E�@���:���   p�Ͼ�	u�2l�    �T�a	�q?�Ŀ0?ރ��?w�p_�?   ��d�>?��%j?   
�'=��0��>e��0?;����>>���
B�>^��ަ�>SЫ>@?�p�o?��<Βt��3�=K�,_|�>���-'o�=�
�3��>�\ꑅ�=emˍY�>�݈����=1S���Y�>ZrV���?֨]�i�?�?��0�#@�������~��"3@   HBP_DP_SEPM2   	   CSRCSBEND)/ߩ��> �_T-��xs��t�Ux��(i���F�՛i���a��<�=������Qt~F�>���Ě=>�9r\�=3Q�<��==6���D�#m��m�;_	�cn�>1��3¢�=��j�h�=�)%M��p�N�S�VV;
����A�>&����=lk5#�u�h��q��W;&�a�>��n�����&��ڣ�;�o=���A?�jUiO!7�F��&��< G�.��>y|�*� ?S��?G ?5�Sf?   �\��>����Wm?   � = G�.���>�".��۵n i���{���    C/ɾ����Wm�   t��k�s�B�>y|�*� ?S��?G ?5�Sf?   �\��> NA&T2j?   � =)/ߩ��>�Qt~F�>_	�cn�>
����A�>&�a�>�o=���A?F��&��<�a�_�S�=�k���>��Js�=��K\��>�-�病�=3�0:�X�>�������=�y���X�>F����?ɕ�al�̿<k��/�(@nyM����O��f@   HBP_DP_SEPM2   	   CSRCSBEND5��X�>ݮg���=+�s��e=N��z��:����b=^�ޝi��ć��ܵ�;�aWH��>�?��W�=��h���=���5�ƒ���|�>b�R|�л����@�>��`n��=�5���=�����j�x)�`IP;��r��A�>��Yi��=4�?��_u���#+k|C;��UL9v�>���u����i�l��;�,���C?w:rM��;�L�)����<~x�u�m ?����}^?h�)�!?�H5V`?   b6|�>�/����n?   �؍ =K��?o�������}^��B�����v�8���   ��&Ǿ�/����n�   �D��~x�u�m ?;9�:��?h�)�!?�H5V`?   b6|�>@=��˯i?   �؍ =5��X�>�aWH��>����@�>��r��A�>��UL9v�>�,���C?L�)����<UX�(���=��pl�>39ެk��=/�[�x�>��a���=��;X�>H�˥���=�؀AX�>�u"�$�?���{��3��� /@겨��i��O��f@
   FITT_HBP_8      MARK5��X�>ݮg���=+�s��e=N��z��:����b=^�ޝi��ć��ܵ�;�aWH��>�?��W�=��h���=���5�ƒ���|�>b�R|�л����@�>��`n��=�5���=�����j�x)�`IP;��r��A�>��Yi��=4�?��_u���#+k|C;��UL9v�>���u����i�l��;�,���C?w:rM��;�L�)����<~x�u�m ?����}^?h�)�!?�H5V`?   b6|�>�/����n?   �؍ =K��?o�������}^��B�����v�8���   ��&Ǿ�/����n�   �D��~x�u�m ?;9�:��?h�)�!?�H5V`?   b6|�>@=��˯i?   �؍ =5��X�>�aWH��>����@�>��r��A�>��UL9v�>�,���C?L�)����<UX�(���=��pl�>39ެk��=/�[�x�>��a���=��;X�>H�˥���=�؀AX�>�u"�$�?���{��3��� /@겨��i��O��f@   HBP_CNT      CENTER5��X�>ݮg���=+�s��e=P��z��:����b=^�ޝi��Ç��ܵ�;�aWH��>�?��W�=��h���=���5�ƒ���|�>b�R|�л����@�>��`n��=�5���=�����j�z)�`IP;��r��A�>��Yi��=7�?��_u���#+k|C;��UL9v�>���u����i�l��;�,���C?w:rM��;�L�)����<����a�>j�A�T?s��!?��``?   b6|�>�/����n?   �؍ =����a��؋�͎��������ڲ.���   ��&Ǿ�/����n�   �D��w���X�>j�A�T?s��!?��``?   b6|�>@=��˯i?   �؍ =5��X�>�aWH��>����@�>��r��A�>��UL9v�>�,���C?L�)����<UX�(���=��pl�>39ެk��=/�[�x�>��a���=��;X�>H�˥���=�؀AX�>�u"�$�?���{��3��� /@겨��i��O��f@
   HBP_WA_OU3      WATCH5��X�>ݮg���=+�s��e=P��z��:����b=^�ޝi��Ç��ܵ�;�aWH��>�?��W�=��h���=���5�ƒ���|�>b�R|�л����@�>��`n��=�5���=�����j�z)�`IP;��r��A�>��Yi��=7�?��_u���#+k|C;��UL9v�>���u����i�l��;�,���C?w:rM��;�L�)����<����a�>j�A�T?s��!?��``?   b6|�>�/����n?   �؍ =����a��؋�͎��������ڲ.���   ��&Ǿ�/����n�   �D��w���X�>j�A�T?s��!?��``?   b6|�>@=��˯i?   �؍ =5��X�>�aWH��>����@�>��r��A�>��UL9v�>�,���C?L�)����<UX�(���=��pl�>39ެk��=/�[�x�>��a���=��;X�>H�˥���=�؀AX�>�u"�$�?���{��3��� /@겨��i�