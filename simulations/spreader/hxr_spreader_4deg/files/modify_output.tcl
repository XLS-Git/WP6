

proc modifyfile {indist } {
# 	global sddsbeam

  set mc2 0.510998910  ;# MeV/c^2
  set c   299792458.e6 ;# um

  file copy -force $indist $indist.0


  set pref  [exec sddsprocess $indist -pipe=out -process=p,average,pref -nowarning | sddsprintout -pipe -par=pref,format=\%.15e  -notitle -nolabel ]
  set tref  [exec sddsprocess $indist -pipe=out -process=t,average,tref -nowarning | sddsprintout -pipe -par=tref,format=\%.15e  -notitle -nolabel ]


  set t0 $tref
  set z00 0.0
  set p0 $pref
  set E0 [expr $p0*$mc2]

  set dT  "t $t0 - 1e15 * "
  set dZ  "t $t0 - $c * "
  set dP  "p $p0 -"
  set ee  "p $mc2 *"
  set dE  "E $E0 - $E0 / 100 *"
  set dE0  "E $E0 - "
  set ec  "pCentral $mc2 *" 
  
  exec sddsprocess -process=t,average,tMean -process=p,average,pMean -define=par,t0,$t0,units=s \
           -define=par,p0,$p0,units=m\$be\$nc \
		   -define=par,ECentral,$ec,units=MeV -define=par,E0,$E0,units=MeV -redefine=column,dt,$dT,units=fs \
		   -define=column,dz,$dZ,units=\$gm\$rm  -define=par,zero,$z00,units=\$gm\$rm -define=column,dp,$dP,units=m\$be\$n \
		   -define=column,E,$ee,units=MeV -define=column,dE,$dE,units=% -define=column,dE0,$dE0,units=MeV -noWarnings  $indist $indist.tmp
  after 250
  set form "format=\%.6e"
  set params [eval exec sddsanalyzebeam $indist -pipe=out -nowarning | sddsprintout -pipe \
	  -col=enx,$form  -col=eny,$form -col=Sx,$form -col=Sy,$form  -col=St,$form -col=pCentral,$form -nolabel -notitle]
    set enx [expr [lindex $params 0]*1e6]
    set eny [expr [lindex $params 1]*1e6]
    set sx  [expr [lindex $params 2]*1e3]
    set sy  [expr [lindex $params 3]*1e3]
    set st  [lindex $params 4] 
    set pc  [lindex $params 5] 
  
  puts "emittance $enx"
  
  
  exec sddsprocess  -def=par,emtnx,$enx,units=umrad -def=par,emtny,$eny,units=umrad -def=par,sx,$sx,units=mm -def=par,sy,$sy,units=mm -process=dz,rms,sz  -process=dt,rms,st   -process=dE0,rms,sE  -noWarnings $indist.tmp $indist
  

  file delete -force $indist.tmp

}


proc modifytwiss {indist } {

  set mc2 0.510998910  ;# MeV/c^2
  set c   299792458.e3 ;# mm

  set ec  "pCentral0 $mc2 *"

  exec sddsprocess -define=column,E,$ec,units=MeV -noWarnings  $indist $indist.tmp
  after 250
  exec mv $indist.tmp $indist

}



proc color {foreground args} {
    return [exec tput setaf $foreground]$args[exec tput sgr0]
}


#  get_slice_info inj_ou.sdds 101

proc print_ellapsed_time {t0} {

set t1 [clock clicks -milliseconds] 
set dt_usec_all [expr $t1-$t0]
set dt_sec_all [expr int($dt_usec_all/1000)]
set dt_min_all [expr int($dt_sec_all/60)]
set dt_hou_all [expr int($dt_min_all/60)]
set dt_day_all [expr int($dt_hou_all/24)]

set dt_use [expr $dt_usec_all-$dt_sec_all*1000]
set dt_sec [expr $dt_sec_all-$dt_min_all*60]
set dt_min [expr $dt_min_all-$dt_hou_all*60]
set dt_hou [expr $dt_hou_all-$dt_day_all*24]

puts "[color 1 ==========================================================]"
puts [color 2 [format   "%2d hour %2d min %2d sec %3d usec" $dt_hou $dt_min $dt_sec $dt_use ]]
puts "[color 1 ==========================================================]"

}



 proc plotdist { {fl} {disp 1} } {
         
        modifyfile $fl
        exec sddsmultihist  $fl $fl.tmph0 -columns=dt,dz,dE,E -bins=32 -sides -sep
        exec sddssmooth $fl.tmph0 $fl.tmph -columns=dtFrequency,dzFrequency,dEFrequency,EFrequency \
                -SavitzkyGolay=3,3,3 
        
        lassign [exec sddsprocess $fl.tmph -pipe=out -process=dt,minimum,tmin -process=dt,maximum,tmax -process=dt,count,nline -nowarning | sddsprintout -pipe -par=tmax -par=tmin -par=nline  -notitle -nolabel]  tmax tmin nline
   
        set binstep [expr ($tmax-$tmin)/($nline-1)*1e-12]
   
        set curcalc  " Charge Particles / dtFrequency *  $binstep / " ;#kA conversation
   
        exec sddsprocess $fl.tmph $fl.hist -define=col,I,$curcalc,units=kA  -process=I,average,Iav
       
        file delete -force $fl.tmph
        
 
        print_parameters $fl

        if {$disp > 0} {
            exec sddsplot -thick=2  -layout=2,2 -sep  \
            -ticksettings=xgrid,ygrid,xlinetype=13,ylinetype=13,xthickness=1,ythickness=1 -linetypedefault=0,thickness=1 \
            -col=dt,I -graphic=line $fl.hist \
            -parameter=zero,E0  -lSpace=0.2,0.75,0,1 -nolabel -noscales -noborder \
            -string=Name=,pCoordinate=0,qCoordinate=1.0,scale=2 -string=@Description,pCoordinate=0.3,qCoordinate=1,scale=2 \
            -string=E0/MeV=,pCoordinate=0,qCoordinate=0.9,scale=2 -string=@E0,pCoordinate=0.3,qCoordinate=0.9,scale=2 \
            -string=St/fs=,pCoordinate=0,qCoordinate=0.8,scale=2 -string=@st,pCoordinate=0.3,qCoordinate=0.8,scale=2 \
            -string=Sz/um=,pCoordinate=0,qCoordinate=0.7,scale=2 -string=@sz,pCoordinate=0.3,qCoordinate=0.7,scale=2 \
            -string=Ib/kA=,pCoordinate=0,qCoordinate=0.6,scale=2 -string=@Iav,pCoordinate=0.3,qCoordinate=0.6,scale=2 \
            -string=SE/MeV=,pCoordinate=0,qCoordinate=0.5,scale=2 -string=@sE,pCoordinate=0.3,qCoordinate=0.5,scale=2 \
            -string=Sx/mm=,pCoordinate=0,qCoordinate=0.4,scale=2 -string=@sx,pCoordinate=0.3,qCoordinate=0.4,scale=2 \
            -string=Sy/mm=,pCoordinate=0,qCoordinate=0.3,scale=2 -string=@sy,pCoordinate=0.3,qCoordinate=0.3,scale=2 \
            -string=Emx/urad=,pCoordinate=0,qCoordinate=0.2,scale=2 -string=@emtnx,pCoordinate=0.3,qCoordinate=0.2,scale=2 \
            -string=Emy/urad=,pCoordinate=0,qCoordinate=0.1,scale=2 -string=@emtny,pCoordinate=0.3,qCoordinate=0.1,scale=2 \
            $fl.hist\
            -col=dt,dE -graph=dot -nolabel  $fl \
            -col=dEFrequency,dE -graphic=line $fl.hist  &
        } else {
            exec sddsplot -thick=4  -layout=2,2 -sep  \
            -ticksettings=xgrid,ygrid,xlinetype=13,ylinetype=13,xthickness=1,ythickness=1 -linetypedefault=0,thickness=1 \
            -col=dt,I -graphic=line $fl.hist \
            -parameter=zero,E0  -lSpace=0.2,0.75,0,1 -nolabel -noscales -noborder \
            -string=Name=,pCoordinate=0,qCoordinate=1.0,scale=2 -string=@Description,pCoordinate=0.3,qCoordinate=1,scale=2 \
            -string=E0/MeV=,pCoordinate=0,qCoordinate=0.9,scale=2 -string=@E0,pCoordinate=0.3,qCoordinate=0.9,scale=2 \
            -string=St/fs=,pCoordinate=0,qCoordinate=0.8,scale=2 -string=@st,pCoordinate=0.3,qCoordinate=0.8,scale=2 \
            -string=Sz/um=,pCoordinate=0,qCoordinate=0.7,scale=2 -string=@sz,pCoordinate=0.3,qCoordinate=0.7,scale=2 \
            -string=Ib/kA=,pCoordinate=0,qCoordinate=0.6,scale=2 -string=@Iav,pCoordinate=0.3,qCoordinate=0.6,scale=2 \
            -string=SE/MeV=,pCoordinate=0,qCoordinate=0.5,scale=2 -string=@sE,pCoordinate=0.3,qCoordinate=0.5,scale=2 \
            -string=Sx/mm=,pCoordinate=0,qCoordinate=0.4,scale=2 -string=@sx,pCoordinate=0.3,qCoordinate=0.4,scale=2 \
            -string=Sy/mm=,pCoordinate=0,qCoordinate=0.3,scale=2 -string=@sy,pCoordinate=0.3,qCoordinate=0.3,scale=2 \
            -string=Emx/urad=,pCoordinate=0,qCoordinate=0.2,scale=2 -string=@emtnx,pCoordinate=0.3,qCoordinate=0.2,scale=2 \
            -string=Emy/urad=,pCoordinate=0,qCoordinate=0.1,scale=2 -string=@emtny,pCoordinate=0.3,qCoordinate=0.1,scale=2 \
            $fl.hist\
            -col=dt,dE -graph=dot -nolabel  $fl \
            -col=dEFrequency,dE -graphic=line $fl.hist \
            -device=gpng,onwhite -output=$fl.png &
            
        }
    
 }

 
 proc print_parameters  {indist } {
    
    set form "format=\%.6e"
    
    set position [eval exec sddsprintout $indist -par=Description,label=location -nolabel -notitle ]
        
    set position [regsub -all {\s+} $position " "]
    
    
    puts "
    [color 4 $position ]"
    
    
    
    set params [eval exec sddsanalyzebeam $indist -pipe=out -nowarning | sddsprintout -pipe \
	  -col=en?,$form  -col=beta?,$form -col=alpha?,$form  -col=eta?,$form -col=eta?p,$form -col=pMean,$form -col=p0,$form \
	  -col=ECentral,$form  -col=E0,$form -col=sz,$form  -col=st,$form -col=sE,$form -col=Cx,$form -col=Cy,$form -col=Sx,$form -col=Sy,$form  -nolabel -notitle]
    set enx [lindex $params 0] 
    set eny [lindex $params 1] 
    set btx [lindex $params 2] 
    set bty [lindex $params 3] 
    set alx [lindex $params 4] 
    set aly [lindex $params 5] 
    set etx [lindex $params 6] 
    set ety [lindex $params 7]
    set etxp [lindex $params 8] 
    set etyp [lindex $params 9]   
    set PM [lindex $params 10] 
    set P0 [lindex $params 11] 
    set EC [lindex  $params 12] 
    set E0 [lindex  $params 13] 
    set sz [lindex  $params 14] 
    set st [lindex  $params 15] 
    set sE [lindex  $params 16] 
    set Cx [lindex  $params 17] 
    set Cy [lindex  $params 18] 
    set sx [lindex  $params 19] 
    set sy [lindex  $params 20] 
    
    puts "[color 2 E0    =]$E0  MeV              [color 2 sig_E    =]$sE MeV  [color 2 dE=][expr $sE/$E0*100] \%"
    puts "[color 2 emit_x=]$enx mm.mrad        [color 2 emit_y   =]$eny  mm.mrad "
    puts "[color 2   <sx> =]$sx  m              [color 2   <sy>    =]$sy  m "
    puts "[color 2 sig_z =]$sz um             [color 2 sig_t    =]$st fs"
    puts "[color 2 beta_x,y=]$btx $bty [color 2 alpha_x,y=]$alx $aly "

  
 }
 
 
 
proc integrate_linac {twissfile } {
    set mc2 0.510998910  ;# MeV/c^2
    
    set leng   [exec sdds2plaindata -pipe=output $twissfile -outputMode=ascii -col=s -noRowCount]
    set beta_x [exec sdds2plaindata -pipe=output $twissfile -outputMode=ascii -col=betax -noRowCount]
    set beta_y [exec sdds2plaindata -pipe=output $twissfile -outputMode=ascii -col=betay -noRowCount]
    set moment [exec sdds2plaindata -pipe=output $twissfile -outputMode=ascii -col=pCentral0 -noRowCount]
    
    set totline [llength $leng]


  set int_x 0.0
  set int_y 0.0
  set phase_x 0.0
  set phase_y 0.0



  for {set i 0} {$i< [expr $totline-1]} {incr i} {
	set s_ii [lindex $leng [expr $i]]
	set s_jj [lindex $leng [expr $i+1]]
	set beta_xii [lindex $beta_x [expr $i]]
	set beta_xjj [lindex $beta_x [expr $i+1]]
	set beta_yii [lindex $beta_y [expr $i]]
	set beta_yjj [lindex $beta_y [expr $i+1]]
	set ener_ii  [expr $mc2*[lindex $moment [expr $i]]]
	set ener_jj  [expr $mc2*[lindex $moment [expr $i+1]]]

	set phase_x [expr $phase_x + 0.5*($s_jj-$s_ii)*(1./$beta_xii +1./$beta_xjj)]
	set phase_y [expr $phase_y + 0.5*($s_jj-$s_ii)*(1./$beta_yii +1./$beta_yjj)]
		

	if { [expr ($ener_jj + $ener_ii)*0.5] <= $ener_ii } {
        set int_x [expr $int_x + 0.5*($s_jj-$s_ii)*($beta_xii/$ener_ii + $beta_xjj/$ener_jj)]
        set int_y [expr $int_y + 0.5*($s_jj-$s_ii)*($beta_yii/$ener_ii + $beta_yjj/$ener_jj)]
	} else {
	   set int_x [expr $int_x + 0.0]
	   set int_y [expr $int_y + 0.0]
	}

  }

    puts "[color 4 $twissfile ] [color 2 int(s=0..L,betax-y/E)=] $int_x   $int_y "
    puts "[color 2 phase_x-y=]$phase_x    $phase_y"


} 

     


proc normalize_bunch { dist_in twissfile } {
    global pi
    
     set elename [eval exec sddsprintout $dist_in -par=Description,label=location -nolabel -notitle ]
     set elename [regsub -all {\s+} $elename ""]
#      puts $elename
     set par_in [exec sddsprocess $twissfile -pipe=out -match=col,ElementName=$elename | \
     sddsprintout -pipe -col=betay -col=alphax -col=betax -col=alphay -col=pCentral0 -col=psix  -col=psiy  -nolabel -notitle]
     lassign $par_in btx_in alx_in bty_in aly_in p0_in psx psy
     

     
     puts $btx_in
     puts $bty_in
     puts $alx_in
     puts $aly_in
     puts $p0_in
    

    
    set sepe " "

    exec sddssort  -column=particleID -pipe=out $dist_in | sdds2plaindata -pipe=in s.$dist_in \
        -outputMode=ascii  -noRowCount -separator=$sepe \
        -column=x,format=%20.13e -column=xp,format=%20.13e -column=y,format=%20.13e -column=yp,format=%20.13e \
        -column=t,format=%20.13e -column=p,format=%20.13e -column=particleID,format=%d
        
#    set ref_par  [exec sddsprocess $dist_in -pipe=out -filter=column,particleID,0,0 -nowarning | sddsprintout -pipe -col=p,format=\%.15e  -notitle -nolabel ]

        
#         
#     exec sddssort  -column=particleID -pipe=out $s_dist_ou | sdds2plaindata -pipe=in sl_ou.tmp \
#         -outputMode=ascii  -noRowCount -separator=$sepe \
#         -column=x,format=%20.13e -column=xp,format=%20.13e -column=y,format=%20.13e -column=yp,format=%20.13e \
#         -column=t,format=%20.13e -column=p,format=%20.13e -column=particleID,format=%d
#     
# 
#     set xn_in  {}
#     set xpn_in {}
#     set rx_in {}
#     set tx_in {}
#     set yn_in  {}
#     set ypn_in {}
#     set ry_in {}
#     set ty_in {}
#     set z_in  {}
#     set p_in  {}
#     set i_in  {}

#     set psx [expr fmod($psx,2*$pi)]
#     set psy [expr fmod($psy,2*$pi)]

    set all_in {}
    set m 0
    set sf1 [open s.$dist_in r]
    while {[gets $sf1 line] >= 0} {
        set x          [lindex $line 0]
        set xp         [lindex $line 1]
        set y          [lindex $line 2]
        set yp         [lindex $line 3]
        set z          [lindex $line 4]
        set bgm        [lindex $line 5]
        set inx        [lindex $line 6]


        set gm         [expr sqrt($bgm*$bgm+1)]
        
        set xn         [expr sqrt($gm/$btx_in)*$x]
        set xpn        [expr $xn*$alx_in + sqrt($gm*$btx_in)*$xp]
        set rx         [expr sqrt($xn*$xn+$xpn*$xpn)]
        set tx         [expr atan2($xpn,$xn) ]
        
        set yn         [expr sqrt($gm/$bty_in)*$y]
        set ypn        [expr $yn*$aly_in + sqrt($gm*$bty_in)*$yp]
        set ry         [expr sqrt($yn*$yn+$ypn*$ypn)]
        set ty         [expr atan2($ypn,$yn)]
        
        if {$m < 1 } {
            set tx_in0 $tx
            set rx_in0 $rx
            set ty_in0 $ty
            set ry_in0 $ry
            set z0_in  $z
        }
        
        set z          [expr $z -$z0_in]
        
        
#         puts "$gm $z $inx"
        
#         set tx [expr $tx - $tx_in0]
#         set ty [expr $ty - $ty_in0]
        
        set xn     [expr cos($tx-$tx_in0)*$rx]
        set xpn    [expr sin($tx-$tx_in0)*$rx]
        set yn     [expr cos($ty-$ty_in0)*$ry]
        set ypn    [expr sin($ty-$ty_in0)*$ry]
        
#         lappend x_in   $xn
#         lappend xp_in  $xpn
#         lappend rx_in  $rx
#         lappend tx_in  $tx
#         lappend y_in   $yn
#         lappend yp_in  $ypn
#         lappend ry_in  $ry
#         lappend ty_in  $ty
#         lappend z_in   $z
#         lappend p_in   $gm
#         lappend i_in   $inx
        
        lappend all_in "$xn $xpn $yn $ypn $z $gm $rx $tx  $ry $ty  $inx"
        incr m
    }
#              puts "teta x calculated $tx_in0 "
#              puts "teta x twiss $psx "
#              puts "teta y calculated $ty_in0"
#              puts "teta y twiss $psy "
             puts $rx_in0 
             puts $ry_in0 
             puts $tx_in0 
             puts $ty_in0 
             
    set title "# 1-xn \n# 2-xpn \n# 3-yn \n# 4-ypn \n# 5-z (s) \n# 6-p (mec2) \n# 7-rx \n# 8-ry \n# 9-idx"
     
    set fs_in [open s_b.$dist_in w]
   
    puts $fs_in $title
    
    foreach line $all_in {
        puts $fs_in $line
#         puts $line
    }
    
    close $fs_in

}


proc modifyfile0 {indist } {
# 	global sddsbeam

  set mc2 0.510998910  ;# MeV/c^2
  set c   299792458.e6 ;# um

  file copy -force $indist $indist.0


  set pref  [exec sddsprocess $indist -pipe=out -process=p,average,pref -nowarning | sddsprintout -pipe -par=pref,format=\%.15e  -notitle -nolabel ]
  set tref  [exec sddsprocess $indist -pipe=out -process=t,average,tref -nowarning | sddsprintout -pipe -par=tref,format=\%.15e  -notitle -nolabel ]

#   set pref  [exec sddsprocess $indist -pipe=out -filter=column,particleID,0,0 -nowarning | sddsprintout -pipe -col=p,format=\%.15e  -notitle -nolabel ]
#   set tref  [exec sddsprocess $indist -pipe=out -filter=column,particleID,0,0 -nowarning | sddsprintout -pipe -col=t,format=\%.15e  -notitle -nolabel ]

  puts $tref
  puts $pref

  set t0 $tref
  set z00 0.0
  set p0 $pref
  set E0 [expr $p0*$mc2]

  set dT  "t $t0 - 1e15 * "
  set dZ  "t $t0 - $c * "
  set dP  "p $p0 -"
  set ee  "p $mc2 *"
  set dE  "E $E0 - $E0 / 100 *"
  set dE0  "E $E0 - "
  set ec  "pMean $mc2 *"
  set chr  7.500e-11
#   
#   
# &parameter name=Filename, description="Name of file from which this page came", type=string, &end
# &parameter name=NumberCombined, description="Number of files combined to make this file", type=long, &end
# &parameter name=n_rows2, type=long, &end
# &parameter name=tMin, units=s, description="minimum of t", type=double, &end
# &parameter name=tSpread, units=s, description="spread in t", type=double, &end
# &parameter name=pSpread, units="m$be$nc", description="spread in p", type=double, &end
# &column name=p, units="m$be$nc", type=double,  &end
# &column name=x, units=m, type=double,  &end
# &column name=xp, symbol=x', type=double,  &end
# &column name=y, units=m, type=double,  &end
# &column name=yp, symbol=y', type=double,  &end
# &column name=t, units=s, type=double,  &end
# &column name=page, type=short,  &end
# &column name=doAve, type=short,  &end
# &data mode=binary, &end
# 
# 
# &parameter name=z0, units=m, description="average of z", type=double, &end
# &parameter name=Particles, description="count x", type=double, &end
# &parameter name=Charge, units=C, type=double, &end
# &parameter name=pCentral, units="m$be$nc", description="average of p", type=double, &end
# &column name=x, units=m, type=double,  &end
# &column name=y, units=m, type=double,  &end
# &column name=z, units=m, type=double,  &end
# &column name=px, type=double,  &end
# &column name=py, type=double,  &end
# &column name=pz, type=double,  &end
# &column name=p, units="m$be$nc", type=double,  &end
# &column name=E, units=MeV, type=double,  &end
# &column name=t, units=s, type=double,  &end
# &column name=xp, type=double,  &end
# &column name=yp, type=double,  &end
# &data mode=binary, &end


  exec sddsprocess -delete=par,NumberCombined -delete=par,n_rows2 -delete=par,tMin -delete=par,tSpread -delete=par,pSpread \
                -delete=col,page -delete=col,doAve \
                -nowarning $indist tmp0
  
  exec sddsprocess -process=x,count,Particles -define=par,Charge,$chr,units=C  -define=par,pCentral,$p0,units=m\$be\$nc \
                   -nowarning  tmp0 $indist
  
  
#   exec sddsprocess -process=t,average,tMean -process=p,average,pMean -define=par,t0,$t0,units=s \
#            -define=par,p0,$p0,units=m\$be\$nc \
# 		   -define=par,E0,$E0,units=MeV -redefine=column,dt,$dT,units=fs \
# 		   -define=column,dz,$dZ,units=\$gm\$rm  -define=par,zero,$z00,units=\$gm\$rm -define=column,dp,$dP,units=m\$be\$n \
# 		   -define=column,E,$ee,units=MeV -define=column,dE,$dE,units=% -define=column,dE0,$dE0,units=MeV -noWarnings  $indist $indist.tmp
#   after 250
#   exec sddsprocess   -process=dz,rms,sz  -process=dt,rms,st -define=par,ECentral,$ec,units=MeV -noWarnings $indist.tmp $indist.tmp1
#   exec sddsprocess   -process=dE0,rms,sE -noWarnings $indist.tmp1 $indist
  file delete -force $indist.tmp
#   file delete -force $indist.tmp1

}

 
