# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# %% short range wake calculation 
# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

set clight  299792458.0
set pi      [expr acos(-1.0)]   ;# pi number
set Z0      120.0*$pi		    ;# empadance of free space
set c       $clight			    ;# speed of light


# *****************************************************************************************
# longitudinal shortrange wakefield # s is given in meters, return value is in V/C
# *****************************************************************************************
proc w_long {ss aa ll gg long_fact } {
    global    pi Z0 clight 
    set s0 [expr 0.41*pow($aa,1.8)*pow($gg,1.6)*pow($ll,-2.4)]
    return  [expr  $Z0*$clight/($pi*$aa*$aa)*exp(-sqrt($ss/$s0))*$long_fact]
}


# *****************************************************************************************
# transverse short range wakefield  s is given in micro metres  return value is in V/C/m
# *****************************************************************************************
proc w_trans {ss aa ll gg tran_fact } {
    global    pi Z0 clight
    set s0 [expr 0.169*pow($aa,1.79)*pow($gg,0.38)*pow($ll,-1.17)]
    return [expr 4.0*$Z0*$clight*$s0/($pi*pow($aa,4.0))*(1.0-(1.0+sqrt($ss/$s0))*exp(-sqrt($ss/$s0)))*$tran_fact]
}



proc compute_wake { &params } {
global clight
upvar 1 ${&params} strpars

set header "SDDS1
&column name=z, units=m, type=double,  &end
&column name=t, units=s, type=double,  &end
&column name=Wl, units=V/C, type=double,  &end
&column name=Wt, units=V/C/m, type=double,  &end

&data mode=ascii, &end
! page number 1"

set nsttp 30000
set tot_bunch_l 3.0e-3
set dzz [expr $tot_bunch_l/$nsttp]

# puts $strpars(aper)

set flwk [open "$strpars(wakefile)" w]


puts $flwk $header
puts $flwk "         $nsttp"

for {set i 0} {$i< [expr 1+$nsttp]} { incr i} {

  set s [expr $i*$dzz]
  set t [expr $s/$clight]
  
  set wkt [expr [w_trans $s $strpars(aper) $strpars(cell_l) $strpars(gap) $strpars(trwakefact)  ]*$strpars(cell_l) ]
  set wkz [expr [w_long  $s $strpars(aper) $strpars(cell_l) $strpars(gap) $strpars(lnwakefact)  ]*$strpars(cell_l) ]

  puts $flwk [format " %.12e  %.12e  %.12e  %.12e  " $s $t $wkz $wkt ]

}

close $flwk

}

# # creating wakefiles... 
# compute_wake cband_par
# compute_wake xband_par
# compute_wake kband_par



if {[file exist ../files/$cband_par(wakefile)]} {
	exec cp -f ../files/$cband_par(wakefile) .
} else {
	compute_wake cband_par
}

if {[file exist ../files/$xband_par(wakefile)]} {
	exec cp -f  ../files/$xband_par(wakefile) .
} else {
	compute_wake xband_par
}

if {[file exist ../files/$kband_par(wakefile)]} {
	exec cp -f ../files/$kband_par(wakefile) .
} else {
	compute_wake kband_par
}

if {[file exist ../files/$xdband_par(wakefile)] } {
	exec cp -f  ../files/$xdband_par(wakefile) .
} else {
	compute_wake xdband_par
}


if {[file exist ../files/$sdband_par(wakefile)] } {
	exec cp -f  ../files/$sdband_par(wakefile) .
} else {
	compute_wake sdband_par
}

# 
# exec  sddsplot -col=z,Wl -yscale=id=1 -graph=line,vary -legend $cband_par(wakefile) \
#                -col=z,Wt -yscale=id=2 -graph=line,vary -legend $cband_par(wakefile)  &
# exec  sddsplot -col=z,Wl -yscale=id=1 -graph=line,vary -legend $xband_par(wakefile) \
#                -col=z,Wt -yscale=id=2 -graph=line,vary -legend $xband_par(wakefile)  &
# exec  sddsplot -col=z,Wl -yscale=id=1 -graph=line,vary -legend $kband_par(wakefile) \
#                -col=z,Wt -yscale=id=2 -graph=line,vary -legend $kband_par(wakefile)  &
# exit
