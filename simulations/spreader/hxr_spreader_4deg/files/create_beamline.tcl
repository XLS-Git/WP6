

set filee [open $filename.$indx.lte w]
set pi [expr acos(-1.)]


set case0 [info exists track_ln0]
set case1 [info exists track_bc1]
set case2 [info exists track_bc1_dia]
set case3 [info exists track_ln1]
set case4 [info exists track_bc2]
set case5 [info exists track_ln2]
set case6 [info exists track_ln2_dia]
set case7 [info exists track_srb]
set case8 [info exists track_ln3]
set case9 [info exists track_sxrbpl]
set case10a [info exists track_hbp1]
set case10b [info exists track_hbp2]
set case10c [info exists track_hbp3]

set case11 [info exists track_hxrl]
set case12 [info exists spare]


puts $case9

after 1000

puts $filee "
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! beamline for XLS
!! [clock format [clock seconds] -format "%B %Y"]
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!------------------------------------------------------------------------------
!          bunch
!------------------------------------------------------------------------------
BNCH           : CHARGE,TOTAL=75e-12

EMTCH          : ENERGY, MATCH_BEAMLINE=1

"


puts $filee "

!------------------------------------------------------------------------------
! Setup SXR BPL  ON OFF after LN3
!------------------------------------------------------------------------------


LN3_SCA0       : RFDF, frequency=$sdband_par(freq),L=$sdband_par(active_l),voltage=$ln3_sdefvolt ,phase=\"$ln2_sdefphs \", &
                       n_kicks=$ln3_sdefkicks, tilt=$ln3_sdeftitl, GROUP=\"LN3\" &
                       
LN3_SZW        : WAKE, inputfile=\"$sdband_par(wakefile)\", factor=$sdband_par(n_cell), tColumn=\"t\", wColumn=\"Wl\", &
                       interpolate=1, smoothing=1, N_BINS=0,CHANGE_P0=1,allow_long_beam=1 

LN3_STW        : TRWAKE ,inputfile=\"$sdband_par(wakefile)\",factor=$sdband_par(n_cell), tColumn=\"t\",wxColumn=\"Wt\",wyColumn=\"Wt\", &
                       interpolate=1, smoothing=1, N_BINS=0
                       
LN3_SCA        : LINE=(LN3_SCA0,LN3_SZW,LN3_STW)

LN3_DR_SCA_ED : DRIFT,L= $sdband_par(edge_l), GROUP=\"LN3\"

LN3_DR_QD_ED  : DRIFT,L= $ln3_quad_edge, GROUP=\"LN3\"
LN3_QD_FH     : QUAD, L=[expr $ln3_quad_l*0.5], K1=[expr  1.0*$ln3_quad_k], GROUP=\"LN3\"
LN3_QD_DH     : QUAD, L=[expr $ln3_quad_l*0.5], K1=[expr -1.0*$ln3_quad_k], GROUP=\"LN3\"
LN3_COR       : KICKER, L=0.0,HKICK=0,VKICK=0, GROUP=\"LN3\"
LN3_BPM       : MONI, L=0.0, GROUP=\"LN3\"

LN3_DR_BP_FL     : DRIFT,L= $flange, GROUP=\"LN3\"
LN3_DR_BP_BL     : DRIFT,L= $belowl, GROUP=\"LN3\"
LN3_DR_BP_SV     : DRIFT,L= $vacpor, GROUP=\"LN3\"
LN3_DR_BP_VS     : DRIFT,L= $viewsc, GROUP=\"LN3\"
LN3_DR_BP_DR     : DRIFT,L= $viewsc, GROUP=\"LN3\"
LN3_DR_BP_CT     : DRIFT,L= $viewsc, GROUP=\"LN3\"

LN3_QD_BP_V01: QUAD,L=0.04,K1=-7.707923079290234
LN3_QD_BP_V02: QUAD,L=0.04,K1=8.732333860343914
LN3_QD_BP_V03: QUAD,L=0.04,K1=-8.265153018715839
LN3_QD_BP_V04: QUAD,L=0.04,K1=-5.14389813034124
LN3_DR_BP_V1: DRIF,L=1.877747502447945
LN3_DR_BP_V2: DRIF,L=2.286348484078106
LN3_DR_BP_V3: DRIF,L=1.241106074618104
LN3_DR_BP_V4: DRIF,L=0.2000019276770758
LN3_DR_BP_V5: DRIF,L=0.7001603960975262

LN3_QD_BP_V1   : LINE=(LN3_DR_QD_ED,LN3_QD_BP_V01,LN3_COR,LN3_BPM,LN3_QD_BP_V01,LN3_DR_QD_ED)
LN3_QD_BP_V2   : LINE=(LN3_DR_QD_ED,LN3_QD_BP_V02,LN3_COR,LN3_BPM,LN3_QD_BP_V02,LN3_DR_QD_ED)
LN3_QD_BP_V3   : LINE=(LN3_DR_QD_ED,LN3_QD_BP_V03,LN3_COR,LN3_BPM,LN3_QD_BP_V03,LN3_DR_QD_ED)
LN3_QD_BP_V4   : LINE=(LN3_DR_QD_ED,LN3_QD_BP_V04,LN3_COR,LN3_BPM,LN3_QD_BP_V04,LN3_DR_QD_ED)


!LN3_BP_MATCH0    : LINE=(LN3_DR_BP_V1,LN3_QD_BP_V1,LN3_DR_BP_V2,LN3_QD_BP_V2,LN3_DR_BP_V3,LN3_QD_BP_V3,LN3_DR_BP_V4,LN3_QD_BP_V4,LN3_DR_BP_V5)
LN3_BP_MATCH0    : LINE=(LN3_DR_BP_V1,LN3_QD_BP_V1,LN3_DR_BP_V2,LN3_QD_BP_V2,LN3_DR_BP_V3,LN3_QD_BP_V3,LN3_DR_BP_V4)

LN3_DR_BP_0    : DRIF,GROUP=\"LN3\",L=0.2
LN3_DR_BP      : DRIF,GROUP=\"LN3\",L=$ln3_swi_trip_dir
LN3_DR_BP_C    : CSRDRIFT,GROUP=\"LN3\",L=$ln3_swi_trip_dir,N_KICKS= 10,USE_STUPAKOV=1, CSR=$csr_flag

LN3_BP_SDEF     : LINE=(LN3_DR_BP_0,LN3_DR_BP_BL,LN3_DR_SCA_ED,LN3_SCA,LN3_DR_SCA_ED,LN3_DR_BP_BL,LN3_DR_BP_0)


LN3_BP_FITP1     : MARKER, FITPOINT=1
LN3_BP_FITP2     : MARKER, FITPOINT=1
LN3_BP_FITP3     : MARKER, FITPOINT=1
LN3_BP_FITP4     : MARKER, FITPOINT=1

LN3_BP_WA_OU      : WATCH,filename=\"ln3_bp_ou.sdds\",mode=coord, label=\"ln3 bp ou \"
LN3_BP_WA_OUa      : WATCH,filename=\"ln3_bp_oua.sdds\",mode=coord, label=\"ln3 bp ou \"
LN3_BP_LINE     :LINE=(LN3_BP_SDEF,LN3_DR_BP,LN3_DR_BP_VS,LN3_DR_BP)

LN3_BP_MATCH_TO  :LINE=(LN3_BP_MATCH0,LN3_BP_FITP1,LN3_BP_LINE,LN3_BP_FITP2 )
LN3_BP_MATCHED   : LINE=(LN3_BP_MATCH0,LN3_BP_LINE)
LN3_BP_MATCHED_TR: LINE=(LN3_BP_MATCH0,LN3_BP_LINE,LN3_BP_WA_OU)
LN3_BP_MATCHED_TR2: LINE=(LN3_BP_MATCH0,LN3_BP_LINE,LN3_BP_WA_OUa)
"


puts $filee "

!-----------------------------
! MATCH HXR BY-PASS LINE
!-----------------------------

                 
HBP_DR_BP      : DRIF,GROUP=\"HPB\",L=$hbp_swi_trip_dir
HBP_DR_BP_C    : CSRDRIFT,GROUP=\"HPB\",L=$hbp_swi_trip_dir,N_KICKS= 10,USE_STUPAKOV=1, CSR=$csr_flag  

HBP_DP_SEPM1 :  CSRCSBEND,L= [expr $hbp_swi_s_dipole*0.5], EDGE_ORDER=2, ANGLE=[expr $hbp_swi_angle_rd*0.5], E1=0.0, E2=0.0, &
                 N_KICKS=10,INTEGRATION_ORDER=4, TILT=\"pi\", BINS=25,SG_HALFWIDTH=3, SG_ORDER=2, STEADY_STATE=0, &
                 CSR=$csr_flag, ISR=$isr_flag,HIGH_FREQUENCY_CUTOFF0=$f0_csr_hxrbp,HIGH_FREQUENCY_CUTOFF1=$f1_csr_hxrbp
                 
HBP_DP_SEPM2 :  CSRCSBEND, L= [expr $hbp_swi_s_dipole*0.5], EDGE_ORDER=2,  ANGLE=[expr $hbp_swi_angle_rd*0.5], E1=0.0, E2=0.0, &
                 N_KICKS=10, INTEGRATION_ORDER=4,TILT=0, BINS=25,SG_HALFWIDTH=3, SG_ORDER=2, STEADY_STATE=1, &
                 CSR=$csr_flag, ISR=$isr_flag,HIGH_FREQUENCY_CUTOFF0=$f0_csr_hxrbp,HIGH_FREQUENCY_CUTOFF1=$f1_csr_hxrbp
                 
HBP_COR       : KICKER, L=0.0,HKICK=0,VKICK=0, GROUP=\"HBP\"
HBP_BPM       : MONI, L=0.0, GROUP=\"HBP\"
HBP_DR_QD_ED  : DRIFT,L= $hbp_quad_edge, GROUP=\"HBP\"
HBP_DR_FL     : DRIFT,L= $flange, GROUP=\"HBP\"
HBP_DR_BL     : DRIFT,L= $belowl, GROUP=\"HBP\"
HBP_DR_SV     : DRIFT,L= $vacpor, GROUP=\"HBP\"
HBP_DR_VS     : DRIFT,L= $viewsc, GROUP=\"HBP\"
HBP_DR_DR     : DRIFT,L= $viewsc, GROUP=\"HBP\"

HBP_SX_V01: SEXT,GROUP=\"HBP\",L=[expr $hbp_quad_l*1.],K2=-$ksext
!HBP_SX_V01: SEXT,GROUP=\"HBP\",L=[expr $hbp_quad_l*1.5],K2=$ksext
HBP_SX_V02: SEXT,GROUP=\"HBP\",L=[expr $hbp_quad_l*1.],K2=-$ksext



 HBP_QD_V01: QUAD,GROUP=\"HBP\",L=0.04,K1=7.1405
 HBP_QD_V02: QUAD,GROUP=\"HBP\",L=0.04,K1=-6.213833011107281
 HBP_QD_V03: QUAD,GROUP=\"HBP\",L=0.04,K1=7.049170249878435
 HBP_QD_V04: QUAD,GROUP=\"HBP\",L=0.04,K1=-3.352457771039597
 HBP_QD_V05: QUAD,GROUP=\"HBP\",L=0.04,K1=7.1405
 HBP_QD_V06: QUAD,GROUP=\"HBP\",L=0.04,K1=-4.406495743442422
 HBP_DR_V1: DRIF,GROUP=\"HBP\",L=0.2768301473283097
 HBP_DR_V2: DRIF,GROUP=\"HBP\",L=0.2075
 HBP_DR_V3: DRIF,GROUP=\"HBP\",L=1.220167921017036e-15
 HBP_DR_V4: DRIF,GROUP=\"HBP\",L=1.340915598179951
 HBP_DR_V5: DRIF,GROUP=\"HBP\",L=4.472884401820048
 HBP_DR_V6: DRIF,GROUP=\"HBP\",L=0.2768301473283097
 HBP_DR_V7: DRIF,GROUP=\"HBP\",L=0.3075

HBP_QD_V07: QUAD,GROUP=\"HBP\",L=[expr $hbp_quad_l*0.5],K1=8.511843305536269
HBP_DR_V8: DRIF,GROUP=\"HBP\",L=0.96

HBP_QD_V1   : LINE=(HBP_DR_QD_ED,HBP_QD_V01,HBP_COR,HBP_BPM,HBP_QD_V01,HBP_DR_QD_ED)
HBP_QD_V2   : LINE=(HBP_DR_QD_ED,HBP_QD_V02,HBP_COR,HBP_BPM,HBP_QD_V02,HBP_DR_QD_ED)
HBP_QD_V3   : LINE=(HBP_DR_QD_ED,HBP_QD_V03,HBP_COR,HBP_BPM,HBP_QD_V03,HBP_DR_QD_ED)
HBP_QD_V4   : LINE=(HBP_DR_QD_ED,HBP_QD_V04,HBP_COR,HBP_BPM,HBP_QD_V04,HBP_DR_QD_ED)
HBP_QD_V4A  : LINE=(HBP_DR_QD_ED,HBP_QD_V04,HBP_COR)
HBP_QD_V4B  : LINE=(HBP_BPM,HBP_QD_V04,HBP_DR_QD_ED)
HBP_QD_V5   : LINE=(HBP_DR_QD_ED,HBP_QD_V05,HBP_COR,HBP_BPM,HBP_QD_V05,HBP_DR_QD_ED)
HBP_QD_V5A   : LINE=(HBP_DR_QD_ED,HBP_QD_V05,HBP_COR)
HBP_QD_V5B   : LINE=(HBP_BPM,HBP_QD_V05,HBP_DR_QD_ED)
HBP_QD_V6   : LINE=(HBP_DR_QD_ED,HBP_QD_V06,HBP_COR,HBP_BPM,HBP_QD_V06,HBP_DR_QD_ED)
HBP_QD_V7   : LINE=(HBP_DR_QD_ED,HBP_QD_V07,HBP_COR,HBP_BPM,HBP_QD_V07,HBP_DR_QD_ED)

FITT_HBP_0:  MARKER, FITPOINT=1
FITT_HBP_1:  MARKER, FITPOINT=1
FITT_HBP_2:  MARKER, FITPOINT=1
FITT_HBP_3:  MARKER, FITPOINT=1
FITT_HBP_4:  MARKER, FITPOINT=1
FITT_HBP_5:  MARKER, FITPOINT=1
FITT_HBP_6:  MARKER, FITPOINT=1
FITT_HBP_7:  MARKER, FITPOINT=1
FITT_HBP_8:  MARKER, FITPOINT=1
FITT_HBP_PI:  MARKER, FITPOINT=1
FITT_HBP_PO:  MARKER, FITPOINT=1
HBP_CNT0     : DRIFT, L=0 
HBP_IN_FIXED_1   : LINE=(HBP_CNT0,HBP_DP_SEPM1,2*HBP_DR_BP_C,3*HBP_DR_BP)
HBP_IN_FIXED_2   : LINE=(5*HBP_DR_BP,HBP_CNT0,HBP_DP_SEPM1)
HBP_OU_FIXED_1   : LINE=(HBP_DP_SEPM2,2*HBP_DR_BP_C,3*HBP_DR_BP)
HBP_OU_FIXED_2   : LINE=(5*HBP_DR_BP,HBP_DP_SEPM2)


HBP_MATCH_IN	:LINE=(HBP_DR_V1,HBP_CNT0,HBP_QD_V1,HBP_DR_V2,HBP_DR_V2,HBP_CNT0,HBP_QD_V1,HBP_DR_V1)
!HBP_MATCH_OU	:LINE=(HBP_DR_V6,HBP_QD_V6,HBP_DR_V6)
!HBP_MATCH_OU	:LINE=(HBP_SX_V02,HBP_DR_V8,HBP_QD_V7,HBP_DR_V8,HBP_SX_V01)

FITT_HBP_AA:  MARKER, FITPOINT=1
FITT_HBP_BB:  MARKER, FITPOINT=1
HBP_MATCH_CNT_IN_1 :LINE=(HBP_DR_BP_C,HBP_DR_V3,HBP_QD_V2,FITT_HBP_AA,HBP_DR_V4,HBP_QD_V3,HBP_DR_V5,HBP_QD_V4A)
HBP_MATCH_CNT_IN_2 :LINE=(HBP_QD_V4B,HBP_DR_V5,HBP_QD_V3,HBP_DR_V4,FITT_HBP_BB,HBP_QD_V2,HBP_DR_V3,HBP_DR_BP)

HBP_MATCH_OU	:LINE=(HBP_DR_V6,HBP_QD_V5,HBP_DR_V7,HBP_DR_V7,HBP_QD_V5,HBP_DR_V6)

HBP_WA_OU1:       WATCH,filename=\"hbp_ou1.sdds\",mode=coord, label=\"hbp first \"
HBP_WA_OU2:       WATCH,filename=\"hbp_ou2.sdds\",mode=coord, label=\"hbp cent1 \"
HBP_WA_OU3:       WATCH,filename=\"hbp_ou3.sdds\",mode=coord, label=\"hbp cent2 \"

HBP_WA_OU1a:       WATCH,filename=\"hbp_ou1a.sdds\",mode=coord, label=\"hbp first \"
HBP_WA_OU2a:       WATCH,filename=\"hbp_ou2a.sdds\",mode=coord, label=\"hbp cent1 \"
HBP_WA_OU3a:       WATCH,filename=\"hbp_ou3a.sdds\",mode=coord, label=\"hbp cent2 \"

HBP_WA_OU4:       WATCH,filename=\"hbp_ou4.sdds\",mode=coord, label=\"hbp ou \" 
HBP_CNT     : CENTER                 
HBP_CNT1     : DRIFT,L=0

HBP_MATCH_TO_1   :LINE=(FITT_HBP_0,HBP_DP_SEPM1,HBP_DP_SEPM1,HBP_DR_BP_C, &
                   3*HBP_DR_BP,HBP_MATCH_IN,4*HBP_DR_BP, & 
                                  HBP_DP_SEPM1,HBP_DP_SEPM1,FITT_HBP_3)
            
HBP_MATCH_TO_2   :LINE=(FITT_HBP_3, HBP_MATCH_CNT_IN_1,FITT_HBP_4,HBP_MATCH_CNT_IN_2,FITT_HBP_5)

!HBP_MATCH_TO_3   :LINE=(FITT_HBP_5, HBP_OU_FIXED_1,FITT_HBP_6,HBP_MATCH_OU,FITT_HBP_7,5*HBP_DR_BP,FITT_HBP_PO,HBP_DP_SEPM2,FITT_HBP_8)

HBP_MATCH_TO_3   :LINE=(FITT_HBP_5, HBP_DP_SEPM2,HBP_DP_SEPM2,HBP_DR_BP_C, &
                   3*HBP_DR_BP,HBP_MATCH_OU,2*HBP_DR_BP, 2*HBP_DR_BP, & 
                                   HBP_DP_SEPM2,HBP_DP_SEPM2,FITT_HBP_8)


HBP_MATCHED_1    :LINE=(FITT_HBP_0,HBP_CNT0,HBP_DP_SEPM1,HBP_DP_SEPM1,HBP_DR_BP_C,3*HBP_DR_BP,HBP_MATCH_IN, &
                    4*HBP_DR_BP,FITT_HBP_PI,HBP_CNT0,HBP_DP_SEPM1,HBP_DP_SEPM1)
HBP_MATCHED_2    :LINE=(HBP_MATCH_CNT_IN_1,HBP_MATCH_CNT_IN_2)
!HBP_MATCHED_3    :LINE=(HBP_OU_FIXED_1,HBP_MATCH_OU,HBP_OU_FIXED_2)

HBP_MATCHED_3    :LINE=(FITT_HBP_5, HBP_DP_SEPM2,HBP_DP_SEPM2,HBP_DR_BP_C, &
                   3*HBP_DR_BP,HBP_MATCH_OU,2*HBP_DR_BP, 2*HBP_DR_BP, & 
                                   HBP_DP_SEPM2,HBP_DP_SEPM2,FITT_HBP_8)

HBP_MATCHED_TR_1: LINE=(HBP_MATCHED_1,HBP_CNT,HBP_WA_OU1)
HBP_MATCHED_TR_2: LINE=(HBP_MATCHED_2,HBP_CNT,HBP_WA_OU2)
HBP_MATCHED_TR_3: LINE=(HBP_MATCHED_3,HBP_CNT,HBP_WA_OU3)

HBP_MATCHED_TR_12: LINE=(HBP_MATCHED_1,HBP_WA_OU1a)
HBP_MATCHED_TR_22: LINE=(HBP_MATCHED_2,HBP_CNT,HBP_WA_OU2a)
HBP_MATCHED_TR_32: LINE=(HBP_MATCHED_3,HBP_CNT,HBP_WA_OU3a)
               
"



puts $filee "

!------------------------------------------------------------------------------
! Setup HXR undulator  module
!------------------------------------------------------------------------------

HXR_DR_FL     : DRIFT,L= $flange, GROUP=\"HXR\"
HXR_DR_BL     : DRIFT,L= $belowl, GROUP=\"HXR\"
HXR_DR_SV     : DRIFT,L= $vacpor, GROUP=\"HXR\"
HXR_DR_VS     : DRIFT,L= $viewsc, GROUP=\"HXR\"
HXR_DR_DR     : DRIFT,L= $viewsc, GROUP=\"HXR\"
HXR_DR_CT     : DRIFT,L= $viewsc, GROUP=\"HXR\"


HXR_WIG_0: WIGGLER, L = [expr $hxr_np*$hxr_pl], B = 0.75, POLES = $hxr_np, GROUP=\"HXR\" &

HXR_DR_WIG    : DRIFT,L= [expr $hxr_np*$hxr_pl], GROUP=\"HXR\"
HXR_DR_WIG_H    : DRIFT,L= [expr $hxr_np*$hxr_pl*0.5], GROUP=\"HXR\"
HXR_DR_WIG_ED : DRIFT,L= $hxr_wiged, GROUP=\"HXR\"
HXR_DR_PHS : DRIFT,L= 0.2, GROUP=\"HXR\"
HXR_WIG       : LINE=(HXR_DR_WIG_ED,HXR_WIG_0,HXR_DR_WIG_ED)

HXR_WIGW      : LINE=(HXR_DR_SV,HXR_DR_BL,HXR_WIG,HXR_DR_BL)
HXR_WIGC      : LINE=(HXR_DR_VS,HXR_DR_BL,HXR_WIG,HXR_DR_BL)
HXR_DR_WIGH    : LINE=(HXR_DR_DR,HXR_DR_BL,HXR_DR_WIG_ED, HXR_DR_WIG_H)

HXR_DR_QD_ED  : DRIFT,L= $hxr_quad_edge, GROUP=\"HXR\"
HXR_QD_FH     : QUAD, L=[expr $hxr_quad_l*0.5], K1=[expr  1.0*$hxr_quad_k], GROUP=\"HXR\"
HXR_QD_DH     : QUAD, L=[expr $hxr_quad_l*0.5], K1=[expr -1.0*$hxr_quad_k], GROUP=\"HXR\"
HXR_COR       : KICKER, L=0.0,HKICK=0,VKICK=0, GROUP=\"HXR\"
HXR_BPM       : MONI, L=0.0, GROUP=\"HXR\"


HXR_QD_F      : LINE=(HXR_DR_QD_ED,HXR_QD_FH,HXR_COR,HXR_BPM,HXR_QD_FH,HXR_DR_QD_ED)
HXR_QD_D      : LINE=(HXR_DR_QD_ED,HXR_QD_DH,HXR_COR,HXR_BPM,HXR_QD_DH,HXR_DR_QD_ED)
HXR_MODULE    : LINE=(HXR_QD_F, HXR_WIGW,HXR_QD_D,HXR_WIGC)

!------------------------------------------------------------------------------
! Linac 1 matching quads
!------------------------------------------------------------------------------


HXR_QD_V01: QUAD,GROUP=\"HXR\",L=[expr $hxr_quad_l*0.5],K1=-7.435578148916512
HXR_QD_V02: QUAD,GROUP=\"HXR\",L=[expr $hxr_quad_l*0.5],K1=9.76104990543428
HXR_QD_V03: QUAD,GROUP=\"HXR\",L=[expr $hxr_quad_l*0.5],K1=-3.059188882171066
HXR_QD_V04: QUAD,GROUP=\"HXR\",L=[expr $hxr_quad_l*0.5],K1=-5
HXR_DR_V1: DRIF,GROUP=\"HXR\",L=0.205778599667551
HXR_DR_V2: DRIF,GROUP=\"HXR\",L=0.5975909758789514
HXR_DR_V3: DRIF,GROUP=\"HXR\",L=2.905816054764469
HXR_DR_V4: DRIF,GROUP=\"HXR\",L=1.572213000296411
HXR_DR_V5: DRIF,GROUP=\"HXR\",L=0.7001603960975262

HXR_QD_V1   : LINE=(HXR_DR_QD_ED,HXR_QD_V01,HXR_COR,HXR_BPM,HXR_QD_V01,HXR_DR_QD_ED)
HXR_QD_V2   : LINE=(HXR_DR_QD_ED,HXR_QD_V02,HXR_COR,HXR_BPM,HXR_QD_V02,HXR_DR_QD_ED)
HXR_QD_V3   : LINE=(HXR_DR_QD_ED,HXR_QD_V03,HXR_COR,HXR_BPM,HXR_QD_V03,HXR_DR_QD_ED)
HXR_QD_V4   : LINE=(HXR_DR_QD_ED,HXR_QD_V04,HXR_COR,HXR_BPM,HXR_QD_V04,HXR_DR_QD_ED)

!HXR_MATCH0    : LINE=(HXR_DR_V1,HXR_QD_V1,HXR_DR_V2,HXR_QD_V2,HXR_DR_V3,HXR_QD_V3,HXR_DR_V4,HXR_QD_V4,HXR_DR_V5)
HXR_MATCH0    : LINE=(HXR_DR_V1,HXR_QD_V1,HXR_DR_V2,HXR_QD_V2,HXR_DR_V3,HXR_QD_V3,HXR_DR_V4,HXR_DR_CT)

HXR_FITP1     : MARKER, FITPOINT=1
HXR_FITP2     : MARKER, FITPOINT=1
HXR_FITP3     : MARKER, FITPOINT=1
HXR_FITP4     : MARKER, FITPOINT=1

HXR_FITT_1    : LINE=(HXR_DR_QD_ED,HXR_QD_FH, HXR_FITP1, HXR_QD_FH, HXR_DR_QD_ED, HXR_WIGW)
HXR_FITT_2    : LINE=(HXR_DR_QD_ED,HXR_QD_DH, HXR_FITP2, HXR_QD_DH, HXR_DR_QD_ED, HXR_WIGW)
HXR_FITT_3    : LINE=(HXR_DR_QD_ED,HXR_QD_FH, HXR_FITP3, HXR_QD_FH, HXR_DR_QD_ED, HXR_WIGW)
HXR_FITT_4    : LINE=(HXR_DR_QD_ED,HXR_QD_DH, HXR_FITP4, HXR_QD_DH)

HXR_WA_M_OU    : WATCH,filename=\"hxr_mtch_ou.sdds\",mode=coord, label=\"hxr m ou \"
HXR_WA_OU      : WATCH,filename=\"hxr_ou.sdds\",mode=coord, label=\"hxr ou \"
HXR_WA_OUa      : WATCH,filename=\"hxr_oua.sdds\",mode=coord, label=\"hxr ou \"
HXR_MATCH_TO  : LINE=(HXR_MATCH0,HXR_FITT_1,HXR_FITT_2,HXR_FITT_3,HXR_FITT_4 )
HXR_MATCHED   : LINE=(HXR_MATCH0,$hxr_nmodule*HXR_MODULE)
HXR_MATCHED_TR: LINE=(HXR_MATCH0,$hxr_nmodule*HXR_MODULE,HXR_WA_OU)
HXR_MATCHED_TR2: LINE=(HXR_MATCH0,$hxr_nmodule*HXR_MODULE,HXR_WA_OUa)
"



if {$case9 > 0  && $case10a <1 && $case10b <1 && $case10c <1  && $case11 <1 && $case12 <1 } {


puts $filee "
MATCHINS_[expr $indx+0] : LINE=(LN3_BP_MATCH_TO)
MATCHEDS_[expr $indx+1] : LINE=(LN3_BP_MATCHED)
TRACKINS_[expr $indx+1] : LINE=(BNCH,LN3_BP_MATCHED_TR)

MATCHEDA_[expr $indx+1]  : LINE=(LN3_BP_MATCHED)
TRACKA_[expr $indx+1]    : LINE=(BNCH,LN3_BP_MATCHED_TR2)
"
} elseif {$case9 >0  && $case10a >0 && $case10b <1 && $case10c <1  && $case11 <1 &&  $case12 <1 } {
    

puts $filee "

MATCHINS_[expr $indx+0] : LINE=(LN3_BP_MATCH_TO)
MATCHEDS_[expr $indx+1] : LINE=(LN3_BP_MATCHED)
TRACKINS_[expr $indx+1] : LINE=(BNCH,LN3_BP_MATCHED_TR)

MATCHEDA_[expr $indx+1]  : LINE=(LN3_BP_MATCHED)
TRACKA_[expr $indx+1]    : LINE=(BNCH,LN3_BP_MATCHED_TR)

MATCHINS_[expr $indx+1] : LINE=(HBP_MATCH_TO_1)
MATCHEDS_[expr $indx+2] : LINE=(HBP_MATCHED_1)
TRACKINS_[expr $indx+2] : LINE=(BNCH,HBP_MATCHED_TR_1)

MATCHEDA_[expr $indx+2]  : LINE=(LN3_BP_MATCHED_TR,HBP_MATCHED_1)
TRACKA_[expr $indx+2]    : LINE=(BNCH,LN3_BP_MATCHED_TR2,HBP_MATCHED_TR_12)
"    

} elseif {$case9 >0  && $case10a >0 && $case10b >0 && $case10c <1  && $case11 <1 &&  $case12 <1 } {
    

puts $filee "

MATCHINS_[expr $indx+0] : LINE=(LN3_BP_MATCH_TO)
MATCHEDS_[expr $indx+1] : LINE=(LN3_BP_MATCHED)
TRACKINS_[expr $indx+1] : LINE=(BNCH,LN3_BP_MATCHED_TR)

MATCHEDA_[expr $indx+1]  : LINE=(LN3_BP_MATCHED)
TRACKA_[expr $indx+1]    : LINE=(BNCH,LN3_BP_MATCHED_TR)

MATCHINS_[expr $indx+1] : LINE=(HBP_MATCH_TO_1)
MATCHEDS_[expr $indx+2] : LINE=(HBP_MATCHED_1)
TRACKINS_[expr $indx+2] : LINE=(BNCH,HBP_MATCHED_TR_1)

MATCHINS_[expr $indx+2] : LINE=(HBP_MATCH_TO_2)
MATCHEDS_[expr $indx+3] : LINE=(HBP_MATCHED_2)
TRACKINS_[expr $indx+3] : LINE=(BNCH,HBP_MATCHED_TR_2)

MATCHEDA_[expr $indx+3]  : LINE=(LN3_BP_MATCHED_TR,HBP_MATCHED_1,HBP_MATCHED_2)
TRACKA_[expr $indx+3]    : LINE=(BNCH,LN3_BP_MATCHED_TR2,HBP_MATCHED_TR_12,HBP_MATCHED_TR_22)
"

} elseif {$case9 >0  && $case10a >0 && $case10b >0 && $case10c > 0  && $case11 <1 &&  $case12 <1 } {
    

puts $filee "

MATCHINS_[expr $indx+0] : LINE=(LN3_BP_MATCH_TO)
MATCHEDS_[expr $indx+1] : LINE=(LN3_BP_MATCHED)
TRACKINS_[expr $indx+1] : LINE=(BNCH,LN3_BP_MATCHED_TR)

MATCHEDA_[expr $indx+1]  : LINE=(LN3_BP_MATCHED)
TRACKA_[expr $indx+1]    : LINE=(BNCH,LN3_BP_MATCHED_TR)

MATCHINS_[expr $indx+1] : LINE=(HBP_MATCH_TO_1)
MATCHEDS_[expr $indx+2] : LINE=(HBP_MATCHED_1)
TRACKINS_[expr $indx+2] : LINE=(BNCH,HBP_MATCHED_TR_1)

MATCHINS_[expr $indx+2] : LINE=(HBP_MATCH_TO_2)
MATCHEDS_[expr $indx+3] : LINE=(HBP_MATCHED_2)
TRACKINS_[expr $indx+3] : LINE=(BNCH,HBP_MATCHED_TR_2)

MATCHINS_[expr $indx+3] : LINE=(HBP_MATCH_TO_3)
MATCHEDS_[expr $indx+4] : LINE=(HBP_MATCHED_3)
TRACKINS_[expr $indx+4] : LINE=(BNCH,HBP_MATCHED_TR_3)

MATCHEDA_[expr $indx+4]  : LINE=(LN3_BP_MATCHED_TR,HBP_MATCHED_1,HBP_MATCHED_2,HBP_MATCHED_3)
TRACKA_[expr $indx+4]    : LINE=(BNCH,LN3_BP_MATCHED_TR2,HBP_MATCHED_TR_12,HBP_MATCHED_TR_22,HBP_MATCHED_TR_32)
"
} elseif {$case9 >0  && $case10a >0 && $case10b >0 && $case10c > 0  && $case11 >0 &&  $case12 <1 } {
    

puts $filee "

MATCHINS_[expr $indx+0] : LINE=(LN3_BP_MATCH_TO)
MATCHEDS_[expr $indx+1] : LINE=(LN3_BP_MATCHED)
TRACKINS_[expr $indx+1] : LINE=(BNCH,LN3_BP_MATCHED_TR)

MATCHEDA_[expr $indx+1]  : LINE=(LN3_BP_MATCHED)
TRACKA_[expr $indx+1]    : LINE=(BNCH,LN3_BP_MATCHED_TR)

MATCHINS_[expr $indx+1] : LINE=(HBP_MATCH_TO_1)
MATCHEDS_[expr $indx+2] : LINE=(HBP_MATCHED_1)
TRACKINS_[expr $indx+2] : LINE=(BNCH,HBP_MATCHED_TR_1)

MATCHINS_[expr $indx+2] : LINE=(HBP_MATCH_TO_2)
MATCHEDS_[expr $indx+3] : LINE=(HBP_MATCHED_2)
TRACKINS_[expr $indx+3] : LINE=(BNCH,HBP_MATCHED_TR_2)

MATCHINS_[expr $indx+3] : LINE=(HBP_MATCH_TO_3)
MATCHEDS_[expr $indx+4] : LINE=(HBP_MATCHED_3)
TRACKINS_[expr $indx+4] : LINE=(BNCH,HBP_MATCHED_TR_3)

MATCHINS_[expr $indx+4] : LINE=(HXR_MATCH_TO)
MATCHEDS_[expr $indx+5] : LINE=(HXR_MATCHED)
TRACKINS_[expr $indx+5] : LINE=(BNCH,HXR_MATCHED_TR)

MATCHEDA_[expr $indx+5]  : LINE=(LN3_BP_MATCHED_TR,HBP_MATCHED_1,HBP_MATCHED_2,HBP_MATCHED_3,HXR_MATCHED)
TRACKA_[expr $indx+5]    : LINE=(BNCH,LN3_BP_MATCHED_TR2,HBP_MATCHED_TR_12,HBP_MATCHED_TR_22,HBP_MATCHED_TR_32,HXR_MATCHED_TR2)

"


} else {
    
    puts "done"
    
}



close $filee


