#!/bin/sh  
# \
exec tclsh "$0" "$@"
# set names [gets stdin]

set infile      [lindex $argv 0]
set outfile      [lindex $argv 1]

puts $infile
puts $outfile
# 
set sepe " "
# 


exec  sdds2plaindata $infile $outfile -outputMode=ascii -noRowCount -separator=$sepe \
    -column=s,format=%16.9e \
    -column=betax,format=%16.9e \
    -column=alphax,format=%16.9e \
    -column=betay,format=%16.9e \
    -column=alphay,format=%16.9e \
    -column=E,format=%16.9e \
    -column=etax,format=%16.9e \
    -column=etaxp,format=%16.9e

    
set fl [open $outfile.plt.tcl w]


puts $fl "

set term qt size 1200,600 enhanced font 'Sans,18'
set y2label 'etax'
set xlabel 'Distance (m)' 
set ylabel ' {/Symbol b}_{x,y} (m)' 
set ytics nomirror
set y2tics
set tics out
set autoscale  y
set autoscale y2
set grid lt 0 
pl '$outfile' u (\$1*1e0):(\$2*1e0) w l lw 3 lc 'red'  ti '{/Symbol b}_{x)', \
   '$outfile' u (\$1*1e0):(\$4*1e0) w l lw 3 lc 'blue'  ti '{/Symbol b}_{y)', \
   '$outfile' u (\$1*1e0):(\$7*1e0) w l lw 3 lc 'black'  axes x1y2 ti 'etax', \
   '$outfile' u (\$1*1e0):(\$8*1e0) w l lw 3 lc 'gray'  axes x1y2 ti 'etaxp', \

set term png size 1200,600 enhanced font 'Sans,20'
set output '$outfile.png' 
replot

pause -1
"
close $fl
