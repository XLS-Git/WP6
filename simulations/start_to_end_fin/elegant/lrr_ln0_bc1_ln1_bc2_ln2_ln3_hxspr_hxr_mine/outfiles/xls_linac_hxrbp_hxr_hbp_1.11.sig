SDDS1
!# little-endian
&description text="sigma matrix--input: xls_linac_hxrbp_hxr_hbp_1.track.ele  lattice: xls_linac_hxrbp_hxr.11.lte", contents="sigma matrix", &end
&parameter name=Step, description="Simulation step", type=long, &end
&parameter name=SVNVersion, description="SVN version number", type=string, fixed_value=unknown, &end
&column name=s, units=m, description=Distance, type=double,  &end
&column name=ElementName, description="Element name", format_string=%10s, type=string,  &end
&column name=ElementOccurence, description="Occurence of element", format_string=%6ld, type=long,  &end
&column name=ElementType, description="Element-type name", format_string=%10s, type=string,  &end
&column name=s1, symbol="$gs$r$b1$n", units=m, description="sqrt(<x*x>)", type=double,  &end
&column name=s12, symbol="$gs$r$b12$n", units=m, description="<x*xp'>", type=double,  &end
&column name=s13, symbol="$gs$r$b13$n", units="m$a2$n", description="<x*y>", type=double,  &end
&column name=s14, symbol="$gs$r$b14$n", units=m, description="<x*y'>", type=double,  &end
&column name=s15, symbol="$gs$r$b15$n", units="m$a2$n", description="<x*s>", type=double,  &end
&column name=s16, symbol="$gs$r$b16$n", units=m, description="<x*delta>", type=double,  &end
&column name=s17, symbol="$gs$r$b17$n", units="m*s", description="<x*t>", type=double,  &end
&column name=s2, symbol="$gs$r$b2$n", description="sqrt(<x'*x'>)", type=double,  &end
&column name=s23, symbol="$gs$r$b23$n", units=m, description="<x'*y>", type=double,  &end
&column name=s24, symbol="$gs$r$b24$n", description="<x'*y'>", type=double,  &end
&column name=s25, symbol="$gs$r$b25$n", units=m, description="<x'*s>", type=double,  &end
&column name=s26, symbol="$gs$r$b26$n", description="<x'*delta>", type=double,  &end
&column name=s27, symbol="$gs$r$b27$n", units=s, description="<x'*t>", type=double,  &end
&column name=s3, symbol="$gs$r$b3$n", units=m, description="sqrt(<y*y>)", type=double,  &end
&column name=s34, symbol="$gs$r$b34$n", units=m, description="<y*y'>", type=double,  &end
&column name=s35, symbol="$gs$r$b35$n", units="m$a2$n", description="<y*s>", type=double,  &end
&column name=s36, symbol="$gs$r$b36$n", units=m, description="<y*delta>", type=double,  &end
&column name=s37, symbol="$gs$r$b37$n", units="m*s", description="<y*t>", type=double,  &end
&column name=s4, symbol="$gs$r$b4$n", description="sqrt(<y'*y')>", type=double,  &end
&column name=s45, symbol="$gs$r$b45$n", units=m, description="<y'*s>", type=double,  &end
&column name=s46, symbol="$gs$r$b46$n", description="<y'*delta>", type=double,  &end
&column name=s47, symbol="$gs$r$b47$n", units=s, description="<y'*t>", type=double,  &end
&column name=s5, symbol="$gs$r$b5$n", units=m, description="sqrt(<s*s>)", type=double,  &end
&column name=s56, symbol="$gs$r$b56$n", units=m, description="<s*delta>", type=double,  &end
&column name=s57, symbol="$gs$r$b57$n", units="m*s", description="<s*t>", type=double,  &end
&column name=s6, symbol="$gs$r$b6$n", description="sqrt(<delta*delta>)", type=double,  &end
&column name=s67, symbol="$gs$r$b67$n", units=s, description="<delta*t>", type=double,  &end
&column name=s7, symbol="$gs$r$b7$n", description="sqrt(<t*t>)", type=double,  &end
&column name=ma1, symbol="max$sb$ex$sb$e", units=m, description="maximum absolute value of x", type=double,  &end
&column name=ma2, symbol="max$sb$ex'$sb$e", description="maximum absolute value of x'", type=double,  &end
&column name=ma3, symbol="max$sb$ey$sb$e", units=m, description="maximum absolute value of y", type=double,  &end
&column name=ma4, symbol="max$sb$ey'$sb$e", description="maximum absolute value of y'", type=double,  &end
&column name=ma5, symbol="max$sb$e$gD$rs$sb$e", units=m, description="maximum absolute deviation of s", type=double,  &end
&column name=ma6, symbol="max$sb$e$gd$r$sb$e", description="maximum absolute value of delta", type=double,  &end
&column name=ma7, symbol="max$sb$e$gD$rt$sb$e", units=s, description="maximum absolute deviation of t", type=double,  &end
&column name=minimum1, symbol="x$bmin$n", units=m, type=double,  &end
&column name=minimum2, symbol="x'$bmin$n", units=m, type=double,  &end
&column name=minimum3, symbol="y$bmin$n", units=m, type=double,  &end
&column name=minimum4, symbol="y'$bmin$n", units=m, type=double,  &end
&column name=minimum5, symbol="$gD$rs$bmin$n", units=m, type=double,  &end
&column name=minimum6, symbol="$gd$r$bmin$n", units=m, type=double,  &end
&column name=minimum7, symbol="t$bmin$n", units=s, type=double,  &end
&column name=maximum1, symbol="x$bmax$n", units=m, type=double,  &end
&column name=maximum2, symbol="x'$bmax$n", units=m, type=double,  &end
&column name=maximum3, symbol="y$bmax$n", units=m, type=double,  &end
&column name=maximum4, symbol="y'$bmax$n", units=m, type=double,  &end
&column name=maximum5, symbol="$gD$rs$bmax$n", units=m, type=double,  &end
&column name=maximum6, symbol="$gd$r$bmax$n", units=m, type=double,  &end
&column name=maximum7, symbol="t$bmax$n", units=s, type=double,  &end
&column name=Sx, symbol="$gs$r$bx$n", units=m, description=sqrt(<(x-<x>)^2>), type=double,  &end
&column name=Sxp, symbol="$gs$r$bx'$n", description=sqrt(<(x'-<x'>)^2>), type=double,  &end
&column name=Sy, symbol="$gs$r$by$n", units=m, description=sqrt(<(y-<y>)^2>), type=double,  &end
&column name=Syp, symbol="$gs$r$by'$n", description=sqrt(<(y'-<y'>)^2>), type=double,  &end
&column name=Ss, symbol="$gs$r$bs$n", units=m, description=sqrt(<(s-<s>)^2>), type=double,  &end
&column name=Sdelta, symbol="$gs$bd$n$r", description=sqrt(<(delta-<delta>)^2>), type=double,  &end
&column name=St, symbol="$gs$r$bt$n", units=s, description=sqrt(<(t-<t>)^2>), type=double,  &end
&column name=ex, symbol="$ge$r$bx$n", units=m, description="geometric horizontal emittance", type=double,  &end
&column name=enx, symbol="$ge$r$bx,n$n", units=m, description="normalized horizontal emittance", type=double,  &end
&column name=ecx, symbol="$ge$r$bx,c$n", units=m, description="geometric horizontal emittance less dispersive contributions", type=double,  &end
&column name=ecnx, symbol="$ge$r$bx,cn$n", units=m, description="normalized horizontal emittance less dispersive contributions", type=double,  &end
&column name=ey, symbol="$ge$r$by$n", units=m, description="geometric vertical emittance", type=double,  &end
&column name=eny, symbol="$ge$r$by,n$n", units=m, description="normalized vertical emittance", type=double,  &end
&column name=ecy, symbol="$ge$r$by,c$n", units=m, description="geometric vertical emittance less dispersive contributions", type=double,  &end
&column name=ecny, symbol="$ge$r$by,cn$n", units=m, description="normalized vertical emittance less dispersive contributions", type=double,  &end
&column name=betaxBeam, symbol="$gb$r$bx,beam$n", units=m, description="betax for the beam, excluding dispersive contributions", type=double,  &end
&column name=alphaxBeam, symbol="$ga$r$bx,beam$n", description="alphax for the beam, excluding dispersive contributions", type=double,  &end
&column name=betayBeam, symbol="$gb$r$by,beam$n", units=m, description="betay for the beam, excluding dispersive contributions", type=double,  &end
&column name=alphayBeam, symbol="$ga$r$by,beam$n", description="alphay for the beam, excluding dispersive contributions", type=double,  &end
&data mode=binary, &end
&                 _BEG_      MARK{N�D�>@�s�|ҽ��9�?���@�&��=��a	�=$�%�s�btPм;0$�Q���>w���F�=��0����t�0�=iM4����=�Ů�4��;��>z��>_M`8�p�|�g���?���[r$3���h�]|�;Mm7Wq�>�%s��&=�^���H�=ԯ�vG5d;�)Ƒ��>��r���=M�1����; � ��WC?	��ަ'<e���7�<T �(I?�74F�b?~����\*?���'�?   �=v�>B^7�2p|?   �j=T �(I��74F�b�~����\*�l�o�J@�    ��ɾn�M8:�v�   `~�X�l�	?���F�?����%?���'�?   �=v�>B^7�2p|?   �j={N�D�>0$�Q���>��>z��>;Mm7Wq�>�)Ƒ��> � ��WC?e���7�<�kKr�n�=N�Q̇�>�B��Wc�=�:|�y�>*��2�=}u�,��>,��	2�=�0�j)��>p�Õ���?7���DP�?��|T�)@��S�Yl@           BNCH      CHARGE{N�D�>@�s�|ҽ��9�?���@�&��=��a	�=$�%�s�btPм;0$�Q���>w���F�=��0����t�0�=iM4����=�Ů�4��;��>z��>_M`8�p�|�g���?���[r$3���h�]|�;Mm7Wq�>�%s��&=�^���H�=ԯ�vG5d;�)Ƒ��>��r���=M�1����; � ��WC?	��ަ'<e���7�<T �(I?�74F�b?~����\*?���'�?   �=v�>B^7�2p|?   �j=T �(I��74F�b�~����\*�l�o�J@�    ��ɾn�M8:�v�   `~�X�l�	?���F�?����%?���'�?   �=v�>B^7�2p|?   �j={N�D�>0$�Q���>��>z��>;Mm7Wq�>�)Ƒ��> � ��WC?e���7�<�kKr�n�=N�Q̇�>�B��Wc�=�:|�y�>*��2�=}u�,��>,��	2�=�0�j)��>p�Õ���?7���DP�?��|T�)@��S�Yl@        
   FITT_HBP_0      MARK{N�D�>@�s�|ҽ��9�?���@�&��=��a	�=$�%�s�btPм;0$�Q���>w���F�=��0����t�0�=iM4����=�Ů�4��;��>z��>_M`8�p�|�g���?���[r$3���h�]|�;Mm7Wq�>�%s��&=�^���H�=ԯ�vG5d;�)Ƒ��>��r���=M�1����; � ��WC?	��ަ'<e���7�<T �(I?�74F�b?~����\*?���'�?   �=v�>B^7�2p|?   �j=T �(I��74F�b�~����\*�l�o�J@�    ��ɾn�M8:�v�   `~�X�l�	?���F�?����%?���'�?   �=v�>B^7�2p|?   �j={N�D�>0$�Q���>��>z��>;Mm7Wq�>�)Ƒ��> � ��WC?e���7�<�kKr�n�=N�Q̇�>�B��Wc�=�:|�y�>*��2�=}u�,��>,��	2�=�0�j)��>p�Õ���?7���DP�?��|T�)@��S�Yl@           HBP_CNT0      DRIF{N�D�>@�s�|ҽ��9�?���@�&��=��a	�=$�%�s�btPм;0$�Q���>w���F�=��0����t�0�=iM4����=�Ů�4��;��>z��>_M`8�p�|�g���?���[r$3���h�]|�;Mm7Wq�>�%s��&=�^���H�=ԯ�vG5d;�)Ƒ��>��r���=M�1����; � ��WC?	��ަ'<e���7�<T �(I?�74F�b?~����\*?���'�?   �=v�>B^7�2p|?   �j=T �(I��74F�b�~����\*�l�o�J@�    ��ɾn�M8:�v�   `~�X�l�	?���F�?����%?���'�?   �=v�>B^7�2p|?   �j={N�D�>0$�Q���>��>z��>;Mm7Wq�>�)Ƒ��> � ��WC?e���7�<�kKr�n�=N�Q̇�>�B��Wc�=�:|�y�>*��2�=}u�,��>,��	2�=�0�j)��>p�Õ���?7���DP�?��|T�)@��S�Yl@��0-$�?   HBP_DP_SEPM1   	   CSRCSBEND��\�a�>B�8L���=m�������C`�~�=p9@&do=��YU�V$��ã����;Lr��f�>/�*��x�=AM?�/暽A�g�:�r�;�X�J�~)�yBC��[����>�8#��ݽGı��I=A=
�ķ�._./��;�=�Wq�>��(�5����jR�=�%��r�����e�>�]Ҏ�=�[<��;<�
]D?������<\�{�a��<SF��?�;�2?sM�nN�'?�Q6)�?   ��m�>J�uB(�}?   ���=SF���``��v1�sM�nN�'�9���6@�   ��ʾ>;�Y�v�   �	�ֻx�1?�;�2?�um��#?�Q6)�?   ��m�>J�uB(�}?   ���=��\�a�>Lr��f�>[����>�=�Wq�>����e�><�
]D?\�{�a��<Fi����=ٻpM��>�]9����=�hFUԈ>�}]2�=yN�ރ>���2�=�L���ރ>�����4�?w������?�ǳ]$@�hޗG�@��0-$�?   HBP_DP_SEPM1   	   CSRCSBEND�l�G>��>Өd�*�>=Ea�9i}��E�f �V=�(�b[1��.}]���C�Б�sm-λGy�{?��Q�=V��zVx����Z�E쬽W5ȆBb^�+�؉n��҃�"��>���93Xٽyi(�)�S=,/�d$ƽ._�,�͑;Z�IWq�>9�[,A��R�(�=D|����~����o,��>6��ژU�=S��#l�;��g F?�?��<4";X��<�sl�Z%?ĢD���@?d=rOï$?�`V;��?   X���>��>��}?   Ri]=�sl�Z%�ĢD���@�d=rOï$���?�   �;̾m��J��v�   ȟB	�%O��#?swj���>?y5��+!?�`V;��?   X���>��>��}?   Ri]=�l�G>��>Gy�{?�҃�"��>Z�IWq�>���o,��>��g F?4";X��<�btKt��=8@T�ő�>���C��=����b/�>U���32�=;� �g݃>N^�b!2�=~��y[݃>���W/��?��w�Q�0ʳ�|}@�����
@@�ƽ�?   HBP_DR_BP_C      CSRDRIFT���~�>y�
��O>���o�2y=�"eF��|���χ7H��|�Cva}S��� Y޻Gy�{?s�����=V��zVx��zo|&Z�AQ�`�(_�����mn�E�(����>����y�սX��wKQ=
��E6!ý�"D�-�;Z�IWq�>^M�:A����^�=�?��h�~�h�C���>m��9>�=B ��m�;׬+�ŽF?� ��7P$<�r�����<��doq�4?ĢD���@?�{XF"?�`V;��?   ެ��>;iMࡾ}?   �%q=��doq�4�ĢD���@��{XF"���?�   �9̾p����v�   ,�B	��0�2?swj���>?�E���?�`V;��?   ެ��>;iMࡾ}?   �%q=���~�>Gy�{?E�(����>Z�IWq�>h�C���>׬+�ŽF?�r�����<�btKt��=Q�����>|��Pm�=vܩV�#�>U���32�=�B��܃>Уf#2�=��&�܃>��e�?��4>������k�@GD2@m2�+��?	   HBP_DR_BP      DRIF��kp?��c�G>�r0���=JK=SaU����L-������^�\�"��U���Gy�{?�E���ơ=V��zVx������AQ�`�(_�ct���n�F�����>?t�I�Hҽ)�6*KuM=��Gj����  bm�h�;Z�IWq�>ѲC�!A����^�=j9�?�~�x�j���>#"�Ҡ>�=Z���/n�;׬+�ŽF?��g�lP$<I�(*��<0��(n�>?ĢD���@?�s}���?�`V;��?   ��>;iMࡾ}?   B�=0��(n�>�ĢD���@��s}������?�   �#:̾p����v�   �HC	�q���<?swj���>?�̓@��?�`V;��?   ��>;iMࡾ}?   B�=��kp?Gy�{?F�����>Z�IWq�>x�j���>׬+�ŽF?I�(*��<�btKt��=Q�����>��Pm�=ީV�#�>P���32�=�B��܃>ˣf#2�=��&�܃>��;� @7eD�_�?{����@�g<Y`@:��|���?	   HBP_DR_BP      DRIF-��?��a�$><kP��:�=��15��罧�BS��Q��>�c�znk�o�Gy�{?�.A��=V��zVx��c
����AQ�`�(_��KU�Eo�|qPIS�>׃ܣ�ͽ��i�SH=�.p����{Yυ;Z�IWq�>D�=	A����^�=�C��~�ޢI���>؞S�?�=���+�n�;׬+�ŽF?�\�P$<LD����<l�q5LD?ĢD���@?x�I6�l?�`V;��?   ���>;iMࡾ}?   ���=l�q5LD�ĢD���@�x�I6�l���?�   �:̾p����v�   D�C	��/�0z�B?swj���>?�}��,?�`V;��?   ���>;iMࡾ}?   ���=-��?Gy�{?|qPIS�>Z�IWq�>ޢI���>׬+�ŽF?LD����<4ctKt��=������>\��Pm�=�ߩV�#�>N���32�=�B��܃>ɣf#2�=��&�܃>��kz�8@>��������k�@��7�FE�?��I�r�?	   HBP_DR_BP      DRIF���k@t?Sl
�t)>h)1?6��=ӯԬ�_���h�����O -�g�8Q�"��Gy�{?��;�x�=V��zVx���WTbE�AQ�`�(_�3���o���X����>.���rƽ+�L.2C=���e݆��Ύ�6�;Z�IWq�>�}��A����^�=�`�_��~�uwA��>��9p?�={�˛�o�;׬+�ŽF?��U�P$<�=5C���<@�ͳHI?ĢD���@?m��?�`V;��?   �0�>;iMࡾ}?   [�=@�ͳHI�ĢD���@�m�����?�   ��:̾p����v�   ��C	�����ZG?swj���>?�.1�^�?�`V;��?   �0�>;iMࡾ}?   [�=���k@t?Gy�{?��X����>Z�IWq�>uwA��>׬+�ŽF?�=5C���<�btKt��=������>���Pm�=֩V�#�>P���32�=�B��܃>ˣf#2�=��&�܃>�`-.�@�V̢����E��&z @v�%���?��R��� @	   HBP_DR_V1      DRIF�Q���z?�$��y->���򝒅=�1��������y���|=��k���	ia��Gy�{?���^���=V��zVx���Q���AQ�`�(_�>��Fp��Ɓ����>�ܭ��B���_��&f==���9���mO�}\z;Z�IWq�>��w�A����^�=+�oNɩ~��T �t��>�VM�?�=D�d�p�;׬+�ŽF??t�PQ$<�@Q�[��<\BL f�M?ĢD���@?x���?�`V;��?   ��C�>;iMࡾ}?   즽=\BL f�M�ĢD���@�x������?�   P6;̾p����v�   �;D	�o��imK?swj���>?.�g(�	?�`V;��?   ��C�>;iMࡾ}?   즽=�Q���z?Gy�{?�Ɓ����>Z�IWq�>�T �t��>׬+�ŽF?�@Q�[��<4ctKt��=������><��Pm�=��V�#�>O���32�=�B��܃>ʣf#2�=��&�܃>N�՞��%@W��^��tk+�?|�pA7;�?��R��� @   HBP_CNT0      DRIF�Q���z?�$��y->���򝒅=�1��������y���|=��k���	ia��Gy�{?���^���=V��zVx���Q���AQ�`�(_�>��Fp��Ɓ����>�ܭ��B���_��&f==���9���mO�}\z;Z�IWq�>��w�A����^�=+�oNɩ~��T �t��>�VM�?�=D�d�p�;׬+�ŽF??t�PQ$<�@Q�[��<\BL f�M?ĢD���@?x���?�`V;��?   ��C�>;iMࡾ}?   즽=\BL f�M�ĢD���@�x������?�   P6;̾p����v�   �;D	�o��imK?swj���>?.�g(�	?�`V;��?   ��C�>;iMࡾ}?   즽=�Q���z?Gy�{?�Ɓ����>Z�IWq�>�T �t��>׬+�ŽF?�@Q�[��<4ctKt��=������><��Pm�=��V�#�>O���32�=�B��܃>ʣf#2�=��&�܃>N�՞��%@W��^��tk+�?|�pA7;�?�����*@   HBP_DR_QD_ED      DRIFR��D�?v'5�V..>I����='�z.Ը��P�'�@���uv!D�l�$�3�o���Gy�{?x�#O��=V��zVx��hi��AQ�`�(_�cR��p������>+���������6�;=��������	g2�y;Z�IWq�>t*$�A����^�=�	wé~��؃���>~���?�=��5p�;׬+�ŽF?æt�Q$<�^�k��<VK+<\N?ĢD���@?\���#d?�`V;��?   �G�>;iMࡾ}?   �r�=VK+<\N�ĢD���@�\���#d���?�   �C;̾p����v�   �GD	�@q��L?swj���>?�H�8ݣ?�`V;��?   �G�>;iMࡾ}?   �r�=R��D�?Gy�{?�����>Z�IWq�>�؃���>׬+�ŽF?�^�k��<�btKt��=������>(��Pm�=��V�#�>P���32�=�B��܃>ˣf#2�=��&�܃>���@�&@���k~^�2ҹB���?@%7V�+�?�<�a�|@
   HBP_QD_V01      QUAD�8$RO?b��o�&>�A���_�=�,N�⣽��<)����[� ��l����L��H���� �>���y={�[�+���m�xۛ�Im=��pM�2Ż�U|ػ̦%��g�>C2;�"��dl����:=e�,���+��G�w;HIй��>����h:���!�pQ�=���/�w���p9���>f`%�?�=�G�(Bp�;׬+�ŽF?�w�Q$<1x!.s��<��_#�N?| ��s�/?g�+<#?(��7K?   ��H�>;iMࡾ}?   ��=��_#�N�| ��s�/�g�+<#�̐�Yd�   �K;̾p����v�   OD	�S�y�L?��}d-?��4S��?(��7K?   ��H�>;iMࡾ}?   ��=�8$RO?H���� �>̦%��g�>HIй��>��p9���>׬+�ŽF?1x!.s��<�Wi��=@>	���>ݸ��?�=2�<��(�>!���C2�=G�?��܃>p��032�=��H��܃>_��G:z'@y���[ee��?ln:�("�?�<�a�|@   HBP_COR      KICKER�8$RO?b��o�&>�A���_�=�,N�⣽��<)����[� ��l����L��H���� �>���y={�[�+���m�xۛ�Im=��pM�2Ż�U|ػ̦%��g�>C2;�"��dl����:=e�,���+��G�w;HIй��>����h:���!�pQ�=���/�w���p9���>f`%�?�=�G�(Bp�;׬+�ŽF?�w�Q$<1x!.s��<��_#�N?| ��s�/?g�+<#?(��7K?   ��H�>;iMࡾ}?   ��=��_#�N�| ��s�/�g�+<#�̐�Yd�   �K;̾p����v�   OD	�S�y�L?��}d-?��4S��?(��7K?   ��H�>;iMࡾ}?   ��=�8$RO?H���� �>̦%��g�>HIй��>��p9���>׬+�ŽF?1x!.s��<�Wi��=@>	���>ݸ��?�=2�<��(�>!���C2�=G�?��܃>p��032�=��H��܃>_��G:z'@y���[ee��?ln:�("�?�<�a�|@   HBP_BPM      MONI�8$RO?b��o�&>�A���_�=�,N�⣽��<)����[� ��l����L��H���� �>���y={�[�+���m�xۛ�Im=��pM�2Ż�U|ػ̦%��g�>C2;�"��dl����:=e�,���+��G�w;HIй��>����h:���!�pQ�=���/�w���p9���>f`%�?�=�G�(Bp�;׬+�ŽF?�w�Q$<1x!.s��<��_#�N?| ��s�/?g�+<#?(��7K?   ��H�>;iMࡾ}?   ��=��_#�N�| ��s�/�g�+<#�̐�Yd�   �K;̾p����v�   OD	�S�y�L?��}d-?��4S��?(��7K?   ��H�>;iMࡾ}?   ��=�8$RO?H���� �>̦%��g�>HIй��>��p9���>׬+�ŽF?1x!.s��<�Wi��=@>	���>ݸ��?�=2�<��(�>!���C2�=G�?��܃>p��032�=��H��܃>_��G:z'@y���[ee��?ln:�("�??� ��@
   HBP_QD_V01      QUAD�Q�h?�:(뽦���MW{=}�S0����H�H������[ضm�UH�h��f[�%�>'�ͅsQo=v��gKJ`��|���d==K+N.>.0\�x�;E�c�K�>o
W6��������(�9==#V;���>�M7Q)w;��kꃂ�>O3K�2�e�pnM�="MV8��p����+���>�����?�=��-fCp�;׬+�ŽF?���TQ$<���s��<� N�e�N?��]Ӿk�>-Ɵ�4?&:��?   ��H�>;iMࡾ}?   �%�=� N�e�N����	��-Ɵ�4�&:���   pM;̾p����v�   LPD	���P��L?��]Ӿk�>�?�̫�?*.Ô�t?   ��H�>;iMࡾ}?   �%�=�Q�h?f[�%�>E�c�K�>��kꃂ�>���+���>׬+�ŽF?���s��<y��p��=Rj�)��>��f��!�=�d�e0�>?�(]2�=��U�܃>���aL2�=��^�܃>j�z�u�'@����3��'�BJ5�?c�5�?�q$�%@   HBP_DR_QD_ED      DRIFa���=a?���뽄P���u='�3�C����q�Ԟ���۽�sm��d��b��f[�%�>��ʷ6�n=v��gKJ`�a���d==K+N.>�+�݄x�;�D�����>�|�(4��0�VAg9=�3��_��F��;�pv;��kꃂ�>3��I�2�e�pnM�='��]��p����ڏ��>ݪ�?�=�j�Bp�;׬+�ŽF?B��sQ$<�R�qs��<ܺ��4�N?��]Ӿk�>,=M]n`
?&:��?   P�H�>;iMࡾ}?   z(�=ܺ��4�N����	��,=M]n`
�&:���   �L;̾p����v�   �OD	�F���L?��]Ӿk�>{�,|�/?*.Ô�t?   P�H�>;iMࡾ}?   z(�=a���=a?f[�%�>�D�����>��kꃂ�>���ڏ��>׬+�ŽF?�R�qs��<{��p��=Uj�)��>��f��!�=�d�e0�>@�(]2�=��U�܃>���aL2�=��^�܃>���.�'@����Œ��"w|��?`�0�x�??� ��@	   HBP_DR_V2      DRIF�Q1��??>��p�����!��\����BY;���T��}���<Z�l����=E��f[�%�>K;ګ�>k=v��gKJ`�� ��d==K+N.>r<jx�;8{���>=���ƸC=>�;��5=fZ-��^ʇ���r;��kꃂ�>X�v�D�2�e�pnM�=�d�'��p��4rP���>�g ��?�=٪F�?p�;׬+�ŽF?����P$<�@�Nq��<��*}T�N?��]Ӿk�>�8�B�?&:��?   .�H�>;iMࡾ}?   *5�=��*}T�N����	���j|n�&:���   LJ;̾p����v�   �LD	�����tL?��]Ӿk�>�8�B�?*.Ô�t?   .�H�>;iMࡾ}?   *5�=�Q1��??f[�%�>8{���>��kꃂ�>�4rP���>׬+�ŽF?�@�Nq��<y��p��=Rj�)��>��f��!�=,�d�e0�>?�(]2�=��U�܃>���aL2�=��^�܃>�x���'@Ȏ�`�Y��	 �\�`�?=R����t�hQ���w@	   HBP_DR_V2      DRIFS�B�l?y��tn����T���걣nq��С�<L\����M�@�l���%d(��f[�%�>
�韊�g=v��gKJ`�����d==K+N.>�/�2Ox�;޿Ve���>,���p�=<+uH-1=	Pօᴭ�1�oC��n;��kꃂ�>�Mk$?�2�e�pnM�=�z���p�xI�Ō��>��c��?�=��5%=p�;׬+�ŽF?���
�P$<L|g+o��<�ƮQt�N?��]Ӿk�>ox���
?&:��?   I�>;iMࡾ}?   �A�=�ƮQt�N����	��������&:���   �G;̾p����v�   �ID	�2Hz�EL?��]Ӿk�>ox���
?*.Ô�t?   I�>;iMࡾ}?   �A�=S�B�l?f[�%�>޿Ve���>��kꃂ�>xI�Ō��>׬+�ŽF?L|g+o��<y��p��=Rj�)��>��f��!�=�d�e0�>?�(]2�=��U�܃>���aL2�=��^�܃>{=�Ei�'@r�-ѐP����u���?��L�ǿhQ���w@   HBP_CNT0      DRIFS�B�l?y��tn����T���걣nq��С�<L\����M�@�l���%d(��f[�%�>
�韊�g=v��gKJ`�����d==K+N.>�/�2Ox�;޿Ve���>,���p�=<+uH-1=	Pօᴭ�1�oC��n;��kꃂ�>�Mk$?�2�e�pnM�=�z���p�xI�Ō��>��c��?�=��5%=p�;׬+�ŽF?���
�P$<L|g+o��<�ƮQt�N?��]Ӿk�>ox���
?&:��?   I�>;iMࡾ}?   �A�=�ƮQt�N����	��������&:���   �G;̾p����v�   �ID	�2Hz�EL?��]Ӿk�>ox���
?*.Ô�t?   I�>;iMࡾ}?   �A�=S�B�l?f[�%�>޿Ve���>��kꃂ�>xI�Ō��>׬+�ŽF?L|g+o��<y��p��=Rj�)��>��f��!�=�d�e0�>?�(]2�=��U�܃>���aL2�=��^�܃>{=�Ei�'@r�-ѐP����u���?��L�ǿ?� ��@   HBP_DR_QD_ED      DRIF�Ӕ�?dC��^�D,
�/c��y�p�|�����{U��������l�x�Xn"��f[�%�>n���M,g=v��gKJ`��?n�d==K+N.>���Ix�;i9�7b"�>���8�=>v��_0=?��0�����`m;��kꃂ�>�1�>�2�e�pnM�=�ȡ��p�6*u���>TBoO�?�=Uu�<p�;׬+�ŽF?��)�P$<AxS�n��<�RC�N?��]Ӿk�>(2:��k
?&:��?   vI�>;iMࡾ}?   rD�=�RC�N����	��I5����&:���    G;̾p����v�   ID	��x�r�;L?��]Ӿk�>(2:��k
?*.Ô�t?   vI�>;iMࡾ}?   rD�=�Ӕ�?f[�%�>i9�7b"�>��kꃂ�>6*u���>׬+�ŽF?AxS�n��<x��p��=Qj�)��>��f��!�=�d�e0�>@�(]2�=��U�܃>���aL2�=��^�܃>��۝��'@��@�&��zZ �I�?���}σ̿��l� @
   HBP_QD_V01      QUADG�2Ml�?W1Y�	^!�7sUOD�����@���y]�(��8GC��pl�h������5�犐��>O�B�)Jy=��d#8��=݀+`T=�=�46��3R>�|s�%��;�X���v�>� ��j�=�����j/=Au�'�ެ��I��0l;�0�� "�>�n�Y�,���	�~=D�'��i����Ǎ��>��I��?�=>�/X>p�;׬+�ŽF?�P$<�#�o��<�\�G]N?���?��2?�Ʊ�I�
?�#E"�?   �[I�>;iMࡾ}?   ��=�\�G]N��Y��1�����2	��#E"��   LF;̾p����v�   ,HD	�W|�3�	L?���?��2?�Ʊ�I�
?�h,�3?   �[I�>;iMࡾ}?   ��=G�2Ml�?5�犐��>�X���v�>�0�� "�>���Ǎ��>׬+�ŽF?�#�o��<�]:��=����>Z�협0�=_�S�:�>p䣨�2�=e�2݃>޽���2�=G���'݃>U`�k�w'@Z��Dm
@��S���?=��Bƃ㿑�l� @   HBP_COR      KICKERG�2Ml�?W1Y�	^!�7sUOD�����@���y]�(��8GC��pl�h������5�犐��>O�B�)Jy=��d#8��=݀+`T=�=�46��3R>�|s�%��;�X���v�>� ��j�=�����j/=Au�'�ެ��I��0l;�0�� "�>�n�Y�,���	�~=D�'��i����Ǎ��>��I��?�=>�/X>p�;׬+�ŽF?�P$<�#�o��<�\�G]N?���?��2?�Ʊ�I�
?�#E"�?   �[I�>;iMࡾ}?   ��=�\�G]N��Y��1�����2	��#E"��   LF;̾p����v�   ,HD	�W|�3�	L?���?��2?�Ʊ�I�
?�h,�3?   �[I�>;iMࡾ}?   ��=G�2Ml�?5�犐��>�X���v�>�0�� "�>���Ǎ��>׬+�ŽF?�#�o��<�]:��=����>Z�협0�=_�S�:�>p䣨�2�=e�2݃>޽���2�=G���'݃>U`�k�w'@Z��Dm
@��S���?=��Bƃ㿑�l� @   HBP_BPM      MONIG�2Ml�?W1Y�	^!�7sUOD�����@���y]�(��8GC��pl�h������5�犐��>O�B�)Jy=��d#8��=݀+`T=�=�46��3R>�|s�%��;�X���v�>� ��j�=�����j/=Au�'�ެ��I��0l;�0�� "�>�n�Y�,���	�~=D�'��i����Ǎ��>��I��?�=>�/X>p�;׬+�ŽF?�P$<�#�o��<�\�G]N?���?��2?�Ʊ�I�
?�#E"�?   �[I�>;iMࡾ}?   ��=�\�G]N��Y��1�����2	��#E"��   LF;̾p����v�   ,HD	�W|�3�	L?���?��2?�Ʊ�I�
?�h,�3?   �[I�>;iMࡾ}?   ��=G�2Ml�?5�犐��>�X���v�>�0�� "�>���Ǎ��>׬+�ŽF?�#�o��<�]:��=����>Z�협0�=_�S�:�>p䣨�2�=e�2݃>޽���2�=G���'݃>U`�k�w'@Z��Dm
@��S���?=��Bƃ��e>�r@
   HBP_QD_V01      QUAD(�.d	�? ��%0����%n�����{���½�ƌ�����n���k�@=�Դ�����r�A(	?
��f�j�=D��'���=l���
�=R�M�'a>�˲�C�;G�de�>���KC�=i7t��r.=��k=�ᬽ��0�Rk;�ű���>�̵V��#��M�����	!r�S�a�������>:����?�=����Lp�;׬+�ŽF?�=��P$<����x��<sJ_ 4�M?:��B?�.j�ƙ?�CG��c
?   �bK�>;iMࡾ}?   �c�=sJ_ 4�M�X��^�@����-��	��CG��c
�    H;̾p����v�   �ID	�99`���K?:��B?�.j�ƙ?5���?   �bK�>;iMࡾ}?   �c�=(�.d	�?��r�A(	?G�de�>�ű���>������>׬+�ŽF?����x��<))=9�'�=d�H ǯ>����OB�=���W�E�>�mu�i3�=\�<�݃>���XY3�=�zI�݃>�;ӂ�&@�$���@�d����?�qB��e�	�.��@   HBP_DR_QD_ED      DRIF��J�U?Nxδ�t/�"OEr�卽.���j�������JT �L/k�x��nq�����r�A(	?�F1��=D��'���=��
�=R�M�'a>��rC�;�(\��>B�o |�=�n�:�-=.S
����W���j;�ű���>�&o��#��M�����*�)�a����v���>�3[O�?�=��vNgp�;׬+�ŽF?o���P$<���'���</w�M?:��B?u�֣܎?�CG��c
?   D�N�>;iMࡾ}?   ���=/w�M�X��^�@�&?&2�l
��CG��c
�   �L;̾p����v�   �MD	��LIL�J?:��B?u�֣܎?5���?   D�N�>;iMࡾ}?   ���=��J�U?��r�A(	?�(\��>�ű���>���v���>׬+�ŽF?���'���<))=9�'�=d�H ǯ>A���OB�=ݦ�W�E�>�mu�i3�=]�<�݃>���XY3�=�zI�݃>=�HW�%@@w>C�U@x�clL�?��9:nw�z)�@	   HBP_DR_V1      DRIFC^e��?��Y�kA*�S�����a{i�����4���s������f�<��:g򻇹r�A(	?"U&����=D��'���=`���$�=R�M�'a>�V+�6C�;B-���>��e�V��=��2�e(=w��S�:���'���e;�ű���>�6k�#��M�����k��	�a��i�(��>�d�QX@�=ی�Nq�;׬+�ŽF?Ee��P$<o������<jI*�$NH?:��B?����=?�CG��c
?   p:e�>;iMࡾ}?   d��=jI*�$NH�X��^�@�@������CG��c
�   Dk;̾p����v�   0hD	��d/jWF?:��B?����=?5���?   p:e�>;iMࡾ}?   d��=C^e��?��r�A(	?B-���>�ű���>�i�(��>׬+�ŽF?o������<(=9�'�=��H ǯ>ƣ��OB�= ��W�E�>�mu�i3�=\�<�݃>���XY3�=�zI�݃>/:���@�y�OQ@l��r� @��?�����}��qJ@	   HBP_DR_BP      DRIF/|�g��	?"7�}R$��8eb䘽��X~v��P;��J��S7��a�����,�컇�r�A(	?m���>�=D��'���=�Q���=R�M�'a>{�?dC�;[��j�>��v� ��=���Iu"=�M�
ߍ��X���`;�ű���>�ȯW��#��M�������:�ʽa�ea�ߠ��>�r��@�=�3���q�;׬+�ŽF?�w��4Q$<����c��<8}D�B?:��B?4���?�CG��c
?   ��~�>;iMࡾ}?   �U�=8}D�B�X��^�@��R������CG��c
�   �;̾p����v�   D�D	� jFp�AA?:��B?4���?5���?   ��~�>;iMࡾ}?   �U�=/|�g��	?��r�A(	?[��j�>�ű���>ea�ߠ��>׬+�ŽF?����c��<�'=9�'�=��H ǯ>Ӟ��OB�=ީ�W�E�>�mu�i3�=Y�<�݃>���XY3�=�zI�݃>�%� U`@ڠr�@ ����@D�0jt���F���װ@	   HBP_DR_BP      DRIF��4g?�(����'�'����PÕ���4�E����X�El]��s仇�r�A(	?qB�U���=D��'���=��:��=R�M�'a>�@2��C�;�Q*��.�>uD��|�=6!��=��app��p�%S�V;�ű���>�ZH�g�#��M�����@Ю[��a��z%��>i�$1A�=��P�r�;׬+�ŽF?�e��mQ$<�ϖm���<0M�o�:?:��B?\����?�CG��c
?   v���>;iMࡾ}?   �	=0M�o�:�X��^�@�'�B�'���CG��c
�   ��;̾p����v�   P�D	��޺��W8?:��B?\����?5���?   v���>;iMࡾ}?   �	=��4g?��r�A(	?�Q*��.�>�ű���>�z%��>׬+�ŽF?�ϖm���<�'=9�'�=��H ǯ>����OB�=��W�E�>�mu�i3�=X�<�݃>���XY3�=�zI�݃>�z� �k@Ps��/M@~ �u�@z
�w���V�'.�@	   HBP_DR_BP      DRIF�����>���uC���7�R���>^M!��P]�뛽�"/��L�����ػ��r�A(	?b�4�vҤ=D��'���=i$<�L�=R�M�'a>��v��C�;�9�23�>�̷��=�s��d
=sF�<����D��G;�ű���>�����#��M�������L�a�$�A����>>�H��A�=1��CDs�;׬+�ŽF?��֦Q$<��u�O��<^*FV�%0?:��B?��9 l?�CG��c
?   �a��>;iMࡾ}?   "�=^*FV�%0�X��^�@�rU�b�j��CG��c
�   ,�;̾p����v�   \�D	������W,?:��B?��9 l?5���?   �a��>;iMࡾ}?   "�=�����>��r�A(	?�9�23�>�ű���>$�A����>׬+�ŽF?��u�O��< (=9�'�=��H ǯ>H���OB�=���W�E�>�mu�i3�=c�<�݃>���XY3�=!�zI�݃>����z.�?#NnP�D�?�D���<@¨	�.]���Za�>@	   HBP_DR_BP      DRIF�p(����>8�z.�-����#��{��\:7vy�׹��b���}O��̳0�H��o�b����r�A(	?RZ�.���=D��'���=���}��=R�M�'a>c8�n�C�;o�x���>�kUؿ��=�A�%��<-�4rñ��b^��;�ű���>.y=d�#��M������`��a�OL`1��>�z�	B�=�r�t�;׬+�ŽF?1�A��Q$<c[&���<2��4O?:��B?~�.��?�CG��c
?   |���>;iMࡾ}?   b�6=2��4O�X��^�@�mW�����CG��c
�   ��;̾p����v�   l�D	����y�?:��B?~�.��?5���?   |���>;iMࡾ}?   b�6=�p(����>��r�A(	?o�x���>�ű���>OL`1��>׬+�ŽF?c[&���<�'=9�'�=��H ǯ>e���OB�=���W�E�>�mu�i3�=U�<�݃>���XY3�=�zI�݃>�P��?y�&��?��+�@-G��h#���Za�>@   FITT_HBP_PI      MARK�p(����>8�z.�-����#��{��\:7vy�׹��b���}O��̳0�H��o�b����r�A(	?RZ�.���=D��'���=���}��=R�M�'a>c8�n�C�;o�x���>�kUؿ��=�A�%��<-�4rñ��b^��;�ű���>.y=d�#��M������`��a�OL`1��>�z�	B�=�r�t�;׬+�ŽF?1�A��Q$<c[&���<2��4O?:��B?~�.��?�CG��c
?   |���>;iMࡾ}?   b�6=2��4O�X��^�@�mW�����CG��c
�   ��;̾p����v�   l�D	����y�?:��B?~�.��?5���?   |���>;iMࡾ}?   b�6=�p(����>��r�A(	?o�x���>�ű���>OL`1��>׬+�ŽF?c[&���<�'=9�'�=��H ǯ>e���OB�=���W�E�>�mu�i3�=U�<�݃>���XY3�=�zI�݃>�P��?y�&��?��+�@-G��h#���Za�>@   HBP_CNT0      DRIF�p(����>8�z.�-����#��{��\:7vy�׹��b���}O��̳0�H��o�b����r�A(	?RZ�.���=D��'���=���}��=R�M�'a>c8�n�C�;o�x���>�kUؿ��=�A�%��<-�4rñ��b^��;�ű���>.y=d�#��M������`��a�OL`1��>�z�	B�=�r�t�;׬+�ŽF?1�A��Q$<c[&���<2��4O?:��B?~�.��?�CG��c
?   |���>;iMࡾ}?   b�6=2��4O�X��^�@�mW�����CG��c
�   ��;̾p����v�   l�D	����y�?:��B?~�.��?5���?   |���>;iMࡾ}?   b�6=�p(����>��r�A(	?o�x���>�ű���>OL`1��>׬+�ŽF?c[&���<�'=9�'�=��H ǯ>e���OB�=���W�E�>�mu�i3�=U�<�݃>���XY3�=�zI�݃>�P��?y�&��?��+�@-G��h#���-�Ć@   HBP_DP_SEPM1   	   CSRCSBEND�ߛ�8s�>[	�ٓ��=���Cc��=�[��b^=(�� ��s=���h��/>�',Q�;��1��>��-�6.�=�,��=�_��T/�=/����O>,�S�9��;Q�FU��>����2j�=�+�@v��t
,��-�
o�"I������>eUU�=���)�.�v��XG�Q���O:ѹ>�<d���=焦�P��;U\�`)I?b�_9<v��z�<�l�M#?��Of!�4?t��#W!? ��ƃc
?   h`'�>#��L��}?   t��=�l�M#���Of!�4��jy�j� � ��ƃc
�   ��CȾ�p��cw�   �B��N���s?7��Gw*4?t��#W!?^��\�?   h`'�>#��L��}?   t��=�ߛ�8s�>��1��>Q�FU��>�����>��O:ѹ>U\�`)I?v��z�<A]n<���=����~�>�*��Uq�=����:��>Z�A�R3�=���n8܃>��t73�=���&܃>T�8����?��s�����c>�$@8����*�=� ��@   HBP_DP_SEPM1   	   CSRCSBEND�����>|d<t
�=�R/��x�=峡\Cv�=O�uc=p�����5>8���x�;�������>/ֹ=��=�[8 �=F��d���=����lC��(���;d������>G��-כ�=[0ރ�J�?4�
�ᵽ��I�⇻���X��>=T��m2�bۻ��Jm��+y �p�C4�O_�>T��.��r�{�w�;���٥PL?/Ӊ���)����)��<��6-�"?��t�$&?�����6#?����c
?   Mox�>�xv+F�}?   
|!=��6-�"���t�$&��	�M�"�����c
�   @�xɾa�#�dw�   ,�����(S?��E?�����6#?����i�?   Mox�>�xv+F�}?   
|!=�����>�������>d������>���X��>C4�O_�>���٥PL?���)��<�����=���]D�>P�����=���6��>/���[3�=|T�çڃ>�H�bD3�=^]�X�ڃ>ҁZ�y��?��S�����w�)@�7�8��=� ��@   HBP_CNT      CENTER�����>~d<t
�=�R/��x�=峡\Cv�=O�uc=p�����5>8���x�;�������>/ֹ=��=�[8 �=F��d���=����lC��(���;d������>G��-כ�=[0ރ�J�=4�
�ᵽ��I�⇻���X��>=T��m2��ۻ��Jm��+y �p�C4�O_�>T��.��r�{�w�;���٥PL?/Ӊ���)����)��<���ñY"?�קzA(&?!����6#?3�qc
?   Mox�>�xv+F�}?   
|!=���ñY"��קzA(&��$�7�"�3�qc
�   @�xɾa�#�dw�   ,���A}n���?���ܣ>?!����6#?�7��?   Mox�>�xv+F�}?   
|!=�����>�������>d������>���X��>C4�O_�>���٥PL?���)��<
�����=���]D�>E�����=���6��>/���[3�=|T�çڃ>�H�bD3�=^]�X�ڃ>�Z�y��? ��S�����w�)@�7�8��=� ��@
   HBP_WA_OU1      WATCH�����>~d<t
�=�R/��x�=峡\Cv�=O�uc=p�����5>8���x�;�������>/ֹ=��=�[8 �=F��d���=����lC��(���;d������>G��-כ�=[0ރ�J�=4�
�ᵽ��I�⇻���X��>=T��m2��ۻ��Jm��+y �p�C4�O_�>T��.��r�{�w�;���٥PL?/Ӊ���)����)��<���ñY"?�קzA(&?!����6#?3�qc
?   Mox�>�xv+F�}?   
|!=���ñY"��קzA(&��$�7�"�3�qc
�   @�xɾa�#�dw�   ,���A}n���?���ܣ>?!����6#?�7��?   Mox�>�xv+F�}?   
|!=�����>�������>d������>���X��>C4�O_�>���٥PL?���)��<
�����=���]D�>E�����=���6��>/���[3�=|T�çڃ>�H�bD3�=^]�X�ڃ>�Z�y��? ��S�����w�)@�7�8��