set TIME_start [clock clicks -milliseconds] 

if {[file exist outfiles]} {
	exec rm -rf outfiles
	exec mkdir -p outfiles
} else {
	exec mkdir -p outfiles
}

set  indx 0 
cd   outfiles
set  script_dir "../files"
set  script_dir2 "../../common_files"



source $script_dir2/structure_pars.tcl
source $script_dir2/srw_calculation.tcl
source ../lrr_ln0_bc1_ln1_bc2_ln2_ln3_hxspr_hxr_mine_pars.tcl



source $script_dir2/create_bunch.tcl
source $script_dir2/match_aug_19.tcl
source $script_dir2/modify_output.tcl

set  track_ln0 1
set  track_bc1 1
set  track_bc1_dia 1
set  track_ln1 1
set  track_bc2 1
set  track_ln2 1
set  track_ln2_dia 1
set  track_sxrbpl 1
set  track_ln3 1
set  track_hxrbpl 1
set  track_hbp1 1
set  track_hbp2 1
set  track_hxrl 1

set  track_all 1




set e0 [expr (sqrt($p0*$p0 + 1.0)-1)*$mc2]

puts "\nreference momentum $p0"
puts "reference energy $e0\n"



# exit

after 100

# the bunch is created with defined parameters
# create_bunch BeamDefine
exec cp $script_dir2/$indist_read .
exec cp $script_dir2/$indist_s2e .


source $script_dir/create_beamline.tcl
source $script_dir/create_elegant_file.tcl


set ncpu [expr int([exec nproc]-8)]
puts "number of cores to be used in calculation $ncpu"

puts $watches
puts $watches2



foreach flem $elegant_match flet $elegant_track {
    
    puts $flem
    puts $flet
    exec elegant $flem >/dev/tty
    after 1000
#      exec elegant $flet >/dev/tty
    exec mpirun -host localhost --oversubscribe  -np $ncpu Pelegant $flet >/dev/tty
    after 1000

}

print_ellapsed_time $TIME_start

set watches [lreplace $watches 0 0 ]
foreach fl $watches {
        puts $fl
#         plot_long_space $fl
        exec rm $fl
#         calc_slice $fl 31 10
#         set flname [file rootname $fl]
#         plot_slice $flname.slc.sdds 
}


# foreach fl $twissfiles {
#     
# #     plottwiss $fl
#     plotr56 $fl
# }
# 
# foreach fl $sigmafiles {
# #     plotsigma $fl
#     plottwiss2 $fl
# }



after 100



if {[info exists track_all] } {
    exec mpiexec -host localhost --oversubscribe  -np $ncpu Pelegant $elegant_track_all >&@stdout
#      exec elegant $filename.trck.ele >&@stdout
    
    foreach fl $watches2 {
        puts $fl
        plot_long_space $fl 1
#         calc_slice $fl 31 10
#         set flname [file rootname $fl]
#         plot_slice $flname.slc.sdds 1

    }
    
    if {[file exist ../eleouts]} {
        exec rm -rf ../eleouts
        exec mkdir -p ../eleouts
    } else {
        exec mkdir -p ../eleouts
    }
    set placetnames ""
    foreach fl $watches2 {
        set flname [file rootname [file tail $fl]]
        lappend placetnames '$flname.dat', 
        exec elegant2placet.tcl $fl ../eleouts/$flname.dat >&@stdout
        exec gzip ../eleouts/$flname.dat
    }
     
    plottwiss $twissall
# 
    plottwiss2 $sigmaall
    

   
}
		
print_ellapsed_time $TIME_start
exit


