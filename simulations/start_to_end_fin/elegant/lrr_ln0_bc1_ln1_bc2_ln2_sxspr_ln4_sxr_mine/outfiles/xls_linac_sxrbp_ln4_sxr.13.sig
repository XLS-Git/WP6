SDDS1
!# little-endian
&description text="sigma matrix--input: xls_linac_sxrbp_ln4_sxr.track.ele  lattice: xls_linac_sxrbp_ln4.13.lte", contents="sigma matrix", &end
&parameter name=Step, description="Simulation step", type=long, &end
&parameter name=SVNVersion, description="SVN version number", type=string, fixed_value=unknown, &end
&column name=s, units=m, description=Distance, type=double,  &end
&column name=ElementName, description="Element name", format_string=%10s, type=string,  &end
&column name=ElementOccurence, description="Occurence of element", format_string=%6ld, type=long,  &end
&column name=ElementType, description="Element-type name", format_string=%10s, type=string,  &end
&column name=s1, symbol="$gs$r$b1$n", units=m, description="sqrt(<x*x>)", type=double,  &end
&column name=s12, symbol="$gs$r$b12$n", units=m, description="<x*xp'>", type=double,  &end
&column name=s13, symbol="$gs$r$b13$n", units="m$a2$n", description="<x*y>", type=double,  &end
&column name=s14, symbol="$gs$r$b14$n", units=m, description="<x*y'>", type=double,  &end
&column name=s15, symbol="$gs$r$b15$n", units="m$a2$n", description="<x*s>", type=double,  &end
&column name=s16, symbol="$gs$r$b16$n", units=m, description="<x*delta>", type=double,  &end
&column name=s17, symbol="$gs$r$b17$n", units="m*s", description="<x*t>", type=double,  &end
&column name=s2, symbol="$gs$r$b2$n", description="sqrt(<x'*x'>)", type=double,  &end
&column name=s23, symbol="$gs$r$b23$n", units=m, description="<x'*y>", type=double,  &end
&column name=s24, symbol="$gs$r$b24$n", description="<x'*y'>", type=double,  &end
&column name=s25, symbol="$gs$r$b25$n", units=m, description="<x'*s>", type=double,  &end
&column name=s26, symbol="$gs$r$b26$n", description="<x'*delta>", type=double,  &end
&column name=s27, symbol="$gs$r$b27$n", units=s, description="<x'*t>", type=double,  &end
&column name=s3, symbol="$gs$r$b3$n", units=m, description="sqrt(<y*y>)", type=double,  &end
&column name=s34, symbol="$gs$r$b34$n", units=m, description="<y*y'>", type=double,  &end
&column name=s35, symbol="$gs$r$b35$n", units="m$a2$n", description="<y*s>", type=double,  &end
&column name=s36, symbol="$gs$r$b36$n", units=m, description="<y*delta>", type=double,  &end
&column name=s37, symbol="$gs$r$b37$n", units="m*s", description="<y*t>", type=double,  &end
&column name=s4, symbol="$gs$r$b4$n", description="sqrt(<y'*y')>", type=double,  &end
&column name=s45, symbol="$gs$r$b45$n", units=m, description="<y'*s>", type=double,  &end
&column name=s46, symbol="$gs$r$b46$n", description="<y'*delta>", type=double,  &end
&column name=s47, symbol="$gs$r$b47$n", units=s, description="<y'*t>", type=double,  &end
&column name=s5, symbol="$gs$r$b5$n", units=m, description="sqrt(<s*s>)", type=double,  &end
&column name=s56, symbol="$gs$r$b56$n", units=m, description="<s*delta>", type=double,  &end
&column name=s57, symbol="$gs$r$b57$n", units="m*s", description="<s*t>", type=double,  &end
&column name=s6, symbol="$gs$r$b6$n", description="sqrt(<delta*delta>)", type=double,  &end
&column name=s67, symbol="$gs$r$b67$n", units=s, description="<delta*t>", type=double,  &end
&column name=s7, symbol="$gs$r$b7$n", description="sqrt(<t*t>)", type=double,  &end
&column name=ma1, symbol="max$sb$ex$sb$e", units=m, description="maximum absolute value of x", type=double,  &end
&column name=ma2, symbol="max$sb$ex'$sb$e", description="maximum absolute value of x'", type=double,  &end
&column name=ma3, symbol="max$sb$ey$sb$e", units=m, description="maximum absolute value of y", type=double,  &end
&column name=ma4, symbol="max$sb$ey'$sb$e", description="maximum absolute value of y'", type=double,  &end
&column name=ma5, symbol="max$sb$e$gD$rs$sb$e", units=m, description="maximum absolute deviation of s", type=double,  &end
&column name=ma6, symbol="max$sb$e$gd$r$sb$e", description="maximum absolute value of delta", type=double,  &end
&column name=ma7, symbol="max$sb$e$gD$rt$sb$e", units=s, description="maximum absolute deviation of t", type=double,  &end
&column name=minimum1, symbol="x$bmin$n", units=m, type=double,  &end
&column name=minimum2, symbol="x'$bmin$n", units=m, type=double,  &end
&column name=minimum3, symbol="y$bmin$n", units=m, type=double,  &end
&column name=minimum4, symbol="y'$bmin$n", units=m, type=double,  &end
&column name=minimum5, symbol="$gD$rs$bmin$n", units=m, type=double,  &end
&column name=minimum6, symbol="$gd$r$bmin$n", units=m, type=double,  &end
&column name=minimum7, symbol="t$bmin$n", units=s, type=double,  &end
&column name=maximum1, symbol="x$bmax$n", units=m, type=double,  &end
&column name=maximum2, symbol="x'$bmax$n", units=m, type=double,  &end
&column name=maximum3, symbol="y$bmax$n", units=m, type=double,  &end
&column name=maximum4, symbol="y'$bmax$n", units=m, type=double,  &end
&column name=maximum5, symbol="$gD$rs$bmax$n", units=m, type=double,  &end
&column name=maximum6, symbol="$gd$r$bmax$n", units=m, type=double,  &end
&column name=maximum7, symbol="t$bmax$n", units=s, type=double,  &end
&column name=Sx, symbol="$gs$r$bx$n", units=m, description=sqrt(<(x-<x>)^2>), type=double,  &end
&column name=Sxp, symbol="$gs$r$bx'$n", description=sqrt(<(x'-<x'>)^2>), type=double,  &end
&column name=Sy, symbol="$gs$r$by$n", units=m, description=sqrt(<(y-<y>)^2>), type=double,  &end
&column name=Syp, symbol="$gs$r$by'$n", description=sqrt(<(y'-<y'>)^2>), type=double,  &end
&column name=Ss, symbol="$gs$r$bs$n", units=m, description=sqrt(<(s-<s>)^2>), type=double,  &end
&column name=Sdelta, symbol="$gs$bd$n$r", description=sqrt(<(delta-<delta>)^2>), type=double,  &end
&column name=St, symbol="$gs$r$bt$n", units=s, description=sqrt(<(t-<t>)^2>), type=double,  &end
&column name=ex, symbol="$ge$r$bx$n", units=m, description="geometric horizontal emittance", type=double,  &end
&column name=enx, symbol="$ge$r$bx,n$n", units=m, description="normalized horizontal emittance", type=double,  &end
&column name=ecx, symbol="$ge$r$bx,c$n", units=m, description="geometric horizontal emittance less dispersive contributions", type=double,  &end
&column name=ecnx, symbol="$ge$r$bx,cn$n", units=m, description="normalized horizontal emittance less dispersive contributions", type=double,  &end
&column name=ey, symbol="$ge$r$by$n", units=m, description="geometric vertical emittance", type=double,  &end
&column name=eny, symbol="$ge$r$by,n$n", units=m, description="normalized vertical emittance", type=double,  &end
&column name=ecy, symbol="$ge$r$by,c$n", units=m, description="geometric vertical emittance less dispersive contributions", type=double,  &end
&column name=ecny, symbol="$ge$r$by,cn$n", units=m, description="normalized vertical emittance less dispersive contributions", type=double,  &end
&column name=betaxBeam, symbol="$gb$r$bx,beam$n", units=m, description="betax for the beam, excluding dispersive contributions", type=double,  &end
&column name=alphaxBeam, symbol="$ga$r$bx,beam$n", description="alphax for the beam, excluding dispersive contributions", type=double,  &end
&column name=betayBeam, symbol="$gb$r$by,beam$n", units=m, description="betay for the beam, excluding dispersive contributions", type=double,  &end
&column name=alphayBeam, symbol="$ga$r$by,beam$n", description="alphay for the beam, excluding dispersive contributions", type=double,  &end
&data mode=binary, &end
/                 _BEG_      MARK�ɮ�?����=��r����v�=H�Z�C��=M��+��Ƚ�{R�Mu������_�����>�lBc�!���� Re�y�\�(��=4:-��H>E*s��;F$P���>XũE��=�=߈h���ǖ^�8��=�����&@�ze��Ny�>|�}XD 3=��^��=(U�[q p;�k��0�>��d&M>�S&ӯ�;��I�m?_$sv���<��b�]=��/߆F?�k�v?�97�� ?p7F3�?   (���>7y�,.�?   �j0#=FL���>��k�v��97�� ��o����   � ־�|@�#��   ��)���/߆F?��s�?m�p��$ ?p7F3�?   (���>7y�,.�?   �j0#=�ɮ�?_�����>F$P���>ze��Ny�>�k��0�>��I�m?��b�]=��R/��=�б�*L�>�/��+��=��?d�>�]�N3^�=��~5��>�B�(^�=oP�)��>��ߐ!�7@�'�#�@$�{z@Z�$�߿           BNCH      CHARGE�ɮ�?����=��r����v�=H�Z�C��=M��+��Ƚ�{R�Mu������_�����>�lBc�!���� Re�y�\�(��=4:-��H>E*s��;F$P���>XũE��=�=߈h���ǖ^�8��=�����&@�ze��Ny�>|�}XD 3=��^��=(U�[q p;�k��0�>��d&M>�S&ӯ�;��I�m?_$sv���<��b�]=��/߆F?�k�v?�97�� ?p7F3�?   (���>7y�,.�?   �j0#=FL���>��k�v��97�� ��o����   � ־�|@�#��   ��)���/߆F?��s�?m�p��$ ?p7F3�?   (���>7y�,.�?   �j0#=�ɮ�?_�����>F$P���>ze��Ny�>�k��0�>��I�m?��b�]=��R/��=�б�*L�>�/��+��=��?d�>�]�N3^�=��~5��>�B�(^�=oP�)��>��ߐ!�7@�'�#�@$�{z@Z�$�߿{�����?	   HXR_DR_V1      DRIFgQ�_T�?� �i�<�{�h�=��J�K<�=�h�wȽ��6U�t��t�U[1�_�����>y��G|i���� Re�y���N�0��=4:-��H>̣���;���>�o�2S��=��O�<�@h	���=�bJn�2;ze��Ny�>Ѽ�pH 3=��^��=�]��C p;"�P�0�>Bv�!&M>��rȄ��;��I�m?���K��<{��o]=vD���F?�k�v?�i��6W ?p7F3�?   v���>7y�,.�?   �,0#=���'v;>��k�v��i��6W ��o����   �� ־�|@�#��   4�)�vD���F?��s�?�s��N�?p7F3�?   v���>7y�,.�?   �,0#=gQ�_T�?_�����>���>ze��Ny�>"�P�0�>��I�m?{��o]=��R/��=�б�*L�>�/��+��=��?d�>�]�N3^�=��~5��>�B�(^�=oP�)��>���u�6@���@�O���@��Lo]����� �?   HXR_DR_QD_ED      DRIFRkjy�?r�kh���f�ܓf�=B�r2�&�=� >���ǽY�̟�t�N�����_�����>��J������ Re�y���N�2��=4:-��H>�*�р�;9��$7�>�iR�L}�=kD�sv�=���w�m�=�t9�@;ze��Ny�>\��I 3=��^��="C�7 p;"t�.�0�>��~k&M>/�Khp��;��I�m?��9
8��<���[]=1�Ti F?�k�v?}b��D ?p7F3�?   H���>7y�,.�?   �0#=%1	��>��k�v�}b��D ��o����     ־�|@�#��   ��)�1�Ti F?��s�?��~zN?p7F3�?   H���>7y�,.�?   �0#=Rkjy�?_�����>9��$7�>ze��Ny�>"t�.�0�>��I�m?���[]=��R/��=�б�*L�>|/��+��=��?d�>�]�N3^�=��~5��>�B�(^�=oP�)��>��/�Yb6@��E7@U��0t@�����gMx����?
   HXR_QD_V01      QUADb�Ug�v?�&�=Kw�Y]�i�=1���S�=�t�v�ǽ���5xt���%�����֡��>'Q�i����_���������Р��=�G=�ZW>���]��;�ӫ��B�>�nR����=�:\��=��0�U��=E5`V��A;4V�h��>���j3=K,�S{�=(��Kp;01mQ�0�>�_Z�&M>Ǌ��k��;��I�m?�1QJ3��<�9��V]=�7DK�E?�]`%y5$?�ǰ��C ?�F��?   l���>7y�,.�?   �0#=��9>��]`%y5$��ǰ��C ��F���   8 ־�|@�#��   ��)��7DK�E?��[�n�?�Շ8�H?�f��3?   l���>7y�,.�?   �0#=b�Ug�v?��֡��>�ӫ��B�>4V�h��>01mQ�0�>��I�m?�9��V]=�����=`���Y�>K����=/[�tpu�>�Q&;`�=<F��>�%��/`�=����:��>�a�;6@��㫭�@�X/|%@��v����gMx����?   HXR_COR      KICKERb�Ug�v?�&�=Kw�Y]�i�=1���S�=�t�v�ǽ���5xt���%�����֡��>'Q�i����_���������Р��=�G=�ZW>���]��;�ӫ��B�>�nR����=�:\��=��0�U��=E5`V��A;4V�h��>���j3=K,�S{�=(��Kp;01mQ�0�>�_Z�&M>Ǌ��k��;��I�m?�1QJ3��<�9��V]=�7DK�E?�]`%y5$?�ǰ��C ?�F��?   l���>7y�,.�?   �0#=��9>��]`%y5$��ǰ��C ��F���   8 ־�|@�#��   ��)��7DK�E?��[�n�?�Շ8�H?�f��3?   l���>7y�,.�?   �0#=b�Ug�v?��֡��>�ӫ��B�>4V�h��>01mQ�0�>��I�m?�9��V]=�����=`���Y�>K����=/[�tpu�>�Q&;`�=<F��>�%��/`�=����:��>�a�;6@��㫭�@�X/|%@��v����gMx����?   HXR_BPM      MONIb�Ug�v?�&�=Kw�Y]�i�=1���S�=�t�v�ǽ���5xt���%�����֡��>'Q�i����_���������Р��=�G=�ZW>���]��;�ӫ��B�>�nR����=�:\��=��0�U��=E5`V��A;4V�h��>���j3=K,�S{�=(��Kp;01mQ�0�>�_Z�&M>Ǌ��k��;��I�m?�1QJ3��<�9��V]=�7DK�E?�]`%y5$?�ǰ��C ?�F��?   l���>7y�,.�?   �0#=��9>��]`%y5$��ǰ��C ��F���   8 ־�|@�#��   ��)��7DK�E?��[�n�?�Շ8�H?�f��3?   l���>7y�,.�?   �0#=b�Ug�v?��֡��>�ӫ��B�>4V�h��>01mQ�0�>��I�m?�9��V]=�����=`���Y�>K����=/[�tpu�>�Q&;`�=<F��>�%��/`�=����:��>�a�;6@��㫭�@�X/|%@��v����	�.����?
   HXR_QD_V01      QUAD��C �c?M��m��������=ХB_9�=�X�l�ǽ�$R3`t�)��3���59��'��>�F�=ٷ��$�:��r_z��=i�}I-a>��J���;���V�>�kbٞ��=£�2=gyxr�k�=|�
�C;�h�z}�>�}_��3=��+�sr�=|��1��p;��_Ɍ0�>݂��&M>��h��;��I�m?%�/��<E\%�R]=�y�J��E?�T�9^0?$E�Y�I ?T�b�7�
?   ~���>7y�,.�?   �0#=��l�=��T�9^0�$E�Y�I �T�b�7�
�   d ־�|@�#��   l�)��y�J��E?���o%?�����P?vY&���?   ~���>7y�,.�?   �0#=��C �c?59��'��>���V�>�h�z}�>��_Ɍ0�>��I�m?E\%�R]=~�I����=n�pi�>�M#�	�=���Y��>�6VUb�=�VB�i��>�]fJb�=�|S^��>���� 6@9�U�n\ @�
O�J@���ʾ�������?   HXR_DR_QD_ED      DRIF��N%�?e��
-��F�J��=*� a��=A<B�% ǽ�ے���s�`�0B�r�59��'��>!�*�9����$�:�e�S���=i�}I-a>������;M~��ɶ�>��E˽��=>���=PН�;��=g��j�kJ;�h�z}�>���d��3=��+�sr�=��*͗�p;��ۓ�0�> ߰�&M>JH]��;��I�m?�����<��C]=���f�uE?�T�9^0?�%-+	r ?T�b�7�
?   ����>7y�,.�?   0#=���.p\=��T�9^0��%-+	r �T�b�7�
�   X־�|@�#��   v�)����f�uE?���o%?�t!ю?vY&���?   ����>7y�,.�?   0#=��N%�?59��'��>M~��ɶ�>�h�z}�>��ۓ�0�>��I�m?��C]=]�I����=L�pi�>�M#�	�=y��Y��>�6VUb�=�VB�i��>�]fJb�=�|S^��>�,���(5@�x6>
 @ o��@�e������Rg���@	   HXR_DR_V2      DRIF��c�U��>TSz�D������,̔=���r�=t��6��=a���>>?�u%�S�;59��'��>B���ν�$�:��E_�c��=i�}I-a>�'�O���;P@<�#8?�Z�5zD�=Qp`�xK=����=��=�@��KƆ;�h�z}�>�����3=��+�sr�=q�J��p;D�l�1�>?_%� 'M>@a�B��;��I�m?Kr~z8��<�E� MZ=l�ka�?�T�9^0?���p-?T�b�7�
?   �T��>7y�,.�?   �7.#=@WQC(���T�9^0����p-�T�b�7�
�   �/־�|@�#��   p(�l�ka�?���o%?�k���,?vY&���?   �T��>7y�,.�?   �7.#=��c�U��>59��'��>P@<�#8?�h�z}�>D�l�1�>��I�m?�E� MZ=��I����=z�pi�>�M#�	�=���Y��>�6VUb�=�VB�i��>�]fJb�=�|S^��>�����h�?'�
��#�?Tz_�4@3�_����uB�_@   HXR_DR_QD_ED      DRIF�\����>�5��b�=h�&qq>�=(���%Ex=����=������B>j}����;59��'��>�	��cν�$�:�]vl�r��=i�}I-a>;
b$Յ�;�Ne}�m?���	}�=�U��J�K=�x�-��=U�>}5�;�h�z}�>� )��3=��+�sr�=|f���p;B�1�>c�#�%'M>0r��7��;��I�m?f�wA)��<�5�=Z=�v�Z�?�T�9^0?Y���l�-?T�b�7�
?   �[��>7y�,.�?   !..#=p��Z���T�9^0�Y���l�-�T�b�7�
�   �0־�|@�#��   (j(��v�Z�?���o%?I��I�,?vY&���?   �[��>7y�,.�?   !..#=�\����>59��'��>�Ne}�m?�h�z}�>B�1�>��I�m?�5�=Z=��I����=z�pi�>�M#�	�=���Y��>�6VUb�=�VB�i��>�]fJb�=�|S^��>�� =���?nJmA��?�6��4@ų�!��m>���y@
   HXR_QD_V02      QUAD��dx��>��"+� �=iG��$v=ֻ%:gsj=*�?l
�=���ڍC>^м"a��;��b�>���	�ν��B�
�=�pC`y\�=H�����b>ĬBZ���;��+V�n?��������s��L=��ތ��=ʊ;B�;�-��b�>kM׮����ʓ6���L#@�G)�?F�q1�>"�''M>�ʊ�4��;��I�m?"�g%��<�� �9Z=-h~rI?{p��G/?<��	�-?\@�a?   �\��>7y�,.�?   2+.#=�d?�֢�{p��G/�<��	�-� ���2��   �0־�|@�#��   �h(�-h~rI?}��P$?~��i��,?\@�a?   �\��>7y�,.�?   2+.#=��dx��>��b�>��+V�n?�-��b�>?F�q1�>��I�m?�� �9Z=���"O��=`b�i�>���;�=��x���>��vq�;�=��U'I��>���P�;�=���=��>R�'o��?|��
¿(6Mm��4@ϙ̂ɦ@m>���y@   HXR_COR      KICKER��dx��>��"+� �=iG��$v=ֻ%:gsj=*�?l
�=���ڍC>^м"a��;��b�>���	�ν��B�
�=�pC`y\�=H�����b>ĬBZ���;��+V�n?��������s��L=��ތ��=ʊ;B�;�-��b�>kM׮����ʓ6���L#@�G)�?F�q1�>"�''M>�ʊ�4��;��I�m?"�g%��<�� �9Z=-h~rI?{p��G/?<��	�-?\@�a?   �\��>7y�,.�?   2+.#=�d?�֢�{p��G/�<��	�-� ���2��   �0־�|@�#��   �h(�-h~rI?}��P$?~��i��,?\@�a?   �\��>7y�,.�?   2+.#=��dx��>��b�>��+V�n?�-��b�>?F�q1�>��I�m?�� �9Z=���"O��=`b�i�>���;�=��x���>��vq�;�=��U'I��>���P�;�=���=��>R�'o��?|��
¿(6Mm��4@ϙ̂ɦ@m>���y@   HXR_BPM      MONI��dx��>��"+� �=iG��$v=ֻ%:gsj=*�?l
�=���ڍC>^м"a��;��b�>���	�ν��B�
�=�pC`y\�=H�����b>ĬBZ���;��+V�n?��������s��L=��ތ��=ʊ;B�;�-��b�>kM׮����ʓ6���L#@�G)�?F�q1�>"�''M>�ʊ�4��;��I�m?"�g%��<�� �9Z=-h~rI?{p��G/?<��	�-?\@�a?   �\��>7y�,.�?   2+.#=�d?�֢�{p��G/�<��	�-� ���2��   �0־�|@�#��   �h(�-h~rI?}��P$?~��i��,?\@�a?   �\��>7y�,.�?   2+.#=��dx��>��b�>��+V�n?�-��b�>?F�q1�>��I�m?�� �9Z=���"O��=`b�i�>���;�=��x���>��vq�;�=��U'I��>���P�;�=���=��>R�'o��?|��
¿(6Mm��4@ϙ̂ɦ@!0qF�@
   HXR_QD_V02      QUADM.oŧ�>K=� ��=˴o+��c=8u1g=DS�T�=���>�D>��zZ��;z�Q��g�>;�x<�ͽ�,$���=���(F0�= �F�d>��'�;ߪ���W?�8<��X	�6�����K=�F��
�=u�q|2�;7�t���>ۉ,�6��7���۽��2r��w>1�>�p`h('M>���1��;��I�m?k��!��<P��5Z=�N�/u�?i5�V�.?�n���-?�D4�� ?   2]��>7y�,.�?   a'.#=�y����i5�V�.��n���-�/�ྏ�   �1־�|@�#��   lg(��N�/u�?�h��M#?~��$�,?�D4�� ?   2]��>7y�,.�?   a'.#=M.oŧ�>z�Q��g�>ߪ���W?7�t���>�w>1�>��I�m?P��5Z=kڦ����=�L}j�>6�e?��=c�胋�>���^o�=X],pCm�>���fd�=ν�F8m�>�Sn�3�?��]�B�ӿ�4�&�4@�RBI�e&@�)k��@   HXR_DR_QD_ED      DRIF�8��s�>�߁�:�=���q�ق�h��o��=:���Vc�=fBK��H>��hSy�;z�Q��g�>)ŵָ�̽�,$���=����[0�= �F�d>�.> �;��8 o�?��C���?`gK=���R�\�=�رf���;7�t���>���|�6��7���۽�*[ z2r��#��1�>�b��-'M>��&��;��I�m?�����<p3C&Z=c8��T�?i5�V�.?ni��ƿ,?�D4�� ?   �\��>7y�,.�?   �.#=���زH�i5�V�.�ni��ƿ,�/�ྏ�   p5־�|@�#��    d(�c8��T�?�h��M#?J5����+?�D4�� ?   �\��>7y�,.�?   �.#=�8��s�>z�Q��g�>��8 o�?7�t���>�#��1�>��I�m?p3C&Z=kڦ����=�L}j�>6�e?��=c�胋�>9��^o�=�\,pCm�>!��fd�=[��F8m�>��4J���?9�%��޿���8�3@H�N��%@ʘ����@	   HXR_DR_V3      DRIF�N�5��>ːP���>�����$���P�f�=�����=AK�쐱e>��;�1�;z�Q��g�>Tg=e5-���,$���=h���1�= �F�d>,��̂�;���]�>\�d����s-�1C=���>��=�����;7�t���>�s@�k$6��7���۽T�h[V8r��=oG1�>k��{'M>�<p���;��I�m?�2�;��<���HY=u4Meќ&?i5�V�.?|���@� ?�D4�� ?   S��>7y�,.�?   �)-#=u4Meќ&�i5�V�.�|���@� �/�ྏ�   �m־�|@�#��   5(���M�@k?�h��M#?guf	rd ?�D4�� ?   S��>7y�,.�?   �)-#=�N�5��>z�Q��g�>���]�>7�t���>�=oG1�>��I�m?���HY=mڦ����=�L}j�>:�e?��=c�胋�>���^o�=.],pCm�>h��fd�=���F8m�>���O�@@y6��ss�������@ԏ2L�S@��ʻ=\@   HXR_DR_QD_ED      DRIF�F�U1��>��[9
>�@n�����=^�m��=��5G	�=yWj�f>{���-J�;z�Q��g�>��ñZ���,$���=<��ݩ1�= �F�d>2�X�x�;`ꓓ���>ё([�8��/�m�؞B=W��ET�=9ܯ���~;7�t���>6��~�$6��7���۽�!#L�8r�u�.�I1�>"��U�'M>����;��I�m?X��,��<��+9Y=t����3(?i5�V�.?$����?�D4�� ?   lR��>7y�,.�?   -#=t����3(�i5�V�.�$�����/�ྏ�   �q־�|@�#��   �1(�S�lwu5 ?�h��M#?f�y"�+?�D4�� ?   lR��>7y�,.�?   -#=�F�U1��>z�Q��g�>`ꓓ���>7�t���>u�.�I1�>��I�m?��+9Y=uڦ����=�L}j�>?�e?��=c�胋�>���^o�=J],pCm�>���fd�=���F8m�>�(���	@��P���؋�^tL@;/K��@O�Az�v@
   HXR_QD_V03      QUAD�)~D���>Q���=�p~󏺽��M/��=�H�3M�=E�M��f>x�ʚ���;�p��Ŧ�>(�7�"��9�[�{�=�s�h�2�=oX�v��W>'�R�f�;Ad�'+��>��7�l���J�B=���	C3�=B�q�J�~;�C�޽D�>ݾԃ���b�U���ǽE8An�X��UuJ1�>��V�'M>L?�{��;��I�m?��p�'��<�g�4Y=�Cu^��(?
�>�%?u*b�}i?<��	!�?   �R��>7y�,.�?   B-#=�Cu^��(�
�>�%�u*b�}i��.�(��   Pr־�|@�#��   �0(�ս���j ?�����?0aɶ��?<��	!�?   �R��>7y�,.�?   B-#=�)~D���>�p��Ŧ�>Ad�'+��>�C�޽D�>�UuJ1�>��I�m?�g�4Y="�����=LV�3|w�>�q����=������>G 	W%�=��5�Ww�>�OL%�=����Lw�>y'�L�;
@E��.���^��>S�@�}��e�@O�Az�v@   HXR_COR      KICKER�)~D���>Q���=�p~󏺽��M/��=�H�3M�=E�M��f>x�ʚ���;�p��Ŧ�>(�7�"��9�[�{�=�s�h�2�=oX�v��W>'�R�f�;Ad�'+��>��7�l���J�B=���	C3�=B�q�J�~;�C�޽D�>ݾԃ���b�U���ǽE8An�X��UuJ1�>��V�'M>L?�{��;��I�m?��p�'��<�g�4Y=�Cu^��(?
�>�%?u*b�}i?<��	!�?   �R��>7y�,.�?   B-#=�Cu^��(�
�>�%�u*b�}i��.�(��   Pr־�|@�#��   �0(�ս���j ?�����?0aɶ��?<��	!�?   �R��>7y�,.�?   B-#=�)~D���>�p��Ŧ�>Ad�'+��>�C�޽D�>�UuJ1�>��I�m?�g�4Y="�����=LV�3|w�>�q����=������>G 	W%�=��5�Ww�>�OL%�=����Lw�>y'�L�;
@E��.���^��>S�@�}��e�@O�Az�v@   HXR_BPM      MONI�)~D���>Q���=�p~󏺽��M/��=�H�3M�=E�M��f>x�ʚ���;�p��Ŧ�>(�7�"��9�[�{�=�s�h�2�=oX�v��W>'�R�f�;Ad�'+��>��7�l���J�B=���	C3�=B�q�J�~;�C�޽D�>ݾԃ���b�U���ǽE8An�X��UuJ1�>��V�'M>L?�{��;��I�m?��p�'��<�g�4Y=�Cu^��(?
�>�%?u*b�}i?<��	!�?   �R��>7y�,.�?   B-#=�Cu^��(�
�>�%�u*b�}i��.�(��   Pr־�|@�#��   �0(�ս���j ?�����?0aɶ��?<��	!�?   �R��>7y�,.�?   B-#=�)~D���>�p��Ŧ�>Ad�'+��>�C�޽D�>�UuJ1�>��I�m?�g�4Y="�����=LV�3|w�>�q����=������>G 	W%�=��5�Ww�>�OL%�=����Lw�>y'�L�;
@E��.���^��>S�@�}��e�@M�8}�@
   HXR_QD_V03      QUAD��H��>y
,�9�=���l���������=ro&dj�=��\)�g>vA0���;�G��x�>�����y���lk=��تBt�=����8>�9埧*�;�/m��>RkXq�׽�;S�A�B=��6o,�=�
�#��~;q!{�=�>X(o���=�>�]��=	7�9.W;�E�J1�>���ł'M>}I�Ww��;��I�m?��X#��<0#�F0Y=� �	~�(?o��߿?l�)��E?��6L_
?   zS��>7y�,.�?   -#=� �	~�(�o��߿�l�)��E���6L_
�   |r־�|@�#��   /(���ڊ ?�׭>�.?�8�
��?�|h@
?   zS��>7y�,.�?   -#=��H��>�G��x�>�/m��>q!{�=�>�E�J1�>��I�m?0#�F0Y=��	��=�Mg)��>(����'�=sDNꦜ>3��b�/�=��(�ˁ�>��Q�/�=��^���>Y���xp
@s+%B����6�yp@�h�M���?�o�2��@   HXR_DR_QD_ED      DRIF�'@	j9�>a6�u:��=M�4��ٺ��!<t�<�=�z�Dm��=�br��>g>%}��;�G��x�>B� �c���lk=-)ِ?t�=����8>�H��*�;f�	L#h�>��O�h׽� _�B=.�fE�=>�8[�!;q!{�=�>ѿ�˃�=�>�]��=�j6��-W;Y�;5K1�>����'M>��Kd��;��I�m?bfn7��<B�k�Y=��8��V)?o��߿?�>0O}?��6L_
?   �W��>7y�,.�?   -#=��8��V)�o��߿��>0O}���6L_
�   �r־�|@�#��   �((���s� ?�׭>�.?�#��Q?�|h@
?   �W��>7y�,.�?   -#=�'@	j9�>�G��x�>f�	L#h�>q!{�=�>Y�;5K1�>��I�m?B�k�Y=��	��=�Mg)��>&����'�=}sDNꦜ>1��b�/�=�(�ˁ�>��Q�/�=��^���>a�� ��
@3\�s��ï`��@��E��?	�T�@	   HXR_DR_V4      DRIF�¿�6?KH����=�=��P���K��>��=]WԼ���=ת �]�l>��_���;�G��x�>��cU7H���lk=
{3��s�=����8>�pR%�;�� l�>��Ȏ<��J���(�H=jjv-��=6���̈́;q!{�=�>�\?zQ�=�>�]��=�H5�i+W;�<B]1�>M5�y�'M>?tȜ̩�;��I�m?W��u~�<?�1rV=�U�6?o��߿?�n�Z�j?��6L_
?   ����>7y�,.�?   Zd+#=�U�6�o��߿���l����6L_
�   �־�|@�#��   dF'��O� .?�׭>�.?�n�Z�j?�|h@
?   ����>7y�,.�?   Zd+#=�¿�6?�G��x�>�� l�>q!{�=�>�<B]1�>��I�m??�1rV=��	��=�Mg)��>'����'�=~sDNꦜ>1��b�/�=�(�ˁ�>��Q�/�=��^���>�h\�"�@�A�(W���p�
���@��]����?ko`�t#@	   HXR_DR_CT      DRIF[�y�M�?�.W�>�=g:���ɶ�l��=� �0*�=։*��m>r���}�;�G��x�>s=v'W���lk=�fX��s�=����8>,Rl�%�;�bK�6��>�1��������5I=�0̓0��=�=1���;q!{�=�>��'�@�=�>�]��=x���E+W;�|^1�>���:�'M>0����;��I�m?N2-�P~�<cnc�LV=~ ��7?o��߿?>���&?��6L_
?   <���>7y�,.�?   KM+#=~ ��7�o��߿�˄��:���6L_
�   4�־�|@�#��   �9'�9����.?�׭>�.?>���&?�|h@
?   <���>7y�,.�?   KM+#=[�y�M�?�G��x�>�bK�6��>q!{�=�>�|^1�>��I�m?cnc�LV=��	��=�Mg)��>'����'�=~sDNꦜ>1��b�/�=�(�ˁ�>��Q�/�=��^���>�����@'��S���{vi�@`����?ko`�t#@   HXR_WA_M_OU      WATCH[�y�M�?�.W�>�=g:���ɶ�l��=� �0*�=։*��m>r���}�;�G��x�>s=v'W���lk=�fX��s�=����8>,Rl�%�;�bK�6��>�1��������5I=�0̓0��=�=1���;q!{�=�>��'�@�=�>�]��=x���E+W;�|^1�>���:�'M>0����;��I�m?N2-�P~�<cnc�LV=~ ��7?o��߿?>���&?��6L_
?   <���>7y�,.�?   KM+#=~ ��7�o��߿�˄��:���6L_
�   4�־�|@�#��   �9'�9����.?�׭>�.?>���&?�|h@
?   <���>7y�,.�?   KM+#=[�y�M�?�G��x�>�bK�6��>q!{�=�>�|^1�>��I�m?cnc�LV=��	��=�Mg)��>'����'�=~sDNꦜ>1��b�/�=�(�ˁ�>��Q�/�=��^���>�����@'��S���{vi�@`����?� N8�X@   HXR_DR_QD_ED      DRIF��7��?�%�����=e�=m��{�a(���=0���)�=�O@��Dm>�?5��%�;�G��x�>�*�k����lk=<�Xw�s�=����8>pH ��$�;��avE��>0l?q�W���I~�HI=�ʮ�'��=ۦ�u<>�;q!{�=�>$p�7�=�>�]��=;R�3+W;[��^1�>�Fo$�'M>e/픩�;��I�m?jT��=~�<<To�8V=�JO�_�7?o��߿?T D��?��6L_
?   � ��>7y�,.�?   MA+#=�JO�_�7�o��߿�E�#Y3����6L_
�   ��־�|@�#��   l3'�w�Qb/?�׭>�.?T D��?�|h@
?   � ��>7y�,.�?   MA+#=��7��?�G��x�>��avE��>q!{�=�>[��^1�>��I�m?<To�8V=��	��=�Mg)��>)����'�=�sDNꦜ>1��b�/�=�(�ˁ�>��Q�/�=��^���>�R{k@������i.�HȬ@,��O�?-e�f@	   HXR_QD_FH      QUAD�X���?�e%	DO��k�t����D��Å=B�)i�(�="7�T{Em>	����#�;vV�7��>�����	��ԵU�z�߿g"zV��+�g��4��Bht�ʻ]�O�q��>Pgj��=i$�:\I=���/�="j���N�;�#+��>����2�0=�Eeb<�=]w�)�.l;M!%�^1�>m5@�'M>�*4�;��I�m?.z��8~�<f;��3V=�N��7?�D���?]y��?A��
�p?   ���>7y�,.�?   >+#=�N��7��D����ES��A��
�p�   ��־�|@�#��   �1'�1����t/?�(��?��>]y��?)t��?   ���>7y�,.�?   >+#=�X���?vV�7��>]�O�q��>�#+��>M!%�^1�>��I�m?f;��3V=� 0�p�=.K_���>�\[j�7�=O��N��>^��P1�=�ۤ|���>�>KxE1�=���7|��>)=���m@uz��_��?e��ף@��#��㻿-e�f@   HXR_COR      KICKER�X���?�e%	DO��k�t����D��Å=B�)i�(�="7�T{Em>	����#�;vV�7��>�����	��ԵU�z�߿g"zV��+�g��4��Bht�ʻ]�O�q��>Pgj��=i$�:\I=���/�="j���N�;�#+��>����2�0=�Eeb<�=]w�)�.l;M!%�^1�>m5@�'M>�*4�;��I�m?.z��8~�<f;��3V=�N��7?�D���?]y��?A��
�p?   ���>7y�,.�?   >+#=�N��7��D����ES��A��
�p�   ��־�|@�#��   �1'�1����t/?�(��?��>]y��?)t��?   ���>7y�,.�?   >+#=�X���?vV�7��>]�O�q��>�#+��>M!%�^1�>��I�m?f;��3V=� 0�p�=.K_���>�\[j�7�=O��N��>^��P1�=�ۤ|���>�>KxE1�=���7|��>)=���m@uz��_��?e��ף@��#��㻿-e�f@   HXR_BPM      MONI�X���?�e%	DO��k�t����D��Å=B�)i�(�="7�T{Em>	����#�;vV�7��>�����	��ԵU�z�߿g"zV��+�g��4��Bht�ʻ]�O�q��>Pgj��=i$�:\I=���/�="j���N�;�#+��>����2�0=�Eeb<�=]w�)�.l;M!%�^1�>m5@�'M>�*4�;��I�m?.z��8~�<f;��3V=�N��7?�D���?]y��?A��
�p?   ���>7y�,.�?   >+#=�N��7��D����ES��A��
�p�   ��־�|@�#��   �1'�1����t/?�(��?��>]y��?)t��?   ���>7y�,.�?   >+#=�X���?vV�7��>]�O�q��>�#+��>M!%�^1�>��I�m?f;��3V=� 0�p�=.K_���>�\[j�7�=O��N��>^��P1�=�ۤ|���>�>KxE1�=���7|��>)=���m@uz��_��?e��ף@��#��㻿����Ss@	   HXR_QD_FH      QUAD�j���?����c���E�FAz����𥎌��x��=��zdV3m>:6�
�;���r�j�>`�V��=eU�s��z�1�R2.���?&��P�W����������+��>��B���=ON��I=��>��=g�^4m�;U|XnPL�>�f�3ق:=j���S��=s��mv;q���^1�>�yC�'M>G?齊��;��I�m?����3~�<��.V=旇9�7?���?�Y���?�-5~�?   ���>7y�,.�?   F:+#=旇9�7��O�T�����p=��z�Dc�J�   Ȁ־�|@�#��   20'�|zFs/?���?�Y���?�-5~�?   ���>7y�,.�?   F:+#=�j���?���r�j�>����+��>U|XnPL�>q���^1�>��I�m?��.V=�|�8�=A�t̫��>r=��G�=(�wyǜ>�'�^3�=��%C��>��J�2�=����7��>ͧ�hI@g�a~
�?@��η�@��j_��Z�s��@   HXR_DR_QD_ED      DRIF㫄ૐ?�7A��S���k��B��c�d!�@���`�G��=���l>qz~6v��;���r�j�>��
&�3�=eU�s��z���i�3.���?&��P���'���O�,"��>K6�l+��=��Fk0J=�u���=�,l�T�;U|XnPL�>���؂:=j���S��=���*mv;�U��^1�>6��a�'M>,�v��;��I�m?|n� ~�<���aV=J�&�ԫ7?���?���\�?�-5~�?   ���>7y�,.�?   x*+#=J�&�ԫ7��O�T���7�t�0�z�Dc�J�   h�־�|@�#��   �)'��1J\�C/?���?���\�?�-5~�?   ���>7y�,.�?   x*+#=㫄ૐ?���r�j�>O�,"��>U|XnPL�>�U��^1�>��I�m?���aV=�|�8�=?�t̫��>p=��G�=(�wyǜ>�'�^3�=��%C��>��J�2�=����7��>��&L5�@��a��?����c6@ޝ��o��7<-�Ap@   HXR_DR_WIG_ED      DRIF7��\�?]^�_+���
��F��դ�׾܏�X�^{��=K��C'k>#3�G���;���r�j�>��o���=eU�s��z�<�28.���?&��P��o.�3��|8�^��>�k���=�_��1�L=I�����=����1�;U|XnPL�>��9�ׂ:=j���S��=-��F�lv;#��^1�>�#Xզ'M>�s��(��;��I�m?/�α�}�<q����U=w|�e�6?���?���}~?�-5~�?   ���>7y�,.�?   /�*#=w|�e�6��O�T��sO��2��z�Dc�J�   ��־�|@�#��   x'�> ���.?���?���}~?�-5~�?   ���>7y�,.�?   /�*#=7��\�?���r�j�>|8�^��>U|XnPL�>#��^1�>��I�m?q����U=�|�8�=A�t̫��>r=��G�=(�wyǜ>�'�^3�=��%C��>��J�2�=����7��>u�'�+�@�T%�a�?�����R@`�,^�����@	   HXR_WIG_0      WIGGLER��a����>�%��!��B�'{�ĽOS%�mL��=A����=�p"f��X>�W J���;���r�j�>����Mvm����q� {��gT"[(���?&��P����Ổ>�J�'�>��3Sq�=�	ԗtY=(������=�I�-�s�;�=6�.�>��H��V7=�s��6��=�&���s;k9٬�.�>��"M>Ȭ/�q��;��I�m?�V��v�<`��J�N=P�c3k0?���?f-�9��,?��i#�?   x��>7y�,.�?   =%#=P�c3k0��O�T���0V�?*&���i#��   t��վ�|@�#��   |�$��q�N�K(?���?f-�9��,?�g���E?   x��>7y�,.�?   =%#=��a����>���r�j�>�>�J�'�>�=6�.�>k9٬�.�>��I�m?`��J�N=�|�8�=A�t̫��>r=��G�=(�wyǜ>�'�^3�=��%C��>��J�2�=����7��>(XH�	@|�}��?�uy�C�@'p�h?����/% @   HXR_DR_WIG_ED      DRIF�y����>Ks �����H��Ľ�D�>�������H�c�=���C��U>�Gu�e�;���r�j�>����Yt����q� {��*O�_(���?&��P�δ�n|��#Y^�9�>���J�=2���ߗZ=>̯���=�N�T"j�;�=6�.�>r�S��V7=�s��6��=`�W�s;����.�>'Ќo�"M>�?�$��;��I�m?Q�Lzv�<+�!�=N=4�߈e/?���?r�zex3.?��i#�?   R��>7y�,.�?   �%#=4�߈e/��O�T�����^g['���i#��   ���վ�|@�#��   �$��`d~_�'?���?r�zex3.?�g���E?   R��>7y�,.�?   �%#=�y����>���r�j�>#Y^�9�>�=6�.�>����.�>��I�m?+�!�=N=�|�8�=@�t̫��>q=��G�=(�wyǜ>�'�^3�=��%C��>��J�2�=����7��>{jr�n@�03vd�?
��(uN@1G�㑯��c���? @   HXR_DR_QD_ED	      DRIF�e�kt�>�2����བྷ�)P�Ľ�@��A���\sy��<�=,,
���T>^��0:w�;���r�j�>�`xnu����q� {�JZa(���?&��P��B��e��=Ӽ�Y�>�Ay���=9mE��Z=W1��r	�=%�vǫ�;�=6�.�>�,?�V7=�s��6��=ֻ�H�s;����.�>wkx��"M>Amc���;��I�m?�kszfv�<0}<k)N=��5�+/?���?d�\�7�.?��i#�?   ���>7y�,.�?   
�$#=��5�+/��O�T����BǬ'���i#��   D��վ�|@�#��   ��$��h�k'?���?d�\�7�.?�g���E?   ���>7y�,.�?   
�$#=�e�kt�>���r�j�>=Ӽ�Y�>�=6�.�>����.�>��I�m?0}<k)N=�|�8�=?�t̫��>q=��G�=(�wyǜ>�'�^3�=��%C��>��J�2�=����7��>$��W��@�g�@c�?��4D�@��@�����1/�vF @	   HXR_QD_DH      QUADc0m8�k�>�)N���u=ƞ�s��Ľ"&.�,��=�V��=��;o�T>����-A�;�g����> ;��K����B�448p���^��ߡ��5ffI�E[��ݻ�l��a�>-QU��Ǟ=�P��;�Z=B����=�
@�Ӵ�;	a�׳�>�at���=�J͢�=�=xb���\@;|Ҕ��.�>9�`��"M>��Z�
��;��I�m?́^�av�<��K$N=&u>��.?z::��?��)��.?=+1��>   ���>7y�,.�?   h�$#=&u>��.�z::���F����'�8^��   h��վ�|@�#��   
�$��)�f'?��uc ?��)��.?=+1��>   ���>7y�,.�?   h�$#=c0m8�k�>�g����>�l��a�>	a�׳�>|Ҕ��.�>��I�m?��K$N=�O���=���TR��>%Y�G�=x��eǜ>k��:25�=	�z��>8U%'5�=O@.oo��>�u���@񉀝ĕ��^�h��@X�Z�|���1/�vF @   HXR_COR      KICKERc0m8�k�>�)N���u=ƞ�s��Ľ"&.�,��=�V��=��;o�T>����-A�;�g����> ;��K����B�448p���^��ߡ��5ffI�E[��ݻ�l��a�>-QU��Ǟ=�P��;�Z=B����=�
@�Ӵ�;	a�׳�>�at���=�J͢�=�=xb���\@;|Ҕ��.�>9�`��"M>��Z�
��;��I�m?́^�av�<��K$N=&u>��.?z::��?��)��.?=+1��>   ���>7y�,.�?   h�$#=&u>��.�z::���F����'�8^��   h��վ�|@�#��   
�$��)�f'?��uc ?��)��.?=+1��>   ���>7y�,.�?   h�$#=c0m8�k�>�g����>�l��a�>	a�׳�>|Ҕ��.�>��I�m?��K$N=�O���=���TR��>%Y�G�=x��eǜ>k��:25�=	�z��>8U%'5�=O@.oo��>�u���@񉀝ĕ��^�h��@X�Z�|���1/�vF @   HXR_BPM      MONIc0m8�k�>�)N���u=ƞ�s��Ľ"&.�,��=�V��=��;o�T>����-A�;�g����> ;��K����B�448p���^��ߡ��5ffI�E[��ݻ�l��a�>-QU��Ǟ=�P��;�Z=B����=�
@�Ӵ�;	a�׳�>�at���=�J͢�=�=xb���\@;|Ҕ��.�>9�`��"M>��Z�
��;��I�m?́^�av�<��K$N=&u>��.?z::��?��)��.?=+1��>   ���>7y�,.�?   h�$#=&u>��.�z::���F����'�8^��   h��վ�|@�#��   
�$��)�f'?��uc ?��)��.?=+1��>   ���>7y�,.�?   h�$#=c0m8�k�>�g����>�l��a�>	a�׳�>|Ҕ��.�>��I�m?��K$N=�O���=���TR��>%Y�G�=x��eǜ>k��:25�=	�z��>8U%'5�=O@.oo��>�u���@񉀝ĕ��^�h��@X�Z�|����̷M @	   HXR_QD_DH      QUAD�s���t�>x�
ʱP�=xF���ĽW�㑤="Yt��ť=[3^�p�T>��F�?�;�)uу�>Ij�������� ��=�$+�E��|�n��A�Q>`�ruֻY�vM[�>}�ؽ>�p�Z=s��`�
�=Q՛�$��;�e�|�>�mF�,�2�c�^��ѽ�̿�Po���G��.�>7���"M>����;��I�m?� ��\v�<���,N=���Ԁ�.?�4�?�{�U�.?y�Ɛ�?   V��>7y�,.�?   ��$#=���Ԁ�.��4���+����'�y�Ɛ��   ���վ�|@�#��   ~�$�|�֎q'?�w���?�{�U�.?���W!�?   V��>7y�,.�?   ��$#=�s���t�>�)uу�>Y�vM[�>�e�|�>��G��.�>��I�m?���,N=.Ӽ���=C�-���>�PaK�G�=��m2xǜ>�9N�7�=Hc9ԉ�>PC�6v7�=<���ȉ�>�.Ӡ�@�	h�[^�sM�DG�@�8��?q�Cv�g @   HXR_DR_QD_ED
      DRIF=�b ���>c���A�=���#�$Ž����ݤ=��u�Z�=D�9�/#T>�>Mx���;�)uу�>�c�~������ ��=���E��|�n��A���tcuֻ,r,i*#�>�<�+Tؽ��N�N�Z=jG9(��=��F��z�;�e�|�>��f*�2�c�^��ѽ5E(�Po�py�
�.�>��ø"M>o@����;��I�m?l���Hv�<�G��
N=N%�+:/?�4�?S�2�k.?y�Ɛ�?   ���>7y�,.�?   �$#=N%�+:/��4����1�i�'�y�Ɛ��   t��վ�|@�#��   v�$���,&��'?�w���?S�2�k.?���W!�?   ���>7y�,.�?   �$#==�b ���>�)uу�>,r,i*#�>�e�|�>py�
�.�>��I�m?�G��
N=,Ӽ���=A�-���>�PaK�G�=��m2xǜ>�9N�7�= Hc9ԉ�>RC�6v7�=>���ȉ�>28��=@���z.濥� c@�|��4^�?���� @   HXR_DR_WIG_ED      DRIF�⍍���>�C^��=��F~E�ŽA$�X8��=�U-/�g�=0�o�|zR>�"
��;�)uу�>lb.�𰽑��� ��=�'���E��|�n��A�^�	+uֻG&�]S�>yY����ֽ=Մ�Q�Y=^U��@��=����B��;�e�|�>l�LL"�2�c�^��ѽ�bg�*Po�	S�I�.�>S�L\�"M>�&ß���;��I�m?�s�u�<XBsR�M=�`v0?�4�?*���՗-?y�Ɛ�?   ~/��>7y�,.�?   ��$#=�`v0��4���6^	C�&�y�Ɛ��   ���վ�|@�#��   �j$�u�`��(?�w���?*���՗-?���W!�?   ~/��>7y�,.�?   ��$#=�⍍���>�)uу�>G&�]S�>�e�|�>	S�I�.�>��I�m?XBsR�M=+Ӽ���=@�-���>�PaK�G�=��m2xǜ>�9N�7�=Hc9ԉ�>PC�6v7�=<���ȉ�>t��첍
@_���:�?�ci\@��$�/�?�G��T$@	   HXR_WIG_0      WIGGLER
6�z��?���@;�=����
~Ž� יQ�=g���ֆ���4�~+>2)��
2ƻ�)uу�>��@�DǦ�:�B��8�=
����?��|�n��A���}t�lֻ3���j��>5SѾg�ɽ�X�d�P=g��繚�=zD8�;�;B���>g�lC6��Gm	~�Խ3�>�zr��ee)G,�>�Il�M>fG֛��;��I�m?e��_�n�<�@��zF=HK��`K6?�4�?����,%?���$�?   82��>7y�,.�?   �#=HK��`K6��4����!q6#����$��   p,�վ�|@�#��   � "�,�Z�@1?�w���?����,%?X�b ?   82��>7y�,.�?   �#=
6�z��?�)uу�>3���j��>�;B���>�ee)G,�>��I�m?�@��zF=/Ӽ���=D�-���>�PaK�G�=��m2xǜ>�9N�7�=Hc9ԉ�>QC�6v7�==���ȉ�>�TH4��@X�u��o��;��	>3
@}�fܦ�?�5�֣�$@   HXR_DR_WIG_ED      DRIFQM�?�|�O��=W|�Մ�Ľ��Y�w��=�?����rw<�r>��V�λ�)uу�>­��s��:�B��8�=a�j��?��|�n��A�V�9]lֻ�J���>@qh�Ȉƽirb CBO=�C�?��=UQM�j�;�;B���>�q=�:6��Gm	~�Խ��.]�yr��Z�WG,�>��|��M>u7$���;��I�m?����n�<���i.F=,���D7?�4�?�d�'$?���$�?   Q��>7y�,.�?   G�#=,���D7��4���xB���"����$��   �/�վ�|@�#��   R�!�l�6P��1?�w���?�d�'$?X�b ?   Q��>7y�,.�?   G�#=QM�?�)uу�>�J���>�;B���>�Z�WG,�>��I�m?���i.F=*Ӽ���=?�-���>�PaK�G�=��m2xǜ>�9N�7�=Hc9ԉ�>QC�6v7�==���ȉ�>���0� @��o���������!@R ���?]���C�$@   HXR_DR_QD_ED      DRIF��|��?�XH��=&��b�Ľ��y��(�=�� '��Z�,ʙ^>�����л�)uу�>������:�B��8�=��U��?��|�n��A�X�5;Nlֻ,���V�>!W*T|�Ž*�㳯N=|���/�=����;�;B���>�986��Gm	~�ԽK
.�yr�I��cG,�>7��
�M>$��{���;��I�m?c��,�n�<9i�F=$�L\B�7?�4�?�� .��#?���$�?   LY��>7y�,.�?   ��#=$�L\B�7��4��x����"����$��   �0�վ�|@�#��   P�!�(�=�V�1?�w���?�� .��#?X�b ?   LY��>7y�,.�?   ��#=��|��?�)uу�>,���V�>�;B���>I��cG,�>��I�m?9i�F=*Ӽ���=?�-���>�PaK�G�=��m2xǜ>�9N�7�=Hc9ԉ�>QC�6v7�==���ȉ�>�>�!�� @���]����{�Y�@>k$
q�?������$@	   HXR_QD_FH      QUAD�u�_��?YQ�����=�:��zĽ,��[�=Q��b�{��L���>U;�N�лA�����>���xU���f
��j�n�˅�˗��xh���A�gA�_�?ӻhQ�L�>��fi�(q��b%� �N=��bX���=x���!ى;��r}���>~�fĚ($���@x�Ž℗�-�`���cG,�>����M>���C���;��I�m?���0�n�<�j��F=zG1�7?LE�7x�>TD���#?H�B,{�>   "Z��>7y�,.�?   h�#=zG1�7�~�������"����"�eԖf���   �0�վ�|@�#��   ��!��`(��1?LE�7x�>TD���#?H�B,{�>   "Z��>7y�,.�?   h�#=�u�_��?A�����>hQ�L�>��r}���>��cG,�>��I�m?�j��F=�`W�=&�����>Bb�2�=e�4�\��>�����4�==�IE���>;���4�=�ˊ����>�W�!@"�n�2����7����@͝��$�?������$@	   HXR_WA_OU      WATCH�u�_��?YQ�����=�:��zĽ,��[�=Q��b�{��L���>U;�N�лA�����>���xU���f
��j�n�˅�˗��xh���A�gA�_�?ӻhQ�L�>��fi�(q��b%� �N=��bX���=x���!ى;��r}���>~�fĚ($���@x�Ž℗�-�`���cG,�>����M>���C���;��I�m?���0�n�<�j��F=zG1�7?LE�7x�>TD���#?H�B,{�>   "Z��>7y�,.�?   h�#=zG1�7�~�������"����"�eԖf���   �0�վ�|@�#��   ��!��`(��1?LE�7x�>TD���#?H�B,{�>   "Z��>7y�,.�?   h�#=�u�_��?A�����>hQ�L�>��r}���>��cG,�>��I�m?�j��F=�`W�=&�����>Bb�2�=e�4�\��>�����4�==�IE���>;���4�=�ˊ����>�W�!@"�n�2����7����@͝��$�?