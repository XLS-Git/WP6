SDDS1
!# little-endian
&description text="Twiss parameters--input: xls_linac_sxrbp_ln4_sbp_1.track.ele  lattice: xls_linac_sxrbp_ln4.9.lte", contents="Twiss parameters", &end
&parameter name=Step, description="Simulation step", type=long, &end
&parameter name=SVNVersion, description="SVN version number", type=string, fixed_value=unknown, &end
&parameter name=nux, symbol="$gn$r$bx$n", units="1/(2$gp$r)", description="Horizontal tune", type=double, &end
&parameter name=dnux/dp, symbol="$gx$r$bx$n", units="1/(2$gp$r)", description="Horizontal chromaticity", type=double, &end
&parameter name=dnux/dp2, symbol="$gx$r$bx2$n", units="1/(2$gp$r)", description="Horizontal 2nd-order chromaticity", type=double, &end
&parameter name=dnux/dp3, symbol="$gx$r$bx3$n", units="1/(2$gp$r)", description="Horizontal 3rd-order chromaticity", type=double, &end
&parameter name=Ax, symbol="A$bx$n", units=m, description="Horizontal acceptance", type=double, &end
&parameter name=AxLocation, units=m, description="Location of horizontal acceptance limit", type=double, &end
&parameter name=nuy, symbol="$gn$r$by$n", units="1/(2$gp$r)", description="Vertical tune", type=double, &end
&parameter name=dnuy/dp, symbol="$gx$r$by$n", units="1/(2$gp$r)", description="Vertical chromaticity", type=double, &end
&parameter name=dnuy/dp2, symbol="$gx$r$by2$n", units="1/(2$gp$r)", description="Vertical 2nd-order chromaticity", type=double, &end
&parameter name=dnuy/dp3, symbol="$gx$r$by3$n", units="1/(2$gp$r)", description="Vertical 3rd-order chromaticity", type=double, &end
&parameter name=Ay, symbol="A$by$n", units=m, description="Vertical acceptance", type=double, &end
&parameter name=AyLocation, units=m, description="Location of vertical acceptance limit", type=double, &end
&parameter name=deltaHalfRange, symbol="$gDd$r/2", description="Half range of momentum offset for chromatic tune spread evaluation", type=double, &end
&parameter name=nuxChromUpper, symbol="$gx$r$bu$n", description="Upper limit of x tune due to chromaticity and deltaRange", type=double, &end
&parameter name=nuxChromLower, symbol="$gx$r$bu$n", description="Lower limit of x tune due to chromaticity and deltaRange", type=double, &end
&parameter name=nuyChromUpper, symbol="$gy$r$bu$n", description="Upper limit of y tune due to chromaticity and deltaRange", type=double, &end
&parameter name=nuyChromLower, symbol="$gy$r$bu$n", description="Lower limit of y tune due to chromaticity and deltaRange", type=double, &end
&parameter name=Stage, description="Stage of computation", type=string, &end
&parameter name=pCentral, units="m$be$nc", description="Central momentum", type=double, &end
&parameter name=dbetax/dp, units=m, description="Derivative of betax with momentum offset", type=double, &end
&parameter name=dbetay/dp, units=m, description="Derivative of betay with momentum offset", type=double, &end
&parameter name=dalphax/dp, description="Derivative of alphax with momentum offset", type=double, &end
&parameter name=dalphay/dp, description="Derivative of alphay with momentum offset", type=double, &end
&parameter name=etax2, symbol="$gc$r$bx2$n", units=m, description="Second-order dispersion (for matched or periodic case only)", type=double, &end
&parameter name=etay2, symbol="$gc$r$by2$n", units=m, description="Second-order dispersion (for matched or periodic case only)", type=double, &end
&parameter name=etax3, symbol="$gc$r$bx3$n", units=m, description="Third-order dispersion (for matched or periodic case only)", type=double, &end
&parameter name=etay3, symbol="$gc$r$by3$n", units=m, description="Third-order dispersion (for matched or periodic case only)", type=double, &end
&parameter name=etaxp2, symbol="$gc$r$bx2$n$a'$n", units=m, description="Second-order dispersion (for matched or periodic case only)", type=double, &end
&parameter name=etayp2, symbol="$gc$r$by2$n$a'$n", units=m, description="Second-order dispersion (for matched or periodic case only)", type=double, &end
&parameter name=etaxp3, symbol="$gc$r$bx3$n$a'$n", units=m, description="Third-order dispersion (for matched or periodic case only)", type=double, &end
&parameter name=etayp3, symbol="$gc$r$by3$n$a'$n", units=m, description="Third-order dispersion (for matched or periodic case only)", type=double, &end
&parameter name=betaxMin, units=m, description="Minimum betax", type=double, &end
&parameter name=betaxAve, units=m, description="Average betax", type=double, &end
&parameter name=betaxMax, units=m, description="Maximum betax", type=double, &end
&parameter name=betayMin, units=m, description="Minimum betay", type=double, &end
&parameter name=betayAve, units=m, description="Average betay", type=double, &end
&parameter name=betayMax, units=m, description="Maximum betay", type=double, &end
&parameter name=etaxMax, units=m, description="Maximum absolute value of etax", type=double, &end
&parameter name=etayMax, units=m, description="Maximum absolute value of etay", type=double, &end
&parameter name=waistsx, description="Number of changes in the sign of alphax", type=long, &end
&parameter name=waistsy, description="Number of changes in the sign of alphay", type=long, &end
&parameter name=dnux/dAx, units=1/m, description="Horizontal tune shift with horizontal amplitude", type=double, &end
&parameter name=dnux/dAy, units=1/m, description="Horizontal tune shift with vertical amplitude", type=double, &end
&parameter name=dnuy/dAx, units=1/m, description="Vertical tune shift with horizontal amplitude", type=double, &end
&parameter name=dnuy/dAy, units=1/m, description="Vertical tune shift with vertical amplitude", type=double, &end
&parameter name=dnux/dAx2, units="1/m$a2$n", description="Horizontal tune shift with horizontal amplitude", type=double, &end
&parameter name=dnux/dAy2, units="1/m$a2$n", description="Horizontal tune shift with vertical amplitude", type=double, &end
&parameter name=dnux/dAxAy, units="1/m$a2$n", description="Horizontal tune shift with horizontal and vertical amplitude", type=double, &end
&parameter name=dnuy/dAx2, units="1/m$a2$n", description="Vertical tune shift with horizontal amplitude", type=double, &end
&parameter name=dnuy/dAy2, units="1/m$a2$n", description="Vertical tune shift with vertical amplitude", type=double, &end
&parameter name=dnuy/dAxAy, units="1/m$a2$n", description="Vertical tune shift with horizontal and vertical amplitude", type=double, &end
&parameter name=nuxTswaLower, description="Minimum horizontal tune from tune-shift-with-amplitude calculations", type=double, &end
&parameter name=nuxTswaUpper, description="Maximum horizontal tune from tune-shift-with-amplitude calculations", type=double, &end
&parameter name=nuyTswaLower, description="Minimum vertical tune from tune-shift-with-amplitude calculations", type=double, &end
&parameter name=nuyTswaUpper, description="Maximum vertical tune from tune-shift-with-amplitude calculations", type=double, &end
&parameter name=couplingIntegral, description="Coupling integral for difference resonance", type=double, &end
&parameter name=couplingDelta, description="Distance from difference resonance", type=double, &end
&parameter name=emittanceRatio, description="Emittance ratio from coupling integral", type=double, &end
&parameter name=alphac2, symbol="$ga$r$bc2$n", description="2nd-order momentum compaction factor", type=double, &end
&parameter name=alphac, symbol="$ga$r$bc$n", description="Momentum compaction factor", type=double, &end
&parameter name=I1, units=m, description="Radiation integral 1", type=double, &end
&parameter name=I2, units=1/m, description="Radiation integral 2", type=double, &end
&parameter name=I3, units="1/m$a2$n", description="Radiation integral 3", type=double, &end
&parameter name=I4, units=1/m, description="Radiation integral 4", type=double, &end
&parameter name=I5, units=1/m, description="Radiation integral 5", type=double, &end
&parameter name=ex0, units="$gp$rm", description="Damped horizontal emittance", type=double, &end
&parameter name=enx0, units="m$be$nc $gp$rm", description="Damped normalized horizontal emittance", type=double, &end
&parameter name=taux, units=s, description="Horizontal damping time", type=double, &end
&parameter name=Jx, description="Horizontal damping partition number", type=double, &end
&parameter name=tauy, units=s, description="Vertical damping time", type=double, &end
&parameter name=Jy, description="Vertical damping partition number", type=double, &end
&parameter name=Sdelta0, description="RMS fractional energy spread", type=double, &end
&parameter name=taudelta, units=s, description="Longitudinal damping time", type=double, &end
&parameter name=Jdelta, description="Longitudinal damping partition number", type=double, &end
&parameter name=U0, units=MeV, description="Energy loss per turn", type=double, &end
&column name=s, units=m, description=Distance, type=double,  &end
&column name=betax, symbol="$gb$r$bx$n", units=m, description="Horizontal beta-function", type=double,  &end
&column name=alphax, symbol="$ga$r$bx$n", description="Horizontal alpha-function", type=double,  &end
&column name=psix, symbol="$gy$r$bx$n", units=rad, description="Horizontal phase advance", type=double,  &end
&column name=etax, symbol="$gc$r$bx$n", units=m, description="Horizontal dispersion", type=double,  &end
&column name=etaxp, symbol="$gc$r$bx$n$a'$n", description="Slope of horizontal dispersion", type=double,  &end
&column name=xAperture, symbol="a$bx,eff$n", units=m, description="Effective horizontal aperture", type=double,  &end
&column name=betay, symbol="$gb$r$by$n", units=m, description="Vertical beta-function", type=double,  &end
&column name=alphay, symbol="$ga$r$by$n", description="Vertical alpha-function", type=double,  &end
&column name=psiy, symbol="$gy$r$by$n", units=rad, description="Vertical phase advance", type=double,  &end
&column name=etay, symbol="$gc$r$by$n", units=m, description="Vertical dispersion", type=double,  &end
&column name=etayp, symbol="$gc$r$by$n$a'$n", description="Slope of vertical dispersion", type=double,  &end
&column name=yAperture, symbol="a$by,eff$n", units=m, description="Effective vertical aperture", type=double,  &end
&column name=pCentral0, symbol="p$bcent$n", units="m$be$nc", description="Initial central momentum", type=double,  &end
&column name=ElementName, description="Element name", format_string=%10s, type=string,  &end
&column name=ElementOccurence, description="Occurence of element", format_string=%6ld, type=long,  &end
&column name=ElementType, description="Element-type name", format_string=%10s, type=string,  &end
&column name=ChamberShape, type=string,  &end
&column name=dI1, units=m, description="Contribution to radiation integral 1", type=double,  &end
&column name=dI2, units=1/m, description="Contribution to radiation integral 2", type=double,  &end
&column name=dI3, units="1/m$a2$n", description="Contribution to radiation integral 3", type=double,  &end
&column name=dI4, units=1/m, description="Contribution to radiation integral 4", type=double,  &end
&column name=dI5, units=1/m, description="Contribution to radiation integral 5", type=double,  &end
&data mode=binary, &end
%       �6?<��?�C����                        ��������{�vm��?��sc^�?                        ��������        �6?<��?�6?<��?{�vm��?{�vm��?   tunes uncorrectedU(S�U/�@�{ݝ"��5  #/0�����,�?�0?��?                                                                T���,��?����@f��h�)@����"�?~�͇�@�!�%@�}�U�ռ?                                                                                                                                      ޵��A��?        u ��{?h�e]?���%)�n�Z]{�?F?��\C�?r�R�=G	|,"�>0���><ǘ9 �>�������?�CT���?gP~+���?      �?�td�|F?�z���z?��n��?�;ƣ�g?        P2,�A�?�Ac~��?        N�/�Z_?�(���a?      $@�!�%@�ö�@                              $@U(S�U/�@   _BEG_      MARK                                                    P2,�A�?�Ac~��?        N�/�Z_?�(���a?      $@�!�%@�ö�@                              $@U(S�U/�@   BNCH      CHARGE   ?                                                P2,�A�?�Ac~��?        N�/�Z_?�(���a?      $@�!�%@�ö�@                              $@U(S�U/�@
   FITT_SBP_0      MARK   ?                                                P2,�A�?�Ac~��?        N�/�Z_?�(���a?      $@�!�%@�ö�@                              $@U(S�U/�@   SBP_CNT0      DRIF   ?                                        v�=T�?�)����?r��x)��?Ѿ"6��?�7����]�uW�&Š�      $@�4r�{�#@N���@C0آ��?                      $@U(S�U/�@   SBP_DP_SEPM1   	   CSRCSBEND   ?��=k�V?n�Z]{�?F?��\Cf?~X<�7��>t�ٓTJ�>v�=T�?�)����?r��x)��?Ѿ"6��?�7����]�uW�&Š�      $@�4r�{�#@N���@C0آ��?                      $@U(S�U/�@
   FITT_SBP_A      MARK   ?                                        v�=T�?T���,��?2p�[ǿ������?TATH���L���EO��      $@$kL�%"@}?)�5=@JsC��?                      $@U(S�U/�@   SBP_DP_SEPM1   	   CSRCSBEND   ?h;av��n�Z]{�?F?��\Cf? _�Ӡ��@�-�>T�f�C��?���dP�?�(�c���@��W�?��#�	��L���EO��      $@�>����@=����@�c&�	�?                      $@U(S�U/�@   SBP_DR_BP_C      CSRDRIFT   ?                                        �' R�4�?�A��N�@���_.���v�� @J�NH쨿L���EO��      $@��l��@���dg�@����HZ�?                      $@U(S�U/�@	   SBP_DR_BP      DRIF   ?                                        ���u;g�?�	A��@��=YAY�j��P@��慧��L���EO��      $@�'\F3-@��]F 7@v�{�uZ�?                      $@U(S�U/�@	   SBP_DR_BP      DRIF   ?                                        ���B4�?,�=@\�@8Z��k9������@V��ض�L���EO��      $@�P��@�ĈO2��?���m���?                      $@U(S�U/�@	   SBP_DR_BP      DRIF   ?                                        zL��h�?����Qo&@�l؎�_��$����@�2�Y�e��L���EO��      $@��,�s� @<Go����?�����?                      $@U(S�U/�@	   SBP_DR_V1      DRIF   ?                                        (�����?
��;n�'@�����4��U ���@.5�9"��L���EO��      $@�����.�?�ԑ���?�$vC�?                      $@U(S�U/�@   SBP_DR_QD_ED      DRIF   ?                                        ��̺�?%?��D�(@�`.Y����J@��+�����a| e�t��      $@T�p��?����$ �?�b )Vn�?                      $@U(S�U/�@
   SBP_QD_V01      QUAD   ?                                        ��̺�?%?��D�(@�`.Y����J@��+�����a| e�t��      $@T�p��?����$ �?�b )Vn�?                      $@U(S�U/�@   SBP_COR      KICKER   ?                                        ��̺�?%?��D�(@�`.Y����J@��+�����a| e�t��      $@T�p��?����$ �?�b )Vn�?                      $@U(S�U/�@   SBP_BPM      MONI   ?                                        pu!��^�?՝�`��(@)A��Cҿ�"���@3�x{�Լ� ��:�j%�      $@�;0!f�?��Ȯ�~�?!G*[���?                      $@U(S�U/�@
   SBP_QD_V01      QUAD   ?                                        �*��?}Z=2)@'�u��ҿ��`�@3���ռ� ��:�j%�      $@y~;8�D�?5��Q��?^�M��L�?                      $@U(S�U/�@   SBP_DR_QD_ED      DRIF   ?                                        [��5�?���)	)@�r�
�ҿ���M{@;�h/ռ� ��:�j%�      $@�g>>�?��އ�?��%��?                      $@U(S�U/�@	   SBP_DR_V2      DRIF   ?                                        �u!��^�?��>")@��Q5�ҿ��	@����Jռ� ��:�j%�      $@�/Q��7�?��B0���?��M���?                      $@U(S�U/�@	   SBP_DR_V2      DRIF   ?                                        F�*��?f��h�)@����ҿ&1z{@�}�U�ռ� ��:�j%�      $@����"�?����d�?�ʼ�}�?                      $@U(S�U/�@   SBP_DR_QD_ED      DRIF   ?                                        �-@4���?�c�%��(@��ݩ�@aq�@-�$�������=-
K�?      $@��<p�?�5k���Ǖ��@r�?                      $@U(S�U/�@
   SBP_QD_V01      QUAD   ?                                        �-@4���?�c�%��(@��ݩ�@aq�@-�$�������=-
K�?      $@��<p�?�5k���Ǖ��@r�?                      $@U(S�U/�@   SBP_COR      KICKER   ?                                        �-@4���?�c�%��(@��ݩ�@aq�@-�$�������=-
K�?      $@��<p�?�5k���Ǖ��@r�?                      $@U(S�U/�@   SBP_BPM      MONI   ?                                        G�>3* @�.�)�'@질��?@�lMN&@���%���R/8;�?      $@��(S�}�?6�"I�+M�!�?                      $@U(S�U/�@
   SBP_QD_V01      QUAD   ?                                        s�\=� @?�M���&@��"E�@'8��-@��z�i���R/8;�?      $@�r��] @�NC����Ή�P���?                      $@U(S�U/�@   SBP_DR_QD_ED      DRIF   ?                                        �BE˩�@�3�s�( @24�ܺ@]
���e@?g0=�ᶿ�R/8;�?      $@��z�0�@��^���+����r�?                      $@U(S�U/�@	   SBP_DR_V1      DRIF   ?                                        ���1@���#T@rx��@�{X���@\λ�����R/8;�?      $@���HP�@n9+����j�Ad|�?                      $@U(S�U/�@	   SBP_DR_BP      DRIF   ?                                        ^�vh@�Z(�|@d�-��	@��!Zn@�j�Ё���R/8;�?      $@'K�4!�@4^�������gS^��?                      $@U(S�U/�@	   SBP_DR_BP      DRIF   ?                                        �ux���	@h��"�?ǟ�^u7�?��X=<�@\rJ�����R/8;�?      $@Zg�g@]�M}��x��2t�?                      $@U(S�U/�@	   SBP_DR_BP      DRIF   ?                                        *��dC5@�	S�?�ɣZ��?>�e���@������R/8;�?      $@���xO�@��_�X�j��6��?                      $@U(S�U/�@	   SBP_DR_BP      DRIF   ?                                        *��dC5@�	S�?�ɣZ��?>�e���@������R/8;�?      $@���xO�@��_�X�j��6��?                      $@U(S�U/�@   FITT_SBP_PI      MARK   ?                                        *��dC5@�	S�?�ɣZ��?>�e���@������R/8;�?      $@���xO�@��_�X�j��6��?                      $@U(S�U/�@   SBP_CNT0      DRIF   ?                                        �z��x5@Bg5����?L��)G�?E��%�i@�{>�tc��)��\��?      $@~�3�|!@�_ͨ���o�|�G�?                      $@U(S�U/�@   SBP_DP_SEPM1   	   CSRCSBEND   ?cV`�� �n�Z]{�?F?��\Cf?��9$���aCz�5�>�z��x5@Bg5����?L��)G�?E��%�i@�{>�tc��)��\��?      $@~�3�|!@�_ͨ���o�|�G�?                      $@U(S�U/�@
   FITT_SBP_B      MARK   ?                                        X��5@���6;�?p)B�8ؿ;A���B@<�4�P@�`�[��d�      $@%26 #@�]�;��	�^�����?                      $@U(S�U/�@   SBP_DP_SEPM1   	   CSRCSBEND   ?t��o�>n�Z]{�?F?��\Cf?fi�\,u�>C�L�-�>X��5@���6;�?p)B�8ؿ;A���B@<�4�P@�`�[��d�      $@%26 #@�]�;��	�^�����?                      $@U(S�U/�@
   SBP_WA_OU1      WATCH   ?                                        