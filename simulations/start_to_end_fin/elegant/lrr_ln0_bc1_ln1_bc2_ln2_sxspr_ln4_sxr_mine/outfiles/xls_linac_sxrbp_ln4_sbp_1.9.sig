SDDS1
!# little-endian
&description text="sigma matrix--input: xls_linac_sxrbp_ln4_sbp_1.track.ele  lattice: xls_linac_sxrbp_ln4.9.lte", contents="sigma matrix", &end
&parameter name=Step, description="Simulation step", type=long, &end
&parameter name=SVNVersion, description="SVN version number", type=string, fixed_value=unknown, &end
&column name=s, units=m, description=Distance, type=double,  &end
&column name=ElementName, description="Element name", format_string=%10s, type=string,  &end
&column name=ElementOccurence, description="Occurence of element", format_string=%6ld, type=long,  &end
&column name=ElementType, description="Element-type name", format_string=%10s, type=string,  &end
&column name=s1, symbol="$gs$r$b1$n", units=m, description="sqrt(<x*x>)", type=double,  &end
&column name=s12, symbol="$gs$r$b12$n", units=m, description="<x*xp'>", type=double,  &end
&column name=s13, symbol="$gs$r$b13$n", units="m$a2$n", description="<x*y>", type=double,  &end
&column name=s14, symbol="$gs$r$b14$n", units=m, description="<x*y'>", type=double,  &end
&column name=s15, symbol="$gs$r$b15$n", units="m$a2$n", description="<x*s>", type=double,  &end
&column name=s16, symbol="$gs$r$b16$n", units=m, description="<x*delta>", type=double,  &end
&column name=s17, symbol="$gs$r$b17$n", units="m*s", description="<x*t>", type=double,  &end
&column name=s2, symbol="$gs$r$b2$n", description="sqrt(<x'*x'>)", type=double,  &end
&column name=s23, symbol="$gs$r$b23$n", units=m, description="<x'*y>", type=double,  &end
&column name=s24, symbol="$gs$r$b24$n", description="<x'*y'>", type=double,  &end
&column name=s25, symbol="$gs$r$b25$n", units=m, description="<x'*s>", type=double,  &end
&column name=s26, symbol="$gs$r$b26$n", description="<x'*delta>", type=double,  &end
&column name=s27, symbol="$gs$r$b27$n", units=s, description="<x'*t>", type=double,  &end
&column name=s3, symbol="$gs$r$b3$n", units=m, description="sqrt(<y*y>)", type=double,  &end
&column name=s34, symbol="$gs$r$b34$n", units=m, description="<y*y'>", type=double,  &end
&column name=s35, symbol="$gs$r$b35$n", units="m$a2$n", description="<y*s>", type=double,  &end
&column name=s36, symbol="$gs$r$b36$n", units=m, description="<y*delta>", type=double,  &end
&column name=s37, symbol="$gs$r$b37$n", units="m*s", description="<y*t>", type=double,  &end
&column name=s4, symbol="$gs$r$b4$n", description="sqrt(<y'*y')>", type=double,  &end
&column name=s45, symbol="$gs$r$b45$n", units=m, description="<y'*s>", type=double,  &end
&column name=s46, symbol="$gs$r$b46$n", description="<y'*delta>", type=double,  &end
&column name=s47, symbol="$gs$r$b47$n", units=s, description="<y'*t>", type=double,  &end
&column name=s5, symbol="$gs$r$b5$n", units=m, description="sqrt(<s*s>)", type=double,  &end
&column name=s56, symbol="$gs$r$b56$n", units=m, description="<s*delta>", type=double,  &end
&column name=s57, symbol="$gs$r$b57$n", units="m*s", description="<s*t>", type=double,  &end
&column name=s6, symbol="$gs$r$b6$n", description="sqrt(<delta*delta>)", type=double,  &end
&column name=s67, symbol="$gs$r$b67$n", units=s, description="<delta*t>", type=double,  &end
&column name=s7, symbol="$gs$r$b7$n", description="sqrt(<t*t>)", type=double,  &end
&column name=ma1, symbol="max$sb$ex$sb$e", units=m, description="maximum absolute value of x", type=double,  &end
&column name=ma2, symbol="max$sb$ex'$sb$e", description="maximum absolute value of x'", type=double,  &end
&column name=ma3, symbol="max$sb$ey$sb$e", units=m, description="maximum absolute value of y", type=double,  &end
&column name=ma4, symbol="max$sb$ey'$sb$e", description="maximum absolute value of y'", type=double,  &end
&column name=ma5, symbol="max$sb$e$gD$rs$sb$e", units=m, description="maximum absolute deviation of s", type=double,  &end
&column name=ma6, symbol="max$sb$e$gd$r$sb$e", description="maximum absolute value of delta", type=double,  &end
&column name=ma7, symbol="max$sb$e$gD$rt$sb$e", units=s, description="maximum absolute deviation of t", type=double,  &end
&column name=minimum1, symbol="x$bmin$n", units=m, type=double,  &end
&column name=minimum2, symbol="x'$bmin$n", units=m, type=double,  &end
&column name=minimum3, symbol="y$bmin$n", units=m, type=double,  &end
&column name=minimum4, symbol="y'$bmin$n", units=m, type=double,  &end
&column name=minimum5, symbol="$gD$rs$bmin$n", units=m, type=double,  &end
&column name=minimum6, symbol="$gd$r$bmin$n", units=m, type=double,  &end
&column name=minimum7, symbol="t$bmin$n", units=s, type=double,  &end
&column name=maximum1, symbol="x$bmax$n", units=m, type=double,  &end
&column name=maximum2, symbol="x'$bmax$n", units=m, type=double,  &end
&column name=maximum3, symbol="y$bmax$n", units=m, type=double,  &end
&column name=maximum4, symbol="y'$bmax$n", units=m, type=double,  &end
&column name=maximum5, symbol="$gD$rs$bmax$n", units=m, type=double,  &end
&column name=maximum6, symbol="$gd$r$bmax$n", units=m, type=double,  &end
&column name=maximum7, symbol="t$bmax$n", units=s, type=double,  &end
&column name=Sx, symbol="$gs$r$bx$n", units=m, description=sqrt(<(x-<x>)^2>), type=double,  &end
&column name=Sxp, symbol="$gs$r$bx'$n", description=sqrt(<(x'-<x'>)^2>), type=double,  &end
&column name=Sy, symbol="$gs$r$by$n", units=m, description=sqrt(<(y-<y>)^2>), type=double,  &end
&column name=Syp, symbol="$gs$r$by'$n", description=sqrt(<(y'-<y'>)^2>), type=double,  &end
&column name=Ss, symbol="$gs$r$bs$n", units=m, description=sqrt(<(s-<s>)^2>), type=double,  &end
&column name=Sdelta, symbol="$gs$bd$n$r", description=sqrt(<(delta-<delta>)^2>), type=double,  &end
&column name=St, symbol="$gs$r$bt$n", units=s, description=sqrt(<(t-<t>)^2>), type=double,  &end
&column name=ex, symbol="$ge$r$bx$n", units=m, description="geometric horizontal emittance", type=double,  &end
&column name=enx, symbol="$ge$r$bx,n$n", units=m, description="normalized horizontal emittance", type=double,  &end
&column name=ecx, symbol="$ge$r$bx,c$n", units=m, description="geometric horizontal emittance less dispersive contributions", type=double,  &end
&column name=ecnx, symbol="$ge$r$bx,cn$n", units=m, description="normalized horizontal emittance less dispersive contributions", type=double,  &end
&column name=ey, symbol="$ge$r$by$n", units=m, description="geometric vertical emittance", type=double,  &end
&column name=eny, symbol="$ge$r$by,n$n", units=m, description="normalized vertical emittance", type=double,  &end
&column name=ecy, symbol="$ge$r$by,c$n", units=m, description="geometric vertical emittance less dispersive contributions", type=double,  &end
&column name=ecny, symbol="$ge$r$by,cn$n", units=m, description="normalized vertical emittance less dispersive contributions", type=double,  &end
&column name=betaxBeam, symbol="$gb$r$bx,beam$n", units=m, description="betax for the beam, excluding dispersive contributions", type=double,  &end
&column name=alphaxBeam, symbol="$ga$r$bx,beam$n", description="alphax for the beam, excluding dispersive contributions", type=double,  &end
&column name=betayBeam, symbol="$gb$r$by,beam$n", units=m, description="betay for the beam, excluding dispersive contributions", type=double,  &end
&column name=alphayBeam, symbol="$ga$r$by,beam$n", description="alphay for the beam, excluding dispersive contributions", type=double,  &end
&data mode=binary, &end
%                 _BEG_      MARK�r{���>#G����ѽӡ�=�^�=v�W\�(���~�#�Dh=�Ï�>�e�3b�;έBa+��>�x��ڒ�=��Ń��X=W4�*w��=���4FR>ӗ��k�;?r�AA�>oP]>�w����d�:=�;S�m�=n��5Kw;Ο�.c�>��?(��'����!t�ѽVFi��d�s$� 
�> 7�4P2>b�<����;����f?�ۖ-.Ao<HeT��<�~k?O�?��]��3%?����(?*8��L?   .e|�>��vô�?   (��=�ڥY���]��3%����Ϊr&�*8��L�    ��ʾ#���Wz�   `�U��~k?O�?����L ?����(?��^��a?   .e|�>��vô�?   (��=�r{���>έBa+��>?r�AA�>Ο�.c�>s$� 
�>����f?HeT��<�""Y���=s�hcyӌ>��9OD~�=�|6�BY�>�4�p�$�=l�N%~��>�{��$�=W?�v��>�2,�A�?�Ac~��?��!�%@�ƶ�@           BNCH      CHARGE�r{���>#G����ѽӡ�=�^�=v�W\�(���~�#�Dh=�Ï�>�e�3b�;έBa+��>�x��ڒ�=��Ń��X=W4�*w��=���4FR>ӗ��k�;?r�AA�>oP]>�w����d�:=�;S�m�=n��5Kw;Ο�.c�>��?(��'����!t�ѽVFi��d�s$� 
�> 7�4P2>b�<����;����f?�ۖ-.Ao<HeT��<�~k?O�?��]��3%?����(?*8��L?   .e|�>��vô�?   (��=�ڥY���]��3%����Ϊr&�*8��L�    ��ʾ#���Wz�   `�U��~k?O�?����L ?����(?��^��a?   .e|�>��vô�?   (��=�r{���>έBa+��>?r�AA�>Ο�.c�>s$� 
�>����f?HeT��<�""Y���=s�hcyӌ>��9OD~�=�|6�BY�>�4�p�$�=l�N%~��>�{��$�=W?�v��>�2,�A�?�Ac~��?��!�%@�ƶ�@        
   FITT_SBP_0      MARK�r{���>#G����ѽӡ�=�^�=v�W\�(���~�#�Dh=�Ï�>�e�3b�;έBa+��>�x��ڒ�=��Ń��X=W4�*w��=���4FR>ӗ��k�;?r�AA�>oP]>�w����d�:=�;S�m�=n��5Kw;Ο�.c�>��?(��'����!t�ѽVFi��d�s$� 
�> 7�4P2>b�<����;����f?�ۖ-.Ao<HeT��<�~k?O�?��]��3%?����(?*8��L?   .e|�>��vô�?   (��=�ڥY���]��3%����Ϊr&�*8��L�    ��ʾ#���Wz�   `�U��~k?O�?����L ?����(?��^��a?   .e|�>��vô�?   (��=�r{���>έBa+��>?r�AA�>Ο�.c�>s$� 
�>����f?HeT��<�""Y���=s�hcyӌ>��9OD~�=�|6�BY�>�4�p�$�=l�N%~��>�{��$�=W?�v��>�2,�A�?�Ac~��?��!�%@�ƶ�@           SBP_CNT0      DRIF�r{���>#G����ѽӡ�=�^�=v�W\�(���~�#�Dh=�Ï�>�e�3b�;έBa+��>�x��ڒ�=��Ń��X=W4�*w��=���4FR>ӗ��k�;?r�AA�>oP]>�w����d�:=�;S�m�=n��5Kw;Ο�.c�>��?(��'����!t�ѽVFi��d�s$� 
�> 7�4P2>b�<����;����f?�ۖ-.Ao<HeT��<�~k?O�?��]��3%?����(?*8��L?   .e|�>��vô�?   (��=�ڥY���]��3%����Ϊr&�*8��L�    ��ʾ#���Wz�   `�U��~k?O�?����L ?����(?��^��a?   .e|�>��vô�?   (��=�r{���>έBa+��>?r�AA�>Ο�.c�>s$� 
�>����f?HeT��<�""Y���=s�hcyӌ>��9OD~�=�|6�BY�>�4�p�$�=l�N%~��>�{��$�=W?�v��>�2,�A�?�Ac~��?��!�%@�ƶ�@v�=T�?   SBP_DP_SEPM1   	   CSRCSBEND8�]���>6|���=q���;�=	��z���ke*��)��0�0t�qM�RQ-��ݻ�2fK�?�S�ޅ�==��[�T�=��_�����3`L��Z�u�@ ���k4�>�T>gI�G5�;S�9� t�����=�����w��au�,c�>h3��_�,=�m6�[ҽ��გ9j;��7��>�}�ͤ1>�K����;c���f?O%����n<|�ŪL/�<���Y�?���-�p7?
/v�'?Ur��L?   WV��>�.�jkj�?   Ny=֪�^�����-�p7�״/QM�%�Ur��L�   �̾S�M�{�   P�/	����Y�?)��j�A5?
/v�'?ߗ[�@a?   WV��>�.�jkj�?   Ny=8�]���>�2fK�?��k4�>�au�,c�>��7��>c���f?|�ŪL/�<��
����=4���>CC��9g�=}q7_QÇ>ʨ���$�=X0��>�r%˟$�=W.�^'��>K���E�?K9�����?T�=x�#@�8���@v�=T�?
   FITT_SBP_A      MARK8�]���>6|���=q���;�=	��z���ke*��)��0�0t�qM�RQ-��ݻ�2fK�?�S�ޅ�==��[�T�=��_�����3`L��Z�u�@ ���k4�>�T>gI�G5�;S�9� t�����=�����w��au�,c�>h3��_�,=�m6�[ҽ��გ9j;��7��>�}�ͤ1>�K����;c���f?O%����n<|�ŪL/�<���Y�?���-�p7?
/v�'?Ur��L?   WV��>�.�jkj�?   Ny=֪�^�����-�p7�״/QM�%�Ur��L�   �̾S�M�{�   P�/	����Y�?)��j�A5?
/v�'?ߗ[�@a?   WV��>�.�jkj�?   Ny=8�]���>�2fK�?��k4�>�au�,c�>��7��>c���f?|�ŪL/�<��
����=4���>CC��9g�=}q7_QÇ>ʨ���$�=X0��>�r%˟$�=W.�^'��>K���E�?K9�����?T�=x�#@�8���@v�=T�?   SBP_DP_SEPM1   	   CSRCSBEND�T�GH��>b.AS�l1>�\N+Ǘ=w��!�A���D�%ȽU��� o�cq�	��j��b(?r�Ԩ&ri��]��=��a��e��<,=њ��˽
�[5���	�(�>�L9l�ϴ	S��Q#W
�=fAVmn=���;��b�>��� �B=���f�Խ��E4�܀;j~��V�>#~j� �5>r��b}�;�GR�je?!`�@�s<�y��;o�<��+e�?N=��wE?^���=�&?E���K?   ���>��M���?   pO=��+e��N=��wE���+��$�E���K�   
�Ѿ��'���{�   g��F��U1�?�jc���A?^���=�&?	z7�o_?   ���>��M���?   pO=�T�GH��>�j��b(?��	�(�>�;��b�>j~��V�>�GR�je?�y��;o�<�a+�P>XH��b��>|�<�R��=��0�f�>�g0��$�=f��2���>�!8�$�=���v��>cFj����?��n��Aƿ�-�� "@k*�)�<@T�f�C��?   SBP_DR_BP_C      CSRDRIFT��N��>?X&l#_N>�Ȍ���=���P��b��+aE��U�>m�������"��j��b(?Q@����l=�]��=�~k~p���D��Zw{{b5�?*35���>��m�D佩k��yIP��!Vn��=k����l���;��b�>R�I~ܧB=˖��[Խl�a܀;=;��[�>t΋�16>hy�oj��;�A�e?&�Q��js<�Z�Pu�<�bo9F�2?N=��wE??-�lm�$?E���K?   ]��>�}؝�~�?   �a=�bo9F�2�N=��wE�{�2 #�E���K�   ��Ѿg>���{�   �V��\�W�$+?�jc���A??-�lm�$?	z7�o_?   ]��>�}؝�~�?   �a=��N��>?�j��b(??*35���>�;��b�>=;��[�>�A�e?�Z�Pu�<�a+�P>���g��>Cs���=�^�>��>�g0��$�=;���i��>MJ��$�=bK,�^��>ݩ�$��?5P�j�4�����꾂@��d�@�' R�4�?	   SBP_DR_BP      DRIF�=��S!?�ڛN�Z>0G[���=����q=}�$E�񽬏Ɛ�x��&#��Q�.��j��b(?�M�髄=�]��=��fy���D���l��i5�wuI��7�>��voὫ���q�J���|���=|բC_���;��b�>��tpW�B=˖��[ԽQ���ۀ;�g�L_�>���96>Q���A��;�A�e?�2l�rs<8��x{�<8!Z�R�??N=��wE?[/�P"?E���K?   u/��>�}؝�~�?   n�s=8!Z�R�?�N=��wE���k8(P!�E���K�   ���Ѿg>���{�   G�����+/O8?�jc���A?[/�P"?	z7�o_?   u/��>�}؝�~�?   n�s=�=��S!?�j��b(?wuI��7�>�;��b�>�g�L_�>�A�e?8��x{�<�`+�P>��g��>rw���=�d�>��>�g0��$�=;���i��>MJ��$�=bK,�^��><�J3+�@���5�LD�J��@�����@���u;g�?	   SBP_DR_BP      DRIF���(?*��E lb>���^�=��G�J�=f������W��J����Jk�T�5��j��b(?��R��=�]��=Z�a�����D��U�MEfq5��<+4���>!�Ƭ�3ݽx��(?dE��}�K>h�=jiL^�Q���;��b�>>[�bҦB=˖��[Խ`�ن�ۀ;i&�_c�>M5�!�A6>�9/��;�A�e?눅
*ys<s"Z���<�o��/IF?N=��wE?��,�� ?E���K?   �B��>�}؝�~�?   ��=�o��/IF�N=��wE���s}� �E���K�   n��Ѿg>���{�   P7����E)�A?�jc���A?��,�� ?	z7�o_?   �B��>�}؝�~�?   ��=���(?�j��b(?�<+4���>�;��b�>i&�_c�>�A�e?s"Z���<Y+�P>O��g��>�����=¹>��>�g0��$�=@���i��>SJ��$�=gK,�^��>?��+�D@ک�fT��l1�{-@��6@���B4�?	   SBP_DR_BP      DRIF7|�JX�/?�3dM�g>�=����=�X�~�=��|"< ��`�M�~��_�_�h<��j��b(?��!�.ڗ=�]��=�,�\[����D�������x5���cm �>!<��׽���!��?�8n�7ޙ�=/�=,�|��;��b�>���TM�B=˖��[Խ8D:ۀ;����f�>�h�J6>g��23��;�A�e?�X�#?�s<*P���<�Ηߵ�L?N=��wE?�m�f��?E���K?   �U��>�}؝�~�?   $�=�Ηߵ�L�N=��wE����`�E���K�   �Ѿg>���{�   �A��9�����F?�jc���A?�m�f��?	z7�o_?   �U��>�}؝�~�?   $�=7|�JX�/?�j��b(?��cm �>�;��b�>����f�>�A�e?*P���<�c+�P>���g��>2���=�8�>��>�g0��$�=@���i��>SJ��$�=gK,�^��>�f�=�P!@? �L�5���b�u@[x���?zL��h�?	   SBP_DR_V1      DRIFh-S�3?�ʉ�l>Y��/���=��ӗ=pu�
�{�YIuZ�ũ�B��8A��j��b(?�ܸ�YƝ=�]��=��m����D��pn}_e5����]*�>�5Ky�ҽ��!�Y�5���M����=V��5�s��;��b�>��5�إB=˖��[ԽLѺ(�ڀ;��M�j�>P���@Q6>e"|k��;�A�e?	�t�s<lSn���<�0/Q?N=��wE?�R�6�3?E���K?   �)��>�}؝�~�?   p�=�0/Q�N=��wE��R�6�3�E���K�   ��Ѿg>���{�   Lu����R�K?�jc���A?ǂ�gK?	z7�o_?   �)��>�}؝�~�?   p�=h-S�3?�j��b(?���]*�>�;��b�>��M�j�>�A�e?lSn���<�c+�P>���g��>����=���>��>�g0��$�=>���i��>PJ��$�=eK,�^��>.�|�!9)@��y ������ @9��d��?(�����?   SBP_DR_QD_ED      DRIF�W�)��3?R���pYm>��Fd&�=fwn��Ș=�W��x�0��v��}K���A��j��b(?DV8.s��=�]��=3��"����D����'�s�5�(�Ԓ�>�y����ѽF/�:4��(���*�=iƇ��@r��;��b�>S�Y�ťB=˖��[Խ"�<�ڀ;��V�j�>iR6>9l�n��;�A�e?���t�s<�#I���<�����Q?N=��wE?� �C�?E���K?   +���>�}؝�~�?   <��=�����Q�N=��wE�� �C��E���K�   �Ѿg>���{�   dд�qU�(\L?�jc���A?��3b?	z7�o_?   +���>�}؝�~�?   <��=�W�)��3?�j��b(?(�Ԓ�>�;��b�>��V�j�>�A�e?�#I���<Y+�P>O��g��>C&���=�$�>��>�g0��$�=>���i��>PJ��$�=eK,�^��>�����*@HQ��� ���(0�?������?��̺�?
   SBP_QD_V01      QUAD~*�6�3?��d-^>����2G�=ͣ̂�|�=�T��a�cGK�����c��\�A���^A D?����֍�=�u��=t7���轫o�
Vj���Zd{�%�"c�g&�>�p�Xo�½A��#��2���N�g��=S׽<q�%1rw�8�>�d�sDg?=���rS�ѽ�%��d|;�n���j�>����S6>�/%���;�A�e?��<T��s<Ls�M ��<9va���Q?ݾ�m@�5?Jܵ$1Y?�8S�,?   �f��>�}؝�~�?   ��=9va���Q�ݾ�m@�5�Jܵ$1Y��8S�,�   ^#�Ѿg>���{�    ��wb�J��L?"H���b2?����P�?���M?   �f��>�}؝�~�?   ��=~*�6�3?��^A D?"c�g&�>%1rw�8�>�n���j�>�A�e?Ls�M ��<��n1ǝ>Pc.��>`��)��=�3-��Q�>�P̈́�(�=QTa/F��>���(�=��;��>о�Z�+@}�j�s��&ɶE��?��l�!�?��̺�?   SBP_COR      KICKER~*�6�3?��d-^>����2G�=ͣ̂�|�=�T��a�cGK�����c��\�A���^A D?����֍�=�u��=t7���轫o�
Vj���Zd{�%�"c�g&�>�p�Xo�½A��#��2���N�g��=S׽<q�%1rw�8�>�d�sDg?=���rS�ѽ�%��d|;�n���j�>����S6>�/%���;�A�e?��<T��s<Ls�M ��<9va���Q?ݾ�m@�5?Jܵ$1Y?�8S�,?   �f��>�}؝�~�?   ��=9va���Q�ݾ�m@�5�Jܵ$1Y��8S�,�   ^#�Ѿg>���{�    ��wb�J��L?"H���b2?����P�?���M?   �f��>�}؝�~�?   ��=~*�6�3?��^A D?"c�g&�>%1rw�8�>�n���j�>�A�e?Ls�M ��<��n1ǝ>Pc.��>`��)��=�3-��Q�>�P̈́�(�=QTa/F��>���(�=��;��>о�Z�+@}�j�s��&ɶE��?��l�!�?��̺�?   SBP_BPM      MONI~*�6�3?��d-^>����2G�=ͣ̂�|�=�T��a�cGK�����c��\�A���^A D?����֍�=�u��=t7���轫o�
Vj���Zd{�%�"c�g&�>�p�Xo�½A��#��2���N�g��=S׽<q�%1rw�8�>�d�sDg?=���rS�ѽ�%��d|;�n���j�>����S6>�/%���;�A�e?��<T��s<Ls�M ��<9va���Q?ݾ�m@�5?Jܵ$1Y?�8S�,?   �f��>�}؝�~�?   ��=9va���Q�ݾ�m@�5�Jܵ$1Y��8S�,�   ^#�Ѿg>���{�    ��wb�J��L?"H���b2?����P�?���M?   �f��>�}؝�~�?   ��=~*�6�3?��^A D?"c�g&�>%1rw�8�>�n���j�>�A�e?Ls�M ��<��n1ǝ>Pc.��>`��)��=�3-��Q�>�P̈́�(�=QTa/F��>���(�=��;��>о�Z�+@}�j�s��&ɶE��?��l�!�?pu!��^�?
   SBP_QD_V01      QUAD	՞�4?��Ŷ��=Q7d{o�=\��l��=յ�D�����FpZ��I�/+E�A�C��&���>�GP�<�i��y�g1?t=�Pu�o�e��
�:��[(��䢻������>�b|�������F�1�R�7��=���4��o��S����>n�<�9=�;<�ν����kw;���V�j�>��L%S6>l#��;�A�e?.���s<n{�o	��<*mSR?�����>�h>�J?"��(?   >���>�}؝�~�?   2R�=*mSR��������h>�J�"��(�   �&�Ѿg>���{�   T���?���M?p�ڠ��>�{�k��?�"?'*�?   >���>�}؝�~�?   2R�=	՞�4?C��&���>������>�S����>���V�j�>�A�e?n{�o	��<�|�G	>�;+��L�>�M�%�E�=]Z�>H�,�=cr�E1��>�;,�=)�w�&��>��Fw*@��m{�̿zз�]^�?����{��?�*��?   SBP_DR_QD_ED      DRIFL�z�4?���J��=+mC�ƭ�=�[�ܮ=�b~����G}����F
p�A�C��&���>݇ȣ�h��y�g1?t=��p�e��
�:��e���䢻�.��^��>���C^u��5��?@�0�*��P��=O=[���m��S����>6#��:�9=�;<�ν+�?�kw;���<�j�>_Bh�$S6>Bg)V��;�A�e?�Ogm�s<E��e���<�uYR?�����>^��b?"��(?   ����>�}؝�~�?   �W�=�uYR�������^��b�"��(�   `&�Ѿg>���{�   ���v�+�M?p�ڠ��>��Q�.�?�"?'*�?   ����>�}؝�~�?   �W�=L�z�4?C��&���>�.��^��>�S����>���<�j�>�A�e?E��e���<�|�G	>�;+��L�>�M�%�E�=]Z�>I�,�=dr�E1��>�;,�=*�w�&��>��J�.�*@�y߯[Ϳ�.�=�<�?!+KB��?[��5�?	   SBP_DR_V2      DRIF�54?��]" �=�����=�@��p�=
��	�����M>	���O�z�A�C��&���> � �g��y�g1?t=!�2p�e��
�:�Ƽ��䢻�lp���>��؛B���
�	�Y0�2�0&p�=8�7�m��S����>��z&:�9=�;<�νQ]�ɥkw;L��6�j�>턚�$S6>(����;�A�e?�5 b�s<�������<G<�ZR?�����>�F�N�g?"��(?   ݉��>�}؝�~�?   �X�=G<�ZR��������F�N�g�"��(�   L&�Ѿg>���{�   0����M?p�ڠ��>:�s8e�?�"?'*�?   ݉��>�}؝�~�?   �X�=�54?C��&���>�lp���>�S����>L��6�j�>�A�e?�������<�|�G	>�;+��L�>�M�%�E�=]Z�>H�,�=cr�E1��>�;,�=)�w�&��>�"눃*@I�ɶuͿ�e�y�5�?>(�:S�?�u!��^�?	   SBP_DR_V2      DRIF�\��'4?r*��D"�=��Sn/"�=���W��=c
���Hi�*�������A�C��&���>[x[�V3g��y�g1?t=~5�Mp�e��
�:����䢻��N���>���m�����կ0�XY�iӺ=�x4Em��S����>�pq�9�9=�;<�ν׷f��kw;�k0�j�>{���$S6>���2��;�A�e?��V �s<�x����<��o�[R?�����>ؙ���l?"��(?   ����>�}؝�~�?   Z�=��o�[R�������ؙ���l�"��(�   6&�Ѿg>���{�   ���VR���M?p�ڠ��>�t����?�"?'*�?   ����>�}؝�~�?   Z�=�\��'4?C��&���>��N���>�S����>�k0�j�>�A�e?�x����<�|�G	>�;+��L�>�M�%�E�=]Z�>H�,�=cr�E1��>�;,�=)�w�&��>��K�*@h\4���Ϳ��rM�/�?Z%os��?F�*��?   SBP_DR_QD_ED      DRIF ���4?dI���+�=�+1�ð=�$+|t�=e\��>��:�T����m�|��A�C��&���>>��v�ze��y�g1?t=F���p�e��
�:���e��䢻�j�����>yN>���X�� ��-��^IE9�=�մ�|	k��S����>y�A<8�9=�;<�ν㊹��kw;؃5�j�>o��$S6>��߇���;�A�e?��4���s<	����<d,�aR?�����>�{^_1�?"��(?   }���>�}؝�~�?   �_�=d,�aR��������{^_1��"��(�   �%�Ѿg>���{�   ���/�?�"M?p�ڠ��>��$�?�"?'*�?   }���>�}؝�~�?   �_�= ���4?C��&���>�j�����>�S����>؃5�j�>�A�e?	����<�|�G	>�;+��L�>QN�%�E�=�]Z�>I�,�=dr�E1��>�;,�=*�w�&��> �"�*@(�@[�Ϳ�h�?�7�G%թ?�-@4���?
   SBP_QD_V01      QUAD�*��"�3?��&S�]�H���Z�=З�����=�oZ��b����Sl�����<�A�!i���?�R�x����w�Y����Zu(��=nR��p7�>i@�|�%<QHq���>;�3�Eu�=F_�},��
q��=G��Ri����5��>���5=u���˽����ys;t��k�>"͜c:S6>��`��;�A�e?�o��s<�ְ���<��K޽�Q?MQp�5?z����? �<�Ki?   ����>�}؝�~�?   ���=��K޽�Q�Y%�>p�0�z����� �<�Ki�   8�Ѿg>���{�   ������g��L?MQp�5?�%[�?�\�?   ����>�}؝�~�?   ���=�*��"�3?!i���?QHq���>���5��>t��k�>�A�e?�ְ���<�턙�D
>���<�>h2ku�;�=�&�7��>;��:1�=X���> 8	,1�=Z��\t��>�����)@�i���@���Zb�?���ӹ���-@4���?   SBP_COR      KICKER�*��"�3?��&S�]�H���Z�=З�����=�oZ��b����Sl�����<�A�!i���?�R�x����w�Y����Zu(��=nR��p7�>i@�|�%<QHq���>;�3�Eu�=F_�},��
q��=G��Ri����5��>���5=u���˽����ys;t��k�>"͜c:S6>��`��;�A�e?�o��s<�ְ���<��K޽�Q?MQp�5?z����? �<�Ki?   ����>�}؝�~�?   ���=��K޽�Q�Y%�>p�0�z����� �<�Ki�   8�Ѿg>���{�   ������g��L?MQp�5?�%[�?�\�?   ����>�}؝�~�?   ���=�*��"�3?!i���?QHq���>���5��>t��k�>�A�e?�ְ���<�턙�D
>���<�>h2ku�;�=�&�7��>;��:1�=X���> 8	,1�=Z��\t��>�����)@�i���@���Zb�?���ӹ���-@4���?   SBP_BPM      MONI�*��"�3?��&S�]�H���Z�=З�����=�oZ��b����Sl�����<�A�!i���?�R�x����w�Y����Zu(��=nR��p7�>i@�|�%<QHq���>;�3�Eu�=F_�},��
q��=G��Ri����5��>���5=u���˽����ys;t��k�>"͜c:S6>��`��;�A�e?�o��s<�ְ���<��K޽�Q?MQp�5?z����? �<�Ki?   ����>�}؝�~�?   ���=��K޽�Q�Y%�>p�0�z����� �<�Ki�   8�Ѿg>���{�   ������g��L?MQp�5?�%[�?�\�?   ����>�}؝�~�?   ���=�*��"�3?!i���?QHq���>���5��>t��k�>�A�e?�ְ���<�턙�D
>���<�>h2ku�;�=�&�7��>;��:1�=X���> 8	,1�=Z��\t��>�����)@�i���@���Zb�?���ӹ��G�>3* @
   SBP_QD_V01      QUAD+��G�3?���]R-m�0�|�=aX+���=��� ������y���!�)�A�����'?v�Z� 1���%ߍ�~����y��=��Fr+�>N92�o5<���cW�>x�XP6��=�Zx���*�I`��2�= ڎ��g�������>A��e1=��?�^�ǽ(b�N��o;���Nk�>�����S6>�w�&���;�A�e?:�����s<<a��e��<��H��Q?�ɑ�tE?��o�(C?� �0ס?   FA��>�}؝�~�?   N��=��H��Q�u��/A���o�(C�� �0ס�   p�Ѿg>���{�   � ���#-AuL?�ɑ�tE?C^�2?Oq�~�?   FA��>�}؝�~�?   N��=+��G�3?����'?���cW�>������>���Nk�>�A�e?<a��e��<_p��?�>��7R��>Vo��+`�=�2C��2�>(L�6�=;轭���>%�V��5�=?9��>�Ć��&@䥯r��@,�^��i�?@��U+��s�\=� @   SBP_DR_QD_ED      DRIF�[�c3?��2�rkl�Wc0�~�=��5{-�=�}���[�y|ʩ��6�jVA�����'?o�Fr���%ߍ�~���]U���=��Fr+�>r:�4�p5<�ƳzA��>��	����=}�dH)��&rb�ֱ=B�u�f�������>�6���d1=��?�^�ǽB9�6�o;��)�k�>�*��T6>j�PX~��;�A�e?W�i2y�s<BU�@��<�>m�`/Q?�ɑ�tE?��|��?� �0ס?   
s �>�}؝�~�?   \ׯ=�>m�`/Q�u��/A���|���� �0ס�   n��Ѿg>���{�   ���t09�D�K?�ɑ�tE?e��1�?Oq�~�?   
s �>�}؝�~�?   \ׯ=�[�c3?����'?�ƳzA��>������>��)�k�>�A�e?BU�@��<_p��?�>��7R��>Vo��+`�=�2C��2�>'L�6�=;轭���>$�V��5�=>9��>`j��K�%@�"μ��@]������?�����BE˩�@	   SBP_DR_V1      DRIF.��H��/?��N(�g�݋���δ=h����=��w�L ���S�̉�� �y}܅<�����'?{� �����%ߍ�~��/���=��Fr+�>���`�v5<�< ��:�>�R��E2�=�|�-���R���=�=�w�\�������>���^1=��?�^�ǽ7�>�$zo;����n�>݄���[6>��Sh��;�A�e?d(�Y�s<5����<��ǣ�L?�ɑ�tE?H�o=T?� �0ס?   |��>�}؝�~�?   Z.�=��ǣ�L�u��/A�H�o=T�� �0ס�   &��Ѿg>���{�   ߵ�zn(�5G?�ɑ�tE?�����6?Oq�~�?   |��>�}؝�~�?   Z.�=.��H��/?����'?�< ��:�>������>����n�>�A�e?5����<_p��?�>��7R��>Vo��+`�=�2C��2�>(L�6�=;轭���>%�V��5�=?9��>�P'~4�@�BM�d@���K��@ YU;������1@	   SBP_DR_BP      DRIFSW�R�(? ���cb����7��=��	��=��<#AF��ī������&u��6�����'?�
|�)����%ߍ�~����Sӏ�=��Fr+�>�&	}5<[/��[�>3]�ԕ�=�lM��h�{��A���w���C�������>�}Z�W1=��?�^�ǽr���mo;I���r�>��{c6>�;�<��;�A�e?�����s<��f���<zQ�h�JF?�ɑ�tE?M�F0'!?� �0ס?   �D�>�}؝�~�?   <g�=zQ�h�JF�u��/A�M�F0'!�� �0ס�   ���Ѿg>���{�   4����꣯B?�ɑ�tE?�7fe�(?Oq�~�?   �D�>�}؝�~�?   <g�=SW�R�(?����'?[/��[�>������>I���r�>�A�e?��f���<_p��?�>��7R��>h���+`�=�PD��2�>(L�6�=;轭���>%�V��5�=?9��>$�D2��@%�j�V@J�k`Ǎ@l��zt��^�vh@	   SBP_DR_BP      DRIFR�3g}!?U��:Z�4���s�=w��X�=������,C���r� _/�����'?O���򸽁%ߍ�~���X�
���=��Fr+�>mc�U�5<�?�V�>�g����=@��`�=����R�����8&УA;������>�tJ��P1=��?�^�ǽ
��?�ao;@B7�Rv�>]��<k6>y+�o%��;�A�e?0�4]Üs<j�y�ߡ�<~
���??�ɑ�tE?����A�#?� �0ס?   +��>�}؝�~�?   ��=~
���?�u��/A�����A�#�� �0ס�   >W�Ѿg>���{�   d����o�>��9?�ɑ�tE?{I|w�!?Oq�~�?   +��>�}؝�~�?   ��=R�3g}!?����'?�?�V�>������>@B7�Rv�>�A�e?j�y�ߡ�<_p��?�>��7R��>̩��+`�=��B��2�>*L�6�==轭���>'�V��5�=@9��> 	1���@�uC�_�@ӓ�_�@������ux���	@	   SBP_DR_BP      DRIF�����?��F `�N��Ut���=��N�O��=��ޥ�!�e}�i0񋾺X�JU|"�����'?�/r�]_���%ߍ�~��b�^���=��Fr+�>�²Y,�5<�%�Ϗ�>�8_#_�=/���=�p��½Ԥ�Q z[;������>����I1=��?�^�ǽ8��gSUo;���z�>%����r6>��~C$��;�A�e?�V�/x�s<PUzc$��<r�U��2?�ɑ�tE?�XdS!&?� �0ס?   �!!�>�}؝�~�?   ���=r�U��2�u��/A��XdS!&�� �0ס�   ���Ѿg>���{�   �m���Z�k��.?�ɑ�tE?�F����#?Oq�~�?   �!!�>�}؝�~�?   ���=�����?����'?�%�Ϗ�>������>���z�>�A�e?PUzc$��<_p��?�>��7R��>G��+`�=3\B��2�>#L�6�=8轭���> �V��5�=;9��>��x�4�?��.����?7���R@���)�m�*��dC5@	   SBP_DR_BP      DRIFl%�����>��ٔ�2�L��L�=���Tϙ�=�Qwzxɽ����q�Y�ɛ�E�����'?��������%ߍ�~��)�����=��Fr+�>6��5<�TS~���>$>�?���=N��L_F)=3&jv�ʽ|�h$�	g;������>�@��B1=��?�^�ǽ�*��Io;���
�}�><5��z6>8��;�A�e?Dp�-�s<$���{��<���k?�ɑ�tE?i�d�(?� �0ס?   ۏ)�>�}؝�~�?   ��=���k�u��/A�i�d�(�� �0ס�   Jt�Ѿg>���{�   �G��D�ڶ{?�ɑ�tE?��ao�&?Oq�~�?   ۏ)�>�}؝�~�?   ��=l%�����>����'?�TS~���>������>���
�}�>�A�e?$���{��<_p��?�>��7R��>!���+`�=ǑB��2�>,L�6�=>轭���>)�V��5�=B9��>��v}J��?�����e�?�׋}=�@V_5`�G�*��dC5@   FITT_SBP_PI      MARKl%�����>��ٔ�2�L��L�=���Tϙ�=�Qwzxɽ����q�Y�ɛ�E�����'?��������%ߍ�~��)�����=��Fr+�>6��5<�TS~���>$>�?���=N��L_F)=3&jv�ʽ|�h$�	g;������>�@��B1=��?�^�ǽ�*��Io;���
�}�><5��z6>8��;�A�e?Dp�-�s<$���{��<���k?�ɑ�tE?i�d�(?� �0ס?   ۏ)�>�}؝�~�?   ��=���k�u��/A�i�d�(�� �0ס�   Jt�Ѿg>���{�   �G��D�ڶ{?�ɑ�tE?��ao�&?Oq�~�?   ۏ)�>�}؝�~�?   ��=l%�����>����'?�TS~���>������>���
�}�>�A�e?$���{��<_p��?�>��7R��>!���+`�=ǑB��2�>,L�6�=>轭���>)�V��5�=B9��>��v}J��?�����e�?�׋}=�@V_5`�G�*��dC5@   SBP_CNT0      DRIFl%�����>��ٔ�2�L��L�=���Tϙ�=�Qwzxɽ����q�Y�ɛ�E�����'?��������%ߍ�~��)�����=��Fr+�>6��5<�TS~���>$>�?���=N��L_F)=3&jv�ʽ|�h$�	g;������>�@��B1=��?�^�ǽ�*��Io;���
�}�><5��z6>8��;�A�e?Dp�-�s<$���{��<���k?�ɑ�tE?i�d�(?� �0ס?   ۏ)�>�}؝�~�?   ��=���k�u��/A�i�d�(�� �0ס�   Jt�Ѿg>���{�   �G��D�ڶ{?�ɑ�tE?��ao�&?Oq�~�?   ۏ)�>�}؝�~�?   ��=l%�����>����'?�TS~���>������>���
�}�>�A�e?$���{��<_p��?�>��7R��>!���+`�=ǑB��2�>,L�6�=>轭���>)�V��5�=B9��>��v}J��?�����e�?�׋}=�@V_5`�G��z��x5@   SBP_DP_SEPM1   	   CSRCSBEND7�R�\�>#"^���A/@��x=H�W�'Vy=�-������g�a�S���ߠ�r�<��L�?�H/������w�sE*��t���=�*z�$i�>z�O+<	r%����>K1��p��=����X2���k��ͽ[�@�1p�J���>����  �?�*ӴȽŦ�~�N�Ç�*�|�>�ܒ]1�;>�:���f�;�M1��e?H�
8�qx<���M�=v@�?��&��s7?�����)?�E�c:�?   �t�>&`��wi�?   ��=v@����q+r4������)��E�c:��   �
1Ҿ��*�J0|�   V�����@5�?��&��s7?�K�+�&?�u�	?   �t�>&`��wi�?   ��=7�R�\�><��L�?	r%����>J���>Ç�*�|�>�M1��e?���M�=��mZ��=�=wq-�>jkZ���=�K�Q�J�>��ʐ4�=p�k,��>n���4�=R�� ��>�{H}۰�?���ǛJz������o!@_˪�6y��z��x5@
   FITT_SBP_B      MARK7�R�\�>#"^���A/@��x=H�W�'Vy=�-������g�a�S���ߠ�r�<��L�?�H/������w�sE*��t���=�*z�$i�>z�O+<	r%����>K1��p��=����X2���k��ͽ[�@�1p�J���>����  �?�*ӴȽŦ�~�N�Ç�*�|�>�ܒ]1�;>�:���f�;�M1��e?H�
8�qx<���M�=v@�?��&��s7?�����)?�E�c:�?   �t�>&`��wi�?   ��=v@����q+r4������)��E�c:��   �
1Ҿ��*�J0|�   V�����@5�?��&��s7?�K�+�&?�u�	?   �t�>&`��wi�?   ��=7�R�\�><��L�?	r%����>J���>Ç�*�|�>�M1��e?���M�=��mZ��=�=wq-�>jkZ���=�K�Q�J�>��ʐ4�=p�k,��>n���4�=R�� ��>�{H}۰�?���ǛJz������o!@_˪�6y�X��5@   SBP_DP_SEPM1   	   CSRCSBENDڪT��5�>�� ���=���u���Tn���+=��I/y���F{�μI3�)��L�ϻ,��"��>��]��w���s:������ij��}��4��?�^�<��븻1�"S��>�W�A��=^���L�.�7�'mGѽ`|�E�j���R���>a��Gb'���8��
ɽ��ƺ�d��)�ch�>�H�P��<>o��^]��;�9k�
�e?��d�vy<��5��= W���?��~�6)!?%aէ��*?p���;�?   4���>�nA�Ka�?   �[= W������~�6)!�%aէ��*�p���;��   ��Ѿm7�ʝ|�   $��� T���>��k
�!?箐���'?�ខ�	?   4���>�nA�Ka�?   �[=ڪT��5�>,��"��>1�"S��>��R���>�)�ch�>�9k�
�e?��5��="������=|��ap��>~!tlr�=Dߍ��>����4�=�x����>��b�4�=�:�<���>��O��?��<���-8pI#@��j���	�X��5@
   SBP_WA_OU1      WATCHڪT��5�>�� ���=���u���Tn���+=��I/y���F{�μI3�)��L�ϻ,��"��>��]��w���s:������ij��}��4��?�^�<��븻1�"S��>�W�A��=^���L�.�7�'mGѽ`|�E�j���R���>a��Gb'���8��
ɽ��ƺ�d��)�ch�>�H�P��<>o��^]��;�9k�
�e?��d�vy<��5��= W���?��~�6)!?%aէ��*?p���;�?   4���>�nA�Ka�?   �[= W������~�6)!�%aէ��*�p���;��   ��Ѿm7�ʝ|�   $��� T���>��k
�!?箐���'?�ខ�	?   4���>�nA�Ka�?   �[=ڪT��5�>,��"��>1�"S��>��R���>�)�ch�>�9k�
�e?��5��="������=|��ap��>~!tlr�=Dߍ��>����4�=�x����>��b�4�=�:�<���>��O��?��<���-8pI#@��j���	�