

set filee [open $filename.0.lte w]
set pi [expr acos(-1.)]


set case0 [info exists track_ln0]
set case1 [info exists track_bc1]
set case2 [info exists track_bc1_dia]
set case3 [info exists track_ln1]
set case4 [info exists track_bc2]
set case5 [info exists track_ln2]
set case6 [info exists track_ln2_dia]

set case7 [info exists track_sxrbpl]

set case8a [info exists track_sbp1]
set case8b [info exists track_sbp2]
set case9 [info exists track_ln4]
set case10 [info exists track_sxdr ]
set case11 [info exists track_sxrl ]

puts " track ln0 $case0 "
puts " track bc1 $case1 "
puts " track bc1dia $case2 "
puts " track ln1 $case3" 
puts " track bc2 $case4" 
puts " track ln2 $case5"
puts " track ln2 di $case6"
puts " track sxrbp $case7"
puts " track sxr bp1 $case8a"
puts " track sxr bp2 $case8b"
puts " track ln4 $case9 "
puts " track sx dr $case10 "
puts " track sxrl $case11 "
after 1000

puts $filee "
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! beamline for XLS
!! [clock format [clock seconds] -format "%B %Y"]
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!------------------------------------------------------------------------------
!          bunch
!------------------------------------------------------------------------------
BNCH           : CHARGE,TOTAL=75e-12

EMTCH          : ENERGY, MATCH_BEAMLINE=1

!------------------------------------------------------------------------------
! Accelerating structures
!------------------------------------------------------------------------------
LN0_CCA0       : RFCW, freq=$cband_par(freq),L=$cband_par(active_l),volt=$ln0_cvolt ,phase=\"90 $ln0_cphs -\", change_p0=1, &
                       cell_length=$cband_par(cell_l),end1_focus=1, end2_focus=1, smoothing=1, ZWAKE=$zwake_flag, TRWAKE=$twake_flag, &
                       zwakefile=\"$cband_par(wakefile)\",tColumn=\"t\",wzColumn=\"Wl\",interpolate=1, n_bins=0, &
                       trwakefile=\"$cband_par(wakefile)\",tColumn=\"t\",wxColumn=\"Wt\",wyColumn=\"Wt\", GROUP=\"LN0\" &

LN0_XCA0       : RFCW, freq=$xband_par(freq),L=$xband_par(active_l),volt=$ln0_xvolt ,phase=\"90 $ln0_xphs -\", change_p0=1, &
                       cell_length=$xband_par(cell_l),end1_focus=1, end2_focus=1, smoothing=1, ZWAKE=$zwake_flag, TRWAKE=$twake_flag, &
                       zwakefile=\"$xband_par(wakefile)\",tColumn=\"t\",wzColumn=\"Wl\",interpolate=1, n_bins=0, &
                       trwakefile=\"$xband_par(wakefile)\",tColumn=\"t\",wxColumn=\"Wt\",wyColumn=\"Wt\", GROUP=\"LN0\" &
                       
LN0_KCA0       : RFCW, freq=$kband_par(freq),L=$kband_par(active_l),volt=$ln0_kvolt ,phase=\"90 $ln0_kphs -\", change_p0=1, &
                       cell_length=$kband_par(cell_l),end1_focus=1, end2_focus=1, smoothing=1, ZWAKE=$zwake_flag, TRWAKE=$twake_flag, &
                       zwakefile=\"$kband_par(wakefile)\",tColumn=\"t\",wzColumn=\"Wl\",interpolate=1, n_bins=0, &
                       trwakefile=\"$kband_par(wakefile)\",tColumn=\"t\",wxColumn=\"Wt\",wyColumn=\"Wt\", GROUP=\"LN0\" &
                       
LN0_XCA0_DI       : RFDF, frequency=$xdband_par(freq),L=$xdband_par(active_l),voltage=$ln0_xvolt_di ,phase=\"$ln0_xphs_di \", &
                        tilt=$ln0_xtilt_di, n_kicks=$ln0_xkick_di, GROUP=\"LN0\" &
                       

LN0_XZW_DI         : WAKE, inputfile=\"$xdband_par(wakefile)\", factor=$xdband_par(n_cell), tColumn=\"t\", wColumn=\"Wl\", &
                       interpolate=1, smoothing=1, N_BINS=0,CHANGE_P0=1,allow_long_beam=1 

LN0_XTW_DI         : TRWAKE ,inputfile=\"$xdband_par(wakefile)\",factor=$xdband_par(n_cell), tColumn=\"t\",wxColumn=\"Wt\",wyColumn=\"Wt\", &
                       interpolate=1, smoothing=1, N_BINS=0
                       
LN0_XCA_DI         : LINE=(LN0_XCA0_DI,$zwake_flag*LN0_XZW_DI,$twake_flag*LN0_XTW_DI) 

                       
BC1_XCA0_DI       : RFDF, frequency=$xdband_par(freq),L=$xdband_par(active_l),voltage=$bc1_xvolt ,phase=\"$bc1_xphs \", tilt=$bc1_xtilt, &
                       n_kicks=$bc1_xkick, GROUP=\"BC1\" &
                       

BC1_XZW_DI        : WAKE, inputfile=\"$xdband_par(wakefile)\", factor=$xdband_par(n_cell), tColumn=\"t\", wColumn=\"Wl\", &
                       interpolate=1, smoothing=1, N_BINS=0,CHANGE_P0=1,allow_long_beam=1

BC1_XTW_DI        : TRWAKE ,inputfile=\"$xdband_par(wakefile)\",factor=$xdband_par(n_cell), tColumn=\"t\",wxColumn=\"Wt\",wyColumn=\"Wt\", &
                       interpolate=1, smoothing=1, N_BINS=0
                       
BC1_XCA_DI        : LINE=(BC1_XCA0_DI,$zwake_flag*BC1_XZW_DI,$twake_flag*BC1_XTW_DI) 

                      
LN1_XCA0       : RFCW, freq=$xband_par(freq),L=$xband_par(active_l),volt=$ln1_xvolt ,phase=\"90 $ln1_xphs -\", change_p0=1, &
                       cell_length=$xband_par(cell_l),end1_focus=1, end2_focus=1, smoothing=1, ZWAKE=$zwake_flag, TRWAKE=$twake_flag, &
                       zwakefile=\"$xband_par(wakefile)\",tColumn=\"t\",wzColumn=\"Wl\",interpolate=1, n_bins=0, &
                       trwakefile=\"$xband_par(wakefile)\",tColumn=\"t\",wxColumn=\"Wt\",wyColumn=\"Wt\", GROUP=\"LN1\" &
                       
LN2_XCA0       : RFCW, freq=$xband_par(freq),L=$xband_par(active_l),volt=$ln2_xvolt ,phase=\"90 $ln2_xphs -\", change_p0=1, &
                       cell_length=$xband_par(cell_l),end1_focus=1, end2_focus=1, smoothing=1, ZWAKE=$zwake_flag, TRWAKE=$twake_flag, &
                       zwakefile=\"$xband_par(wakefile)\",tColumn=\"t\",wzColumn=\"Wl\",interpolate=1, n_bins=0, &
                       trwakefile=\"$xband_par(wakefile)\",tColumn=\"t\",wxColumn=\"Wt\",wyColumn=\"Wt\", GROUP=\"LN2\" &
                     
LN2_SCA0       : RFDF, frequency=$sdband_par(freq),L=$sdband_par(active_l),voltage=$ln2_sdefvolt ,phase=\"$ln2_sdefphs \", &
                       n_kicks=$ln2_sdefkicks, tilt=$ln2_sdeftitl, GROUP=\"LN2\" &

LN2_SZW        : WAKE, inputfile=\"$sdband_par(wakefile)\", factor=$sdband_par(n_cell), tColumn=\"t\", wColumn=\"Wl\", &
                       interpolate=1, smoothing=1, N_BINS=0,CHANGE_P0=1,allow_long_beam=1 

LN2_STW        : TRWAKE ,inputfile=\"$sdband_par(wakefile)\",factor=$sdband_par(n_cell), tColumn=\"t\",wxColumn=\"Wt\",wyColumn=\"Wt\", &
                       interpolate=1, smoothing=1, N_BINS=0
                       
LN2_SCA        : LINE=(LN2_SCA0,$zwake_flag*LN2_SZW,$twake_flag*LN2_STW) 


LN2_XCA0_DI    : RFDF, frequency=$xdband_par(freq),L=$xdband_par(active_l),voltage=$ln2_di_xvolt ,phase=\"$ln2_di_xphs\", &
                        tilt=$ln2_di_xtilt, n_kicks=$ln2_di_xkick, GROUP=\"LN2\" &
                       

LN2_XZW_DI     : WAKE, inputfile=\"$xdband_par(wakefile)\", factor=$xdband_par(n_cell), tColumn=\"t\", wColumn=\"Wl\", &
                       interpolate=1, smoothing=1, N_BINS=0,CHANGE_P0=1,allow_long_beam=1 

LN2_XTW_DI     : TRWAKE ,inputfile=\"$xdband_par(wakefile)\",factor=$xdband_par(n_cell), tColumn=\"t\",wxColumn=\"Wt\",wyColumn=\"Wt\", &
                       interpolate=1, smoothing=1, N_BINS=0
                       
LN2_XCA_DI     : LINE=(LN2_XCA0_DI,$zwake_flag*LN2_XZW_DI,$twake_flag*LN2_XTW_DI) 

LN3_XCA0       : RFCW, freq=$xband_par(freq),L=$xband_par(active_l),volt=$ln3_xvolt ,phase=\"90 $ln3_xphs -\", change_p0=1, &
                       cell_length=$xband_par(cell_l),end1_focus=1, end2_focus=1, smoothing=1, ZWAKE=$zwake_flag, TRWAKE=$twake_flag, &
                       zwakefile=\"$xband_par(wakefile)\",tColumn=\"t\",wzColumn=\"Wl\",interpolate=1, n_bins=0, &
                       trwakefile=\"$xband_par(wakefile)\",tColumn=\"t\",wxColumn=\"Wt\",wyColumn=\"Wt\", GROUP=\"LN3\" &
                       
LN3_SCA0       : RFDF, frequency=$sdband_par(freq),L=$sdband_par(active_l),voltage=$ln3_sdefvolt ,phase=\"$ln2_sdefphs \", &
                       n_kicks=$ln3_sdefkicks, tilt=$ln3_sdeftitl, GROUP=\"LN3\" &
                       
LN3_SZW        : WAKE, inputfile=\"$sdband_par(wakefile)\", factor=$sdband_par(n_cell), tColumn=\"t\", wColumn=\"Wl\", &
                       interpolate=1, smoothing=1, N_BINS=0,CHANGE_P0=1,allow_long_beam=1

LN3_STW        : TRWAKE ,inputfile=\"$sdband_par(wakefile)\",factor=$sdband_par(n_cell), tColumn=\"t\",wxColumn=\"Wt\",wyColumn=\"Wt\", &
                       interpolate=1, smoothing=1, N_BINS=0
                       
LN3_SCA        : LINE=(LN3_SCA0,$zwake_flag*LN3_SZW,$twake_flag*LN3_STW)
                    
LN4_XCA0       : RFCW, freq=$xband_par(freq),L=$xband_par(active_l),volt=$ln4_xvolt ,phase=\"90 $ln4_xphs -\", change_p0=1, &
                       cell_length=$xband_par(cell_l),end1_focus=1, end2_focus=1, smoothing=1, ZWAKE=$zwake_flag, TRWAKE=$twake_flag, &
                       zwakefile=\"$xband_par(wakefile)\",tColumn=\"t\",wzColumn=\"Wl\",interpolate=1, n_bins=0, &
                       trwakefile=\"$xband_par(wakefile)\",tColumn=\"t\",wxColumn=\"Wt\",wyColumn=\"Wt\", GROUP=\"LN4\" &
                       
LN0_DR_CCA_ED : DRIFT,L= $cband_par(edge_l), GROUP=\"LN0\"
LN0_DR_XCA_ED : DRIFT,L= $xband_par(edge_l), GROUP=\"LN0\"
LN0_DR_KCA_ED : DRIFT,L= $kband_par(edge_l), GROUP=\"LN0\"
LN0_DR_XCA_ED_DI   : DRIFT,L= $xdband_par(edge_l), GROUP=\"LN0_DI\"

LN0_DR_CCA0   : DRIFT,L= $cband_par(active_l), GROUP=\"LN0\"
LN0_DR_CCA0H  : DRIFT,L= [expr $cband_par(active_l)*0.5-0.6], GROUP=\"LN0\"
LN0_DR_XCA0   : DRIFT,L= $xband_par(active_l), GROUP=\"LN0\"
LN0_DR_XCA0H  : DRIFT,L= [expr $xband_par(active_l)*0.5], GROUP=\"LN0\"
LN0_DR_KCA0   : DRIFT,L= $kband_par(active_l), GROUP=\"LN0\"

BC1_DR_XCA_ED_DI   : DRIFT,L= $xdband_par(edge_l), GROUP=\"BC1_DI\"
LN2_DR_XCA_ED_DI   : DRIFT,L= $xdband_par(edge_l), GROUP=\"LN2_DI\"


LN1_DR_XCA_ED : DRIFT,L= $xband_par(edge_l), GROUP=\"LN1\"
LN2_DR_XCA_ED : DRIFT,L= $xband_par(edge_l), GROUP=\"LN2\"
LN2_DR_SCA_ED : DRIFT,L= $sdband_par(edge_l), GROUP=\"LN2\"
LN3_DR_XCA_ED : DRIFT,L= $xband_par(edge_l), GROUP=\"LN3\"
LN3_DR_SCA_ED : DRIFT,L= $sdband_par(edge_l), GROUP=\"LN3\"
LN4_DR_XCA_ED : DRIFT,L= $xband_par(edge_l), GROUP=\"LN4\"

LN1_DR_XCA_   : DRIFT,L= $xband_par(active_l), GROUP=\"LN1\"
LN2_DR_XCA_   : DRIFT,L= $xband_par(active_l), GROUP=\"LN2\"
LN2_DR_SCA_   : DRIFT,L= $sdband_par(active_l), GROUP=\"LN2\"
LN3_DR_XCA_   : DRIFT,L= $xband_par(active_l), GROUP=\"LN3\"
LN3_DR_SCA_   : DRIFT,L= $sdband_par(active_l), GROUP=\"LN3\"
LN4_DR_XCA_   : DRIFT,L= $xband_par(active_l), GROUP=\"LN4\"


"


puts $filee "
!------------------------------------------------------------------------------
! Setup linac 0 module
!------------------------------------------------------------------------------
LN0_DR_FL     : DRIFT,L= $flange, GROUP=\"LN0\"
LN0_DR_BL     : DRIFT,L= $belowl, GROUP=\"LN0\"
LN0_DR_SV     : DRIFT,L= $vacpor, GROUP=\"LN0\"
LN0_DR_VS     : DRIFT,L= $viewsc, GROUP=\"LN0\"
LN0_DR_DR     : DRIFT,L= $viewsc, GROUP=\"LN0\"
LN0_DR_CT     : DRIFT,L= $viewsc, GROUP=\"LN0\"
LN0_DR_A      : DRIFT,L= 0.2, GROUP=\"LN0\"

LN0_ACC        : LINE=(LN0_DR_BL,LN0_DR_CCA_ED,LN0_CCA0,LN0_DR_CCA_ED,LN0_DR_BL)
LN0_ACCSW      : LINE=(LN0_DR_SV,LN0_ACC)
LN0_ACCSS      : LINE=(LN0_DR_VS,LN0_ACC)
LN0_ACCSD      : LINE=(LN0_DR_DR,LN0_ACC)
LN0_DR_ACC      : LINE=(LN0_DR_DR,LN0_DR_BL,LN0_DR_CCA_ED,LN0_DR_CCA0H,LN0_DR_A,LN0_DR_VS,LN0_DR_A,LN0_DR_CT)

LN0_DR_LNZ_ED2: DRIFT,L= [expr ($cband_par(total_l)-2.0*$kband_par(total_l)-$belowl)*0.5], GROUP=\"LN0\"
LN0_LNZ_STR   : LINE=(LN0_DR_BL,LN0_DR_KCA_ED,LN0_KCA0,LN0_DR_KCA_ED,LN0_DR_BL,LN0_DR_KCA_ED,LN0_DR_KCA0,LN0_DR_KCA_ED,LN0_DR_BL)
LN0_LNZ_STR2  : LINE=(LN0_DR_BL,LN0_DR_KCA_ED,LN0_KCA0,LN0_DR_KCA_ED,LN0_DR_BL,LN0_DR_KCA_ED,LN0_DR_KCA0,LN0_DR_KCA_ED,LN0_DR_BL)
!LN0_LNZ_STR2  : LINE=(LN0_DR_BL,LN0_DR_KCA_ED,LN0_DR_KCA0,LN0_DR_KCA_ED,LN0_DR_BL,LN0_DR_KCA_ED,LN0_DR_KCA0,LN0_DR_KCA_ED,LN0_DR_BL)

LN0_LNZ       : LINE=(LN0_DR_VS,LN0_DR_LNZ_ED2,LN0_LNZ_STR,LN0_DR_LNZ_ED2)
LN0_LNZ2       : LINE=(LN0_DR_VS,LN0_DR_LNZ_ED2,LN0_LNZ_STR2,LN0_DR_LNZ_ED2)


LN0_DR_QD_ED  : DRIFT,L= $ln0_quad_edge, GROUP=\"LN0\"
LN0_QD_FH     : QUAD, L=[expr $ln0_quad_l*0.5], K1=[expr  1.0*$ln0_quad_k], GROUP=\"LN0\"
LN0_QD_DH     : QUAD, L=[expr $ln0_quad_l*0.5], K1=[expr -1.0*$ln0_quad_k], GROUP=\"LN0\"
LN0_COR       : KICKER, L=0.0,HKICK=0,VKICK=0, GROUP=\"LN0\"
LN0_BPM       : MONI, L=0.0, GROUP=\"LN0\"


LN0_QD_F      : LINE=(LN0_DR_QD_ED,LN0_QD_FH,LN0_COR,LN0_BPM,LN0_QD_FH,LN0_DR_QD_ED)
LN0_QD_D      : LINE=(LN0_DR_QD_ED,LN0_QD_DH,LN0_COR,LN0_BPM,LN0_QD_DH,LN0_DR_QD_ED)
LN0_MODULE    : LINE=(LN0_QD_F, LN0_ACCSW,LN0_QD_D,LN0_ACCSS,LN0_QD_F,LN0_ACCSS,LN0_QD_D,LN0_ACCSS)
LN0_MODULE2    : LINE=(LN0_QD_F, LN0_ACCSS,LN0_QD_D,LN0_ACCSS)
LN0_LNZ_MODULE: LINE=(LN0_QD_F, LN0_LNZ, LN0_QD_D,LN0_DR_ACC)



!------------------------------------------------------------------------------
! Linac 0 laser heater
!------------------------------------------------------------------------------

LN0_DP_DIP1 :  CSRCSBEND, l= $ln0_s_dipole, EDGE_ORDER=2,  ANGLE=-$ln0_angle_rd, E1= 0.0, E2= -$ln0_angle_rd, GROUP=\"LN0\", &
                 N_KICKS=10, INTEGRATION_ORDER=4, BINS=100,SG_HALFWIDTH=2, SG_ORDER=1, STEADY_STATE=0, &
                 CSR=$csr_flag, ISR=$isr_flag,HIGH_FREQUENCY_CUTOFF0=$f0_csr_ln0,HIGH_FREQUENCY_CUTOFF1=$f1_csr_ln0
LN0_DP_DIP2 :  CSRCSBEND, l= $ln0_s_dipole, EDGE_ORDER=2,  ANGLE= $ln0_angle_rd, E1=$ln0_angle_rd, E2=0.0, GROUP=\"LN0\", &
                 N_KICKS=10, INTEGRATION_ORDER=4, BINS=100,SG_HALFWIDTH=2, SG_ORDER=1, STEADY_STATE=0, &
                 CSR=$csr_flag, ISR=$isr_flag,HIGH_FREQUENCY_CUTOFF0=$f0_csr_ln0,HIGH_FREQUENCY_CUTOFF1=$f1_csr_ln0
LN0_DP_DIP3 :  CSRCSBEND, l= $ln0_s_dipole, EDGE_ORDER=2,  ANGLE= $ln0_angle_rd, E1= 0.0, E2=$ln0_angle_rd, GROUP=\"LN0\", &
                 N_KICKS=10, INTEGRATION_ORDER=4, BINS=100,SG_HALFWIDTH=2, SG_ORDER=1, STEADY_STATE=0, &
                 CSR=$csr_flag, ISR=$isr_flag,HIGH_FREQUENCY_CUTOFF0=$f0_csr_ln0,HIGH_FREQUENCY_CUTOFF1=$f1_csr_ln0
LN0_DP_DIP4 :  CSRCSBEND, l= $ln0_s_dipole, EDGE_ORDER=2,  ANGLE= -$ln0_angle_rd, E1= -$ln0_angle_rd, E2= 0.0, GROUP=\"LN0\", &
                 N_KICKS=10, INTEGRATION_ORDER=4, BINS=100,SG_HALFWIDTH=2, SG_ORDER=1, STEADY_STATE=0, &
                 CSR=$csr_flag, ISR=$isr_flag,HIGH_FREQUENCY_CUTOFF0=$f0_csr_ln0,HIGH_FREQUENCY_CUTOFF1=$f1_csr_ln0
"


if { [info exists use_wiggler] } {
    exec cp  $script_dir2/laserProfile.sdds .
puts $filee "
LN0_UN_UND: LSRMDLTR, L = 0.24, BU = $und_fld, PERIODS = 8, LASER_PEAK_POWER = $laser_power,&
           LASER_W0 = 120E-06, N_STEPS = 1000, method=\"non-adaptive runge-kutta\", field_expansion=\"leading terms\",accuracy=1E-03, &
           LASER_WAVELENGTH=$laser_wavel, POLE_FACTOR1=1,POLE_FACTOR2=1,POLE_FACTOR3=1, GROUP=\"LN0\", &
           time_profile=\"laserProfile.sdds=t+A\",time_offset=0e-12
!LN0_UN_UND_sca: scatter,DP=$flag_scatt
!LN0_UN_UND: WIGGLER, L = 0.24, B = 0.4, POLES = 8, GROUP=\"LN0\" &
" } else {
puts $filee "
LN0_UN_UND: DRIFT, L = 0.24,  GROUP=\"LN0\" &
"}

puts $filee "
LN0_DR_30   : DRIFT,L= 0.3, GROUP=\"LN0\"
LN0_DR_20   : DRIFT,L= 0.2, GROUP=\"LN0\"
LN0_DR_20_C   : CSRDRIFT,L= 0.2, GROUP=\"LN0\",N_KICKS= 10, USE_STUPAKOV=1, CSR=$csr_flag
LN0_DR_10   : DRIFT,L= 0.1, GROUP=\"LN0\"
LN0_BPM_LH  : MONI, L=0.0, GROUP=\"LN0\"

LN0_TDS_DR: DRIFT, L = 0.25,  GROUP=\"LN0\" &

LN0_DP_TDS :  SBEND, l= 0.1, ORDER=2, EDGE_ORDER=2, ANGLE=0.0, E1= 0, E2=0.0, GROUP=\"LN0\"

LN0_LH_SEC  :LINE=(LN0_DR_20,LN0_DP_DIP1,LN0_DR_20_C,LN0_DP_DIP2,LN0_DR_20_C,LN0_BPM_LH,LN0_DR_20,LN0_UN_UND, &
                    LN0_DR_20,LN0_BPM_LH,LN0_DR_VS,LN0_DR_10,LN0_DP_DIP3,LN0_DR_20_C,LN0_DP_DIP4,LN0_DR_20_C,LN0_DR_VS)
                    
LN0_TDS       : LINE=(LN0_DR_VS,LN0_DR_BL,LN0_DR_XCA_ED_DI,LN0_XCA_DI,LN0_DR_XCA_ED_DI,LN0_DR_BL,8*LN0_TDS_DR,LN0_DP_TDS,LN0_DR_20,LN0_DR_VS)


"

puts $filee "

!------------------------------------------------------------------------------
! Linac 0 matching quads
!------------------------------------------------------------------------------

LN0_IN_CNT    : CENTER
LN0_QD_V01: QUAD,GROUP=\"LN0\",L=[expr $ln0_quad_l*0.5],K1=-9.346625639045008
LN0_QD_V02: QUAD,GROUP=\"LN0\",L=[expr $ln0_quad_l*0.5],K1=10.23296059706703
LN0_QD_V03: QUAD,GROUP=\"LN0\",L=[expr $ln0_quad_l*0.5],K1=1.298038547655649
LN0_QD_V04: QUAD,GROUP=\"LN0\",L=[expr $ln0_quad_l*0.5],K1=4.08755073927122
LN0_QD_V05: QUAD,GROUP=\"LN0\",L=[expr $ln0_quad_l*0.5],K1=6.418625756834372
LN0_QD_V06: QUAD,GROUP=\"LN0\",L=[expr $ln0_quad_l*0.5],K1=-7.02375048760752
LN0_QD_V07: QUAD,GROUP=\"LN0\",L=[expr $ln0_quad_l*0.5],K1=-1.117533035814508
LN0_DR_V1: DRIF,GROUP=\"LN0\",L=0.2000000014791121
LN0_DR_V2: DRIF,GROUP=\"LN0\",L=0.3573054382012023
LN0_DR_V3: DRIF,GROUP=\"LN0\",L=0.9972431108477009
LN0_DR_V4: DRIF,GROUP=\"LN0\",L=1.421439525410731
LN0_DR_V5: DRIF,GROUP=\"LN0\",L=0.6639433118625861
LN0_DR_V6: DRIF,GROUP=\"LN0\",L=0.2598928927243854
LN0_DR_V7: DRIF,GROUP=\"LN0\",L=1.419750542172655
LN0_DR_V8: DRIF,GROUP=\"LN0\",L=0.2000007602231658
LN0_DR_V9: DRIF,GROUP=\"LN0\",L=0.3946480507774797

LN0_QD_V1   : LINE=(LN0_DR_QD_ED,LN0_QD_V01,LN0_COR,LN0_BPM,LN0_QD_V01,LN0_DR_QD_ED)
LN0_QD_V2   : LINE=(LN0_DR_QD_ED,LN0_QD_V02,LN0_COR,LN0_BPM,LN0_QD_V02,LN0_DR_QD_ED)
LN0_QD_V3   : LINE=(LN0_DR_QD_ED,LN0_QD_V03,LN0_COR,LN0_BPM,LN0_QD_V03,LN0_DR_QD_ED)
LN0_QD_V4   : LINE=(LN0_DR_QD_ED,LN0_QD_V04,LN0_COR,LN0_BPM,LN0_QD_V04,LN0_DR_QD_ED)

LN0_QD_V5   : LINE=(LN0_DR_QD_ED,LN0_QD_V05,LN0_COR,LN0_BPM,LN0_QD_V05,LN0_DR_QD_ED)
LN0_QD_V6   : LINE=(LN0_DR_QD_ED,LN0_QD_V06,LN0_COR,LN0_BPM,LN0_QD_V06,LN0_DR_QD_ED)
LN0_QD_V7   : LINE=(LN0_DR_QD_ED,LN0_QD_V07,LN0_COR,LN0_BPM,LN0_QD_V07,LN0_DR_QD_ED)

LN0_MATCH0    : LINE=(LN0_IN_CNT,LN0_DR_V1,LN0_QD_V1,LN0_DR_V2,LN0_QD_V2,LN0_DR_V3,LN0_QD_V3,LN0_DR_V4)

LN0_MATCH1    : LINE=(LN0_IN_CNT,LN0_DR_V5,LN0_QD_V4,LN0_DR_V6,LN0_QD_V5,LN0_DR_V7,LN0_QD_V6,LN0_DR_V8)

LN0_FITP1     : MARKER, FITPOINT=1
LN0_FITP2     : MARKER, FITPOINT=1
LN0_FITP3     : MARKER, FITPOINT=1
LN0_FITP4     : MARKER, FITPOINT=1
LN0_FITP00    : MARKER, FITPOINT=1
LN0_FITP01     : MARKER, FITPOINT=1

LN0_FITT_1    : LINE=(LN0_DR_QD_ED,LN0_QD_FH, LN0_FITP1, LN0_QD_FH, LN0_DR_QD_ED, LN0_ACCSS)
LN0_FITT_2    : LINE=(LN0_DR_QD_ED,LN0_QD_DH, LN0_FITP2, LN0_QD_DH, LN0_DR_QD_ED, LN0_ACCSS)
LN0_FITT_3    : LINE=(LN0_DR_QD_ED,LN0_QD_FH, LN0_FITP3, LN0_QD_FH, LN0_DR_QD_ED, LN0_ACCSS)
LN0_FITT_4    : LINE=(LN0_DR_QD_ED,LN0_QD_DH, LN0_FITP4, LN0_QD_DH)


INJ_WA_OU      : WATCH,filename=\"inj_in.sdds\",mode=coord, label=\"ln0 in\"
LN0_WA_M_OU    : WATCH,filename=\"ln0_mtch_ou.sdds\",mode=coord, label=\"ln0 m ou \"
LN0_WA_LH_OU   : WATCH,filename=\"ln0_lh_ou.sdds\",mode=coord, label=\"lh out \"
LN0_WA_LNZ_IN  : WATCH,filename=\"ln0_lnz_in.sdds\",mode=coord, label=\"lnz in \"
LN0_WA_OU      : WATCH,filename=\"ln0_ou.sdds\",mode=coord, label=\"ln0 ou \"


LN0_WA_LH_OU2   : WATCH,filename=\"ln0_lh_oua.sdds\",mode=coord, label=\"lh out \"
LN0_WA_LNZ_IN2  : WATCH,filename=\"ln0_lnz_ina.sdds\",mode=coord, label=\"lnz in \"
LN0_WA_OU2      : WATCH,filename=\"ln0_oua.sdds\",mode=coord, label=\"ln0 ou \"


LN0_MATCH_TO  : LINE=(LN0_MATCH0,LN0_FITP00,LN0_LH_SEC,LN0_FITP01,LN0_MATCH1,LN0_TDS,LN0_FITT_1,LN0_FITT_2,LN0_FITT_3,LN0_FITT_4)
LN0_MATCHED   : LINE=(LN0_MATCH0,LN0_LH_SEC,LN0_MATCH1,LN0_TDS,LN0_MODULE,LN0_MODULE2,LN0_LNZ_MODULE)
LN0_MATCHED_TR: LINE=(LN0_MATCH0,LN0_LH_SEC,LN0_WA_LH_OU,LN0_IN_CNT,LN0_MATCH1,LN0_TDS,LN0_MODULE,LN0_MODULE2,LN0_WA_LNZ_IN,LN0_LNZ_MODULE, LN0_WA_OU)

LN0_MATCHED_TR2: LINE=(LN0_MATCH0,LN0_LH_SEC,LN0_WA_LH_OU2,LN0_IN_CNT,LN0_MATCH1,LN0_TDS,LN0_MODULE,LN0_MODULE2,LN0_WA_LNZ_IN2,LN0_LNZ_MODULE, LN0_WA_OU2)


"

puts $filee "

!-----------------------------
! MATCH BC1 CHICANE SECTION
-----------------------------

BC1_QD_V01: QUAD,GROUP=\"BC1\",L=[expr $ln0_quad_l*0.5],K1=1.837410336422465
BC1_QD_V02: QUAD,GROUP=\"BC1\",L=[expr $ln0_quad_l*0.5],K1=-9.146337187547037
BC1_QD_V03: QUAD,GROUP=\"BC1\",L=[expr $ln0_quad_l*0.5],K1=3.214839715336778
BC1_QD_V04: QUAD,GROUP=\"BC1\",L=[expr $ln0_quad_l*0.5],K1=5.283636905364187
BC1_DR_V1: DRIF,GROUP=\"BC1\",L=0.5382066713730278
BC1_DR_V2: DRIF,GROUP=\"BC1\",L=5.900340259610077
BC1_DR_V3: DRIF,GROUP=\"BC1\",L=0.2496128028814724
BC1_DR_V4: DRIF,GROUP=\"BC1\",L=0.2000000160447174
BC1_DR_V5: DRIF,GROUP=\"BC1\",L=0.2002039723245518

BC1_DR_20     : DRIF,L=0.5, GROUP=\"BC1\"
BC1_COR       : KICKER, L=0.0,HKICK=0,VKICK=0, GROUP=\"BC1\"
BC1_BPM       : MONI, L=0.0, GROUP=\"BC1\"
BC1_DR_QD_ED  : DRIFT,L= $ln0_quad_edge, GROUP=\"BC1\"
BC1_DR_FL     : DRIFT,L= $flange, GROUP=\"BC1\"
BC1_DR_BL     : DRIFT,L= $belowl, GROUP=\"BC1\"
BC1_DR_SV     : DRIFT,L= $vacpor, GROUP=\"BC1\"
BC1_DR_VS     : DRIFT,L= $viewsc, GROUP=\"BC1\"
BC1_DR_DR     : DRIFT,L= $viewsc, GROUP=\"BC1\"
BC1_DR_CT     : DRIFT,L= $viewsc, GROUP=\"BC1\"

BC1_QD_V1   : LINE=(BC1_DR_QD_ED,BC1_QD_V01,BC1_COR,BC1_BPM,BC1_QD_V01,BC1_DR_QD_ED)
BC1_QD_V2   : LINE=(BC1_DR_QD_ED,BC1_QD_V02,BC1_COR,BC1_BPM,BC1_QD_V02,BC1_DR_QD_ED)
BC1_QD_V3   : LINE=(BC1_DR_QD_ED,BC1_QD_V03,BC1_COR,BC1_BPM,BC1_QD_V03,BC1_DR_QD_ED)
BC1_QD_V4   : LINE=(BC1_DR_QD_ED,BC1_QD_V04,BC1_COR,BC1_BPM,BC1_QD_V04,BC1_DR_QD_ED)


BC1_DR_SIDE_C: CSRDRIFT,l= 0.5, GROUP=\"BC1\",N_KICKS= 10, USE_STUPAKOV=1, CSR=$csr_flag
BC1_DR_CENT_C: CSRDRIFT,l= [expr $bc1_d_cent*0.5], GROUP=\"BC1\",N_KICKS= 10, USE_STUPAKOV=1, CSR=$csr_flag
BC1_DR_20_C	: CSRDRIFT,L=0.5, GROUP=\"BC1\",N_KICKS= 10, USE_STUPAKOV=1, CSR=$csr_flag

BC1_DR_SIDE: DRIFT,l= [expr $bc1_d_longed-0.5], GROUP=\"BC1\"
BC1_DR_CENT: DRIFT,l= [expr $bc1_d_cent*0.5], GROUP=\"BC1\"


BC1_DP_DIP1 :  CSRCSBEND, l= $bc1_s_dipole, EDGE_ORDER=2,  ANGLE=-$bc1_angle_rd, E1= 00, E2=-$bc1_angle_rd, GROUP=\"BC1\", &
                 N_KICKS=10, INTEGRATION_ORDER=4, BINS=100,SG_HALFWIDTH=2, SG_ORDER=1, STEADY_STATE=0, &
                 CSR=$csr_flag, ISR=$isr_flag,HIGH_FREQUENCY_CUTOFF0=$f0_csr_bc1,HIGH_FREQUENCY_CUTOFF1=$f1_csr_bc1 
BC1_DP_DIP2 :  CSRCSBEND, l= $bc1_s_dipole, EDGE_ORDER=2,  ANGLE= $bc1_angle_rd, E1= $bc1_angle_rd, E2= 0.0, GROUP=\"BC1\", &
                 N_KICKS=10, INTEGRATION_ORDER=4, BINS=100,SG_HALFWIDTH=2, SG_ORDER=1, STEADY_STATE=0, &
                 CSR=$csr_flag, ISR=$isr_flag,HIGH_FREQUENCY_CUTOFF0=$f0_csr_bc1,HIGH_FREQUENCY_CUTOFF1=$f1_csr_bc1
BC1_DP_DIP3 :  CSRCSBEND, l= $bc1_s_dipole, EDGE_ORDER=2,  ANGLE= $bc1_angle_rd, E1= 0.0, E2= $bc1_angle_rd, GROUP=\"BC1\", &
                 N_KICKS=10, INTEGRATION_ORDER=4, BINS=100,SG_HALFWIDTH=2, SG_ORDER=1, STEADY_STATE=0, &
                 CSR=$csr_flag, ISR=$isr_flag,HIGH_FREQUENCY_CUTOFF0=$f0_csr_bc1,HIGH_FREQUENCY_CUTOFF1=$f1_csr_bc1
BC1_DP_DIP4 :  CSRCSBEND, l= $bc1_s_dipole, EDGE_ORDER=2,  ANGLE=-$bc1_angle_rd, E1=-$bc1_angle_rd, E2=00, GROUP=\"BC1\", &
                 N_KICKS=10, INTEGRATION_ORDER=4, BINS=100,SG_HALFWIDTH=2, SG_ORDER=1, STEADY_STATE=0, &
                 CSR=$csr_flag, ISR=$isr_flag,HIGH_FREQUENCY_CUTOFF0=$f0_csr_bc1,HIGH_FREQUENCY_CUTOFF1=$f1_csr_bc1
                 

BC1_CHICANE_L:  LINE=(BC1_DR_20,BC1_DP_DIP1,BC1_DR_SIDE_C,BC1_DR_SIDE,BC1_DP_DIP2,BC1_DR_CENT_C)
BC1_CHICANE_R:  LINE=(BC1_DR_CENT,BC1_DP_DIP3,BC1_DR_SIDE_C,BC1_DR_SIDE,BC1_DP_DIP4,BC1_DR_20_C)

BC1_CHICANE:  LINE=(BC1_DR_20,BC1_DP_DIP1,BC1_DR_SIDE_C,BC1_DR_SIDE,BC1_DP_DIP2,BC1_DR_CENT, BC1_BPM, & 
                    BC1_DR_CENT,BC1_DP_DIP3,BC1_DR_SIDE_C,BC1_DR_SIDE,BC1_DP_DIP4,BC1_DR_20_C)
BC1_IN_CNT    : CENTER
BC1_MATCH_IN	:LINE=(BC1_IN_CNT,BC1_DR_V1,BC1_QD_V1,BC1_DR_V2,BC1_QD_V2,BC1_DR_V3,BC1_QD_V3,BC1_DR_V4,BC1_QD_V4,BC1_DR_V5)
BC1_MATCH_OU	:LINE=(BC1_DR_V5,BC1_QD_V4,BC1_DR_V4,BC1_QD_V3,BC1_DR_V3,BC1_QD_V2,BC1_DR_V2,BC1_QD_V1,BC1_DR_V1)

BC1_WA_OU       : WATCH,filename=\"bc1_ou.sdds\",mode=coord, label=\"bc1 ou \"
BC1_WA_OU2       : WATCH,filename=\"bc1_oua.sdds\",mode=coord, label=\"bc1 ou \"
FITT_BC1_1: MARKER, FITPOINT=1
FITT_BC1_2: MARKER, FITPOINT=1
FITT_BC1_C: MARKER, FITPOINT=1

BC1_MATCH_TO   :LINE=(BC1_MATCH_IN,FITT_BC1_1,BC1_CHICANE_L,FITT_BC1_C,BC1_CHICANE_R,FITT_BC1_2)
BC1_MATCHED    :LINE=(BC1_MATCH_IN,BC1_CHICANE)
BC1_MATCHED_TR :LINE=(BC1_MATCHED,BC1_WA_OU)
BC1_MATCHED_TR2 :LINE=(BC1_MATCHED,BC1_WA_OU2)

"


puts $filee "

!------------------------------------------------------------------------------
! BC1 diagnostic section
!------------------------------------------------------------------------------

BC1_DR_DI_QD_ED  : DRIFT,L= $ln0_quad_edge, GROUP=\"BC1_DI\"
BC1_QD_DI_FH     : QUAD, L=[expr $ln0_quad_l*0.5], K1=[expr  1.0*$bc1_dia_quad_k1], GROUP=\"BC1_DI\"
BC1_QD_DI_DH     : QUAD, L=[expr $ln0_quad_l*0.5], K1=[expr -1.0*$bc1_dia_quad_k1], GROUP=\"BC1_DI\"
BC1_COR_DI       : KICKER, L=0.0,HKICK=0,VKICK=0, GROUP=\"BC1_DI\"
BC1_BPM_DI       : MONI, L=0.0, GROUP=\"BC1_DI\"


BC1_QD_DI_V01: QUAD,GROUP=\"BC1_DI\",L=[expr $ln0_quad_l*0.5],K1=-8.549173824452717
BC1_QD_DI_V02: QUAD,GROUP=\"BC1_DI\",L=[expr $ln0_quad_l*0.5],K1=13.28879455727866
BC1_QD_DI_V03: QUAD,GROUP=\"BC1_DI\",L=[expr $ln0_quad_l*0.5],K1=-5.036878259879915
BC1_QD_DI_V04: QUAD,GROUP=\"BC1_DI\",L=[expr $ln0_quad_l*0.5],K1=4.798484346610981
BC1_DR_DI_V1: DRIF,GROUP=\"BC1_DI\",L=0.2911672376633794
BC1_DR_DI_V2: DRIF,GROUP=\"BC1_DI\",L=0.4700020007651122
BC1_DR_DI_V3: DRIF,GROUP=\"BC1_DI\",L=1.199082800269682
BC1_DR_DI_V4: DRIF,GROUP=\"BC1_DI\",L=2.999999999585806
BC1_DR_DI_V5: DRIF,GROUP=\"BC1_DI\",L=0.2

BC1_QD_DI_V1   : LINE=(BC1_DR_QD_ED,BC1_QD_DI_V01,BC1_COR_DI,BC1_BPM_DI,BC1_QD_DI_V01,BC1_DR_QD_ED)
BC1_QD_DI_V2   : LINE=(BC1_DR_QD_ED,BC1_QD_DI_V02,BC1_COR_DI,BC1_BPM_DI,BC1_QD_DI_V02,BC1_DR_QD_ED)
BC1_QD_DI_V3   : LINE=(BC1_DR_QD_ED,BC1_QD_DI_V03,BC1_COR_DI,BC1_BPM_DI,BC1_QD_DI_V03,BC1_DR_QD_ED)
BC1_QD_DI_V4   : LINE=(BC1_DR_QD_ED,BC1_QD_DI_V04,BC1_COR_DI,BC1_BPM_DI,BC1_QD_DI_V04,BC1_DR_QD_ED)



BC1_QD_DI_F      : LINE=(BC1_DR_DI_QD_ED,BC1_QD_DI_FH,BC1_COR_DI,BC1_BPM_DI,BC1_QD_DI_FH,BC1_DR_DI_QD_ED)
BC1_QD_DI_D      : LINE=(BC1_DR_DI_QD_ED,BC1_QD_DI_DH,BC1_COR_DI,BC1_BPM_DI,BC1_QD_DI_DH,BC1_DR_DI_QD_ED)

BC1_IN_CNT_DI   :CENTER
BC1_MATCH0_DI : LINE=(BC1_IN_CNT_DI,BC1_DR_CT,BC1_DR_DI_V1,BC1_QD_DI_V1,BC1_DR_DI_V2,BC1_QD_DI_V2,BC1_DR_DI_V3,BC1_QD_DI_V3,BC1_DR_DI_V5)

BC1_TDS       : LINE=(BC1_DR_VS,BC1_DR_BL,BC1_DR_XCA_ED_DI,BC1_XCA_DI,BC1_DR_XCA_ED_DI,BC1_DR_BL)

BC1_DR_DI     : DRIF,L=0.25, GROUP=\"BC1_DI\"
BC1_DR_FODO   : DRIF,L=$bc1_dia_fodol, GROUP=\"BC1_DI\"
BC1_DR_DI_VS  : DRIFT,L= $viewsc, GROUP=\"BC1_DI\"


BC1_TDS_LINE  : LINE=(BC1_DR_DI,BC1_TDS,BC1_DR_VS,2*BC1_DR_DI)
BC1_FODO_LINE : LINE=(BC1_QD_DI_F,4*BC1_DR_FODO,BC1_DR_DI_VS,4*BC1_DR_FODO,BC1_QD_DI_D,4*BC1_DR_FODO,BC1_DR_DI_VS,4*BC1_DR_FODO)

BC1_DI_FIT0   : LINE=(BC1_DR_DI_QD_ED,BC1_QD_DI_FH)
BC1_DI_FIT1   : LINE=(BC1_QD_DI_FH,BC1_DR_DI_QD_ED,4*BC1_DR_FODO,BC1_DR_DI_VS,4*BC1_DR_FODO,BC1_DR_DI_QD_ED,BC1_QD_DI_DH)
BC1_DI_FIT2   : LINE=(BC1_QD_DI_DH,BC1_DR_DI_QD_ED,4*BC1_DR_FODO,BC1_DR_DI_VS,4*BC1_DR_FODO,BC1_DR_DI_QD_ED,BC1_QD_DI_FH)
BC1_DI_FIT3   : LINE=(BC1_QD_DI_FH,BC1_DR_DI_QD_ED,4*BC1_DR_FODO,BC1_DR_DI_VS,4*BC1_DR_FODO,BC1_DR_DI_QD_ED,BC1_QD_DI_DH)
BC1_DI_FIT4   : LINE=(BC1_QD_DI_DH,BC1_DR_DI_QD_ED)

BC1_LDR       : LINE=(8*BC1_DR_DI)
BC1_DP_DI     :  SBEND, l= 0.25, EDGE_ORDER=2,  ANGLE= 0, E1= 0, E2= 0, GROUP=\"BC1_DI\"

BC1_LDR2       : LINE=(BC1_DR_DI,BC1_DP_DI,3*BC1_DR_DI)

BC1_FITP1_DI     : MARKER, FITPOINT=1
BC1_FITP2_DI     : MARKER, FITPOINT=1
BC1_FITP3_DI     : MARKER, FITPOINT=1
BC1_FITP4_DI     : MARKER, FITPOINT=1
!BC1_FITT_DI      : LINE=(BC1_FITP1_DI,BC1_TDS,BC1_LDR,BC1_FITP2_DI,BC1_LDR,BC1_LDR,BC1_FITP3_DI)

BC1_FITT_DI      : LINE=(BC1_TDS_LINE,BC1_DI_FIT0,BC1_FITP1_DI,BC1_DI_FIT1,BC1_FITP2_DI,BC1_DI_FIT2,BC1_FITP3_DI,BC1_DI_FIT3,BC1_FITP4_DI)
!BC1_DI           : LINE=(BC1_TDS,BC1_LDR,BC1_LDR,BC1_LDR2)

BC1_DI           : LINE=(BC1_TDS_LINE,2*BC1_FODO_LINE,BC1_QD_DI_F,BC1_LDR2)


BC1_WA_M_OU_DI    : WATCH,filename=\"bc1_dia_mtch_ou.sdds\",mode=coord, label=\"bc1 def in \"
BC1_WA_OU_DI      : WATCH,filename=\"bc1_dia_ou.sdds\",mode=coord, label=\"ln1 in \"
BC1_WA_OU_DI2      : WATCH,filename=\"bc1_dia_oua.sdds\",mode=coord, label=\"ln1 in \"

BC1_DI_MATCH_TO  : LINE=(BC1_MATCH0_DI,BC1_FITT_DI )
BC1_DI_MATCHED   : LINE=(BC1_MATCH0_DI,BC1_DI)
BC1_DI_MATCHED_TR: LINE=(BC1_MATCH0_DI,BC1_DI,BC1_WA_OU_DI)
BC1_DI_MATCHED_TR2: LINE=(BC1_MATCH0_DI,BC1_DI,BC1_WA_OU_DI2)
"

puts $filee "

!------------------------------------------------------------------------------
! Setup linac 1 module
!------------------------------------------------------------------------------

LN1_DR_FL     : DRIFT,L= $flange, GROUP=\"LN1\"
LN1_DR_BL     : DRIFT,L= $belowl, GROUP=\"LN1\"
LN1_DR_SV     : DRIFT,L= $vacpor, GROUP=\"LN1\"
LN1_DR_VS     : DRIFT,L= $viewsc, GROUP=\"LN1\"
LN1_DR_DR     : DRIFT,L= $viewsc, GROUP=\"LN1\"
LN1_DR_CT     : DRIFT,L= $viewsc, GROUP=\"LN1\"

LN1_DR_XCA_A  : DRIFT,L= [expr $xband_par(active_l)-$viewsc], GROUP=\"LN1\"

LN1_ACCSW      : LINE=(LN1_DR_SV,LN1_DR_BL,LN1_DR_XCA_ED,LN1_XCA0,LN1_DR_XCA_ED,LN1_DR_BL,LN1_DR_XCA_ED,LN1_XCA0,LN1_DR_XCA_ED,LN1_DR_BL)
LN1_ACCSC      : LINE=(LN1_DR_VS,LN1_DR_BL,LN1_DR_XCA_ED,LN1_XCA0,LN1_DR_XCA_ED,LN1_DR_BL,LN1_DR_XCA_ED,LN1_XCA0,LN1_DR_XCA_ED,LN1_DR_BL)
LN1_ACCDR      : LINE=(LN1_DR_DR,LN1_DR_BL,LN1_DR_XCA_ED,LN1_DR_XCA_,LN1_DR_XCA_ED,LN1_DR_BL,LN1_DR_XCA_ED,LN1_DR_XCA_,LN1_DR_XCA_ED,LN1_DR_BL)
LN1_DR_ACC     : LINE=(LN1_DR_DR,LN1_DR_BL,LN1_DR_XCA_ED,LN1_DR_XCA_A,LN1_DR_VS)

LN1_DR_QD_ED  : DRIFT,L= $ln1_quad_edge, GROUP=\"LN1\"
LN1_QD_FH     : QUAD, L=[expr $ln1_quad_l*0.5], K1=[expr  1.0*$ln1_quad_k], GROUP=\"LN1\"
LN1_QD_DH     : QUAD, L=[expr $ln1_quad_l*0.5], K1=[expr -1.0*$ln1_quad_k], GROUP=\"LN1\"
LN1_COR       : KICKER, L=0.0,HKICK=0,VKICK=0, GROUP=\"LN1\"
LN1_BPM       : MONI, L=0.0, GROUP=\"LN1\"


LN1_QD_F      : LINE=(LN1_DR_QD_ED,LN1_QD_FH,LN1_COR,LN1_BPM,LN1_QD_FH,LN1_DR_QD_ED)
LN1_QD_D      : LINE=(LN1_DR_QD_ED,LN1_QD_DH,LN1_COR,LN1_BPM,LN1_QD_DH,LN1_DR_QD_ED)
LN1_MODULE    : LINE=(LN1_QD_F, LN1_ACCSW,LN1_QD_D,LN1_ACCSC)


!------------------------------------------------------------------------------
! Linac 1 matching quads
!------------------------------------------------------------------------------

LN1_QD_V01: QUAD,GROUP=\"LN1\",L=[expr $ln1_quad_l*0.5],K1=5.289222146288118
LN1_QD_V02: QUAD,GROUP=\"LN1\",L=[expr $ln1_quad_l*0.5],K1=-10.86165777889886
LN1_QD_V03: QUAD,GROUP=\"LN1\",L=[expr $ln1_quad_l*0.5],K1=1.117375116906607
LN1_QD_V04: QUAD,GROUP=\"LN1\",L=[expr $ln1_quad_l*0.5],K1=-16.14389813034124
LN1_DR_V1: DRIF,GROUP=\"LN1\",L=0.2005299719296311
LN1_DR_V2: DRIF,GROUP=\"LN1\",L=0.2192504339950915
LN1_DR_V3: DRIF,GROUP=\"LN1\",L=1.914617779628016
LN1_DR_V4: DRIF,GROUP=\"LN1\",L=0.2
LN1_DR_V5: DRIF,GROUP=\"LN1\",L=0.7001603960975262

LN1_QD_V1   : LINE=(LN1_DR_QD_ED,LN1_QD_V01,LN1_COR,LN1_BPM,LN1_QD_V01,LN1_DR_QD_ED)
LN1_QD_V2   : LINE=(LN1_DR_QD_ED,LN1_QD_V02,LN1_COR,LN1_BPM,LN1_QD_V02,LN1_DR_QD_ED)
LN1_QD_V3   : LINE=(LN1_DR_QD_ED,LN1_QD_V03,LN1_COR,LN1_BPM,LN1_QD_V03,LN1_DR_QD_ED)
LN1_QD_V4   : LINE=(LN1_DR_QD_ED,LN1_QD_V04,LN1_COR,LN1_BPM,LN1_QD_V04,LN1_DR_QD_ED)
LN1_IN_CNT   :CENTER
!LN1_MATCH0    : LINE=(LN1_IN_CNT,LN1_DR_V1,LN1_QD_V1,LN1_DR_V2,LN1_QD_V2,LN1_DR_V3,LN1_QD_V3,LN1_DR_V4,LN1_QD_V4,LN1_DR_V5)
LN1_MATCH0    : LINE=(LN1_IN_CNT,LN1_DR_V1,LN1_QD_V1,LN1_DR_V2,LN1_QD_V2,LN1_DR_V3,LN1_QD_V3,LN1_DR_V4)

LN1_FITP1     : MARKER, FITPOINT=1
LN1_FITP2     : MARKER, FITPOINT=1
LN1_FITP3     : MARKER, FITPOINT=1
LN1_FITP4     : MARKER, FITPOINT=1

LN1_FITT_1    : LINE=(LN1_DR_QD_ED,LN1_QD_FH, LN1_FITP1, LN1_QD_FH, LN1_DR_QD_ED, LN1_ACCSW)
LN1_FITT_2    : LINE=(LN1_DR_QD_ED,LN1_QD_DH, LN1_FITP2, LN1_QD_DH, LN1_DR_QD_ED, LN1_ACCSW)
LN1_FITT_3    : LINE=(LN1_DR_QD_ED,LN1_QD_FH, LN1_FITP3, LN1_QD_FH, LN1_DR_QD_ED, LN1_ACCSW)
LN1_FITT_4    : LINE=(LN1_DR_QD_ED,LN1_QD_DH, LN1_FITP4, LN1_QD_DH)

LN1_WA_M_OU    : WATCH,filename=\"ln1_mtch_ou.sdds\",mode=coord, label=\"ln1 m ou \"
LN1_WA_OU      : WATCH,filename=\"ln1_ou.sdds\",mode=coord, label=\"ln1 ou \"
LN1_WA_OU2      : WATCH,filename=\"ln1_oua.sdds\",mode=coord, label=\"ln1 ou \"
LN1_MATCH_TO  : LINE=(LN1_MATCH0,LN1_FITT_1,LN1_FITT_2,LN1_FITT_3,LN1_FITT_4 )
LN1_MATCHED   : LINE=(LN1_MATCH0,$ln1_nmodule*LN1_MODULE,LN1_QD_F,LN1_ACCDR,LN1_QD_D,LN1_DR_ACC)
LN1_MATCHED_TR: LINE=(LN1_MATCH0,$ln1_nmodule*LN1_MODULE,,LN1_QD_F,LN1_ACCDR,LN1_QD_D,LN1_DR_ACC,LN1_WA_OU)
LN1_MATCHED_TR2: LINE=(LN1_MATCH0,$ln1_nmodule*LN1_MODULE,,LN1_QD_F,LN1_ACCDR,LN1_QD_D,LN1_DR_ACC,LN1_WA_OU2)
"


puts $filee "


!-----------------------------
! MATCH BC2 CHICANE SECTION
!-----------------------------

BC2_DR_20	: DRIF,L=0.5

BC2_COR       : KICKER, L=0.0,HKICK=0,VKICK=0, GROUP=\"BC2\"
BC2_BPM       : MONI, L=0.0, GROUP=\"BC2\"
BC2_DR_QD_ED  : DRIFT,L= $ln1_quad_edge, GROUP=\"BC2\"
BC2_DR_FL     : DRIFT,L= $flange, GROUP=\"BC2\"
BC2_DR_BL     : DRIFT,L= $belowl, GROUP=\"BC2\"
BC2_DR_SV     : DRIFT,L= $vacpor, GROUP=\"BC2\"
BC2_DR_VS     : DRIFT,L= $viewsc, GROUP=\"BC2\"
BC2_DR_DR     : DRIFT,L= $viewsc, GROUP=\"BC2\"

BC2_QD_V01: QUAD,GROUP=\"BC2\",L=[expr $ln1_quad_l*0.5],K1=0.3860380892581589
BC2_QD_V02: QUAD,GROUP=\"BC2\",L=[expr $ln1_quad_l*0.5],K1=-11.62308959734747
BC2_QD_V03: QUAD,GROUP=\"BC2\",L=[expr $ln1_quad_l*0.5],K1=10.77216995778987
BC2_QD_V04: QUAD,GROUP=\"BC2\",L=[expr $ln1_quad_l*0.5],K1=10
BC2_DR_V1: DRIF,GROUP=\"BC2\",L=0.471866672144182
BC2_DR_V2: DRIF,GROUP=\"BC2\",L=5.872422594778211
BC2_DR_V3: DRIF,GROUP=\"BC2\",L=0.2000000010107992
BC2_DR_V4: DRIF,GROUP=\"BC2\",L=0.2000000000000056
BC2_DR_V5: DRIF,GROUP=\"BC2\",L=0.3

BC2_QD_V1   : LINE=(BC2_DR_QD_ED,BC2_QD_V01,BC2_COR,BC2_BPM,BC2_QD_V01,BC2_DR_QD_ED)
BC2_QD_V2   : LINE=(BC2_DR_QD_ED,BC2_QD_V02,BC2_COR,BC2_BPM,BC2_QD_V02,BC2_DR_QD_ED)
BC2_QD_V3   : LINE=(BC2_DR_QD_ED,BC2_QD_V03,BC2_COR,BC2_BPM,BC2_QD_V03,BC2_DR_QD_ED)
BC2_QD_V4   : LINE=(BC2_DR_QD_ED,BC2_QD_V04,BC2_COR,BC2_BPM,BC2_QD_V04,BC2_DR_QD_ED)


BC2_DR_SIDE_C: CSRDRIFT,l= 0.5,GROUP=\"BC2\",N_KICKS= 10,USE_STUPAKOV=1, CSR=$csr_flag
BC2_DR_CENT_C: CSRDRIFT,l= [expr $bc2_d_cent*0.5],GROUP=\"BC2\",N_KICKS= 10,USE_STUPAKOV=1, CSR=$csr_flag
BC2_DR_20_C	: CSRDRIFT,L=0.5,N_KICKS= 10, USE_STUPAKOV=1, CSR=$csr_flag

BC2_DR_SIDE: DRIFT,l= [expr $bc2_d_longed-0.5],GROUP=\"BC2\"
BC2_DR_CENT: DRIFT,l= [expr $bc2_d_cent*0.5],GROUP=\"BC2\"

BC2_DP_DIP1 :  CSRCSBEND, l= $bc2_s_dipole, EDGE_ORDER=2,  ANGLE=-$bc2_angle_rd, E1=-$bc2_angle_zr, E2=-$bc2_angle_rd, &
                 N_KICKS=10, INTEGRATION_ORDER=4, BINS=50,SG_HALFWIDTH=2, SG_ORDER=1, STEADY_STATE=0, &
                 CSR=$csr_flag, ISR=$isr_flag,HIGH_FREQUENCY_CUTOFF0=$f0_csr_bc2,HIGH_FREQUENCY_CUTOFF1=$f1_csr_bc2 
BC2_DP_DIP2 :  CSRCSBEND, l= $bc2_s_dipole, EDGE_ORDER=2,  ANGLE= $bc2_angle_rd, E1= $bc2_angle_rd, E2= $bc2_angle_zr, &
                 N_KICKS=10, INTEGRATION_ORDER=4, BINS=50,SG_HALFWIDTH=2, SG_ORDER=1, STEADY_STATE=0, &
                 CSR=$csr_flag, ISR=$isr_flag,HIGH_FREQUENCY_CUTOFF0=$f0_csr_bc2,HIGH_FREQUENCY_CUTOFF1=$f1_csr_bc2
BC2_DP_DIP3 :  CSRCSBEND, l= $bc2_s_dipole, EDGE_ORDER=2,  ANGLE= $bc2_angle_rd, E1= $bc2_angle_zr, E2= $bc2_angle_rd, &
                 N_KICKS=10, INTEGRATION_ORDER=4, BINS=50,SG_HALFWIDTH=2, SG_ORDER=1, STEADY_STATE=0, &
                 CSR=$csr_flag, ISR=$isr_flag,HIGH_FREQUENCY_CUTOFF0=$f0_csr_bc2,HIGH_FREQUENCY_CUTOFF1=$f1_csr_bc2
BC2_DP_DIP4 :  CSRCSBEND, l= $bc2_s_dipole, EDGE_ORDER=2,  ANGLE=-$bc2_angle_rd, E1=-$bc2_angle_rd, E2=-$bc2_angle_zr, &
                 N_KICKS=10, INTEGRATION_ORDER=4, BINS=50,SG_HALFWIDTH=2, SG_ORDER=1, STEADY_STATE=0, &
                 CSR=$csr_flag, ISR=$isr_flag,HIGH_FREQUENCY_CUTOFF0=$f0_csr_bc2,HIGH_FREQUENCY_CUTOFF1=$f1_csr_bc2 
                

BC2_CHICANE_L:  LINE=(BC2_DR_20,BC2_DP_DIP1,BC2_DR_SIDE_C,BC2_DR_SIDE,BC2_DP_DIP2,BC2_DR_CENT_C,BC2_DR_CENT,BC2_DP_DIP3)
BC2_CHICANE_R:  LINE=(BC2_DR_SIDE_C,BC2_DR_SIDE,BC2_DP_DIP4)

BC2_CHICANE:  LINE=(BC2_DR_20,BC2_DP_DIP1,BC2_DR_SIDE_C,BC2_DR_SIDE,BC2_DP_DIP2,BC2_DR_CENT_C,BC2_BPM,&
                    BC2_DR_CENT,BC2_DP_DIP3,BC2_DR_SIDE_C,BC2_DR_SIDE,BC2_DP_DIP4)
BC2_IN_CNT   :CENTER
!BC2_MATCH_IN	:LINE=(BC2_IN_CNT,BC2_DR_V1,BC2_QD_V1,BC2_DR_V2,BC2_QD_V2,BC2_DR_V3,BC2_QD_V3,BC2_DR_V4,BC2_QD_V4,BC2_DR_V5)
BC2_MATCH_IN	:LINE=(BC2_IN_CNT,BC2_DR_V1,BC2_QD_V1,BC2_DR_V2,BC2_QD_V2,BC2_DR_V3,BC2_QD_V3,BC2_DR_V4)
BC2_MATCH_OU	:LINE=(BC2_DR_V4,BC2_QD_V3,BC2_DR_V3,BC2_QD_V2,BC2_DR_V2,BC2_QD_V1,BC2_DR_V1)

BC2_WA_OU:       WATCH,filename=\"bc2_ou.sdds\",mode=coord, label=\"bc2 ou \"
BC2_WA_OU2:       WATCH,filename=\"bc2_oua.sdds\",mode=coord, label=\"bc2 ou \"

FITT_BC2_1: MARKER, FITPOINT=1
FITT_BC2_2: MARKER, FITPOINT=1
FITT_BC2_C: MARKER, FITPOINT=1


BC2_MATCH_TO   :LINE=(BC2_MATCH_IN,FITT_BC2_1,BC2_CHICANE_L,FITT_BC2_C,BC2_CHICANE_R,FITT_BC2_2)
BC2_MATCHED    :LINE=(BC2_MATCH_IN,BC2_CHICANE,BC2_IN_CNT,BC2_DR_20_C)
BC2_MATCHED_TR :LINE=(BC2_MATCHED,BC2_IN_CNT,BC2_WA_OU)
BC2_MATCHED_TR2 :LINE=(BC2_MATCHED,BC2_IN_CNT,BC2_WA_OU2)

"



puts $filee "

!------------------------------------------------------------------------------
! Setup linac 2 module
!------------------------------------------------------------------------------

LN2_DR_FL     : DRIFT,L= $flange, GROUP=\"LN2\"
LN2_DR_BL     : DRIFT,L= $belowl, GROUP=\"LN2\"
LN2_DR_SV     : DRIFT,L= $vacpor, GROUP=\"LN2\"
LN2_DR_VS     : DRIFT,L= $viewsc, GROUP=\"LN2\"
LN2_DR_DR     : DRIFT,L= $viewsc, GROUP=\"LN2\"
LN2_DR_CT     : DRIFT,L= $viewsc, GROUP=\"LN2\"

LN2_ACCSW      : LINE=(LN2_DR_SV,LN2_DR_BL,LN2_DR_XCA_ED,LN2_XCA0,LN2_DR_XCA_ED,LN2_DR_BL,LN2_DR_XCA_ED,LN2_XCA0,LN2_DR_XCA_ED,LN2_DR_BL)
LN2_ACCSC      : LINE=(LN2_DR_VS,LN2_DR_BL,LN2_DR_XCA_ED,LN2_XCA0,LN2_DR_XCA_ED,LN2_DR_BL,LN2_DR_XCA_ED,LN2_XCA0,LN2_DR_XCA_ED,LN2_DR_BL)
LN2_DR_ACC     : LINE=(LN2_DR_DR,LN2_DR_BL,LN2_DR_XCA_ED, LN2_DR_XCA_)

LN2_DR_QD_ED  : DRIFT,L= $ln2_quad_edge, GROUP=\"LN2\"
LN2_QD_FH     : QUAD, L=[expr $ln2_quad_l*0.5], K1=[expr  1.0*$ln2_quad_k], GROUP=\"LN2\"
LN2_QD_DH     : QUAD, L=[expr $ln2_quad_l*0.5], K1=[expr -1.0*$ln2_quad_k], GROUP=\"LN2\"
LN2_COR       : KICKER, L=0.0,HKICK=0,VKICK=0, GROUP=\"LN2\"
LN2_BPM       : MONI, L=0.0, GROUP=\"LN2\"


LN2_QD_F      : LINE=(LN2_DR_QD_ED,LN2_QD_FH,LN2_COR,LN2_BPM,LN2_QD_FH,LN2_DR_QD_ED)
LN2_QD_D      : LINE=(LN2_DR_QD_ED,LN2_QD_DH,LN2_COR,LN2_BPM,LN2_QD_DH,LN2_DR_QD_ED)
LN2_MODULE    : LINE=(LN2_QD_F, LN2_ACCSW,LN2_QD_D,LN2_ACCSC)


!------------------------------------------------------------------------------
! Linac 1 matching quads
!------------------------------------------------------------------------------

LN2_QD_V01: QUAD,GROUP=\"LN2\",L=[expr $ln2_quad_l*0.5],K1=13.78534131238308
LN2_QD_V02: QUAD,GROUP=\"LN2\",L=[expr $ln2_quad_l*0.5],K1=-13.13562789693326
LN2_QD_V03: QUAD,GROUP=\"LN2\",L=[expr $ln2_quad_l*0.5],K1=9.179471794269439
LN2_QD_V04: QUAD,GROUP=\"LN2\",L=[expr $ln2_quad_l*0.5],K1=-16.14389813034124
LN2_DR_V1: DRIF,GROUP=\"LN2\",L=2.294829669314446
LN2_DR_V2: DRIF,GROUP=\"LN2\",L=0.5090890647731466
LN2_DR_V3: DRIF,GROUP=\"LN2\",L=1.490463532320567
LN2_DR_V4: DRIF,GROUP=\"LN2\",L=2.999939327234165
LN2_DR_V5: DRIF,GROUP=\"LN2\",L=0.7001603960975262

LN2_QD_V1   : LINE=(LN2_DR_QD_ED,LN2_QD_V01,LN2_COR,LN2_BPM,LN2_QD_V01,LN2_DR_QD_ED)
LN2_QD_V2   : LINE=(LN2_DR_QD_ED,LN2_QD_V02,LN2_COR,LN2_BPM,LN2_QD_V02,LN2_DR_QD_ED)
LN2_QD_V3   : LINE=(LN2_DR_QD_ED,LN2_QD_V03,LN2_COR,LN2_BPM,LN2_QD_V03,LN2_DR_QD_ED)
LN2_QD_V4   : LINE=(LN2_DR_QD_ED,LN2_QD_V04,LN2_COR,LN2_BPM,LN2_QD_V04,LN2_DR_QD_ED)

LN2_IN_CNT   :CENTER
!LN2_MATCH0    : LINE=(LN2_IN_CNT,LN2_DR_V1,LN2_QD_V1,LN2_DR_V2,LN2_QD_V2,LN2_DR_V3,LN2_QD_V3,LN2_DR_V4,LN2_QD_V4,LN2_DR_V5)
LN2_MATCH0    : LINE=(LN2_IN_CNT,LN2_DR_CT,LN2_DR_V1,LN2_QD_V1,LN2_DR_V2,LN2_QD_V2,LN2_DR_V3,LN2_QD_V3,LN2_DR_V4,LN2_DR_VS)

LN2_FITP1     : MARKER, FITPOINT=1
LN2_FITP2     : MARKER, FITPOINT=1
LN2_FITP3     : MARKER, FITPOINT=1
LN2_FITP4     : MARKER, FITPOINT=1

LN2_FITT_1    : LINE=(LN2_DR_QD_ED,LN2_QD_FH, LN2_FITP1, LN2_QD_FH, LN2_DR_QD_ED, LN2_ACCSW)
LN2_FITT_2    : LINE=(LN2_DR_QD_ED,LN2_QD_DH, LN2_FITP2, LN2_QD_DH, LN2_DR_QD_ED, LN2_ACCSW)
LN2_FITT_3    : LINE=(LN2_DR_QD_ED,LN2_QD_FH, LN2_FITP3, LN2_QD_FH, LN2_DR_QD_ED, LN2_ACCSW)
LN2_FITT_4    : LINE=(LN2_DR_QD_ED,LN2_QD_DH, LN2_FITP4, LN2_QD_DH)

LN2_WA_M_OU    : WATCH,filename=\"ln2_mtch_ou.sdds\",mode=coord, label=\"ln2 m ou \"
LN2_WA_OU      : WATCH,filename=\"ln2_ou.sdds\",mode=coord, label=\"ln2 ou \"
LN2_WA_OU2      : WATCH,filename=\"ln2_oua.sdds\",mode=coord, label=\"ln2 ou \"

LN2_MATCH_TO  : LINE=(LN2_MATCH0,LN2_FITT_1,LN2_FITT_2,LN2_FITT_3,LN2_FITT_4 )
LN2_MATCHED   : LINE=(LN2_MATCH0,$ln2_nmodule*LN2_MODULE,LN2_QD_F)
LN2_MATCHED_TR: LINE=(LN2_MATCH0,$ln2_nmodule*LN2_MODULE,LN2_QD_F,LN2_WA_OU)
LN2_MATCHED_TR2: LINE=(LN2_MATCH0,$ln2_nmodule*LN2_MODULE,LN2_QD_F,LN2_WA_OU2)
"


puts $filee "

!------------------------------------------------------------------------------
! LN2 diagnostic section
!------------------------------------------------------------------------------

LN2_DR_DI_QD_ED  : DRIFT,L= $ln0_quad_edge, GROUP=\"LN2_DI\"
LN2_QD_DI_FH     : QUAD, L=[expr $ln2_quad_l*0.5], K1=[expr  1.0*$ln2_dia_quad_k1], GROUP=\"LN2_DI\"
LN2_QD_DI_DH     : QUAD, L=[expr $ln2_quad_l*0.5], K1=[expr -1.0*$ln2_dia_quad_k1], GROUP=\"LN2_DI\"
LN2_COR_DI       : KICKER, L=0.0,HKICK=0,VKICK=0, GROUP=\"LN2_DI\"
LN2_BPM_DI       : MONI, L=0.0, GROUP=\"LN2_DI\"


LN2_DR_FL_DI     : DRIFT,L= $flange, GROUP=\"BC1\"
LN2_DR_BL_DI     : DRIFT,L= $belowl, GROUP=\"BC1\"
LN2_DR_SV_DI     : DRIFT,L= $vacpor, GROUP=\"BC1\"
LN2_DR_VS_DI     : DRIFT,L= $viewsc, GROUP=\"BC1\"
LN2_DR_DR_DI     : DRIFT,L= $viewsc, GROUP=\"BC1\"

LN2_QD_DI_V01: QUAD,GROUP=\"LN2_DI\",L=[expr $ln2_quad_l*0.5],K1=-2.652342266813625
LN2_QD_DI_V02: QUAD,GROUP=\"LN2_DI\",L=[expr $ln2_quad_l*0.5],K1=10.17418600662849
LN2_QD_DI_V03: QUAD,GROUP=\"LN2_DI\",L=[expr $ln2_quad_l*0.5],K1=-12.49200898249094
LN2_QD_DI_V04: QUAD,GROUP=\"LN2_DI\",L=[expr $ln2_quad_l*0.5],K1=-4.798484346610981
LN2_DR_DI_V1: DRIF,GROUP=\"LN2_DI\",L=1.161052290328007
LN2_DR_DI_V2: DRIF,GROUP=\"LN2_DI\",L=0.8279980102478744
LN2_DR_DI_V3: DRIF,GROUP=\"LN2_DI\",L=0.2001539170651899
LN2_DR_DI_V4: DRIF,GROUP=\"LN2_DI\",L=2.999999999585806
LN2_DR_DI_V5: DRIF,GROUP=\"LN2_DI\",L=0.2

LN2_QD_DI_V1   : LINE=(LN2_DR_QD_ED,LN2_QD_DI_V01,LN2_COR_DI,LN2_BPM_DI,LN2_QD_DI_V01,LN2_DR_QD_ED)
LN2_QD_DI_V2   : LINE=(LN2_DR_QD_ED,LN2_QD_DI_V02,LN2_COR_DI,LN2_BPM_DI,LN2_QD_DI_V02,LN2_DR_QD_ED)
LN2_QD_DI_V3   : LINE=(LN2_DR_QD_ED,LN2_QD_DI_V03,LN2_COR_DI,LN2_BPM_DI,LN2_QD_DI_V03,LN2_DR_QD_ED)
LN2_QD_DI_V4   : LINE=(LN2_DR_QD_ED,LN2_QD_DI_V04,LN2_COR_DI,LN2_BPM_DI,LN2_QD_DI_V04,LN2_DR_QD_ED)



LN2_QD_DI_F      : LINE=(LN2_DR_DI_QD_ED,LN2_QD_DI_FH,LN2_COR_DI,LN2_BPM_DI,LN2_QD_DI_FH,LN2_DR_DI_QD_ED)
LN2_QD_DI_D      : LINE=(LN2_DR_DI_QD_ED,LN2_QD_DI_DH,LN2_COR_DI,LN2_BPM_DI,LN2_QD_DI_DH,LN2_DR_DI_QD_ED)

LN2_IN_CNT_DI   :CENTER
LN2_MATCH0_DI : LINE=(LN2_IN_CNT_DI,LN2_DR_DI_V1,LN2_QD_DI_V1,LN2_DR_DI_V2,LN2_QD_DI_V2,LN2_DR_DI_V3,LN2_QD_DI_V3,LN2_DR_DI_V5)

LN2_TDS       : LINE=(LN2_DR_VS_DI,LN2_DR_BL_DI,LN2_DR_XCA_ED_DI,LN2_XCA_DI,LN2_DR_XCA_ED_DI,LN2_DR_BL_DI)



LN2_DR_DI     : DRIF,L=0.25, GROUP=\"LN2_DI\"
LN2_DR_FODO   : DRIF,L=$ln2_dia_fodol, GROUP=\"LN2_DI\"
LN2_DR_DI_VS  : DRIFT,L= $viewsc, GROUP=\"LN2_DI\"


LN2_TDS_LINE  : LINE=(LN2_DR_VS,LN2_DR_DI,LN2_TDS,2*LN2_DR_DI)
LN2_FODO_LINE : LINE=(LN2_QD_DI_F,4*LN2_DR_FODO,LN2_DR_DI_VS,4*LN2_DR_FODO,LN2_QD_DI_D,4*LN2_DR_FODO,LN2_DR_DI_VS,4*LN2_DR_FODO)
LN2_FODO_LINE2 : LINE=(LN2_QD_DI_F,4*LN2_DR_FODO,LN2_DR_DI_VS,4*LN2_DR_FODO,LN2_QD_DI_D,4*LN2_DR_FODO)

LN2_DI_FIT0   : LINE=(LN2_DR_DI_QD_ED,LN2_QD_DI_FH)
LN2_DI_FIT1   : LINE=(LN2_QD_DI_FH,LN2_DR_DI_QD_ED,4*LN2_DR_FODO,LN2_DR_DI_VS,4*LN2_DR_FODO,LN2_DR_DI_QD_ED,LN2_QD_DI_DH)
LN2_DI_FIT2   : LINE=(LN2_QD_DI_DH,LN2_DR_DI_QD_ED,4*LN2_DR_FODO,LN2_DR_DI_VS,4*LN2_DR_FODO,LN2_DR_DI_QD_ED,LN2_QD_DI_FH)
LN2_DI_FIT3   : LINE=(LN2_QD_DI_FH,LN2_DR_DI_QD_ED,4*LN2_DR_FODO,LN2_DR_DI_VS,4*LN2_DR_FODO,LN2_DR_DI_QD_ED,LN2_QD_DI_DH)
LN2_DI_FIT4   : LINE=(LN2_QD_DI_DH,LN2_DR_DI_QD_ED)

LN2_LDR       : LINE=(8*LN2_DR_DI)
LN2_DP_DI     :  SBEND, l= $ln2_dia_fodol, EDGE_ORDER=2,  ANGLE= $ln2_di_dp_angle, E1= 0, E2= 0, GROUP=\"LN2_DI\"

!LN2_LDR2       : LINE=(LN2_DR_DI,LN2_DP_DI,3*LN2_DR_DI)
LN2_LDR2       : LINE=(4*LN2_DR_FODO,LN2_DR_DI_VS,LN2_DP_DI,3*LN2_DR_FODO)

LN2_FITP1_DI     : MARKER, FITPOINT=1
LN2_FITP2_DI     : MARKER, FITPOINT=1
LN2_FITP3_DI     : MARKER, FITPOINT=1
LN2_FITP4_DI     : MARKER, FITPOINT=1
!LN2_FITT_DI      : LINE=(LN2_FITP1_DI,LN2_TDS,LN2_LDR,LN2_FITP2_DI,LN2_LDR,LN2_LDR,LN2_FITP3_DI)

LN2_FITT_DI      : LINE=(LN2_TDS_LINE,LN2_DI_FIT0,LN2_FITP1_DI,LN2_DI_FIT1,LN2_FITP2_DI,LN2_DI_FIT2,LN2_FITP3_DI,LN2_DI_FIT3,LN2_FITP4_DI)
!LN2_DI           : LINE=(LN2_TDS,LN2_LDR,LN2_LDR,LN2_LDR2)

LN2_DI           : LINE=(LN2_TDS_LINE,2*LN2_FODO_LINE,LN2_QD_DI_F,LN2_LDR2,LN2_QD_DI_D,LN2_DR_DI)


LN2_WA_OU_DI      : WATCH,filename=\"ln2_di_ou.sdds\",mode=coord, label=\"ln2 dia out \"
LN2_WA_OU_DI2      : WATCH,filename=\"ln2_di_oua.sdds\",mode=coord, label=\"ln2 dia out \"

LN2_DI_MATCH_TO  : LINE=(LN2_MATCH0_DI,LN2_FITT_DI )
LN2_DI_MATCHED   : LINE=(LN2_MATCH0_DI,LN2_DI)
LN2_DI_MATCHED_TR: LINE=(LN2_MATCH0_DI,LN2_DI,LN2_WA_OU_DI)
LN2_DI_MATCHED_TR2: LINE=(LN2_MATCH0_DI,LN2_DI,LN2_WA_OU_DI2)
"



puts $filee "

!------------------------------------------------------------------------------
! Setup SXR SPREADER to LN4
!------------------------------------------------------------------------------

LN2_DR_BP_FL     : DRIFT,L= $flange, GROUP=\"LN2_BP\"
LN2_DR_BP_BL     : DRIFT,L= $belowl, GROUP=\"LN2_BP\"
LN2_DR_BP_SV     : DRIFT,L= $vacpor, GROUP=\"LN2_BP\"
LN2_DR_BP_VS     : DRIFT,L= $viewsc, GROUP=\"LN2_BP\"
LN2_DR_BP_DR     : DRIFT,L= $viewsc, GROUP=\"LN2_BP\"


LN2_QD_BP_V01: QUAD,GROUP=\"LN2_BP\",L=[expr $ln2_quad_l*0.5],K1=-1.415038060498936
LN2_QD_BP_V02: QUAD,GROUP=\"LN2_BP\",L=[expr $ln2_quad_l*0.5],K1=13.99990729966754
LN2_QD_BP_V03: QUAD,GROUP=\"LN2_BP\",L=[expr $ln2_quad_l*0.5],K1=-13.54832048422707
LN2_QD_BP_V04: QUAD,GROUP=\"LN2_BP\",L=[expr $ln2_quad_l*0.5],K1=5.14389813034124
LN2_DR_BP_V1: DRIF,GROUP=\"LN2_BP\",L=3.697136884640986
LN2_DR_BP_V2: DRIF,GROUP=\"LN2_BP\",L=3.593261919988714
LN2_DR_BP_V3: DRIF,GROUP=\"LN2_BP\",L=0.2000567527307129
LN2_DR_BP_V4: DRIF,GROUP=\"LN2_BP\",L=0.275258421695094
LN2_DR_BP_V5: DRIF,GROUP=\"LN2_BP\",L=0.7001603960975262

LN2_QD_BP_V1   : LINE=(LN2_DR_QD_ED,LN2_QD_BP_V01,LN2_COR,LN2_BPM,LN2_QD_BP_V01,LN2_DR_QD_ED)
LN2_QD_BP_V2   : LINE=(LN2_DR_QD_ED,LN2_QD_BP_V02,LN2_COR,LN2_BPM,LN2_QD_BP_V02,LN2_DR_QD_ED)
LN2_QD_BP_V3   : LINE=(LN2_DR_QD_ED,LN2_QD_BP_V03,LN2_COR,LN2_BPM,LN2_QD_BP_V03,LN2_DR_QD_ED)
LN2_QD_BP_V4   : LINE=(LN2_DR_QD_ED,LN2_QD_BP_V04,LN2_COR,LN2_BPM,LN2_QD_BP_V04,LN2_DR_QD_ED)

LN2_IN_CNT_BP   :CENTER

LN2_BP_MATCH0    : LINE=(LN2_IN_CNT_BP,LN2_DR_BP_V1,LN2_QD_BP_V1,LN2_DR_BP_V2,LN2_QD_BP_V2,LN2_DR_BP_V3,LN2_QD_BP_V3,LN2_DR_BP_V4)

LN2_DR_BP_0    : DRIF,GROUP=\"LN2_BP\",L=0.2
LN2_DR_BP      : DRIF,GROUP=\"LN2_BP\",L=$ln2_swi_trip_dir


LN2_BP_SDEF     : LINE=(LN2_DR_BP_0,LN2_DR_BP_BL,LN2_DR_SCA_ED,LN2_SCA,LN2_DR_SCA_ED,LN2_DR_BP_BL,LN2_DR_BP_0)

LN2_DP_SEPM     : DRIF, L= [expr $sbp_swi_l_dip],GROUP=\"LN2_BP\"

LN2_BP_FITP1     : MARKER, FITPOINT=1
LN2_BP_FITP2     : MARKER, FITPOINT=1
LN2_BP_FITP3     : MARKER, FITPOINT=1
LN2_BP_FITP4     : MARKER, FITPOINT=1

LN2_BP_WA_OU      : WATCH,filename=\"ln2_bp_ou.sdds\",mode=coord, label=\"ln2 bp ou \"
LN2_BP_WA_OU2      : WATCH,filename=\"ln2_bp_oua.sdds\",mode=coord, label=\"ln2 bp ou \"


LN2_BP_LINE     :LINE=(LN2_BP_SDEF,LN2_DR_BP,LN2_DR_BP_VS,LN2_DR_BP)
LN2_BP_LINE2    :LINE=(LN2_DP_SEPM,5*LN2_DR_BP,LN2_DR_BP_VS,LN2_DR_BP)

LN2_BP_MATCH_TO  :LINE=(LN2_BP_MATCH0,LN2_BP_FITP1,LN2_BP_LINE, LN2_BP_FITP2 )
LN2_BP_MATCHED   : LINE=(LN2_BP_MATCH0,LN2_BP_LINE)

LN2_BP_MATCHED_TR: LINE=(LN2_BP_MATCH0,LN2_BP_LINE,LN2_BP_WA_OU)
LN2_BP_MATCHED_TR2: LINE=(LN2_BP_MATCH0,LN2_BP_LINE,LN2_BP_WA_OU2)

"


puts $filee "

!-----------------------------
! MATCH SXR BY-PASS LINE
!-----------------------------

                 
SBP_DR_BP      : DRIF,GROUP=\"HPB\",L=$sbp_swi_trip_dir
SBP_DR_BP_H    : DRIF,GROUP=\"HPB\",L=[expr $sbp_swi_trip_dir*0.5]
SBP_DR_BP_C    : CSRDRIFT,GROUP=\"HPB\",L=$sbp_swi_trip_dir,N_KICKS= 10,USE_STUPAKOV=1, CSR=$csr_flag  
SBP_DR_BP_CH   : CSRDRIFT,GROUP=\"HPB\",L=[expr $sbp_swi_trip_dir*0.5],N_KICKS= 10,USE_STUPAKOV=1, CSR=$csr_flag  

SBP_DP_SEPM1 :  CSRCSBEND,L= [expr $sbp_swi_s_dipole*0.5], EDGE_ORDER=2, ANGLE=[expr $sbp_swi_angle_rd*0.5], E1=0.0, E2=0.0, &
                 N_KICKS=10,INTEGRATION_ORDER=4, TILT=\"pi\", BINS=25,SG_HALFWIDTH=3, SG_ORDER=2, STEADY_STATE=0, &
                 CSR=$csr_flag, ISR=$isr_flag,HIGH_FREQUENCY_CUTOFF0=$f0_csr_sxrbp,HIGH_FREQUENCY_CUTOFF1=$f1_csr_sxrbp
                 
SBP_DP_SEPM2 :  CSRCSBEND, L= [expr $sbp_swi_s_dipole*0.5], EDGE_ORDER=2,  ANGLE=[expr $sbp_swi_angle_rd*0.5], E1=0.0, E2=0.0, &
                 N_KICKS=10, INTEGRATION_ORDER=4, BINS=25,SG_HALFWIDTH=3, SG_ORDER=2, STEADY_STATE=0, &
                 CSR=$csr_flag, ISR=$isr_flag,HIGH_FREQUENCY_CUTOFF0=$f0_csr_sxrbp,HIGH_FREQUENCY_CUTOFF1=$f1_csr_sxrbp
                 
SBP_COR       : KICKER, L=0.0,HKICK=0,VKICK=0, GROUP=\"SBP\"
SBP_BPM       : MONI, L=0.0, GROUP=\"SBP\"
SBP_DR_QD_ED  : DRIFT,L= $sbp_quad_edge, GROUP=\"SBP\"
SBP_DR_FL     : DRIFT,L= $flange, GROUP=\"SBP\"
SBP_DR_BL     : DRIFT,L= $belowl, GROUP=\"SBP\"
SBP_DR_SV     : DRIFT,L= $vacpor, GROUP=\"SBP\"
SBP_DR_VS     : DRIFT,L= $viewsc, GROUP=\"SBP\"
SBP_DR_DR     : DRIFT,L= $viewsc, GROUP=\"SBP\"

SBP_SX_V01: SEXT,GROUP=\"SBP\",L=[expr $sbp_quad_l*1.],K2=$sbp_ksext
SBP_SX_V02: SEXT,GROUP=\"SBP\",L=[expr $sbp_quad_l*1.],K2=-$sbp_ksext

SBP_QD_V01: QUAD,GROUP=\"SBP\",L=0.04,K1=7.561815873204035
SBP_QD_V02: QUAD,GROUP=\"SBP\",L=0.04,K1=-5.991812861815918
SBP_QD_V03: QUAD,GROUP=\"SBP\",L=0.04,K1=7.209417476937203
SBP_QD_V04: QUAD,GROUP=\"SBP\",L=0.04,K1=-3.526488954806807
SBP_QD_V05: QUAD,GROUP=\"SBP\",L=0.04,K1=6.64281155176975
SBP_QD_V06: QUAD,GROUP=\"SBP\",L=0.04,K1=-4.406495743442422
SBP_DR_V1: DRIF,GROUP=\"SBP\",L=0.262902130076265
SBP_DR_V2: DRIF,GROUP=\"SBP\",L=0.01000000000000444
SBP_DR_V3: DRIF,GROUP=\"SBP\",L=0.03475657741378942
SBP_DR_V4: DRIF,GROUP=\"SBP\",L=2.161021266372867
SBP_DR_V5: DRIF,GROUP=\"SBP\",L=4.504222156213344
SBP_DR_V6: DRIF,GROUP=\"SBP\",L=0.231193470062079
SBP_DR_V7: DRIF,GROUP=\"SBP\",L=0.0100000000000002

SBP_QD_V1   : LINE=(SBP_DR_QD_ED,SBP_QD_V01,SBP_COR,SBP_BPM,SBP_QD_V01,SBP_DR_QD_ED)
SBP_QD_V2   : LINE=(SBP_DR_QD_ED,SBP_QD_V02,SBP_COR,SBP_BPM,SBP_QD_V02,SBP_DR_QD_ED)
SBP_QD_V3   : LINE=(SBP_DR_QD_ED,SBP_QD_V03,SBP_COR,SBP_BPM,SBP_QD_V03,SBP_DR_QD_ED)
SBP_QD_V4   : LINE=(SBP_DR_QD_ED,SBP_QD_V04,SBP_COR,SBP_BPM,SBP_QD_V04,SBP_DR_QD_ED)
SBP_QD_V4A  : LINE=(SBP_DR_QD_ED,SBP_QD_V04,SBP_COR)
SBP_QD_V4B  : LINE=(SBP_BPM,SBP_QD_V04,SBP_DR_QD_ED)
SBP_QD_V5   : LINE=(SBP_DR_QD_ED,SBP_QD_V05,SBP_COR,SBP_BPM,SBP_QD_V05,SBP_DR_QD_ED)



FITT_SBP_0:  MARKER, FITPOINT=1
FITT_SBP_1:  MARKER, FITPOINT=1
FITT_SBP_2:  MARKER, FITPOINT=1
FITT_SBP_3:  MARKER, FITPOINT=1
FITT_SBP_4:  MARKER, FITPOINT=1
FITT_SBP_5:  MARKER, FITPOINT=1
FITT_SBP_6:  MARKER, FITPOINT=1
FITT_SBP_7:  MARKER, FITPOINT=1
FITT_SBP_8:  MARKER, FITPOINT=1
FITT_SBP_PI:  MARKER, FITPOINT=1
FITT_SBP_PO:  MARKER, FITPOINT=1
SBP_CNT0     : DRIFT, L=0 




SBP_MATCH_IN	:LINE=(SBP_DR_V1,SBP_QD_V1,SBP_DR_V2,SBP_DR_V2,SBP_QD_V1,SBP_DR_V1)

FITT_SBP_AA:  MARKER, FITPOINT=1
FITT_SBP_BB:  MARKER, FITPOINT=1
SBP_MATCH_CNT_IN_1 :LINE=(SBP_DR_BP_C,SBP_DR_V3,SBP_QD_V2,FITT_SBP_AA,SBP_DR_V4,SBP_QD_V3,SBP_DR_V5,SBP_QD_V4A)
SBP_MATCH_CNT_IN_2 :LINE=(SBP_QD_V4B,SBP_DR_V5,SBP_QD_V3,SBP_DR_V4,FITT_SBP_BB,SBP_QD_V2,SBP_DR_V3,SBP_DR_BP)

SBP_MATCH_OU	:LINE=(SBP_DR_V6,SBP_QD_V5,SBP_DR_V7,SBP_DR_V7,SBP_QD_V5,SBP_DR_V6)



SBP_WA_OU1:       WATCH,filename=\"sbp_ou1.sdds\",mode=coord, label=\"sbp first \"
SBP_WA_OU2:       WATCH,filename=\"sbp_ou2.sdds\",mode=coord, label=\"sbp cent1 \"
SBP_WA_OU3:       WATCH,filename=\"sbp_ou3.sdds\",mode=coord, label=\"sbp cent2 \"

SBP_WA_OU1a:       WATCH,filename=\"sbp_ou1a.sdds\",mode=coord, label=\"sbp first \"
SBP_WA_OU2a:       WATCH,filename=\"sbp_ou2a.sdds\",mode=coord, label=\"sbp cent1 \"
SBP_WA_OU3a:       WATCH,filename=\"sbp_ou3a.sdds\",mode=coord, label=\"sbp cent2 \"
SBP_WA_OU4  : WATCH,filename=\"sbp_ou4.sdds\",mode=coord, label=\"sbp ou \" 

SBP_CNT     : CENTER                 
SBP_CNT1    : DRIFT,L=0

SBP_MATCH_TO_1   :LINE=(FITT_SBP_0,SBP_DP_SEPM1,SBP_DP_SEPM1,SBP_DR_BP_CH,SBP_DR_BP_H, &
                   3*SBP_DR_BP,SBP_MATCH_IN,4*SBP_DR_BP, & 
                                  SBP_DP_SEPM1,SBP_DP_SEPM1,FITT_SBP_3)
            
SBP_MATCH_TO_2   :LINE=(FITT_SBP_3, SBP_MATCH_CNT_IN_1,FITT_SBP_4,SBP_MATCH_CNT_IN_2,FITT_SBP_5)

SBP_MATCH_TO_3   :LINE=(FITT_SBP_5,SBP_DP_SEPM2,SBP_DP_SEPM2,SBP_DR_BP_CH,SBP_DR_BP_H, &
                   3*SBP_DR_BP,SBP_MATCH_OU,2*SBP_DR_BP, 2*SBP_DR_BP, & 
                                   SBP_DP_SEPM2,SBP_DP_SEPM2,FITT_SBP_8)

FITT_SBP_A:  MARKER, FITPOINT=1
FITT_SBP_B:  MARKER, FITPOINT=1

                                   
SBP_MATCHED_1    :LINE=(FITT_SBP_0,SBP_CNT0,SBP_DP_SEPM1,FITT_SBP_A,SBP_DP_SEPM1,SBP_DR_BP_C,3*SBP_DR_BP,SBP_MATCH_IN, &
                    4*SBP_DR_BP,FITT_SBP_PI,SBP_CNT0,SBP_DP_SEPM1,FITT_SBP_B,SBP_DP_SEPM1)
                    
SBP_MATCHED_2    :LINE=(SBP_MATCH_CNT_IN_1,SBP_MATCH_CNT_IN_2)

!SBP_MATCHED_3   :LINE=(SBP_DP_SEPM2,SBP_DP_SEPM2,SBP_DR_BP_CH,SBP_DR_BP_H, 3*SBP_DR_BP,SBP_MATCH_OU,2*SBP_DR_BP, 2*SBP_DR_BP, & 
                                   SBP_DP_SEPM2,SBP_DP_SEPM2)
                                   
SBP_MATCHED_3   :LINE=(SBP_DP_SEPM2,SBP_DP_SEPM2,SBP_DR_BP_CH,SBP_DR_BP_H, 3*SBP_DR_BP,SBP_MATCH_IN,2*SBP_DR_BP, 2*SBP_DR_BP, & 
                                   SBP_DP_SEPM2,SBP_DP_SEPM2)

SBP_MATCHED_TR_1: LINE=(SBP_MATCHED_1,SBP_WA_OU1)
SBP_MATCHED_TR_2: LINE=(SBP_MATCHED_2,SBP_WA_OU2)
SBP_MATCHED_TR_3: LINE=(SBP_MATCHED_3,SBP_WA_OU3)

SBP_MATCHED_TR_12: LINE=(SBP_MATCHED_1,SBP_WA_OU1a)
SBP_MATCHED_TR_22: LINE=(SBP_MATCHED_2,SBP_WA_OU2a)
SBP_MATCHED_TR_32: LINE=(SBP_MATCHED_3,SBP_WA_OU3a)
               
"



puts $filee "

!------------------------------------------------------------------------------
! Setup linac 4 module
!------------------------------------------------------------------------------

LN4_DR_FL     : DRIFT,L= $flange, GROUP=\"LN4\"
LN4_DR_BL     : DRIFT,L= $belowl, GROUP=\"LN4\"
LN4_DR_SV     : DRIFT,L= $vacpor, GROUP=\"LN4\"
LN4_DR_VS     : DRIFT,L= $viewsc, GROUP=\"LN4\"
LN4_DR_DR     : DRIFT,L= $viewsc, GROUP=\"LN4\"
LN4_DR_CT     : DRIFT,L= $viewsc, GROUP=\"LN4\"

LN4_ACCSW      : LINE=(LN4_DR_SV,LN4_DR_BL,LN4_DR_XCA_ED,LN4_XCA0,LN4_DR_XCA_ED,LN4_DR_BL,LN4_DR_XCA_ED,LN4_XCA0,LN4_DR_XCA_ED,LN4_DR_BL)
LN4_ACCSC      : LINE=(LN4_DR_VS,LN4_DR_BL,LN4_DR_XCA_ED,LN4_XCA0,LN4_DR_XCA_ED,LN4_DR_BL,LN4_DR_XCA_ED,LN4_XCA0,LN4_DR_XCA_ED,LN4_DR_BL)
LN4_DR_ACC     : LINE=(LN4_DR_DR,LN4_DR_BL,LN4_DR_XCA_ED, LN4_DR_XCA_)

LN4_DR_QD_ED  : DRIFT,L= $ln4_quad_edge, GROUP=\"LN4\"
LN4_QD_FH     : QUAD, L=[expr $ln4_quad_l*0.5], K1=[expr  1.0*$ln4_quad_k], GROUP=\"LN4\"
LN4_QD_DH     : QUAD, L=[expr $ln4_quad_l*0.5], K1=[expr -1.0*$ln4_quad_k], GROUP=\"LN4\"
LN4_COR       : KICKER, L=0.0,HKICK=0,VKICK=0, GROUP=\"LN4\"
LN4_BPM       : MONI, L=0.0, GROUP=\"LN4\"


LN4_QD_F      : LINE=(LN4_DR_QD_ED,LN4_QD_FH,LN4_COR,LN4_BPM,LN4_QD_FH,LN4_DR_QD_ED)
LN4_QD_D      : LINE=(LN4_DR_QD_ED,LN4_QD_DH,LN4_COR,LN4_BPM,LN4_QD_DH,LN4_DR_QD_ED)
LN4_MODULE    : LINE=(LN4_QD_F, LN4_ACCSW,LN4_QD_D,LN4_ACCSC)


!------------------------------------------------------------------------------
! Linac 1 matching quads
!------------------------------------------------------------------------------

LN4_QD_V01: QUAD,GROUP=\"LN4\",L=[expr $ln4_quad_l*0.5],K1=13.99999999997731
LN4_QD_V02: QUAD,GROUP=\"LN4\",L=[expr $ln4_quad_l*0.5],K1=-10.74063711847344
LN4_QD_V03: QUAD,GROUP=\"LN4\",L=[expr $ln4_quad_l*0.5],K1=6.520775985403053
LN4_QD_V04: QUAD,GROUP=\"LN4\",L=[expr $ln4_quad_l*0.5],K1=-16.14389813034124
LN4_DR_V1: DRIF,GROUP=\"LN4\",L=1.581247924936149
LN4_DR_V2: DRIF,GROUP=\"LN4\",L=0.7045084212603139
LN4_DR_V3: DRIF,GROUP=\"LN4\",L=2.784115854020499
LN4_DR_V4: DRIF,GROUP=\"LN4\",L=3.062101707144716
LN4_DR_V5: DRIF,GROUP=\"LN4\",L=0.7001603960975262

LN4_QD_V1   : LINE=(LN4_DR_QD_ED,LN4_QD_V01,LN4_COR,LN4_BPM,LN4_QD_V01,LN4_DR_QD_ED)
LN4_QD_V2   : LINE=(LN4_DR_QD_ED,LN4_QD_V02,LN4_COR,LN4_BPM,LN4_QD_V02,LN4_DR_QD_ED)
LN4_QD_V3   : LINE=(LN4_DR_QD_ED,LN4_QD_V03,LN4_COR,LN4_BPM,LN4_QD_V03,LN4_DR_QD_ED)
LN4_QD_V4   : LINE=(LN4_DR_QD_ED,LN4_QD_V04,LN4_COR,LN4_BPM,LN4_QD_V04,LN4_DR_QD_ED)

LN4_IN_CNT   :CENTER
!LN4_MATCH0    : LINE=(LN4_IN_CNT,LN4_DR_V1,LN4_QD_V1,LN4_DR_V2,LN4_QD_V2,LN4_DR_V3,LN4_QD_V3,LN4_DR_V4,LN4_QD_V4,LN4_DR_V5)
LN4_MATCH0    : LINE=(LN4_IN_CNT,LN4_DR_CT,LN4_DR_V1,LN4_QD_V1,LN4_DR_V2,LN4_QD_V2,LN4_DR_V3,LN4_QD_V3,LN4_DR_V4,LN4_DR_VS)

LN4_FITP1     : MARKER, FITPOINT=1
LN4_FITP2     : MARKER, FITPOINT=1
LN4_FITP3     : MARKER, FITPOINT=1
LN4_FITP4     : MARKER, FITPOINT=1

LN4_FITT_1    : LINE=(LN4_DR_QD_ED,LN4_QD_FH, LN4_FITP1, LN4_QD_FH, LN4_DR_QD_ED, LN4_ACCSW)
LN4_FITT_2    : LINE=(LN4_DR_QD_ED,LN4_QD_DH, LN4_FITP2, LN4_QD_DH, LN4_DR_QD_ED, LN4_ACCSW)
LN4_FITT_3    : LINE=(LN4_DR_QD_ED,LN4_QD_FH, LN4_FITP3, LN4_QD_FH, LN4_DR_QD_ED, LN4_ACCSW)
LN4_FITT_4    : LINE=(LN4_DR_QD_ED,LN4_QD_DH, LN4_FITP4, LN4_QD_DH)

LN4_WA_M_OU    : WATCH,filename=\"ln4_mtch_ou.sdds\",mode=coord, label=\"ln4 m ou \"
LN4_WA_OU      : WATCH,filename=\"ln4_ou.sdds\",mode=coord, label=\"ln4 ou \"
LN4_WA_OU2      : WATCH,filename=\"ln4_oua.sdds\",mode=coord, label=\"ln4 ou \"

LN4_MATCH_TO  : LINE=(LN4_MATCH0,LN4_FITT_1,LN4_FITT_2,LN4_FITT_3,LN4_FITT_4 )
LN4_MATCHED   : LINE=(LN4_MATCH0,$ln4_nmodule*LN4_MODULE,LN4_QD_F)
LN4_MATCHED_TR: LINE=(LN4_MATCH0,$ln4_nmodule*LN4_MODULE,LN4_QD_F,LN4_WA_OU)
LN4_MATCHED_TR2: LINE=(LN4_MATCH0,$ln4_nmodule*LN4_MODULE,LN4_QD_F,LN4_WA_OU2)
"



puts $filee "

!------------------------------------------------------------------------------
! Long drift section after linac 4  to SXR
!------------------------------------------------------------------------------


DR4_DR_FL     : DRIFT,L= $flange, GROUP=\"DR4\"
DR4_DR_BL     : DRIFT,L= $belowl, GROUP=\"DR4\"
DR4_DR_SV     : DRIFT,L= $vacpor, GROUP=\"DR4\"
DR4_DR_VS     : DRIFT,L= $viewsc, GROUP=\"DR4\"
DR4_DR_DR     : DRIFT,L= $viewsc, GROUP=\"DR4\"
DR4_DR_CT     : DRIFT,L= $viewsc, GROUP=\"DR4\"
DR4_DR_ST     : DRIFT,L= $ln42_drift GROUP=\"DR4\"


DR4_DR_A      : LINE=(4*DR4_DR_ST)



DR4_DR_QD_ED  : DRIFT,L= $ln4_quad_edge, GROUP=\"DR4\"
DR4_QD_FH     : QUAD, L=[expr $ln4_quad_l*0.5], K1=[expr  1.0*$ln42_quad_k], GROUP=\"DR4\"
DR4_QD_DH     : QUAD, L=[expr $ln4_quad_l*0.5], K1=[expr -1.0*$ln42_quad_k], GROUP=\"DR4\"
DR4_COR       : KICKER, L=0.0,HKICK=0,VKICK=0, GROUP=\"DR4\"
DR4_BPM       : MONI, L=0.0, GROUP=\"DR4\"


DR4_QD_F      : LINE=(DR4_DR_QD_ED,DR4_QD_FH,DR4_COR,DR4_BPM,DR4_QD_FH,DR4_DR_QD_ED)
DR4_QD_D      : LINE=(DR4_DR_QD_ED,DR4_QD_DH,DR4_COR,DR4_BPM,DR4_QD_DH,DR4_DR_QD_ED)
DR4_MODULE    : LINE=(DR4_QD_F, DR4_DR_A,DR4_QD_D,DR4_DR_A)


!------------------------------------------------------------------------------
! matching quads to drift section 
!------------------------------------------------------------------------------

DR4_QD_V01: QUAD,GROUP=\"DR4\",L=[expr $ln4_quad_l*0.5],K1=13.99999999997731
DR4_QD_V02: QUAD,GROUP=\"DR4\",L=[expr $ln4_quad_l*0.5],K1=-10.74063711847344
DR4_QD_V03: QUAD,GROUP=\"DR4\",L=[expr $ln4_quad_l*0.5],K1=6.520775985403053
DR4_QD_V04: QUAD,GROUP=\"DR4\",L=[expr $ln4_quad_l*0.5],K1=-16.14389813034124
DR4_DR_V1: DRIF,GROUP=\"DR4\",L=1.581247924936149
DR4_DR_V2: DRIF,GROUP=\"DR4\",L=0.7045084212603139
DR4_DR_V3: DRIF,GROUP=\"DR4\",L=2.784115854020499
DR4_DR_V4: DRIF,GROUP=\"DR4\",L=3.062101707144716
DR4_DR_V5: DRIF,GROUP=\"DR4\",L=0.7001603960975262

DR4_QD_V1   : LINE=(DR4_DR_QD_ED,DR4_QD_V01,DR4_COR,DR4_BPM,DR4_QD_V01,DR4_DR_QD_ED)
DR4_QD_V2   : LINE=(DR4_DR_QD_ED,DR4_QD_V02,DR4_COR,DR4_BPM,DR4_QD_V02,DR4_DR_QD_ED)
DR4_QD_V3   : LINE=(DR4_DR_QD_ED,DR4_QD_V03,DR4_COR,DR4_BPM,DR4_QD_V03,DR4_DR_QD_ED)
DR4_QD_V4   : LINE=(DR4_DR_QD_ED,DR4_QD_V04,DR4_COR,DR4_BPM,DR4_QD_V04,DR4_DR_QD_ED)

DR4_IN_CNT   :CENTER
DR4_MATCH0    : LINE=(DR4_IN_CNT,DR4_DR_CT,DR4_DR_V1,DR4_QD_V1,DR4_DR_V2,DR4_QD_V2,DR4_DR_V3,DR4_QD_V3,DR4_DR_V4,DR4_DR_VS)

DR4_FITP1     : MARKER, FITPOINT=1
DR4_FITP2     : MARKER, FITPOINT=1
DR4_FITP3     : MARKER, FITPOINT=1
DR4_FITP4     : MARKER, FITPOINT=1

DR4_FITT_1    : LINE=(DR4_DR_QD_ED,DR4_QD_FH, DR4_FITP1, DR4_QD_FH, DR4_DR_QD_ED, DR4_DR_A)
DR4_FITT_2    : LINE=(DR4_DR_QD_ED,DR4_QD_DH, DR4_FITP2, DR4_QD_DH, DR4_DR_QD_ED, DR4_DR_A)
DR4_FITT_3    : LINE=(DR4_DR_QD_ED,DR4_QD_FH, DR4_FITP3, DR4_QD_FH, DR4_DR_QD_ED, DR4_DR_A)
DR4_FITT_4    : LINE=(DR4_DR_QD_ED,DR4_QD_DH, DR4_FITP4, DR4_QD_DH)

DR4_WA_M_OU    : WATCH,filename=\"ln4_dr_mtch_ou.sdds\",mode=coord, label=\"ln4 dr m ou \"
DR4_WA_OU      : WATCH,filename=\"ln4_dr_ou.sdds\",mode=coord, label=\"ln4  dr ou \"
DR4_WA_OU2      : WATCH,filename=\"ln4_dr_oua.sdds\",mode=coord, label=\"ln4 dr ou \"

DR4_MATCH_TO  : LINE=(DR4_MATCH0,DR4_FITT_1,DR4_FITT_2,DR4_FITT_3,DR4_FITT_4 )
DR4_MATCHED   : LINE=(DR4_MATCH0,$ln4_dr_nmodule*DR4_MODULE,DR4_QD_F)
DR4_MATCHED_TR: LINE=(DR4_MATCH0,$ln4_dr_nmodule*DR4_MODULE,DR4_QD_F,DR4_WA_OU)
DR4_MATCHED_TR2: LINE=(DR4_MATCH0,$ln4_dr_nmodule*DR4_MODULE,DR4_QD_F,DR4_WA_OU2)
"




puts $filee "

!------------------------------------------------------------------------------
! Setup HXR undulator  module same undulator will be used for SXR
!------------------------------------------------------------------------------

HXR_DR_FL     : DRIFT,L= $flange, GROUP=\"HXR\"
HXR_DR_BL     : DRIFT,L= $belowl, GROUP=\"HXR\"
HXR_DR_SV     : DRIFT,L= $vacpor, GROUP=\"HXR\"
HXR_DR_VS     : DRIFT,L= $viewsc, GROUP=\"HXR\"
HXR_DR_DR     : DRIFT,L= $viewsc, GROUP=\"HXR\"
HXR_DR_CT     : DRIFT,L= $viewsc, GROUP=\"HXR\"


HXR_WIG_0: WIGGLER, L = [expr $hxr_np*$hxr_pl], B = 0.75, POLES = $hxr_np, GROUP=\"HXR\" &

HXR_DR_WIG    : DRIFT,L= [expr $hxr_np*$hxr_pl], GROUP=\"HXR\"
HXR_DR_WIG_H    : DRIFT,L= [expr $hxr_np*$hxr_pl*0.5], GROUP=\"HXR\"
HXR_DR_WIG_ED : DRIFT,L= $hxr_wiged, GROUP=\"HXR\"
HXR_DR_PHS : DRIFT,L= 0.2, GROUP=\"HXR\"
HXR_WIG       : LINE=(HXR_DR_WIG_ED,HXR_WIG_0,HXR_DR_WIG_ED)

!HXR_WIGW      : LINE=(HXR_DR_SV,HXR_DR_BL,HXR_WIG,HXR_DR_BL)
!HXR_WIGC      : LINE=(HXR_DR_VS,HXR_DR_BL,HXR_WIG,HXR_DR_BL)
HXR_DR_WIGH    : LINE=(HXR_DR_DR,HXR_DR_BL,HXR_DR_WIG_ED, HXR_DR_WIG_H)

HXR_DR_QD_ED  : DRIFT,L= $hxr_quad_edge, GROUP=\"HXR\"
HXR_QD_FH     : QUAD, L=[expr $hxr_quad_l*0.5], K1=[expr  1.0*$hxr_quad_k], GROUP=\"HXR\"
HXR_QD_DH     : QUAD, L=[expr $hxr_quad_l*0.5], K1=[expr -1.0*$hxr_quad_k], GROUP=\"HXR\"
HXR_COR       : KICKER, L=0.0,HKICK=0,VKICK=0, GROUP=\"HXR\"
HXR_BPM       : MONI, L=0.0, GROUP=\"HXR\"


HXR_QD_F      : LINE=(HXR_DR_QD_ED,HXR_QD_FH,HXR_COR,HXR_BPM,HXR_QD_FH,HXR_DR_QD_ED)
HXR_QD_FHA      : LINE=(HXR_DR_QD_ED,HXR_QD_FH)
HXR_QD_D      : LINE=(HXR_DR_QD_ED,HXR_QD_DH,HXR_COR,HXR_BPM,HXR_QD_DH,HXR_DR_QD_ED)
HXR_MODULE    : LINE=(HXR_QD_F, HXR_WIG,HXR_QD_D,HXR_WIG)


!------------------------------------------------------------------------------
! HXR  matching quads
!------------------------------------------------------------------------------

HXR_QD_V01: QUAD,GROUP=\"HXR\",L=[expr $hxr_quad_l*0.5],K1=10.74969291048027
HXR_QD_V02: QUAD,GROUP=\"HXR\",L=[expr $hxr_quad_l*0.5],K1=-29.99990297783807
HXR_QD_V03: QUAD,GROUP=\"HXR\",L=[expr $hxr_quad_l*0.5],K1=21.94194628292705
HXR_QD_V04: QUAD,GROUP=\"HXR\",L=[expr $hxr_quad_l*0.5],K1=-5
HXR_DR_V1: DRIF,GROUP=\"HXR\",L=0.2005064553554299
HXR_DR_V2: DRIF,GROUP=\"HXR\",L=1.773575207192217
HXR_DR_V3: DRIF,GROUP=\"HXR\",L=0.8777966550499587
HXR_DR_V4: DRIF,GROUP=\"HXR\",L=0.3162998423719277
HXR_DR_V5: DRIF,GROUP=\"HXR\",L=0.7001603960975262

HXR_QD_V1   : LINE=(HXR_DR_QD_ED,HXR_QD_V01,HXR_COR,HXR_BPM,HXR_QD_V01,HXR_DR_QD_ED)
HXR_QD_V2   : LINE=(HXR_DR_QD_ED,HXR_QD_V02,HXR_COR,HXR_BPM,HXR_QD_V02,HXR_DR_QD_ED)
HXR_QD_V3   : LINE=(HXR_DR_QD_ED,HXR_QD_V03,HXR_COR,HXR_BPM,HXR_QD_V03,HXR_DR_QD_ED)
HXR_QD_V4   : LINE=(HXR_DR_QD_ED,HXR_QD_V04,HXR_COR,HXR_BPM,HXR_QD_V04,HXR_DR_QD_ED)

!HXR_MATCH0    : LINE=(HXR_DR_V1,HXR_QD_V1,HXR_DR_V2,HXR_QD_V2,HXR_DR_V3,HXR_QD_V3,HXR_DR_V4,HXR_QD_V4,HXR_DR_V5)
HXR_MATCH0    : LINE=(HXR_DR_V1,HXR_QD_V1,HXR_DR_V2,HXR_QD_V2,HXR_DR_V3,HXR_QD_V3,HXR_DR_V4,HXR_DR_CT)

HXR_FITP1     : MARKER, FITPOINT=1
HXR_FITP2     : MARKER, FITPOINT=1
HXR_FITP3     : MARKER, FITPOINT=1
HXR_FITP4     : MARKER, FITPOINT=1

HXR_FITT_1    : LINE=(HXR_DR_QD_ED,HXR_QD_FH, HXR_FITP1, HXR_QD_FH, HXR_DR_QD_ED, HXR_WIG)
HXR_FITT_2    : LINE=(HXR_DR_QD_ED,HXR_QD_DH, HXR_FITP2, HXR_QD_DH, HXR_DR_QD_ED, HXR_WIG)
HXR_FITT_3    : LINE=(HXR_DR_QD_ED,HXR_QD_FH, HXR_FITP3, HXR_QD_FH, HXR_DR_QD_ED, HXR_WIG)
HXR_FITT_4    : LINE=(HXR_DR_QD_ED,HXR_QD_DH, HXR_FITP4, HXR_QD_DH)

HXR_WA_M_OU    : WATCH,filename=\"hxr_m_ou.sdds\",mode=coord, label=\"hxr m ou \"
HXR_WA_OU      : WATCH,filename=\"hxr_ou.sdds\",mode=coord, label=\"hxr ou \"

HXR_WA_M_OU2    : WATCH,filename=\"hxr_m_oua.sdds\",mode=coord, label=\"hxr m ou \"
HXR_WA_OU2      : WATCH,filename=\"hxr_oua.sdds\",mode=coord, label=\"hxr ou \"

HXR_MATCH_TO  : LINE=(HXR_MATCH0,HXR_FITT_1,HXR_FITT_2,HXR_FITT_3,HXR_FITT_4 )
HXR_MATCHED   : LINE=(HXR_MATCH0,$hxr_nmodule*HXR_MODULE,HXR_QD_FHA)
HXR_MATCHED_TR: LINE=(HXR_MATCH0,HXR_WA_M_OU,$hxr_nmodule*HXR_MODULE,HXR_QD_FHA,HXR_WA_OU)
HXR_MATCHED_TR2: LINE=(HXR_MATCH0,HXR_WA_M_OU2,$hxr_nmodule*HXR_MODULE,HXR_QD_FHA,HXR_WA_OU2)


"



if {$case0 > 0 && $case1 < 1 && $case2 <1 && $case3 <1 && $case4 <1 && $case5 <1 && $case7 <1 && $case8a <1 && $case8b <1 && $case9 <1  && $case10 <1 &&  $case11 <1  && $case11 <1 } {
#  case linac 0
# set watches "inj_in.sdds ln0_lher_ou.sdds ln0_lnz_in.sdds ln0_ou.sdds "
# # lappend watches ln0_mtch_ou.sdds 



puts $filee "
MATCHINS_[expr $indx+0] : LINE=(LN0_MATCH_TO)
MATCHEDS_[expr $indx+1] : LINE=(LN0_MATCHED)
TRACKINS_[expr $indx+1] : LINE=(BNCH,LN0_MATCHED_TR)

MATCHEDA_[expr $indx+1]  : LINE=(LN0_MATCHED)
TRACKA_[expr $indx+1]    : LINE=(BNCH,LN0_MATCHED_TR)
"


} elseif {$case0 > 0 && $case1 > 0 && $case2 <1 && $case3 <1 && $case4 <1 && $case5 <1 && $case6 <1 && $case7 <1 && $case8a <1 && $case8b <1 && $case9 <1  && $case10 <1 &&  $case11 <1} {
    #  case linac 0 + bc1

puts $filee "

MATCHINS_[expr $indx+0] : LINE=(LN0_MATCH_TO)
MATCHEDS_[expr $indx+1] : LINE=(LN0_MATCHED)
TRACKINS_[expr $indx+1] : LINE=(BNCH,LN0_MATCHED_TR)

MATCHEDA_[expr $indx+1]  : LINE=(LN0_MATCHED)
TRACKA_[expr $indx+1]    : LINE=(BNCH,LN0_MATCHED_TR2)

MATCHINS_[expr $indx+1] : LINE=(BC1_MATCH_TO)
MATCHEDS_[expr $indx+2] : LINE=(BC1_MATCHED)
TRACKINS_[expr $indx+2] : LINE=(BNCH,BC1_MATCHED_TR)

MATCHEDA_[expr $indx+2]  : LINE=(LN0_MATCHED,BC1_MATCHED)
TRACKA_[expr $indx+2]    : LINE=(BNCH,LN0_MATCHED_TR2,BC1_MATCHED_TR2)

"
} elseif {$case0 > 0 && $case1 > 0 && $case2 >0 && $case3 <1 && $case4 <1 && $case5 <1 && $case6 <1  && $case7 <1 && $case8a <1 && $case8b <1 && $case9 <1  && $case10 <1 &&  $case11 <1} {

puts $filee "

MATCHINS_[expr $indx+0] : LINE=(LN0_MATCH_TO)
MATCHEDS_[expr $indx+1] : LINE=(LN0_MATCHED)
TRACKINS_[expr $indx+1] : LINE=(BNCH,LN0_MATCHED_TR)

MATCHEDA_[expr $indx+1]  : LINE=(LN0_MATCHED)
TRACKA_[expr $indx+1]    : LINE=(BNCH,LN0_MATCHED_TR2)

MATCHINS_[expr $indx+1] : LINE=(BC1_MATCH_TO)
MATCHEDS_[expr $indx+2] : LINE=(BC1_MATCHED)
TRACKINS_[expr $indx+2] : LINE=(BNCH,BC1_MATCHED_TR)

MATCHEDA_[expr $indx+2]  : LINE=(LN0_MATCHED,BC1_MATCHED)
TRACKA_[expr $indx+2]    : LINE=(BNCH,LN0_MATCHED_TR2,BC1_MATCHED_TR2)

MATCHINS_[expr $indx+2] : LINE=(BC1_DI_MATCH_TO)
MATCHEDS_[expr $indx+3] : LINE=(BC1_DI_MATCHED)
TRACKINS_[expr $indx+3] : LINE=(BNCH,BC1_DI_MATCHED_TR)

MATCHEDA_[expr $indx+3]  : LINE=(LN0_MATCHED,BC1_MATCHED,BC1_DI_MATCHED)
TRACKA_[expr $indx+3]    : LINE=(BNCH,LN0_MATCHED_TR2,BC1_MATCHED_TR2,BC1_DI_MATCHED_TR2)

"
} elseif {$case0 > 0 && $case1 > 0 && $case2 >0 && $case3 >0  && $case4 <1 && $case5 <1 && $case6 <1 && $case7 <1 && $case8a <1 && $case8b <1 && $case9 <1  && $case10 <1 &&  $case11 <1 && $case11 <1 } {

puts $filee "

MATCHINS_[expr $indx+0] : LINE=(LN0_MATCH_TO)
MATCHEDS_[expr $indx+1] : LINE=(LN0_MATCHED)
TRACKINS_[expr $indx+1] : LINE=(BNCH,LN0_MATCHED_TR)

MATCHEDA_[expr $indx+1]  : LINE=(LN0_MATCHED)
TRACKA_[expr $indx+1]    : LINE=(BNCH,LN0_MATCHED_TR2)

MATCHINS_[expr $indx+1] : LINE=(BC1_MATCH_TO)
MATCHEDS_[expr $indx+2] : LINE=(BC1_MATCHED)
TRACKINS_[expr $indx+2] : LINE=(BNCH,BC1_MATCHED_TR)

MATCHEDA_[expr $indx+2]  : LINE=(LN0_MATCHED,BC1_MATCHED)
TRACKA_[expr $indx+2]    : LINE=(BNCH,LN0_MATCHED_TR2,BC1_MATCHED_TR2)

MATCHINS_[expr $indx+2] : LINE=(BC1_DI_MATCH_TO)
MATCHEDS_[expr $indx+3] : LINE=(BC1_DI_MATCHED)
TRACKINS_[expr $indx+3] : LINE=(BNCH,BC1_DI_MATCHED_TR)

MATCHEDA_[expr $indx+3]  : LINE=(LN0_MATCHED,BC1_MATCHED,BC1_DI_MATCHED)
TRACKA_[expr $indx+3]    : LINE=(BNCH,LN0_MATCHED_TR2,BC1_MATCHED_TR2,BC1_DI_MATCHED_TR2)

MATCHINS_[expr $indx+3] : LINE=(LN1_MATCH_TO)
MATCHEDS_[expr $indx+4] : LINE=(LN1_MATCHED)
TRACKINS_[expr $indx+4] : LINE=(BNCH,LN1_MATCHED_TR)

MATCHEDA_[expr $indx+4]  : LINE=(LN0_MATCHED,BC1_MATCHED,BC1_DI_MATCHED,LN1_MATCHED)
TRACKA_[expr $indx+4]    : LINE=(BNCH,LN0_MATCHED_TR2,BC1_MATCHED_TR2,BC1_DI_MATCHED_TR2,LN1_MATCHED_TR2)

"
} elseif {$case0 > 0 && $case1 > 0 && $case2 >0 && $case3 >0  && $case4 >0 && $case5 <1 && $case6 <1 && $case7 <1 && $case8a <1 && $case8b <1 && $case9 <1  && $case10 <1 &&  $case11 <1 && $case11 <1 } {


puts $filee "

MATCHINS_[expr $indx+0] : LINE=(LN0_MATCH_TO)
MATCHEDS_[expr $indx+1] : LINE=(LN0_MATCHED)
TRACKINS_[expr $indx+1] : LINE=(BNCH,LN0_MATCHED_TR)

MATCHEDA_[expr $indx+1]  : LINE=(LN0_MATCHED)
TRACKA_[expr $indx+1]    : LINE=(BNCH,LN0_MATCHED_TR2)

MATCHINS_[expr $indx+1] : LINE=(BC1_MATCH_TO)
MATCHEDS_[expr $indx+2] : LINE=(BC1_MATCHED)
TRACKINS_[expr $indx+2] : LINE=(BNCH,BC1_MATCHED_TR)

MATCHEDA_[expr $indx+2]  : LINE=(LN0_MATCHED,BC1_MATCHED)
TRACKA_[expr $indx+2]    : LINE=(BNCH,LN0_MATCHED_TR2,BC1_MATCHED_TR2)

MATCHINS_[expr $indx+2] : LINE=(BC1_DI_MATCH_TO)
MATCHEDS_[expr $indx+3] : LINE=(BC1_DI_MATCHED)
TRACKINS_[expr $indx+3] : LINE=(BNCH,BC1_DI_MATCHED_TR)

MATCHEDA_[expr $indx+3]  : LINE=(LN0_MATCHED,BC1_MATCHED,BC1_DI_MATCHED)
TRACKA_[expr $indx+3]    : LINE=(BNCH,LN0_MATCHED_TR2,BC1_MATCHED_TR2,BC1_DI_MATCHED_TR2)

MATCHINS_[expr $indx+3] : LINE=(LN1_MATCH_TO)
MATCHEDS_[expr $indx+4] : LINE=(LN1_MATCHED)
TRACKINS_[expr $indx+4] : LINE=(BNCH,LN1_MATCHED_TR)

MATCHEDA_[expr $indx+4]  : LINE=(LN0_MATCHED,BC1_MATCHED,BC1_DI_MATCHED,LN1_MATCHED)
TRACKA_[expr $indx+4]    : LINE=(BNCH,LN0_MATCHED_TR2,BC1_MATCHED_TR2,BC1_DI_MATCHED_TR2,LN1_MATCHED_TR2)

MATCHINS_[expr $indx+4] : LINE=(BC2_MATCH_TO)
MATCHEDS_[expr $indx+5] : LINE=(BC2_MATCHED)
TRACKINS_[expr $indx+5] : LINE=(BNCH,BC2_MATCHED_TR)

MATCHEDA_[expr $indx+5]  : LINE=(LN0_MATCHED,BC1_MATCHED,BC1_DI_MATCHED,LN1_MATCHED,BC2_MATCHED)
TRACKA_[expr $indx+5]    : LINE=(BNCH,LN0_MATCHED_TR2,BC1_MATCHED_TR2,BC1_DI_MATCHED_TR2,LN1_MATCHED_TR2,BC2_MATCHED_TR2)


"
} elseif {$case0 > 0 && $case1 > 0 && $case2 >0 && $case3 >0  && $case4 >0 && $case5 >0 && $case6 <1  && $case7 <1 && $case8a <1 && $case8b <1 && $case9 <1  && $case10 <1 &&  $case11 <1} {


puts $filee "


MATCHINS_[expr $indx+0] : LINE=(LN0_MATCH_TO)
MATCHEDS_[expr $indx+1] : LINE=(LN0_MATCHED)
TRACKINS_[expr $indx+1] : LINE=(BNCH,LN0_MATCHED_TR)

MATCHEDA_[expr $indx+1]  : LINE=(LN0_MATCHED)
TRACKA_[expr $indx+1]    : LINE=(BNCH,LN0_MATCHED_TR2)

MATCHINS_[expr $indx+1] : LINE=(BC1_MATCH_TO)
MATCHEDS_[expr $indx+2] : LINE=(BC1_MATCHED)
TRACKINS_[expr $indx+2] : LINE=(BNCH,BC1_MATCHED_TR)

MATCHEDA_[expr $indx+2]  : LINE=(LN0_MATCHED,BC1_MATCHED)
TRACKA_[expr $indx+2]    : LINE=(BNCH,LN0_MATCHED_TR2,BC1_MATCHED_TR2)

MATCHINS_[expr $indx+2] : LINE=(BC1_DI_MATCH_TO)
MATCHEDS_[expr $indx+3] : LINE=(BC1_DI_MATCHED)
TRACKINS_[expr $indx+3] : LINE=(BNCH,BC1_DI_MATCHED_TR)

MATCHEDA_[expr $indx+3]  : LINE=(LN0_MATCHED,BC1_MATCHED,BC1_DI_MATCHED)
TRACKA_[expr $indx+3]    : LINE=(BNCH,LN0_MATCHED_TR2,BC1_MATCHED_TR2,BC1_DI_MATCHED_TR2)

MATCHINS_[expr $indx+3] : LINE=(LN1_MATCH_TO)
MATCHEDS_[expr $indx+4] : LINE=(LN1_MATCHED)
TRACKINS_[expr $indx+4] : LINE=(BNCH,LN1_MATCHED_TR)

MATCHEDA_[expr $indx+4]  : LINE=(LN0_MATCHED,BC1_MATCHED,BC1_DI_MATCHED,LN1_MATCHED)
TRACKA_[expr $indx+4]    : LINE=(BNCH,LN0_MATCHED_TR2,BC1_MATCHED_TR2,BC1_DI_MATCHED_TR2,LN1_MATCHED_TR2)

MATCHINS_[expr $indx+4] : LINE=(BC2_MATCH_TO)
MATCHEDS_[expr $indx+5] : LINE=(BC2_MATCHED)
TRACKINS_[expr $indx+5] : LINE=(BNCH,BC2_MATCHED_TR)

MATCHEDA_[expr $indx+5]  : LINE=(LN0_MATCHED,BC1_MATCHED,BC1_DI_MATCHED,LN1_MATCHED,BC2_MATCHED)
TRACKA_[expr $indx+5]    : LINE=(BNCH,LN0_MATCHED_TR2,BC1_MATCHED_TR2,BC1_DI_MATCHED_TR2,LN1_MATCHED_TR2,BC2_MATCHED_TR2)

MATCHINS_[expr $indx+5] : LINE=(LN2_MATCH_TO)
MATCHEDS_[expr $indx+6] : LINE=(LN2_MATCHED)
TRACKINS_[expr $indx+6] : LINE=(BNCH,LN2_MATCHED_TR)

MATCHEDA_[expr $indx+6]  : LINE=(LN0_MATCHED,BC1_MATCHED,BC1_DI_MATCHED,LN1_MATCHED,BC2_MATCHED,LN2_MATCHED)
TRACKA_[expr $indx+6]    : LINE=(BNCH,LN0_MATCHED_TR2,BC1_MATCHED_TR2,BC1_DI_MATCHED_TR2,LN1_MATCHED_TR2,BC2_MATCHED_TR2,LN2_MATCHED_TR2)


"

} elseif {$case0 > 0 && $case1 > 0 && $case2 >0 && $case3 >0  && $case4 >0 && $case5 >0 && $case6 >0  && $case7 <1 && $case8a <1 && $case8b <1 && $case9 <1  && $case10 <1 &&  $case11 <1} {



puts $filee "

MATCHINS_[expr $indx+0] : LINE=(LN0_MATCH_TO)
MATCHEDS_[expr $indx+1] : LINE=(LN0_MATCHED)
TRACKINS_[expr $indx+1] : LINE=(BNCH,LN0_MATCHED_TR)

MATCHEDA_[expr $indx+1]  : LINE=(LN0_MATCHED)
TRACKA_[expr $indx+1]    : LINE=(BNCH,LN0_MATCHED_TR2)

MATCHINS_[expr $indx+1] : LINE=(BC1_MATCH_TO)
MATCHEDS_[expr $indx+2] : LINE=(BC1_MATCHED)
TRACKINS_[expr $indx+2] : LINE=(BNCH,BC1_MATCHED_TR)

MATCHEDA_[expr $indx+2]  : LINE=(LN0_MATCHED,BC1_MATCHED)
TRACKA_[expr $indx+2]    : LINE=(BNCH,LN0_MATCHED_TR2,BC1_MATCHED_TR2)

MATCHINS_[expr $indx+2] : LINE=(BC1_DI_MATCH_TO)
MATCHEDS_[expr $indx+3] : LINE=(BC1_DI_MATCHED)
TRACKINS_[expr $indx+3] : LINE=(BNCH,BC1_DI_MATCHED_TR)

MATCHEDA_[expr $indx+3]  : LINE=(LN0_MATCHED,BC1_MATCHED,BC1_DI_MATCHED)
TRACKA_[expr $indx+3]    : LINE=(BNCH,LN0_MATCHED_TR2,BC1_MATCHED_TR2,BC1_DI_MATCHED_TR2)

MATCHINS_[expr $indx+3] : LINE=(LN1_MATCH_TO)
MATCHEDS_[expr $indx+4] : LINE=(LN1_MATCHED)
TRACKINS_[expr $indx+4] : LINE=(BNCH,LN1_MATCHED_TR)

MATCHEDA_[expr $indx+4]  : LINE=(LN0_MATCHED,BC1_MATCHED,BC1_DI_MATCHED,LN1_MATCHED)
TRACKA_[expr $indx+4]    : LINE=(BNCH,LN0_MATCHED_TR2,BC1_MATCHED_TR2,BC1_DI_MATCHED_TR2,LN1_MATCHED_TR2)

MATCHINS_[expr $indx+4] : LINE=(BC2_MATCH_TO)
MATCHEDS_[expr $indx+5] : LINE=(BC2_MATCHED)
TRACKINS_[expr $indx+5] : LINE=(BNCH,BC2_MATCHED_TR)

MATCHEDA_[expr $indx+5]  : LINE=(LN0_MATCHED,BC1_MATCHED,BC1_DI_MATCHED,LN1_MATCHED,BC2_MATCHED)
TRACKA_[expr $indx+5]    : LINE=(BNCH,LN0_MATCHED_TR2,BC1_MATCHED_TR2,BC1_DI_MATCHED_TR2,LN1_MATCHED_TR2,BC2_MATCHED_TR2)

MATCHINS_[expr $indx+5] : LINE=(LN2_MATCH_TO)
MATCHEDS_[expr $indx+6] : LINE=(LN2_MATCHED)
TRACKINS_[expr $indx+6] : LINE=(BNCH,LN2_MATCHED_TR)

MATCHEDA_[expr $indx+6]  : LINE=(LN0_MATCHED,BC1_MATCHED,BC1_DI_MATCHED,LN1_MATCHED,BC2_MATCHED,LN2_MATCHED)
TRACKA_[expr $indx+6]    : LINE=(BNCH,LN0_MATCHED_TR2,BC1_MATCHED_TR2,BC1_DI_MATCHED_TR2,LN1_MATCHED_TR2,BC2_MATCHED_TR2,LN2_MATCHED_TR2)

MATCHINS_[expr $indx+6] : LINE=(LN2_DI_MATCH_TO)
MATCHEDS_[expr $indx+7] : LINE=(LN2_DI_MATCHED)
TRACKINS_[expr $indx+7] : LINE=(BNCH,LN2_DI_MATCHED_TR)

MATCHEDA_[expr $indx+7]  : LINE=(LN0_MATCHED,BC1_MATCHED,BC1_DI_MATCHED,LN1_MATCHED,BC2_MATCHED,LN2_MATCHED,LN2_DI_MATCHED)
TRACKA_[expr $indx+7]    : LINE=(BNCH,LN0_MATCHED_TR2,BC1_MATCHED_TR2,BC1_DI_MATCHED_TR2,LN1_MATCHED_TR2,BC2_MATCHED_TR2,LN2_MATCHED_TR2,LN2_DI_MATCHED_TR2)

"
    
    
    
} elseif {$case0 > 0 && $case1 > 0 && $case2 >0 && $case3 >0  && $case4 >0 && $case5 >0 && $case6 >0  && $case7 > 0 && $case8a <1 && $case8b <1 && $case9 <1  && $case10 <1 &&  $case11 <1  } {


puts $filee "


MATCHINS_[expr $indx+0] : LINE=(LN0_MATCH_TO)
MATCHEDS_[expr $indx+1] : LINE=(LN0_MATCHED)
TRACKINS_[expr $indx+1] : LINE=(BNCH,LN0_MATCHED_TR)

MATCHEDA_[expr $indx+1]  : LINE=(LN0_MATCHED)
TRACKA_[expr $indx+1]    : LINE=(BNCH,LN0_MATCHED_TR2)

MATCHINS_[expr $indx+1] : LINE=(BC1_MATCH_TO)
MATCHEDS_[expr $indx+2] : LINE=(BC1_MATCHED)
TRACKINS_[expr $indx+2] : LINE=(BNCH,BC1_MATCHED_TR)

MATCHEDA_[expr $indx+2]  : LINE=(LN0_MATCHED,BC1_MATCHED)
TRACKA_[expr $indx+2]    : LINE=(BNCH,LN0_MATCHED_TR2,BC1_MATCHED_TR2)

MATCHINS_[expr $indx+2] : LINE=(BC1_DI_MATCH_TO)
MATCHEDS_[expr $indx+3] : LINE=(BC1_DI_MATCHED)
TRACKINS_[expr $indx+3] : LINE=(BNCH,BC1_DI_MATCHED_TR)

MATCHEDA_[expr $indx+3]  : LINE=(LN0_MATCHED,BC1_MATCHED,BC1_DI_MATCHED)
TRACKA_[expr $indx+3]    : LINE=(BNCH,LN0_MATCHED_TR2,BC1_MATCHED_TR2,BC1_DI_MATCHED_TR2)

MATCHINS_[expr $indx+3] : LINE=(LN1_MATCH_TO)
MATCHEDS_[expr $indx+4] : LINE=(LN1_MATCHED)
TRACKINS_[expr $indx+4] : LINE=(BNCH,LN1_MATCHED_TR)

MATCHEDA_[expr $indx+4]  : LINE=(LN0_MATCHED,BC1_MATCHED,BC1_DI_MATCHED,LN1_MATCHED)
TRACKA_[expr $indx+4]    : LINE=(BNCH,LN0_MATCHED_TR2,BC1_MATCHED_TR2,BC1_DI_MATCHED_TR2,LN1_MATCHED_TR2)

MATCHINS_[expr $indx+4] : LINE=(BC2_MATCH_TO)
MATCHEDS_[expr $indx+5] : LINE=(BC2_MATCHED)
TRACKINS_[expr $indx+5] : LINE=(BNCH,BC2_MATCHED_TR)

MATCHEDA_[expr $indx+5]  : LINE=(LN0_MATCHED,BC1_MATCHED,BC1_DI_MATCHED,LN1_MATCHED,BC2_MATCHED)
TRACKA_[expr $indx+5]    : LINE=(BNCH,LN0_MATCHED_TR2,BC1_MATCHED_TR2,BC1_DI_MATCHED_TR2,LN1_MATCHED_TR2,BC2_MATCHED_TR2)

MATCHINS_[expr $indx+5] : LINE=(LN2_MATCH_TO)
MATCHEDS_[expr $indx+6] : LINE=(LN2_MATCHED)
TRACKINS_[expr $indx+6] : LINE=(BNCH,LN2_MATCHED_TR)

MATCHEDA_[expr $indx+6]  : LINE=(LN0_MATCHED,BC1_MATCHED,BC1_DI_MATCHED,LN1_MATCHED,BC2_MATCHED,LN2_MATCHED)
TRACKA_[expr $indx+6]    : LINE=(BNCH,LN0_MATCHED_TR2,BC1_MATCHED_TR2,BC1_DI_MATCHED_TR2,LN1_MATCHED_TR2,BC2_MATCHED_TR2,LN2_MATCHED_TR2)

MATCHINS_[expr $indx+6] : LINE=(LN2_DI_MATCH_TO)
MATCHEDS_[expr $indx+7] : LINE=(LN2_DI_MATCHED)
TRACKINS_[expr $indx+7] : LINE=(BNCH,LN2_DI_MATCHED_TR)

MATCHEDA_[expr $indx+7]  : LINE=(LN0_MATCHED,BC1_MATCHED,BC1_DI_MATCHED,LN1_MATCHED,BC2_MATCHED,LN2_MATCHED,LN2_DI_MATCHED)
TRACKA_[expr $indx+7]    : LINE=(BNCH,LN0_MATCHED_TR2,BC1_MATCHED_TR2,BC1_DI_MATCHED_TR2,LN1_MATCHED_TR2,BC2_MATCHED_TR2,LN2_MATCHED_TR2,LN2_DI_MATCHED_TR2)

MATCHINS_[expr $indx+7] : LINE=(LN2_BP_MATCH_TO)
MATCHEDS_[expr $indx+8] : LINE=(LN2_BP_MATCHED)
TRACKINS_[expr $indx+8] : LINE=(BNCH,LN2_BP_MATCHED_TR)

MATCHEDA_[expr $indx+8]  : LINE=(LN0_MATCHED,BC1_MATCHED,BC1_DI_MATCHED,LN1_MATCHED,BC2_MATCHED,LN2_MATCHED,LN2_DI_MATCHED,LN2_BP_MATCHED)
TRACKA_[expr $indx+8]    : LINE=(BNCH,LN0_MATCHED_TR2,BC1_MATCHED_TR2,BC1_DI_MATCHED_TR2,LN1_MATCHED_TR2,BC2_MATCHED_TR2,LN2_MATCHED_TR2,LN2_DI_MATCHED_TR2,LN2_BP_MATCHED_TR2)


"
} elseif {$case0 > 0 && $case1 > 0 && $case2 >0 && $case3 >0  && $case4 >0 && $case5 >0 && $case6 >0 && $case7 > 0 && $case8a >0 && $case8b <1  && $case9 <1 && $case10 <1 &&  $case11 <1} {
       ##  case lin0 + bc1 + bc1 diag +ln1 +bc2 +ln2 +ln2 diag+ln2 bp +ln3

puts $filee "
MATCHINS_[expr $indx+0] : LINE=(LN0_MATCH_TO)
MATCHEDS_[expr $indx+1] : LINE=(LN0_MATCHED)
TRACKINS_[expr $indx+1] : LINE=(BNCH,LN0_MATCHED_TR)

MATCHEDA_[expr $indx+1]  : LINE=(LN0_MATCHED)
TRACKA_[expr $indx+1]    : LINE=(BNCH,LN0_MATCHED_TR2)

MATCHINS_[expr $indx+1] : LINE=(BC1_MATCH_TO)
MATCHEDS_[expr $indx+2] : LINE=(BC1_MATCHED)
TRACKINS_[expr $indx+2] : LINE=(BNCH,BC1_MATCHED_TR)

MATCHEDA_[expr $indx+2]  : LINE=(LN0_MATCHED,BC1_MATCHED)
TRACKA_[expr $indx+2]    : LINE=(BNCH,LN0_MATCHED_TR2,BC1_MATCHED_TR2)

MATCHINS_[expr $indx+2] : LINE=(BC1_DI_MATCH_TO)
MATCHEDS_[expr $indx+3] : LINE=(BC1_DI_MATCHED)
TRACKINS_[expr $indx+3] : LINE=(BNCH,BC1_DI_MATCHED_TR)

MATCHEDA_[expr $indx+3]  : LINE=(LN0_MATCHED,BC1_MATCHED,BC1_DI_MATCHED)
TRACKA_[expr $indx+3]    : LINE=(BNCH,LN0_MATCHED_TR2,BC1_MATCHED_TR2,BC1_DI_MATCHED_TR2)

MATCHINS_[expr $indx+3] : LINE=(LN1_MATCH_TO)
MATCHEDS_[expr $indx+4] : LINE=(LN1_MATCHED)
TRACKINS_[expr $indx+4] : LINE=(BNCH,LN1_MATCHED_TR)

MATCHEDA_[expr $indx+4]  : LINE=(LN0_MATCHED,BC1_MATCHED,BC1_DI_MATCHED,LN1_MATCHED)
TRACKA_[expr $indx+4]    : LINE=(BNCH,LN0_MATCHED_TR2,BC1_MATCHED_TR2,BC1_DI_MATCHED_TR2,LN1_MATCHED_TR2)

MATCHINS_[expr $indx+4] : LINE=(BC2_MATCH_TO)
MATCHEDS_[expr $indx+5] : LINE=(BC2_MATCHED)
TRACKINS_[expr $indx+5] : LINE=(BNCH,BC2_MATCHED_TR)

MATCHEDA_[expr $indx+5]  : LINE=(LN0_MATCHED,BC1_MATCHED,BC1_DI_MATCHED,LN1_MATCHED,BC2_MATCHED)
TRACKA_[expr $indx+5]    : LINE=(BNCH,LN0_MATCHED_TR2,BC1_MATCHED_TR2,BC1_DI_MATCHED_TR2,LN1_MATCHED_TR2,BC2_MATCHED_TR2)

MATCHINS_[expr $indx+5] : LINE=(LN2_MATCH_TO)
MATCHEDS_[expr $indx+6] : LINE=(LN2_MATCHED)
TRACKINS_[expr $indx+6] : LINE=(BNCH,LN2_MATCHED_TR)

MATCHEDA_[expr $indx+6]  : LINE=(LN0_MATCHED,BC1_MATCHED,BC1_DI_MATCHED,LN1_MATCHED,BC2_MATCHED,LN2_MATCHED)
TRACKA_[expr $indx+6]    : LINE=(BNCH,LN0_MATCHED_TR2,BC1_MATCHED_TR2,BC1_DI_MATCHED_TR2,LN1_MATCHED_TR2,BC2_MATCHED_TR2,LN2_MATCHED_TR2)

MATCHINS_[expr $indx+6] : LINE=(LN2_DI_MATCH_TO)
MATCHEDS_[expr $indx+7] : LINE=(LN2_DI_MATCHED)
TRACKINS_[expr $indx+7] : LINE=(BNCH,LN2_DI_MATCHED_TR)

MATCHEDA_[expr $indx+7]  : LINE=(LN0_MATCHED,BC1_MATCHED,BC1_DI_MATCHED,LN1_MATCHED,BC2_MATCHED,LN2_MATCHED,LN2_DI_MATCHED)
TRACKA_[expr $indx+7]    : LINE=(BNCH,LN0_MATCHED_TR2,BC1_MATCHED_TR2,BC1_DI_MATCHED_TR2,LN1_MATCHED_TR2,BC2_MATCHED_TR2,LN2_MATCHED_TR2,LN2_DI_MATCHED_TR2)

MATCHINS_[expr $indx+7] : LINE=(LN2_BP_MATCH_TO)
MATCHEDS_[expr $indx+8] : LINE=(LN2_BP_MATCHED)
TRACKINS_[expr $indx+8] : LINE=(BNCH,LN2_BP_MATCHED_TR)

MATCHEDA_[expr $indx+8]  : LINE=(LN0_MATCHED,BC1_MATCHED,BC1_DI_MATCHED,LN1_MATCHED,BC2_MATCHED,LN2_MATCHED,LN2_DI_MATCHED,LN2_BP_MATCHED)
TRACKA_[expr $indx+8]    : LINE=(BNCH,LN0_MATCHED_TR2,BC1_MATCHED_TR2,BC1_DI_MATCHED_TR2,LN1_MATCHED_TR2,BC2_MATCHED_TR2,LN2_MATCHED_TR2,LN2_DI_MATCHED_TR2,LN2_BP_MATCHED_TR2)

MATCHINS_[expr $indx+8] : LINE=(SBP_MATCH_TO_1)
MATCHEDS_[expr $indx+9] : LINE=(SBP_MATCHED_1)
TRACKINS_[expr $indx+9] : LINE=(BNCH,SBP_MATCHED_TR_1)

MATCHEDA_[expr $indx+9]  : LINE=(LN0_MATCHED,BC1_MATCHED,BC1_DI_MATCHED,LN1_MATCHED,BC2_MATCHED,LN2_MATCHED,LN2_DI_MATCHED,LN2_BP_MATCHED,SBP_MATCHED_1)
TRACKA_[expr $indx+9]    : LINE=(BNCH,LN0_MATCHED_TR2,BC1_MATCHED_TR2,BC1_DI_MATCHED_TR2,LN1_MATCHED_TR2,BC2_MATCHED_TR2,LN2_MATCHED_TR2,LN2_DI_MATCHED_TR2,LN2_BP_MATCHED_TR2,SBP_MATCHED_TR_12)


"
} elseif {$case0 > 0 && $case1 > 0 && $case2 >0 && $case3 >0  && $case4 >0 && $case5 >0 && $case6 >0 && $case7 > 0 && $case8a >0 && $case8b >0  && $case9 <1 && $case10 <1 &&  $case11 <1 } {
    
##  case lin0 + bc1 + bc1 diag +ln1 +bc2 +ln2 +sxbp2
puts $filee "
MATCHINS_[expr $indx+0] : LINE=(LN0_MATCH_TO)
MATCHEDS_[expr $indx+1] : LINE=(LN0_MATCHED)
TRACKINS_[expr $indx+1] : LINE=(BNCH,LN0_MATCHED_TR)

MATCHEDA_[expr $indx+1]  : LINE=(LN0_MATCHED)
TRACKA_[expr $indx+1]    : LINE=(BNCH,LN0_MATCHED_TR2)

MATCHINS_[expr $indx+1] : LINE=(BC1_MATCH_TO)
MATCHEDS_[expr $indx+2] : LINE=(BC1_MATCHED)
TRACKINS_[expr $indx+2] : LINE=(BNCH,BC1_MATCHED_TR)

MATCHEDA_[expr $indx+2]  : LINE=(LN0_MATCHED,BC1_MATCHED)
TRACKA_[expr $indx+2]    : LINE=(BNCH,LN0_MATCHED_TR2,BC1_MATCHED_TR2)

MATCHINS_[expr $indx+2] : LINE=(BC1_DI_MATCH_TO)
MATCHEDS_[expr $indx+3] : LINE=(BC1_DI_MATCHED)
TRACKINS_[expr $indx+3] : LINE=(BNCH,BC1_DI_MATCHED_TR)

MATCHEDA_[expr $indx+3]  : LINE=(LN0_MATCHED,BC1_MATCHED,BC1_DI_MATCHED)
TRACKA_[expr $indx+3]    : LINE=(BNCH,LN0_MATCHED_TR2,BC1_MATCHED_TR2,BC1_DI_MATCHED_TR2)

MATCHINS_[expr $indx+3] : LINE=(LN1_MATCH_TO)
MATCHEDS_[expr $indx+4] : LINE=(LN1_MATCHED)
TRACKINS_[expr $indx+4] : LINE=(BNCH,LN1_MATCHED_TR)

MATCHEDA_[expr $indx+4]  : LINE=(LN0_MATCHED,BC1_MATCHED,BC1_DI_MATCHED,LN1_MATCHED)
TRACKA_[expr $indx+4]    : LINE=(BNCH,LN0_MATCHED_TR2,BC1_MATCHED_TR2,BC1_DI_MATCHED_TR2,LN1_MATCHED_TR2)

MATCHINS_[expr $indx+4] : LINE=(BC2_MATCH_TO)
MATCHEDS_[expr $indx+5] : LINE=(BC2_MATCHED)
TRACKINS_[expr $indx+5] : LINE=(BNCH,BC2_MATCHED_TR)

MATCHEDA_[expr $indx+5]  : LINE=(LN0_MATCHED,BC1_MATCHED,BC1_DI_MATCHED,LN1_MATCHED,BC2_MATCHED)
TRACKA_[expr $indx+5]    : LINE=(BNCH,LN0_MATCHED_TR2,BC1_MATCHED_TR2,BC1_DI_MATCHED_TR2,LN1_MATCHED_TR2,BC2_MATCHED_TR2)

MATCHINS_[expr $indx+5] : LINE=(LN2_MATCH_TO)
MATCHEDS_[expr $indx+6] : LINE=(LN2_MATCHED)
TRACKINS_[expr $indx+6] : LINE=(BNCH,LN2_MATCHED_TR)

MATCHEDA_[expr $indx+6]  : LINE=(LN0_MATCHED,BC1_MATCHED,BC1_DI_MATCHED,LN1_MATCHED,BC2_MATCHED,LN2_MATCHED)
TRACKA_[expr $indx+6]    : LINE=(BNCH,LN0_MATCHED_TR2,BC1_MATCHED_TR2,BC1_DI_MATCHED_TR2,LN1_MATCHED_TR2,BC2_MATCHED_TR2,LN2_MATCHED_TR2)

MATCHINS_[expr $indx+6] : LINE=(LN2_DI_MATCH_TO)
MATCHEDS_[expr $indx+7] : LINE=(LN2_DI_MATCHED)
TRACKINS_[expr $indx+7] : LINE=(BNCH,LN2_DI_MATCHED_TR)

MATCHEDA_[expr $indx+7]  : LINE=(LN0_MATCHED,BC1_MATCHED,BC1_DI_MATCHED,LN1_MATCHED,BC2_MATCHED,LN2_MATCHED,LN2_DI_MATCHED)
TRACKA_[expr $indx+7]    : LINE=(BNCH,LN0_MATCHED_TR2,BC1_MATCHED_TR2,BC1_DI_MATCHED_TR2,LN1_MATCHED_TR2,BC2_MATCHED_TR2,LN2_MATCHED_TR2,LN2_DI_MATCHED_TR2)

MATCHINS_[expr $indx+7] : LINE=(LN2_BP_MATCH_TO)
MATCHEDS_[expr $indx+8] : LINE=(LN2_BP_MATCHED)
TRACKINS_[expr $indx+8] : LINE=(BNCH,LN2_BP_MATCHED_TR)

MATCHEDA_[expr $indx+8]  : LINE=(LN0_MATCHED,BC1_MATCHED,BC1_DI_MATCHED,LN1_MATCHED,BC2_MATCHED,LN2_MATCHED,LN2_DI_MATCHED,LN2_BP_MATCHED)
TRACKA_[expr $indx+8]    : LINE=(BNCH,LN0_MATCHED_TR2,BC1_MATCHED_TR2,BC1_DI_MATCHED_TR2,LN1_MATCHED_TR2,BC2_MATCHED_TR2,LN2_MATCHED_TR2,LN2_DI_MATCHED_TR2,LN2_BP_MATCHED_TR2)

MATCHINS_[expr $indx+8] : LINE=(SBP_MATCH_TO_1)
MATCHEDS_[expr $indx+9] : LINE=(SBP_MATCHED_1)
TRACKINS_[expr $indx+9] : LINE=(BNCH,SBP_MATCHED_TR_1)

MATCHEDA_[expr $indx+9]  : LINE=(LN0_MATCHED,BC1_MATCHED,BC1_DI_MATCHED,LN1_MATCHED,BC2_MATCHED,LN2_MATCHED,LN2_DI_MATCHED,LN2_BP_MATCHED,SBP_MATCHED_1)
TRACKA_[expr $indx+9]    : LINE=(BNCH,LN0_MATCHED_TR2,BC1_MATCHED_TR2,BC1_DI_MATCHED_TR2,LN1_MATCHED_TR2,BC2_MATCHED_TR2,LN2_MATCHED_TR2,LN2_DI_MATCHED_TR2,LN2_BP_MATCHED_TR2,SBP_MATCHED_TR_12)


MATCHINS_[expr $indx+9] : LINE=(SBP_MATCH_TO_2)
MATCHEDS_[expr $indx+10] : LINE=(SBP_MATCHED_2,SBP_MATCHED_3)
TRACKINS_[expr $indx+10] : LINE=(BNCH,SBP_MATCHED_TR_2,SBP_MATCHED_TR_3)

MATCHEDA_[expr $indx+10]  : LINE=(LN0_MATCHED,BC1_MATCHED,BC1_DI_MATCHED,LN1_MATCHED,BC2_MATCHED,LN2_MATCHED,LN2_DI_MATCHED,LN2_BP_MATCHED,SBP_MATCHED_1,SBP_MATCHED_2,SBP_MATCHED_3)
TRACKA_[expr $indx+10]    : LINE=(BNCH,LN0_MATCHED_TR2,BC1_MATCHED_TR2,BC1_DI_MATCHED_TR2,LN1_MATCHED_TR2,BC2_MATCHED_TR2,LN2_MATCHED_TR2,LN2_DI_MATCHED_TR2,LN2_BP_MATCHED_TR2,SBP_MATCHED_TR_12,SBP_MATCHED_TR_22,SBP_MATCHED_TR_32)

"
    
} elseif {$case0 > 0 && $case1 > 0 && $case2 >0 && $case3 >0  && $case4 >0 && $case5 >0 && $case6 >0 && $case7 > 0 && $case8a >0 && $case8b >0  && $case9 >0 && $case10 <1 &&  $case11 <1 } {
    
   ##  case lin0 + bc1 + bc1 diag +ln1 +bc2 +ln2 +sxbp + ln4  

puts $filee "
MATCHINS_[expr $indx+0] : LINE=(LN0_MATCH_TO)
MATCHEDS_[expr $indx+1] : LINE=(LN0_MATCHED)
TRACKINS_[expr $indx+1] : LINE=(BNCH,LN0_MATCHED_TR)

MATCHEDA_[expr $indx+1]  : LINE=(LN0_MATCHED)
TRACKA_[expr $indx+1]    : LINE=(BNCH,LN0_MATCHED_TR2)

MATCHINS_[expr $indx+1] : LINE=(BC1_MATCH_TO)
MATCHEDS_[expr $indx+2] : LINE=(BC1_MATCHED)
TRACKINS_[expr $indx+2] : LINE=(BNCH,BC1_MATCHED_TR)

MATCHEDA_[expr $indx+2]  : LINE=(LN0_MATCHED,BC1_MATCHED)
TRACKA_[expr $indx+2]    : LINE=(BNCH,LN0_MATCHED_TR2,BC1_MATCHED_TR2)

MATCHINS_[expr $indx+2] : LINE=(BC1_DI_MATCH_TO)
MATCHEDS_[expr $indx+3] : LINE=(BC1_DI_MATCHED)
TRACKINS_[expr $indx+3] : LINE=(BNCH,BC1_DI_MATCHED_TR)

MATCHEDA_[expr $indx+3]  : LINE=(LN0_MATCHED,BC1_MATCHED,BC1_DI_MATCHED)
TRACKA_[expr $indx+3]    : LINE=(BNCH,LN0_MATCHED_TR2,BC1_MATCHED_TR2,BC1_DI_MATCHED_TR2)

MATCHINS_[expr $indx+3] : LINE=(LN1_MATCH_TO)
MATCHEDS_[expr $indx+4] : LINE=(LN1_MATCHED)
TRACKINS_[expr $indx+4] : LINE=(BNCH,LN1_MATCHED_TR)

MATCHEDA_[expr $indx+4]  : LINE=(LN0_MATCHED,BC1_MATCHED,BC1_DI_MATCHED,LN1_MATCHED)
TRACKA_[expr $indx+4]    : LINE=(BNCH,LN0_MATCHED_TR2,BC1_MATCHED_TR2,BC1_DI_MATCHED_TR2,LN1_MATCHED_TR2)

MATCHINS_[expr $indx+4] : LINE=(BC2_MATCH_TO)
MATCHEDS_[expr $indx+5] : LINE=(BC2_MATCHED)
TRACKINS_[expr $indx+5] : LINE=(BNCH,BC2_MATCHED_TR)

MATCHEDA_[expr $indx+5]  : LINE=(LN0_MATCHED,BC1_MATCHED,BC1_DI_MATCHED,LN1_MATCHED,BC2_MATCHED)
TRACKA_[expr $indx+5]    : LINE=(BNCH,LN0_MATCHED_TR2,BC1_MATCHED_TR2,BC1_DI_MATCHED_TR2,LN1_MATCHED_TR2,BC2_MATCHED_TR2)

MATCHINS_[expr $indx+5] : LINE=(LN2_MATCH_TO)
MATCHEDS_[expr $indx+6] : LINE=(LN2_MATCHED)
TRACKINS_[expr $indx+6] : LINE=(BNCH,LN2_MATCHED_TR)

MATCHEDA_[expr $indx+6]  : LINE=(LN0_MATCHED,BC1_MATCHED,BC1_DI_MATCHED,LN1_MATCHED,BC2_MATCHED,LN2_MATCHED)
TRACKA_[expr $indx+6]    : LINE=(BNCH,LN0_MATCHED_TR2,BC1_MATCHED_TR2,BC1_DI_MATCHED_TR2,LN1_MATCHED_TR2,BC2_MATCHED_TR2,LN2_MATCHED_TR2)

MATCHINS_[expr $indx+6] : LINE=(LN2_DI_MATCH_TO)
MATCHEDS_[expr $indx+7] : LINE=(LN2_DI_MATCHED)
TRACKINS_[expr $indx+7] : LINE=(BNCH,LN2_DI_MATCHED_TR)

MATCHEDA_[expr $indx+7]  : LINE=(LN0_MATCHED,BC1_MATCHED,BC1_DI_MATCHED,LN1_MATCHED,BC2_MATCHED,LN2_MATCHED,LN2_DI_MATCHED)
TRACKA_[expr $indx+7]    : LINE=(BNCH,LN0_MATCHED_TR2,BC1_MATCHED_TR2,BC1_DI_MATCHED_TR2,LN1_MATCHED_TR2,BC2_MATCHED_TR2,LN2_MATCHED_TR2,LN2_DI_MATCHED_TR2)

MATCHINS_[expr $indx+7] : LINE=(LN2_BP_MATCH_TO)
MATCHEDS_[expr $indx+8] : LINE=(LN2_BP_MATCHED)
TRACKINS_[expr $indx+8] : LINE=(BNCH,LN2_BP_MATCHED_TR)

MATCHEDA_[expr $indx+8]  : LINE=(LN0_MATCHED,BC1_MATCHED,BC1_DI_MATCHED,LN1_MATCHED,BC2_MATCHED,LN2_MATCHED,LN2_DI_MATCHED,LN2_BP_MATCHED)
TRACKA_[expr $indx+8]    : LINE=(BNCH,LN0_MATCHED_TR2,BC1_MATCHED_TR2,BC1_DI_MATCHED_TR2,LN1_MATCHED_TR2,BC2_MATCHED_TR2,LN2_MATCHED_TR2,LN2_DI_MATCHED_TR2,LN2_BP_MATCHED_TR2)

MATCHINS_[expr $indx+8] : LINE=(SBP_MATCH_TO_1)
MATCHEDS_[expr $indx+9] : LINE=(SBP_MATCHED_1)
TRACKINS_[expr $indx+9] : LINE=(BNCH,SBP_MATCHED_TR_1)

MATCHEDA_[expr $indx+9]  : LINE=(LN0_MATCHED,BC1_MATCHED,BC1_DI_MATCHED,LN1_MATCHED,BC2_MATCHED,LN2_MATCHED,LN2_DI_MATCHED,LN2_BP_MATCHED,SBP_MATCHED_1)
TRACKA_[expr $indx+9]    : LINE=(BNCH,LN0_MATCHED_TR2,BC1_MATCHED_TR2,BC1_DI_MATCHED_TR2,LN1_MATCHED_TR2,BC2_MATCHED_TR2,LN2_MATCHED_TR2,LN2_DI_MATCHED_TR2,LN2_BP_MATCHED_TR2,SBP_MATCHED_TR_12)


MATCHINS_[expr $indx+9] : LINE=(SBP_MATCH_TO_2)
MATCHEDS_[expr $indx+10] : LINE=(SBP_MATCHED_2,SBP_MATCHED_3)
TRACKINS_[expr $indx+10] : LINE=(BNCH,SBP_MATCHED_TR_2,SBP_MATCHED_TR_3)

MATCHEDA_[expr $indx+10]  : LINE=(LN0_MATCHED,BC1_MATCHED,BC1_DI_MATCHED,LN1_MATCHED,BC2_MATCHED,LN2_MATCHED,LN2_DI_MATCHED,LN2_BP_MATCHED,SBP_MATCHED_1,SBP_MATCHED_2,SBP_MATCHED_3)
TRACKA_[expr $indx+10]    : LINE=(BNCH,LN0_MATCHED_TR2,BC1_MATCHED_TR2,BC1_DI_MATCHED_TR2,LN1_MATCHED_TR2,BC2_MATCHED_TR2,LN2_MATCHED_TR2,LN2_DI_MATCHED_TR2,LN2_BP_MATCHED_TR2,SBP_MATCHED_TR_12,SBP_MATCHED_TR_22,SBP_MATCHED_TR_32)

MATCHINS_[expr $indx+10] : LINE=(LN4_MATCH_TO)
MATCHEDS_[expr $indx+11] : LINE=(LN4_MATCHED)
TRACKINS_[expr $indx+11] : LINE=(BNCH,LN4_MATCHED_TR)

MATCHEDA_[expr $indx+11]  : LINE=(LN0_MATCHED,BC1_MATCHED,BC1_DI_MATCHED,LN1_MATCHED,BC2_MATCHED,LN2_MATCHED,LN2_DI_MATCHED,LN2_BP_MATCHED,SBP_MATCHED_1,SBP_MATCHED_2,SBP_MATCHED_3,LN4_MATCHED)
TRACKA_[expr $indx+11]    : LINE=(BNCH,LN0_MATCHED_TR2,BC1_MATCHED_TR2,BC1_DI_MATCHED_TR2,LN1_MATCHED_TR2,BC2_MATCHED_TR2,LN2_MATCHED_TR2,LN2_DI_MATCHED_TR2,LN2_BP_MATCHED_TR2,SBP_MATCHED_TR_12,SBP_MATCHED_TR_22,SBP_MATCHED_TR_32,LN4_MATCHED_TR2 )

"


    
} elseif {$case0 > 0 && $case1 > 0 && $case2 >0 && $case3 >0  && $case4 >0 && $case5 >0 && $case6 >0 && $case7 > 0 && $case8a >0 && $case8b >0  && $case9 >0 && $case10 >0 &&  $case11 <1 } {
    
   ##  case lin0 + bc1 + bc1 diag +ln1 +bc2 +ln2 +sxbp + ln4  +dr

puts $filee "
MATCHINS_[expr $indx+0] : LINE=(LN0_MATCH_TO)
MATCHEDS_[expr $indx+1] : LINE=(LN0_MATCHED)
TRACKINS_[expr $indx+1] : LINE=(BNCH,LN0_MATCHED_TR)

MATCHEDA_[expr $indx+1]  : LINE=(LN0_MATCHED)
TRACKA_[expr $indx+1]    : LINE=(BNCH,LN0_MATCHED_TR2)

MATCHINS_[expr $indx+1] : LINE=(BC1_MATCH_TO)
MATCHEDS_[expr $indx+2] : LINE=(BC1_MATCHED)
TRACKINS_[expr $indx+2] : LINE=(BNCH,BC1_MATCHED_TR)

MATCHEDA_[expr $indx+2]  : LINE=(LN0_MATCHED,BC1_MATCHED)
TRACKA_[expr $indx+2]    : LINE=(BNCH,LN0_MATCHED_TR2,BC1_MATCHED_TR2)

MATCHINS_[expr $indx+2] : LINE=(BC1_DI_MATCH_TO)
MATCHEDS_[expr $indx+3] : LINE=(BC1_DI_MATCHED)
TRACKINS_[expr $indx+3] : LINE=(BNCH,BC1_DI_MATCHED_TR)

MATCHEDA_[expr $indx+3]  : LINE=(LN0_MATCHED,BC1_MATCHED,BC1_DI_MATCHED)
TRACKA_[expr $indx+3]    : LINE=(BNCH,LN0_MATCHED_TR2,BC1_MATCHED_TR2,BC1_DI_MATCHED_TR2)

MATCHINS_[expr $indx+3] : LINE=(LN1_MATCH_TO)
MATCHEDS_[expr $indx+4] : LINE=(LN1_MATCHED)
TRACKINS_[expr $indx+4] : LINE=(BNCH,LN1_MATCHED_TR)

MATCHEDA_[expr $indx+4]  : LINE=(LN0_MATCHED,BC1_MATCHED,BC1_DI_MATCHED,LN1_MATCHED)
TRACKA_[expr $indx+4]    : LINE=(BNCH,LN0_MATCHED_TR2,BC1_MATCHED_TR2,BC1_DI_MATCHED_TR2,LN1_MATCHED_TR2)

MATCHINS_[expr $indx+4] : LINE=(BC2_MATCH_TO)
MATCHEDS_[expr $indx+5] : LINE=(BC2_MATCHED)
TRACKINS_[expr $indx+5] : LINE=(BNCH,BC2_MATCHED_TR)

MATCHEDA_[expr $indx+5]  : LINE=(LN0_MATCHED,BC1_MATCHED,BC1_DI_MATCHED,LN1_MATCHED,BC2_MATCHED)
TRACKA_[expr $indx+5]    : LINE=(BNCH,LN0_MATCHED_TR2,BC1_MATCHED_TR2,BC1_DI_MATCHED_TR2,LN1_MATCHED_TR2,BC2_MATCHED_TR2)

MATCHINS_[expr $indx+5] : LINE=(LN2_MATCH_TO)
MATCHEDS_[expr $indx+6] : LINE=(LN2_MATCHED)
TRACKINS_[expr $indx+6] : LINE=(BNCH,LN2_MATCHED_TR)

MATCHEDA_[expr $indx+6]  : LINE=(LN0_MATCHED,BC1_MATCHED,BC1_DI_MATCHED,LN1_MATCHED,BC2_MATCHED,LN2_MATCHED)
TRACKA_[expr $indx+6]    : LINE=(BNCH,LN0_MATCHED_TR2,BC1_MATCHED_TR2,BC1_DI_MATCHED_TR2,LN1_MATCHED_TR2,BC2_MATCHED_TR2,LN2_MATCHED_TR2)

MATCHINS_[expr $indx+6] : LINE=(LN2_DI_MATCH_TO)
MATCHEDS_[expr $indx+7] : LINE=(LN2_DI_MATCHED)
TRACKINS_[expr $indx+7] : LINE=(BNCH,LN2_DI_MATCHED_TR)

MATCHEDA_[expr $indx+7]  : LINE=(LN0_MATCHED,BC1_MATCHED,BC1_DI_MATCHED,LN1_MATCHED,BC2_MATCHED,LN2_MATCHED,LN2_DI_MATCHED)
TRACKA_[expr $indx+7]    : LINE=(BNCH,LN0_MATCHED_TR2,BC1_MATCHED_TR2,BC1_DI_MATCHED_TR2,LN1_MATCHED_TR2,BC2_MATCHED_TR2,LN2_MATCHED_TR2,LN2_DI_MATCHED_TR2)

MATCHINS_[expr $indx+7] : LINE=(LN2_BP_MATCH_TO)
MATCHEDS_[expr $indx+8] : LINE=(LN2_BP_MATCHED)
TRACKINS_[expr $indx+8] : LINE=(BNCH,LN2_BP_MATCHED_TR)

MATCHEDA_[expr $indx+8]  : LINE=(LN0_MATCHED,BC1_MATCHED,BC1_DI_MATCHED,LN1_MATCHED,BC2_MATCHED,LN2_MATCHED,LN2_DI_MATCHED,LN2_BP_MATCHED)
TRACKA_[expr $indx+8]    : LINE=(BNCH,LN0_MATCHED_TR2,BC1_MATCHED_TR2,BC1_DI_MATCHED_TR2,LN1_MATCHED_TR2,BC2_MATCHED_TR2,LN2_MATCHED_TR2,LN2_DI_MATCHED_TR2,LN2_BP_MATCHED_TR2)

MATCHINS_[expr $indx+8] : LINE=(SBP_MATCH_TO_1)
MATCHEDS_[expr $indx+9] : LINE=(SBP_MATCHED_1)
TRACKINS_[expr $indx+9] : LINE=(BNCH,SBP_MATCHED_TR_1)

MATCHEDA_[expr $indx+9]  : LINE=(LN0_MATCHED,BC1_MATCHED,BC1_DI_MATCHED,LN1_MATCHED,BC2_MATCHED,LN2_MATCHED,LN2_DI_MATCHED,LN2_BP_MATCHED,SBP_MATCHED_1)
TRACKA_[expr $indx+9]    : LINE=(BNCH,LN0_MATCHED_TR2,BC1_MATCHED_TR2,BC1_DI_MATCHED_TR2,LN1_MATCHED_TR2,BC2_MATCHED_TR2,LN2_MATCHED_TR2,LN2_DI_MATCHED_TR2,LN2_BP_MATCHED_TR2,SBP_MATCHED_TR_12)


MATCHINS_[expr $indx+9] : LINE=(SBP_MATCH_TO_2)
MATCHEDS_[expr $indx+10] : LINE=(SBP_MATCHED_2,SBP_MATCHED_3)
TRACKINS_[expr $indx+10] : LINE=(BNCH,SBP_MATCHED_TR_2,SBP_MATCHED_TR_3)

MATCHEDA_[expr $indx+10]  : LINE=(LN0_MATCHED,BC1_MATCHED,BC1_DI_MATCHED,LN1_MATCHED,BC2_MATCHED,LN2_MATCHED,LN2_DI_MATCHED,LN2_BP_MATCHED,SBP_MATCHED_1,SBP_MATCHED_2,SBP_MATCHED_3)
TRACKA_[expr $indx+10]    : LINE=(BNCH,LN0_MATCHED_TR2,BC1_MATCHED_TR2,BC1_DI_MATCHED_TR2,LN1_MATCHED_TR2,BC2_MATCHED_TR2,LN2_MATCHED_TR2,LN2_DI_MATCHED_TR2,LN2_BP_MATCHED_TR2,SBP_MATCHED_TR_12,SBP_MATCHED_TR_22,SBP_MATCHED_TR_32)

MATCHINS_[expr $indx+10] : LINE=(LN4_MATCH_TO)
MATCHEDS_[expr $indx+11] : LINE=(LN4_MATCHED)
TRACKINS_[expr $indx+11] : LINE=(BNCH,LN4_MATCHED_TR)

MATCHEDA_[expr $indx+11]  : LINE=(LN0_MATCHED,BC1_MATCHED,BC1_DI_MATCHED,LN1_MATCHED,BC2_MATCHED,LN2_MATCHED,LN2_DI_MATCHED,LN2_BP_MATCHED,SBP_MATCHED_1,SBP_MATCHED_2,SBP_MATCHED_3,LN4_MATCHED)
TRACKA_[expr $indx+11]    : LINE=(BNCH,LN0_MATCHED_TR2,BC1_MATCHED_TR2,BC1_DI_MATCHED_TR2,LN1_MATCHED_TR2,BC2_MATCHED_TR2,LN2_MATCHED_TR2,LN2_DI_MATCHED_TR2,LN2_BP_MATCHED_TR2,SBP_MATCHED_TR_12,SBP_MATCHED_TR_22,SBP_MATCHED_TR_32,LN4_MATCHED_TR2 )

MATCHINS_[expr $indx+11] : LINE=(DR4_MATCH_TO)
MATCHEDS_[expr $indx+12] : LINE=(DR4_MATCHED)
TRACKINS_[expr $indx+12] : LINE=(BNCH,DR4_MATCHED_TR)

MATCHEDA_[expr $indx+12]  : LINE=(LN0_MATCHED,BC1_MATCHED,BC1_DI_MATCHED,LN1_MATCHED,BC2_MATCHED,LN2_MATCHED,LN2_DI_MATCHED,LN2_BP_MATCHED,SBP_MATCHED_1,SBP_MATCHED_2,SBP_MATCHED_3,LN4_MATCHED,DR4_MATCH_TO)
TRACKA_[expr $indx+12]    : LINE=(BNCH,LN0_MATCHED_TR2,BC1_MATCHED_TR2,BC1_DI_MATCHED_TR2,LN1_MATCHED_TR2,BC2_MATCHED_TR2,LN2_MATCHED_TR2,LN2_DI_MATCHED_TR2,LN2_BP_MATCHED_TR2,SBP_MATCHED_TR_12,SBP_MATCHED_TR_22,SBP_MATCHED_TR_32,LN4_MATCHED_TR2,DR4_MATCHED_TR2 )

"


} elseif {$case0 > 0 && $case1 > 0 && $case2 >0 && $case3 >0  && $case4 >0 && $case5 >0 && $case6 >0 && $case7 > 0 && $case8a >0 && $case8b >0  && $case9 >0 && $case10 >0 && $case11 >0} {
    
    ##  case lin0 + bc1 + bc1 diag +ln1 +bc2 +ln2 +sxbp + ln4  +dr + sxr


puts $filee "
MATCHINS_[expr $indx+0] : LINE=(LN0_MATCH_TO)
MATCHEDS_[expr $indx+1] : LINE=(LN0_MATCHED)
TRACKINS_[expr $indx+1] : LINE=(BNCH,LN0_MATCHED_TR)

MATCHEDA_[expr $indx+1]  : LINE=(LN0_MATCHED)
TRACKA_[expr $indx+1]    : LINE=(BNCH,LN0_MATCHED_TR2)

MATCHINS_[expr $indx+1] : LINE=(BC1_MATCH_TO)
MATCHEDS_[expr $indx+2] : LINE=(BC1_MATCHED)
TRACKINS_[expr $indx+2] : LINE=(BNCH,BC1_MATCHED_TR)

MATCHEDA_[expr $indx+2]  : LINE=(LN0_MATCHED,BC1_MATCHED)
TRACKA_[expr $indx+2]    : LINE=(BNCH,LN0_MATCHED_TR2,BC1_MATCHED_TR2)

MATCHINS_[expr $indx+2] : LINE=(BC1_DI_MATCH_TO)
MATCHEDS_[expr $indx+3] : LINE=(BC1_DI_MATCHED)
TRACKINS_[expr $indx+3] : LINE=(BNCH,BC1_DI_MATCHED_TR)

MATCHEDA_[expr $indx+3]  : LINE=(LN0_MATCHED,BC1_MATCHED,BC1_DI_MATCHED)
TRACKA_[expr $indx+3]    : LINE=(BNCH,LN0_MATCHED_TR2,BC1_MATCHED_TR2,BC1_DI_MATCHED_TR2)

MATCHINS_[expr $indx+3] : LINE=(LN1_MATCH_TO)
MATCHEDS_[expr $indx+4] : LINE=(LN1_MATCHED)
TRACKINS_[expr $indx+4] : LINE=(BNCH,LN1_MATCHED_TR)

MATCHEDA_[expr $indx+4]  : LINE=(LN0_MATCHED,BC1_MATCHED,BC1_DI_MATCHED,LN1_MATCHED)
TRACKA_[expr $indx+4]    : LINE=(BNCH,LN0_MATCHED_TR2,BC1_MATCHED_TR2,BC1_DI_MATCHED_TR2,LN1_MATCHED_TR2)

MATCHINS_[expr $indx+4] : LINE=(BC2_MATCH_TO)
MATCHEDS_[expr $indx+5] : LINE=(BC2_MATCHED)
TRACKINS_[expr $indx+5] : LINE=(BNCH,BC2_MATCHED_TR)

MATCHEDA_[expr $indx+5]  : LINE=(LN0_MATCHED,BC1_MATCHED,BC1_DI_MATCHED,LN1_MATCHED,BC2_MATCHED)
TRACKA_[expr $indx+5]    : LINE=(BNCH,LN0_MATCHED_TR2,BC1_MATCHED_TR2,BC1_DI_MATCHED_TR2,LN1_MATCHED_TR2,BC2_MATCHED_TR2)

MATCHINS_[expr $indx+5] : LINE=(LN2_MATCH_TO)
MATCHEDS_[expr $indx+6] : LINE=(LN2_MATCHED)
TRACKINS_[expr $indx+6] : LINE=(BNCH,LN2_MATCHED_TR)

MATCHEDA_[expr $indx+6]  : LINE=(LN0_MATCHED,BC1_MATCHED,BC1_DI_MATCHED,LN1_MATCHED,BC2_MATCHED,LN2_MATCHED)
TRACKA_[expr $indx+6]    : LINE=(BNCH,LN0_MATCHED_TR2,BC1_MATCHED_TR2,BC1_DI_MATCHED_TR2,LN1_MATCHED_TR2,BC2_MATCHED_TR2,LN2_MATCHED_TR2)

MATCHINS_[expr $indx+6] : LINE=(LN2_DI_MATCH_TO)
MATCHEDS_[expr $indx+7] : LINE=(LN2_DI_MATCHED)
TRACKINS_[expr $indx+7] : LINE=(BNCH,LN2_DI_MATCHED_TR)

MATCHEDA_[expr $indx+7]  : LINE=(LN0_MATCHED,BC1_MATCHED,BC1_DI_MATCHED,LN1_MATCHED,BC2_MATCHED,LN2_MATCHED,LN2_DI_MATCHED)
TRACKA_[expr $indx+7]    : LINE=(BNCH,LN0_MATCHED_TR2,BC1_MATCHED_TR2,BC1_DI_MATCHED_TR2,LN1_MATCHED_TR2,BC2_MATCHED_TR2,LN2_MATCHED_TR2,LN2_DI_MATCHED_TR2)

MATCHINS_[expr $indx+7] : LINE=(LN2_BP_MATCH_TO)
MATCHEDS_[expr $indx+8] : LINE=(LN2_BP_MATCHED)
TRACKINS_[expr $indx+8] : LINE=(BNCH,LN2_BP_MATCHED_TR)

MATCHEDA_[expr $indx+8]  : LINE=(LN0_MATCHED,BC1_MATCHED,BC1_DI_MATCHED,LN1_MATCHED,BC2_MATCHED,LN2_MATCHED,LN2_DI_MATCHED,LN2_BP_MATCHED)
TRACKA_[expr $indx+8]    : LINE=(BNCH,LN0_MATCHED_TR2,BC1_MATCHED_TR2,BC1_DI_MATCHED_TR2,LN1_MATCHED_TR2,BC2_MATCHED_TR2,LN2_MATCHED_TR2,LN2_DI_MATCHED_TR2,LN2_BP_MATCHED_TR2)

MATCHINS_[expr $indx+8] : LINE=(SBP_MATCH_TO_1)
MATCHEDS_[expr $indx+9] : LINE=(SBP_MATCHED_1)
TRACKINS_[expr $indx+9] : LINE=(BNCH,SBP_MATCHED_TR_1)

MATCHEDA_[expr $indx+9]  : LINE=(LN0_MATCHED,BC1_MATCHED,BC1_DI_MATCHED,LN1_MATCHED,BC2_MATCHED,LN2_MATCHED,LN2_DI_MATCHED,LN2_BP_MATCHED,SBP_MATCHED_1)
TRACKA_[expr $indx+9]    : LINE=(BNCH,LN0_MATCHED_TR2,BC1_MATCHED_TR2,BC1_DI_MATCHED_TR2,LN1_MATCHED_TR2,BC2_MATCHED_TR2,LN2_MATCHED_TR2,LN2_DI_MATCHED_TR2,LN2_BP_MATCHED_TR2,SBP_MATCHED_TR_12)


MATCHINS_[expr $indx+9] : LINE=(SBP_MATCH_TO_2)
MATCHEDS_[expr $indx+10] : LINE=(SBP_MATCHED_2,SBP_MATCHED_3)
TRACKINS_[expr $indx+10] : LINE=(BNCH,SBP_MATCHED_TR_2,SBP_MATCHED_TR_3)

MATCHEDA_[expr $indx+10]  : LINE=(LN0_MATCHED,BC1_MATCHED,BC1_DI_MATCHED,LN1_MATCHED,BC2_MATCHED,LN2_MATCHED,LN2_DI_MATCHED,LN2_BP_MATCHED,SBP_MATCHED_1,SBP_MATCHED_2,SBP_MATCHED_3)
TRACKA_[expr $indx+10]    : LINE=(BNCH,LN0_MATCHED_TR2,BC1_MATCHED_TR2,BC1_DI_MATCHED_TR2,LN1_MATCHED_TR2,BC2_MATCHED_TR2,LN2_MATCHED_TR2,LN2_DI_MATCHED_TR2,LN2_BP_MATCHED_TR2,SBP_MATCHED_TR_12,SBP_MATCHED_TR_22,SBP_MATCHED_TR_32)

MATCHINS_[expr $indx+10] : LINE=(LN4_MATCH_TO)
MATCHEDS_[expr $indx+11] : LINE=(LN4_MATCHED)
TRACKINS_[expr $indx+11] : LINE=(BNCH,LN4_MATCHED_TR)

MATCHEDA_[expr $indx+11]  : LINE=(LN0_MATCHED,BC1_MATCHED,BC1_DI_MATCHED,LN1_MATCHED,BC2_MATCHED,LN2_MATCHED,LN2_DI_MATCHED,LN2_BP_MATCHED,SBP_MATCHED_1,SBP_MATCHED_2,SBP_MATCHED_3,LN4_MATCHED)
TRACKA_[expr $indx+11]    : LINE=(BNCH,LN0_MATCHED_TR2,BC1_MATCHED_TR2,BC1_DI_MATCHED_TR2,LN1_MATCHED_TR2,BC2_MATCHED_TR2,LN2_MATCHED_TR2,LN2_DI_MATCHED_TR2,LN2_BP_MATCHED_TR2,SBP_MATCHED_TR_12,SBP_MATCHED_TR_22,SBP_MATCHED_TR_32,LN4_MATCHED_TR2 )

MATCHINS_[expr $indx+11] : LINE=(DR4_MATCH_TO)
MATCHEDS_[expr $indx+12] : LINE=(DR4_MATCHED)
TRACKINS_[expr $indx+12] : LINE=(BNCH,DR4_MATCHED_TR)

MATCHEDA_[expr $indx+12]  : LINE=(LN0_MATCHED,BC1_MATCHED,BC1_DI_MATCHED,LN1_MATCHED,BC2_MATCHED,LN2_MATCHED,LN2_DI_MATCHED,LN2_BP_MATCHED,SBP_MATCHED_1,SBP_MATCHED_2,SBP_MATCHED_3,LN4_MATCHED,DR4_MATCH_TO)
TRACKA_[expr $indx+12]    : LINE=(BNCH,LN0_MATCHED_TR2,BC1_MATCHED_TR2,BC1_DI_MATCHED_TR2,LN1_MATCHED_TR2,BC2_MATCHED_TR2,LN2_MATCHED_TR2,LN2_DI_MATCHED_TR2,LN2_BP_MATCHED_TR2,SBP_MATCHED_TR_12,SBP_MATCHED_TR_22,SBP_MATCHED_TR_32,LN4_MATCHED_TR2,DR4_MATCHED_TR2 )

MATCHINS_[expr $indx+12] : LINE=(HXR_MATCH_TO)
MATCHEDS_[expr $indx+13] : LINE=(HXR_MATCHED)
TRACKINS_[expr $indx+13] : LINE=(BNCH,HXR_MATCHED_TR)

MATCHEDA_[expr $indx+13]  : LINE=(LN0_MATCHED,BC1_MATCHED,BC1_DI_MATCHED,LN1_MATCHED,BC2_MATCHED,LN2_MATCHED,LN2_DI_MATCHED,LN2_BP_MATCHED,SBP_MATCHED_1,SBP_MATCHED_2,SBP_MATCHED_3,LN4_MATCHED,,DR4_MATCH_TO,HXR_MATCHED)
TRACKA_[expr $indx+13]    : LINE=(BNCH,LN0_MATCHED_TR2,BC1_MATCHED_TR2,BC1_DI_MATCHED_TR2,LN1_MATCHED_TR2,BC2_MATCHED_TR2,LN2_MATCHED_TR2,LN2_DI_MATCHED_TR2,LN2_BP_MATCHED_TR2,SBP_MATCHED_TR_12,SBP_MATCHED_TR_22,SBP_MATCHED_TR_32,LN4_MATCHED_TR2,DR4_MATCHED_TR2,HXR_MATCHED_TR2)

"

    
}  else {
puts " there is error on definition of tracking sextion"
exit
} 


close $filee


