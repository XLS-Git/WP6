#!/bin/sh  
# \
exec tclsh "$0" "$@"
# set names [gets stdin]

set infile      [lindex $argv 0]
set outfile      [lindex $argv 1]

if {[file exist tmp.0]} {
	exec rm -rf tmp.0
} 

if {[file exist tmp.1]} {
	exec rm -rf tmp.1
}

puts $infile

set flname [file rootname $infile]
set sig $flname.sig
set cl $flname.cent
set tw $flname.twi
set mag $flname.mag
set mat $flname.mtr
set foname [file rootname $outfile]

set profile  $foname.prf
set parfile  $foname.dat
set pltfile  $foname\_plt.tcl

puts $cl
puts $outfile

package require math::statistics
package require math::fourier 
package require math::bigfloat
package require math::special
package require Tcl 
package require struct
package require math::interpolate 
package require math::calculus
namespace import ::math::bigfloat::*

# 
set sepe " "
# 

set from "\%16.9e"

set sl  [exec sddsprintout $sig -column=s,format=%16.9e  -notitle -nolabel ]
set sxl  [exec sddsprintout $sig -column=Sx,format=%16.9e  -notitle -nolabel ]
set syl  [exec sddsprintout $sig -column=Sy,format=%16.9e  -notitle -nolabel ]
set szl  [exec sddsprintout $sig -column=Ss,format=%16.9e  -notitle -nolabel ]
set btxl  [exec sddsprintout $sig -column=betaxBeam,format=%16.9e   -notitle -nolabel ]
set btyl  [exec sddsprintout $sig -column=betayBeam,format=%16.9e  -notitle -nolabel ]
set enxl  [exec sddsprintout $sig -column=ecnx,format=%16.9e   -notitle -nolabel ]
set enyl  [exec sddsprintout $sig -column=ecny,format=%16.9e   -notitle -nolabel ]
set pcl  [exec sddsprintout $cl -column=pCentral,format=%16.9e   -notitle -nolabel ]
set btxlt  [exec sddsprintout $tw -column=betax,format=%16.9e   -notitle -nolabel ]
set btylt  [exec sddsprintout $tw -column=betay,format=%16.9e   -notitle -nolabel ]
set etaxlt  [exec sddsprintout $tw -column=etax,format=%16.9e   -notitle -nolabel ]
set etaxplt  [exec sddsprintout $tw -column=etaxp,format=%16.9e   -notitle -nolabel ]
set prsl   [exec sddsprintout $mag -column=s,format=%16.9e   -notitle -nolabel ]
set prpl   [exec sddsprintout $mag -column=Profile,format=%16.9e   -notitle -nolabel ]
set r56l   [exec sddsprintout $mat -column=R56,format=%16.9e   -notitle -nolabel ]
set t56l   [exec sddsprintout $mat -column=T566,format=%16.9e   -notitle -nolabel ]

set mc2 0.510998910e-3  ;# GeV/c^2



set sxmax [lindex $sxl 0]
set symax [lindex $syl 0]


set smax  [::math::statistics::max "[::math::statistics::max $sxl] [::math::statistics::max $syl]"]
set smax  [::math::statistics::max "[lindex $sxl 0] [lindex $syl 0]"]
set smax [expr $smax*5*1e6]
set emtmin [::math::statistics::min "[::math::statistics::min $enxl] [::math::statistics::min $enyl]"]
set emtmax [::math::statistics::max "[::math::statistics::max $enxl] [::math::statistics::max $enyl]"]
set betamin [::math::statistics::min "[::math::statistics::min $btxl] [::math::statistics::min $btyl]"]
set betamax [::math::statistics::max "[::math::statistics::max $btxl] [::math::statistics::max $btyl]"]
set promax  [::math::statistics::max $prpl]
set r56max  [::math::statistics::max "[::math::statistics::max $r56l] [::math::statistics::max $t56l]"]


set emtc [expr ($emtmax)*0.5]
set emtfct [expr ($emtmax)/10.0]
set betc [expr ($betamax)*0.5]
set betfct [expr ($betamax)/10.0]
set sigc [expr ($smax)*0.5]
set sigfct [expr ($smax)/10.0]
set r56c [expr ($r56max)*0.5]
set r56cfct [expr ($r56max)/10.0]

puts $emtc
puts $emtfct

puts $betc
puts $betfct

puts $sigc
puts $sigfct

set fop [open $profile w]

foreach s $prsl pr $prpl {
    
    set pr $pr
    set premt [expr ($pr*$emtfct+$emtc)*1e6]
    set prsig [expr ($pr*$sigfct+$sigc)*1e6]
    set prbet [expr  $pr*$betfct+$betc]
    set prr56 [expr  $pr*$r56cfct+$r56c]
    puts $fop "$s $pr $premt $prsig $prbet $prr56"
    
    
}

close $fop


set fo [open $parfile w]

puts $fo "# s(m) sx(um)  sy(um) sz(um) enx(mm.mrad) eny(mm.mrad) E(GeV) betax(m) betay(m) betax2(m) betay2(m) etax(m) etaxp(m) prof(m) "
  
  foreach s $sl sx $sxl sy $syl sz $szl btx $btxl bty $btyl enx $enxl eny $enyl pc $pcl  btx2 $btxlt bty2 $btylt etax $etaxlt etaxp $etaxplt r56 $r56l t56 $t56l {
      
      set sx [expr $sx*1e6]
      set sy [expr $sy*1e6]
      set sz [expr $sz*1e6]
      
      set enx [expr $enx*1e6]
      set eny [expr $eny*1e6]
      set enr [expr $pc*$mc2]
      
      puts $fo [format "%16.9e %16.9e %16.9e %16.9e %16.9e %16.9e %16.9e %16.9e %16.9e %16.9e %16.9e %16.9e %16.9e %16.9e %16.9e " $s $sx $sy $sz $enx $eny $enr $btx $bty $btx2 $bty2 $etax $etaxp $r56 $t56  ]

      
  }
close $fo




    
set fl [open $pltfile w]


puts $fl "

set term pdfcairo size 18cm,9cm enhanced font 'Times,20'
set output 'emt_tmp.pdf' 
set y2label '{/Symbol s}_{z} ({/Symbol m}m)' offset -1,0
set xlabel 'Distance (m)' offset 0,0.8
set ylabel ' {/Symbol e}_{x,y} (mm.mrad)'
set key opaque    
set ytics nomirror
set y2tics
set tics out
set autoscale  y
set autoscale y2
set grid lt 0 
pl '$profile' u (\$1*1e0):(\$3*1e0) w l lw 2 lc 'gray'  noti, \
   '$parfile' u (\$1*1e0):(\$5*1e0) w l lw 3 lc 'red'  ti '{/Symbol e}_{x}', \
   '$parfile' u (\$1*1e0):(\$6*1e0) w l lw 3 lc 'blue'  ti '{/Symbol e}_{y}', \
   '$parfile' u (\$1*1e0):(\$4*1e0) w l lw 3 lc 'black'  axes x1y2 ti '{/Symbol s}_{z}'
   
unset output

set term pdfcairo size 18cm,9cm enhanced font 'Times,20'
set output 'beta_tmp.pdf' 
set key top left opaque   
#set y2label '{/Symbol h}_{x} (m)' offset -2,0
set y2label 'E (GeV)' offset -2,0
set xlabel 'Distance (m)' offset 0,0.8
set ylabel ' {/Symbol b}_{x,y} (m)' offset 1,0
set ytics nomirror
set y2tics nomirror
# set tics out
set autoscale  y
set autoscale y2
set grid lt 0 
pl  '$profile' u (\$1*1e0):(\$5*1e0) w l lw 2 lc 'gray'  noti, \
    '$outfile' u (\$1*1e0):(\$8*1e0) w l lw 3 lc 'red'  ti '{/Symbol b}_{x}', \
    '$outfile' u (\$1*1e0):(\$9*1e0) w l lw 3 lc 'blue'  ti '{/Symbol b}_{y}', \
    '$outfile' u (\$1*1e0):(\$7*1e0) w l lw 3 lc 'black' axes x1y2  ti 'E', \
#     '$outfile' u (\$1*1e0):(\$12*1e0) w l lw 3 lc 'black'  axes x1y2 ti '{/Symbol h}_{x}', \

unset output

set term pdfcairo size 18cm,9cm enhanced font 'Times,20'
set output 'sigma_tmp.pdf' 
set key opaque   
set y2label '{/Symbol s}_{z} ({/Symbol m}m)' offset -1,0
set xlabel 'Distance (m)' offset 0,0.8
set ylabel ' {/Symbol s}_{x,y} ({/Symbol m}m)' 
set ytics nomirror
set y2tics
set tics out
set autoscale  y
set yrange \[0:$smax\]
set autoscale y2
set grid lt 0 
pl '$profile' u (\$1*1e0):(\$4*1e0) w l lw 2 lc 'gray'  noti, \
   '$outfile' u (\$1*1e0):(\$2*1e0) w l lw 3 lc 'red'  ti '{/Symbol s}_{x}', \
   '$outfile' u (\$1*1e0):(\$3*1e0) w l lw 3 lc 'blue'  ti '{/Symbol s}_{y}', \
   '$outfile' u (\$1*1e0):(\$4*1e0) w l lw 3 lc 'black'  axes x1y2 ti '{/Symbol s}_{z}', \

unset output

set term pdfcairo size 18cm,9cm enhanced font 'Times,20'
set output 'r56_tmp.pdf' 
set key opaque   
set y2label '{/Symbol s}_{z} ({/Symbol m}m)' offset -1,0
set xlabel 'Distance (m)' offset 0,0.8
set ylabel ' R_{56} (cm), T_{566} (m)' 
set ytics nomirror
set y2tics
set tics out
set autoscale  y
set autoscale y2
set grid lt 0 
pl '$profile' u (\$1*1e0):(\$6*1e0) w l lw 2 lc 'gray'  noti, \
   '$outfile' u (\$1*1e0):(\$14*1e2) w l lw 3 lc 'red'  ti 'R_{56}', \
   '$outfile' u (\$1*1e0):(\$15*1e0) w l lw 3 lc 'blue'  ti 'T_{566}', \
   '$outfile' u (\$1*1e0):(\$4*1e0) w l lw 3 lc 'black'  axes x1y2 ti '{/Symbol s}_{z}', \

unset output

# 
# # pause -1
"
close $fl

exec gnuplot $pltfile

exec pdfcrop emt_tmp.pdf emt_$foname.pdf
exec pdfcrop sigma_tmp.pdf sigma_$foname.pdf
exec pdfcrop beta_tmp.pdf beta_$foname.pdf
exec pdfcrop r56_tmp.pdf r56_$foname.pdf

exec pdftoppm -png -rx 300 -ry 300 emt_$foname.pdf > emt_$foname.png
exec pdftoppm -png -rx 300 -ry 300  sigma_$foname.pdf > sigma_$foname.png
exec pdftoppm -png -rx 300 -ry 300  beta_$foname.pdf > beta_$foname.png
exec pdftoppm -png -rx 300 -ry 300  r56_$foname.pdf > r56_$foname.png

exec rm emt_tmp.pdf sigma_tmp.pdf beta_tmp.pdf r56_tmp.pdf
