

set term pdfcairo size 18cm,8cm enhanced font 'Times,16'
set output 'xlstiws_emt.pdf' 
set y2label '{/Symbol s}_{z} ({/Symbol m}m)'
set xlabel 'Distance (m)' 
set ylabel ' {/Symbol e}_{x,y} (mm.mrad)' 
set ytics nomirror
set y2tics
set tics out
set autoscale  y
set autoscale y2
set grid lt 0 
pl 'xlstiws.dat' u ($1*1e0):($5*1e0) w l lw 3 lc 'red'  ti '{/Symbol e}_{x}',  'xlstiws.dat' u ($1*1e0):($6*1e0) w l lw 3 lc 'blue'  ti '{/Symbol e}_{y}',  'xlstiws.dat' u ($1*1e0):($4*1e0) w l lw 3 lc 'black'  axes x1y2 ti '{/Symbol s}_{z}',  
unset output

set term pdfcairo size 18cm,8cm enhanced font 'Times,16'
set output 'xlstiws_beta.pdf' 
set y2label 'E (GeV)'
set xlabel 'Distance (m)' 
set ylabel ' {/Symbol b}_{x,y} (m)' 
set ytics nomirror
set y2tics
set tics out
set autoscale  y
set autoscale y2
set grid lt 0 
pl 'xlstiws.dat' u ($1*1e0):($8*1e0) w l lw 3 lc 'red'  ti '{/Symbol b}_{x}',  'xlstiws.dat' u ($1*1e0):($9*1e0) w l lw 3 lc 'blue'  ti '{/Symbol b}_{y}',  'xlstiws.dat' u ($1*1e0):($7*1e0) w l lw 3 lc 'black'  axes x1y2 ti 'E',  
unset output

set term pdfcairo size 18cm,8cm enhanced font 'Times,16'
set output 'xlstiws_sigma.pdf' 
set y2label '{/Symbol s}_{z} ({/Symbol m}m)'
set xlabel 'Distance (m)' 
set ylabel ' {/Symbol s}_{x,y} ({/Symbol m}m)' 
set ytics nomirror
set y2tics
set tics out
# set autoscale  y
set yrange [0:109.197328884]
set autoscale y2
set grid lt 0 
pl 'xlstiws.dat' u ($1*1e0):($2*1e0) w l lw 3 lc 'red'  ti '{/Symbol s}_{x}',  'xlstiws.dat' u ($1*1e0):($3*1e0) w l lw 3 lc 'blue'  ti '{/Symbol s}_{y}',  'xlstiws.dat' u ($1*1e0):($4*1e0) w l lw 3 lc 'black'  axes x1y2 ti '{/Symbol s}_{z}',  
unset output

# pause -1

