#!/bin/sh  
# \
exec tclsh "$0" "$@"
# set names [gets stdin]

set infile      [lindex $argv 0]
set outfile      [lindex $argv 1]

if {[file exist tmp.0]} {
	exec rm -rf tmp.0
} 

if {[file exist tmp.1]} {
	exec rm -rf tmp.1
}

puts $infile

set flname [file rootname $infile]
set sf $flname.sig
set cl $flname.cent
set tw $flname.twi
set mag $flname.mag
set mat $flname.mtr

set foname [file rootname $outfile]

set profile  $foname.prf
set parfile  $foname.dat
set pltfile  $foname\_plt.tcl

puts $cl
puts $outfile

package require math::statistics
package require math::fourier 
package require math::bigfloat
package require math::special
package require Tcl 
package require struct
package require math::interpolate 
package require math::calculus
namespace import ::math::bigfloat::*

# 
set sepe " "
# 

set from "\%16.9e"

set sl  [exec sddsprintout $tw -column=s,format=%16.9e  -notitle -nolabel ]
set sxl  [exec sddsprintout $sf -column=Sx,format=%16.9e  -notitle -nolabel ]
set syl  [exec sddsprintout $sf -column=Sy,format=%16.9e  -notitle -nolabel ]
set szl  [exec sddsprintout $sf -column=Ss,format=%16.9e  -notitle -nolabel ]
set btxl  [exec sddsprintout $sf -column=betaxBeam,format=%16.9e   -notitle -nolabel ]
set btyl  [exec sddsprintout $sf -column=betayBeam,format=%16.9e  -notitle -nolabel ]
set r56l [exec sddsprintout $mat -column=R56,format=%16.9e  -notitle -nolabel ]
set t56l [exec sddsprintout $mat -column=T566,format=%16.9e  -notitle -nolabel ]
set enxl  [exec sddsprintout $sf -column=ecnx,format=%16.9e   -notitle -nolabel ]
set enyl  [exec sddsprintout $sf -column=ecny,format=%16.9e   -notitle -nolabel ]
set pcl  [exec sddsprintout $cl -column=pCentral,format=%16.9e   -notitle -nolabel ]
set btxlt  [exec sddsprintout $tw -column=betax,format=%16.9e   -notitle -nolabel ]
set btylt  [exec sddsprintout $tw -column=betay,format=%16.9e   -notitle -nolabel ]
set etaxlt  [exec sddsprintout $tw -column=etax,format=%16.9e   -notitle -nolabel ]
set etaxplt  [exec sddsprintout $tw -column=etaxp,format=%16.9e   -notitle -nolabel ]
set prsl   [exec sddsprintout $mag -column=s,format=%16.9e   -notitle -nolabel ]
set prpl   [exec sddsprintout $mag -column=Profile,format=%16.9e   -notitle -nolabel ]


set mc2 0.510998910e-3  ;# GeV/c^2



# set sxmax [lindex $sxl 0]
# set symax [lindex $syl 0]


set smax  [::math::statistics::max "[::math::statistics::max $sxl] [::math::statistics::max $syl]"]
set smax  [::math::statistics::max "[lindex $sxl 0] [lindex $syl 0]"]
set smax [expr $smax*5*1e6]
set emtmin [::math::statistics::min "[::math::statistics::min $enxl] [::math::statistics::min $enyl]"]
set emtmax [::math::statistics::max "[::math::statistics::max $enxl] [::math::statistics::max $enyl]"]
set betamin [::math::statistics::min "[::math::statistics::min $btxlt] [::math::statistics::min $btylt]"]
set betamax [::math::statistics::max "[::math::statistics::max $btxlt] [::math::statistics::max $btylt]"]
set promax  [::math::statistics::max $prpl]


set emtc [expr ($emtmax)*0.5]
set emtfct [expr ($emtmax)/10.0]
set betc [expr ($betamax)*0.5]
set betfct [expr ($betamax)/10.0]
set sigc [expr ($smax)*0.5]
set sigfct [expr ($smax)/10.0]


puts $emtc
puts $emtfct

puts $betc
puts $betfct

puts $sigc
puts $sigfct

set fop [open $profile w]

foreach s $prsl pr $prpl {
    
    set pr $pr
    set premt [expr ($pr*$emtfct+$emtc)*1e6]
    set prsig [expr ($pr*$sigfct+$sigc)*1e6]
    set prbet [expr  $pr*$betfct+$betc]
    
    puts $fop "$s $pr $premt $prsig $prbet"
    
    
}

close $fop


set sxl  [exec sddsprintout $infile -column=Sx,format=%16.9e  -notitle -nolabel ]
set syl  [exec sddsprintout $infile -column=Sy,format=%16.9e  -notitle -nolabel ]
set szl  [exec sddsprintout $infile -column=Ss,format=%16.9e  -notitle -nolabel ]
set btxl  [exec sddsprintout $infile -column=betaxBeam,format=%16.9e   -notitle -nolabel ]
set btyl  [exec sddsprintout $infile -column=betayBeam,format=%16.9e  -notitle -nolabel ]
set enxl  [exec sddsprintout $infile -column=ecnx,format=%16.9e   -notitle -nolabel ]
set enyl  [exec sddsprintout $infile -column=ecny,format=%16.9e   -notitle -nolabel ]
set pcl  [exec sddsprintout $cl -column=pCentral,format=%16.9e   -notitle -nolabel ]
set btxlt  [exec sddsprintout $tw -column=betax,format=%16.9e   -notitle -nolabel ]
set btylt  [exec sddsprintout $tw -column=betay,format=%16.9e   -notitle -nolabel ]
set etaxlt  [exec sddsprintout $tw -column=etax,format=%16.9e   -notitle -nolabel ]
set etaxplt  [exec sddsprintout $tw -column=etaxp,format=%16.9e   -notitle -nolabel ]
set prsl   [exec sddsprintout $mag -column=s,format=%16.9e   -notitle -nolabel ]
set prpl   [exec sddsprintout $mag -column=Profile,format=%16.9e   -notitle -nolabel ]

set fo [open $parfile w]

puts $fo "# s(m) sx(um)  sy(um) sz(um) enx(mm.mrad) eny(mm.mrad) betax(m) betay(m) betax2(m) betay2(m) etax(m) etaxp(m) E(GeV) "
  
  foreach s $sl  btx2 $btxlt bty2 $btylt etax $etaxlt etaxp $etaxplt sx $sxl sy $syl sz $szl btx $btxl bty $btyl enx $enxl eny $enyl pc $pcl r5 $r56l t5 $t56l {
      
      set sx [expr $sx*1e6]
      set sy [expr $sy*1e6]
      set sz [expr $sz*1e6]
      
      set enx [expr $enx*1e6]
      set eny [expr $eny*1e6]
      set enr [expr $pc*$mc2]
      
      puts $fo [format "%12.5e %12.5e %12.5e %12.5e %12.5e %12.5e %12.5e %12.5e %12.5e %12.5e %12.5e %12.5e %12.5e %12.5e %12.5e" $s $sx $sy $sz $enx $eny $btx $bty $btx2 $bty2 $etax $etaxp $enr $r5 $t5 ]

      
  }
close $fo




    
set fl [open $pltfile w]


puts $fl "


set term pdfcairo size 18cm,10cm enhanced font 'Times,20'
set output 'beta_tmp.pdf' 
set key top left
set y2label '{/Symbol h}_{x} (m)' offset -1,0
set xlabel 'Distance (m)' 
set ylabel ' {/Symbol b}_{x,y} (m)' 
set ytics nomirror
set y2tics
set tics out
set autoscale  y
set autoscale y2
set grid lt 0 
pl  '$profile' u (\$1*1e0):(\$5*1e0) w l lw 2 lc 'gray'  noti, \
    '$parfile' u (\$1*1e0):(\$9*1e0) w l lw 3 lc 'red'  ti '{/Symbol b}_{x}', \
    '$parfile' u (\$1*1e0):(\$10*1e0) w l lw 3 lc 'blue'  ti '{/Symbol b}_{y}', \
    '$parfile' u (\$1*1e0):(\$11*1e0) w l lw 3 lc 'black'  axes x1y2 ti '{/Symbol h}_{x}', \

unset output

set term pdfcairo size 18cm,10cm enhanced font 'Times,20'
set output 'emt_tmp.pdf' 
set key top left
set y2label '{/Symbol s}_{z} (um)' offset -1,0
set xlabel 'Distance (m)' 
set ylabel ' {/Symbol e}_{x,y} (mm.mrad)' 
set ytics nomirror
set y2tics
set tics out
set autoscale  y
set autoscale y2
set grid lt 0 
pl  '$profile' u (\$1*1e0):(\$3*1e0) w l lw 2 lc 'gray'  noti, \
    '$parfile' u (\$1*1e0):(\$5*1e0) w l lw 3 lc 'red'  ti '{/Symbol b}_{x}', \
    '$parfile' u (\$1*1e0):(\$6*1e0) w l lw 3 lc 'blue'  ti '{/Symbol b}_{y}', \
    '$parfile' u (\$1*1e0):(\$4*1e0) w l lw 3 lc 'black'  axes x1y2 ti '{/Symbol h}_{x}', \

unset output
"
close $fl

exec gnuplot $pltfile

# exec pdfcrop emt_tmp.pdf emt_$foname.pdf
# exec pdfcrop sigma_tmp.pdf sigma_$foname.pdf
exec pdfcrop beta_tmp.pdf beta_$foname.pdf

# exec pdftoppm -png  emt_$foname.pdf > emt_$foname.png
# exec pdftoppm -png  sigma_$foname.pdf > sigma_$foname.png
exec pdftoppm -png -rx 300 -ry 300 beta_$foname.pdf > beta_$foname.png

exec rm beta_tmp.pdf
