#!/bin/sh  
# \
exec tclsh "$0" "$@"
# set names [gets stdin]

set infile      [lindex $argv 0]
set outfile      [lindex $argv 1]

if {[file exist tmp.0]} {
	exec rm -rf tmp.0
} 

if {[file exist tmp.1]} {
	exec rm -rf tmp.1
}

puts $infile

set flname [file rootname $infile]
set cl $flname.cent

set foname [file rootname $outfile]
     

puts $cl
puts $outfile

# 
set sepe " "
# 

set from "\%16.9e"

set sl  [exec sddsprintout $infile -column=s,format=%16.9e  -notitle -nolabel ]
set sxl  [exec sddsprintout $infile -column=Sx,format=%16.9e  -notitle -nolabel ]
set syl  [exec sddsprintout $infile -column=Sy,format=%16.9e  -notitle -nolabel ]
set szl  [exec sddsprintout $infile -column=Ss,format=%16.9e  -notitle -nolabel ]
set btxl  [exec sddsprintout $infile -column=betaxBeam,format=%16.9e   -notitle -nolabel ]
set btyl  [exec sddsprintout $infile -column=betayBeam,format=%16.9e  -notitle -nolabel ]
set enxl  [exec sddsprintout $infile -column=ecnx,format=%16.9e   -notitle -nolabel ]
set enyl  [exec sddsprintout $infile -column=ecny,format=%16.9e   -notitle -nolabel ]
set pcl  [exec sddsprintout $cl -column=pCentral,format=%16.9e   -notitle -nolabel ]


set mc2 0.510998910e-3  ;# GeV/c^2

set fo [open $outfile w]

set sxmax [lindex $sxl 0]
set symax [lindex $syl 0]

set smax $sxmax 
if {$smax < $symax} {
    set smax $symax
}
set smax [expr $smax*1.2*1e6]

puts $fo "# s(m) sx(um)  sy(um) sz(um) enx(mm.mrad) eny(mm.mrad) E(GeV) betax(m) betay(m) "
  
  foreach s $sl sx $sxl sy $syl sz $szl btx $btxl bty $btyl enx $enxl eny $enyl pc $pcl {
      
      set sx [expr $sx*1e6]
      set sy [expr $sy*1e6]
      set sz [expr $sz*1e6]
      
      set enx [expr $enx*1e6]
      set eny [expr $eny*1e6]
      set enr [expr $pc*$mc2]
      
      puts $fo [format "%16.9e %16.9e %16.9e %16.9e %16.9e %16.9e %16.9e %16.9e %16.9e" $s $sx $sy $sz $enx $eny $enr $btx $bty ]

      
  }
  
  close $fo




    
set fl [open $foname.emt_plt.tcl w]


puts $fl "

set term pdfcairo size 18cm,8cm enhanced font 'Times,16'
set output '$foname\_emt.pdf' 
set y2label '{/Symbol s}_{z} ({/Symbol m}m)'
set xlabel 'Distance (m)' 
set ylabel ' {/Symbol e}_{x,y} (mm.mrad)' 
set key top left
set ytics nomirror
set y2tics
set tics out
set autoscale  y
set autoscale y2
set grid lt 0 
pl '$outfile' u (\$1*1e0):(\$5*1e0) w l lw 3 lc 'red'  ti '{/Symbol e}_{x}', \
   '$outfile' u (\$1*1e0):(\$6*1e0) w l lw 3 lc 'blue'  ti '{/Symbol e}_{y}', \
   '$outfile' u (\$1*1e0):(\$4*1e0) w l lw 3 lc 'black'  axes x1y2 ti '{/Symbol s}_{z}', \

unset output

set term pdfcairo size 18cm,8cm enhanced font 'Times,16'
set output '$foname\_beta.pdf' 
set y2label 'E (GeV)'
set xlabel 'Distance (m)' 
set ylabel ' {/Symbol b}_{x,y} (m)' 
set ytics nomirror
set y2tics
set tics out
set autoscale  y
set autoscale y2
set grid lt 0 
pl '$outfile' u (\$1*1e0):(\$8*1e0) w l lw 3 lc 'red'  ti '{/Symbol b}_{x}', \
   '$outfile' u (\$1*1e0):(\$9*1e0) w l lw 3 lc 'blue'  ti '{/Symbol b}_{y}', \
   '$outfile' u (\$1*1e0):(\$7*1e0) w l lw 3 lc 'black'  axes x1y2 ti 'E', \

unset output

set term pdfcairo size 18cm,8cm enhanced font 'Times,16'
set output '$foname\_sigma.pdf' 
set y2label '{/Symbol s}_{z} ({/Symbol m}m)'
set xlabel 'Distance (m)' 
set ylabel ' {/Symbol s}_{x,y} ({/Symbol m}m)' 
set ytics nomirror
set y2tics
set tics out
# set autoscale  y
set yrange \[0:$smax\]
set autoscale y2
set grid lt 0 
pl '$outfile' u (\$1*1e0):(\$2*1e0) w l lw 3 lc 'red'  ti '{/Symbol s}_{x}', \
   '$outfile' u (\$1*1e0):(\$3*1e0) w l lw 3 lc 'blue'  ti '{/Symbol s}_{y}', \
   '$outfile' u (\$1*1e0):(\$4*1e0) w l lw 3 lc 'black'  axes x1y2 ti '{/Symbol s}_{z}', \

unset output

# pause -1
"
close $fl

exec gnuplot $foname.emt_plt.tcl 
