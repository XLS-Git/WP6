SDDS1
!# little-endian
&description text="sigma matrix--input: xls_linac_tmc_hxr_hxr.track.ele  lattice: xls_linac_tmc_hxr.12.lte", contents="sigma matrix", &end
&parameter name=Step, description="Simulation step", type=long, &end
&parameter name=SVNVersion, description="SVN version number", type=string, fixed_value=unknown, &end
&column name=s, units=m, description=Distance, type=double,  &end
&column name=ElementName, description="Element name", format_string=%10s, type=string,  &end
&column name=ElementOccurence, description="Occurence of element", format_string=%6ld, type=long,  &end
&column name=ElementType, description="Element-type name", format_string=%10s, type=string,  &end
&column name=s1, symbol="$gs$r$b1$n", units=m, description="sqrt(<x*x>)", type=double,  &end
&column name=s12, symbol="$gs$r$b12$n", units=m, description="<x*xp'>", type=double,  &end
&column name=s13, symbol="$gs$r$b13$n", units="m$a2$n", description="<x*y>", type=double,  &end
&column name=s14, symbol="$gs$r$b14$n", units=m, description="<x*y'>", type=double,  &end
&column name=s15, symbol="$gs$r$b15$n", units="m$a2$n", description="<x*s>", type=double,  &end
&column name=s16, symbol="$gs$r$b16$n", units=m, description="<x*delta>", type=double,  &end
&column name=s17, symbol="$gs$r$b17$n", units="m*s", description="<x*t>", type=double,  &end
&column name=s2, symbol="$gs$r$b2$n", description="sqrt(<x'*x'>)", type=double,  &end
&column name=s23, symbol="$gs$r$b23$n", units=m, description="<x'*y>", type=double,  &end
&column name=s24, symbol="$gs$r$b24$n", description="<x'*y'>", type=double,  &end
&column name=s25, symbol="$gs$r$b25$n", units=m, description="<x'*s>", type=double,  &end
&column name=s26, symbol="$gs$r$b26$n", description="<x'*delta>", type=double,  &end
&column name=s27, symbol="$gs$r$b27$n", units=s, description="<x'*t>", type=double,  &end
&column name=s3, symbol="$gs$r$b3$n", units=m, description="sqrt(<y*y>)", type=double,  &end
&column name=s34, symbol="$gs$r$b34$n", units=m, description="<y*y'>", type=double,  &end
&column name=s35, symbol="$gs$r$b35$n", units="m$a2$n", description="<y*s>", type=double,  &end
&column name=s36, symbol="$gs$r$b36$n", units=m, description="<y*delta>", type=double,  &end
&column name=s37, symbol="$gs$r$b37$n", units="m*s", description="<y*t>", type=double,  &end
&column name=s4, symbol="$gs$r$b4$n", description="sqrt(<y'*y')>", type=double,  &end
&column name=s45, symbol="$gs$r$b45$n", units=m, description="<y'*s>", type=double,  &end
&column name=s46, symbol="$gs$r$b46$n", description="<y'*delta>", type=double,  &end
&column name=s47, symbol="$gs$r$b47$n", units=s, description="<y'*t>", type=double,  &end
&column name=s5, symbol="$gs$r$b5$n", units=m, description="sqrt(<s*s>)", type=double,  &end
&column name=s56, symbol="$gs$r$b56$n", units=m, description="<s*delta>", type=double,  &end
&column name=s57, symbol="$gs$r$b57$n", units="m*s", description="<s*t>", type=double,  &end
&column name=s6, symbol="$gs$r$b6$n", description="sqrt(<delta*delta>)", type=double,  &end
&column name=s67, symbol="$gs$r$b67$n", units=s, description="<delta*t>", type=double,  &end
&column name=s7, symbol="$gs$r$b7$n", description="sqrt(<t*t>)", type=double,  &end
&column name=ma1, symbol="max$sb$ex$sb$e", units=m, description="maximum absolute value of x", type=double,  &end
&column name=ma2, symbol="max$sb$ex'$sb$e", description="maximum absolute value of x'", type=double,  &end
&column name=ma3, symbol="max$sb$ey$sb$e", units=m, description="maximum absolute value of y", type=double,  &end
&column name=ma4, symbol="max$sb$ey'$sb$e", description="maximum absolute value of y'", type=double,  &end
&column name=ma5, symbol="max$sb$e$gD$rs$sb$e", units=m, description="maximum absolute deviation of s", type=double,  &end
&column name=ma6, symbol="max$sb$e$gd$r$sb$e", description="maximum absolute value of delta", type=double,  &end
&column name=ma7, symbol="max$sb$e$gD$rt$sb$e", units=s, description="maximum absolute deviation of t", type=double,  &end
&column name=minimum1, symbol="x$bmin$n", units=m, type=double,  &end
&column name=minimum2, symbol="x'$bmin$n", units=m, type=double,  &end
&column name=minimum3, symbol="y$bmin$n", units=m, type=double,  &end
&column name=minimum4, symbol="y'$bmin$n", units=m, type=double,  &end
&column name=minimum5, symbol="$gD$rs$bmin$n", units=m, type=double,  &end
&column name=minimum6, symbol="$gd$r$bmin$n", units=m, type=double,  &end
&column name=minimum7, symbol="t$bmin$n", units=s, type=double,  &end
&column name=maximum1, symbol="x$bmax$n", units=m, type=double,  &end
&column name=maximum2, symbol="x'$bmax$n", units=m, type=double,  &end
&column name=maximum3, symbol="y$bmax$n", units=m, type=double,  &end
&column name=maximum4, symbol="y'$bmax$n", units=m, type=double,  &end
&column name=maximum5, symbol="$gD$rs$bmax$n", units=m, type=double,  &end
&column name=maximum6, symbol="$gd$r$bmax$n", units=m, type=double,  &end
&column name=maximum7, symbol="t$bmax$n", units=s, type=double,  &end
&column name=Sx, symbol="$gs$r$bx$n", units=m, description=sqrt(<(x-<x>)^2>), type=double,  &end
&column name=Sxp, symbol="$gs$r$bx'$n", description=sqrt(<(x'-<x'>)^2>), type=double,  &end
&column name=Sy, symbol="$gs$r$by$n", units=m, description=sqrt(<(y-<y>)^2>), type=double,  &end
&column name=Syp, symbol="$gs$r$by'$n", description=sqrt(<(y'-<y'>)^2>), type=double,  &end
&column name=Ss, symbol="$gs$r$bs$n", units=m, description=sqrt(<(s-<s>)^2>), type=double,  &end
&column name=Sdelta, symbol="$gs$bd$n$r", description=sqrt(<(delta-<delta>)^2>), type=double,  &end
&column name=St, symbol="$gs$r$bt$n", units=s, description=sqrt(<(t-<t>)^2>), type=double,  &end
&column name=ex, symbol="$ge$r$bx$n", units=m, description="geometric horizontal emittance", type=double,  &end
&column name=enx, symbol="$ge$r$bx,n$n", units=m, description="normalized horizontal emittance", type=double,  &end
&column name=ecx, symbol="$ge$r$bx,c$n", units=m, description="geometric horizontal emittance less dispersive contributions", type=double,  &end
&column name=ecnx, symbol="$ge$r$bx,cn$n", units=m, description="normalized horizontal emittance less dispersive contributions", type=double,  &end
&column name=ey, symbol="$ge$r$by$n", units=m, description="geometric vertical emittance", type=double,  &end
&column name=eny, symbol="$ge$r$by,n$n", units=m, description="normalized vertical emittance", type=double,  &end
&column name=ecy, symbol="$ge$r$by,c$n", units=m, description="geometric vertical emittance less dispersive contributions", type=double,  &end
&column name=ecny, symbol="$ge$r$by,cn$n", units=m, description="normalized vertical emittance less dispersive contributions", type=double,  &end
&column name=betaxBeam, symbol="$gb$r$bx,beam$n", units=m, description="betax for the beam, excluding dispersive contributions", type=double,  &end
&column name=alphaxBeam, symbol="$ga$r$bx,beam$n", description="alphax for the beam, excluding dispersive contributions", type=double,  &end
&column name=betayBeam, symbol="$gb$r$by,beam$n", units=m, description="betay for the beam, excluding dispersive contributions", type=double,  &end
&column name=alphayBeam, symbol="$ga$r$by,beam$n", description="alphay for the beam, excluding dispersive contributions", type=double,  &end
&data mode=binary, &end
/                 _BEG_      MARK3��^7��>6��Ҏ�=��#����.o�u_V���~�Ġ����d�ڽ��"$޻�Lb�L�>]��X�=�Aen�UT�c�4��s����н'�u)���]�p45��>Ne�zf�=?��9�-=�����=��|Z��j;��9_[f�>��ë3����'m�#i=O�;��4'���n�+�>�yw4��=���H�)�;�yV���3?��j��t"<�V�Gn�<ҟ��r:?60����>�&t���?O��wX�>   ,M�>��\
l?   F�=	���?������q��cM��j�O��wX�   ���ɾ�D���;f�   ��+�ҟ��r:?60����>�&t���?ޒU~�r�>   ,M�>��\
l?   F�=3��^7��>�Lb�L�>]�p45��>��9_[f�>��n�+�>�yV���3?�V�Gn�<�nO!,`�=���=�d�>)⥖�N�=�gN�>$�A>M�=(b�Z<�>aV2�7M�=m/�7�>`�M�	@�����ӿ��F''$@�^'���ۿ           BNCH      CHARGE3��^7��>6��Ҏ�=��#����.o�u_V���~�Ġ����d�ڽ��"$޻�Lb�L�>]��X�=�Aen�UT�c�4��s����н'�u)���]�p45��>Ne�zf�=?��9�-=�����=��|Z��j;��9_[f�>��ë3����'m�#i=O�;��4'���n�+�>�yw4��=���H�)�;�yV���3?��j��t"<�V�Gn�<ҟ��r:?60����>�&t���?O��wX�>   ,M�>��\
l?   F�=	���?������q��cM��j�O��wX�   ���ɾ�D���;f�   ��+�ҟ��r:?60����>�&t���?ޒU~�r�>   ,M�>��\
l?   F�=3��^7��>�Lb�L�>]�p45��>��9_[f�>��n�+�>�yV���3?�V�Gn�<�nO!,`�=���=�d�>)⥖�N�=�gN�>$�A>M�=(b�Z<�>aV2�7M�=m/�7�>`�M�	@�����ӿ��F''$@�^'���ۿ윙����?	   HXR_DR_V1      DRIF��8PqY�>�ij��Р=���d�)���R} dZ�ǻ�,C��v����ݽq��>��޻�Lb�L�>�?@2\G�Aen�UT���Ĥ��s����н��)���F��M#��>,̝6aԛ=��i�Vn-=�꥙�G�=)�3�Wj;��9_[f�>3[4����'m�#i=�S���4'��"�X�+�>a�&5��=�����)�;�yV���3?��A�t"<�E�Hn�<�����?60����>�xb�k?O��wX�>   �T�>��\
l?   �M�=��V~b������q���{�  �O��wX�   `��ɾ�D���;f�   <�+������?60����>�xb�k?ޒU~�r�>   �T�>��\
l?   �M�=��8PqY�>�Lb�L�>F��M#��>��9_[f�>�"�X�+�>�yV���3?�E�Hn�<�nO!,`�=���=�d�>(⥖�N�=�gN�>$�A>M�='b�Z<�>`V2�7M�=l/�7�>7̓	v�
@�rj��׿��ǂ$@�+��cݿ�䥛� �?   HXR_DR_QD_ED      DRIFKV��s�>��� ��=�� g�I���^E��o[��ϟd����~��޽�x��$߻�Lb�L�>��&ZDI�Aen�UT�������s����н����)�����w��>;v�H�3�=F"~�X-=q�-�r\�=Z�VkDj;��9_[f�>���24����'m�#i=�K���4'��Dql�+�>xNT5��=�����)�;�yV���3?{a�t"< |nHn�<!\�<��?60����>�;��#^?O��wX�>   �V�>��\
l?   �O�=�%)��s������q��;E")�&�O��wX�   H��ɾ�D���;f�   (�+�!\�<��?60����>�;��#^?ޒU~�r�>   �V�>��\
l?   �O�=KV��s�>�Lb�L�>��w��>��9_[f�>�Dql�+�>�yV���3? |nHn�<�nO!,`�=���=�d�>(⥖�N�=�gN�>$�A>M�='b�Z<�>`V2�7M�=l/�7�>!��+�@@rͪ�ٿPp�Je�$@��k4�ݿ�*\����?
   HXR_QD_V01      QUAD=>�2p�>�?��ne��c�iER���\*&�'��,7%�a���ֽ�޽J�ɟ� ߻@o�3��>����*�=��q>��g=�H˟~=�R��'��V8jO��;��EE��>/nƅ��=?�2Z�d-=�~�A<m�=��FOj;{O����>�5�P��=f��5�
�=�e�O;�,w�+�>A-Lp5��=R'��)�;�yV���3?D"�t"<0wD#Hn�<	H\ d�?��r�>(�]�j?�H���9?   �W�>��\
l?   �P�=�d�"�m���r��̠{O?��H���9�   H��ɾ�D���;f�   ,�+�	H\ d�?�nᑾ��>(�]�j?(,��)�?   �W�>��\
l?   �P�==>�2p�>@o�3��>��EE��>{O����>�,w�+�>�yV���3?0wD#Hn�<?�ɺV`�=W�4(e�>�����N�=�UE�NN�>�5N�=��o��>�0S.N�=���	��>Uk�v5@ܪMVm�?���$@i\ٳ��*\����?   HXR_COR      KICKER=>�2p�>�?��ne��c�iER���\*&�'��,7%�a���ֽ�޽J�ɟ� ߻@o�3��>����*�=��q>��g=�H˟~=�R��'��V8jO��;��EE��>/nƅ��=?�2Z�d-=�~�A<m�=��FOj;{O����>�5�P��=f��5�
�=�e�O;�,w�+�>A-Lp5��=R'��)�;�yV���3?D"�t"<0wD#Hn�<	H\ d�?��r�>(�]�j?�H���9?   �W�>��\
l?   �P�=�d�"�m���r��̠{O?��H���9�   H��ɾ�D���;f�   ,�+�	H\ d�?�nᑾ��>(�]�j?(,��)�?   �W�>��\
l?   �P�==>�2p�>@o�3��>��EE��>{O����>�,w�+�>�yV���3?0wD#Hn�<?�ɺV`�=W�4(e�>�����N�=�UE�NN�>�5N�=��o��>�0S.N�=���	��>Uk�v5@ܪMVm�?���$@i\ٳ��*\����?   HXR_BPM      MONI=>�2p�>�?��ne��c�iER���\*&�'��,7%�a���ֽ�޽J�ɟ� ߻@o�3��>����*�=��q>��g=�H˟~=�R��'��V8jO��;��EE��>/nƅ��=?�2Z�d-=�~�A<m�=��FOj;{O����>�5�P��=f��5�
�=�e�O;�,w�+�>A-Lp5��=R'��)�;�yV���3?D"�t"<0wD#Hn�<	H\ d�?��r�>(�]�j?�H���9?   �W�>��\
l?   �P�=�d�"�m���r��̠{O?��H���9�   H��ɾ�D���;f�   ,�+�	H\ d�?�nᑾ��>(�]�j?(,��)�?   �W�>��\
l?   �P�==>�2p�>@o�3��>��EE��>{O����>�,w�+�>�yV���3?0wD#Hn�<?�ɺV`�=W�4(e�>�����N�=�UE�NN�>�5N�=��o��>�0S.N�=���	��>Uk�v5@ܪMVm�?���$@i\ٳ�Ap����?
   HXR_QD_V01      QUADe��W�>ر��]�ƽ���dKZ����T�X���z�[K��ձ�'��޽{�:Ĳ�޻���e�I�>��,?��=�.�X���=/$��R��=�%�[:�=�������;�7�mY@�>M��@+�=�	[�)�-=���4��=���H�yj;���D�>4�<�{#=�o��=U*��
pa;�̖�+�>X��5��=��V�)�;�yV���3?�f�p�t"<G�<Hn�<*�E��?��0~A?��r{�?<�UTv?   l[�>��\
l?   T�=����R���0~A��;1t�Ԗ��y\�   ���ɾ�D���;f�   ��+�*�E��?\�O`�T�>��r{�?<�UTv?   l[�>��\
l?   T�=e��W�>���e�I�>�7�mY@�>���D�>�̖�+�>�yV���3?G�<Hn�<�)&M�`�=�Cqae�>A�dO�=��փN�>ස@:O�=�"���><U+�3O�=����>׼+(��
@���7 @��	%@ �G+���Ȇ�Q��?   HXR_DR_QD_ED      DRIF~��AQ��>���ľŽ��29n���Ł�����_�0!Ơ�x����޽����	޻���e�I�>�<�lٕ=�.�X���=��jfR��=�%�[:�=���f���;�����>�>��>�`�=��$��.=��c�=o�:��ak;���D�>9�t�{#=�o��=��Zpa;�HKR�+�>���9��=PW��)�;�yV���3?k+��t"<[!��Hn�<o���"�?��0~A?�����?<�UTv?   �q�>��\
l?   @h�=�N3�A� ���0~A��[�=��Ԗ��y\�   ��ɾ�D���;f�   X�+�o���"�?\�O`�T�>�����?<�UTv?   �q�>��\
l?   @h�=~��AQ��>���e�I�>�����>�>���D�>�HKR�+�>�yV���3?[!��Hn�<�)&M�`�=�Cqae�>B�dO�=��փN�>��@:O�=�"���>^U+�3O�= ����>l�Y�oA	@K���,�?V_��&@p=�� ��D<j�?	   HXR_DR_V2      DRIFF��`�M�>���􄷽vc�~���&�%n�u���U��ȕ� �Fk�d۽9�q��ӻ���e�I�><��ܘ��=�.�X���=����N��=�%�[:�=6P���;�/�,��>j�����=t�<�
5=�y\�1��=�m��r;���D�>/���|#=�o��=E�+��pa;����+�>lOb\��=-]��)�;�yV���3?Tƅu"<P��On�<`fA'�Q�>��0~A?�W��;#?<�UTv?   �n�>��\
l?   2M�=w�g�������0~A���XC�"�Ԗ��y\�   ���ɾ�D���;f�   (�+�`fA'�Q�>\�O`�T�>�W��;#?<�UTv?   �n�>��\
l?   2M�=F��`�M�>���e�I�>�/�,��>���D�>����+�>�yV���3?P��On�<�)&M�`�=�Cqae�>?�dO�=��փN�>2��@:O�=^#���>�V+�3O�=� ����>���7�?�]����?�OU�?7@|e�B�'�Lw~+8�?   HXR_DR_QD_ED      DRIF�x�	�q�>I�l*����{���d���y�~n扽m*�ƾ��Ŀ��۽�}@�һ���e�I�>��T=w�=�.�X���= ��N��=�%�[:�=AJg
���;U�=)�D�>�hc �=����ɋ5=�gͩU�=G`-��Hs;���D�>4���|#=�o��=C�%�pa;��@Z�+�>�1U3_��=����)�;�yV���3?Ǜj�u"<��hPn�<��nq��>��0~A?%n�(�#?<�UTv?   ���>��\
l?   ha�=b��W�����0~A�r�~��H#�Ԗ��y\�   ��ɾ�D���;f�   ��+���nq��>\�O`�T�>%n�(�#?<�UTv?   ���>��\
l?   ha�=�x�	�q�>���e�I�>U�=)�D�>���D�>��@Z�+�>�yV���3?��hPn�<�)&M�`�=�Cqae�>?�dO�=��փN�>$��@:O�=�"���>�U+�3O�= ����>���O��?�;���,�?+p N�8@t��Hj(��Y%�~�?
   HXR_QD_V02      QUAD�m�K�I�>��_\���:�bY{��n���j�̪_�����~Mq��۽5��F�cһЙyт�>���W���=�&_�V=�P��և=�1'Ļ��LA��X�;j��eX�>�����=�� 6�5=i��`�u�=Xz�UYs;p�R���>��W��<W0���4}=j}�~��2;w��g�+�>/i�_��=N����)�;�yV���3?�}�.u"<�SrPn�<��n�w(�>M���L�>���7�#?(�i7
��>   ���>��\
l?    d�=��J�����M���L���e9^#�(�i7
���   ���ɾ�D���;f�    �+���n�w(�>�/Ӽ�M�>���7�#?�\}b���>   ���>��\
l?    d�=�m�K�I�>Йyт�>j��eX�>p�R���>w��g�+�>�yV���3?�SrPn�<��Q`�=�vF]!e�>g�p��N�=���5DN�>�t�ɩK�=,�2�>��K�=�>J.�>d���;�?�l����?�ׁ_��8@�x�*�$��Y%�~�?   HXR_COR      KICKER�m�K�I�>��_\���:�bY{��n���j�̪_�����~Mq��۽5��F�cһЙyт�>���W���=�&_�V=�P��և=�1'Ļ��LA��X�;j��eX�>�����=�� 6�5=i��`�u�=Xz�UYs;p�R���>��W��<W0���4}=j}�~��2;w��g�+�>/i�_��=N����)�;�yV���3?�}�.u"<�SrPn�<��n�w(�>M���L�>���7�#?(�i7
��>   ���>��\
l?    d�=��J�����M���L���e9^#�(�i7
���   ���ɾ�D���;f�    �+���n�w(�>�/Ӽ�M�>���7�#?�\}b���>   ���>��\
l?    d�=�m�K�I�>Йyт�>j��eX�>p�R���>w��g�+�>�yV���3?�SrPn�<��Q`�=�vF]!e�>g�p��N�=���5DN�>�t�ɩK�=,�2�>��K�=�>J.�>d���;�?�l����?�ׁ_��8@�x�*�$��Y%�~�?   HXR_BPM      MONI�m�K�I�>��_\���:�bY{��n���j�̪_�����~Mq��۽5��F�cһЙyт�>���W���=�&_�V=�P��և=�1'Ļ��LA��X�;j��eX�>�����=�� 6�5=i��`�u�=Xz�UYs;p�R���>��W��<W0���4}=j}�~��2;w��g�+�>/i�_��=N����)�;�yV���3?�}�.u"<�SrPn�<��n�w(�>M���L�>���7�#?(�i7
��>   ���>��\
l?    d�=��J�����M���L���e9^#�(�i7
���   ���ɾ�D���;f�    �+���n�w(�>�/Ӽ�M�>���7�#?�\}b���>   ���>��\
l?    d�=�m�K�I�>Йyт�>j��eX�>p�R���>w��g�+�>�yV���3?�SrPn�<��Q`�=�vF]!e�>g�p��N�=���5DN�>�t�ɩK�=,�2�>��K�=�>J.�>d���;�?�l����?�ׁ_��8@�x�*�$��46��?
   HXR_QD_V02      QUAD3'��>�>v��-�n=���=��.��?��x=*�|l_o���R,�;۽���Kһ�1���>)������=2��I0�r�E!�wo=͓���0Ͻ��C�'.�;���{�R�>���2�oڽq�����5=���m�=Fa��Ps;������>C��A�r�-�]�Ft��+����vY�$G�l�+�>0~�_��=���)�;�yV���3?�$p:u"<3�sPn�<D��j��>>����?7Z�ޔ�#?�3��2�?   ���>��\
l?   2e�=��B�����rW�����8Z#��3��2��   h��ɾ�D���;f�    �+�D��j��>>����?7Z�ޔ�#?�o�Ȕ?   ���>��\
l?   2e�=3'��>�>�1���>���{�R�>������>$G�l�+�>�yV���3?3�sPn�<6j� `�=�?0%�d�>�c�N�=�LhN�>���PH�=b��G��>͍�YIH�=Q�#���>��4��$�?=������M���8@�E��e�@�P���?   HXR_DR_QD_ED      DRIF2&�F_L�>�3�OJ~=$/�H��F;%�w=�B��;���O��ܽ1rh�һ�1���>��X-Jc�=2��I0�r�\'�wo=͓���0Ͻ]��'.�;�Op:�	�>f�j�	ڽɈ���55=�ʇ(l �=����r;������>���0�r�-�]�Ft����i��vY����+�>V��:a��=���s�)�;�yV���3?��BGu"<�/��Pn�<��^� ?>����?ܿ�j�#?�3��2�?   ؒ�>��\
l?   >n�=�թ��N����rW��DS�9M#��3��2��   ���ɾ�D���;f�   (�+���^� ?>����?ܿ�j�#?�o�Ȕ?   ؒ�>��\
l?   >n�=2&�F_L�>�1���>�Op:�	�>������>���+�>�yV���3?�/��Pn�<6j� `�=�?0%�d�>�c�N�=�LhN�>n��PH�=6��G��>���YIH�=%�#���>(d{��>�?�X1�/s�p�7@�a�M�@j�2Ռ@	   HXR_DR_V3      DRIF���3���>9�R��?�=�a�F�M��yՍ�B=N+�Ou������潞�x��)̻�1���>GZ1�8>{=2��I0�r��',�wo=͓���0Ͻ`߹,).�;Q �]���>�/��CKѽo[H�$*=�� �ؕ�=��_fg;������>%k}�s�-�]�Ft��c�
�wY��`�#�+�>���:���=�[��)�;�yV���3?\�b.u"<'ɁYn�<őua�?>����?�#��?�3��2�?   �k �>��\
l?   �5�=�iJK����rW�������3��2��   ��ɾ�D���;f�   8�+�őua�?>����?�#��?�o�Ȕ?   �k �>��\
l?   �5�=���3���>�1���>Q �]���>������>�`�#�+�>�yV���3?'ɁYn�<6j� `�=�?0%�d�>�c�N�=�LhN�>��PH�=A��G��>���YIH�=0�#���>x,O3�@�,aF���c�ن�%@Q�yZF@;��+T�@   HXR_DR_QD_ED      DRIF��ƍ�><��W�,�=��B�M�����u%�5=9�CK��(�-ZZ�s�H(�˻�1���>_�ӌ�Cz=2��I0�r��- -�wo=͓���0Ͻ�J;).�;�����>�~���нҜ�g)=�ʾS(�=}��-��f;������>�� l�s�-�]�Ft���\��wY��v��+�>��Ƅ��=��V�)�;�yV���3?�:o/u"<��hpYn�<:'��b$?>����?D'���?�3��2�?   �u �>��\
l?   �>�=��Y�����rW��2f��W��3��2��    ��ɾ�D���;f�   `�+�:'��b$?>����?D'���?�o�Ȕ?   �u �>��\
l?   �>�=��ƍ�>�1���>�����>������>�v��+�>�yV���3?��hpYn�<6j� `�=�?0%�d�>�c�N�=�LhN�>��PH�=A��G��>���YIH�=0�#���>������@�yB���C*���$@6m���@�c��@
   HXR_QD_V03      QUAD��i���>���c�ï=��90�a��8�J��tH������뎽׬cATo�$���ή˻糱?3��>�.��@~=��M0n�a�%�@�w=B��\:ý�ﺐ8.�;Ʋ9��>[��e�ǽ�×=)=d�s:�=5_y�f;λ����>����Y��&�Y�����p�Y�S��s��+�>*�(���=�|�q�)�;�yV���3?ʀ�/u"<���Yn�<�nԏL>?�Z	]�>��s�Tb?M��i��?   �w �>��\
l?   ^@�=�Ͻ=����Z	]�����֬;�M��i���   ��ɾ�D���;f�   \�+��nԏL>?�vK�9�>��s�Tb?��r7Q ?   �w �>��\
l?   ^@�=��i���>糱?3��>Ʋ9��>λ����>�s��+�>�yV���3?���Yn�<㧓5`�=hEɦ�d�>�\�N�=��ՀN�>����H�=��3r�>���O�H�=�`��m�>�@AO�@Eگ1P�濅2r�$@�b�-	@�c��@   HXR_COR      KICKER��i���>���c�ï=��90�a��8�J��tH������뎽׬cATo�$���ή˻糱?3��>�.��@~=��M0n�a�%�@�w=B��\:ý�ﺐ8.�;Ʋ9��>[��e�ǽ�×=)=d�s:�=5_y�f;λ����>����Y��&�Y�����p�Y�S��s��+�>*�(���=�|�q�)�;�yV���3?ʀ�/u"<���Yn�<�nԏL>?�Z	]�>��s�Tb?M��i��?   �w �>��\
l?   ^@�=�Ͻ=����Z	]�����֬;�M��i���   ��ɾ�D���;f�   \�+��nԏL>?�vK�9�>��s�Tb?��r7Q ?   �w �>��\
l?   ^@�=��i���>糱?3��>Ʋ9��>λ����>�s��+�>�yV���3?���Yn�<㧓5`�=hEɦ�d�>�\�N�=��ՀN�>����H�=��3r�>���O�H�=�`��m�>�@AO�@Eگ1P�濅2r�$@�b�-	@�c��@   HXR_BPM      MONI��i���>���c�ï=��90�a��8�J��tH������뎽׬cATo�$���ή˻糱?3��>�.��@~=��M0n�a�%�@�w=B��\:ý�ﺐ8.�;Ʋ9��>[��e�ǽ�×=)=d�s:�=5_y�f;λ����>����Y��&�Y�����p�Y�S��s��+�>*�(���=�|�q�)�;�yV���3?ʀ�/u"<���Yn�<�nԏL>?�Z	]�>��s�Tb?M��i��?   �w �>��\
l?   ^@�=�Ͻ=����Z	]�����֬;�M��i���   ��ɾ�D���;f�   \�+��nԏL>?�vK�9�>��s�Tb?��r7Q ?   �w �>��\
l?   ^@�=��i���>糱?3��>Ʋ9��>λ����>�s��+�>�yV���3?���Yn�<㧓5`�=hEɦ�d�>�\�N�=��ՀN�>����H�=��3r�>���O�H�=�`��m�>�@AO�@Eگ1P�濅2r�$@�b�-	@��ڨ�,@
   HXR_QD_V03      QUADs �X���>��,D��=�ߡ��4��ޤHΌ]����������;�Vz��� ��˻�uQa��>� � �=&��yϛd�#�B��=��̋H��][<�;gW��A��>�s��f��D��b)=��ā��=q���{f;��@&�>/����8��-�x��s���K�R&г�+�>^qs1���=��v�)�;�yV���3?�F��/u"<���Yn�<��^�P?;������>��=x!P?�p��>   �x �>��\
l?   6A�=`�����;������sW�.�+��p���   ��ɾ�D���;f�   H�+���^�P?[���b�>��=x!P?���')�>   �x �>��\
l?   6A�=s �X���>�uQa��>gW��A��>��@&�>R&г�+�>�yV���3?���Yn�<S�8bJ`�=\�%�e�>u�@�N�=�ڕi:N�>܎r��I�=МG���>�����I�=S1��>q�07@?-��Cؿ+���g$@Om�/A��?t����@   HXR_DR_QD_ED      DRIFt�g �>�͘6���=n9����u}�_��_���덽k�W�h�罯;H���ʻ�uQa��>�AR�yۀ=&��yϛd��yqᗈ=��̋H��,[<�;������>�}6eG����U���(=w�T p٣=\]uوf;��@&�>�e	��8��-�x�l�(���K�4�Ѱ�+�>�f{���=�Zi�)�;�yV���3?����/u"<>�sYn�<��|oj�?;������>�	_�?�p��>   �z �>��\
l?   RC�=%�Z��B�;������&��+��p���   p��ɾ�D���;f�   С+���|oj�?[���b�>�	_�?���')�>   �z �>��\
l?   RC�=t�g �>�uQa��>������>��@&�>4�Ѱ�+�>�yV���3?>�sYn�<S�8bJ`�=\�%�e�>u�@�N�=�ڕi:N�>܎r��I�=МG���>�����I�=S1��>��@X_k@PB��ٿ���$@�?`u��?V�ŧ�U@	   HXR_DR_V4      DRIF��cA��>�!�xp��= J�x\,��f���7.���9��0/{=nJ2��l�[�7Y�;�uQa��>J���U=&��yϛd�% J��=��̋H���Q<�;�Dñ�q�>.��� ��k8�+�<�>G���=�!�_�b7;��@&�>������8��-�x����K�F�e�+�>�Hǔ��=�X��)�;�yV���3?ə(�+u"<��U�Vn�<�E�O��?;������>+��H2:?�p��>   � �>��\
l?   (��=<�v�2~�;������+��H2:��p���   (��ɾ�D���;f�   Ї+��E�O��?[���b�>�G�(^Z?���')�>   � �>��\
l?   (��=��cA��>�uQa��>�Dñ�q�>��@&�>F�e�+�>�yV���3?��U�Vn�<Q�8bJ`�=Y�%�e�>s�@�N�=�ڕi:N�>܎r��I�=МG���>�����I�=S1��>&:w��e @��C������Ji��
@9N�Y�5�?�;,��@	   HXR_DR_CT      DRIF��x[��> ��RU�=-�mǄ�4(������D�qV~=��?/�콭T0,�;�uQa��>��ac�P=&��yϛd��A�ދ�=��̋H����P<�;�}����>Q�>�ۡ��9�g��<�a]����=�8/�4�1;��@&�>r�%��8��-�x��
���K�Ə�+�>7��U���=�����)�;�yV���3?|�Bk+u"<�1�Vn�<��v�k6?;������>Eq1��?�p��>   (� �>��\
l?   8��=�}�����;������Eq1����p���   ��ɾ�D���;f�   ��+���v�k6?[���b�>㼲篘?���')�>   (� �>��\
l?   8��=��x[��>�uQa��>�}����>��@&�>Ə�+�>�yV���3?�1�Vn�<R�8bJ`�=Z�%�e�>t�@�N�=�ڕi:N�>܎r��I�=МG���>�����I�=S1��>��Î�!@��tSF������	@�c�di��?�;,��@   HXR_WA_M_OU      WATCH��x[��> ��RU�=-�mǄ�4(������D�qV~=��?/�콭T0,�;�uQa��>��ac�P=&��yϛd��A�ދ�=��̋H����P<�;�}����>Q�>�ۡ��9�g��<�a]����=�8/�4�1;��@&�>r�%��8��-�x��
���K�Ə�+�>7��U���=�����)�;�yV���3?|�Bk+u"<�1�Vn�<��v�k6?;������>Eq1��?�p��>   (� �>��\
l?   8��=�}�����;������Eq1����p���   ��ɾ�D���;f�   ��+���v�k6?[���b�>㼲篘?���')�>   (� �>��\
l?   8��=��x[��>�uQa��>�}����>��@&�>Ə�+�>�yV���3?�1�Vn�<R�8bJ`�=Z�%�e�>t�@�N�=�ڕi:N�>܎r��I�=МG���>�����I�=S1��>��Î�!@��tSF������	@�c�di��?$��5�@   HXR_DR_QD_ED      DRIF�$ߥ0��>�]�k���=��������M�x������87�=��FA��<k�룼;�uQa��>��hh�M=&��yϛd��4����=��̋H��N�P<�;*��&n��>��n�3�������<._}���=ٹ���-;��@&�>XX�~��8��-�x�9����K�{���+�>�;�����=��[{�)�;�yV���3? 
X+u"<���xVn�<嵔Q�l?;������>{�3�p�?�p��>   D� �>��\
l?   V��=�_C��8�;������{�3�p���p���   x��ɾ�D���;f�   `�+�嵔Q�l?[���b�>�"�!�3?���')�>   D� �>��\
l?   V��=�$ߥ0��>�uQa��>*��&n��>��@&�>{���+�>�yV���3?���xVn�<V�8bJ`�=_�%�e�>x�@�N�=�ڕi:N�>܎r��I�=МG���>�����I�=S1��>Y,���c!@��������O)n��A	@;���+�?~1Uj��@	   HXR_QD_FH      QUAD+�>���>�!���g��}n����������>����)_,�=�^�����i����;����,��>&ƿ4H@h=Wv�5!�++��CCy=�Hj�^��=h�M���;q�+��>˝@�H�h=������<���
���=��xPr,;Fr���>��t��{��f�}�_�j�_��eJ�m���+�>������=���t�)�;�yV���3?��]I+u"<|MsVn�<�i��3p?-���g��>��4�L�?��+*�>   p� �>��\
l?   ���=1�@D�>�-���g�ྮ�4�L����+*��   P��ɾ�D���;f�   H�+��i��3p?�� GE;�>�[f�$?�>   p� �>��\
l?   ���=+�>���>����,��>q�+��>Fr���>m���+�>�yV���3?|MsVn�<+?���_�=(P�bd�>��3LPN�=��ZzM�>ƙ0pJ�=�8�d�>��8piJ�=�eD`�>�L���n!@��Z��&�?���*@3	@���NK��~1Uj��@   HXR_COR      KICKER+�>���>�!���g��}n����������>����)_,�=�^�����i����;����,��>&ƿ4H@h=Wv�5!�++��CCy=�Hj�^��=h�M���;q�+��>˝@�H�h=������<���
���=��xPr,;Fr���>��t��{��f�}�_�j�_��eJ�m���+�>������=���t�)�;�yV���3?��]I+u"<|MsVn�<�i��3p?-���g��>��4�L�?��+*�>   p� �>��\
l?   ���=1�@D�>�-���g�ྮ�4�L����+*��   P��ɾ�D���;f�   H�+��i��3p?�� GE;�>�[f�$?�>   p� �>��\
l?   ���=+�>���>����,��>q�+��>Fr���>m���+�>�yV���3?|MsVn�<+?���_�=(P�bd�>��3LPN�=��ZzM�>ƙ0pJ�=�8�d�>��8piJ�=�eD`�>�L���n!@��Z��&�?���*@3	@���NK��~1Uj��@   HXR_BPM      MONI+�>���>�!���g��}n����������>����)_,�=�^�����i����;����,��>&ƿ4H@h=Wv�5!�++��CCy=�Hj�^��=h�M���;q�+��>˝@�H�h=������<���
���=��xPr,;Fr���>��t��{��f�}�_�j�_��eJ�m���+�>������=���t�)�;�yV���3?��]I+u"<|MsVn�<�i��3p?-���g��>��4�L�?��+*�>   p� �>��\
l?   ���=1�@D�>�-���g�ྮ�4�L����+*��   P��ɾ�D���;f�   H�+��i��3p?�� GE;�>�[f�$?�>   p� �>��\
l?   ���=+�>���>����,��>q�+��>Fr���>m���+�>�yV���3?|MsVn�<+?���_�=(P�bd�>��3LPN�=��ZzM�>ƙ0pJ�=�8�d�>��8piJ�=�eD`�>�L���n!@��Z��&�?���*@3	@���NK��ؕ�I�@	   HXR_QD_FH      QUAD��z��>@Y/ONTý������pe��~���?�f%Q�=!6�&ּ�N�@i:�;C�����>�H�
��t=�t�@�"i=�F�k��r=�ő��=#b0!�;�Ԓ�m��>/�qʍS�=N,���`�<K�`���=z$�J�+;B���a�>��N��^E1��a=ƓL	I���]�+�>��ƨ���=[ƿp�)�;�yV���3?�i�5+u"<MVhoVn�<m̗,"_?�4T#e ?�ݟ�
�?>��B.�>   �� �>��\
l?   й�=W�("Y1��4T#e ��ݟ�
��>��B.��   8��ɾ�D���;f�   0�+�m̗,"_?``�b�>��(?{�����>   �� �>��\
l?   й�=��z��>C�����>�Ԓ�m��>B���a�>��]�+�>�yV���3?MVhoVn�<����7_�=���!�c�>ev���M�=��s�L�>�w�1K�=� ����>�B+K�=q�4���>��JĠc!@�B�=��?����E	@B�UZ�x�@'~�A@   HXR_DR_QD_ED      DRIF��jt˙�>��p�ýQ�p�2���KHAH+����i!π=��-?콳�
��;C�����>@�.a�mu=�t�@�"i='U���r=�ő��=��n=!�;z�t����>Oې~��=��ˏ�<��m�麕=��j��%;B���a�>�|�.P��^E1��a=0�]uM	I��L}�+�>	&�ĕ��=���v�)�;�yV���3?6�=�*u"<ј�mVn�<w4�?�4T#e ?sx��?>��B.�>   P� �>��\
l?   j��=�������4T#e �sx���>��B.��   ��ɾ�D���;f�   �+�w4�?``�b�>��L�a?{�����>   P� �>��\
l?   j��=��jt˙�>C�����>z�t����>B���a�>�L}�+�>�yV���3?ј�mVn�<ߘ��7_�=���!�c�>dv���M�=��s�L�>�w�1K�=� ����>�B+K�=q�4���>Go`!@���"�:�?��hJ�	@�;7�1<濈���@   HXR_DR_WIG_ED      DRIF2�cv��>3֦��½m*�kl񉽻o�>����B~���=����g�b6���;C�����>bK<f`�w=�t�@�"i=�u/ܘ�r=�ő��=��hn!�;t�X�3�>L�b�§=	>�Q��<ձ�(#+�=цp���:B���a�>�2�U��^E1��a=��bR	I���1�+�>!W�-���=YiZ��)�;�yV���3?��6*u"<�gVn�<�z�u�X?�4T#e ?���?>��B.�>   <� �>��\
l?   "��=<#��7u��4T#e �����>��B.��   8��ɾ�D���;f�   |�+��z�u�X?``�b�>���v?{�����>   <� �>��\
l?   "��=2�cv��>C�����>t�X�3�>B���a�>��1�+�>�yV���3?�gVn�<ߘ��7_�=���!�c�>dv���M�=��s�L�>�w�1K�=� ����>�B+K�=r�4���>a����z@��g�3��?��[&@�E΂�gX�r1@	   HXR_WIG_0      WIGGLER:w�>��>߸|������W�
��r���U�}�l24Ċ��=�p�c&oӽ���߯�;C�����>�:X��=F��;i=+-5ۖ�r=�ő��=y���;ɐ+���>�>ݯ�=�,;^\�wP���=�o��U�Ճ2��>|N �@��Iű��a=.%�n/I�����+�>���.���=�m�r�)�;�yV���3?����u"<��Rn�<��Th/
?�4T#e ?�b�_~?\��}L+�>   l!�>��\
l?   ���=��Th/
��4T#e ��b�_~�\��}L+��   ���ɾ�D���;f�   �+�ۗ:��?``�b�>�(:��?W�~����>   l!�>��\
l?   ���=:w�>��>C�����>ɐ+���>Ճ2��>����+�>�yV���3?��Rn�<����7_�=���!�c�>ev���M�=��s�L�>�w�1K�=� ����>�B+K�=r�4���>�]����@��B�\��?�Rm��@�]�DN]���9���@   HXR_DR_WIG_ED      DRIF�ӳ�*�>�cI^����#�(a.3����([�,{�:fÝ�=c,^�l�ϽZ�>_k��;C�����>�䬛�?�=F��;i=��4��r=�ő��=��q��;*�+�}�>1�S��[�=�_S\��;��=�|��CX�Ճ2��>6�43F��Iű��a=*UD4I��J��+�>�w虁��=��h��)�;�yV���3?�lu"<n#�Rn�< fLc?�4T#e ?X��y�/?\��}L+�>   P!�>��\
l?   @��= fLc��4T#e �X��y�/�\��}L+��   ���ɾ�D���;f�   `�+�%$+���
?``�b�>����L?W�~����>   P!�>��\
l?   @��=�ӳ�*�>C�����>*�+�}�>Ճ2��>�J��+�>�yV���3?n#�Rn�<ߘ��7_�=���!�c�>dv���M�=��s�L�>�w�1K�=� ����>�B+K�=r�4���>p��:j
@?�>����?B��ձ @Ƈ�@*�����@   HXR_DR_QD_ED	      DRIF��_���>ձO��殽P4A砌�#t,�z��slp�ܒ=�s��ʊͽ�ؐ���;C�����>>�:3M��=F��;i=d�L��r=�ő��=���;/�<E��>����`��=Wd�9��>N�����=�4�9�X�Ճ2��>"^L�G��Iű��a=��5I��(��+�>z�V����=����)�;�yV���3?�w��
u"<!Ot�Rn�<h8����?�4T#e ?7��YK_?\��}L+�>   �!�>��\
l?   ���=h8������4T#e �7��YK_�\��}L+��   ���ɾ�D���;f�   8�+��7��S�	?``�b�>�_��|?W�~����>   �!�>��\
l?   ���=��_���>C�����>/�<E��>Ճ2��>�(��+�>�yV���3?!Ot�Rn�<����7_�=���!�c�>ev���M�=��s�L�>�w�1K�=� ����>�B+K�=o�4���>�} ���	@�_��?7����!@��1*��q/*�n%@	   HXR_QD_DH      QUAD�N]�Y��>����L*u����%{��+f��n��S����=>	ٰͽ�ҳ�f��;$p��'�>��/��=c��p/�(H���Հ=�z���=q�o7%�;�,����>�#���[a=� �y���R���_��=�]^�Y�3Nt��O�>�Q�����x�g��j���>�Aْ��+�>�XZ����=��ؑ�)�;�yV���3?H&�
u"<���Rn�<��3Ԭ?p�W7��>�v�9Cc?R���a�>   `!�>��\
l?   B��=��3Ԭ�p�W7����v�9Cc�R���a�   ���ɾ�D���;f�   (�+�H���	?�EuR-��>U�Nʄ�?G �V��>   `!�>��\
l?   B��=�N]�Y��>$p��'�>�,����>3Nt��O�>Aْ��+�>�yV���3?���Rn�<t�t�_�=�{�d�>�{n5N�=���M�>����I�=����>�"b<�I�=zFv�>y�+=Q�	@�${�8��?��d��!@cK�W��q/*�n%@   HXR_COR      KICKER�N]�Y��>����L*u����%{��+f��n��S����=>	ٰͽ�ҳ�f��;$p��'�>��/��=c��p/�(H���Հ=�z���=q�o7%�;�,����>�#���[a=� �y���R���_��=�]^�Y�3Nt��O�>�Q�����x�g��j���>�Aْ��+�>�XZ����=��ؑ�)�;�yV���3?H&�
u"<���Rn�<��3Ԭ?p�W7��>�v�9Cc?R���a�>   `!�>��\
l?   B��=��3Ԭ�p�W7����v�9Cc�R���a�   ���ɾ�D���;f�   (�+�H���	?�EuR-��>U�Nʄ�?G �V��>   `!�>��\
l?   B��=�N]�Y��>$p��'�>�,����>3Nt��O�>Aْ��+�>�yV���3?���Rn�<t�t�_�=�{�d�>�{n5N�=���M�>����I�=����>�"b<�I�=zFv�>y�+=Q�	@�${�8��?��d��!@cK�W��q/*�n%@   HXR_BPM      MONI�N]�Y��>����L*u����%{��+f��n��S����=>	ٰͽ�ҳ�f��;$p��'�>��/��=c��p/�(H���Հ=�z���=q�o7%�;�,����>�#���[a=� �y���R���_��=�]^�Y�3Nt��O�>�Q�����x�g��j���>�Aْ��+�>�XZ����=��ؑ�)�;�yV���3?H&�
u"<���Rn�<��3Ԭ?p�W7��>�v�9Cc?R���a�>   `!�>��\
l?   B��=��3Ԭ�p�W7����v�9Cc�R���a�   ���ɾ�D���;f�   (�+�H���	?�EuR-��>U�Nʄ�?G �V��>   `!�>��\
l?   B��=�N]�Y��>$p��'�>�,����>3Nt��O�>Aْ��+�>�yV���3?���Rn�<t�t�_�=�{�d�>�{n5N�=���M�>����I�=����>�"b<�I�=zFv�>y�+=Q�	@�${�8��?��d��!@cK�W��˓e¾2@	   HXR_QD_DH      QUADQ�@���>�$;0��=�G��=U���3���)Q�ݽ����=�(�Ǆ�̽�k\^��;y�X�OM�>eJѴ��z=����`���`�n?�=�аefa�=�':��;�ڐ���>��N=
����,����W/���=Tj^fY�����[�>n�����h{��g���_�j6�%�e4=��+�>R$5ȁ��=}���)�;�yV���3?ܼ
u"<V���Rn�<�9vv�??�#�� �>�^2i^W?������>   �!�>��\
l?   ���=�9vv��?�#�� ��^2i^W�������   X��ɾ�D���;f�   �+�(f�#�	?SeN7X��>m����w?������>   �!�>��\
l?   ���=Q�@���>y�X�OM�>�ڐ���>����[�>e4=��+�>�yV���3?V���Rn�<0M�G�_�=�-imd�>.�|WN�=(L�˃M�>`���H�=�H�5�>wٛH�=��1�>�����	@pɹ��d���r !@u�w�?3%S?�g@   HXR_DR_QD_ED
      DRIF$XW9� �>�=`ߊ�=�֘�H����$���R���!P2��=����WV˽��/����;y�X�OM�>$�.@�z=����`��Y2�n?�=�аefa�=������;(��J��>���L��K?�r�0���ڞ�'�=V����AY�����[�>q��3���h{��g��Q�6�%�灂��+�>K ف��=��Jl�)�;�yV���3?���v
u"<�(jRn�<�(H�?�??�#�� �>�:�?������>   (!�>��\
l?   $��=�(H�?��?�#�� ��:��������   ��ɾ�D���;f�   І+�r�ZQ�	?SeN7X��>2��2?������>   (!�>��\
l?   $��=$XW9� �>y�X�OM�>(��J��>����[�>灂��+�>�yV���3?�(jRn�<2M�G�_�=�-imd�>0�|WN�=+L�˃M�>[���H�=�H�5�>�vٛH�=��1�>�ܠpJ
@
���c�H1���� @U� ]��?{�S�/@   HXR_DR_WIG_ED      DRIF�&����>�w�,0$�=���ɉ�al�^�aY�B?�j�=u��m1ƽd�\�e��;y�X�OM�>0|�J��x=����`��d��m?�=�аefa�=1=��;���E��>���ʃ	��(};l������=��Zs��Y�����[�>��
����h{��g���D�4�%����f�+�>������=����)�;�yV���3?��tn	u"<��	Rn�<�lr�??�#�� �>(��?������>   �!�>��\
l?   ��=�lr��?�#�� �(���������   0��ɾ�D���;f�   ��+�>`=��<
?SeN7X��>S��bG?������>   �!�>��\
l?   ��=�&����>y�X�OM�>���E��>����[�>���f�+�>�yV���3?��	Rn�<1M�G�_�=�-imd�>.�|WN�=(L�˃M�>[���H�=�H�5�>�vٛH�=��1�>����F@��|�F�忠�Щ�@>�h�W�?-��u!#@	   HXR_WIG_0      WIGGLER{��1z`�>WK��b�=��~`��C\b�`u��"d��=L�	�r�=+� �z�;y�X�OM�>��<�|Td=���W3�`��cfm?�=�аefa�=�T
��;��VQ��>��}�Q���j.!��s+��=�+����^���&�p�>���vt:�鈳��t���erЪ�%�W�۸�+�>v��m��=a]K��)�;�yV���3?h(K�t"<U7
2Nn�<����??�#�� �>_9�j�l?�W����>   .!�>��\
l?   ��=�����?�#�� ��;m�lN�9!5����   ���ɾ�D���;f�   �+��l��q?SeN7X��>_9�j�l?�W����>   .!�>��\
l?   ��={��1z`�>y�X�OM�>��VQ��>��&�p�>W�۸�+�>�yV���3?U7
2Nn�<0M�G�_�=�-imd�>.�|WN�=(L�˃M�>a���H�=�H�5�>wٛH�=��1�>�65m��@�1d�~��1��$�@W�����?���#@   HXR_DR_WIG_ED      DRIF�0hS�Y�>'V�����=�]`�m����頫�v������=ЏD�~��=�_�����;y�X�OM�>z��Kha=���W3�`��r�l?�=�аefa�=g(F0���;��Mw+��>�	
i�Ť����r�c!���[�؀=�%��'%_���&�p�>q辶t:�鈳��t���|+R��%����y�+�>W�Vm��=�K�V�)�;�yV���3?��8B�t"<~f��Mn�<�<[�R??�#�� �>;iT<�
?�W����>   �3!�>��\
l?   �	�=�<[�R�?�#�� ��@��
�9!5����   ���ɾ�D���;f�   ؁+��G=��?SeN7X��>;iT<�
?�W����>   �3!�>��\
l?   �	�=�0hS�Y�>y�X�OM�>��Mw+��>��&�p�>���y�+�>�yV���3?~f��Mn�<0M�G�_�=�-imd�>.�|WN�=(L�˃M�>a���H�=�H�5�>wٛH�=��1�>�-RG@j������R�I@$m�>��?��a>��#@   HXR_DR_QD_ED      DRIFB����>�� ���=�}.���t��o�w���{��@�=y�����=��� ��;y�X�OM�>X��@5`=���W3�`��r�k?�=�аefa�=U����;j�R�s�>]�=���9��w!��ڮv�=��1h2I_���&�p�>"
B�t:�鈳��t����ਨ%�t�3i�+�>U�"gm��=dX5�)�;�yV���3?D ���t"<��Mn�<!�7:.�??�#�� �><ѣ}q
?�W����>   \5!�>��\
l?   �=!�7:.��?�#�� ﾖ�z��	�9!5����   h��ɾ�D���;f�   ��+�0�����?SeN7X��><ѣ}q
?�W����>   \5!�>��\
l?   �=B����>y�X�OM�>j�R�s�>��&�p�>t�3i�+�>�yV���3?��Mn�<1M�G�_�=�-imd�>/�|WN�=*L�˃M�>a���H�=�H�5�>wٛH�=��1�>{e����@5�z����Ӄ���
@\#ژ;�?���-,�#@	   HXR_QD_FH      QUAD��9	'��>��uټ\k����8����6Z@����QM�=�.z)jD�=c��M��;���{�>�R��iq=��ch2��[h=|	��xT�=	�w�;Υ;x?r"�i�>_8nι@�e�{{�!���G��=��ol\\_�x�t��>GKC\�����0�z�0�Tm��A�	�Ik�+�>�m�qm��=_)6�)�;�yV���3?3T��t"<��4�Mn�<� :	�?���j�>-:�Nc_
?xҔ�\��>   �5!�>��\
l?   ��=� :	�����j���^��	�xҔ�\��   H��ɾ�D���;f�   ��+���t���?��[R��>-:�Nc_
?�m-,��>   �5!�>��\
l?   ��=��9	'��>���{�>x?r"�i�>x�t��>	�Ik�+�>�yV���3?��4�Mn�<	ĭk_�=`�}l�c�>�(���M�=����L�>@��xH�=���>.���qH�=Ѐ�>�m>��@�JD^V4�?(�$>�
@�/yWt��?���-,�#@	   HXR_WA_OU      WATCH��9	'��>��uټ\k����8����6Z@����QM�=�.z)jD�=c��M��;���{�>�R��iq=��ch2��[h=|	��xT�=	�w�;Υ;x?r"�i�>_8nι@�e�{{�!���G��=��ol\\_�x�t��>GKC\�����0�z�0�Tm��A�	�Ik�+�>�m�qm��=_)6�)�;�yV���3?3T��t"<��4�Mn�<� :	�?���j�>-:�Nc_
?xҔ�\��>   �5!�>��\
l?   ��=� :	�����j���^��	�xҔ�\��   H��ɾ�D���;f�   ��+���t���?��[R��>-:�Nc_
?�m-,��>   �5!�>��\
l?   ��=��9	'��>���{�>x?r"�i�>x�t��>	�Ik�+�>�yV���3?��4�Mn�<	ĭk_�=`�}l�c�>�(���M�=����L�>@��xH�=���>.���qH�=Ѐ�>�m>��@�JD^V4�?(�$>�
@�/yWt��?