SDDS1
!# little-endian
&description text="Twiss parameters--input: xls_linac_tmc_hxr_tmc.track.ele  lattice: xls_linac_tmc_hxr.11.lte", contents="Twiss parameters", &end
&parameter name=Step, description="Simulation step", type=long, &end
&parameter name=SVNVersion, description="SVN version number", type=string, fixed_value=unknown, &end
&parameter name=nux, symbol="$gn$r$bx$n", units="1/(2$gp$r)", description="Horizontal tune", type=double, &end
&parameter name=dnux/dp, symbol="$gx$r$bx$n", units="1/(2$gp$r)", description="Horizontal chromaticity", type=double, &end
&parameter name=dnux/dp2, symbol="$gx$r$bx2$n", units="1/(2$gp$r)", description="Horizontal 2nd-order chromaticity", type=double, &end
&parameter name=dnux/dp3, symbol="$gx$r$bx3$n", units="1/(2$gp$r)", description="Horizontal 3rd-order chromaticity", type=double, &end
&parameter name=Ax, symbol="A$bx$n", units=m, description="Horizontal acceptance", type=double, &end
&parameter name=AxLocation, units=m, description="Location of horizontal acceptance limit", type=double, &end
&parameter name=nuy, symbol="$gn$r$by$n", units="1/(2$gp$r)", description="Vertical tune", type=double, &end
&parameter name=dnuy/dp, symbol="$gx$r$by$n", units="1/(2$gp$r)", description="Vertical chromaticity", type=double, &end
&parameter name=dnuy/dp2, symbol="$gx$r$by2$n", units="1/(2$gp$r)", description="Vertical 2nd-order chromaticity", type=double, &end
&parameter name=dnuy/dp3, symbol="$gx$r$by3$n", units="1/(2$gp$r)", description="Vertical 3rd-order chromaticity", type=double, &end
&parameter name=Ay, symbol="A$by$n", units=m, description="Vertical acceptance", type=double, &end
&parameter name=AyLocation, units=m, description="Location of vertical acceptance limit", type=double, &end
&parameter name=deltaHalfRange, symbol="$gDd$r/2", description="Half range of momentum offset for chromatic tune spread evaluation", type=double, &end
&parameter name=nuxChromUpper, symbol="$gx$r$bu$n", description="Upper limit of x tune due to chromaticity and deltaRange", type=double, &end
&parameter name=nuxChromLower, symbol="$gx$r$bu$n", description="Lower limit of x tune due to chromaticity and deltaRange", type=double, &end
&parameter name=nuyChromUpper, symbol="$gy$r$bu$n", description="Upper limit of y tune due to chromaticity and deltaRange", type=double, &end
&parameter name=nuyChromLower, symbol="$gy$r$bu$n", description="Lower limit of y tune due to chromaticity and deltaRange", type=double, &end
&parameter name=Stage, description="Stage of computation", type=string, &end
&parameter name=pCentral, units="m$be$nc", description="Central momentum", type=double, &end
&parameter name=dbetax/dp, units=m, description="Derivative of betax with momentum offset", type=double, &end
&parameter name=dbetay/dp, units=m, description="Derivative of betay with momentum offset", type=double, &end
&parameter name=dalphax/dp, description="Derivative of alphax with momentum offset", type=double, &end
&parameter name=dalphay/dp, description="Derivative of alphay with momentum offset", type=double, &end
&parameter name=etax2, symbol="$gc$r$bx2$n", units=m, description="Second-order dispersion (for matched or periodic case only)", type=double, &end
&parameter name=etay2, symbol="$gc$r$by2$n", units=m, description="Second-order dispersion (for matched or periodic case only)", type=double, &end
&parameter name=etax3, symbol="$gc$r$bx3$n", units=m, description="Third-order dispersion (for matched or periodic case only)", type=double, &end
&parameter name=etay3, symbol="$gc$r$by3$n", units=m, description="Third-order dispersion (for matched or periodic case only)", type=double, &end
&parameter name=etaxp2, symbol="$gc$r$bx2$n$a'$n", units=m, description="Second-order dispersion (for matched or periodic case only)", type=double, &end
&parameter name=etayp2, symbol="$gc$r$by2$n$a'$n", units=m, description="Second-order dispersion (for matched or periodic case only)", type=double, &end
&parameter name=etaxp3, symbol="$gc$r$bx3$n$a'$n", units=m, description="Third-order dispersion (for matched or periodic case only)", type=double, &end
&parameter name=etayp3, symbol="$gc$r$by3$n$a'$n", units=m, description="Third-order dispersion (for matched or periodic case only)", type=double, &end
&parameter name=betaxMin, units=m, description="Minimum betax", type=double, &end
&parameter name=betaxAve, units=m, description="Average betax", type=double, &end
&parameter name=betaxMax, units=m, description="Maximum betax", type=double, &end
&parameter name=betayMin, units=m, description="Minimum betay", type=double, &end
&parameter name=betayAve, units=m, description="Average betay", type=double, &end
&parameter name=betayMax, units=m, description="Maximum betay", type=double, &end
&parameter name=etaxMax, units=m, description="Maximum absolute value of etax", type=double, &end
&parameter name=etayMax, units=m, description="Maximum absolute value of etay", type=double, &end
&parameter name=waistsx, description="Number of changes in the sign of alphax", type=long, &end
&parameter name=waistsy, description="Number of changes in the sign of alphay", type=long, &end
&parameter name=dnux/dAx, units=1/m, description="Horizontal tune shift with horizontal amplitude", type=double, &end
&parameter name=dnux/dAy, units=1/m, description="Horizontal tune shift with vertical amplitude", type=double, &end
&parameter name=dnuy/dAx, units=1/m, description="Vertical tune shift with horizontal amplitude", type=double, &end
&parameter name=dnuy/dAy, units=1/m, description="Vertical tune shift with vertical amplitude", type=double, &end
&parameter name=dnux/dAx2, units="1/m$a2$n", description="Horizontal tune shift with horizontal amplitude", type=double, &end
&parameter name=dnux/dAy2, units="1/m$a2$n", description="Horizontal tune shift with vertical amplitude", type=double, &end
&parameter name=dnux/dAxAy, units="1/m$a2$n", description="Horizontal tune shift with horizontal and vertical amplitude", type=double, &end
&parameter name=dnuy/dAx2, units="1/m$a2$n", description="Vertical tune shift with horizontal amplitude", type=double, &end
&parameter name=dnuy/dAy2, units="1/m$a2$n", description="Vertical tune shift with vertical amplitude", type=double, &end
&parameter name=dnuy/dAxAy, units="1/m$a2$n", description="Vertical tune shift with horizontal and vertical amplitude", type=double, &end
&parameter name=nuxTswaLower, description="Minimum horizontal tune from tune-shift-with-amplitude calculations", type=double, &end
&parameter name=nuxTswaUpper, description="Maximum horizontal tune from tune-shift-with-amplitude calculations", type=double, &end
&parameter name=nuyTswaLower, description="Minimum vertical tune from tune-shift-with-amplitude calculations", type=double, &end
&parameter name=nuyTswaUpper, description="Maximum vertical tune from tune-shift-with-amplitude calculations", type=double, &end
&parameter name=couplingIntegral, description="Coupling integral for difference resonance", type=double, &end
&parameter name=couplingDelta, description="Distance from difference resonance", type=double, &end
&parameter name=emittanceRatio, description="Emittance ratio from coupling integral", type=double, &end
&parameter name=alphac2, symbol="$ga$r$bc2$n", description="2nd-order momentum compaction factor", type=double, &end
&parameter name=alphac, symbol="$ga$r$bc$n", description="Momentum compaction factor", type=double, &end
&parameter name=I1, units=m, description="Radiation integral 1", type=double, &end
&parameter name=I2, units=1/m, description="Radiation integral 2", type=double, &end
&parameter name=I3, units="1/m$a2$n", description="Radiation integral 3", type=double, &end
&parameter name=I4, units=1/m, description="Radiation integral 4", type=double, &end
&parameter name=I5, units=1/m, description="Radiation integral 5", type=double, &end
&parameter name=ex0, units="$gp$rm", description="Damped horizontal emittance", type=double, &end
&parameter name=enx0, units="m$be$nc $gp$rm", description="Damped normalized horizontal emittance", type=double, &end
&parameter name=taux, units=s, description="Horizontal damping time", type=double, &end
&parameter name=Jx, description="Horizontal damping partition number", type=double, &end
&parameter name=tauy, units=s, description="Vertical damping time", type=double, &end
&parameter name=Jy, description="Vertical damping partition number", type=double, &end
&parameter name=Sdelta0, description="RMS fractional energy spread", type=double, &end
&parameter name=taudelta, units=s, description="Longitudinal damping time", type=double, &end
&parameter name=Jdelta, description="Longitudinal damping partition number", type=double, &end
&parameter name=U0, units=MeV, description="Energy loss per turn", type=double, &end
&column name=s, units=m, description=Distance, type=double,  &end
&column name=betax, symbol="$gb$r$bx$n", units=m, description="Horizontal beta-function", type=double,  &end
&column name=alphax, symbol="$ga$r$bx$n", description="Horizontal alpha-function", type=double,  &end
&column name=psix, symbol="$gy$r$bx$n", units=rad, description="Horizontal phase advance", type=double,  &end
&column name=etax, symbol="$gc$r$bx$n", units=m, description="Horizontal dispersion", type=double,  &end
&column name=etaxp, symbol="$gc$r$bx$n$a'$n", description="Slope of horizontal dispersion", type=double,  &end
&column name=xAperture, symbol="a$bx,eff$n", units=m, description="Effective horizontal aperture", type=double,  &end
&column name=betay, symbol="$gb$r$by$n", units=m, description="Vertical beta-function", type=double,  &end
&column name=alphay, symbol="$ga$r$by$n", description="Vertical alpha-function", type=double,  &end
&column name=psiy, symbol="$gy$r$by$n", units=rad, description="Vertical phase advance", type=double,  &end
&column name=etay, symbol="$gc$r$by$n", units=m, description="Vertical dispersion", type=double,  &end
&column name=etayp, symbol="$gc$r$by$n$a'$n", description="Slope of vertical dispersion", type=double,  &end
&column name=yAperture, symbol="a$by,eff$n", units=m, description="Effective vertical aperture", type=double,  &end
&column name=pCentral0, symbol="p$bcent$n", units="m$be$nc", description="Initial central momentum", type=double,  &end
&column name=ElementName, description="Element name", format_string=%10s, type=string,  &end
&column name=ElementOccurence, description="Occurence of element", format_string=%6ld, type=long,  &end
&column name=ElementType, description="Element-type name", format_string=%10s, type=string,  &end
&column name=ChamberShape, type=string,  &end
&column name=dI1, units=m, description="Contribution to radiation integral 1", type=double,  &end
&column name=dI2, units=1/m, description="Contribution to radiation integral 2", type=double,  &end
&column name=dI3, units="1/m$a2$n", description="Contribution to radiation integral 3", type=double,  &end
&column name=dI4, units=1/m, description="Contribution to radiation integral 4", type=double,  &end
&column name=dI5, units=1/m, description="Contribution to radiation integral 5", type=double,  &end
&data mode=binary, &end
&       *YAH'��?/���+�                        ��������5�W��?���/��                        ��������        *YAH'��?*YAH'��?5�W��?5�W��?   tunes uncorrectedB����@��]p$%�މ*�W@S��2����?|$�                                                                ��o��@�!+�;0@�G'"��B@����
��?a�H�m&@¿�0p�;@G��h�l?                                                                                                                                      XP�x>��        �? R���>Xt)�K��h��'�|ؾ.����>�o6��F>l-կ�4{��"��[H1=5� +y_=&��<�4>&��	�@�$D  �?�*uD
�@      �?z�}?C�)?��
�@c�ۻ���?�1��%�?        �{���s4@Ԋ�<�t �                              $@����
��?�5��{ο                              $@B����@   _BEG_      MARK                                                    �{���s4@Ԋ�<�t �                              $@����
��?�5��{ο                              $@B����@   BNCH      CHARGE   ?                                        ��C\��?��Ĩ��7@��=Os�!�~xϓ�?                      $@�Y�;��?��A���_G�pbA�?                      $@B����@	   TMC_DR_V1      DRIF   ?                                        N3��?��W��8@S���"��3N�D(�?                      $@���?�S�?��'L����\��Uk�?                      $@B����@   TMC_DR_QD_ED      DRIF   ?                                        �Ü�9�?��Eƫ+9@�tV^"��a�}6q�?                      $@���Z�:�?��c��>�˕39�?                      $@B����@
   TMC_QD_V01      QUAD   ?                                        �Ü�9�?��Eƫ+9@�tV^"��a�}6q�?                      $@���Z�:�?��c��>�˕39�?                      $@B����@   TMC_COR      KICKER   ?                                        �Ü�9�?��Eƫ+9@�tV^"��a�}6q�?                      $@���Z�:�?��c��>�˕39�?                      $@B����@   TMC_BPM      MONI   ?                                        *��땪�?ٻ�G�G9@��OWp��?��	�G��?                      $@.`�e|i�?�Y�Z��):�_��?                      $@B����@
   TMC_QD_V01      QUAD   ?                                        ���b�?Ya}jZ-9@3�S�>��?���[=�?                      $@�s4���?�������!)u[�?                      $@B����@   TMC_DR_QD_ED      DRIF   ?                                        �F�[��	@3��a��2@�pL#3��?U�K��?                      $@6���C;@���6�9��sa��?                      $@B����@	   TMC_DR_V2      DRIF   ?                                        f����1
@�o�J�2@9�T���?�4[��?                      $@����;@�P1�k����i3��?                      $@B����@   TMC_DR_QD_ED      DRIF   ?                                        ���
@�k(�$3@){�9�_�F�
��c�?                      $@¿�0p�;@�.̢�@�2lrζ�?                      $@B����@
   TMC_QD_V02      QUAD   ?                                        ���
@�k(�$3@){�9�_�F�
��c�?                      $@¿�0p�;@�.̢�@�2lrζ�?                      $@B����@   TMC_COR      KICKER   ?                                        ���
@�k(�$3@){�9�_�F�
��c�?                      $@¿�0p�;@�.̢�@�2lrζ�?                      $@B����@   TMC_BPM      MONI   ?                                        
[9���
@��8�3@���T�-����~���?                      $@�!�+%E;@�I31�+@3������?                      $@B����@
   TMC_QD_V02      QUAD   ?                                        �����,@�9�{=5@������-��^Ru���?                      $@ϧ�̊:@�C��;+@F�Մ���?                      $@B����@   TMC_DR_QD_ED      DRIF   ?                                        D 5&�H@���
?A@L�J3�S	HJ���?                      $@�6.Ę0@h�ݜ�%@5��Q4�?                      $@B����@	   TMC_DR_V3      DRIF   ?                                        ��c��@LI�ȀB@r����3���rk��?                      $@+_>aO`/@f�L%@ms�?�?                      $@B����@   TMC_DR_QD_ED      DRIF   ?                                        m\���@��U谠B@ɽY�(� �|�9N�?                      $@7�ؼ�.@^]��>@��Ww�I�?                      $@B����@
   TMC_QD_V03      QUAD   ?                                        m\���@��U谠B@ɽY�(� �|�9N�?                      $@7�ؼ�.@^]��>@��Ww�I�?                      $@B����@   TMC_COR      KICKER   ?                                        m\���@��U谠B@ɽY�(� �|�9N�?                      $@7�ؼ�.@^]��>@��Ww�I�?                      $@B����@   TMC_BPM      MONI   ?                                        ��mwC@�G'"��B@�c��d@����/�?                      $@��.��-@��Y�͜�?u���T�?                      $@B����@
   TMC_QD_V03      QUAD   ?                                        ��S���@�2�{�B@Nc�!�F@x]yjT�?                      $@U���o-@2ċO�s�?z��H�`�?                      $@B����@   TMC_DR_QD_ED      DRIF   ?                                        �v��@�N"��A@R�8S\�
@}���?                      $@$\d@�,@�j��}��?�#s���?                      $@B����@	   TMC_DR_V4      DRIF   ?                                        �v��@��G�F@@]��N{_	@K�g����?                      $@wǉ{1&+@劖=_��?l�1ن+�?                      $@B����@	   TMC_DR_20      DRIF   ?                                        �s}��@~tQ�qw=@k�hA�@ygyx���?jj�/g�,�<iT�g�L�      $@u�yT�)@}X��i��?hj�����?                      $@B����@   TMC_DP_DIP1   	   CSRCSBEND   ?�V��	q>.����>�o6��&>*�ȯ�4[�:���=�s}��@�~�r��:@��f+��@Ԝ<��B�?8��MrE�<iT�g�L�      $@� ��H(@1.�H	�?3y�d�j�?                      $@B����@   TMC_DR_SIDE_C      CSRDRIFT   ?                                        �V��M!@�+eW;)@mjm�� �?�e<;+�?P����j�<iT�g�L�      $@S����"@O��m�?�JA`��?                      $@B����@   TMC_DR_SIDE      DRIF   ?                                        E��M"@����%@�.�"I�?#)J h��?G��h�l�              $@J;˘_�!@s�D�M�?�s�����?                      $@B����@   TMC_DP_DIP2   	   CSRCSBEND   ?��^Cɾ.����>�o6��&>����4[�(����P"=E���"@�hJ�-$@��q�A��?F|�Dm�?G��h�l�              $@�/艽!@U���?ms�9U�?                      $@B����@   TMC_DR_CENT_C      CSRDRIFT   ?                                        E���"@�hJ�-$@��q�A��?F|�Dm�?G��h�l�              $@�/艽!@U���?ms�9U�?                      $@B����@   TMC_BPM      MONI   ?                                        E���#@)���:!@��'�5�? ���7n�?G��h�l�              $@a*�^!@���.��?��>�?                      $@B����@   TMC_DR_CENT      DRIF   ?                                        �!��$@��H?@�_ђ�}�?;==C�r�?�
���j�<iT�g�L?      $@!ݲo�!@H��)޸?IW��a,�?                      $@B����@   TMC_DP_DIP3   	   CSRCSBEND   ?š�^Cɾ.����>�o6��&> �ȯ�4[���~����<�!��%@o4�`Ѷ@���f���?sdݜ��?d{?��;g�<iT�g�L?      $@2�..� @"g��/��?	X���?                      $@B����@   TMC_DR_SIDE_C      CSRDRIFT   ?                                        ���0�+@��o��@ Xe[y۝?;������? ��/g�,�<iT�g�L?      $@�b�V�"@m:��T4Կq��N @                      $@B����@   TMC_DR_SIDE      DRIF   ?                                        �#�2�,@A�9Ҿ@!4E��¿��"'���?       <              $@5��vV#@��[�׿��񍮺 @                      $@B����@   TMC_DP_DIP4   	   CSRCSBEND   ?����	q>.����>�o6��&>����4[�:��e �<�#�2�-@�� o��	@�!��X�ӿ��wV-�?       <              $@�y^�$$@9� ��ۿ�+�~"@                      $@B����@   TMC_DR_20_C      CSRDRIFT   ?                                        �#�2�-@�� o��	@�!��X�ӿ��wV-�?       <              $@�y^�$$@9� ��ۿ�+�~"@                      $@B����@	   TMC_WA_OU      WATCH   ?                                        