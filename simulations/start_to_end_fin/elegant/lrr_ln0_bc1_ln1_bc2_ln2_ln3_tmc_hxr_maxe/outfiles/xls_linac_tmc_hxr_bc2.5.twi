SDDS1
!# little-endian
&description text="Twiss parameters--input: xls_linac_tmc_hxr_bc2.track.ele  lattice: xls_linac_tmc_hxr.5.lte", contents="Twiss parameters", &end
&parameter name=Step, description="Simulation step", type=long, &end
&parameter name=SVNVersion, description="SVN version number", type=string, fixed_value=unknown, &end
&parameter name=nux, symbol="$gn$r$bx$n", units="1/(2$gp$r)", description="Horizontal tune", type=double, &end
&parameter name=dnux/dp, symbol="$gx$r$bx$n", units="1/(2$gp$r)", description="Horizontal chromaticity", type=double, &end
&parameter name=dnux/dp2, symbol="$gx$r$bx2$n", units="1/(2$gp$r)", description="Horizontal 2nd-order chromaticity", type=double, &end
&parameter name=dnux/dp3, symbol="$gx$r$bx3$n", units="1/(2$gp$r)", description="Horizontal 3rd-order chromaticity", type=double, &end
&parameter name=Ax, symbol="A$bx$n", units=m, description="Horizontal acceptance", type=double, &end
&parameter name=AxLocation, units=m, description="Location of horizontal acceptance limit", type=double, &end
&parameter name=nuy, symbol="$gn$r$by$n", units="1/(2$gp$r)", description="Vertical tune", type=double, &end
&parameter name=dnuy/dp, symbol="$gx$r$by$n", units="1/(2$gp$r)", description="Vertical chromaticity", type=double, &end
&parameter name=dnuy/dp2, symbol="$gx$r$by2$n", units="1/(2$gp$r)", description="Vertical 2nd-order chromaticity", type=double, &end
&parameter name=dnuy/dp3, symbol="$gx$r$by3$n", units="1/(2$gp$r)", description="Vertical 3rd-order chromaticity", type=double, &end
&parameter name=Ay, symbol="A$by$n", units=m, description="Vertical acceptance", type=double, &end
&parameter name=AyLocation, units=m, description="Location of vertical acceptance limit", type=double, &end
&parameter name=deltaHalfRange, symbol="$gDd$r/2", description="Half range of momentum offset for chromatic tune spread evaluation", type=double, &end
&parameter name=nuxChromUpper, symbol="$gx$r$bu$n", description="Upper limit of x tune due to chromaticity and deltaRange", type=double, &end
&parameter name=nuxChromLower, symbol="$gx$r$bu$n", description="Lower limit of x tune due to chromaticity and deltaRange", type=double, &end
&parameter name=nuyChromUpper, symbol="$gy$r$bu$n", description="Upper limit of y tune due to chromaticity and deltaRange", type=double, &end
&parameter name=nuyChromLower, symbol="$gy$r$bu$n", description="Lower limit of y tune due to chromaticity and deltaRange", type=double, &end
&parameter name=Stage, description="Stage of computation", type=string, &end
&parameter name=pCentral, units="m$be$nc", description="Central momentum", type=double, &end
&parameter name=dbetax/dp, units=m, description="Derivative of betax with momentum offset", type=double, &end
&parameter name=dbetay/dp, units=m, description="Derivative of betay with momentum offset", type=double, &end
&parameter name=dalphax/dp, description="Derivative of alphax with momentum offset", type=double, &end
&parameter name=dalphay/dp, description="Derivative of alphay with momentum offset", type=double, &end
&parameter name=etax2, symbol="$gc$r$bx2$n", units=m, description="Second-order dispersion (for matched or periodic case only)", type=double, &end
&parameter name=etay2, symbol="$gc$r$by2$n", units=m, description="Second-order dispersion (for matched or periodic case only)", type=double, &end
&parameter name=etax3, symbol="$gc$r$bx3$n", units=m, description="Third-order dispersion (for matched or periodic case only)", type=double, &end
&parameter name=etay3, symbol="$gc$r$by3$n", units=m, description="Third-order dispersion (for matched or periodic case only)", type=double, &end
&parameter name=etaxp2, symbol="$gc$r$bx2$n$a'$n", units=m, description="Second-order dispersion (for matched or periodic case only)", type=double, &end
&parameter name=etayp2, symbol="$gc$r$by2$n$a'$n", units=m, description="Second-order dispersion (for matched or periodic case only)", type=double, &end
&parameter name=etaxp3, symbol="$gc$r$bx3$n$a'$n", units=m, description="Third-order dispersion (for matched or periodic case only)", type=double, &end
&parameter name=etayp3, symbol="$gc$r$by3$n$a'$n", units=m, description="Third-order dispersion (for matched or periodic case only)", type=double, &end
&parameter name=betaxMin, units=m, description="Minimum betax", type=double, &end
&parameter name=betaxAve, units=m, description="Average betax", type=double, &end
&parameter name=betaxMax, units=m, description="Maximum betax", type=double, &end
&parameter name=betayMin, units=m, description="Minimum betay", type=double, &end
&parameter name=betayAve, units=m, description="Average betay", type=double, &end
&parameter name=betayMax, units=m, description="Maximum betay", type=double, &end
&parameter name=etaxMax, units=m, description="Maximum absolute value of etax", type=double, &end
&parameter name=etayMax, units=m, description="Maximum absolute value of etay", type=double, &end
&parameter name=waistsx, description="Number of changes in the sign of alphax", type=long, &end
&parameter name=waistsy, description="Number of changes in the sign of alphay", type=long, &end
&parameter name=dnux/dAx, units=1/m, description="Horizontal tune shift with horizontal amplitude", type=double, &end
&parameter name=dnux/dAy, units=1/m, description="Horizontal tune shift with vertical amplitude", type=double, &end
&parameter name=dnuy/dAx, units=1/m, description="Vertical tune shift with horizontal amplitude", type=double, &end
&parameter name=dnuy/dAy, units=1/m, description="Vertical tune shift with vertical amplitude", type=double, &end
&parameter name=dnux/dAx2, units="1/m$a2$n", description="Horizontal tune shift with horizontal amplitude", type=double, &end
&parameter name=dnux/dAy2, units="1/m$a2$n", description="Horizontal tune shift with vertical amplitude", type=double, &end
&parameter name=dnux/dAxAy, units="1/m$a2$n", description="Horizontal tune shift with horizontal and vertical amplitude", type=double, &end
&parameter name=dnuy/dAx2, units="1/m$a2$n", description="Vertical tune shift with horizontal amplitude", type=double, &end
&parameter name=dnuy/dAy2, units="1/m$a2$n", description="Vertical tune shift with vertical amplitude", type=double, &end
&parameter name=dnuy/dAxAy, units="1/m$a2$n", description="Vertical tune shift with horizontal and vertical amplitude", type=double, &end
&parameter name=nuxTswaLower, description="Minimum horizontal tune from tune-shift-with-amplitude calculations", type=double, &end
&parameter name=nuxTswaUpper, description="Maximum horizontal tune from tune-shift-with-amplitude calculations", type=double, &end
&parameter name=nuyTswaLower, description="Minimum vertical tune from tune-shift-with-amplitude calculations", type=double, &end
&parameter name=nuyTswaUpper, description="Maximum vertical tune from tune-shift-with-amplitude calculations", type=double, &end
&parameter name=couplingIntegral, description="Coupling integral for difference resonance", type=double, &end
&parameter name=couplingDelta, description="Distance from difference resonance", type=double, &end
&parameter name=emittanceRatio, description="Emittance ratio from coupling integral", type=double, &end
&parameter name=alphac2, symbol="$ga$r$bc2$n", description="2nd-order momentum compaction factor", type=double, &end
&parameter name=alphac, symbol="$ga$r$bc$n", description="Momentum compaction factor", type=double, &end
&parameter name=I1, units=m, description="Radiation integral 1", type=double, &end
&parameter name=I2, units=1/m, description="Radiation integral 2", type=double, &end
&parameter name=I3, units="1/m$a2$n", description="Radiation integral 3", type=double, &end
&parameter name=I4, units=1/m, description="Radiation integral 4", type=double, &end
&parameter name=I5, units=1/m, description="Radiation integral 5", type=double, &end
&parameter name=ex0, units="$gp$rm", description="Damped horizontal emittance", type=double, &end
&parameter name=enx0, units="m$be$nc $gp$rm", description="Damped normalized horizontal emittance", type=double, &end
&parameter name=taux, units=s, description="Horizontal damping time", type=double, &end
&parameter name=Jx, description="Horizontal damping partition number", type=double, &end
&parameter name=tauy, units=s, description="Vertical damping time", type=double, &end
&parameter name=Jy, description="Vertical damping partition number", type=double, &end
&parameter name=Sdelta0, description="RMS fractional energy spread", type=double, &end
&parameter name=taudelta, units=s, description="Longitudinal damping time", type=double, &end
&parameter name=Jdelta, description="Longitudinal damping partition number", type=double, &end
&parameter name=U0, units=MeV, description="Energy loss per turn", type=double, &end
&column name=s, units=m, description=Distance, type=double,  &end
&column name=betax, symbol="$gb$r$bx$n", units=m, description="Horizontal beta-function", type=double,  &end
&column name=alphax, symbol="$ga$r$bx$n", description="Horizontal alpha-function", type=double,  &end
&column name=psix, symbol="$gy$r$bx$n", units=rad, description="Horizontal phase advance", type=double,  &end
&column name=etax, symbol="$gc$r$bx$n", units=m, description="Horizontal dispersion", type=double,  &end
&column name=etaxp, symbol="$gc$r$bx$n$a'$n", description="Slope of horizontal dispersion", type=double,  &end
&column name=xAperture, symbol="a$bx,eff$n", units=m, description="Effective horizontal aperture", type=double,  &end
&column name=betay, symbol="$gb$r$by$n", units=m, description="Vertical beta-function", type=double,  &end
&column name=alphay, symbol="$ga$r$by$n", description="Vertical alpha-function", type=double,  &end
&column name=psiy, symbol="$gy$r$by$n", units=rad, description="Vertical phase advance", type=double,  &end
&column name=etay, symbol="$gc$r$by$n", units=m, description="Vertical dispersion", type=double,  &end
&column name=etayp, symbol="$gc$r$by$n$a'$n", description="Slope of vertical dispersion", type=double,  &end
&column name=yAperture, symbol="a$by,eff$n", units=m, description="Effective vertical aperture", type=double,  &end
&column name=pCentral0, symbol="p$bcent$n", units="m$be$nc", description="Initial central momentum", type=double,  &end
&column name=ElementName, description="Element name", format_string=%10s, type=string,  &end
&column name=ElementOccurence, description="Occurence of element", format_string=%6ld, type=long,  &end
&column name=ElementType, description="Element-type name", format_string=%10s, type=string,  &end
&column name=ChamberShape, type=string,  &end
&column name=dI1, units=m, description="Contribution to radiation integral 1", type=double,  &end
&column name=dI2, units=1/m, description="Contribution to radiation integral 2", type=double,  &end
&column name=dI3, units="1/m$a2$n", description="Contribution to radiation integral 3", type=double,  &end
&column name=dI4, units=1/m, description="Contribution to radiation integral 4", type=double,  &end
&column name=dI5, units=1/m, description="Contribution to radiation integral 5", type=double,  &end
&data mode=binary, &end
)       �J�ay��?/�i'�Z�                        ��������"����#�?j�=b8�                        ��������        �J�ay��?�J�ay��?"����#�?"����#�?   tunes uncorrected��j�@4� 
v�E�Ѵ�,f�I�(8�.�8�{��V�z�                                                                Xq� hd�?NΔr;;@�s~K�aU@��%�M�@�þ&!1 @�����/@w�tG�o�?                                                                                                                                      N�[k`TͿ        <�	�T9?|cڳ0�vPm[s���E�"so??�'�$?�%�8��� n�oC��>�j�I>v�1ʛY�>��FŦ�?D5X� �?;��`S��?      �?�q���'?�����?���6��?��F�3?        i�%OmQ@e{�! ��                              $@5�%�l�@��H�	�?                              $@��j�@   _BEG_      MARK                                                    i�%OmQ@e{�! ��                              $@5�%�l�@��H�	�?                              $@��j�@   BNCH      CHARGE   ?                                                i�%OmQ@e{�! ��                              $@5�%�l�@��H�	�?                              $@��j�@
   BC2_IN_CNT      DRIF   ?                                        ��;E3�?��;5��@^F��3��w/�� ��?                      $@�؁�p"@$[����?�(ӂ���?                      $@��j�@	   BC2_DR_V1      DRIF   ?                                        (_`�u�?k�C~v@*>)������n��?                      $@ _Abef@RH�m	�?ˌ:E���?                      $@��j�@   BC2_DR_QD_ED      DRIF   ?                                        p@�,_��?�`B��@H�e�����4��]�?                      $@�1���@E��7���?��ˬ#��?                      $@��j�@
   BC2_QD_V01      QUAD   ?                                        p@�,_��?�`B��@H�e�����4��]�?                      $@�1���@E��7���?��ˬ#��?                      $@��j�@   BC2_COR      KICKER   ?                                        p@�,_��?�`B��@H�e�����4��]�?                      $@�1���@E��7���?��ˬ#��?                      $@��j�@   BC2_BPM      MONI   ?                                        �!VA�??ٚ?R}@��󕻡��lպ�!�?                      $@r�x >!@7����|�?\�y�l�?                      $@��j�@
   BC2_QD_V01      QUAD   ?                                        �76a�?y �)a@0�b	����c{�?                      $@��%�M�@��,8��?}=b)>k�?                      $@��j�@   BC2_DR_QD_ED      DRIF   ?                                        ��& �	@�:ꕽF@�5��S��V�n�N��?                      $@lg��:�.@�t��rB�ʗ?�E`@                      $@��j�@	   BC2_DR_V2      DRIF   ?                                        ���>5@���JF@��sQM����l ��?                      $@(2	��/@��'ʡ|��Q1K�e@                      $@��j�@   BC2_DR_QD_ED      DRIF   ?                                        �8n�]@�Z���F@2�t �':��a����?                      $@b�&�d�.@�Q'���@�6k4k@                      $@��j�@
   BC2_QD_V02      QUAD   ?                                        �8n�]@�Z���F@2�t �':��a����?                      $@b�&�d�.@�Q'���@�6k4k@                      $@��j�@   BC2_COR      KICKER   ?                                        �8n�]@�Z���F@2�t �':��a����?                      $@b�&�d�.@�Q'���@�6k4k@                      $@��j�@   BC2_BPM      MONI   ?                                        �����@��V�hH@�jR~�H��[�9�?                      $@��^> �-@���\�&@9�G8�p@                      $@��j�@
   BC2_QD_V02      QUAD   ?                                        ���x�@J�T܌J@Zir ��I�(�
�?                      $@E%���+@��6\f%@�6}�v@                      $@��j�@   BC2_DR_QD_ED      DRIF   ?                                        @��E@w���{�R@�/đ�N��8'��#�?                      $@0&�K;�#@dFi�y"@PR{`�@                      $@��j�@	   BC2_DR_V3      DRIF   ?                                        ,cL�ʪ@_�}5>T@�.(`�O�G�W[(�?                      $@:O~�du"@+!��a!@ޒ�uq�@                      $@��j�@   BC2_DR_QD_ED      DRIF   ?                                        U�ې��@�h���+U@jg�*��<�V��sM,�?                      $@����f!@���P�@�3Ԧ��@                      $@��j�@
   BC2_QD_V03      QUAD   ?                                        U�ې��@�h���+U@jg�*��<�V��sM,�?                      $@����f!@���P�@�3Ԧ��@                      $@��j�@   BC2_COR      KICKER   ?                                        U�ې��@�h���+U@jg�*��<�V��sM,�?                      $@����f!@���P�@�3Ԧ��@                      $@��j�@   BC2_BPM      MONI   ?                                        ~kS��@�s~K�aU@.$.�~@:oJ$0�?                      $@�u$�� @У�/I��?���g-�@                      $@��j�@
   BC2_QD_V03      QUAD   ?                                        jm#r;(@�^�
7U@o���^@�|:4�?                      $@䯨
�� @�0ӂEa�?��|�@                      $@��j�@   BC2_DR_QD_ED      DRIF   ?                                        =:�>�@ k�g)pT@�$uw��@���*�G�?                      $@W�~�z @R�E���?d�X�D�@                      $@��j�@	   BC2_DR_V4      DRIF   ?                                        =:�>�@L>��u�R@�q�h�N@H@��y|�?                      $@��
��@?F�$6��?��#�0w@                      $@��j�@	   BC2_DR_20      DRIF   ?                                        ׋g߾� @xCh'kP@L�c��@������?����}}����6���      $@Ls& �@�7���?�w9�*@                      $@��j�@   BC2_DP_DIP1   	   CSRCSBEND   ?̎\?Z2?��E�"sO??�'�?�%�8����MJ񌌡>׋g߾�!@/����|M@V��@��<��?�V1�������6���      $@�sCZ�@n�\fP�?퍩���@                      $@��j�@   BC2_DR_SIDE_C      CSRDRIFT   ?                                        �=���(@��R��8@�\v��@��r���?쿶Pҕ�����6���      $@
L��H@ }��TƿXfY!N�@                      $@��j�@   BC2_DR_SIDE      DRIF   ?                                        f����G)@�L�.�4@�����@�E?�=��?w�tG�o��              $@<�c�g@6����ӿ��>н�	@                      $@��j�@   BC2_DP_DIP2   	   CSRCSBEND   ?��jl��c���E�"sO??�'�?�%�8���D�*��ի>f�����)@9v�|I2@&[�|?@������?w�tG�o��              $@�x[��@���|'{ֿ��WN�S
@                      $@��j�@   BC2_DR_CENT_C      CSRDRIFT   ?                                        f�����)@9v�|I2@&[�|?@������?w�tG�o��              $@�x[��@���|'{ֿ��WN�S
@                      $@��j�@   BC2_BPM      MONI   ?                                        f����G*@����Ӑ0@�_��_�
@�2��o�?w�tG�o��              $@��o�5�@�>hb�ٿ��0w3�
@                      $@��j�@   BC2_DR_CENT      DRIF   ?                                        �t&{+@EL�vϜ)@E�A
=@�d��P��?���Pҕ�����6��?      $@v�SS(@�,����c���0�@                      $@��j�@   BC2_DP_DIP3   	   CSRCSBEND   ?&�jl��c���E�"sO??�'�?�%�8������}>�t&{,@15��+$@Z�{�`J@����)�?ЫK������6��?      $@A�dsa@��q��B�L=�)�,@                      $@��j�@   BC2_DR_SIDE_C      CSRDRIFT   ?                                        {��Dq1@Xq� hd�?{�>���?{��2f�? ���}}����6��?      $@�� TQ�)@"5�p����;�	@                      $@��j�@   BC2_DR_SIDE      DRIF   ?                                        ם���
2@����?�?e�U-�п@����@      y�              $@g?�SK�,@6fC,�����w]$N_@                      $@��j�@   BC2_DP_DIP4   	   CSRCSBEND   ?.�\?Z2?��E�"sO??�'�?�%�8����gz^m�E>ם���
2@����?�?e�U-�п@����@      y�              $@g?�SK�,@6fC,�����w]$N_@                      $@��j�@
   BC2_IN_CNT      DRIF   ?                                        ם����2@E��$%n�?V�� B俘�Sh:@      y�              $@�����/@o�'�Ӂ��@^��¢@                      $@��j�@   BC2_DR_20_C      CSRDRIFT   ?                                        ם����2@E��$%n�?V�� B俘�Sh:@      y�              $@�����/@o�'�Ӂ��@^��¢@                      $@��j�@
   BC2_IN_CNT      DRIF   ?                                        ם����2@E��$%n�?V�� B俘�Sh:@      y�              $@�����/@o�'�Ӂ��@^��¢@                      $@��j�@	   BC2_WA_OU      WATCH   ?                                        