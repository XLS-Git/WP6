SDDS1
!# little-endian
&description text="Twiss parameters--input: xls_linac_tmc_hxr_ln3_bp.track.ele  lattice: xls_linac_tmc_hxr.10.lte", contents="Twiss parameters", &end
&parameter name=Step, description="Simulation step", type=long, &end
&parameter name=SVNVersion, description="SVN version number", type=string, fixed_value=unknown, &end
&parameter name=nux, symbol="$gn$r$bx$n", units="1/(2$gp$r)", description="Horizontal tune", type=double, &end
&parameter name=dnux/dp, symbol="$gx$r$bx$n", units="1/(2$gp$r)", description="Horizontal chromaticity", type=double, &end
&parameter name=dnux/dp2, symbol="$gx$r$bx2$n", units="1/(2$gp$r)", description="Horizontal 2nd-order chromaticity", type=double, &end
&parameter name=dnux/dp3, symbol="$gx$r$bx3$n", units="1/(2$gp$r)", description="Horizontal 3rd-order chromaticity", type=double, &end
&parameter name=Ax, symbol="A$bx$n", units=m, description="Horizontal acceptance", type=double, &end
&parameter name=AxLocation, units=m, description="Location of horizontal acceptance limit", type=double, &end
&parameter name=nuy, symbol="$gn$r$by$n", units="1/(2$gp$r)", description="Vertical tune", type=double, &end
&parameter name=dnuy/dp, symbol="$gx$r$by$n", units="1/(2$gp$r)", description="Vertical chromaticity", type=double, &end
&parameter name=dnuy/dp2, symbol="$gx$r$by2$n", units="1/(2$gp$r)", description="Vertical 2nd-order chromaticity", type=double, &end
&parameter name=dnuy/dp3, symbol="$gx$r$by3$n", units="1/(2$gp$r)", description="Vertical 3rd-order chromaticity", type=double, &end
&parameter name=Ay, symbol="A$by$n", units=m, description="Vertical acceptance", type=double, &end
&parameter name=AyLocation, units=m, description="Location of vertical acceptance limit", type=double, &end
&parameter name=deltaHalfRange, symbol="$gDd$r/2", description="Half range of momentum offset for chromatic tune spread evaluation", type=double, &end
&parameter name=nuxChromUpper, symbol="$gx$r$bu$n", description="Upper limit of x tune due to chromaticity and deltaRange", type=double, &end
&parameter name=nuxChromLower, symbol="$gx$r$bu$n", description="Lower limit of x tune due to chromaticity and deltaRange", type=double, &end
&parameter name=nuyChromUpper, symbol="$gy$r$bu$n", description="Upper limit of y tune due to chromaticity and deltaRange", type=double, &end
&parameter name=nuyChromLower, symbol="$gy$r$bu$n", description="Lower limit of y tune due to chromaticity and deltaRange", type=double, &end
&parameter name=Stage, description="Stage of computation", type=string, &end
&parameter name=pCentral, units="m$be$nc", description="Central momentum", type=double, &end
&parameter name=dbetax/dp, units=m, description="Derivative of betax with momentum offset", type=double, &end
&parameter name=dbetay/dp, units=m, description="Derivative of betay with momentum offset", type=double, &end
&parameter name=dalphax/dp, description="Derivative of alphax with momentum offset", type=double, &end
&parameter name=dalphay/dp, description="Derivative of alphay with momentum offset", type=double, &end
&parameter name=etax2, symbol="$gc$r$bx2$n", units=m, description="Second-order dispersion (for matched or periodic case only)", type=double, &end
&parameter name=etay2, symbol="$gc$r$by2$n", units=m, description="Second-order dispersion (for matched or periodic case only)", type=double, &end
&parameter name=etax3, symbol="$gc$r$bx3$n", units=m, description="Third-order dispersion (for matched or periodic case only)", type=double, &end
&parameter name=etay3, symbol="$gc$r$by3$n", units=m, description="Third-order dispersion (for matched or periodic case only)", type=double, &end
&parameter name=etaxp2, symbol="$gc$r$bx2$n$a'$n", units=m, description="Second-order dispersion (for matched or periodic case only)", type=double, &end
&parameter name=etayp2, symbol="$gc$r$by2$n$a'$n", units=m, description="Second-order dispersion (for matched or periodic case only)", type=double, &end
&parameter name=etaxp3, symbol="$gc$r$bx3$n$a'$n", units=m, description="Third-order dispersion (for matched or periodic case only)", type=double, &end
&parameter name=etayp3, symbol="$gc$r$by3$n$a'$n", units=m, description="Third-order dispersion (for matched or periodic case only)", type=double, &end
&parameter name=betaxMin, units=m, description="Minimum betax", type=double, &end
&parameter name=betaxAve, units=m, description="Average betax", type=double, &end
&parameter name=betaxMax, units=m, description="Maximum betax", type=double, &end
&parameter name=betayMin, units=m, description="Minimum betay", type=double, &end
&parameter name=betayAve, units=m, description="Average betay", type=double, &end
&parameter name=betayMax, units=m, description="Maximum betay", type=double, &end
&parameter name=etaxMax, units=m, description="Maximum absolute value of etax", type=double, &end
&parameter name=etayMax, units=m, description="Maximum absolute value of etay", type=double, &end
&parameter name=waistsx, description="Number of changes in the sign of alphax", type=long, &end
&parameter name=waistsy, description="Number of changes in the sign of alphay", type=long, &end
&parameter name=dnux/dAx, units=1/m, description="Horizontal tune shift with horizontal amplitude", type=double, &end
&parameter name=dnux/dAy, units=1/m, description="Horizontal tune shift with vertical amplitude", type=double, &end
&parameter name=dnuy/dAx, units=1/m, description="Vertical tune shift with horizontal amplitude", type=double, &end
&parameter name=dnuy/dAy, units=1/m, description="Vertical tune shift with vertical amplitude", type=double, &end
&parameter name=dnux/dAx2, units="1/m$a2$n", description="Horizontal tune shift with horizontal amplitude", type=double, &end
&parameter name=dnux/dAy2, units="1/m$a2$n", description="Horizontal tune shift with vertical amplitude", type=double, &end
&parameter name=dnux/dAxAy, units="1/m$a2$n", description="Horizontal tune shift with horizontal and vertical amplitude", type=double, &end
&parameter name=dnuy/dAx2, units="1/m$a2$n", description="Vertical tune shift with horizontal amplitude", type=double, &end
&parameter name=dnuy/dAy2, units="1/m$a2$n", description="Vertical tune shift with vertical amplitude", type=double, &end
&parameter name=dnuy/dAxAy, units="1/m$a2$n", description="Vertical tune shift with horizontal and vertical amplitude", type=double, &end
&parameter name=nuxTswaLower, description="Minimum horizontal tune from tune-shift-with-amplitude calculations", type=double, &end
&parameter name=nuxTswaUpper, description="Maximum horizontal tune from tune-shift-with-amplitude calculations", type=double, &end
&parameter name=nuyTswaLower, description="Minimum vertical tune from tune-shift-with-amplitude calculations", type=double, &end
&parameter name=nuyTswaUpper, description="Maximum vertical tune from tune-shift-with-amplitude calculations", type=double, &end
&parameter name=couplingIntegral, description="Coupling integral for difference resonance", type=double, &end
&parameter name=couplingDelta, description="Distance from difference resonance", type=double, &end
&parameter name=emittanceRatio, description="Emittance ratio from coupling integral", type=double, &end
&parameter name=alphac2, symbol="$ga$r$bc2$n", description="2nd-order momentum compaction factor", type=double, &end
&parameter name=alphac, symbol="$ga$r$bc$n", description="Momentum compaction factor", type=double, &end
&parameter name=I1, units=m, description="Radiation integral 1", type=double, &end
&parameter name=I2, units=1/m, description="Radiation integral 2", type=double, &end
&parameter name=I3, units="1/m$a2$n", description="Radiation integral 3", type=double, &end
&parameter name=I4, units=1/m, description="Radiation integral 4", type=double, &end
&parameter name=I5, units=1/m, description="Radiation integral 5", type=double, &end
&parameter name=ex0, units="$gp$rm", description="Damped horizontal emittance", type=double, &end
&parameter name=enx0, units="m$be$nc $gp$rm", description="Damped normalized horizontal emittance", type=double, &end
&parameter name=taux, units=s, description="Horizontal damping time", type=double, &end
&parameter name=Jx, description="Horizontal damping partition number", type=double, &end
&parameter name=tauy, units=s, description="Vertical damping time", type=double, &end
&parameter name=Jy, description="Vertical damping partition number", type=double, &end
&parameter name=Sdelta0, description="RMS fractional energy spread", type=double, &end
&parameter name=taudelta, units=s, description="Longitudinal damping time", type=double, &end
&parameter name=Jdelta, description="Longitudinal damping partition number", type=double, &end
&parameter name=U0, units=MeV, description="Energy loss per turn", type=double, &end
&column name=s, units=m, description=Distance, type=double,  &end
&column name=betax, symbol="$gb$r$bx$n", units=m, description="Horizontal beta-function", type=double,  &end
&column name=alphax, symbol="$ga$r$bx$n", description="Horizontal alpha-function", type=double,  &end
&column name=psix, symbol="$gy$r$bx$n", units=rad, description="Horizontal phase advance", type=double,  &end
&column name=etax, symbol="$gc$r$bx$n", units=m, description="Horizontal dispersion", type=double,  &end
&column name=etaxp, symbol="$gc$r$bx$n$a'$n", description="Slope of horizontal dispersion", type=double,  &end
&column name=xAperture, symbol="a$bx,eff$n", units=m, description="Effective horizontal aperture", type=double,  &end
&column name=betay, symbol="$gb$r$by$n", units=m, description="Vertical beta-function", type=double,  &end
&column name=alphay, symbol="$ga$r$by$n", description="Vertical alpha-function", type=double,  &end
&column name=psiy, symbol="$gy$r$by$n", units=rad, description="Vertical phase advance", type=double,  &end
&column name=etay, symbol="$gc$r$by$n", units=m, description="Vertical dispersion", type=double,  &end
&column name=etayp, symbol="$gc$r$by$n$a'$n", description="Slope of vertical dispersion", type=double,  &end
&column name=yAperture, symbol="a$by,eff$n", units=m, description="Effective vertical aperture", type=double,  &end
&column name=pCentral0, symbol="p$bcent$n", units="m$be$nc", description="Initial central momentum", type=double,  &end
&column name=ElementName, description="Element name", format_string=%10s, type=string,  &end
&column name=ElementOccurence, description="Occurence of element", format_string=%6ld, type=long,  &end
&column name=ElementType, description="Element-type name", format_string=%10s, type=string,  &end
&column name=ChamberShape, type=string,  &end
&column name=dI1, units=m, description="Contribution to radiation integral 1", type=double,  &end
&column name=dI2, units=1/m, description="Contribution to radiation integral 2", type=double,  &end
&column name=dI3, units="1/m$a2$n", description="Contribution to radiation integral 3", type=double,  &end
&column name=dI4, units=1/m, description="Contribution to radiation integral 4", type=double,  &end
&column name=dI5, units=1/m, description="Contribution to radiation integral 5", type=double,  &end
&data mode=binary, &end
-       3�40�5�?0P.J��                        ��������������?r���^��                        ��������        3�40�5�?3�40�5�?������?������?   tunes uncorrected����@��]C���=��s��9�V
�魥�J?8�                                                                �Б���?;F�$fB@�TQ���c@l�J0s��?.~'&@nHsE@                                                                                                                                              ��,1n�ο                                                                      ��      ��      ��      ��      �      �?      ��      ��      ��                ;26�b@�5��%��                              $@����P@4zM�z�?                              $@����@   _BEG_      MARK                                                    ;26�b@�5��%��                              $@����P@4zM�z�?                              $@����@   BNCH      CHARGE   ?                                        ��A����?gkY��/@�(���\�vIp��0�?                      $@;!��Y@�)Oy��?��X���?                      $@����@   LN3_DR_BP_V1      DRIF   ?                                        ���8�? @57�10@@�Ƅ������%s��?                      $@v�3-@iI��D{�?�5NVƑ�?                      $@����@   LN3_DR_QD_ED      DRIF   ?                                        S��ё @ �9臡0@d���%E �F�>�|��?                      $@��ӝ�@���Ԓ�?i�B×$�?                      $@����@   LN3_QD_BP_V01      QUAD   ?                                        S��ё @ �9臡0@d���%E �F�>�|��?                      $@��ӝ�@���Ԓ�?i�B×$�?                      $@����@   LN3_COR      KICKER   ?                                        S��ё @ �9臡0@d���%E �F�>�|��?                      $@��ӝ�@���Ԓ�?i�B×$�?                      $@����@   LN3_BPM      MONI   ?                                        e�B�� @T��`�1@[�:�+�)��v$�?                      $@V�O�,� @˗�i���?��^m��?                      $@����@   LN3_QD_BP_V01      QUAD   ?                                        <�?��:@WBV�ʹ2@��Eh�,���Lkq�?                      $@q̬�9�?ZB��T�?���u�j�?                      $@����@   LN3_DR_QD_ED      DRIF   ?                                        �lDeRS@,�%O-c@���G�D��G��>�?                      $@7��i�@Of�|��7�CyNJ@                      $@����@   LN3_DR_BP_V2      DRIF   ?                                        ����~@>�K���c@s�\�D�XJ�G�?                      $@��3b��@�@ٚ�����Z@                      $@����@   LN3_DR_QD_ED      DRIF   ?                                        	�Fͧ@�TQ���c@1^L�)@���&P�?                      $@3N#�2@KE���L� -i@                      $@����@   LN3_QD_BP_V02      QUAD   ?                                        	�Fͧ@�TQ���c@1^L�)@���&P�?                      $@3N#�2@KE���L� -i@                      $@����@   LN3_COR      KICKER   ?                                        	�Fͧ@�TQ���c@1^L�)@���&P�?                      $@3N#�2@KE���L� -i@                      $@����@   LN3_BPM      MONI   ?                                        2w	��@m��]c@��^��P@~�a߁X�?                      $@�4�A6@?E�G����7��v@                      $@����@   LN3_QD_BP_V02      QUAD   ?                                        ��'H�@�3�`	�b@��j�OP@\��a�?                      $@=l�`��@��Z�C�������@                      $@����@   LN3_DR_QD_ED      DRIF   ?                                        ��Ǽ�@�x�9}@@�)�C<�>@&�#��?                      $@��^[ۺC@��3�2����@                      $@����@   LN3_DR_BP_V3      DRIF   ?                                        ���A@�_�|�l>@w!�o=@}�Tb��?                      $@	�KQ��D@ۇ��I3���-@                      $@����@   LN3_DR_QD_ED      DRIF   ?                                        #HA�7+@�i:+<@����E�2@�m��?                      $@nHsE@����'�'�b?$@                      $@����@   LN3_QD_BP_V03      QUAD   ?                                        #HA�7+@�i:+<@����E�2@�m��?                      $@nHsE@����'�'�b?$@                      $@����@   LN3_COR      KICKER   ?                                        #HA�7+@�i:+<@����E�2@�m��?                      $@nHsE@����'�'�b?$@                      $@����@   LN3_BPM      MONI   ?                                        L��k-T@]��5y\;@MZ8��#@],��?                      $@[-���D@6�<m @	}o� @                      $@����@   LN3_QD_BP_V03      QUAD   ?                                        8����@n)j7��:@�M(p��"@{���)�?                      $@k1���D@<�B5��@;R[�,"@                      $@����@   LN3_DR_QD_ED      DRIF   ?                                        4��WL@���Z��6@�)%o!@��w���?                      $@��q�C@}�v��@FHI�|,@                      $@����@   LN3_DR_BP_V4      DRIF   ?                                        �W$L@��'h�3@����\ @ܾؽ*I�?                      $@LI<e�A@o4��Nk@�a�m�7@                      $@����@   LN3_DR_BP_0      DRIF   ?                                        4��WL@-�=��2@�T��߂@����s�?                      $@�O�S�9A@�ܞ7"@�!d�:@                      $@����@   LN3_DR_BP_BL      DRIF   ?                                        s,���@ᅶ�1@��|b�d@�-����?                      $@l?~��@@�:�7��@ �{ �?@                      $@����@   LN3_DR_SCA_ED      DRIF   ?                                        ��`�@�>Ġ�$@ �b�;@U����H�?                      $@���%A:@յ9�.>@��|�d@                      $@����@   LN3_SCA0      RFDF   ?                                        ��`�@�>Ġ�$@ �b�;@U����H�?                      $@���%A:@յ9�.>@��|�d@                      $@����@   LN3_SZW      WAKE   ?                                        ��`�@�>Ġ�$@ �b�;@U����H�?                      $@���%A:@յ9�.>@��|�d@                      $@����@   LN3_STW      TRWAKE   ?                                        �W$L@�0Up�"@�1+��@G�l%��?                      $@�z�
9@�v永@dbomk@                      $@����@   LN3_DR_SCA_ED      DRIF   ?                                        4��WL@�Mh௷!@�A�_q@�v�/�,�?                      $@ybr�j8@7�<�h@�f�k�o@                      $@����@   LN3_DR_BP_BL      DRIF   ?                                        �W$L@�t�8e@ɀ�ʮ�@A�-ˡ��?                      $@���Z
6@)^���'@NNV�9�@                      $@����@   LN3_DR_BP_0      DRIF   ?                                        �$��@ށ�@��?��
@���_���?                      $@�G�ŕ�1@��?l�@g�(���@                      $@����@	   LN3_DR_BP      DRIF   ?                                        ��+� @LJ���@&=��R@\�����?                      $@�8�E�0@���@��=U��@                      $@����@   LN3_DR_BP_VS      DRIF   ?                                        N���r� @bcn��?:sު`��?��&w�?                      $@Ҙ�e�)@�6����@��	?��@                      $@����@	   LN3_DR_BP      DRIF   ?                                        ��^Eٿ!@�Б���?@Z�Y�5�?�Ic]^8�?                      $@�$����"@�FV57f@���C	@                      $@����@   LN3_DP_SEPM      DRIF   ?                                        ��+��"@�\E?.��?�[)?���r��e�@                      $@uގ�[K@]�p�lc@�����	@                      $@����@	   LN3_DR_BP      DRIF   ?                                        N���rY#@�[�l�@o����7�F��=j@                      $@(�����@%V��`@Yz:��D
@                      $@����@	   LN3_DR_BP      DRIF   ?                                        _ū?&$@���
@���I���aQ�r�g@                      $@If�K��@ڻK����?���	v3@                      $@����@	   LN3_DR_BP      DRIF   ?                                        �+�x�$@�t��� @Aܐϗ��>oLP��@                      $@��+�1�?jˀ;��?[*;��@                      $@����@	   LN3_DR_BP      DRIF   ?                                        ��^Eٿ%@c���3*@^(�h8������7	@                      $@ҧ�p�v�?�k�a�?���c��@                      $@����@	   LN3_DR_BP      DRIF   ?                                        �+�x�%@6;���,@w>�D��1пh�F	@                      $@���|�?s{sz��?	_��F`@                      $@����@   LN3_DR_BP_VS      DRIF   ?                                        ��^Eٿ&@z�&�t~4@�7ww ���Xv	@                      $@l�J0s��?��>x��ο7���@                      $@����@	   LN3_DR_BP      DRIF   ?                                        ��^Eٿ&@z�&�t~4@�7ww ���Xv	@                      $@l�J0s��?��>x��ο7���@                      $@����@   LN3_BP_WA_OU_      WATCH   ?                                        