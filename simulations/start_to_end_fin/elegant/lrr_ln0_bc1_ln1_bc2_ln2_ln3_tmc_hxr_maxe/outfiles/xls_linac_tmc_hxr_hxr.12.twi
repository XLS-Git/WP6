SDDS1
!# little-endian
&description text="Twiss parameters--input: xls_linac_tmc_hxr_hxr.track.ele  lattice: xls_linac_tmc_hxr.12.lte", contents="Twiss parameters", &end
&parameter name=Step, description="Simulation step", type=long, &end
&parameter name=SVNVersion, description="SVN version number", type=string, fixed_value=unknown, &end
&parameter name=nux, symbol="$gn$r$bx$n", units="1/(2$gp$r)", description="Horizontal tune", type=double, &end
&parameter name=dnux/dp, symbol="$gx$r$bx$n", units="1/(2$gp$r)", description="Horizontal chromaticity", type=double, &end
&parameter name=dnux/dp2, symbol="$gx$r$bx2$n", units="1/(2$gp$r)", description="Horizontal 2nd-order chromaticity", type=double, &end
&parameter name=dnux/dp3, symbol="$gx$r$bx3$n", units="1/(2$gp$r)", description="Horizontal 3rd-order chromaticity", type=double, &end
&parameter name=Ax, symbol="A$bx$n", units=m, description="Horizontal acceptance", type=double, &end
&parameter name=AxLocation, units=m, description="Location of horizontal acceptance limit", type=double, &end
&parameter name=nuy, symbol="$gn$r$by$n", units="1/(2$gp$r)", description="Vertical tune", type=double, &end
&parameter name=dnuy/dp, symbol="$gx$r$by$n", units="1/(2$gp$r)", description="Vertical chromaticity", type=double, &end
&parameter name=dnuy/dp2, symbol="$gx$r$by2$n", units="1/(2$gp$r)", description="Vertical 2nd-order chromaticity", type=double, &end
&parameter name=dnuy/dp3, symbol="$gx$r$by3$n", units="1/(2$gp$r)", description="Vertical 3rd-order chromaticity", type=double, &end
&parameter name=Ay, symbol="A$by$n", units=m, description="Vertical acceptance", type=double, &end
&parameter name=AyLocation, units=m, description="Location of vertical acceptance limit", type=double, &end
&parameter name=deltaHalfRange, symbol="$gDd$r/2", description="Half range of momentum offset for chromatic tune spread evaluation", type=double, &end
&parameter name=nuxChromUpper, symbol="$gx$r$bu$n", description="Upper limit of x tune due to chromaticity and deltaRange", type=double, &end
&parameter name=nuxChromLower, symbol="$gx$r$bu$n", description="Lower limit of x tune due to chromaticity and deltaRange", type=double, &end
&parameter name=nuyChromUpper, symbol="$gy$r$bu$n", description="Upper limit of y tune due to chromaticity and deltaRange", type=double, &end
&parameter name=nuyChromLower, symbol="$gy$r$bu$n", description="Lower limit of y tune due to chromaticity and deltaRange", type=double, &end
&parameter name=Stage, description="Stage of computation", type=string, &end
&parameter name=pCentral, units="m$be$nc", description="Central momentum", type=double, &end
&parameter name=dbetax/dp, units=m, description="Derivative of betax with momentum offset", type=double, &end
&parameter name=dbetay/dp, units=m, description="Derivative of betay with momentum offset", type=double, &end
&parameter name=dalphax/dp, description="Derivative of alphax with momentum offset", type=double, &end
&parameter name=dalphay/dp, description="Derivative of alphay with momentum offset", type=double, &end
&parameter name=etax2, symbol="$gc$r$bx2$n", units=m, description="Second-order dispersion (for matched or periodic case only)", type=double, &end
&parameter name=etay2, symbol="$gc$r$by2$n", units=m, description="Second-order dispersion (for matched or periodic case only)", type=double, &end
&parameter name=etax3, symbol="$gc$r$bx3$n", units=m, description="Third-order dispersion (for matched or periodic case only)", type=double, &end
&parameter name=etay3, symbol="$gc$r$by3$n", units=m, description="Third-order dispersion (for matched or periodic case only)", type=double, &end
&parameter name=etaxp2, symbol="$gc$r$bx2$n$a'$n", units=m, description="Second-order dispersion (for matched or periodic case only)", type=double, &end
&parameter name=etayp2, symbol="$gc$r$by2$n$a'$n", units=m, description="Second-order dispersion (for matched or periodic case only)", type=double, &end
&parameter name=etaxp3, symbol="$gc$r$bx3$n$a'$n", units=m, description="Third-order dispersion (for matched or periodic case only)", type=double, &end
&parameter name=etayp3, symbol="$gc$r$by3$n$a'$n", units=m, description="Third-order dispersion (for matched or periodic case only)", type=double, &end
&parameter name=betaxMin, units=m, description="Minimum betax", type=double, &end
&parameter name=betaxAve, units=m, description="Average betax", type=double, &end
&parameter name=betaxMax, units=m, description="Maximum betax", type=double, &end
&parameter name=betayMin, units=m, description="Minimum betay", type=double, &end
&parameter name=betayAve, units=m, description="Average betay", type=double, &end
&parameter name=betayMax, units=m, description="Maximum betay", type=double, &end
&parameter name=etaxMax, units=m, description="Maximum absolute value of etax", type=double, &end
&parameter name=etayMax, units=m, description="Maximum absolute value of etay", type=double, &end
&parameter name=waistsx, description="Number of changes in the sign of alphax", type=long, &end
&parameter name=waistsy, description="Number of changes in the sign of alphay", type=long, &end
&parameter name=dnux/dAx, units=1/m, description="Horizontal tune shift with horizontal amplitude", type=double, &end
&parameter name=dnux/dAy, units=1/m, description="Horizontal tune shift with vertical amplitude", type=double, &end
&parameter name=dnuy/dAx, units=1/m, description="Vertical tune shift with horizontal amplitude", type=double, &end
&parameter name=dnuy/dAy, units=1/m, description="Vertical tune shift with vertical amplitude", type=double, &end
&parameter name=dnux/dAx2, units="1/m$a2$n", description="Horizontal tune shift with horizontal amplitude", type=double, &end
&parameter name=dnux/dAy2, units="1/m$a2$n", description="Horizontal tune shift with vertical amplitude", type=double, &end
&parameter name=dnux/dAxAy, units="1/m$a2$n", description="Horizontal tune shift with horizontal and vertical amplitude", type=double, &end
&parameter name=dnuy/dAx2, units="1/m$a2$n", description="Vertical tune shift with horizontal amplitude", type=double, &end
&parameter name=dnuy/dAy2, units="1/m$a2$n", description="Vertical tune shift with vertical amplitude", type=double, &end
&parameter name=dnuy/dAxAy, units="1/m$a2$n", description="Vertical tune shift with horizontal and vertical amplitude", type=double, &end
&parameter name=nuxTswaLower, description="Minimum horizontal tune from tune-shift-with-amplitude calculations", type=double, &end
&parameter name=nuxTswaUpper, description="Maximum horizontal tune from tune-shift-with-amplitude calculations", type=double, &end
&parameter name=nuyTswaLower, description="Minimum vertical tune from tune-shift-with-amplitude calculations", type=double, &end
&parameter name=nuyTswaUpper, description="Maximum vertical tune from tune-shift-with-amplitude calculations", type=double, &end
&parameter name=couplingIntegral, description="Coupling integral for difference resonance", type=double, &end
&parameter name=couplingDelta, description="Distance from difference resonance", type=double, &end
&parameter name=emittanceRatio, description="Emittance ratio from coupling integral", type=double, &end
&parameter name=alphac2, symbol="$ga$r$bc2$n", description="2nd-order momentum compaction factor", type=double, &end
&parameter name=alphac, symbol="$ga$r$bc$n", description="Momentum compaction factor", type=double, &end
&parameter name=I1, units=m, description="Radiation integral 1", type=double, &end
&parameter name=I2, units=1/m, description="Radiation integral 2", type=double, &end
&parameter name=I3, units="1/m$a2$n", description="Radiation integral 3", type=double, &end
&parameter name=I4, units=1/m, description="Radiation integral 4", type=double, &end
&parameter name=I5, units=1/m, description="Radiation integral 5", type=double, &end
&parameter name=ex0, units="$gp$rm", description="Damped horizontal emittance", type=double, &end
&parameter name=enx0, units="m$be$nc $gp$rm", description="Damped normalized horizontal emittance", type=double, &end
&parameter name=taux, units=s, description="Horizontal damping time", type=double, &end
&parameter name=Jx, description="Horizontal damping partition number", type=double, &end
&parameter name=tauy, units=s, description="Vertical damping time", type=double, &end
&parameter name=Jy, description="Vertical damping partition number", type=double, &end
&parameter name=Sdelta0, description="RMS fractional energy spread", type=double, &end
&parameter name=taudelta, units=s, description="Longitudinal damping time", type=double, &end
&parameter name=Jdelta, description="Longitudinal damping partition number", type=double, &end
&parameter name=U0, units=MeV, description="Energy loss per turn", type=double, &end
&column name=s, units=m, description=Distance, type=double,  &end
&column name=betax, symbol="$gb$r$bx$n", units=m, description="Horizontal beta-function", type=double,  &end
&column name=alphax, symbol="$ga$r$bx$n", description="Horizontal alpha-function", type=double,  &end
&column name=psix, symbol="$gy$r$bx$n", units=rad, description="Horizontal phase advance", type=double,  &end
&column name=etax, symbol="$gc$r$bx$n", units=m, description="Horizontal dispersion", type=double,  &end
&column name=etaxp, symbol="$gc$r$bx$n$a'$n", description="Slope of horizontal dispersion", type=double,  &end
&column name=xAperture, symbol="a$bx,eff$n", units=m, description="Effective horizontal aperture", type=double,  &end
&column name=betay, symbol="$gb$r$by$n", units=m, description="Vertical beta-function", type=double,  &end
&column name=alphay, symbol="$ga$r$by$n", description="Vertical alpha-function", type=double,  &end
&column name=psiy, symbol="$gy$r$by$n", units=rad, description="Vertical phase advance", type=double,  &end
&column name=etay, symbol="$gc$r$by$n", units=m, description="Vertical dispersion", type=double,  &end
&column name=etayp, symbol="$gc$r$by$n$a'$n", description="Slope of vertical dispersion", type=double,  &end
&column name=yAperture, symbol="a$by,eff$n", units=m, description="Effective vertical aperture", type=double,  &end
&column name=pCentral0, symbol="p$bcent$n", units="m$be$nc", description="Initial central momentum", type=double,  &end
&column name=ElementName, description="Element name", format_string=%10s, type=string,  &end
&column name=ElementOccurence, description="Occurence of element", format_string=%6ld, type=long,  &end
&column name=ElementType, description="Element-type name", format_string=%10s, type=string,  &end
&column name=ChamberShape, type=string,  &end
&column name=dI1, units=m, description="Contribution to radiation integral 1", type=double,  &end
&column name=dI2, units=1/m, description="Contribution to radiation integral 2", type=double,  &end
&column name=dI3, units="1/m$a2$n", description="Contribution to radiation integral 3", type=double,  &end
&column name=dI4, units=1/m, description="Contribution to radiation integral 4", type=double,  &end
&column name=dI5, units=1/m, description="Contribution to radiation integral 5", type=double,  &end
&data mode=binary, &end
/       �>:���?5�)�@ٿ                        ��������D;�\�?Co+���                        ��������        �>:���?�>:���?D;�\�?D;�\�?   tunes uncorrectedB����@�ɨ�d&@���<Vg@��ף���ڭx�"�                                                                ��s�*�?�+U!@k\�l!@��a�)	@�C�y�� @ ���&�8@                                                                                                                                              �>aR�?                ��?�	 6���f�+k��*�o6h?u"f���?        t=���t�=¼[�nb*=3�Ź�T>�"J��e�?      �?�"J��e�?      �?�M��q�L?�"J��es?       @Ye|�?        ۯ�y�	@��d%��ӿ                              $@����*'$@����ۿ                              $@B����@   _BEG_      MARK                                                    ۯ�y�	@��d%��ӿ                              $@����*'$@����ۿ                              $@B����@   BNCH      CHARGE   ?                                        윙����?�R����
@@���ؿ�F�)6�?                      $@��4˂$@�:��cݿԦ[�%�?                      $@B����@	   HXR_DR_V1      DRIF   ?                                        �䥛� �? �"�k9@���s�-ٿ$��� }�?                      $@Gjz�h�$@σ�B�ݿZ"��S�?                      $@B����@   HXR_DR_QD_ED      DRIF   ?                                        �*\����?	i5�-@p;���N�?J(��^w�?                      $@1��fB�$@](�:���c�+�?                      $@B����@
   HXR_QD_V01      QUAD   ?                                        �*\����?	i5�-@p;���N�?J(��^w�?                      $@1��fB�$@](�:���c�+�?                      $@B����@   HXR_COR      KICKER   ?                                        �*\����?	i5�-@p;���N�?J(��^w�?                      $@1��fB�$@](�:���c�+�?                      $@B����@   HXR_BPM      MONI   ?                                        Ap����?�O`v#�
@�ĥ�- @n�N�Qs�?                      $@@!sI�
%@��J
o����+e��?                      $@B����@
   HXR_QD_V01      QUAD   ?                                        Ȇ�Q��?�Q�4�;	@�.ml�?���%��?                      $@q]�r��&@ȰAՑ �!A�`�a�?                      $@B����@   HXR_DR_QD_ED      DRIF   ?                                        �D<j�?*!8�;�?��Cmm��?zXP9Ť�?                      $@�(5��A7@�u�#�'� [<Rx�?                      $@B����@	   HXR_DR_V2      DRIF   ?                                        Lw~+8�?<ۂ>��?�Q��?p���"�?                      $@0��砂8@��T�<m(�6m��?                      $@B����@   HXR_DR_QD_ED      DRIF   ?                                        �Y%�~�?QU��@�?�-��n�?`U�k���?                      $@ ���&�8@!�B�#��3I�)�?                      $@B����@
   HXR_QD_V02      QUAD   ?                                        �Y%�~�?QU��@�?�-��n�?`U�k���?                      $@ ���&�8@!�B�#��3I�)�?                      $@B����@   HXR_COR      KICKER   ?                                        �Y%�~�?QU��@�?�-��n�?`U�k���?                      $@ ���&�8@!�B�#��3I�)�?                      $@B����@   HXR_BPM      MONI   ?                                        �46��?��s�*�?�s�x�(�����r�?                      $@��X\��8@�.�Z��@��>^&L�?                      $@B����@
   HXR_QD_V02      QUAD   ?                                        �P���?�D��E�?s��30���.�(��?                      $@!;��0�7@�V�{3�@��L�{ز?                      $@B����@   HXR_DR_QD_ED      DRIF   ?                                        j�2Ռ@'>(@��E�ￜ'�	���?                      $@$�`��%@yx�D@��ϾA��?                      $@B����@	   HXR_DR_V3      DRIF   ?                                        ;��+T�@�a�>[�@��:T��F�m�ZD�?                      $@-�B��$@�<���@���$)�?                      $@B����@   HXR_DR_QD_ED      DRIF   ?                                        �c��@Ò9��
@�~����濼9�X�?                      $@�ל6�$@�ykO� 	@�x)�yR�?                      $@B����@
   HXR_QD_V03      QUAD   ?                                        �c��@Ò9��
@�~����濼9�X�?                      $@�ל6�$@�ykO� 	@�x)�yR�?                      $@B����@   HXR_COR      KICKER   ?                                        �c��@Ò9��
@�~����濼9�X�?                      $@�ל6�$@�ykO� 	@�x)�yR�?                      $@B����@   HXR_BPM      MONI   ?                                        ��ڨ�,@C]��'@w�̣�Cؿ��D�l�?                      $@�ܯad$@=+�wR��?�=��"|�?                      $@B����@
   HXR_QD_V03      QUAD   ?                                        t����@i���{@5r��ٿ��Y ͼ�?                      $@�/��$@�����?)���$�?                      $@B����@   HXR_DR_QD_ED      DRIF   ?                                        V�ŧ�U@^gwOd @,������K��?                      $@5]�C�
@�!o�C/�?k��w��?                      $@B����@	   HXR_DR_V4      DRIF   ?                                        �;,��@�]��	!@²*"27���(R��?                      $@����|�	@������?|��e���?                      $@B����@	   HXR_DR_CT      DRIF   ?                                        �;,��@�]��	!@²*"27���(R��?                      $@����|�	@������?|��e���?                      $@B����@   HXR_WA_M_OU      WATCH   ?                                        $��5�@ݱ��a!@�ԭP�����C*��?                      $@�d7	@�o/�4%�?+HH�y�?                      $@B����@   HXR_DR_QD_ED      DRIF   ?                                        ~1Uj��@k\�l!@�9���?�?�;=��?                      $@��a�)	@֛���:��CJ����?                      $@B����@	   HXR_QD_FH      QUAD   ?                                        ~1Uj��@k\�l!@�9���?�?�;=��?                      $@��a�)	@֛���:��CJ����?                      $@B����@   HXR_COR      KICKER   ?                                        ~1Uj��@k\�l!@�9���?�?�;=��?                      $@��a�)	@֛���:��CJ����?                      $@B����@   HXR_BPM      MONI   ?                                        ؕ�I�@$<��`!@�n�B��?y<�(#�?                      $@O��<	@����p����^��?                      $@B����@	   HXR_QD_FH      QUAD   ?                                        @'~�A@X۟r!@R?�M2D�?�k0��;�?                      $@��.��	@P�r4濧���B�?                      $@B����@   HXR_DR_QD_ED      DRIF   ?                                        ����@�N/t@`&����?�{��m��?                      $@T6��@�д�"鿯D��?                      $@B����@   HXR_DR_WIG_ED      DRIF   ?                                        gX�r1@,)�ڳ@�L���?�J��@                      $@�E�._�@62qcV[��I!o�N�?                      $@B����@	   HXR_WIG_0      WIGGLER   ?��f�+[��*�o6X?u"f���
?        jB�>�W|=�9���@b:�8�U
@72-���?5�aV�y@                      $@c � @��ab����I�|�}�?                      $@B����@   HXR_DR_WIG_ED      DRIF   ?                                        ��@�kW%�	@���2O�?L#���@                      $@ӌVa�!@x*8v(���,z�;��?                      $@B����@   HXR_DR_QD_ED	      DRIF   ?                                        q/*�n%@'��+�	@q�\·H�?�����@                      $@</9Q!@�t�S���'��y��?                      $@B����@	   HXR_QD_DH      QUAD   ?                                        q/*�n%@'��+�	@q�\·H�?�����@                      $@</9Q!@�t�S���'��y��?                      $@B����@   HXR_COR      KICKER   ?                                        q/*�n%@'��+�	@q�\·H�?�����@                      $@</9Q!@�t�S���'��y��?                      $@B����@   HXR_BPM      MONI   ?                                        ˓e¾2@8Ù�˹	@�i�cSI�P��P<�@                      $@lDn�!@Q["����?e�}ͷ��?                      $@B����@	   HXR_QD_DH      QUAD   ?                                        3%S?�g@<^���5
@e*(����%3 �@                      $@7;�� @���%���?i�����?                      $@B����@   HXR_DR_QD_ED
      DRIF   ?                                        {�S�/@
�H�{/@9��p����_F�A@                      $@́|��@��Oy�J�?F<��U �?                      $@B����@   HXR_DR_WIG_ED      DRIF   ?                                        -��u!#@��I?�@d&��cq��v�rX� @                      $@��\&��@������?i��$���?                      $@B����@	   HXR_WIG_0      WIGGLER   ?��f�+[��*�o6X?u"f���
?        8B�z=���#@�Rp?|@No�!���O��V@                      $@�c��K@;Q��?B��@�d�?                      $@B����@   HXR_DR_WIG_ED      DRIF   ?                                        ��a>��#@ȶl��@�OmK���{�U�c@                      $@:�i6E�
@ 4E.�?��m]ԣ�?                      $@B����@   HXR_DR_QD_ED      DRIF   ?                                        ���-,�#@v���@�nF$7c�?�Z�f@                      $@�*�z�
@�!M.)|?XzCWʳ�?                      $@B����@	   HXR_QD_FH      QUAD   ?                                        ���-,�#@v���@�nF$7c�?�Z�f@                      $@�*�z�
@�!M.)|?XzCWʳ�?                      $@B����@	   HXR_WA_OU      WATCH   ?                                        