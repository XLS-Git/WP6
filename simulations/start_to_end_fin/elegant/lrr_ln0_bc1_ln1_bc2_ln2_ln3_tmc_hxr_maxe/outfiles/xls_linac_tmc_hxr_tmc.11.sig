SDDS1
!# little-endian
&description text="sigma matrix--input: xls_linac_tmc_hxr_tmc.track.ele  lattice: xls_linac_tmc_hxr.11.lte", contents="sigma matrix", &end
&parameter name=Step, description="Simulation step", type=long, &end
&parameter name=SVNVersion, description="SVN version number", type=string, fixed_value=unknown, &end
&column name=s, units=m, description=Distance, type=double,  &end
&column name=ElementName, description="Element name", format_string=%10s, type=string,  &end
&column name=ElementOccurence, description="Occurence of element", format_string=%6ld, type=long,  &end
&column name=ElementType, description="Element-type name", format_string=%10s, type=string,  &end
&column name=s1, symbol="$gs$r$b1$n", units=m, description="sqrt(<x*x>)", type=double,  &end
&column name=s12, symbol="$gs$r$b12$n", units=m, description="<x*xp'>", type=double,  &end
&column name=s13, symbol="$gs$r$b13$n", units="m$a2$n", description="<x*y>", type=double,  &end
&column name=s14, symbol="$gs$r$b14$n", units=m, description="<x*y'>", type=double,  &end
&column name=s15, symbol="$gs$r$b15$n", units="m$a2$n", description="<x*s>", type=double,  &end
&column name=s16, symbol="$gs$r$b16$n", units=m, description="<x*delta>", type=double,  &end
&column name=s17, symbol="$gs$r$b17$n", units="m*s", description="<x*t>", type=double,  &end
&column name=s2, symbol="$gs$r$b2$n", description="sqrt(<x'*x'>)", type=double,  &end
&column name=s23, symbol="$gs$r$b23$n", units=m, description="<x'*y>", type=double,  &end
&column name=s24, symbol="$gs$r$b24$n", description="<x'*y'>", type=double,  &end
&column name=s25, symbol="$gs$r$b25$n", units=m, description="<x'*s>", type=double,  &end
&column name=s26, symbol="$gs$r$b26$n", description="<x'*delta>", type=double,  &end
&column name=s27, symbol="$gs$r$b27$n", units=s, description="<x'*t>", type=double,  &end
&column name=s3, symbol="$gs$r$b3$n", units=m, description="sqrt(<y*y>)", type=double,  &end
&column name=s34, symbol="$gs$r$b34$n", units=m, description="<y*y'>", type=double,  &end
&column name=s35, symbol="$gs$r$b35$n", units="m$a2$n", description="<y*s>", type=double,  &end
&column name=s36, symbol="$gs$r$b36$n", units=m, description="<y*delta>", type=double,  &end
&column name=s37, symbol="$gs$r$b37$n", units="m*s", description="<y*t>", type=double,  &end
&column name=s4, symbol="$gs$r$b4$n", description="sqrt(<y'*y')>", type=double,  &end
&column name=s45, symbol="$gs$r$b45$n", units=m, description="<y'*s>", type=double,  &end
&column name=s46, symbol="$gs$r$b46$n", description="<y'*delta>", type=double,  &end
&column name=s47, symbol="$gs$r$b47$n", units=s, description="<y'*t>", type=double,  &end
&column name=s5, symbol="$gs$r$b5$n", units=m, description="sqrt(<s*s>)", type=double,  &end
&column name=s56, symbol="$gs$r$b56$n", units=m, description="<s*delta>", type=double,  &end
&column name=s57, symbol="$gs$r$b57$n", units="m*s", description="<s*t>", type=double,  &end
&column name=s6, symbol="$gs$r$b6$n", description="sqrt(<delta*delta>)", type=double,  &end
&column name=s67, symbol="$gs$r$b67$n", units=s, description="<delta*t>", type=double,  &end
&column name=s7, symbol="$gs$r$b7$n", description="sqrt(<t*t>)", type=double,  &end
&column name=ma1, symbol="max$sb$ex$sb$e", units=m, description="maximum absolute value of x", type=double,  &end
&column name=ma2, symbol="max$sb$ex'$sb$e", description="maximum absolute value of x'", type=double,  &end
&column name=ma3, symbol="max$sb$ey$sb$e", units=m, description="maximum absolute value of y", type=double,  &end
&column name=ma4, symbol="max$sb$ey'$sb$e", description="maximum absolute value of y'", type=double,  &end
&column name=ma5, symbol="max$sb$e$gD$rs$sb$e", units=m, description="maximum absolute deviation of s", type=double,  &end
&column name=ma6, symbol="max$sb$e$gd$r$sb$e", description="maximum absolute value of delta", type=double,  &end
&column name=ma7, symbol="max$sb$e$gD$rt$sb$e", units=s, description="maximum absolute deviation of t", type=double,  &end
&column name=minimum1, symbol="x$bmin$n", units=m, type=double,  &end
&column name=minimum2, symbol="x'$bmin$n", units=m, type=double,  &end
&column name=minimum3, symbol="y$bmin$n", units=m, type=double,  &end
&column name=minimum4, symbol="y'$bmin$n", units=m, type=double,  &end
&column name=minimum5, symbol="$gD$rs$bmin$n", units=m, type=double,  &end
&column name=minimum6, symbol="$gd$r$bmin$n", units=m, type=double,  &end
&column name=minimum7, symbol="t$bmin$n", units=s, type=double,  &end
&column name=maximum1, symbol="x$bmax$n", units=m, type=double,  &end
&column name=maximum2, symbol="x'$bmax$n", units=m, type=double,  &end
&column name=maximum3, symbol="y$bmax$n", units=m, type=double,  &end
&column name=maximum4, symbol="y'$bmax$n", units=m, type=double,  &end
&column name=maximum5, symbol="$gD$rs$bmax$n", units=m, type=double,  &end
&column name=maximum6, symbol="$gd$r$bmax$n", units=m, type=double,  &end
&column name=maximum7, symbol="t$bmax$n", units=s, type=double,  &end
&column name=Sx, symbol="$gs$r$bx$n", units=m, description=sqrt(<(x-<x>)^2>), type=double,  &end
&column name=Sxp, symbol="$gs$r$bx'$n", description=sqrt(<(x'-<x'>)^2>), type=double,  &end
&column name=Sy, symbol="$gs$r$by$n", units=m, description=sqrt(<(y-<y>)^2>), type=double,  &end
&column name=Syp, symbol="$gs$r$by'$n", description=sqrt(<(y'-<y'>)^2>), type=double,  &end
&column name=Ss, symbol="$gs$r$bs$n", units=m, description=sqrt(<(s-<s>)^2>), type=double,  &end
&column name=Sdelta, symbol="$gs$bd$n$r", description=sqrt(<(delta-<delta>)^2>), type=double,  &end
&column name=St, symbol="$gs$r$bt$n", units=s, description=sqrt(<(t-<t>)^2>), type=double,  &end
&column name=ex, symbol="$ge$r$bx$n", units=m, description="geometric horizontal emittance", type=double,  &end
&column name=enx, symbol="$ge$r$bx,n$n", units=m, description="normalized horizontal emittance", type=double,  &end
&column name=ecx, symbol="$ge$r$bx,c$n", units=m, description="geometric horizontal emittance less dispersive contributions", type=double,  &end
&column name=ecnx, symbol="$ge$r$bx,cn$n", units=m, description="normalized horizontal emittance less dispersive contributions", type=double,  &end
&column name=ey, symbol="$ge$r$by$n", units=m, description="geometric vertical emittance", type=double,  &end
&column name=eny, symbol="$ge$r$by,n$n", units=m, description="normalized vertical emittance", type=double,  &end
&column name=ecy, symbol="$ge$r$by,c$n", units=m, description="geometric vertical emittance less dispersive contributions", type=double,  &end
&column name=ecny, symbol="$ge$r$by,cn$n", units=m, description="normalized vertical emittance less dispersive contributions", type=double,  &end
&column name=betaxBeam, symbol="$gb$r$bx,beam$n", units=m, description="betax for the beam, excluding dispersive contributions", type=double,  &end
&column name=alphaxBeam, symbol="$ga$r$bx,beam$n", description="alphax for the beam, excluding dispersive contributions", type=double,  &end
&column name=betayBeam, symbol="$gb$r$by,beam$n", units=m, description="betay for the beam, excluding dispersive contributions", type=double,  &end
&column name=alphayBeam, symbol="$ga$r$by,beam$n", description="alphay for the beam, excluding dispersive contributions", type=double,  &end
&data mode=binary, &end
&                 _BEG_      MARK �2�6d�>�#�1��=X惱�鋽�7堃輍澖忺譎=躽e岍3�=>F 闆壼;IX�孷�>|ZSA��秸*Pu�*娊]磄!�=I7�
�=(蹀塜┘;闷�5 溕>絭櫻�=鬘籥雕<素�絽�?倬/;#�|襪�>M�耤 =]�)舧=�!2Y];囑洯;.�>-/宧德�=钫�74-�;%Y飘濆3?掄袩"<�kp�<>8濒3.+?_!虍a?�<-湘褒>S熒�?
?   鲵�>镽颯	l?   ▄�=>8濒3.+縚!虍a縓锛C滫�*(P��   h奚緀祀d:f�   �*街cdK`'?压鱘無?�<-湘褒>S熒�?
?   鲵�>镽颯	l?   ▄�= �2�6d�>IX�孷�>闷�5 溕>#�|襪�>囑洯;.�>%Y飘濆3?�kp�<涆封媉�=�恼d�>�?嗋CN�=櫽�#jM�>鈑朄�= h�.燹�>2远廆�=譥(迌>H鎳g4@e舀莐 雷憍	?%緘d鐊慰           BNCH      CHARGE �2�6d�>�#�1��=X惱�鋽�7堃輍澖忺譎=躽e岍3�=>F 闆壼;IX�孷�>|ZSA��秸*Pu�*娊]磄!�=I7�
�=(蹀塜┘;闷�5 溕>絭櫻�=鬘籥雕<素�絽�?倬/;#�|襪�>M�耤 =]�)舧=�!2Y];囑洯;.�>-/宧德�=钫�74-�;%Y飘濆3?掄袩"<�kp�<>8濒3.+?_!虍a?�<-湘褒>S熒�?
?   鲵�>镽颯	l?   ▄�=>8濒3.+縚!虍a縓锛C滫�*(P��   h奚緀祀d:f�   �*街cdK`'?压鱘無?�<-湘褒>S熒�?
?   鲵�>镽颯	l?   ▄�= �2�6d�>IX�孷�>闷�5 溕>#�|襪�>囑洯;.�>%Y飘濆3?�kp�<涆封媉�=�恼d�>�?嗋CN�=櫽�#jM�>鈑朄�= h�.燹�>2远廆�=譥(迌>H鎳g4@e舀莐 雷憍	?%緘d鐊慰C\?	   TMC_DR_V1      DRIF灇B邬�>;惙武�=�+V鞙�fGY牻<>�=��6� >B;IX�孷�>g刞2V吔�*Pu�*娊*��=I7�
�=H�b┘;房鑕	q�>7櫎<H=Q'*�=�27g顋結酗U矯;#�|襪�>�6l蔯 =]�)舧=-�9�Y];rｓ>.�>h造渚落=J.涏8-�;%Y飘濆3?詵镌珢"<<洉譵p�<V+診 c-?_!虍a?��,5�>S熒�?
?   �?�>镽颯	l?   @领=V+診 c-縚!虍a慷犮誸辂�*(P��   奚緀祀d:f�   �*絑蝪)酭)?压鱘無?��,5�>S熒�?
?   �?�>镽颯	l?   @领=灇B邬�>IX�孷�>房鑕	q�>#�|襪�>rｓ>.�>%Y飘濆3?<洉譵p�<熔封媉�=2恼d�> @嗋CN�=杂�#jM�>醟朄�=�g�.燹�> 2远廆�=誣(迌>霈}�7@�V庑�!繩hL�<疥?澣ǜ熱�N3��?   TMC_DR_QD_ED      DRIF0�=鞫{�>[秐K�=萚~聻匠�%=K牻U.";枱=扝飡�>~�"9G�;IX�孷�>S�0錼喗�*Pu�*娊諃��=I7�
�=帷�d┘;"螰�%F�>�洰┆�=�,曮p�=猾价n雥诫靓&�1F;#�|襪�>��1蘡 =]�)舧=刾s "Y];�/]�?.�>xц缋落=�2欇9-�;%Y飘濆3?b慒\瓙"<賥磎np�<�1DG期-?_!虍a?M箨/�>S熒�?
?   鶲�>镽颯	l?    徐=�1DG期-縚!虍a狂W'4偟*(P��   奚緀祀d:f�   �*絣Of寽�)?压鱘無?M箨/�>S熒�?
?   鶲�>镽颯	l?    徐=0�=鞫{�>IX�孷�>"螰�%F�>#�|襪�>�/]�?.�>%Y飘濆3?賥磎np�<涆封媉�=�恼d�>�?嗋CN�=櫽�#jM�>醟朄�=�g�.燹�> 2远廆�=誣(迌>橖HS�8@-蜥"繮辯蠸�?��镁憧浢溌9�?
   TMC_QD_V01      QUAD添h�击>\�L�=�=眱頞"D牻V�懊＝�柶�=渑鎈;>b3劔r�;渑A堛煳>[觟yt铰�?E痗}接交犉}W=J潈鮟朴=]oI鯥�;鞖敯�=�>伷=執l�=V賮=;?螰{胶` ;"f(顤@�>`P7�=�!=O#�r=魝�$耞;\lM�?.�>}″炅落=q�C:-�;%Y飘濆3?範g異"<'騲p�<焥熔�+.?j8垀6?r発F0旪>垟6;o'?   vX�>镽颯	l?   谱�=焥熔�+.縥8垀6縭発F0旪緙� 潆�   �
奚緀祀d:f�   t*捷�*?W毆 x?熄zH轎�>垟6;o'?   vX�>镽颯	l?   谱�=添h�击>渑A堛煳>鞖敯�=�>"f(顤@�>\lM�?.�>%Y飘濆3?'騲p�<‘睢躝�=桼蹊嘾�>5锫藠N�=篗荕�>ㄣoDˊ�=籣��>$H蕖@�=I筠�>�Uap9@⺳�$d��劸扪:�?e癖擉肟浢溌9�?   TMC_COR      KICKER添h�击>\�L�=�=眱頞"D牻V�懊＝�柶�=渑鎈;>b3劔r�;渑A堛煳>[觟yt铰�?E痗}接交犉}W=J潈鮟朴=]oI鯥�;鞖敯�=�>伷=執l�=V賮=;?螰{胶` ;"f(顤@�>`P7�=�!=O#�r=魝�$耞;\lM�?.�>}″炅落=q�C:-�;%Y飘濆3?範g異"<'騲p�<焥熔�+.?j8垀6?r発F0旪>垟6;o'?   vX�>镽颯	l?   谱�=焥熔�+.縥8垀6縭発F0旪緙� 潆�   �
奚緀祀d:f�   t*捷�*?W毆 x?熄zH轎�>垟6;o'?   vX�>镽颯	l?   谱�=添h�击>渑A堛煳>鞖敯�=�>"f(顤@�>\lM�?.�>%Y飘濆3?'騲p�<‘睢躝�=桼蹊嘾�>5锫藠N�=篗荕�>ㄣoDˊ�=籣��>$H蕖@�=I筠�>�Uap9@⺳�$d��劸扪:�?e癖擉肟浢溌9�?   TMC_BPM      MONI添h�击>\�L�=�=眱頞"D牻V�懊＝�柶�=渑鎈;>b3劔r�;渑A堛煳>[觟yt铰�?E痗}接交犉}W=J潈鮟朴=]oI鯥�;鞖敯�=�>伷=執l�=V賮=;?螰{胶` ;"f(顤@�>`P7�=�!=O#�r=魝�$耞;\lM�?.�>}″炅落=q�C:-�;%Y飘濆3?範g異"<'騲p�<焥熔�+.?j8垀6?r発F0旪>垟6;o'?   vX�>镽颯	l?   谱�=焥熔�+.縥8垀6縭発F0旪緙� 潆�   �
奚緀祀d:f�   t*捷�*?W毆 x?熄zH轎�>垟6;o'?   vX�>镽颯	l?   谱�=添h�击>渑A堛煳>鞖敯�=�>"f(顤@�>\lM�?.�>%Y飘濆3?'騲p�<‘睢躝�=桼蹊嘾�>5锫藠N�=篗荕�>ㄣoDˊ�=籣��>$H蕖@�=I筠�>�Uap9@⺳�$d��劸扪:�?e癖擉肟*啋霑?
   TMC_QD_V01      QUADIIc洒>榃賟+W唤濵崓�)〗Y蘅覨ЫZ刺�礋=�鶤c)B>誚a%b�;/邽h�>帬驁:Z=S峁搘R=K餣织Qt�&*i簺e两W�)[W1不窶�1兿>恸繜[嚤=筷Y鵚v=\JkQ噚z舰皴"UHK;J_L�鱾�>嗗.d&:#=梊�&�i=~z�7a;�)呷?.�>挥a懔落=涎�:-�;%Y飘濆3?韂艄瓙"<汖np�<盶j< =.?罏R偬�>鬔Xq炳>�	KK?   @Y�>镽颯	l?   ㄘ�=盶j< =.坷�14筆嗑鬔Xq炳�$�=瞭��   D
奚緀祀d:f�   �*�灖╉*?罏R偬�>�%y弥↓>�	KK?   @Y�>镽颯	l?   ㄘ�=IIc洒>/邽h�>窶�1兿>J_L�鱾�>�)呷?.�>%Y飘濆3?汖np�<肯儳8`�=6渌 e�>z�/曹N�=F徎�2N�>X尲Ф@�=�*�邇>vB癅�=.尸Z�>h翠NU89@�6 3y�?qi�?!鱙鄩婒库�诅b�?   TMC_DR_QD_ED      DRIF鵷郶慊�>u�)?唤4r@V�⒔-u廮孈Ы.鉯q衸�=a颰6>∮椽�0�;/邽h�> 嗠鹴[=S峁搘R=8L 挴Qt�&*i簺e两廘�W1不粀u倖�>凅x%B��=_K捿= }y谨BC顠5N;J_L�鱾�>盩oY*:#=梊�&�i=傞惟"7a;碱,�?.�>塋_傲落=z頉�9-�;%Y飘濆3?\帄F瓙"<q.h=np�<1lz@N-.?罏R偬�>崋|篎 ?�	KK?   <Y�>镽颯	l?   载�=1lz@N-.坷�14筆嗑崋|篎 �$�=瞭��   d	奚緀祀d:f�   8*絾丌}�
*?罏R偬�>t藈 �>�	KK?   <Y�>镽颯	l?   载�=鵷郶慊�>/邽h�>粀u倖�>J_L�鱾�>碱,�?.�>%Y飘濆3?q.h=np�<孪儳8`�=6渌 e�>}�/曹N�=J徎�2N�>Y尲Ф@�=�*�邇>芭vB癅�=.尸Z�>15C�9@躡B�h�?�匪�?~[蝏&艨廎媅疒	@	   TMC_DR_V2      DRIF嶢訲鼮�>VZM屉唇a

f媒y溹櫏今韾=覊ek�0�=謍1颩�;/邽h�>暵�6咙s=S峁搘R=砫憹Qt�&*i簺e两懦{-F1不/菛>�>7S{t$傒=�9Y樧�?=蜇勍�d=�
lo藅|;J_L�鱾�>�姜4;#=梊�&�i=縌��8a;q[w�/.�>竜�:绰�=-zH -�;%Y飘濆3?�'詭�"<{�(慮p�<晔�
*?罏R偬�>龑L�#�(?�	KK?   ,X�>镽颯	l?   V潇=晔�
*坷�14筆嗑�k?¤#�$�=瞭��   4陷删e祀d:f�   8�)�?朏菍'?罏R偬�>龑L�#�(?�	KK?   ,X�>镽颯	l?   V潇=嶢訲鼮�>/邽h�>/菛>�>J_L�鱾�>q[w�/.�>%Y飘濆3?{�(慮p�<料儳8`�=6渌 e�>{�/曹N�=H徎�2N�>v尲Ф@�=�*�邇>团vB癅�=A尸Z�>�&髙圾2@Yワ紊�?JrA魵;@V�箁6纅犒橔1
@   TMC_DR_QD_ED      DRIF慉6轷擊>4*Ｉ哟絀'鸬毭竭镸�葤ソjX睬系�=骎 �9�=w汑貊晖;/邽h�>�;9�.t=S峁搘R=熇%M漄t�&*i簺e两r
$鞥1不Tq1~�>_戦噍=慶
V>@=Mm3v�#e=�	2o}襹;J_L�鱾�>"匌�8;#=梊�&�i=<�38a;釟蒕/.�>囪�绰�=?gA�-�;%Y飘濆3?啿瀈帎"<苐釷]p�<}��)?罏R偬�>戼泻�)?�	KK?   &X�>镽颯	l?   勪�=}��)坷�14筆嗑�&5�+$�$�=瞭��   X屋删e祀d:f�   t�)酱蠥泃'?罏R偬�>戼泻�)?�	KK?   &X�>镽颯	l?   勪�=慉6轷擊>/邽h�>Tq1~�>J_L�鱾�>釟蒕/.�>%Y飘濆3?苐釷]p�<老儳8`�=6渌 e�>{�/曹N�=H徎�2N�>槍姬禓�=�*�邇>锱vB癅�=X尸Z�>謏窋呬2@^Z)迧ы?�'^u�;@o涄贆栏�鎯
@
   TMC_QD_V02      QUAD練�>臭>衻鈠鋜�=M篛壦媒b.�&��=棒x�=dk=�;�=E堏�)释;]�ay剀>昉t枤X廿�5﹝=殗稆W=蠋我f忉=噏夗gs�;嘴�]愻>�>?兦�< ㄙ(@=yn~鹐=\Q�9饇;�3榕X8�><�p�	綘yN鴃=f糗�{F谎|c/.�>x釺绰�=2L�-�;%Y飘濆3?鶖蒫帎"<肶SY]p�<庡� *?倩鎯�?〆\5)?^M癭或>   j[�>镽颯	l?   滅�=庡� *�倩鎯�縌唭ηA$縙M癭或�   H屋删e祀d:f�   t�)疆�v�7'?妲h趯C?〆\5)?��V�神>   j[�>镽颯	l?   滅�=練�>臭>]�ay剀>嘴�]愻>�3榕X8�>�|c/.�>%Y飘濆3?肶SY]p�<�6k&ー�=t3敂e�>�)bO�=?N�9釴�>庁�%
L�=IW�r鎯>�8騷L�=陁柆m鎯>�愊�3@d黰傝X镭~�U�;@Iプ浾@涪鎯
@   TMC_COR      KICKER練�>臭>衻鈠鋜�=M篛壦媒b.�&��=棒x�=dk=�;�=E堏�)释;]�ay剀>昉t枤X廿�5﹝=殗稆W=蠋我f忉=噏夗gs�;嘴�]愻>�>?兦�< ㄙ(@=yn~鹐=\Q�9饇;�3榕X8�><�p�	綘yN鴃=f糗�{F谎|c/.�>x釺绰�=2L�-�;%Y飘濆3?鶖蒫帎"<肶SY]p�<庡� *?倩鎯�?〆\5)?^M癭或>   j[�>镽颯	l?   滅�=庡� *�倩鎯�縌唭ηA$縙M癭或�   H屋删e祀d:f�   t�)疆�v�7'?妲h趯C?〆\5)?��V�神>   j[�>镽颯	l?   滅�=練�>臭>]�ay剀>嘴�]愻>�3榕X8�>�|c/.�>%Y飘濆3?肶SY]p�<�6k&ー�=t3敂e�>�)bO�=?N�9釴�>庁�%
L�=IW�r鎯>�8騷L�=陁柆m鎯>�愊�3@d黰傝X镭~�U�;@Iプ浾@涪鎯
@   TMC_BPM      MONI練�>臭>衻鈠鋜�=M篛壦媒b.�&��=棒x�=dk=�;�=E堏�)释;]�ay剀>昉t枤X廿�5﹝=殗稆W=蠋我f忉=噏夗gs�;嘴�]愻>�>?兦�< ㄙ(@=yn~鹐=\Q�9饇;�3榕X8�><�p�	綘yN鴃=f糗�{F谎|c/.�>x釺绰�=2L�-�;%Y飘濆3?鶖蒫帎"<肶SY]p�<庡� *?倩鎯�?〆\5)?^M癭或>   j[�>镽颯	l?   滅�=庡� *�倩鎯�縌唭ηA$縙M癭或�   H屋删e祀d:f�   t�)疆�v�7'?妲h趯C?〆\5)?��V�神>   j[�>镽颯	l?   滅�=練�>臭>]�ay剀>嘴�]愻>�3榕X8�>�|c/.�>%Y飘濆3?肶SY]p�<�6k&ー�=t3敂e�>�)bO�=?N�9釴�>庁�%
L�=IW�r鎯>�8騷L�=陁柆m鎯>�愊�3@d黰傝X镭~�U�;@Iプ浾@
[9Ｑ�
@
   TMC_QD_V02      QUAD吳
歖&�>~�V�=5B絤^蝗�=鉪雳栐�=01�悬=v頊�"�;�*灐壸�>貓湺浇驨\闵十=2營Er3�=嘋凐W3�=爌:楣 �;M肍?{O�>�?�S杲C9Ti�?=囍�p~f==痡k檤;�須输>�
�?�/�x淚$Q=2鳬箅^l籧甆40.�>_@儶堵�=汤N&!-�;%Y飘濆3?��$9悙"<V�^p�<cKB(臧*?0Uy�臠#?H0兌�(?3u;p?   w�>镽颯	l?   � �=cKB(臧*�0Uy�臠#縮 埫R$�3u;p�   ㄎ萆緀祀d:f�   墟)脚)戼o�'?箾駳i!?H0兌�(?x蝻u鐙?   w�>镽颯	l?   � �=吳
歖&�>�*灐壸�>M肍?{O�>�須输>c甆40.�>%Y飘濆3?V�^p�<辪�<a�=>Uf�>湖ve
P�=僊�:縊�>��3wW�=i葔S箜�>蕢紿pW�=�b阮韮> 呈�#�3@ぜ鷞�-阑f秎1;@砚磎�+@狺┼�,@   TMC_DR_QD_ED      DRIF搢!猛�>?�征�=饞锦E(慕齮菁=p�=烐墢�,�=潶�3牞�=<v�0钥�;�*灐壸�>惈*浇驨\闵十=礠�(v3�=嘋凐W3�=蓳美 �;�8片摅>鄄@�篱終pOD?=�.\f=s韟炓䓖;�須输>G揜N�/�x淚$Q=J{鳕鴁l�+g煈1.�>R甕泻落=杽�#-�;%Y飘濆3?$t%獡�"<*z�;_p�<6鍌+?0Uy�臠#?Cㄦ浤e(?3u;p?   ��>镽颯	l?   L/�=6鍌+�0Uy�臠#�0w嘄�#�3u;p�   嘉萆緀祀d:f�   戽)教絇糕w(?箾駳i!?Cㄦ浤e(?x蝻u鐙?   ��>镽颯	l?   L/�=搢!猛�>�*灐壸�>�8片摅>�須输>+g煈1.�>%Y飘濆3?*z�;_p�<辪�<a�=>Uf�>湖ve
P�=僊�:縊�>^ �3wW�=氯塖箜�>Q娊HpW�=�b阮韮>篇爏�45@l��皻�-阔�6�<:@1(k(+@D 5&朒@	   TMC_DR_V3      DRIF碸咊塞>阁I/菌=P縮撩y慕猰oG爇�=1GE殨R�=溼wi蚵>�R2:1�;�*灐壸�>獠厽�方驨\闵十=郄O硻3�=嘋凐W3�=薙蝑� �;羹梕Q帮>ぼn舖�浣n覿-�9=龤娔(0j=詎�5|v;�須输>qCD橄�/�x淚$Q=欚l_l�#j�>.�>蒆不嗦�=R�"5-�;%Y飘濆3?�	�硱"<E��jp�<$�<AJ�1?0Uy�臠#?驉矌_�#?3u;p?   鄦�>镽颯	l?   垲=$�<AJࡶUy�臠#吭栔裟��3u;p�   t陷删e祀d:f�   冂)絭h姮�;/?箾駳i!?驉矌_�#?x蝻u鐙?   鄦�>镽颯	l?   垲=碸咊塞>�*灐壸�>羹梕Q帮>�須输>#j�>.�>%Y飘濆3?E��jp�<pl�<a�=;<Uf�>M齰e
P�=�:縊�>��3wW�=i葔S箜�>蕢紿pW�=�b阮韮>軳鄂R8A@v�K僋3栏EA�0@赴弄M�%@ぅc牊@   TMC_DR_QD_ED      DRIF�^鱾p�>�d熶_�="Й笓^慕:�/�"�=�,�-吉�=Tx呏0>蝢c!��;�*灐壸�>SV臔5l督驨\闵十=]Y彇�3�=嘋凐W3�=_锅>�;衍枝项>豋犚s磴进搾襝p8=緶�h峧=4�>族醬;�須输>2茖薤/�x淚$Q=@m*_l粅Rpi?.�>级堘渎�=荌<7-�;%Y飘濆3?晪緪稅"<D逥kp�<yi�H�1?0Uy�臠#?$駂!#?3u;p?   韬�>镽颯	l?   �	�=yi�H�1�0Uy�臠#� 洞z��3u;p�   愊萆緀祀d:f�   翮)絵麵kI�/?箾駳i!?$駂!#?x蝻u鐙?   韬�>镽颯	l?   �	�=�^鱾p�>�*灐壸�>衍枝项>�須输>|Rpi?.�>%Y飘濆3?D逥kp�<辪�<a�=>Uf�>哈ve
P�=侻�:縊�>�3wW�=迩塖箜�> 壗HpW�=b阮韮>+7飢
B@"�脢3纔鱲跇H/@旺K%@m\蔫嬹@
   TMC_QD_V03      QUAD8藙W帱>旯惫�*�=薦蚱m?慕*�(0i�=�&vh軘=vz楦Iy>�鯑;暺蟦燠�>u训]"并綁�s鎸�=�G oY=蛦�$,$�=0\R饬�;�O�1,�>堽战8�#�7=賠:*�k=�йwsu;T;A�>�5袶� 界2窺夤`=d貪wア]哗;��?.�>n:P挎落=!(&�7-�;%Y飘濆3?s愓�"<`∨耴p�<兟 02?WZG:�?�槬�"?T檶?	?   ,��>镽颯	l?   |!�=兟 02�WZG:�侩笽�3�T檶?	�   �陷删e祀d:f�   痃)舰^墤�;0?敭�%?�槬�"?(}K8~�?   ,��>镽颯	l?   |!�=8藙W帱>暺蟦燠�>�O�1,�>T;A�>�;��?.�>%Y飘濆3?`∨耴p�<狉鞶7`�=�
)�d�>{<銷�=�9偺<N�>倾ER�=抭侃夑�>豤VJ>R�=�m�1呹�>�蔷q欱@�"骇e� 纕歋.@C疚2@m\蔫嬹@   TMC_COR      KICKER8藙W帱>旯惫�*�=薦蚱m?慕*�(0i�=�&vh軘=vz楦Iy>�鯑;暺蟦燠�>u训]"并綁�s鎸�=�G oY=蛦�$,$�=0\R饬�;�O�1,�>堽战8�#�7=賠:*�k=�йwsu;T;A�>�5袶� 界2窺夤`=d貪wア]哗;��?.�>n:P挎落=!(&�7-�;%Y飘濆3?s愓�"<`∨耴p�<兟 02?WZG:�?�槬�"?T檶?	?   ,��>镽颯	l?   |!�=兟 02�WZG:�侩笽�3�T檶?	�   �陷删e祀d:f�   痃)舰^墤�;0?敭�%?�槬�"?(}K8~�?   ,��>镽颯	l?   |!�=8藙W帱>暺蟦燠�>�O�1,�>T;A�>�;��?.�>%Y飘濆3?`∨耴p�<狉鞶7`�=�
)�d�>{<銷�=�9偺<N�>倾ER�=抭侃夑�>豤VJ>R�=�m�1呹�>�蔷q欱@�"骇e� 纕歋.@C疚2@m\蔫嬹@   TMC_BPM      MONI8藙W帱>旯惫�*�=薦蚱m?慕*�(0i�=�&vh軘=vz楦Iy>�鯑;暺蟦燠�>u训]"并綁�s鎸�=�G oY=蛦�$,$�=0\R饬�;�O�1,�>堽战8�#�7=賠:*�k=�йwsu;T;A�>�5袶� 界2窺夤`=d貪wア]哗;��?.�>n:P挎落=!(&�7-�;%Y飘濆3?s愓�"<`∨耴p�<兟 02?WZG:�?�槬�"?T檶?	?   ,��>镽颯	l?   |!�=兟 02�WZG:�侩笽�3�T檶?	�   �陷删e祀d:f�   痃)舰^墤�;0?敭�%?�槬�"?(}K8~�?   ,��>镽颯	l?   |!�=8藙W帱>暺蟦燠�>�O�1,�>T;A�>�;��?.�>%Y飘濆3?`∨耴p�<狉鞶7`�=�
)�d�>{<銷�=�9偺<N�>倾ER�=抭侃夑�>豤VJ>R�=�m�1呹�>�蔷q欱@�"骇e� 纕歋.@C疚2@�鉳wC@
   TMC_QD_V03      QUAD茄躢�>��9毰$咏Λ�1�慕Y閅�蠄=猛b:藬=��O�>56~64澮;鶐)O2�>^镕�=踓\2T綋� � 膕结�0ば結此[U脖�4=R7觏>缛掳� 詂仂澠7=<穰ek=繹Uz郔u;結趔Lf�>�9鱸荛捡?綱i={qbb4+'祸#"�?.�>毣告落=螲 �7-�;%Y飘濆3?pE撤�"<憛薏kp�<�;�;�;2?�%lU燀�>捥`銦"?��X�>   愖�>镽颯	l?   �#�=�;�;�;2坷熈泀骶膣[b�凯�X缇   X陷删e祀d:f�   早)絴慵yG0?�%lU燀�>捥`銦"?梩畆�>   愖�>镽颯	l?   �#�=茄躢�>鶐)O2�>4=R7觏>結趔Lf�>�#"�?.�>%Y飘濆3?憛薏kp�<�	Tk_�=sj肽騝�>忔�0鮉�=媴a�M�>�!;>M�=g<鐑>�,茐7M�=赥�8鐑>KO阺4碆@嶾�2jQ@�1~z�-@窾鬚儣�?柛S珌�@   TMC_DR_QD_ED      DRIF贳ㄚ柚�>腷a�咏钜�=濢媒$怀�=灍�w晹=滏�5璴>�)6�m�;鶐)O2�>謔妵Y悐=踓\2T姐� !膕结�0ば絸猠kU脖籭劥.>醉>淦冰┙剘�
咏7=�7#4铬l=_r戵�Au;結趔Lf�>\�}荛捡?綱i=莍1�4+'猾�*@.�>T�捩落=寒)�7-�;%Y飘濆3?δ幏�"<飼G竗p�<甄m}�(2?�%lU燀�>K徫贌�"?��X�>   *��>镽颯	l?   n%�=甄m}�(2坷熈泀骶l:◤�凯�X缇   @陷删e祀d:f�   犁)�A5b)70?�%lU燀�>K徫贌�"?梩畆�>   *��>镽颯	l?   n%�=贳ㄚ柚�>鶐)O2�>i劥.>醉>結趔Lf�>*@.�>%Y飘濆3?飼G竗p�<u	Tk_�=dj肽騝�>勬�0鮉�=}卆�M�>�!;>M�=g<鐑>�,茐7M�=跿�8鐑>鏎穚!廈@N淩荇3@痗c-@}X黦n�?﹙��@	   TMC_DR_V4      DRIF�)�>垶麷锂医斏崳+Y媒|x�&3�=峛珗v槗=2 ,>�6餡帄�;鶐)O2�>筐��=踓\2T絡x纏!膕结�0ば絅崬碪脖�巠烈~�>T磒凚┙a�>漴�7=�-K齪=k_oH�u;結趔Lf�>i鵥墋荛捡?綱i=櫌嚔5+'慌昍@.�>(譽嶇落=�398-�;%Y飘濆3?Y愣�"<旆垦kp�<)袈񹭁?�%lU燀�>柾箣趤"?��X�>   监�>镽颯	l?   -�=)袈񹭁坷熈泀骶$u�凯�X缇   肝萆緀祀d:f�   t�)絟寎H�/?�%lU燀�>柾箣趤"?梩畆�>   监�>镽颯	l?   -�=�)�>鶐)O2�>巠烈~�>結趔Lf�>�昍@.�>%Y飘濆3?旆垦kp�<u	Tk_�=dj肽騝�>勬�0鮉�=}卆�M�>�!;>M�=g<鐑>�,茐7M�=跿�8鐑>Z好釧@極�7T�
@呵半�,@逄/n霈�?﹙��@	   TMC_DR_20      DRIF爨噃�>蟪�壶谎��?�蚜経>瞱绸�=�Q�=t?'轺>!:捫姩�;鶐)O2�>矨霶螆=踓\2T健^�"膕结�0ば近蕺kV脖猾b圐L>写D飹xЫ�*茍-7=X躟Cw=霭�&B纓;結趔Lf�>櫟莮荛捡?綱i=炬譺8+'�7�#A.�>a愜C槁�=�F�8-�;%Y飘濆3?盚b6祼"<淚blp�<麙钙�0?�%lU燀�>Q閰H鞢"?��X�>   ん�>镽颯	l?   *@�=麙钙�0坷熈泀骶C�*�2�凯�X缇   l洼删e祀d:f�   ㄦ)絣鱛�.a.?�%lU燀�>Q閰H鞢"?梩畆�>   ん�>镽颯	l?   *@�=爨噃�>鶐)O2�>玝圐L>結趔Lf�>7�#A.�>%Y飘濆3?淚blp�<z	Tk_�=kj肽騝�>夋�0鮉�=儏a�M�>�!;>M�=g<鐑>�,茐7M�=跿�8鐑>\眜C@@�褯翹	@傍F(9+@s蕣Z设?鉺}��@   TMC_DP_DIP1   	   CSRCSBEND温O4窄�>鸥�
逍�	踰�7]澜后酋�瘏=砤5 L�=J鎁o�< >Y�%m颗;渏p倽R�>�>M湁�=屫漑�"T仅郷~u交(J
蝊憬)�鎩娌籗�!F袍�>笺�&犬ソ-|h隿?>=� 歟墋=Z者;{;(鏨f�>�C佐骣糋惝�i= K唢Q춫N!C&�>R�*飭充=rD逽�!�;_槍濆3?洓涬�"<�:�Di�<廫嚻0?鯷唊Q�>{bI "??S'�X�>   w�>"�碷	l?   Jg=廫嚻0縗皹0aH蹙赙愷5�?S'�X缇   ,掌删m鰪Y:f�   蠻綅G07^�,?鯷唊Q�>{bI "?q.鬆祌�>   w�>"�碷	l?   Jg=温O4窄�>渏p倽R�>S�!F袍�>(鏨f�>4N!C&�>_槍濆3?�:�Di�<2回=|蟹=�5bH�>黌鏜�=NM�<餖�>�>>M�=擃醝<鐑>Xa�7M�=�A8鐑>�#@鐀=@鴆滪I�@症耏罍)@x夌彐?鉺}��@   TMC_DR_SIDE_C      CSRDRIFT亅�,^�>颔覻鹱辖 �桒�1雷m�=芿��=H
迿隂�=�)r笠�;渏p倽R�>^+�tG�=屫漑�"T借﹤�u竭�Y憬珡嫡}娌�>衳�>z凿＝睢敄,�==�%膚埡�=2+"qz;(鏨f�>A=袘蹑黾擯!#i=g�T�4粴�#VC&�>�
玲¨�=e=k瑓!�;�#w3鲢3?NM銪x"<*礓PDi�<_SJ鱣..?鯷唊Q�>帖a��!??S'�X�>   N譿�>穻鴣w
l?   &g=_SJ鱣..縗皹0aH蹙? �?S'�X缇   h悠删,i�8p;f�   <U綁�&$卅+?鯷唊Q�>帖a��!?q.鬆祌�>   N譿�>穻鴣w
l?   &g=亅�,^�>渏p倽R�>>衳�>(鏨f�>殻#VC&�>�#w3鲢3?*礓PDi�<<回=|蟹=�傔荋�>�'鮪糔�=Qs痟N�>�>>M�=镨%X<鐑>橁K�7M�=�w�7鐑>Z界�:@q 魳@�7~=(@^1瞂I�?郪��M!@   TMC_DR_SIDE      DRIF慮忞瑞>饩�*慕�!Q炙敖G�嬏羪=e|VMy��2絟Y+Mě痘渏p倽R�>′�9u}=屫漑�"T�5;\Lu竭�Y憬e惵t冩不l憸�>塄2I<R捊軭9�<9=C�?�=P黀寘榲;(鏨f�>２骣紨P!#i=�4a�4籌< 癎&�>�攉�=��!�;�#w3鲢3?(呂j8x"<蜩沉Ei�<検恦倡#?鯷唊Q�>跎)銃T ??S'�X�>   �2x�>穻鴣w
l?   :pg=鄄夌"縗皹0aH蹙W拹[�?S'�X缇   捞粕�,i�8p;f�   xQ綏蕫v倡#?鯷唊Q�>跎)銃T ?q.鬆祌�>   �2x�>穻鴣w
l?   :pg=慮忞瑞>渏p倽R�>l憸�>(鏨f�>I< 癎&�>�#w3鲢3?蜩沉Ei�<>回=|蟹=�傔荋�>�'鮪糔�=Ts痟N�>�>>M�=铊%X<鐑>楆K�7M�=�w�7鐑>鄤�"B)@v�皗_簏?鶖!*祙"@-�'Y�?E尖M"@   TMC_DP_DIP2   	   CSRCSBEND矙� a2�>ペH6螪陆┘%鯧)}現J>w=�瀁欥�絲#嶔咏Z捅L鷑蓟S绩昄�>4�)鎓{=�LT晋
!h)Xs轿�&囪浶桔"奃被瘋c�Q�>謃%�}嵔田�55=z�哕�=_7葊譺;鯼_f�>閆Qs齧窦跼+狔#i=1鍤t�9/粐r}胉"�>-�+︿=�tX��;0N�鲢3?=O鰕輛"<筀芿�<p頟銭"?�烅�>獺.雿 ?妡�X�>   碒茌>錼辪
l?   F�=蓛F�!�挌q骶'偳I殉繆|�X缇   ╪郎救
iz;f�   8�絧頟銭"?�烅�>獺.雿 ?a荁祌�>   碒茌>錼辪
l?   F�=矙� a2�>S绩昄�>瘋c�Q�>鯼_f�>噐}胉"�>0N�鲢3?筀芿�<潡\�	６=缭酺羌�>~�&罭�=y-�N�>薥Y@>M�=B灗Y<鐑>�%j�7M�=(愨�7鐑>蕲烖�%@瞡Hh6>�?莙��!@�<縞I$�?E尖�"@   TMC_DR_CENT_C      CSRDRIFT媠廅��>渨朼諵两��:N~呎黸=ㄧ蘓�K偨榕Vψ�活寻a阑S绩昄�>Z�$蚶y=�LT絙趑)Xs轿�&囪浶絁_(牔Q被� Nb�*�>釉
�,閴桨��Q~�4=R佝=q矧酰榬;鯼_f�>J稵饼m窦跼+狔#i=�(椏�9/晦郴!a"�>�担,︿=廃n吩�;0N�鲢3?荽嫞躹"<�芿�<4呀6W�!?�烅�>�;�,�?妡�X�>   0R茌>錼辪
l?   ��=蓋�'8 �挌q骶J┘慨�繆|�X缇   黰郎救
iz;f�   臐�4呀6W�!?�烅�>�;�,�?a荁祌�>   0R茌>錼辪
l?   ��=媠廅��>S绩昄�>� Nb�*�>鯼_f�>蕹�!a"�>0N�鲢3?�芿�<潡\�	６=缭酺羌�>~�&罭�=y-�N�>薥Y@>M�=B灗Y<鐑>�%j�7M�=(愨�7鐑>4E6珲5$@牔腄�?R醕磪�!@�7�]�?E尖�"@   TMC_BPM      MONI媠廅��>渨朼諵两��:N~呎黸=ㄧ蘓�K偨榕Vψ�活寻a阑S绩昄�>Z�$蚶y=�LT絙趑)Xs轿�&囪浶絁_(牔Q被� Nb�*�>釉
�,閴桨��Q~�4=R佝=q矧酰榬;鯼_f�>J稵饼m窦跼+狔#i=�(椏�9/晦郴!a"�>�担,︿=廃n吩�;0N�鲢3?荽嫞躹"<�芿�<4呀6W�!?�烅�>�;�,�?妡�X�>   0R茌>錼辪
l?   ��=蓋�'8 �挌q骶J┘慨�繆|�X缇   黰郎救
iz;f�   臐�4呀6W�!?�烅�>�;�,�?a荁祌�>   0R茌>錼辪
l?   ��=媠廅��>S绩昄�>� Nb�*�>鯼_f�>蕹�!a"�>0N�鲢3?�芿�<潡\�	６=缭酺羌�>~�&罭�=y-�N�>薥Y@>M�=B灗Y<鐑>�%j�7M�=(愨�7鐑>4E6珲5$@牔腄�?R醕磪�!@�7�]�?E尖�#@   TMC_DR_CENT      DRIFu寂纹碾>ccp勺窘暯FXN皓綑!靬s=aF�"嚱�
闥呓S雰l赌籗绩昄�>�#灑>w=�LT絡廖+Xs轿�&囪浶絃>媁被鮾䓖莒�>暇誼Z羵剿s]:4=Hr>8�=k{�r;鯼_f�>瑨盤窦跼+狔#i=~譧u�9/桓/糨a"�>轻璝.︿=8� u��;0N�鲢3?]救鲒v"<�)�7莈�<脰棈= ?�烅�>┄Ns?妡�X�>   e茌>錼辪
l?   �,�=】鼘嗱�挌q骶曶Λi;繆|�X缇   ╨郎救
iz;f�   鴾矫枟�= ?�烅�>┄Ns?a荁祌�>   e茌>錼辪
l?   �,�=u寂纹碾>S绩昄�>鮾䓖莒�>鯼_f�>�/糨a"�>0N�鲢3?�)�7莈�<潡\�	６=缭酺羌�>~�&罭�=y-�N�>薥Y@>M�=B灗Y<鐑>�%j�7M�=(愨�7鐑>b)ＱD!@~烬�.�?員�.X!@W.鯉幬�?�!��$@   TMC_DP_DIP3   	   CSRCSBEND憵�橀>\@洉.唤鷩8SlЫ洒p=�1纚}娊梋喳]峤�2[浄腔`);撚>霒╕衪=�4dqT轿P�=鈗q綈栽Eh锏=	鲦=e(uV骆>A挑孓3w�)%偸A�0="瘎虠=舏i飈n;QSaf�>嘟s戈�*|偐#i=RU00亠'籷�/珫�>a8[=$`噫�;Da檩蹉3?��
ks"<╚ v盻�<K�伋?T砣�>騲� q�?⑧￤X�>   X,.�>_GSa
l?   甴
=姕]8��`Q覛n炵�$�竣喋WX缇   @幇删鈎QC�;f�   術終�伋?T砣�>騲� q�?z眬!眗�>   X,.�>_GSa
l?   甴
=憵�橀>`);撚>e(uV骆>QSaf�>q�/珫�>Da檩蹉3?╚ v盻�<至i�R�=�3o蕇R�>葴�/肗�=A鵊N�>绁B>M�=萙<鐑>榍�7M�=⒊婶7鐑>Crl鬁T@$,Y/wy�?`蔙>!@�q�>䜣�?�!��%@   TMC_DR_SIDE_C      CSRDRIFT^W鰵鋱�>剚态g方錰4�7ソ�0%嶌l=繪(惑賻綑 P�
桨0扴W熕籤);撚>K躖譔r=�4dqT�M┾qq綈栽Eh锏=黫殒=圸A芎>瓑k萶萢絎6jb�0=蘱W^�=刄G雙璵;QSaf�>玵跗 戈�!*|偐#i=衐b欄�'谎N)n��>罠�]=笺煣��;Da檩蹉3?�9羫is"<gn驶盻�<L责6Y�?T砣�>壃菈憑?⑧￤X�>   dC.�>_GSa
l?   ~
=�硸�]�`Q覛~<逇竣喋WX缇   袑吧锯hQC�;f�   鋐絃责6Y�?T砣�>壃菈憑?z眬!眗�>   dC.�>_GSa
l?   ~
=^W鰵鋱�>`);撚>圸A芎>QSaf�>袾)n��>Da檩蹉3?gn驶盻�<至i�R�=�3o蕇R�>葴�/肗�=A鵊N�>绁B>M�=萙<鐑>榍�7M�=⒊婶7鐑>\v崽@#筪酿?/d=,� @銅開娶?樉�0�+@   TMC_DR_SIDE      DRIFX'�[@�>鼏wm槠g�$.AH�"澖G焱K鯴"經j9dd倻絵�&狩辖5�=Z鈫倩`);撚>孋唰H.J=�4dqT綔耱,錻q綈栽Eh锏=�2韞�=�-�4喷>曣�*m>�=凱�L$,=f奋蜜e�=GW灸w0i;QSaf�>韠�逢�!*|偐#i=P良层�'�7�
���>?h=沐匭��;Da檩蹉3?酘�`s"<�o]砡�<c饮朱?T砣�>锣═�?⑧￤X�>   8�.�>_GSa
l?   �=淇 鋅�`Q覛sU嵇7�竣喋WX缇    劙删鈎QC�;f�   0a絚饮朱?T砣�>锣═�?z眬!眗�>   8�.�>_GSa
l?   �=X'�[@�>`);撚>�-�4喷>QSaf�>7�
���>Da檩蹉3?�o]砡�<琢i�R�=�3o蕇R�>蓽�/肗�=A鵊N�>绁B>M�=萙<鐑>榍�7M�=⒊婶7鐑>疽壿:�@~檃�?彊彞"@�芌yR钥�#�2�,@   TMC_DP_DIP4   	   CSRCSBENDｗ陉"e�>隢Ub娋�=T貈�澖砌o僵<�熃)+龂pR医c/棼扌刍絃b楲�>C⑾�<y$=Aen蜺T�4"O趺s焦浶涧/J�(脖粸R m5�>歞抓覗=鄆f媟�.=咭E铭-�=艰&/k;扬9_[f�>lBI�2玳紥�'m�#i=脟��4'籆—P�+�>亡嚵2や=~ �壗)�;飝V骢�3?�4+汍t"<T娆籊n�<緭匘�?60�燀�>歒犛s?OwX�>   0:�>�焅
l?   �2�=8F&�=苦蔓仛q骶S酮k餳縊wX缇   @龠删JE渶�;f�   h�+骄搮D�?60�燀�>歒犛s?迴U~�>   0:�>�焅
l?   �2�=ｗ陉"e�>絃b楲�>濺 m5�>扬9_[f�>C—P�+�>飝V骢�3?T娆籊n�<刵O!,`�=澒�=餯�>)猊柺N�=沢N�>$鰽>M�='b罿<鐑>_V2�7M�=k/�7鐑>yw免|@��槁x量>�7}>W#@�7灜z乜�#�2�-@   TMC_DR_20_C      CSRDRIFT3浵^7>6��規�=育#�綕�.o簎_V紿鈡牻�哾犣金�"$藁絃b楲�>]X�=紸en蜺T紻蒴3雒s焦浶溅{妘)脖籡唒45惰>Ne鱶f�=y夳�9�-=^鳍={�=cj;扬9_[f�>8zb�3玳紥�'m�#i=粸欛4'粩(h�+�>鵭w4や=説罤�)�;飝V骢�3?I歒眭t"<(u麲n�<覠鄤r:?60�燀�>�&t䲣�?OwX�>   ,M�>�焅
l?   F�=	斬�?苦蔓仛q骶cM�漥縊wX缇   嘧呱綣E渶�;f�   埪+揭熰剅:?60�燀�>�&t䲣�?迴U~�>   ,M�>�焅
l?   F�=3浵^7>絃b楲�>]唒45惰>扬9_[f�>�(h�+�>飝V骢�3?(u麲n�<刵O!,`�=澒�=餯�>)猊柺N�=沢N�>$鰽>M�=(b罿<鐑>`V2�7M�=l/�7鐑>`贛�	@���瓙涌椼F''$@嘵'姺噗魁#�2�-@	   TMC_WA_OU      WATCH3浵^7>6��規�=育#�綕�.o簎_V紿鈡牻�哾犣金�"$藁絃b楲�>]X�=紸en蜺T紻蒴3雒s焦浶溅{妘)脖籡唒45惰>Ne鱶f�=y夳�9�-=^鳍={�=cj;扬9_[f�>8zb�3玳紥�'m�#i=粸欛4'粩(h�+�>鵭w4や=説罤�)�;飝V骢�3?I歒眭t"<(u麲n�<覠鄤r:?60�燀�>�&t䲣�?OwX�>   ,M�>�焅
l?   F�=	斬�?苦蔓仛q骶cM�漥縊wX缇   嘧呱綣E渶�;f�   埪+揭熰剅:?60�燀�>�&t䲣�?迴U~�>   ,M�>�焅
l?   F�=3浵^7>絃b楲�>]唒45惰>扬9_[f�>�(h�+�>飝V骢�3?(u麲n�<刵O!,`�=澒�=餯�>)猊柺N�=沢N�>$鰽>M�=(b罿<鐑>`V2�7M�=l/�7鐑>`贛�	@���瓙涌椼F''$@嘵'姺噗�