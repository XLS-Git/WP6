SDDS1
!# little-endian
&description text="sigma matrix--input: xls_linac_tmc_sxr_maxe_hxr.track.ele  lattice: xls_linac_tmc_sxr_maxe.12.lte", contents="sigma matrix", &end
&parameter name=Step, description="Simulation step", type=long, &end
&parameter name=SVNVersion, description="SVN version number", type=string, fixed_value=unknown, &end
&column name=s, units=m, description=Distance, type=double,  &end
&column name=ElementName, description="Element name", format_string=%10s, type=string,  &end
&column name=ElementOccurence, description="Occurence of element", format_string=%6ld, type=long,  &end
&column name=ElementType, description="Element-type name", format_string=%10s, type=string,  &end
&column name=s1, symbol="$gs$r$b1$n", units=m, description="sqrt(<x*x>)", type=double,  &end
&column name=s12, symbol="$gs$r$b12$n", units=m, description="<x*xp'>", type=double,  &end
&column name=s13, symbol="$gs$r$b13$n", units="m$a2$n", description="<x*y>", type=double,  &end
&column name=s14, symbol="$gs$r$b14$n", units=m, description="<x*y'>", type=double,  &end
&column name=s15, symbol="$gs$r$b15$n", units="m$a2$n", description="<x*s>", type=double,  &end
&column name=s16, symbol="$gs$r$b16$n", units=m, description="<x*delta>", type=double,  &end
&column name=s17, symbol="$gs$r$b17$n", units="m*s", description="<x*t>", type=double,  &end
&column name=s2, symbol="$gs$r$b2$n", description="sqrt(<x'*x'>)", type=double,  &end
&column name=s23, symbol="$gs$r$b23$n", units=m, description="<x'*y>", type=double,  &end
&column name=s24, symbol="$gs$r$b24$n", description="<x'*y'>", type=double,  &end
&column name=s25, symbol="$gs$r$b25$n", units=m, description="<x'*s>", type=double,  &end
&column name=s26, symbol="$gs$r$b26$n", description="<x'*delta>", type=double,  &end
&column name=s27, symbol="$gs$r$b27$n", units=s, description="<x'*t>", type=double,  &end
&column name=s3, symbol="$gs$r$b3$n", units=m, description="sqrt(<y*y>)", type=double,  &end
&column name=s34, symbol="$gs$r$b34$n", units=m, description="<y*y'>", type=double,  &end
&column name=s35, symbol="$gs$r$b35$n", units="m$a2$n", description="<y*s>", type=double,  &end
&column name=s36, symbol="$gs$r$b36$n", units=m, description="<y*delta>", type=double,  &end
&column name=s37, symbol="$gs$r$b37$n", units="m*s", description="<y*t>", type=double,  &end
&column name=s4, symbol="$gs$r$b4$n", description="sqrt(<y'*y')>", type=double,  &end
&column name=s45, symbol="$gs$r$b45$n", units=m, description="<y'*s>", type=double,  &end
&column name=s46, symbol="$gs$r$b46$n", description="<y'*delta>", type=double,  &end
&column name=s47, symbol="$gs$r$b47$n", units=s, description="<y'*t>", type=double,  &end
&column name=s5, symbol="$gs$r$b5$n", units=m, description="sqrt(<s*s>)", type=double,  &end
&column name=s56, symbol="$gs$r$b56$n", units=m, description="<s*delta>", type=double,  &end
&column name=s57, symbol="$gs$r$b57$n", units="m*s", description="<s*t>", type=double,  &end
&column name=s6, symbol="$gs$r$b6$n", description="sqrt(<delta*delta>)", type=double,  &end
&column name=s67, symbol="$gs$r$b67$n", units=s, description="<delta*t>", type=double,  &end
&column name=s7, symbol="$gs$r$b7$n", description="sqrt(<t*t>)", type=double,  &end
&column name=ma1, symbol="max$sb$ex$sb$e", units=m, description="maximum absolute value of x", type=double,  &end
&column name=ma2, symbol="max$sb$ex'$sb$e", description="maximum absolute value of x'", type=double,  &end
&column name=ma3, symbol="max$sb$ey$sb$e", units=m, description="maximum absolute value of y", type=double,  &end
&column name=ma4, symbol="max$sb$ey'$sb$e", description="maximum absolute value of y'", type=double,  &end
&column name=ma5, symbol="max$sb$e$gD$rs$sb$e", units=m, description="maximum absolute deviation of s", type=double,  &end
&column name=ma6, symbol="max$sb$e$gd$r$sb$e", description="maximum absolute value of delta", type=double,  &end
&column name=ma7, symbol="max$sb$e$gD$rt$sb$e", units=s, description="maximum absolute deviation of t", type=double,  &end
&column name=minimum1, symbol="x$bmin$n", units=m, type=double,  &end
&column name=minimum2, symbol="x'$bmin$n", units=m, type=double,  &end
&column name=minimum3, symbol="y$bmin$n", units=m, type=double,  &end
&column name=minimum4, symbol="y'$bmin$n", units=m, type=double,  &end
&column name=minimum5, symbol="$gD$rs$bmin$n", units=m, type=double,  &end
&column name=minimum6, symbol="$gd$r$bmin$n", units=m, type=double,  &end
&column name=minimum7, symbol="t$bmin$n", units=s, type=double,  &end
&column name=maximum1, symbol="x$bmax$n", units=m, type=double,  &end
&column name=maximum2, symbol="x'$bmax$n", units=m, type=double,  &end
&column name=maximum3, symbol="y$bmax$n", units=m, type=double,  &end
&column name=maximum4, symbol="y'$bmax$n", units=m, type=double,  &end
&column name=maximum5, symbol="$gD$rs$bmax$n", units=m, type=double,  &end
&column name=maximum6, symbol="$gd$r$bmax$n", units=m, type=double,  &end
&column name=maximum7, symbol="t$bmax$n", units=s, type=double,  &end
&column name=Sx, symbol="$gs$r$bx$n", units=m, description=sqrt(<(x-<x>)^2>), type=double,  &end
&column name=Sxp, symbol="$gs$r$bx'$n", description=sqrt(<(x'-<x'>)^2>), type=double,  &end
&column name=Sy, symbol="$gs$r$by$n", units=m, description=sqrt(<(y-<y>)^2>), type=double,  &end
&column name=Syp, symbol="$gs$r$by'$n", description=sqrt(<(y'-<y'>)^2>), type=double,  &end
&column name=Ss, symbol="$gs$r$bs$n", units=m, description=sqrt(<(s-<s>)^2>), type=double,  &end
&column name=Sdelta, symbol="$gs$bd$n$r", description=sqrt(<(delta-<delta>)^2>), type=double,  &end
&column name=St, symbol="$gs$r$bt$n", units=s, description=sqrt(<(t-<t>)^2>), type=double,  &end
&column name=ex, symbol="$ge$r$bx$n", units=m, description="geometric horizontal emittance", type=double,  &end
&column name=enx, symbol="$ge$r$bx,n$n", units=m, description="normalized horizontal emittance", type=double,  &end
&column name=ecx, symbol="$ge$r$bx,c$n", units=m, description="geometric horizontal emittance less dispersive contributions", type=double,  &end
&column name=ecnx, symbol="$ge$r$bx,cn$n", units=m, description="normalized horizontal emittance less dispersive contributions", type=double,  &end
&column name=ey, symbol="$ge$r$by$n", units=m, description="geometric vertical emittance", type=double,  &end
&column name=eny, symbol="$ge$r$by,n$n", units=m, description="normalized vertical emittance", type=double,  &end
&column name=ecy, symbol="$ge$r$by,c$n", units=m, description="geometric vertical emittance less dispersive contributions", type=double,  &end
&column name=ecny, symbol="$ge$r$by,cn$n", units=m, description="normalized vertical emittance less dispersive contributions", type=double,  &end
&column name=betaxBeam, symbol="$gb$r$bx,beam$n", units=m, description="betax for the beam, excluding dispersive contributions", type=double,  &end
&column name=alphaxBeam, symbol="$ga$r$bx,beam$n", description="alphax for the beam, excluding dispersive contributions", type=double,  &end
&column name=betayBeam, symbol="$gb$r$by,beam$n", units=m, description="betay for the beam, excluding dispersive contributions", type=double,  &end
&column name=alphayBeam, symbol="$ga$r$by,beam$n", description="alphay for the beam, excluding dispersive contributions", type=double,  &end
&data mode=binary, &end
/                 _BEG_      MARK0�1�
�>}����=%�M�ڲ�MP�q��r����?e�����ĺ�X�ϧ2�=�y ��>_��
X�u�(Nb;��e�<1�p�4�����LY6ܽ����ӻαp�i��>b��!^�=���C-Q=���@���=�?�����;ۇ9�5��>j4!8	�* R�t`��&\�'
Q�F'p]	��>�(��t>E��t5s�;b��b�c3?��>�J<p���y/=�a��"�?�^�v? ?8�;�CD%?PI�-�,�>   �l1�>3�����k?   n%�"= �{�v���^�v? �^Q`�I#�PI�-�,��   �l1�3�����k�   n%�"��a��"�?�i��E�>8�;�CD%?���x�>   *���>K���xb?   ��"=0�1�
�>=�y ��>αp�i��>ۇ9�5��>F'p]	��>b��b�c3?p���y/=�9����=(�δR�>d�o��=:��%�2�>ݎЙF	�=C?.7�>|�
E	�=�d�T7�>�U\�	@�G<��Uӿ�,X�|�"@bã���޿           BNCH      CHARGE0�1�
�>}����=%�M�ڲ�MP�q��r����?e�����ĺ�X�ϧ2�=�y ��>_��
X�u�(Nb;��e�<1�p�4�����LY6ܽ����ӻαp�i��>b��!^�=���C-Q=���@���=�?�����;ۇ9�5��>j4!8	�* R�t`��&\�'
Q�F'p]	��>�(��t>E��t5s�;b��b�c3?��>�J<p���y/=�a��"�?�^�v? ?8�;�CD%?PI�-�,�>   �l1�>3�����k?   n%�"= �{�v���^�v? �^Q`�I#�PI�-�,��   �l1�3�����k�   n%�"��a��"�?�i��E�>8�;�CD%?���x�>   *���>K���xb?   ��"=0�1�
�>=�y ��>αp�i��>ۇ9�5��>F'p]	��>b��b�c3?p���y/=�9����=(�δR�>d�o��=:��%�2�>ݎЙF	�=C?.7�>|�
E	�=�d�T7�>�U\�	@�G<��Uӿ�,X�|�"@bã���޿윙����?	   HXR_DR_V1      DRIFBD��t��>�`�)�+�=�jw�b�� �Jޗ�t���F�?����Q� ���)ډ/�=�y ��>S�v:�w�(Nb;��e��5譑4�����LY6ܽ�V���ӻ�XF).�>���z�A�=��(Y�P=U�E���=أ�<�S�;ۇ9�5��>�_�8	�* R�t`�F�O(
Q����z	��>F�"u>�E5s�;b��b�c3?˪`�=�J<��Z�y/=����?�^�v? ?�M��)%?PI�-�,�>   �l1�>3�����k?   0%�"=���d)g��^�v? ��q'0�H#�PI�-�,��   �l1�3�����k�   0%�"�����?�i��E�>�M��)%?���x�>   ���>K���xb?   ���"=BD��t��>=�y ��>�XF).�>ۇ9�5��>���z	��>b��b�c3?��Z�y/=�9����=(�δR�>d�o��=:��%�2�>ݎЙF	�=C?.7�>|�
E	�=�d�T7�>�_�?}�
@����+�׿�sg��B#@Jt�5��䥛� �?   HXR_DR_QD_ED      DRIF.��x��>a"� L�=C�
�p����D.bxu��z�ӭ��[��H�b�.ٳ�=�y ��> �˱Tx�(Nb;��e�D�۽�4�����LY6ܽ����ӻ@?�	�)�>���|�=L6����P=�j%x�=�	9l|7�;ۇ9�5��>��R!9	�* R�t`�(�u(
Q�6{��	��>�֪?u>���85s�;b��b�c3?��tr=�J<pf��y/=	���t ?�^�v? ?c~���"%?PI�-�,�>   �l1�>3�����k?   %�"=M�Y��^�v? �	��ۥH#�PI�-�,��   �l1�3�����k�   %�"�	���t ?�i��E�>c~���"%?���x�>   ����>K���xb?   O��"=.��x��>=�y ��>@?�	�)�>ۇ9�5��>6{��	��>b��b�c3?pf��y/=�9����=(�δR�>d�o��=:��%�2�>ݎЙF	�=C?.7�>|�
E	�=�d�T7�>M�F@�(���ؿk�]#@���ţl࿟*\����?
   HXR_QD_V01      QUAD�T��͝�>)������������n��������6�歽�c[�f�чb����81J'�>�F���=���ϔ�{=�v�7`IO���U*'���D#�������7�>����y��=P�����P=\'-��}�="�-�CB�;�/Q�S��>�D���;3=��W�	v=:hkZ8q;c%�	��>��Vu>���C5s�;b��b�c3?ui�m=�J<�Y͆y/=��9�?�,����??ĦS�-%?��#�Xy?   �l1�>3�����k?   %�"=#���J���^��z��E�S#���#dB�   �l1�3�����k�   %�"���9�?�,����??ĦS�-%?��#�Xy?   ����>K���xb?   8��"=�T��͝�>�81J'�>����7�>�/Q�S��>c%�	��>b��b�c3?�Y͆y/=������=�ס�R�>��N6��=�r`}2�>LI4��=-���6�>��u��=IW��6�>�ڵ�:@"�B�;�?�ad�{#@D�@�����*\����?   HXR_COR      KICKER�T��͝�>)������������n��������6�歽�c[�f�чb����81J'�>�F���=���ϔ�{=�v�7`IO���U*'���D#�������7�>����y��=P�����P=\'-��}�="�-�CB�;�/Q�S��>�D���;3=��W�	v=:hkZ8q;c%�	��>��Vu>���C5s�;b��b�c3?ui�m=�J<�Y͆y/=��9�?�,����??ĦS�-%?��#�Xy?   �l1�>3�����k?   %�"=#���J���^��z��E�S#���#dB�   �l1�3�����k�   %�"���9�?�,����??ĦS�-%?��#�Xy?   ����>K���xb?   8��"=�T��͝�>�81J'�>����7�>�/Q�S��>c%�	��>b��b�c3?�Y͆y/=������=�ס�R�>��N6��=�r`}2�>LI4��=-���6�>��u��=IW��6�>�ڵ�:@"�B�;�?�ad�{#@D�@�����*\����?   HXR_BPM      MONI�T��͝�>)������������n��������6�歽�c[�f�чb����81J'�>�F���=���ϔ�{=�v�7`IO���U*'���D#�������7�>����y��=P�����P=\'-��}�="�-�CB�;�/Q�S��>�D���;3=��W�	v=:hkZ8q;c%�	��>��Vu>���C5s�;b��b�c3?ui�m=�J<�Y͆y/=��9�?�,����??ĦS�-%?��#�Xy?   �l1�>3�����k?   %�"=#���J���^��z��E�S#���#dB�   �l1�3�����k�   %�"���9�?�,����??ĦS�-%?��#�Xy?   ����>K���xb?   8��"=�T��͝�>�81J'�>����7�>�/Q�S��>c%�	��>b��b�c3?�Y͆y/=������=�ס�R�>��N6��=�r`}2�>LI4��=-���6�>��u��=IW��6�>�ڵ�:@"�B�;�?�ad�{#@D�@����Ap����?
   HXR_QD_V01      QUAD�nc%|~�>9��]��ҽ�=(U�����3<m]��Mg���֭�jĉ�c콆�aW���.�o����>��?���=)mr�#�=XUq�#A�=uc:�v1�=�N"�;m��b[�>[Q��h��=t�6� Q=���	؜�=���p�;x�ȕ��>�)�v�E=��|�3�=ݢ��}c�;�w�	��>6,�u>�/}5s�;b��b�c3?"��=�J<��f�y/=���6(�?�
�?�M�qQ%?�M��:?    m1�>3�����k?   9%�"="����&��
��UǑ"v#����Ԍ�    m1�3�����k�   9%�"����6(�?s�/n?�M�qQ%?�M��:?   ����>K���xb?   ��"=�nc%|~�>.�o����>m��b[�>x�ȕ��>�w�	��>b��b�c3?��f�y/=xf'���=����R�>�7f޴�=�+�	z2�>��9S��=8�?�^6�>��"���=1|��\6�>=
���
@r�/С @���k�#@�~��Ȇ�Q��?   HXR_DR_QD_ED      DRIF+�q���>�+�ҽ���'e���X�y��k���g�P��
� ��3N>�.�o����>H��E-٩=)mr�#�=(���#A�=uc:�v1�=����N"�;�igF��>�Ѧ�͜�=�@��Q=n�Qq=K�=��uks�;x�ȕ��>T\����E=��|�3�=�����c�;L�w?
��>��	�w>�e�6s�;b��b�c3?)0�?�J<C��z/=���s�K?�
�?{1�&?�M��:?   �m1�>3�����k?   �%�"=�_�Qtk��
��jZ+R�,$����Ԍ�   �m1�3�����k�   �%�"����s�K?s�/n?{1�&?�M��:?   ����>K���xb?   ���"=+�q���>.�o����>�igF��>x�ȕ��>L�w?
��>b��b�c3?C��z/={f'���=����R�>�7f޴�=�+�	z2�>q�9S��=�?�^6�>T�"���=�{��\6�>�*]�J	@�p���?Ѻy�_%@mO��_5��D<j�?	   HXR_DR_V2      DRIFI����>���1	�ýd�h�Ѡ��9Kwݻ���X6i�X��7T�g͂轑B�ue��.�o����>�.��D�=)mr�#�=W~U�$A�=uc:�v1�=�<g3O"�;w:�u'O�>��w7��=�_���W=�g>�W�=Ec�|p�;x�ȕ��>;�/#C�E=��|�3�=k�r�3d�;�� ��>�R��>�U,�Fs�;b��b�c3?+S�nO�J<arH5/=�#���x?�
�?"��X�.?�M��:?   �u1�>3�����k?   �,�"=ލ�d7���
��wn@,����Ԍ�   �u1�3�����k�   �,�"��#���x?s�/n?"��X�.?�M��:?   Z���>K���xb?   ���"=I����>.�o����>w:�u'O�>x�ȕ��>�� ��>b��b�c3?arH5/=zf'���=����R�>�7f޴�=�+�	z2�>��9S��=8�?�^6�>��"���=1|��\6�>R�t�?�U�����?��&�8�5@�P`e?m&�Lw~+8�?   HXR_DR_QD_ED      DRIFiM.����>�m ��=½��Z�$�����eʡ���rN�Ѧ�4�@�I2轞/�c�m�.�o����>�Dq�^��=)mr�#�='��$A�=uc:�v1�=�SAO"�;�O�@c	�>�A �P��=���<5�X=�{�;���=�9�ѝ�;x�ȕ��>�Ģ�S�E=��|�3�=�>6�Bd�;Gd���>���>�j?Hs�;b��b�c3?ͅh�P�J<�yϩ/=�Gꇹ;?�
�?��v�`/?�M��:?   �v1�>3�����k?   2-�"=/�v����
��b|�Ӈ�,����Ԍ�   �v1�3�����k�   2-�"��Gꇹ;?s�/n?��v�`/?�M��:?   :���>K���xb?   5��"=iM.����>.�o����>�O�@c	�>x�ȕ��>Gd���>b��b�c3?�yϩ/=yf'���=����R�>�7f޴�=�+�	z2�>q�9S��=�?�^6�>T�"���=�{��\6�>q 	:��?Gp:G��?��5*�7@Q���b'��Y%�~�?
   HXR_QD_V02      QUAD������>]Q6���)����0�e��h���I�CǾ��hu%��-�^��\�-�r��><suqG�=�[6��l=I=o'a�c=��o�)����yޡ;4ฺ%�>TU�1Y�=��S�X=���ɞ=��s���;Rf�6`!�>UnA�9=<�38UlS=��;�c�Q;�����>�-,�>w��aHs�;b��b�c3?"I��P�J<A(�/=A���z�?�x* ��
?m�b�s|/?^��`S�>   �v1�>3�����k?   C-�"=T
+'���x* ��
�f�WY-�^��`S��   �v1�3�����k�   C-�"�A���z�?~{4�B
?m�b�s|/?�"����>   4���>K���xb?   ��"=������>-�r��>4ฺ%�>Rf�6`!�>�����>b��b�c3?A(�/=Q!�y���=�bk�jR�>\�$���=əD�-2�>l]����=��Y�6�>��=���@�6�>��R�y�?H�����?̩_�AA7@�7�0��Y%�~�?   HXR_COR      KICKER������>]Q6���)����0�e��h���I�CǾ��hu%��-�^��\�-�r��><suqG�=�[6��l=I=o'a�c=��o�)����yޡ;4ฺ%�>TU�1Y�=��S�X=���ɞ=��s���;Rf�6`!�>UnA�9=<�38UlS=��;�c�Q;�����>�-,�>w��aHs�;b��b�c3?"I��P�J<A(�/=A���z�?�x* ��
?m�b�s|/?^��`S�>   �v1�>3�����k?   C-�"=T
+'���x* ��
�f�WY-�^��`S��   �v1�3�����k�   C-�"�A���z�?~{4�B
?m�b�s|/?�"����>   4���>K���xb?   ��"=������>-�r��>4ฺ%�>Rf�6`!�>�����>b��b�c3?A(�/=Q!�y���=�bk�jR�>\�$���=əD�-2�>l]����=��Y�6�>��=���@�6�>��R�y�?H�����?̩_�AA7@�7�0��Y%�~�?   HXR_BPM      MONI������>]Q6���)����0�e��h���I�CǾ��hu%��-�^��\�-�r��><suqG�=�[6��l=I=o'a�c=��o�)����yޡ;4ฺ%�>TU�1Y�=��S�X=���ɞ=��s���;Rf�6`!�>UnA�9=<�38UlS=��;�c�Q;�����>�-,�>w��aHs�;b��b�c3?"I��P�J<A(�/=A���z�?�x* ��
?m�b�s|/?^��`S�>   �v1�>3�����k?   C-�"=T
+'���x* ��
�f�WY-�^��`S��   �v1�3�����k�   C-�"�A���z�?~{4�B
?m�b�s|/?�"����>   4���>K���xb?   ��"=������>-�r��>4ฺ%�>Rf�6`!�>�����>b��b�c3?A(�/=Q!�y���=�bk�jR�>\�$���=əD�-2�>l]����=��Y�6�>��=���@�6�>��R�y�?H�����?̩_�AA7@�7�0��46��?
   HXR_QD_V02      QUADW��Fϻ�>��bq��=貕'�᰽j
�[=�?.Ldɦ�~�.�H�=���Af������>I8,�/�=&a+����a�W������b��ٽ���J�O˻Sh5��>�����ge����X=�$6й�=������;w����>Жu]F�@��Ҁ�[���� ['�}�j�����> /�D�>�v�jHs�;b��b�c3?b[��P�J<V��/=�+��? ��A�?.|)ݢo/?�Yգ�?   �v1�>3�����k?   F-�"=D�i+���D|�)�oi��-��Yգ��   �v1�3�����k�   F-�"��+��? ��A�?.|)ݢo/?�'v_@�?   .���>K���xb?    ��"=W��Fϻ�>�����>Sh5��>w����>j�����>b��b�c3?V��/=��F���=��_AR�>e��\��=ᖇe�1�>~�F�$	�=��{��6�>ƍ.#	�=��E&�6�>��s�d�?kFz����z`37@��K�4@�P���?   HXR_DR_QD_ED      DRIF�����>��n6��=�i�_�����͕=�=���U�.����g�g��;��%�������>��>ן�=&a+�����]�נ�����b��ٽ�����O˻�8��>����{뽺�S�X=V�0%�+�=� �Qƕ�;w����>�dJ�@��Ҁ�[��9��z.�}�Q����>{��A�>t��
Is�;b��b�c3?t�SxQ�J<���/=j}�? ��A�?N͈���.?�Yգ�?   �v1�>3�����k?   |-�"=KLҪ��D|�)��N<�ݏ,��Yգ��   �v1�3�����k�   |-�"�j}�? ��A�?N͈���.?�'v_@�?   ���>K���xb?   ��"=�����>�����>�8��>w����>Q����>b��b�c3?���/=��F���=��_AR�>d��\��=���e�1�>��F�$	�=��{��6�>Ս.#	�=��E&�6�>eL�Z(��?��6��vt��C�6@60�(��@j�2Ռ@	   HXR_DR_V3      DRIF��_���>+�Qp��=[�W���C�s*S~=���I�鯽���4������\��������>�}���=&a+����(��٫�����b��ٽ/�(�O˻�RE���>\�O3A��Up��%M=w����=e$\��;w����>-���@��Ҁ�[�����X��}��(ڦ��>�P�>¤��Vs�;b��b�c3?�o,k\�J<��Ƀ/=��ŉ�? ��A�?�4510$?�Yգ�?   }1�>3�����k?   2�"=��ŉ���D|�)��q�o\"��Yգ��   }1�3�����k�   2�"�1$�Q^|? ��A�?�4510$?�'v_@�?   ���>K���xb?   ��"=��_���>�����>�RE���>w����>�(ڦ��>b��b�c3?��Ƀ/=��F���=��_AR�>e��\��=ᖇe�1�>��F�$	�=��{��6�>΍.#	�=��E&�6�>�*�k{<@�|y`d�￧�4Ow$@FV%@;��+T�@   HXR_DR_QD_ED      DRIF}� �=��>p��.��=�������D!�;ȑ|=�J�O�'��*Tz~�$���+����������>`lȲf�=&a+������Y������b��ٽ����O˻��̡D�>
�����ὝF�d�GL=e����d�=���PQ�;w����>w�G��@��Ҁ�[�����y��}�����>*���>q{�iWs�;b��b�c3?OՊ�\�J<�-��/=.�@? ��A�?Xn�ʵ#?�Yգ�?   H}1�>3�����k?   @2�"=.�@��D|�)�@����!��Yգ��   H}1�3�����k�   @2�"�c��~�? ��A�?Xn�ʵ#?�'v_@�?   ����>K���xb?   �"=}� �=��>�����>��̡D�>w����>����>b��b�c3?�-��/=��F���=��_AR�>d��\��=���e�1�>��F�$	�=��{��6�>Ս.#	�=��E&�6�>�:\<�@��b�ɓ�t�w���#@�`�3I�@�c��@
   HXR_QD_V03      QUADgiC4�>x�k9i�=Tj���Ҟ�ύ�1Hi=�!j1�����<�5��N�GaM�컒��T�r�>���l���=��{z�}�1!Otx�|�D�w�Ͻ���s���㢋��-�>�v1]ٽ�a�{eL=��tO�D�=8=�$�;K{�vIW�>� G:��L�Y���2,u�w�� ��>���3�>	���Ws�;b��b�c3?�/��\�J<�����/=�:I�^?�mI��?�/!�I�#?�N�J�l?   X}1�>3�����k?   L2�"=�:I�^�_��6��P�g���!��N�J�l�   X}1�3�����k�   L2�"��-�Z?�mI��?�/!�I�#?��<���
?   ����>K���xb?   �"=giC4�>���T�r�>㢋��-�>K{�vIW�>� ��>b��b�c3?�����/=�8�ܻ��=����ZR�>��n���=	�f�2�>[��>	�=�ߞ�7�>[	�W=	�=���17�>����(C@��,�R��Œ�f#@�δ-W�@�c��@   HXR_COR      KICKERgiC4�>x�k9i�=Tj���Ҟ�ύ�1Hi=�!j1�����<�5��N�GaM�컒��T�r�>���l���=��{z�}�1!Otx�|�D�w�Ͻ���s���㢋��-�>�v1]ٽ�a�{eL=��tO�D�=8=�$�;K{�vIW�>� G:��L�Y���2,u�w�� ��>���3�>	���Ws�;b��b�c3?�/��\�J<�����/=�:I�^?�mI��?�/!�I�#?�N�J�l?   X}1�>3�����k?   L2�"=�:I�^�_��6��P�g���!��N�J�l�   X}1�3�����k�   L2�"��-�Z?�mI��?�/!�I�#?��<���
?   ����>K���xb?   �"=giC4�>���T�r�>㢋��-�>K{�vIW�>� ��>b��b�c3?�����/=�8�ܻ��=����ZR�>��n���=	�f�2�>[��>	�=�ߞ�7�>[	�W=	�=���17�>����(C@��,�R��Œ�f#@�δ-W�@�c��@   HXR_BPM      MONIgiC4�>x�k9i�=Tj���Ҟ�ύ�1Hi=�!j1�����<�5��N�GaM�컒��T�r�>���l���=��{z�}�1!Otx�|�D�w�Ͻ���s���㢋��-�>�v1]ٽ�a�{eL=��tO�D�=8=�$�;K{�vIW�>� G:��L�Y���2,u�w�� ��>���3�>	���Ws�;b��b�c3?�/��\�J<�����/=�:I�^?�mI��?�/!�I�#?�N�J�l?   X}1�>3�����k?   L2�"=�:I�^�_��6��P�g���!��N�J�l�   X}1�3�����k�   L2�"��-�Z?�mI��?�/!�I�#?��<���
?   ����>K���xb?   �"=giC4�>���T�r�>㢋��-�>K{�vIW�>� ��>b��b�c3?�����/=�8�ܻ��=����ZR�>��n���=	�f�2�>[��>	�=�ߞ�7�>[	�W=	�=���17�>����(C@��,�R��Œ�f#@�δ-W�@��ڨ�,@
   HXR_QD_V03      QUAD�����>F�Z)���=�/�D��������F�H�s*��3����{&�>���gJ����a��>���_`�=�ΐ�K�u�7��ğ"O=Վe)e����c��;�<^B�>��{�qͽ�&6��K=F�$��+�=k����;�Qg���>��&��A3��R����y����m�<q�$����>*gcH�>JT�Ws�;b��b�c3?>��\�J<�m���/=�R�5r?�þ�%?��YN�#?[?   `}1�>3�����k?   O2�"=�R�5r��þ�%��Z����!�[�   `}1�3�����k�   O2�"�|o��U0?
nuZ�?��YN�#?�8��?   ����>K���xb?   ��"=�����>�a��>�<^B�>�Qg���>$����>b��b�c3?�m���/=��q���=�l�R�>r�)�Ǵ�=&CVe_2�>k��Y	�=j�/7�>�S�fX	�=ڛUM-7�>�(�.`@�����ؿ��m|�F#@��i{-��?t����@   HXR_DR_QD_ED      DRIFD3'�CB�>Xջi���=A�?M����Z#E)�P�T�e\0��F�� �P���� C���a��>u�4�Ȓ=�ΐ�K�u��3깟"O=Վe)e����=�]��;�Ld��>]����ͽF.[pK=�˫֐=N��xH��;�Qg���>��ĨA3��R����y��� =q������>2�Rs�>m݅~Ws�;b��b�c3?���\�J<Yl��/=��`&�?�þ�%?J�m��O#?[?   x}1�>3�����k?   [2�"=��`&���þ�%�@1�[�!�[�   x}1�3�����k�   [2�"�졈�:�?
nuZ�?J�m��O#?�8��?   ����>K���xb?   ��"=D3'�CB�>�a��>�Ld��>�Qg���>�����>b��b�c3?Yl��/=��q���=�l�R�>s�)�Ǵ�='CVe_2�>k��Y	�=k�/7�>�S�fX	�=ܛUM-7�>��¦�@��HU�ڿߦїk�"@�,�O�?V�ŧ�U@	   HXR_DR_V4      DRIF�bvd��>�ڞ���=�X�Օ�H�bx珽��	����E����6ۿ���a��>��/3o=�ΐ�K�u���iy�"O=Վe)e����RF�;�;���>Ԃ��A������<=�o��I��LUH�8;�Qg���>H&�K�A3��R����y���v"=q�n����>��eX�>ܙ�Us�;b��b�c3?\b�P�J<����/=5V�ζ&?�þ�%?-��^Ι?[?   ��1�>3�����k?   �4�"=�=�9�*"��þ�%�-��^Ι�[�   ��1�3�����k�   �4�"�5V�ζ&?
nuZ�?�C����?�8��?   ����>K���xb?   Vծ"=�bvd��>�a��>�;���>�Qg���>n����>b��b�c3?����/=��q���=�l�R�>q�)�Ǵ�=%CVe_2�>k��Y	�=j�/7�>�S�fX	�=ڛUM-7�>�_��y @��r=���O[j5R�	@>JJn��?�;,��@	   HXR_DR_CT      DRIF��hW���>�u��}��=q�����:)7I�}��"�PT����.9�M/*��k��ټ��a��>���o��j=�ΐ�K�u�C�De�"O=Վe)e���O\���;��r��}�>V�	�ٱ����2lȼ��V�s'W���<����Qg���>n	��A3��R����y��.~�#=q��7o���>�����>���	Us�;b��b�c3?ٸ��O�J<��t��/=;~�*�)'?�þ�%?�uV� @?[?   ��1�>3�����k?   �4�"=H>��k�"��þ�%��uV� @�[�   ��1�3�����k�   �4�"�;~�*�)'?
nuZ�?��9��?�8��?   ����>K���xb?   �Ԯ"=��hW���>�a��>��r��}�>�Qg���>�7o���>b��b�c3?��t��/=��q���=�l�R�>r�)�Ǵ�=&CVe_2�>k��Y	�=j�/7�>�S�fX	�=ڛUM-7�>�/�.!@�,�WN1��)X�K�@������?�;,��@   HXR_WA_M_OU      WATCH��hW���>�u��}��=q�����:)7I�}��"�PT����.9�M/*��k��ټ��a��>���o��j=�ΐ�K�u�C�De�"O=Վe)e���O\���;��r��}�>V�	�ٱ����2lȼ��V�s'W���<����Qg���>n	��A3��R����y��.~�#=q��7o���>�����>���	Us�;b��b�c3?ٸ��O�J<��t��/=;~�*�)'?�þ�%?�uV� @?[?   ��1�>3�����k?   �4�"=H>��k�"��þ�%��uV� @�[�   ��1�3�����k�   �4�"�;~�*�)'?
nuZ�?��9��?�8��?   ����>K���xb?   �Ԯ"=��hW���>�a��>��r��}�>�Qg���>�7o���>b��b�c3?��t��/=��q���=�l�R�>r�)�Ǵ�=&CVe_2�>k��Y	�=j�/7�>�S�fX	�=ڛUM-7�>�/�.!@�,�WN1��)X�K�@������?$��5�@   HXR_DR_QD_ED      DRIF��s�/�>i����=Vq�I˗�z�^t�Ő�.�r���y/�G<���щ����a��>��z��h=�ΐ�K�u��bZ�"O=Վe)e���$��6�;�&hl
P�>���k��R���&�r�Vy\�[���I1��Qg���>��(��A3��R����y�2zPZ$=q�zi����>�2�ղ>�?O�Ts�;b��b�c3?�ȨbO�J<��'��/=E�xx�e'?�þ�%?�܏?[?   ҂1�>3�����k?   �4�"=��V�"��þ�%��܏�[�   ҂1�3�����k�   �4�"�E�xx�e'?
nuZ�?˧09l?�8��?   ����>K���xb?   :Ԯ"=��s�/�>�a��>�&hl
P�>�Qg���>zi����>b��b�c3?��'��/=��q���=�l�R�>s�)�Ǵ�='CVe_2�>k��Y	�=j�/7�>�S�fX	�=ڛUM-7�>ZX�>�v!@�6?܌��!�f�8@�Y���?~1Uj��@	   HXR_QD_FH      QUADr�H��#�>8�0nl9}�(A#����BQ�x�� ,�kꮽ�ּ��8���o.隬뻞�<�i��>��e���~=�T���C��zʂ��=�@&>�=lqZ}Y�;�@��uK�>(+��=���	��b'��]��/� �4��Ϥ`��>�T�3��CR��{�ˣw��wq�E4����>��~ز>`�u�Ts�;b��b�c3?��+LO�J<d�؍�/=�_z`�l'?�fQ�U�>{ƨ��?π�[\�?   Ԃ1�>3�����k?   �4�"=�$u �"��fQ�U�{ƨ���π�[\��   Ԃ1�3�����k�   �4�"��_z`�l'? Ix!�>E�i?����v��>   ����>K���xb?   $Ԯ"=r�H��#�>��<�i��>�@��uK�>�Ϥ`��>E4����>b��b�c3?d�؍�/=ŜQ�b��=#F�F�Q�>	xܫ9��=�����1�>���	�=��@�7�>��t�	�=�H���7�>r~��;�!@�gB�?7�[݄,@W1V����~1Uj��@   HXR_COR      KICKERr�H��#�>8�0nl9}�(A#����BQ�x�� ,�kꮽ�ּ��8���o.隬뻞�<�i��>��e���~=�T���C��zʂ��=�@&>�=lqZ}Y�;�@��uK�>(+��=���	��b'��]��/� �4��Ϥ`��>�T�3��CR��{�ˣw��wq�E4����>��~ز>`�u�Ts�;b��b�c3?��+LO�J<d�؍�/=�_z`�l'?�fQ�U�>{ƨ��?π�[\�?   Ԃ1�>3�����k?   �4�"=�$u �"��fQ�U�{ƨ���π�[\��   Ԃ1�3�����k�   �4�"��_z`�l'? Ix!�>E�i?����v��>   ����>K���xb?   $Ԯ"=r�H��#�>��<�i��>�@��uK�>�Ϥ`��>E4����>b��b�c3?d�؍�/=ŜQ�b��=#F�F�Q�>	xܫ9��=�����1�>���	�=��@�7�>��t�	�=�H���7�>r~��;�!@�gB�?7�[݄,@W1V����~1Uj��@   HXR_BPM      MONIr�H��#�>8�0nl9}�(A#����BQ�x�� ,�kꮽ�ּ��8���o.隬뻞�<�i��>��e���~=�T���C��zʂ��=�@&>�=lqZ}Y�;�@��uK�>(+��=���	��b'��]��/� �4��Ϥ`��>�T�3��CR��{�ˣw��wq�E4����>��~ز>`�u�Ts�;b��b�c3?��+LO�J<d�؍�/=�_z`�l'?�fQ�U�>{ƨ��?π�[\�?   Ԃ1�>3�����k?   �4�"=�$u �"��fQ�U�{ƨ���π�[\��   Ԃ1�3�����k�   �4�"��_z`�l'? Ix!�>E�i?����v��>   ����>K���xb?   $Ԯ"=r�H��#�>��<�i��>�@��uK�>�Ϥ`��>E4����>b��b�c3?d�؍�/=ŜQ�b��=#F�F�Q�>	xܫ9��=�����1�>���	�=��@�7�>��t�	�=�H���7�>r~��;�!@�gB�?7�[݄,@W1V����ؕ�I�@	   HXR_QD_FH      QUAD[+��>����dsн�`.��&���tɀV!����6��ʮ�8���%��4Y5��r�	�}��>3����Έ=��^5�w=ZX�r�=�?%
0�=黮�r�;.�"�%T�>��Tg�=��ӛ2����%J_�~/i_`T8�Pd���D�>�O����3�C�n#ӝ|�Z�e��q�������>?ز>J6��Ts�;b��b�c3?��3O�J<�E���/=3h��e'?0IxK�?,rT�?
/��] ?   ւ1�>3�����k?   �4�"=����"��P���,rT��
/��] �   ւ1�3�����k�   �4�"�3h��e'?0IxK�?Jޏ��t?z����>   ~���>K���xb?   
Ԯ"=[+��>r�	�}��>.�"�%T�>Pd���D�>������>b��b�c3?�E���/=^�ؕ���=5�iHQ�>fؠ���=��31�>[旮+
�=�(8�>�8@7*
�=T�D&8�>Af��v!@��U��?�t��@@<��W���@'~�A@   HXR_DR_QD_ED      DRIF���l>��>O���!9н���٘��$��љ�����#���,P���I�q����r�	�}��>#7_�n�=��^5�w=�%��r�=�?%
0�=��کr�;h���4��>���C�=+��'���Z)D��b�(@~���C�Pd���D�>yOy���3�C�n#ӝ|��YT�q�uw����>R�U�> ��Ts�;b��b�c3?,���N�J<���n�/=���''?0IxK�?#�R~n?
/��] ?   ؂1�>3�����k?   �4�"=cG+��v"��P���#�R~n�
/��] �   ؂1�3�����k�   �4�"����''?0IxK�?+W&P�?z����>   h���>K���xb?   �Ӯ"=���l>��>r�	�}��>h���4��>Pd���D�>uw����>b��b�c3?���n�/=`�ؕ���=7�iHQ�>fؠ���=��31�>Z旮+
�=�(8�>�8@7*
�=T�D&8�>��Z�!@$��S��?�����@�"�@��濈���@   HXR_DR_WIG_ED      DRIF�v��9�>�ПoM�ν�����-���x݉����&,籫��W���%����NF��r�	�}��>�&)�=��^5�w=����r�=�?%
0�=	;���r�;�E����>�F�b}�=A�F�a�8-��m��0���W�Pd���D�>�����3�C�n#ӝ|���8���q�ǒ~���>�:�2�>l��@Ts�;b��b�c3?��B�M�J<�+��/=�ie>&?0IxK�?+`�{�?
/��] ?   ނ1�>3�����k?   �4�"=��p��!��P���+`�{��
/��] �   ނ1�3�����k�   �4�"��ie>&?0IxK�?xܽ���?z����>   ���>K���xb?   Ү"=�v��9�>r�	�}��>�E����>Pd���D�>ǒ~���>b��b�c3?�+��/=_�ؕ���=6�iHQ�>fؠ���=��31�>[旮+
�=�(8�>�8@7*
�=T�D&8�>�q䇹�@��7�?~��y4@������gX�r1@	   HXR_WIG_0      WIGGLER�;(�> �9�	���;�l{��e	�c�����SU�h��� Z߽�
��ûr�	�}��>���XU�=�/OXw=���n�=�?%
0�="}3��r�;+.�}�1�>'���Ez�=�e�|�D���[=R�����u>���]�4�v��>�2"�(�3�Ȉ<eS|�a	��q�8}<��>�y��>�1K
Bs�;b��b�c3?1���*�J<k�z!x/=�sr6�?0IxK�?��u��"?Qy�Ɨ ?   H�1�>3�����k?   �0�"='�<��+��P�����u��"�Qy�Ɨ �   H�1�3�����k�   �0�"��sr6�?0IxK�?�2}}� ?�.H���>   l���>K���xb?   y��"=�;(�>r�	�}��>+.�}�1�>]�4�v��>8}<��>b��b�c3?k�z!x/=^�ؕ���=5�iHQ�>fؠ���=��31�>[旮+
�=�(8�>�8@7*
�=T�D&8�>�!�qB�@�����?��â�@k�*����9���@   HXR_DR_WIG_ED      DRIF�mk&*�>�����L��=�����������lZ��x�166�	ٽ�w���r�	�}��>�h�\x�=�/OXw=T6�n�=�?%
0�=e?��r�;d�����>�'^R7�=���٬F��]��������v�3L��]�4�v��>8ޡ�+�3�Ȉ<eS|�5V����q���B9��>&G�
�>����As�;b��b�c3?��0c)�J<9�w/=�͐P�0?0IxK�?�@�p�a#?Qy�Ɨ ?   L�1�>3�����k?   �0�"=	1�P2��P����@�p�a#�Qy�Ɨ �   L�1�3�����k�   �0�"��͐P�0?0IxK�?K&O�v!?�.H���>   ���>K���xb?   ���"=�mk&*�>r�	�}��>d�����>]�4�v��>��B9��>b��b�c3?9�w/=^�ؕ���=5�iHQ�>fؠ���=��31�>_旮+
�=�(8�>�8@7*
�=$T�D&8�>�91?
@/�_���?��l� @����B����@   HXR_DR_QD_ED	      DRIF�aT����>O�:�4���de{���vh���z\\nprs�b��L�Z׽/���e��r�	�}��>Pf]�ƙ=�/OXw=����n�=�?%
0�=G6S��r�;zO�{
�>L�)�Ur�=��[V�/G�Bf�L���e���A���]�4�v��>�cKY,�3�Ȉ<eS|�Ǥ�^��q���c8��>i_�>	v�hAs�;b��b�c3?�1�)�J<y ɲw/=��e5r�?0IxK�?)���H�#?Qy�Ɨ ?   N�1�>3�����k?   �0�"=𳲷E���P���)���H�#�Qy�Ɨ �   N�1�3�����k�   �0�"���e5r�?0IxK�?#�^��!?�.H���>   ���>K���xb?   ���"=�aT����>r�	�}��>zO�{
�>]�4�v��>��c8��>b��b�c3?y ɲw/=^�ؕ���=5�iHQ�>fؠ���=��31�>Z旮+
�=�(8�>�8@7*
�=T�D&8�>��%���	@F�|nO�?Q1��
!@&��5\���q/*�n%@	   HXR_QD_DH      QUAD��s0���>P�1�sL��UX�$Je��\�g��~��ؗ)�*r���S���ֽ�v��@��ms����>��V��=3���F����(�=�!�
��=
�F:���;���.t�>�0��#�=*��HG�w`�Xt#��h��ׄ����@B�>	��?L*%����
Fl�k��e�b�wJ8��>�;�#�>�4�`As�;b��b�c3?�~��(�J<���w/=���$Ɲ?:}h�4h�>SQ�#?
�!�ڻ�>   N�1�>3�����k?   �0�"=J:[����1��@��SQ�#�
�!�ڻ��   N�1�3�����k�   �0�"����$Ɲ?:}h�4h�>4���!?5ڜG�>   ����>K���xb?   t��"=��s0���>ms����>���.t�>���@B�>wJ8��>b��b�c3?���w/=��Q�N��=�M��Q�>�e�!'��=O�В�1�>Z7	�=��7�>�$B�5	�=oJ�[7�>����	@|����}�?@�h��!@��M�C	��q/*�n%@   HXR_COR      KICKER��s0���>P�1�sL��UX�$Je��\�g��~��ؗ)�*r���S���ֽ�v��@��ms����>��V��=3���F����(�=�!�
��=
�F:���;���.t�>�0��#�=*��HG�w`�Xt#��h��ׄ����@B�>	��?L*%����
Fl�k��e�b�wJ8��>�;�#�>�4�`As�;b��b�c3?�~��(�J<���w/=���$Ɲ?:}h�4h�>SQ�#?
�!�ڻ�>   N�1�>3�����k?   �0�"=J:[����1��@��SQ�#�
�!�ڻ��   N�1�3�����k�   �0�"����$Ɲ?:}h�4h�>4���!?5ڜG�>   ����>K���xb?   t��"=��s0���>ms����>���.t�>���@B�>wJ8��>b��b�c3?���w/=��Q�N��=�M��Q�>�e�!'��=O�В�1�>Z7	�=��7�>�$B�5	�=oJ�[7�>����	@|����}�?@�h��!@��M�C	��q/*�n%@   HXR_BPM      MONI��s0���>P�1�sL��UX�$Je��\�g��~��ؗ)�*r���S���ֽ�v��@��ms����>��V��=3���F����(�=�!�
��=
�F:���;���.t�>�0��#�=*��HG�w`�Xt#��h��ׄ����@B�>	��?L*%����
Fl�k��e�b�wJ8��>�;�#�>�4�`As�;b��b�c3?�~��(�J<���w/=���$Ɲ?:}h�4h�>SQ�#?
�!�ڻ�>   N�1�>3�����k?   �0�"=J:[����1��@��SQ�#�
�!�ڻ��   N�1�3�����k�   �0�"����$Ɲ?:}h�4h�>4���!?5ڜG�>   ����>K���xb?   t��"=��s0���>ms����>���.t�>���@B�>wJ8��>b��b�c3?���w/=��Q�N��=�M��Q�>�e�!'��=O�В�1�>Z7	�=��7�>�$B�5	�=oJ�[7�>����	@|����}�?@�h��!@��M�C	��˓e¾2@	   HXR_QD_DH      QUAD�`����>i���ڴ=@X%9���D$Jh��,LY��p����_a�ֽ����K����9�m�>ᛖ5�?�=��ʰx�u�Bց_�M�=M�j�=�$�j���;2�`]�>N'V�0˽y����RG��{�L)��<�����������g�>̢��!���8�bn�{�<��әd5�R��7��>��%�>\
WAs�;b��b�c3?#=�(�J<��ʦw/=��c�ė?� r~g�>�i���#?H���n?   J�1�>3�����k?   �0�"=ޑ怏��� r~g���i���#��y��Uz �   J�1�3�����k�   �0�"���c�ė?��(e��>tѝ!?H���n?   ����>K���xb?   ]��"=�`����>��9�m�>2�`]�>����g�>R��7��>b��b�c3?��ʦw/="�ͤ���=y�m2kR�>������=-Cք32�>F�x$D�=1�>~�5�>���B�=�N���5�>_�V�b�	@���=���I_�w!@i�
M��?3%S?�g@   HXR_DR_QD_ED
      DRIFW-���>�} Lz��=e������yp�ڇj���v2,h��Q(�iս /Tʝ����9�m�>�Q��$��=��ʰx�u�w�-4�M�=M�j�=[.�7���;�(oI���>i���x�ʽ'�]��\G���)��n���鄻����g�>�rY����8�bn�{�<���d5���2��>t�'�>h�=)As�;b��b�c3?�u�(�J<W�I�w/=��K�?� r~g�>1�^t�G#?H���n?   H�1�>3�����k?   �0�"=)P���� r~g��1�^t�G#��y��Uz �   H�1�3�����k�   �0�"���K�?��(e��>/<���k!?H���n?   ����>K���xb?   ���"=W-���>��9�m�>�(oI���>����g�>��2��>b��b�c3?W�I�w/="�ͤ���=y�m2kR�>������=-Cք32�>E�x$D�=0�>~�5�>���B�=�N���5�>"㑡�
@N�_F��m�J�	� @{�EYQ2�?{�S�/@   HXR_DR_WIG_ED      DRIF�޾up��>�E�lΩ�=-���I��e�鋣wq��ӌR~\X=������н��rە;��9�m�>�sI�I֍=��ʰx�u�Vݴ��M�=M�j�=���w���;n^p�C�>*�2��pɽ{=g�
�G��N�i�'��Ý�;������g�>�L<���8�bn�{�<	�a�d5��<��>E/�>j��}@s�;b��b�c3?W�'�J<֝6$w/=�Y� ��?� r~g�>(��a"?H���n?   :�1�>3�����k?   �0�"=�Y� ���� r~g��(��a"��y��Uz �   :�1�3�����k�   �0�"�4�y��?��(e��>�*%�e� ?H���n?   ����>K���xb?   ���"=�޾up��>��9�m�>n^p�C�>����g�>�<��>b��b�c3?֝6$w/="�ͤ���=y�m2kR�>������=-Cք32�>G�x$D�=2�>~�5�>���B�=�N���5�>�V��@�g,�&�y>H_@6�#����?-��u!#@	   HXR_WIG_0      WIGGLER��xE�S�>DB��=ji�N�s���aui�����}��\�=v"����=�CĖ �;��9�m�>!}h�Fu=���^��u�Q���M�=M�j�=@�����;�����>�v2�ߢ��Eyʶ�H�=n�IB ��W?-���L�Q��>�@�J�L�i	[�#=��F	>c0�xp>���>�_^��>�nMG.s�;b��b�c3?�O��J<��/o/=��M�� ?� r~g�>n[��?��q���?   �}1�>3�����k?   �,�"=��M�� �� r~g��Hf;L��Z�d�� �   �}1�3�����k�   �,�"�2�]a̞?��(e��>n[��?��q���?   ���>K���xb?   ���"=��xE�S�>��9�m�>�����>L�Q��>xp>���>b��b�c3?��/o/="�ͤ���=y�m2kR�>������=-Cք32�>F�x$D�=1�>~�5�>���B�=�N���5�>�Q�`R~@���,����2.��@�wʚ�?���#@   HXR_DR_WIG_ED      DRIFL,�;��>L�R9z�=q��OR��f�O�΍�;&	3��=�tN��=Zx���(�;��9�m�>��e�(q=���^��u���a�M�=M�j�=��X���;>:��>��%÷�� MTE�H� �*�������0��L�Q��>rܓ�L�i	[�#=i�sq5c0���e|��>�>���-s�;b��b�c3?�G���J<����n/=�����%!?� r~g�>�PmE��?��q���?   �}1�>3�����k?   �,�"=�����%!�� r~g��b�����Z�d�� �   �}1�3�����k�   �,�"�S�/ߣ�?��(e��>�PmE��?��q���?   ����>K���xb?   ���"=L,�;��>��9�m�>>:��>L�Q��>��e|��>b��b�c3?����n/=#�ͤ���={�m2kR�>������=.Cք32�>F�x$D�=1�>~�5�>���B�=�N���5�>0� =ȷ@%��_t��iA��|@��wR�?��a>��#@   HXR_DR_QD_ED      DRIF���{�>\Y�4���=��R�㕽�s&F`��n�O?N=�=����;U�=������;��9�m�>�U���o=���^��u��e	��M�=M�j�=Y��%���;���di�>y�V�����,���H����`����܍��6��L�Q��>�y+�L�i	[�#=P�WA3c0��Z�v��>V�_Ă>²m-s�;b��b�c3?n�L.�J<zGB�n/=ҳ82oB!?� r~g�>�ǷP�?��q���?   �}1�>3�����k?   �,�"=ҳ82oB!�� r~g��1#����Z�d�� �   �}1�3�����k�   �,�"�6��f* ?��(e��>�ǷP�?��q���?   ���>K���xb?   <��"=���{�>��9�m�>���di�>L�Q��>�Z�v��>b��b�c3?zGB�n/=!�ͤ���=x�m2kR�>������=,Cք32�>F�x$D�=1�>~�5�>���B�=�N���5�>I���U@�Xe�����]����
@51`p*��?���-,�#@	   HXR_QD_FH      QUADY��B�$�>�s���wv�.���_	��/OU������N4\�=fY�K��=7��H��;,�(̐�>��G{k�=�*N[}L��*>���=_�:z�z�=�(����;G �>b�>w�L��`�n�o���H�>K����������?��)=2�}�>�afX�%�L7!�;�j�-��[lbc���x��>�\ʂ>lT�h-s�;b��b�c3?����J<O���n/=ϼ%��C!?g�t�h��> x7��}?�$n�y�>   �}1�>3�����k?   �,�"=ϼ%��C!�2�)�r������$n�y��   �}1�3�����k�   �,�"���Zl ?g�t�h��> x7��}?�	+dK�>   ���>K���xb?   &��"=Y��B�$�>,�(̐�>G �>b�>)=2�}�>��x��>b��b�c3?O���n/=�����=��o�PQ�>�)j����=���t1�>����H�=4�o&�5�>K\|vG�=�?�n�5�>14���i@x��p�?'�u�`�
@��F~��?���-,�#@	   HXR_WA_OU      WATCHY��B�$�>�s���wv�.���_	��/OU������N4\�=fY�K��=7��H��;,�(̐�>��G{k�=�*N[}L��*>���=_�:z�z�=�(����;G �>b�>w�L��`�n�o���H�>K����������?��)=2�}�>�afX�%�L7!�;�j�-��[lbc���x��>�\ʂ>lT�h-s�;b��b�c3?����J<O���n/=ϼ%��C!?g�t�h��> x7��}?�$n�y�>   �}1�>3�����k?   �,�"=ϼ%��C!�2�)�r������$n�y��   �}1�3�����k�   �,�"���Zl ?g�t�h��> x7��}?�	+dK�>   ���>K���xb?   &��"=Y��B�$�>,�(̐�>G �>b�>)=2�}�>��x��>b��b�c3?O���n/=�����=��o�PQ�>�)j����=���t1�>����H�=4�o&�5�>K\|vG�=�?�n�5�>14���i@x��p�?'�u�`�
@��F~��?