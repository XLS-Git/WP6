SDDS1
!# little-endian
&description text="sigma matrix--input: xls_linac_tmc_sxr_maxe_tmc.track.ele  lattice: xls_linac_tmc_sxr_maxe.11.lte", contents="sigma matrix", &end
&parameter name=Step, description="Simulation step", type=long, &end
&parameter name=SVNVersion, description="SVN version number", type=string, fixed_value=unknown, &end
&column name=s, units=m, description=Distance, type=double,  &end
&column name=ElementName, description="Element name", format_string=%10s, type=string,  &end
&column name=ElementOccurence, description="Occurence of element", format_string=%6ld, type=long,  &end
&column name=ElementType, description="Element-type name", format_string=%10s, type=string,  &end
&column name=s1, symbol="$gs$r$b1$n", units=m, description="sqrt(<x*x>)", type=double,  &end
&column name=s12, symbol="$gs$r$b12$n", units=m, description="<x*xp'>", type=double,  &end
&column name=s13, symbol="$gs$r$b13$n", units="m$a2$n", description="<x*y>", type=double,  &end
&column name=s14, symbol="$gs$r$b14$n", units=m, description="<x*y'>", type=double,  &end
&column name=s15, symbol="$gs$r$b15$n", units="m$a2$n", description="<x*s>", type=double,  &end
&column name=s16, symbol="$gs$r$b16$n", units=m, description="<x*delta>", type=double,  &end
&column name=s17, symbol="$gs$r$b17$n", units="m*s", description="<x*t>", type=double,  &end
&column name=s2, symbol="$gs$r$b2$n", description="sqrt(<x'*x'>)", type=double,  &end
&column name=s23, symbol="$gs$r$b23$n", units=m, description="<x'*y>", type=double,  &end
&column name=s24, symbol="$gs$r$b24$n", description="<x'*y'>", type=double,  &end
&column name=s25, symbol="$gs$r$b25$n", units=m, description="<x'*s>", type=double,  &end
&column name=s26, symbol="$gs$r$b26$n", description="<x'*delta>", type=double,  &end
&column name=s27, symbol="$gs$r$b27$n", units=s, description="<x'*t>", type=double,  &end
&column name=s3, symbol="$gs$r$b3$n", units=m, description="sqrt(<y*y>)", type=double,  &end
&column name=s34, symbol="$gs$r$b34$n", units=m, description="<y*y'>", type=double,  &end
&column name=s35, symbol="$gs$r$b35$n", units="m$a2$n", description="<y*s>", type=double,  &end
&column name=s36, symbol="$gs$r$b36$n", units=m, description="<y*delta>", type=double,  &end
&column name=s37, symbol="$gs$r$b37$n", units="m*s", description="<y*t>", type=double,  &end
&column name=s4, symbol="$gs$r$b4$n", description="sqrt(<y'*y')>", type=double,  &end
&column name=s45, symbol="$gs$r$b45$n", units=m, description="<y'*s>", type=double,  &end
&column name=s46, symbol="$gs$r$b46$n", description="<y'*delta>", type=double,  &end
&column name=s47, symbol="$gs$r$b47$n", units=s, description="<y'*t>", type=double,  &end
&column name=s5, symbol="$gs$r$b5$n", units=m, description="sqrt(<s*s>)", type=double,  &end
&column name=s56, symbol="$gs$r$b56$n", units=m, description="<s*delta>", type=double,  &end
&column name=s57, symbol="$gs$r$b57$n", units="m*s", description="<s*t>", type=double,  &end
&column name=s6, symbol="$gs$r$b6$n", description="sqrt(<delta*delta>)", type=double,  &end
&column name=s67, symbol="$gs$r$b67$n", units=s, description="<delta*t>", type=double,  &end
&column name=s7, symbol="$gs$r$b7$n", description="sqrt(<t*t>)", type=double,  &end
&column name=ma1, symbol="max$sb$ex$sb$e", units=m, description="maximum absolute value of x", type=double,  &end
&column name=ma2, symbol="max$sb$ex'$sb$e", description="maximum absolute value of x'", type=double,  &end
&column name=ma3, symbol="max$sb$ey$sb$e", units=m, description="maximum absolute value of y", type=double,  &end
&column name=ma4, symbol="max$sb$ey'$sb$e", description="maximum absolute value of y'", type=double,  &end
&column name=ma5, symbol="max$sb$e$gD$rs$sb$e", units=m, description="maximum absolute deviation of s", type=double,  &end
&column name=ma6, symbol="max$sb$e$gd$r$sb$e", description="maximum absolute value of delta", type=double,  &end
&column name=ma7, symbol="max$sb$e$gD$rt$sb$e", units=s, description="maximum absolute deviation of t", type=double,  &end
&column name=minimum1, symbol="x$bmin$n", units=m, type=double,  &end
&column name=minimum2, symbol="x'$bmin$n", units=m, type=double,  &end
&column name=minimum3, symbol="y$bmin$n", units=m, type=double,  &end
&column name=minimum4, symbol="y'$bmin$n", units=m, type=double,  &end
&column name=minimum5, symbol="$gD$rs$bmin$n", units=m, type=double,  &end
&column name=minimum6, symbol="$gd$r$bmin$n", units=m, type=double,  &end
&column name=minimum7, symbol="t$bmin$n", units=s, type=double,  &end
&column name=maximum1, symbol="x$bmax$n", units=m, type=double,  &end
&column name=maximum2, symbol="x'$bmax$n", units=m, type=double,  &end
&column name=maximum3, symbol="y$bmax$n", units=m, type=double,  &end
&column name=maximum4, symbol="y'$bmax$n", units=m, type=double,  &end
&column name=maximum5, symbol="$gD$rs$bmax$n", units=m, type=double,  &end
&column name=maximum6, symbol="$gd$r$bmax$n", units=m, type=double,  &end
&column name=maximum7, symbol="t$bmax$n", units=s, type=double,  &end
&column name=Sx, symbol="$gs$r$bx$n", units=m, description=sqrt(<(x-<x>)^2>), type=double,  &end
&column name=Sxp, symbol="$gs$r$bx'$n", description=sqrt(<(x'-<x'>)^2>), type=double,  &end
&column name=Sy, symbol="$gs$r$by$n", units=m, description=sqrt(<(y-<y>)^2>), type=double,  &end
&column name=Syp, symbol="$gs$r$by'$n", description=sqrt(<(y'-<y'>)^2>), type=double,  &end
&column name=Ss, symbol="$gs$r$bs$n", units=m, description=sqrt(<(s-<s>)^2>), type=double,  &end
&column name=Sdelta, symbol="$gs$bd$n$r", description=sqrt(<(delta-<delta>)^2>), type=double,  &end
&column name=St, symbol="$gs$r$bt$n", units=s, description=sqrt(<(t-<t>)^2>), type=double,  &end
&column name=ex, symbol="$ge$r$bx$n", units=m, description="geometric horizontal emittance", type=double,  &end
&column name=enx, symbol="$ge$r$bx,n$n", units=m, description="normalized horizontal emittance", type=double,  &end
&column name=ecx, symbol="$ge$r$bx,c$n", units=m, description="geometric horizontal emittance less dispersive contributions", type=double,  &end
&column name=ecnx, symbol="$ge$r$bx,cn$n", units=m, description="normalized horizontal emittance less dispersive contributions", type=double,  &end
&column name=ey, symbol="$ge$r$by$n", units=m, description="geometric vertical emittance", type=double,  &end
&column name=eny, symbol="$ge$r$by,n$n", units=m, description="normalized vertical emittance", type=double,  &end
&column name=ecy, symbol="$ge$r$by,c$n", units=m, description="geometric vertical emittance less dispersive contributions", type=double,  &end
&column name=ecny, symbol="$ge$r$by,cn$n", units=m, description="normalized vertical emittance less dispersive contributions", type=double,  &end
&column name=betaxBeam, symbol="$gb$r$bx,beam$n", units=m, description="betax for the beam, excluding dispersive contributions", type=double,  &end
&column name=alphaxBeam, symbol="$ga$r$bx,beam$n", description="alphax for the beam, excluding dispersive contributions", type=double,  &end
&column name=betayBeam, symbol="$gb$r$by,beam$n", units=m, description="betay for the beam, excluding dispersive contributions", type=double,  &end
&column name=alphayBeam, symbol="$ga$r$by,beam$n", description="alphay for the beam, excluding dispersive contributions", type=double,  &end
&data mode=binary, &end
&                 _BEG_      MARKe�ަ�[�>��xɿ��=8���ä��~<(ŭ���/&�<��=��|�Z
>O�F'�<��;�>��������.6��U���9�Q���=ѹ.Q��=(�k�;_N��w7�>if�i�=����%=LM����f=L�����R;��ʐ'�>��֔!E=N>��%�=ʰ�ނ;8��s��>�G��>��V6�u�;��A�c3?������J<*k�{0=�[���/?�����?׶��?�u��	8?   ��1�>�����k?   ���"=���.��������sQ�T?����=Sl�   ��1御����k�   ���"��[���/?�6?m��?׶��?�u��	8?   ����>l����xb?   ��"=e�ަ�[�>��;�>_N��w7�>��ʐ'�>8��s��>��A�c3?*k�{0=�vR���=�f�m�M�>�����=K[�ng-�>SB)�5�=?g`	E2�>�Gl%4�=��3C2�>�q	��3@���k
  �Gs�H��?)O��=�ο           BNCH      CHARGEe�ަ�[�>��xɿ��=8���ä��~<(ŭ���/&�<��=��|�Z
>O�F'�<��;�>��������.6��U���9�Q���=ѹ.Q��=(�k�;_N��w7�>if�i�=����%=LM����f=L�����R;��ʐ'�>��֔!E=N>��%�=ʰ�ނ;8��s��>�G��>��V6�u�;��A�c3?������J<*k�{0=�[���/?�����?׶��?�u��	8?   ��1�>�����k?   ���"=���.��������sQ�T?����=Sl�   ��1御����k�   ���"��[���/?�6?m��?׶��?�u��	8?   ����>l����xb?   ��"=e�ަ�[�>��;�>_N��w7�>��ʐ'�>8��s��>��A�c3?*k�{0=�vR���=�f�m�M�>�����=K[�ng-�>SB)�5�=?g`	E2�>�Gl%4�=��3C2�>�q	��3@���k
  �Gs�H��?)O��=�ο��C\��?	   TMC_DR_V1      DRIF��K���>��i�=��
r$뫽3�E`��>�!*�l�=9���">��o .<��;�> ��9�
���.6��U��)I����=ѹ.Q��=����k�;S !��Z�>ݸPڲ=��\�x+=�=��E[w=�T^O�h;��ʐ'�>���'E=N>��%�=彺U�ނ;��a���>N?F��>$R���u�;��A�c3?��΍�J<� @�|0=|7����0?�����?׎,V��?�u��	8?   ��1�>�����k?   ���"=�::���0���������I	(S����=Sl�   ��1御����k�   ���"�|7����0?�6?m��?׎,V��?�u��	8?   7���>l����xb?   �"=��K���>��;�>S !��Z�>��ʐ'�>��a���>��A�c3?� @�|0=*�vR���=
g�m�M�>Ū���=k[�ng-�>SB)�5�=?g`	E2�>�Gl%4�=��3C2�>���'<+7@��ӂaP!��|�R��?�97���N3��?   TMC_DR_QD_ED      DRIF��'%
�>�28��=���e���F��Q����{�1��=㻱*tq>ߖ�0�u<��;�>'u������.6��U���l�����=ѹ.Q��=*��N�k�;�X
�x��>}�r�M�=ڨ9e/=φ+��y=�dZ��k;��ʐ'�>5��G)E=N>��%�=	��}�ނ;�����>?߷�>U]���u�;��A�c3?/C���J<�R��|0=���t
1?�����?s`gH)	?�u��	8?   ��1�>�����k?   ���"=���t
1���������d�����=Sl�   ��1御����k�   ���"���Vm�1?�6?m��?s`gH)	?�u��	8?   ����>l����xb?   ��"=��'%
�>��;�>�X
�x��>��ʐ'�>�����>��A�c3?�R��|0=�vR���=�f�m�M�>�����=K[�ng-�>SB)�5�=?g`	E2�>�Gl%4�=��3C2�>�܊x �7@>Դ�!��q��d�?��̊P�㿛Ü�9�?
   TMC_QD_V01      QUAD��U�[�>T�]s0d�=1�6�A"��� ��������%{���=l�/�f�>�6Q��<÷@�p�>b�/=����
�^Fӊ���V$�=O��S�X�=��m��;�Cϳ��>�R�Խ=F��G1=g�ɲ�m|=(����n;l��Wa��>o�����F=���!�o�=?��`�;��Q��>� d��>2S��u�;��A�c3?yK���J<wt��|0=!0D��<1?l���q6	?S[|^t	?{U��??   ��1�>�����k?   ���"=!0D��<1�l���q6	�Ψ��%�����   ��1御����k�   ���"�oef31?.`Gw�?S[|^t	?{U��??   ����>l����xb?   �"=��U�[�>÷@�p�>�Cϳ��>l��Wa��>��Q��>��A�c3?wt��|0=�?=���=�-��O�>Ҵ��i��=���E�/�>k��'O�=�{@c2�>�ޛM�=)��ja2�>�0
%i8@9��5� [��L�?�6[
쿛Ü�9�?   TMC_COR      KICKER��U�[�>T�]s0d�=1�6�A"��� ��������%{���=l�/�f�>�6Q��<÷@�p�>b�/=����
�^Fӊ���V$�=O��S�X�=��m��;�Cϳ��>�R�Խ=F��G1=g�ɲ�m|=(����n;l��Wa��>o�����F=���!�o�=?��`�;��Q��>� d��>2S��u�;��A�c3?yK���J<wt��|0=!0D��<1?l���q6	?S[|^t	?{U��??   ��1�>�����k?   ���"=!0D��<1�l���q6	�Ψ��%�����   ��1御����k�   ���"�oef31?.`Gw�?S[|^t	?{U��??   ����>l����xb?   �"=��U�[�>÷@�p�>�Cϳ��>l��Wa��>��Q��>��A�c3?wt��|0=�?=���=�-��O�>Ҵ��i��=���E�/�>k��'O�=�{@c2�>�ޛM�=)��ja2�>�0
%i8@9��5� [��L�?�6[
쿛Ü�9�?   TMC_BPM      MONI��U�[�>T�]s0d�=1�6�A"��� ��������%{���=l�/�f�>�6Q��<÷@�p�>b�/=����
�^Fӊ���V$�=O��S�X�=��m��;�Cϳ��>�R�Խ=F��G1=g�ɲ�m|=(����n;l��Wa��>o�����F=���!�o�=?��`�;��Q��>� d��>2S��u�;��A�c3?yK���J<wt��|0=!0D��<1?l���q6	?S[|^t	?{U��??   ��1�>�����k?   ���"=!0D��<1�l���q6	�Ψ��%�����   ��1御����k�   ���"�oef31?.`Gw�?S[|^t	?{U��??   ����>l����xb?   �"=��U�[�>÷@�p�>�Cϳ��>l��Wa��>��Q��>��A�c3?wt��|0=�?=���=�-��O�>Ҵ��i��=���E�/�>k��'O�=�{@c2�>�ޛM�=)��ja2�>�0
%i8@9��5� [��L�?�6[
�*��땪�?
   TMC_QD_V01      QUAD�d3eDm�>­���/Ž����]���^%$洽�o��@��=��X�	�>[k�U�<�<��˽>��ɖ�7l=��A'�\=��މ�J��P�ͽ����(ǻ.g�<��>��ω���=�-ɚ,3=�����-=���)q;E<���>��!L�H=�ś�)��=H'/�j�;�1���>Ur�>4�B�u�;��A�c3?��=��J< �׏|0=-,z�J1?L-�rM$�>/֬�
?�_�r?   	�1�>�����k?   �"=-,z�J1�L-�rM$�p�������_&�x��   	�1御����k�   �"��ۭq =1?�sC:�>/֬�
?�_�r?   j���>l����xb?   �߲"=�d3eDm�>�<��˽>.g�<��>E<���>�1���>��A�c3? �׏|0=�)��m��=��? �Q�>�v�`G��=_�b��1�>���Yh�=K��%�2�>Hfv�f�=쿮O2�>24~F�8@��ё���?!
.&?s�?�9Rb�����b�?   TMC_DR_QD_ED      DRIF�)�� ^�>b�I�Ža�K7�����9ᴽ���+���=ȴi�H�>4�{!�<�<��˽>��ܹp�l=��A'�\=Y�2�މ�J��P�ͽd�5��(ǻ�������>��Ӝj�=�)Pz�D5=���p �=��?)�	s;E<���>b��O�H=�ś�)��=|��gn�;�I���>��+�>�x���u�;��A�c3?�֬��J<sz�O|0=��O��F1?L-�rM$�>徏��
?�_�r?   7�1�>�����k?   ��"=��O��F1�L-�rM$꾭�5x���_&�x��   7�1御����k�   ��"����=41?�sC:�>徏��
?�_�r?   4���>l����xb?   *߲"=�)�� ^�>�<��˽>�������>E<���>�I���>��A�c3?sz�O|0=�)��m��=��? �Q�>�v�`G��=b�b��1�>���Yh�=O��%�2�>Kfv�f�=�O2�>�{��j8@Z������?��F�i�?�f���!���F�[��	@	   TMC_DR_V2      DRIF0��ܶ��>�h7$��]�4-�ѽ�F�잳�_���!D�= �;�|>8 ,����;�<��˽>���L�F�=��A'�\=r���{މ�J��P�ͽ�@�W�(ǻ�Q�Ea�>�q�Od��=�(�Od=$�!"��=}�~��.�;E<���>���U�H=�ś�)��=��T�;�<yV��>d���>j����u�;��A�c3?�ғ�g�J<��ok0=\�gg�0?L-�rM$�>���1?�_�r?   ��1�>�����k?   Й�"=\�gg�0�L-�rM$�>��V��+��_&�x��   ��1御����k�   Й�"�̚(6��-?�sC:�>���1?�_�r?   ����>l����xb?   ��"=0��ܶ��>�<��˽>�Q�Ea�>E<���>�<yV��>��A�c3?��ok0=�)��m��=��? �Q�>�v�`G��=`�b��1�>���Yh�=e��%�2�>^fv�f�=��O2�>�"�4�2@v�?7:�?�e��;@8�$��8�f����1
@   TMC_DR_QD_ED      DRIF�d����>��rY����ù��ѽ.�%�����>�2�=(Ib;i>9\aK��;�<��˽>yQmսl�=��A'�\=���{މ�J��P�ͽC��(ǻ �Kp��>r8ĘC��=2����d=�kj�=��;��j�;E<���>o�4iY�H=�ś�)��=�i��W�;�)�#��>���^�>�W@P�u�;��A�c3?�cg�J<��~/k0=�糀K0?L-�rM$�>ESײ$2?�_�r?   ��1�>�����k?   ��"=�糀K0�L-�rM$��AaaP,��_&�x��   ��1御����k�   ��"��2"r��-?�sC:�>ESײ$2?�_�r?   v���>l����xb?   ���"=�d����>�<��˽> �Kp��>E<���>�)�#��>��A�c3?��~/k0=�)��m��=��? �Q�>�v�`G��=`�b��1�>���Yh�=@��%�2�>?fv�f�=⿮O2�>�s�Ȼ�2@�s�D�?ZLmz�;@�x�=�����
@
   TMC_QD_V02      QUADZ�AOX��>����\�=�[�OHҽ�^�G�=���(	E�=� �Z�>A��_h��;7�L��>�@��y��;)0c>�=c$�]��=Q�$*[�=X}�ݽ�;�60��>΍�J�;۽0�˶�d=�:PLP��=�D���}�;��ʨ��>kaƟ�w0�#-�C::y�m��\{m�D P,��>���>�N�u�;��A�c3?+�9�f�J<I	&k0=t>�<70?`���8%?�p/Ɠ82?V���u��>   ��1�>�����k?   ���"=t>�<70�`���8%��g��h,����}��   ��1御����k�   ���"���g�-?&��FY?�p/Ɠ82?V���u��>   `���>l����xb?   P��"=Z�AOX��>7�L��>�60��>��ʨ��>D P,��>��A�c3?I	&k0=v^��@��=B��P�>�.��=�D�\0�>l��}	�=��F)�6�>��U�	�=��>P�6�>�e�	�2@?	��V��������;@�*�	@���
@   TMC_COR      KICKERZ�AOX��>����\�=�[�OHҽ�^�G�=���(	E�=� �Z�>A��_h��;7�L��>�@��y��;)0c>�=c$�]��=Q�$*[�=X}�ݽ�;�60��>΍�J�;۽0�˶�d=�:PLP��=�D���}�;��ʨ��>kaƟ�w0�#-�C::y�m��\{m�D P,��>���>�N�u�;��A�c3?+�9�f�J<I	&k0=t>�<70?`���8%?�p/Ɠ82?V���u��>   ��1�>�����k?   ���"=t>�<70�`���8%��g��h,����}��   ��1御����k�   ���"���g�-?&��FY?�p/Ɠ82?V���u��>   `���>l����xb?   P��"=Z�AOX��>7�L��>�60��>��ʨ��>D P,��>��A�c3?I	&k0=v^��@��=B��P�>�.��=�D�\0�>l��}	�=��F)�6�>��U�	�=��>P�6�>�e�	�2@?	��V��������;@�*�	@���
@   TMC_BPM      MONIZ�AOX��>����\�=�[�OHҽ�^�G�=���(	E�=� �Z�>A��_h��;7�L��>�@��y��;)0c>�=c$�]��=Q�$*[�=X}�ݽ�;�60��>΍�J�;۽0�˶�d=�:PLP��=�D���}�;��ʨ��>kaƟ�w0�#-�C::y�m��\{m�D P,��>���>�N�u�;��A�c3?+�9�f�J<I	&k0=t>�<70?`���8%?�p/Ɠ82?V���u��>   ��1�>�����k?   ���"=t>�<70�`���8%��g��h,����}��   ��1御����k�   ���"���g�-?&��FY?�p/Ɠ82?V���u��>   `���>l����xb?   P��"=Z�AOX��>7�L��>�60��>��ʨ��>D P,��>��A�c3?I	&k0=v^��@��=B��P�>�.��=�D�\0�>l��}	�=��F)�6�>��U�	�=��>P�6�>�e�	�2@?	��V��������;@�*�	@
[9���
@
   TMC_QD_V02      QUADmzcrER�>�%��0>��P�?ҽ0�5�
D�=��@Q��=V��w>結����;��|�-�>M�uk˽�}l�ʼ=�8�)V�= ��q� >Qcd�3��;�D�Sx�>��a�����~_��hd=�x&�*�=��i�JE�;X��)���>հ��9|T�5��;�{����NׅV��<�����>�D)�>aB�-�u�;��A�c3?�!h�J<5��lk0=�W�?�0?yr��%)?��gs
2?��<�ť!?   3�1�>�����k?   )��"=�W�?�0�yr��%)�n�i,h,���<�ť!�   3�1御����k�   )��"�a�x.?k��%&?��gs
2?�Z,]z?   ����>l����xb?   ���"=mzcrER�>��|�-�>�D�Sx�>X��)���><�����>��A�c3?5��lk0=�G�e'��=�:�"KO�>�hre���=OOӓ	/�>���_��=̤z�=;�>0c����=�
�<;�>X�b���3@�J��0-�౩�:;@�2�&,@�����,@   TMC_DR_QD_ED      DRIF�w8�O-�>5���v�>�Z��hҽWN����=Ћ�j"�=�A����>��; <��|�-�>w;���ʽ�}l�ʼ=���+V�= ��q� >����5��;]J����>p�p�&V����G=��c=�n�\.��=��9���;X��)���>�LB|T�5��;�{��[L�V����Z��>ի9�>��6y�u�;��A�c3?��#Fj�J<���k0=X��	1?yr��%)?�k�	�1?��<�ť!?   ��1�>�����k?   ���"=X��	1�yr��%)�aŗ66o+���<�ť!�   ��1御����k�   ���"��#��i/?k��%&?�k�	�1?�Z,]z?   o���>l����xb?   ��"=�w8�O-�>��|�-�>]J����>X��)���>��Z��>��A�c3?���k0=�G�e'��=�:�"KO�>�hre���=OOӓ	/�>%��_��=�z�=;�>lc����=�
�<;�>��wN5@J&>.���P^:@�CS��+@D 5&�H@	   TMC_DR_V3      DRIF8ɬ�q ?�_���>��Kk�ҽ�\�a�x�=��4���=(<���R>N�EGlw<��|�-�>�5�E�6Ž�}l�ʼ=1�Rd?V�= ��q� >r��tF��;�}�W��>c2�G���S���_=��g&/��=�}>���;X��)���>&-��|T�5��;�{�����w�V����d+��>�F��>�UO�u�;��A�c3?�g&�}�J<H�?�o0=v����6?yr��%)?ǁ��j,?��<�ť!?   ��1�>�����k?   ��"=v����6�yr��%)�8�.*��%���<�ť!�   ��1御����k�   ��"�Z~�I4?k��%&?ǁ��j,?�Z,]z?   M���>l����xb?   ���"=8ɬ�q ?��|�-�>�}�W��>X��)���>��d+��>��A�c3?H�?�o0=�H�e'��=�;�"KO�>�ire���=QPӓ	/�>���_��=��z�=;�>�b����=o
�<;�>	U#�1A@�KZ�@3�.�"us0@Y�(k��%@��c��@   TMC_DR_QD_ED      DRIF�h9gn?0� p6>���%�ҽ]#��=�=V��7a�=-sX��>��#W��<��|�-�>ou�B�Ľ�}l�ʼ=$KP�AV�= ��q� >L&NH��;CU�OB�>!"A�$������_=���[�=_~E%ܛ;X��)���>R"W�|T�5��;�{�������V��?����>�����>͕w��u�;��A�c3?R�=��J<L�#p0=��nW�6?yr��%)?�m����+?��<�ť!?   m�1�>�����k?   ^��"=��nW�6�yr��%)�+�\4�*%���<�ť!�   m�1御����k�   ^��"����Ĕz4?k��%&?�m����+?�Z,]z?   ����>l����xb?   H��"=�h9gn?��|�-�>CU�OB�>X��)���>?����>��A�c3?L�#p0=�G�e'��=�:�"KO�>�hre���=POӓ	/�>���_��=̤z�=;�>0c����=�
�<;�>2���B@{�:)�3��p�|K/@X��9%@m\���@
   TMC_QD_V03      QUAD�׬4G�?�>;�!��="��f�rҽZ�D*�s�="9S�Ϸ�=��KZB>p�w;<x�EY��>���]+����q��̚=�]��|�=��ۛF�=mH�?��;ʴ�l0��>��)

轶�3PG^=�{�=����L�;��oQV�>+&mTU�E������*��5K��MH����l���>�]��>��u�;��A�c3?d��݀�J<��Ep0=���S�6?�tz�?s�9�Q"+?C�ݚ�?   ��1�>�����k?   ���"=���S�6��tz���G�:۷$�C�ݚ��   ��1御����k�   ���"��9�7��4?�h<<?s�9�Q"+?�m���'?   D���>l����xb?   h��"=�׬4G�?x�EY��>ʴ�l0��>��oQV�>��l���>��A�c3?��Ep0=BՐ-}��=gMW��P�>TM��T��=�ڂ�0�>Gp:��
�=�(�#9�>'�E`�
�=�b�"9�>Dy����B@х��H� ��$�0��-@1���m�@m\���@   TMC_COR      KICKER�׬4G�?�>;�!��="��f�rҽZ�D*�s�="9S�Ϸ�=��KZB>p�w;<x�EY��>���]+����q��̚=�]��|�=��ۛF�=mH�?��;ʴ�l0��>��)

轶�3PG^=�{�=����L�;��oQV�>+&mTU�E������*��5K��MH����l���>�]��>��u�;��A�c3?d��݀�J<��Ep0=���S�6?�tz�?s�9�Q"+?C�ݚ�?   ��1�>�����k?   ���"=���S�6��tz���G�:۷$�C�ݚ��   ��1御����k�   ���"��9�7��4?�h<<?s�9�Q"+?�m���'?   D���>l����xb?   h��"=�׬4G�?x�EY��>ʴ�l0��>��oQV�>��l���>��A�c3?��Ep0=BՐ-}��=gMW��P�>TM��T��=�ڂ�0�>Gp:��
�=�(�#9�>'�E`�
�=�b�"9�>Dy����B@х��H� ��$�0��-@1���m�@m\���@   TMC_BPM      MONI�׬4G�?�>;�!��="��f�rҽZ�D*�s�="9S�Ϸ�=��KZB>p�w;<x�EY��>���]+����q��̚=�]��|�=��ۛF�=mH�?��;ʴ�l0��>��)

轶�3PG^=�{�=����L�;��oQV�>+&mTU�E������*��5K��MH����l���>�]��>��u�;��A�c3?d��݀�J<��Ep0=���S�6?�tz�?s�9�Q"+?C�ݚ�?   ��1�>�����k?   ���"=���S�6��tz���G�:۷$�C�ݚ��   ��1御����k�   ���"��9�7��4?�h<<?s�9�Q"+?�m���'?   D���>l����xb?   h��"=�׬4G�?x�EY��>ʴ�l0��>��oQV�>��l���>��A�c3?��Ep0=BՐ-}��=gMW��P�>TM��T��=�ڂ�0�>Gp:��
�=�(�#9�>'�E`�
�=�b�"9�>Dy����B@х��H� ��$�0��-@1���m�@��mwC@
   TMC_QD_V03      QUAD3rF��?J��a/ང��Sҽ[�t5�=�ˡ���=�r)�J>��|iD<�cx ��>���zQ~�=�{ֺ�e�+�05���)s�W6ܽ��S)s�ӻi��zU�>On�@���&�eB^=�g��sL�=E{�,�;���:*��>�s�=	�p���t`����:
Q�x�+���>j�}�>t����u�;��A�c3?�4����J<���-p0=E��+�6?���? ?�����*?���,�>   ��1�>�����k?   ���"=E��+�6����? �$��6�$����,��   ��1御����k�   ���"�i��R��4?�:��E�>�����*?86o�>   +���>l����xb?   ��"=3rF��?�cx ��>i��zU�>���:*��>x�+���>��A�c3?���-p0=����=���ʹR�>�����='(z'�2�>���F	�=o)7�>�E	�=�$P7�>���V��B@��`Q>@J4��cF-@o�,���?��S���@   TMC_DR_QD_ED      DRIF|��k�?k`������UV1ҽm2��r�='"ښ��=���r7>��m�_)<�cx ��>
���`�=�{ֺ�e����05���)s�W6ܽU��(s�ӻ��q7HE�>�4���'���M��+5^=�,`CA�=�M��
�;���:*��>zwA=	�p���t`��]�:
Q��Uq���>'�,�>zi���u�;��A�c3?�X��J<]�� p0=�@�ԕ�6?���? ?#�����*?���,�>   ��1�>�����k?   ���"=�@�ԕ�6����? ���ړx$����,��   ��1御����k�   ���"��3�l��4?�:��E�>#�����*?86o�>   $���>l����xb?   ì�"=|��k�?�cx ��>��q7HE�>���:*��>�Uq���>��A�c3?]�� p0=!����=���ʹR�>�����=/(z'�2�>���F	�=o)7�>�E	�=�$P7�>Ȗ;��B@tTI�� @!��r�-@�@c��X�?�v��@	   TMC_DR_V4      DRIFk�sZ�Y?��Dyb߽-Y\rƔѽ9�;��=o���y�=-�>�V�>Qt�r#�<�cx ��>ְ�"�՗=�{ֺ�e�hV_(15���)s�W6ܽ��S&s�ӻw)����>M�/)l����e�A�]=��/��=��~Ԛ;���:*��>��>	�p���t`��fw;
Q��S���>���>Ǡ���u�;��A�c3?9uP?�J<����o0=J��굂6?���? ?(/�v�*?���,�>   ��1�>�����k?   f��"=J��굂6����? �}�b��/$����,��   ��1御����k�   f��"��� o�Z4?�:��E�>(/�v�*?86o�>   ���>l����xb?   b��"=k�sZ�Y?�cx ��>w)����>���:*��>�S���>��A�c3?����o0=����=���ʹR�>�����=(z'�2�>���F	�=o)7�>�E	�=�$P7�>�����A@Q�Rdϖ
@�5t],@���|ނ�?�v��@	   TMC_DR_20      DRIFeZ�CY?E���p�ݽ��՘н;9Wo/0�=�ٱ�'��=�h�r��>m�sl<�cx ��>��Kuxy�=�{ֺ�e�����15���)s�W6ܽ
*; s�ӻc�ˣ0H�>8�3���"��_]=��7]���=WQ8��K�;���:*��>Lv�J@	�p���t`�ӱ��<
Q����X��>����>f6�u�;��A�c3?���|�J<��Mo0=s	&"��5?���? ?6,w?_�*?���,�>   ~�1�>�����k?   Ǟ�"=s	&"��5����? � ZzFz#����,��   ~�1御����k�   Ǟ�"�QUu>Y3?�:��E�>6,w?_�*?86o�>   ����>l����xb?   "=eZ�CY?�cx ��>c�ˣ0H�>���:*��>���X��>��A�c3?��Mo0=����=���ʹR�>�����=$(z'�2�>���F	�=o)7�>�E	�=�$P7�>5���=@@��i�o=	@TI����*@��Mol�?�s}��@   TMC_DP_DIP1   	   CSRCSBEND���9� ?4��bܽv<+�tͽhaIXԕ=��o|��=L��̦�>�e�B�<:��.��>����=��ʌ�e�R�Cy$(��u3 �~���}���ֻ�������>��oA;�����i`=�ݎf�=�c�|Ɯ;G;}s4��> ���[����t`�.��2�U��Yw��>K���i
>�ڭ�>e�;�1�A�c3?7����J<n�O�*=M2�|�4?�M�7�� ?�Yf�GM*?�|ֵ�,�>   Æ!�>u �*��k?   ���"=M2�|�4��M�7�� �`��u#��|ֵ�,��   Æ!�u �*��k�   ���"��l���R2?Iݻ$> ?�Yf�GM*?DϘa�>   N���>�Uj��xb?   �!�"=���9� ?:��.��>�������>G;}s4��>�Yw��>�1�A�c3?n�O�*=��i����=��Q(vK�>fZFb��=ݖ��}2�>���F	�=��*7�>8�6E	�=���Q7�>��,h=@�  ��@^kP��(@E�W'V�?�s}��@   TMC_DR_SIDE_C      CSRDRIFTJ��V��>���h^�ڽ�����ʽ�[���x�=I{�W�"�=ޣb�vx>9&9�; <:��.��>���n:��=��ʌ�e���%x%(��u3 �~���ߜ�ֻ�-��e��>4=�f,����rP<Nd_=j��!���=�>�$�;G;}s4��>���l��[����t`��}��U��O�Zw��>��y"k
>-�#%>e�;�1�A�c3?�ة��J<ǭQ�*=zt��3?�M�7�� ?�9�.*?�|ֵ�,�>   y�!�>u �*��k?   ���"=zt��3��M�7�� �Cv�D�r#��|ֵ�,��   y�!�u �*��k�   ���"��;��G1?Iݻ$> ?�9�.*?DϘa�>   ^���>�Uj��xb?   ��"=J��V��>:��.��>�-��e��>G;}s4��>�O�Zw��>�1�A�c3?ǭQ�*=��i����=��Q(vK�>jZFb��=▝�}2�>���F	�=��*7�>8�6E	�=���Q7�>U,	@�:@ҧ�3��@�k7k'@��\U�?�?�V��M!@   TMC_DR_SIDE      DRIF�o�4��>#��M�нJ�I�O����9L�9��=��,0�f�=�A�]�=�V)�;:��.��>\B�:�"�=��ʌ�e���Xq+(��u3 �~�轊�~[��ֻp�*��
�>jy�åզ�,+fk-�Z=`�=}�k�=�C�@�;G;}s4��>p�R��[����t`���,�U��?&y��>��r
>�Ӂ;e�;�1�A�c3?��|���J<�:q׈*=�>fϝ�*?�M�7�� ?��W�~(?�|ֵ�,�>   ��!�>u �*��k?   ��"=�>fϝ�*��M�7�� ���ǒe#��|ֵ�,��   ��!�u �*��k�   ��"�5�R��e'?Iݻ$> ?��W�~(?DϘa�>   ����>�Uj��xb?   ��"=�o�4��>:��.��>p�*��
�>G;}s4��>�?&y��>�1�A�c3?�:q׈*=��i����=��Q(vK�>dZFb��=ۖ��}2�>���F	�=��*7�>8�6E	�=���Q7�>W,�r�D)@A����?���!@M�}�r�?E��M"@   TMC_DP_DIP2   	   CSRCSBENDs�:�5�>^�9�۱ν����޺���h�r�=G������=�;�Xc��=����h��;יx ��>\��;z�=t�����e��NO����\�W6ܽ	�AX�ӻ����Ҽ�>̗���c��e��*Q�X=����3�=|d] ��;�h��8��>�O� Qw�w z�t`�~���67S�Ql���>�8P�>A~
�k�;|�%K�c3?�7����J<qD{�,=C2�Č�(?#�$�? ?��d~<(?��@�,�>   m�(�>�VM|��k?   ,P�"=C2�Č�(�#�$�? �fƟ@Zc#���@�,��   m�(徿VM|��k�   ,P�"��r"3V�%?5Ƙ�E�>��d~<(?a'q��>   !���>e;m��xb?   ��"=s�:�5�>יx ��>����Ҽ�>�h��8��>Ql���>|�%K�c3?qD{�,=Bb/��=e+�����>��~���=�N3�2�>N�U�F	�=g/Z,7�>�v	E	�=B�S7�>{0|#,�%@d.r"2�?��q @�ptbE�?E���"@   TMC_DR_CENT_C      CSRDRIFT|��5�t�>yn"�ͽ�-�����.�9?e��=MvkŢ�=p�rl���=6�E^�;יx ��>�{����=t�����e���aO����\�W6ܽ�O(X�ӻ/kn���>6鞹�*���2ztAX=
���a��=�ʩ���;�h��8��>���Rw�w z�t`�$��g77S�,E��>�c��>+ׯΪk�;|�%K�c3?Z.�[��J<�7�/�,=l'�\(?#�$�? ?t
�p(?��@�,�>   M�(�>�VM|��k?   �O�"=l'�\(�#�$�? ��	��=b#���@�,��   M�(徿VM|��k�   �O�"��@[5.%?5Ƙ�E�>t
�p(?a'q��>   ���>e;m��xb?   P��"=|��5�t�>יx ��>/kn���>�h��8��>,E��>|�%K�c3?�7�/�,=Bb/��=e+�����>��~���=�N3�2�>N�U�F	�=g/Z,7�>�v	E	�=B�S7�>9Ѩ��:$@-�=h���?6��@�0 @�-�]�?E���"@   TMC_BPM      MONI|��5�t�>yn"�ͽ�-�����.�9?e��=MvkŢ�=p�rl���=6�E^�;יx ��>�{����=t�����e���aO����\�W6ܽ�O(X�ӻ/kn���>6鞹�*���2ztAX=
���a��=�ʩ���;�h��8��>���Rw�w z�t`�$��g77S�,E��>�c��>+ׯΪk�;|�%K�c3?Z.�[��J<�7�/�,=l'�\(?#�$�? ?t
�p(?��@�,�>   M�(�>�VM|��k?   �O�"=l'�\(�#�$�? ��	��=b#���@�,��   M�(徿VM|��k�   �O�"��@[5.%?5Ƙ�E�>t
�p(?a'q��>   ���>e;m��xb?   P��"=|��5�t�>יx ��>/kn���>�h��8��>,E��>|�%K�c3?�7�/�,=Bb/��=e+�����>��~���=�N3�2�>N�U�F	�=g/Z,7�>�v	E	�=B�S7�>9Ѩ��:$@-�=h���?6��@�0 @�-�]�?E���#@   TMC_DR_CENT      DRIFs�i��>�
����ɽ��RG��@�ۢJׁ=Κ���i�=3�O���=����I,�;יx ��>�^,Q�e�=t�����e��(�O����\�W6ܽX�ӻe�訕e�>wq�������W= ���"�=�����;�h��8��>K-�8Tw�w z�t`�2%h�87S�S�O��>�����>J�W�k�;|�%K�c3?p�����J<�8.��,=�ߝj�'&?#�$�? ?:tT�V�'?��@�,�>   �(�>�VM|��k?   >O�"=�ߝj�'&�#�$�? ����n`#���@�,��   �(徿VM|��k�   >O�"�o��9��#?5Ƙ�E�>:tT�V�'?a'q��>   ����>e;m��xb?   ���"=s�i��>יx ��>e�訕e�>�h��8��>S�O��>|�%K�c3?�8.��,=@b/��=c+�����>��~���=�N3�2�>N�U�F	�=g/Z,7�>�v	E	�=B�S7�>�<߰�J!@��[T&�?]u���@UB���?�!��$@   TMC_DP_DIP3   	   CSRCSBEND�$����>���q]�ƽ����N���4��
>~=m5+�5Ӡ=��I�E��n�U]o!�;�R$��m�>T��dw{=v����e���p�ؒ�d3�]�漽�
8�лk>4�X@�>pVݯ�����0�:�U=#��>+>�=�8B�l�;�WE):��>��,�0����2٭t`�6�;�[�Q���nr���>����>����yo�;K�[�c3?;�� ��J<!���*.=�DzuOU$?��W���>�+�<�'?��@�,�>   ��,�>��E���k?   �-�"=�DzuOU$���W����L����]#���@�,��   ��,徦�E���k�   �-�"�L�W"?W}�Y%J�>�+�<�'?���d�>   e��>q~n�xb?   �s�"=�$����>�R$��m�>k>4�X@�>�WE):��>��nr���>K�[�c3?!���*.=�*4��=��7�>u��
��=�u��2�>�t�F	�=�ð-7�>Q��
E	�=fdT7�>��D�b@ �,>s�?���aW@���PU�?�!��%@   TMC_DR_SIDE_C      CSRDRIFTJW��j�>����5�ý�/�o�����Ew��x=�h�)b:�=`^��D���K�Ա�;�R$��m�>v�X�@v=v����e��x`ؒ�d3�]�漽�p(8�л|�:f,�>����v�y^V�8U=f{a�6�=���d��;�WE):��>fKHH2����2٭t`�!�5�\�Q���s����>mAӔ>S�alyo�;K�[�c3?�>��J<����).=e�eT�"?��W���>�0S!U'?��@�,�>   d�,�>��E���k?   -�"=e�eT�"���W����Y^8e�[#���@�,��   d�,徦�E���k�   -�"��!�9!?W}�Y%J�>�0S!U'?���d�>   �d��>q~n�xb?   ?p�"=JW��j�>�R$��m�>|�:f,�>�WE):��>��s����>K�[�c3?����).=�*4��=��7�>u��
��=�u��2�>�t�F	�=�ð-7�>Q��
E	�=fdT7�>�7V�-�@M�q~��?�U9Ui�@n��jC�?���0�+@   TMC_DR_SIDE      DRIF�^̡�>���t��������]�g_�ٴ���&��^���U~ڽSrۋ��ܻ�R$��m�>L>�0)Ae�v����e�\��_ؒ�d3�]�漽���s8�л�^i���>;?�dAا=�<,hQ=^e��=��;*�;�WE):��>�0�<����2٭t`���Sb�Q���}q���>� �X�>&f�vo�;K�[�c3?�+���J<Xr�]&.=���	�?��W���>�M�|�%?��@�,�>   �,�>��E���k?   �)�"=���(b����W�������U:N#���@�,��   �,徦�E���k�   �)�"����	�?W}�Y%J�>�M�|�%?���d�>   &b��>q~n�xb?   [�"=�^̡�>�R$��m�>�^i���>�WE):��>��}q���>K�[�c3?Xr�]&.=�*4��=��7�>u��
��=�u��2�>�t�F	�=�ð-7�>Q��
E	�=fdT7�>~����@gz�f���?^t� b4!@+��c�dֿ�#�2�,@   TMC_DP_DIP4   	   CSRCSBENDz�� ME�>� �����=�P�U�ñ��y7���j���Q�Az���_ǂ.���"D�r:�=�y ��>���!&p�(Nb;��e�� Aא4�����LY6ܽ� +��ӻC�6)�>���G%J�=��ٍ�Q=���"�=i��я;ۇ9�5��>����5	�g R�t`�����&
Q�X��	��>�p�s>��F�5s�;b��b�c3?��̑A�J<��4hz/=����r?�^�v? ?��`�%?PI�-�,�>   <m1�>�����k?   &�"=�rY�7%��^�v? �����K#�PI�-�,��   <m1������k�   &�"�����r?�i��E�>��`�%?���x�>   ����>o���xb?   ���"=z�� ME�>=�y ��>C�6)�>ۇ9�5��>X��	��>b��b�c3?��4hz/=�9����=(�δR�>d�o��=:��%�2�>ݎЙF	�=C?.7�>|�
E	�=�d�T7�>b��#M$@ (�3�����-�;�!@S����ڿ�#�2�-@   TMC_DR_20_C      CSRDRIFT0�1�
�>}����=%�M�ڲ�MP�q��r�P��?e��r��ĺ轇@ϧ2�=�y ��>_��
X�u�(Nb;��e���p�4�����LY6ܽd����ӻαp�i��>b��!^�=&-�C-Q=���@���=�������;ۇ9�5��>��p8	�g R�t`�����'
Q���s]	��>�7��t>'H�t5s�;b��b�c3?�>�J<7n��y/=�a��"�?�^�v? ?8�;�CD%?PI�-�,�>   �l1�>�����k?   l%�"= �{�v���^�v? �^Q`�I#�PI�-�,��   �l1������k�   l%�"��a��"�?�i��E�>8�;�CD%?���x�>   (���>o���xb?   ��"=0�1�
�>=�y ��>αp�i��>ۇ9�5��>��s]	��>b��b�c3?7n��y/=�9����=(�δR�>d�o��=:��%�2�>ݎЙF	�=C?.7�>|�
E	�=�d�T7�>�U\�	@�G<��Uӿ�,X�|�"@bã���޿�#�2�-@	   TMC_WA_OU      WATCH0�1�
�>}����=%�M�ڲ�MP�q��r�P��?e��r��ĺ轇@ϧ2�=�y ��>_��
X�u�(Nb;��e���p�4�����LY6ܽd����ӻαp�i��>b��!^�=&-�C-Q=���@���=�������;ۇ9�5��>��p8	�g R�t`�����'
Q���s]	��>�7��t>'H�t5s�;b��b�c3?�>�J<7n��y/=�a��"�?�^�v? ?8�;�CD%?PI�-�,�>   �l1�>�����k?   l%�"= �{�v���^�v? �^Q`�I#�PI�-�,��   �l1������k�   l%�"��a��"�?�i��E�>8�;�CD%?���x�>   (���>o���xb?   ��"=0�1�
�>=�y ��>αp�i��>ۇ9�5��>��s]	��>b��b�c3?7n��y/=�9����=(�δR�>d�o��=:��%�2�>ݎЙF	�=C?.7�>|�
E	�=�d�T7�>�U\�	@�G<��Uӿ�,X�|�"@bã���޿