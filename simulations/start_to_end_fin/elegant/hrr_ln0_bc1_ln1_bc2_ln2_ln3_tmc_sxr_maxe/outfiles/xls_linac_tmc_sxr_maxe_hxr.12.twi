SDDS1
!# little-endian
&description text="Twiss parameters--input: xls_linac_tmc_sxr_maxe_hxr.track.ele  lattice: xls_linac_tmc_sxr_maxe.12.lte", contents="Twiss parameters", &end
&parameter name=Step, description="Simulation step", type=long, &end
&parameter name=SVNVersion, description="SVN version number", type=string, fixed_value=unknown, &end
&parameter name=nux, symbol="$gn$r$bx$n", units="1/(2$gp$r)", description="Horizontal tune", type=double, &end
&parameter name=dnux/dp, symbol="$gx$r$bx$n", units="1/(2$gp$r)", description="Horizontal chromaticity", type=double, &end
&parameter name=dnux/dp2, symbol="$gx$r$bx2$n", units="1/(2$gp$r)", description="Horizontal 2nd-order chromaticity", type=double, &end
&parameter name=dnux/dp3, symbol="$gx$r$bx3$n", units="1/(2$gp$r)", description="Horizontal 3rd-order chromaticity", type=double, &end
&parameter name=Ax, symbol="A$bx$n", units=m, description="Horizontal acceptance", type=double, &end
&parameter name=AxLocation, units=m, description="Location of horizontal acceptance limit", type=double, &end
&parameter name=nuy, symbol="$gn$r$by$n", units="1/(2$gp$r)", description="Vertical tune", type=double, &end
&parameter name=dnuy/dp, symbol="$gx$r$by$n", units="1/(2$gp$r)", description="Vertical chromaticity", type=double, &end
&parameter name=dnuy/dp2, symbol="$gx$r$by2$n", units="1/(2$gp$r)", description="Vertical 2nd-order chromaticity", type=double, &end
&parameter name=dnuy/dp3, symbol="$gx$r$by3$n", units="1/(2$gp$r)", description="Vertical 3rd-order chromaticity", type=double, &end
&parameter name=Ay, symbol="A$by$n", units=m, description="Vertical acceptance", type=double, &end
&parameter name=AyLocation, units=m, description="Location of vertical acceptance limit", type=double, &end
&parameter name=deltaHalfRange, symbol="$gDd$r/2", description="Half range of momentum offset for chromatic tune spread evaluation", type=double, &end
&parameter name=nuxChromUpper, symbol="$gx$r$bu$n", description="Upper limit of x tune due to chromaticity and deltaRange", type=double, &end
&parameter name=nuxChromLower, symbol="$gx$r$bu$n", description="Lower limit of x tune due to chromaticity and deltaRange", type=double, &end
&parameter name=nuyChromUpper, symbol="$gy$r$bu$n", description="Upper limit of y tune due to chromaticity and deltaRange", type=double, &end
&parameter name=nuyChromLower, symbol="$gy$r$bu$n", description="Lower limit of y tune due to chromaticity and deltaRange", type=double, &end
&parameter name=Stage, description="Stage of computation", type=string, &end
&parameter name=pCentral, units="m$be$nc", description="Central momentum", type=double, &end
&parameter name=dbetax/dp, units=m, description="Derivative of betax with momentum offset", type=double, &end
&parameter name=dbetay/dp, units=m, description="Derivative of betay with momentum offset", type=double, &end
&parameter name=dalphax/dp, description="Derivative of alphax with momentum offset", type=double, &end
&parameter name=dalphay/dp, description="Derivative of alphay with momentum offset", type=double, &end
&parameter name=etax2, symbol="$gc$r$bx2$n", units=m, description="Second-order dispersion (for matched or periodic case only)", type=double, &end
&parameter name=etay2, symbol="$gc$r$by2$n", units=m, description="Second-order dispersion (for matched or periodic case only)", type=double, &end
&parameter name=etax3, symbol="$gc$r$bx3$n", units=m, description="Third-order dispersion (for matched or periodic case only)", type=double, &end
&parameter name=etay3, symbol="$gc$r$by3$n", units=m, description="Third-order dispersion (for matched or periodic case only)", type=double, &end
&parameter name=etaxp2, symbol="$gc$r$bx2$n$a'$n", units=m, description="Second-order dispersion (for matched or periodic case only)", type=double, &end
&parameter name=etayp2, symbol="$gc$r$by2$n$a'$n", units=m, description="Second-order dispersion (for matched or periodic case only)", type=double, &end
&parameter name=etaxp3, symbol="$gc$r$bx3$n$a'$n", units=m, description="Third-order dispersion (for matched or periodic case only)", type=double, &end
&parameter name=etayp3, symbol="$gc$r$by3$n$a'$n", units=m, description="Third-order dispersion (for matched or periodic case only)", type=double, &end
&parameter name=betaxMin, units=m, description="Minimum betax", type=double, &end
&parameter name=betaxAve, units=m, description="Average betax", type=double, &end
&parameter name=betaxMax, units=m, description="Maximum betax", type=double, &end
&parameter name=betayMin, units=m, description="Minimum betay", type=double, &end
&parameter name=betayAve, units=m, description="Average betay", type=double, &end
&parameter name=betayMax, units=m, description="Maximum betay", type=double, &end
&parameter name=etaxMax, units=m, description="Maximum absolute value of etax", type=double, &end
&parameter name=etayMax, units=m, description="Maximum absolute value of etay", type=double, &end
&parameter name=waistsx, description="Number of changes in the sign of alphax", type=long, &end
&parameter name=waistsy, description="Number of changes in the sign of alphay", type=long, &end
&parameter name=dnux/dAx, units=1/m, description="Horizontal tune shift with horizontal amplitude", type=double, &end
&parameter name=dnux/dAy, units=1/m, description="Horizontal tune shift with vertical amplitude", type=double, &end
&parameter name=dnuy/dAx, units=1/m, description="Vertical tune shift with horizontal amplitude", type=double, &end
&parameter name=dnuy/dAy, units=1/m, description="Vertical tune shift with vertical amplitude", type=double, &end
&parameter name=dnux/dAx2, units="1/m$a2$n", description="Horizontal tune shift with horizontal amplitude", type=double, &end
&parameter name=dnux/dAy2, units="1/m$a2$n", description="Horizontal tune shift with vertical amplitude", type=double, &end
&parameter name=dnux/dAxAy, units="1/m$a2$n", description="Horizontal tune shift with horizontal and vertical amplitude", type=double, &end
&parameter name=dnuy/dAx2, units="1/m$a2$n", description="Vertical tune shift with horizontal amplitude", type=double, &end
&parameter name=dnuy/dAy2, units="1/m$a2$n", description="Vertical tune shift with vertical amplitude", type=double, &end
&parameter name=dnuy/dAxAy, units="1/m$a2$n", description="Vertical tune shift with horizontal and vertical amplitude", type=double, &end
&parameter name=nuxTswaLower, description="Minimum horizontal tune from tune-shift-with-amplitude calculations", type=double, &end
&parameter name=nuxTswaUpper, description="Maximum horizontal tune from tune-shift-with-amplitude calculations", type=double, &end
&parameter name=nuyTswaLower, description="Minimum vertical tune from tune-shift-with-amplitude calculations", type=double, &end
&parameter name=nuyTswaUpper, description="Maximum vertical tune from tune-shift-with-amplitude calculations", type=double, &end
&parameter name=couplingIntegral, description="Coupling integral for difference resonance", type=double, &end
&parameter name=couplingDelta, description="Distance from difference resonance", type=double, &end
&parameter name=emittanceRatio, description="Emittance ratio from coupling integral", type=double, &end
&parameter name=alphac2, symbol="$ga$r$bc2$n", description="2nd-order momentum compaction factor", type=double, &end
&parameter name=alphac, symbol="$ga$r$bc$n", description="Momentum compaction factor", type=double, &end
&parameter name=I1, units=m, description="Radiation integral 1", type=double, &end
&parameter name=I2, units=1/m, description="Radiation integral 2", type=double, &end
&parameter name=I3, units="1/m$a2$n", description="Radiation integral 3", type=double, &end
&parameter name=I4, units=1/m, description="Radiation integral 4", type=double, &end
&parameter name=I5, units=1/m, description="Radiation integral 5", type=double, &end
&parameter name=ex0, units="$gp$rm", description="Damped horizontal emittance", type=double, &end
&parameter name=enx0, units="m$be$nc $gp$rm", description="Damped normalized horizontal emittance", type=double, &end
&parameter name=taux, units=s, description="Horizontal damping time", type=double, &end
&parameter name=Jx, description="Horizontal damping partition number", type=double, &end
&parameter name=tauy, units=s, description="Vertical damping time", type=double, &end
&parameter name=Jy, description="Vertical damping partition number", type=double, &end
&parameter name=Sdelta0, description="RMS fractional energy spread", type=double, &end
&parameter name=taudelta, units=s, description="Longitudinal damping time", type=double, &end
&parameter name=Jdelta, description="Longitudinal damping partition number", type=double, &end
&parameter name=U0, units=MeV, description="Energy loss per turn", type=double, &end
&column name=s, units=m, description=Distance, type=double,  &end
&column name=betax, symbol="$gb$r$bx$n", units=m, description="Horizontal beta-function", type=double,  &end
&column name=alphax, symbol="$ga$r$bx$n", description="Horizontal alpha-function", type=double,  &end
&column name=psix, symbol="$gy$r$bx$n", units=rad, description="Horizontal phase advance", type=double,  &end
&column name=etax, symbol="$gc$r$bx$n", units=m, description="Horizontal dispersion", type=double,  &end
&column name=etaxp, symbol="$gc$r$bx$n$a'$n", description="Slope of horizontal dispersion", type=double,  &end
&column name=xAperture, symbol="a$bx,eff$n", units=m, description="Effective horizontal aperture", type=double,  &end
&column name=betay, symbol="$gb$r$by$n", units=m, description="Vertical beta-function", type=double,  &end
&column name=alphay, symbol="$ga$r$by$n", description="Vertical alpha-function", type=double,  &end
&column name=psiy, symbol="$gy$r$by$n", units=rad, description="Vertical phase advance", type=double,  &end
&column name=etay, symbol="$gc$r$by$n", units=m, description="Vertical dispersion", type=double,  &end
&column name=etayp, symbol="$gc$r$by$n$a'$n", description="Slope of vertical dispersion", type=double,  &end
&column name=yAperture, symbol="a$by,eff$n", units=m, description="Effective vertical aperture", type=double,  &end
&column name=pCentral0, symbol="p$bcent$n", units="m$be$nc", description="Initial central momentum", type=double,  &end
&column name=ElementName, description="Element name", format_string=%10s, type=string,  &end
&column name=ElementOccurence, description="Occurence of element", format_string=%6ld, type=long,  &end
&column name=ElementType, description="Element-type name", format_string=%10s, type=string,  &end
&column name=ChamberShape, type=string,  &end
&column name=dI1, units=m, description="Contribution to radiation integral 1", type=double,  &end
&column name=dI2, units=1/m, description="Contribution to radiation integral 2", type=double,  &end
&column name=dI3, units="1/m$a2$n", description="Contribution to radiation integral 3", type=double,  &end
&column name=dI4, units=1/m, description="Contribution to radiation integral 4", type=double,  &end
&column name=dI5, units=1/m, description="Contribution to radiation integral 5", type=double,  &end
&data mode=binary, &end
/       ��S�$[�?��^�ٿ                        ���������x��w��?�Cx���                        ��������        ��S�$[�?��S�$[�?�x��w��?�x��w��?   tunes uncorrectedHǰcQ��@֎wb�%@�?ɤ�@��Oo����cN!�                                                                ��M2'r�?���(@�ﳪ3}!@�c��/"@�|���6 @�j�~A7@                                                                                                                                              ����Y�?                q��[����گ���)�����?D.�Ab>R?        Z�&���=��-H�<=I
}2>�*�KQy�?      �?�*�KQy�?      �?�5��+SC?�*�KQy�?       @���w�?        ���*�	@D���ӿ                              $@A�>|�"@��3E��޿                              $@HǰcQ��@   _BEG_      MARK                                                    ���*�	@D���ӿ                              $@A�>|�"@��3E��޿                              $@HǰcQ��@   BNCH      CHARGE   ?                                        윙����?knr��
@Q�͘�׿��a�~�?                      $@�E-
�B#@+��i5�
p�Qi}�?                      $@HǰcQ��@	   HXR_DR_V1      DRIF   ?                                        �䥛� �?=3���;@��[lSٿ�+��z�?                      $@O�cK�]#@|��l࿺��?                      $@HǰcQ��@   HXR_DR_QD_ED      DRIF   ?                                        �*\����?&b�x0@�ڛ�?�>�8u�?                      $@�wF�|{#@F}q�������Q:`�?                      $@HǰcQ��@
   HXR_QD_V01      QUAD   ?                                        �*\����?&b�x0@�ڛ�?�>�8u�?                      $@�wF�|{#@F}q�������Q:`�?                      $@HǰcQ��@   HXR_COR      KICKER   ?                                        �*\����?&b�x0@�ڛ�?�>�8u�?                      $@�wF�|{#@F}q�������Q:`�?                      $@HǰcQ��@   HXR_BPM      MONI   ?                                        Ap����?SD�0b�
@o� @�D,�p�?                      $@<����#@��}���u3s���?                      $@HǰcQ��@
   HXR_QD_V01      QUAD   ?                                        Ȇ�Q��? ��BC	@�Nخ��?��↹?                      $@�|iS/_%@~�8�5�nZ��t�?                      $@HǰcQ��@   HXR_DR_QD_ED      DRIF   ?                                        �D<j�?,t�� ��?|�*����?���Ö́�?                      $@���(�5@�}���m&��jƔƗ�?                      $@HǰcQ��@	   HXR_DR_V2      DRIF   ?                                        Lw~+8�?���{y��?�SP��?pDf����?                      $@2	��7@��T�'��"�h/�?                      $@HǰcQ��@   HXR_DR_QD_ED      DRIF   ?                                        �Y%�~�?mC0����?���<[��?��em���?                      $@�j�~A7@���|� A4(T�?                      $@HǰcQ��@
   HXR_QD_V02      QUAD   ?                                        �Y%�~�?mC0����?���<[��?��em���?                      $@�j�~A7@���|� A4(T�?                      $@HǰcQ��@   HXR_COR      KICKER   ?                                        �Y%�~�?mC0����?���<[��?��em���?                      $@�j�~A7@���|� A4(T�?                      $@HǰcQ��@   HXR_BPM      MONI   ?                                        �46��?��M2'r�? J��p4��u���4F�?                      $@�@�E�37@w��|�7@	�}`�x�?                      $@HǰcQ��@
   HXR_QD_V02      QUAD   ?                                        �P���?~ ����?���4W���P}v���?                      $@O\�!Ά6@�w"���@S�����?                      $@HǰcQ��@   HXR_DR_QD_ED      DRIF   ?                                        j�2Ռ@�P���X@$��̥��hp$�W��?                      $@��V��u$@; ��T&@�=$ϯ�?                      $@HǰcQ��@	   HXR_DR_V3      DRIF   ?                                        ;��+T�@�)��1@��8Ӕ�𿃍�f �?                      $@Dy�FH�#@OV#s�@��h�Z�?                      $@HǰcQ��@   HXR_DR_QD_ED      DRIF   ?                                        �c��@%F�`@�[-���濻���h�?                      $@*�%�d#@=!�G��@n���΅�?                      $@HǰcQ��@
   HXR_QD_V03      QUAD   ?                                        �c��@%F�`@�[-���濻���h�?                      $@*�%�d#@=!�G��@n���΅�?                      $@HǰcQ��@   HXR_COR      KICKER   ?                                        �c��@%F�`@�[-���濻���h�?                      $@*�%�d#@=!�G��@n���΅�?                      $@HǰcQ��@   HXR_BPM      MONI   ?                                        ��ڨ�,@P��D�}@x���-�ؿ�u
G(�?                      $@��E#@��Dܬ�?��N��?                      $@HǰcQ��@
   HXR_QD_V03      QUAD   ?                                        t����@�AL.:�@%ǪaWڿ�)��v�?                      $@�p2~"�"@b�v'�T�?u=�ad�?                      $@HǰcQ��@   HXR_DR_QD_ED      DRIF   ?                                        V�ŧ�U@���vv @]��!d���{�i�?                      $@�:3y	@�S��?l��6�?                      $@HǰcQ��@	   HXR_DR_V4      DRIF   ?                                        �;,��@زÿ'!@ۯ.�5����x�V��?                      $@��.O�@�ӎE"��?���`�;�?                      $@HǰcQ��@	   HXR_DR_CT      DRIF   ?                                        �;,��@زÿ'!@ۯ.�5����x�V��?                      $@��.O�@�ӎE"��?���`�;�?                      $@HǰcQ��@   HXR_WA_M_OU      WATCH   ?                                        $��5�@TH�W�r!@�t7\@n����!���?                      $@��^�-@x �
��?;�b����?                      $@HǰcQ��@   HXR_DR_QD_ED      DRIF   ?                                        ~1Uj��@�ﳪ3}!@�FQ|�)�?c�Y1��?                      $@�c��/"@�M��Z��Ů����?                      $@HǰcQ��@	   HXR_QD_FH      QUAD   ?                                        ~1Uj��@�ﳪ3}!@�FQ|�)�?c�Y1��?                      $@�c��/"@�M��Z��Ů����?                      $@HǰcQ��@   HXR_COR      KICKER   ?                                        ~1Uj��@�ﳪ3}!@�FQ|�)�?c�Y1��?                      $@�c��/"@�M��Z��Ů����?                      $@HǰcQ��@   HXR_BPM      MONI   ?                                        ؕ�I�@;���7q!@�x:����?�w{/��?                      $@.r���6@E����忼���?                      $@HǰcQ��@	   HXR_QD_FH      QUAD   ?                                        @'~�A@�$���!@�N����?vw�|���?                      $@qe�~�@[�?п��2��V5��?                      $@HǰcQ��@   HXR_DR_QD_ED      DRIF   ?                                        ����@ ��Rm�@�qG*�'�?�~�r49�?                      $@r��:�(@�E_��l}y����?                      $@HǰcQ��@   HXR_DR_WIG_ED      DRIF   ?                                        gX�r1@����9|@���vF��?L��\O�@                      $@`�@Z�@�} ^������d��?                      $@HǰcQ��@	   HXR_WIG_0      WIGGLER   ?���گ���)����}?D.�Ab>B?        ���G���=�9���@��1{
@oxoU�?��BS�J@                      $@�U�� @�4h">��J��L6K�?                      $@HǰcQ��@   HXR_DR_WIG_ED      DRIF   ?                                        ��@l�|��	@���G�?ԮD��k@                      $@gנ�]!@;T7Ρ��#i�v�d�?                      $@HǰcQ��@   HXR_DR_QD_ED	      DRIF   ?                                        q/*�n%@O�d0m	@i}�,�?=q��s@                      $@	i�>R!@2>
IgM��D�%S�j�?                      $@HǰcQ��@	   HXR_QD_DH      QUAD   ?                                        q/*�n%@O�d0m	@i}�,�?=q��s@                      $@	i�>R!@2>
IgM��D�%S�j�?                      $@HǰcQ��@   HXR_COR      KICKER   ?                                        q/*�n%@O�d0m	@i}�,�?=q��s@                      $@	i�>R!@2>
IgM��D�%S�j�?                      $@HǰcQ��@   HXR_BPM      MONI   ?                                        ˓e¾2@���z	@�J�R��j���Q|@                      $@�/ޫ)!@vT�R�z�?�<\��p�?                      $@HǰcQ��@	   HXR_QD_DH      QUAD   ?                                        3%S?�g@	�6�5�	@��u�e�C�rRu�@                      $@�=O�� @	�g*y"�?]�k�A��?                      $@HǰcQ��@   HXR_DR_QD_ED
      DRIF   ?                                        {�S�/@-@1h\�@u�u��-�qX@                      $@�@���@p&����?�ۊ��?                      $@HǰcQ��@   HXR_DR_WIG_ED      DRIF   ?                                        -��u!#@��!�2S@�ebt��0����@                      $@u��ۖ�@��E2��?:$;eH�?                      $@HǰcQ��@	   HXR_WIG_0      WIGGLER   ?���گ���)����}?D.�Ab>B?        �ˀ���=���#@�sqwb�@Z�� ]��@�T4@                      $@�S���{@�٫�Z@�?W��'�?                      $@HǰcQ��@   HXR_DR_WIG_ED      DRIF   ?                                        ��a>��#@"��'@�������g���A@                      $@�֌���
@_���e��?�*�'f�?                      $@HǰcQ��@   HXR_DR_QD_ED      DRIF   ?                                        ���-,�#@4u�G:@TǆQ>΢?��7�QE@                      $@��$��
@B暰_ȋ?��l�v�?                      $@HǰcQ��@	   HXR_QD_FH      QUAD   ?                                        ���-,�#@4u�G:@TǆQ>΢?��7�QE@                      $@��$��
@B暰_ȋ?��l�v�?                      $@HǰcQ��@	   HXR_WA_OU      WATCH   ?                                        