SDDS1
!# little-endian
&description text="sigma matrix--input: xls_linac_sxrbp_ln4_ln2_bp.track.ele  lattice: xls_linac_sxrbp_ln4.8.lte", contents="sigma matrix", &end
&parameter name=Step, description="Simulation step", type=long, &end
&parameter name=SVNVersion, description="SVN version number", type=string, fixed_value=unknown, &end
&column name=s, units=m, description=Distance, type=double,  &end
&column name=ElementName, description="Element name", format_string=%10s, type=string,  &end
&column name=ElementOccurence, description="Occurence of element", format_string=%6ld, type=long,  &end
&column name=ElementType, description="Element-type name", format_string=%10s, type=string,  &end
&column name=s1, symbol="$gs$r$b1$n", units=m, description="sqrt(<x*x>)", type=double,  &end
&column name=s12, symbol="$gs$r$b12$n", units=m, description="<x*xp'>", type=double,  &end
&column name=s13, symbol="$gs$r$b13$n", units="m$a2$n", description="<x*y>", type=double,  &end
&column name=s14, symbol="$gs$r$b14$n", units=m, description="<x*y'>", type=double,  &end
&column name=s15, symbol="$gs$r$b15$n", units="m$a2$n", description="<x*s>", type=double,  &end
&column name=s16, symbol="$gs$r$b16$n", units=m, description="<x*delta>", type=double,  &end
&column name=s17, symbol="$gs$r$b17$n", units="m*s", description="<x*t>", type=double,  &end
&column name=s2, symbol="$gs$r$b2$n", description="sqrt(<x'*x'>)", type=double,  &end
&column name=s23, symbol="$gs$r$b23$n", units=m, description="<x'*y>", type=double,  &end
&column name=s24, symbol="$gs$r$b24$n", description="<x'*y'>", type=double,  &end
&column name=s25, symbol="$gs$r$b25$n", units=m, description="<x'*s>", type=double,  &end
&column name=s26, symbol="$gs$r$b26$n", description="<x'*delta>", type=double,  &end
&column name=s27, symbol="$gs$r$b27$n", units=s, description="<x'*t>", type=double,  &end
&column name=s3, symbol="$gs$r$b3$n", units=m, description="sqrt(<y*y>)", type=double,  &end
&column name=s34, symbol="$gs$r$b34$n", units=m, description="<y*y'>", type=double,  &end
&column name=s35, symbol="$gs$r$b35$n", units="m$a2$n", description="<y*s>", type=double,  &end
&column name=s36, symbol="$gs$r$b36$n", units=m, description="<y*delta>", type=double,  &end
&column name=s37, symbol="$gs$r$b37$n", units="m*s", description="<y*t>", type=double,  &end
&column name=s4, symbol="$gs$r$b4$n", description="sqrt(<y'*y')>", type=double,  &end
&column name=s45, symbol="$gs$r$b45$n", units=m, description="<y'*s>", type=double,  &end
&column name=s46, symbol="$gs$r$b46$n", description="<y'*delta>", type=double,  &end
&column name=s47, symbol="$gs$r$b47$n", units=s, description="<y'*t>", type=double,  &end
&column name=s5, symbol="$gs$r$b5$n", units=m, description="sqrt(<s*s>)", type=double,  &end
&column name=s56, symbol="$gs$r$b56$n", units=m, description="<s*delta>", type=double,  &end
&column name=s57, symbol="$gs$r$b57$n", units="m*s", description="<s*t>", type=double,  &end
&column name=s6, symbol="$gs$r$b6$n", description="sqrt(<delta*delta>)", type=double,  &end
&column name=s67, symbol="$gs$r$b67$n", units=s, description="<delta*t>", type=double,  &end
&column name=s7, symbol="$gs$r$b7$n", description="sqrt(<t*t>)", type=double,  &end
&column name=ma1, symbol="max$sb$ex$sb$e", units=m, description="maximum absolute value of x", type=double,  &end
&column name=ma2, symbol="max$sb$ex'$sb$e", description="maximum absolute value of x'", type=double,  &end
&column name=ma3, symbol="max$sb$ey$sb$e", units=m, description="maximum absolute value of y", type=double,  &end
&column name=ma4, symbol="max$sb$ey'$sb$e", description="maximum absolute value of y'", type=double,  &end
&column name=ma5, symbol="max$sb$e$gD$rs$sb$e", units=m, description="maximum absolute deviation of s", type=double,  &end
&column name=ma6, symbol="max$sb$e$gd$r$sb$e", description="maximum absolute value of delta", type=double,  &end
&column name=ma7, symbol="max$sb$e$gD$rt$sb$e", units=s, description="maximum absolute deviation of t", type=double,  &end
&column name=minimum1, symbol="x$bmin$n", units=m, type=double,  &end
&column name=minimum2, symbol="x'$bmin$n", units=m, type=double,  &end
&column name=minimum3, symbol="y$bmin$n", units=m, type=double,  &end
&column name=minimum4, symbol="y'$bmin$n", units=m, type=double,  &end
&column name=minimum5, symbol="$gD$rs$bmin$n", units=m, type=double,  &end
&column name=minimum6, symbol="$gd$r$bmin$n", units=m, type=double,  &end
&column name=minimum7, symbol="t$bmin$n", units=s, type=double,  &end
&column name=maximum1, symbol="x$bmax$n", units=m, type=double,  &end
&column name=maximum2, symbol="x'$bmax$n", units=m, type=double,  &end
&column name=maximum3, symbol="y$bmax$n", units=m, type=double,  &end
&column name=maximum4, symbol="y'$bmax$n", units=m, type=double,  &end
&column name=maximum5, symbol="$gD$rs$bmax$n", units=m, type=double,  &end
&column name=maximum6, symbol="$gd$r$bmax$n", units=m, type=double,  &end
&column name=maximum7, symbol="t$bmax$n", units=s, type=double,  &end
&column name=Sx, symbol="$gs$r$bx$n", units=m, description=sqrt(<(x-<x>)^2>), type=double,  &end
&column name=Sxp, symbol="$gs$r$bx'$n", description=sqrt(<(x'-<x'>)^2>), type=double,  &end
&column name=Sy, symbol="$gs$r$by$n", units=m, description=sqrt(<(y-<y>)^2>), type=double,  &end
&column name=Syp, symbol="$gs$r$by'$n", description=sqrt(<(y'-<y'>)^2>), type=double,  &end
&column name=Ss, symbol="$gs$r$bs$n", units=m, description=sqrt(<(s-<s>)^2>), type=double,  &end
&column name=Sdelta, symbol="$gs$bd$n$r", description=sqrt(<(delta-<delta>)^2>), type=double,  &end
&column name=St, symbol="$gs$r$bt$n", units=s, description=sqrt(<(t-<t>)^2>), type=double,  &end
&column name=ex, symbol="$ge$r$bx$n", units=m, description="geometric horizontal emittance", type=double,  &end
&column name=enx, symbol="$ge$r$bx,n$n", units=m, description="normalized horizontal emittance", type=double,  &end
&column name=ecx, symbol="$ge$r$bx,c$n", units=m, description="geometric horizontal emittance less dispersive contributions", type=double,  &end
&column name=ecnx, symbol="$ge$r$bx,cn$n", units=m, description="normalized horizontal emittance less dispersive contributions", type=double,  &end
&column name=ey, symbol="$ge$r$by$n", units=m, description="geometric vertical emittance", type=double,  &end
&column name=eny, symbol="$ge$r$by,n$n", units=m, description="normalized vertical emittance", type=double,  &end
&column name=ecy, symbol="$ge$r$by,c$n", units=m, description="geometric vertical emittance less dispersive contributions", type=double,  &end
&column name=ecny, symbol="$ge$r$by,cn$n", units=m, description="normalized vertical emittance less dispersive contributions", type=double,  &end
&column name=betaxBeam, symbol="$gb$r$bx,beam$n", units=m, description="betax for the beam, excluding dispersive contributions", type=double,  &end
&column name=alphaxBeam, symbol="$ga$r$bx,beam$n", description="alphax for the beam, excluding dispersive contributions", type=double,  &end
&column name=betayBeam, symbol="$gb$r$by,beam$n", units=m, description="betay for the beam, excluding dispersive contributions", type=double,  &end
&column name=alphayBeam, symbol="$ga$r$by,beam$n", description="alphay for the beam, excluding dispersive contributions", type=double,  &end
&data mode=binary, &end
&                 _BEG_      MARK�<)��R�>\��`�=`�ǕZ��=�2WK���&M���P,FShR�_ؿ�n滏�����>=� Ӗ=CЮ9�1�g��=6��5�q|�2� ����%Ż��ESm�>Df5�ֽo�8ܘ�fA�v�L���5�S��V�M���>6��Ĵ�=��/���=B!S�mPX;���% �>��=b�2>�:�\��;�z��f?/�-��So<yE}b��<����?�"��%U?����b�$?҂�7$�
?   ��D�>�zJ�+��?   �y=�����[M[����H�#�T�=����   \5�ʾ �˂Uz�   $�r��v䇪�?�"��%U?����b�$?҂�7$�
?   ��D�>�zJ�+��?   �y=�<)��R�>������>��ESm�>�V�M���>���% �>�z��f?yE}b��<��5Xg��=4=�~b��>q�����=ɯ�����>ߙP���=�U�,Ç�>b�����=+�򰻇�>��F��=@T&�5'��S��-�@�81�i�?           BNCH      CHARGE�<)��R�>\��`�=`�ǕZ��=�2WK���&M���P,FShR�_ؿ�n滏�����>=� Ӗ=CЮ9�1�g��=6��5�q|�2� ����%Ż��ESm�>Df5�ֽo�8ܘ�fA�v�L���5�S��V�M���>6��Ĵ�=��/���=B!S�mPX;���% �>��=b�2>�:�\��;�z��f?/�-��So<yE}b��<����?�"��%U?����b�$?҂�7$�
?   ��D�>�zJ�+��?   �y=�����[M[����H�#�T�=����   \5�ʾ �˂Uz�   $�r��v䇪�?�"��%U?����b�$?҂�7$�
?   ��D�>�zJ�+��?   �y=�<)��R�>������>��ESm�>�V�M���>���% �>�z��f?yE}b��<��5Xg��=4=�~b��>q�����=ɯ�����>ߙP���=�U�,Ç�>b�����=+�򰻇�>��F��=@T&�5'��S��-�@�81�i�?           LN2_IN_CNT_BP      CENTER�<)��R�>\��`�=`�ǕZ��=�2WK���&M���P,FShR�_ؿ�n滏�����>=� Ӗ=BЮ9�1�g��=6��5�q|�2� ����%Ż��ESm�>Df5�ֽo�8ܘ�cA�v�L���5�S��V�M���>7��Ĵ�=��/���=@!S�mPX;���% �>��=b�2>�:�\��;�z��f?/�-��So<yE}b��<	��?J�ʘ�V?���&f�$?� s!�
?   ��D�>�zJ�+��?   �y=	�����xж��	��#�������   \5�ʾ �˂Uz�   $�r�B�!>��?J�ʘ�V?���&f�$?� s!�
?   ��D�>�zJ�+��?   �y=�<)��R�>������>��ESm�>�V�M���>���% �>�z��f?yE}b��<��5Xg��=4=�~b��>q�����=ɯ�����>ߙP���=�U�,Ç�>b�����=+�򰻇�>��F��=@T&�5'��S��-�@�81�i�?g���@   LN2_DR_BP_V1      DRIFA�5ٚ?A1�m���=���g1o�=-��%����'P ������Πa�.m�{����������>,��Q�Ε=BЮ9�1�lvx6��5�q|�2��<�$�"ŻeuÈJ��>����i�=�n�1K4=v�ݘ�=�^���q;�V�M���>ߙN��=��/���=��v -OX;!bm�g �>��m��2>]��2��;�z��f?�RıMo<�Tjm��<F�G��3?J�ʘ�V?�{+R� ?� s!�
?   ��J�>�zJ�+��?   k��=��0w?�/���xж�܂����������   |��ʾ �˂Uz�   ~Oo�F�G��3?J�ʘ�V?�{+R� ?� s!�
?   ��J�>�zJ�+��?   k��=A�5ٚ?������>euÈJ��>�V�M���>!bm�g �>�z��f?�Tjm��<��5Xg��=;=�~b��>t�����=ͯ�����>ߙP���=�U�,Ç�>_�����=(�򰻇�>bf�-Xw1@�m���
���JZb@�_�8ݤ�>�3���@   LN2_DR_QD_ED      DRIFfX}F�?�w�%�!�=|�O��k�=�G���u��>!��Ȟn-(�a�\	F�p��������>Ut�˕=BЮ9�1���4#y6��5�q|�2�7�*�w"Ż/��{��>�Һ�%�=�#�i֖4=x1x� �=_�c8�q;�V�M���>��t5��=��/���=l��Q)OX;f}h �>d��7�2>��:k)��;�z��f?��ݟMo<�a:�a��<K�}A3?J�ʘ�V?��!�	-!?� s!�
?    �J�>�zJ�+��?   t̀=�@��A
0���xж��U�F�������   ���ʾ �˂Uz�   �Eo�K�}A3?J�ʘ�V?��!�	-!?� s!�
?    �J�>�zJ�+��?   t̀=fX}F�?������>/��{��>�V�M���>f}h �>�z��f?�a:�a��<��5Xg��=1=�~b��>o�����=Ư�����>ߙP���=�U�,Ç�>_�����=(�򰻇�>R�]�1@�K(k��
����0	@x2�4�+�NRC�<@   LN2_QD_BP_V01      QUAD�� ��?����M3�=x�F]nh�=��=�/Z��G�����!�a�8��t7��JK8�Ck�>�
��^�=L�c���g�Wr��x��ZЗ��:�>�-�λcP�4m�>l;`	L�= /��)�4=����2�=�l�zO1r;��� ��>L��kW6=�U�W��=u�b�-MT;,�Gi �>ōW��2>��� ��;�z��f?��&�Mo<���V��<��.��x3?�<����?h^�9S!?�<�O�	?   �J�>�zJ�+��?   ��=�|�/4*0��l���!��,l�@���y���P�   ���ʾ �˂Uz�   v;o���.��x3?�<����?h^�9S!?�<�O�	?   �J�>�zJ�+��?   ��=�� ��?JK8�Ck�>cP�4m�>��� ��>,�Gi �>�z��f?���V��<���&��=̩�Yޢ�>ߢ���=����>FjVd��=d�\���>.N#Z��=�X����>�j ��2@�4�k���p���	@4�Ҿ��NRC�<@   LN2_COR      KICKER�� ��?����M3�=x�F]nh�=��=�/Z��G�����!�a�8��t7��JK8�Ck�>�
��^�=L�c���g�Wr��x��ZЗ��:�>�-�λcP�4m�>l;`	L�= /��)�4=����2�=�l�zO1r;��� ��>L��kW6=�U�W��=u�b�-MT;,�Gi �>ōW��2>��� ��;�z��f?��&�Mo<���V��<��.��x3?�<����?h^�9S!?�<�O�	?   �J�>�zJ�+��?   ��=�|�/4*0��l���!��,l�@���y���P�   ���ʾ �˂Uz�   v;o���.��x3?�<����?h^�9S!?�<�O�	?   �J�>�zJ�+��?   ��=�� ��?JK8�Ck�>cP�4m�>��� ��>,�Gi �>�z��f?���V��<���&��=̩�Yޢ�>ߢ���=����>FjVd��=d�\���>.N#Z��=�X����>�j ��2@�4�k���p���	@4�Ҿ��NRC�<@   LN2_BPM      MONI�� ��?����M3�=x�F]nh�=��=�/Z��G�����!�a�8��t7��JK8�Ck�>�
��^�=L�c���g�Wr��x��ZЗ��:�>�-�λcP�4m�>l;`	L�= /��)�4=����2�=�l�zO1r;��� ��>L��kW6=�U�W��=u�b�-MT;,�Gi �>ōW��2>��� ��;�z��f?��&�Mo<���V��<��.��x3?�<����?h^�9S!?�<�O�	?   �J�>�zJ�+��?   ��=�|�/4*0��l���!��,l�@���y���P�   ���ʾ �˂Uz�   v;o���.��x3?�<����?h^�9S!?�<�O�	?   �J�>�zJ�+��?   ��=�� ��?JK8�Ck�>cP�4m�>��� ��>,�Gi �>�z��f?���V��<���&��=̩�Yޢ�>ߢ���=����>FjVd��=d�\���>.N#Z��=�X����>�j ��2@�4�k���p���	@4�Ҿ���qȝ�@   LN2_QD_BP_V01      QUAD�x�C'?�ΐ�t�=r���vd�=d1�������O�z����v��a���<ɂd���MN���>�����ќ=b�+X/x�R7�G���|��;A��L
��ӻK85}��>��Z]5�=��g��5=]���W�=�i��`r;u�+����>�H|޺�=��9U�=����?P;�[�Dj �>��;��2><��j��;�z��f?�V�~Mo<�2*L��<�(���3?�����?`�Bd�o!?s��s?   �J�>�zJ�+��?   ���=����@S0�Sɏh[s��}@���bu���   ��ʾ �˂Uz�   �.o��(���3?�����?`�Bd�o!?s��s?   �J�>�zJ�+��?   ���=�x�C'?�MN���>K85}��>u�+����>�[�Dj �>�z��f?�2*L��<q�p���=�E��z��>�����=2Z͘�>�����=�d\�?��>�aY��=��8��>v_wRcr2@1@6����/��)�
@o�����쿹����@   LN2_DR_QD_ED      DRIF�w�jX?|6KB��=��n&_�=�#1|Ӟ���o�����NT��,b�����К���MN���>�%�e4��=b�+X/x��_�H���|��;A�����ӻ<�I��>��%���=�p#m.@5=+��;{�=˽��>�r;u�+����>�����=��9U�=��q�?P;���ok �>� ��2>\����;�z��f?��mMo<����@��<�w{��4?�����?����!?s��s?   �K�>�zJ�+��?   8�=�Q�b��0�Sɏh[s�#�!i��bu���   H|�ʾ �˂Uz�   �o��w{��4?�����?����!?s��s?   �K�>�zJ�+��?   8�=�w�jX?�MN���><�I��>u�+����>���ok �>�z��f?����@��<d�p���=�E��z��>�����=Z͘�>�����=�d\�?��>�aY��=��8��>�=i��2@YU���+yc!@V�3�E���rT�@   LN2_DR_BP_V2      DRIF
���=�?���zӦ>��Z0h����5&�F��hξ�7�ƽ���I�p��]����MN���>U��Q�Y{=b�+X/x�������|��;A���i��ӻ.㘚B>�><x�I�=�%ۭ��B=C)m�=Pu ��;u�+����>!� �=��9U�=+*�,'?P;i�u`� �>�k>�O2>0q�<0��;�z��f?3���Go<�.oY���<;�t]їF?�����?8�����,?s��s?   ��S�>�zJ�+��?   ��=`H8��A�Sɏh[s�4�M�)�*��bu���   ��ʾ �˂Uz�   8(j�;�t]їF?�����?8�����,?s��s?   ��S�>�zJ�+��?   ��=
���=�?�MN���>.㘚B>�>u�+����>i�u`� �>�z��f?�.oY���<��p���=�E��z��>�����=_Z͘�>�����=�d\�?��>�aY��=ݩ�8��>�vkT@%rU'�>7��0@�������h*/��@   LN2_DR_QD_ED      DRIF�g���?m���>���Ғ����q�;�g��p�O V�ƽAT�B|�p��V�4����MN���>��~�Rz=b�+X/x�5��;����|��;A���g��ӻ�|<j�u�>�&�qfq�=q$��C=�P@�@�=Ѹ�s��;u�+����>��L���=��9U�=�:�%?P;��5�� �>-�#�P2>��
�'��;�z��f?'$�h�Go<�`|��<
4�s�F?�����?���F�-?s��s?   ^�S�>�zJ�+��?   ���=���"�A�Sɏh[s���AN?(+��bu���   ��ʾ �˂Uz�   ,j�
4�s�F?�����?���F�-?s��s?   ^�S�>�zJ�+��?   ���=�g���?�MN���>�|<j�u�>u�+����>��5�� �>�z��f?�`|��<��p���=�E��z��>�����=UZ͘�>�����=�d\�?��>�aY��=��8��>s���OKT@�O�Cj1'��|��31@:��$���Ĺ��&@   LN2_QD_BP_V02      QUAD����?�r*�f$����Se�����!D]��Vm��M�ƽ��u���p�&_�jW��?v��"� ?�.c^�D�=�-b;�=�cw�ų=�J�2�J]>6b�O�C�;� 0���>[�g��>V����`C=CVO��=��(��;c0���T�>����*:=e�w_#�=M8�0�v;���G� �>A�
#T2>��@#��;�z��f?YD��Go<���s��<gS�1:�F?��ڽ��2?�b*�v�-?�)UN�&?   v�S�>�zJ�+��?   >�=�&6z�A���ڽ��2�;p�?5�+�ǯ�<��#�   �ϝʾ �˂Uz�   x�i�gS�1:�F?����,?�b*�v�-?�)UN�&?   v�S�>�zJ�+��?   >�=����??v��"� ?� 0���>c0���T�>���G� �>�z��f?���s��<�.z&��=_������>�C�xZx�=��銶P�>���	=��=?>-,�~�>��2��=f�D��~�>n�9�KT@���g��@@h�&L}�1@�
8�ܧ)��Ĺ��&@   LN2_COR      KICKER����?�r*�f$����Se�����!D]��Vm��M�ƽ��u���p�&_�jW��?v��"� ?�.c^�D�=�-b;�=�cw�ų=�J�2�J]>6b�O�C�;� 0���>[�g��>V����`C=CVO��=��(��;c0���T�>����*:=e�w_#�=M8�0�v;���G� �>A�
#T2>��@#��;�z��f?YD��Go<���s��<gS�1:�F?��ڽ��2?�b*�v�-?�)UN�&?   v�S�>�zJ�+��?   >�=�&6z�A���ڽ��2�;p�?5�+�ǯ�<��#�   �ϝʾ �˂Uz�   x�i�gS�1:�F?����,?�b*�v�-?�)UN�&?   v�S�>�zJ�+��?   >�=����??v��"� ?� 0���>c0���T�>���G� �>�z��f?���s��<�.z&��=_������>�C�xZx�=��銶P�>���	=��=?>-,�~�>��2��=f�D��~�>n�9�KT@���g��@@h�&L}�1@�
8�ܧ)��Ĺ��&@   LN2_BPM      MONI����?�r*�f$����Se�����!D]��Vm��M�ƽ��u���p�&_�jW��?v��"� ?�.c^�D�=�-b;�=�cw�ų=�J�2�J]>6b�O�C�;� 0���>[�g��>V����`C=CVO��=��(��;c0���T�>����*:=e�w_#�=M8�0�v;���G� �>A�
#T2>��@#��;�z��f?YD��Go<���s��<gS�1:�F?��ڽ��2?�b*�v�-?�)UN�&?   v�S�>�zJ�+��?   >�=�&6z�A���ڽ��2�;p�?5�+�ǯ�<��#�   �ϝʾ �˂Uz�   x�i�gS�1:�F?����,?�b*�v�-?�)UN�&?   v�S�>�zJ�+��?   >�=����??v��"� ?� 0���>c0���T�>���G� �>�z��f?���s��<�.z&��=_������>�C�xZx�=��銶P�>���	=��=?>-,�~�>��2��=f�D��~�>n�9�KT@���g��@@h�&L}�1@�
8�ܧ)�� I��O@   LN2_QD_BP_V02      QUAD�X��+?�6gD�6���A�:���!�0
L���_�O��Ž	�*�Vp��u��.�Y���,?�ok�p�=Sږ��=<О�Oi�=��5�p>7G�>�<�,Q�� ?���L> I�<�D=>8���=ϓ�����;�p��+�?㛹L$H=®�Xݻ�=L��S��;
�i;� �>@��o2>ⱓ[D��;�z��f?�$!X�Go<�R�с��</�H�k F?�����E?�J�˱.?!�6n0J3?   \�U�>�zJ�+��?   ⧌=���>�@A������E�[��b��,�3�~Pp�1�   \ߜʾ �˂Uz�   �i�/�H�k F?���<A?�J�˱.?!�6n0J3?   \�U�>�zJ�+��?   ⧌=�X��+?Y���,?�,Q�� ?�p��+�?
�i;� �>�z��f?�R�с��<,�u�Ac�=x_R���>2Q��`��=�;�����>	�x�'��=[����x�>�8����=�#��x�>�,_���R@��j��R@$�2��X3@��T�7��r�I{@   LN2_DR_QD_ED      DRIF��\�P[?]D��5�o�� �Ǜ�~!�ǓV��d���Ž�
�"Bo�b�[��Y�Y���,?1�=�[�=Sږ��=�R��}i�=��5�p>��cu^�<KVH��� ?�fB�,>]�1U'E=�J��K�=p����x�;�p��+�?���b%H=®�Xݻ�=L�I���;�-
r&�>WK��2>��M���;�z��f?#B�:Ho<Jߢ{���<
��ÃE?�����E?�TV�!0?!�6n0J3?   ��Y�>�zJ�+��?   ��=ń~�Ά@������E�H��<.�3�~Pp�1�   ��ʾ �˂Uz�   Xrg�
��ÃE?���<A?�TV�!0?!�6n0J3?   ��Y�>�zJ�+��?   ��=��\�P[?Y���,?KVH��� ?�p��+�?�-
r&�>�z��f?Jߢ{���<��u�Ac�=�qR���>�]��`��=�M�����>:�x�'��=8����x�>:����=�$��x�>�[UsPQ@f�u��R@E+q�o5@��i>69�ܼj�%H@   LN2_DR_BP_V3      DRIFYRg��
?�Ky3f1��z>�����!��dC����pvM���?�:4�h�g��y����Y���,?vh�b�=Sږ��=�-�7Vj�=��5�p>�����<�Ů�_�?�(�!*>X��kvJ=�-Q52�=f�?l���;�p��+�?�ճ�+H=®�Xݻ�=�r��L�;�Ĭ	&�>���2>K��.���;�z��f?��hIo<����M��<t����@?�����E?�>����3?!�6n0J3?   �6k�>�zJ�+��?   4ٟ=�V�>7:������E�B�2���2�3�~Pp�1�   �a�ʾ �˂Uz�   �_�t����@?���<A?�>����3?!�6n0J3?   �6k�>�zJ�+��?   4ٟ=YRg��
?Y���,?�Ů�_�?�p��+�?�Ĭ	&�>�z��f?����M��<s�u�Ac�=�hR���>W��`��=)D�����>��x�'��=~����x�>�7����=#��x�>�����E@�X_mґL@@x7[@@^��C'?��#��s@   LN2_DR_QD_ED      DRIF��`n�i?h�(�2l0��j4fc��x�j�V���mJ��F,�����0g��Yz��6��Y���,?�v�:�M�=Sږ��=@��/�j�=��5�p>);�1�<?�י��?����9>�G���
K=�VW'��=OO���;�p��+�?����-H=®�Xݻ�=��|u�;��k\�>��n��2>�k1e��;�z��f?��z�Io<u
��q��<�W�u�??�����E?z@���4?!�6n0J3?   0�n�>�zJ�+��?   6�=�j�$!�8������E�9M8!f3�3�~Pp�1�   h��ʾ �˂Uz�   ��]��W�u�??���<A?z@���4?!�6n0J3?   0�n�>�zJ�+��?   6�=��`n�i?Y���,??�י��?�p��+�?��k\�>�z��f?u
��q��<��u�Ac�=�qR���>N^��`��=�N�����>:�x�'��=8����x�>:����=�$��x�>�ة��TC@��"I�J@�D�'�A@h���15@��j�a��@   LN2_QD_BP_V03      QUAD��i�D,
?+�&nr#�7V�o�ӝ��W���K����9����5[,�f��T�A����͢Ya��?�h�Z�=X�+Y?��=:���>�=���b��d>F�t��;��3��_?I�#خ$>��@նK=���T�p�=W�#�3�;>�Q��> ̕�g2=#$����=��sp;�E�K|�>y�^~�2> .f�?��;�z��f?��m��Io<CT'���<S���� >?�2:7�:?�F%!5?�7,)��?   n&q�>�zJ�+��?   v4�=��Pq�7��2:7�:��6/�\�3��i��    _�ʾ �˂Uz�    �\�S���� >?��o��4?�F%!5?�7,)��?   n&q�>�zJ�+��?   v4�=��i�D,
?͢Ya��?��3��_?>�Q��>�E�K|�>�z��f?CT'���<9�U�=�=�n1�k�>�3�����={͔n�u�>+���2��=i�W�>���+(��=?��셃>w�4���A@}�^�@@шUػ�B@�w��K*��j�a��@   LN2_COR      KICKER��i�D,
?+�&nr#�7V�o�ӝ��W���K����9����5[,�f��T�A����͢Ya��?�h�Z�=X�+Y?��=:���>�=���b��d>F�t��;��3��_?I�#خ$>��@նK=���T�p�=W�#�3�;>�Q��> ̕�g2=#$����=��sp;�E�K|�>y�^~�2> .f�?��;�z��f?��m��Io<CT'���<S���� >?�2:7�:?�F%!5?�7,)��?   n&q�>�zJ�+��?   v4�=��Pq�7��2:7�:��6/�\�3��i��    _�ʾ �˂Uz�    �\�S���� >?��o��4?�F%!5?�7,)��?   n&q�>�zJ�+��?   v4�=��i�D,
?͢Ya��?��3��_?>�Q��>�E�K|�>�z��f?CT'���<9�U�=�=�n1�k�>�3�����={͔n�u�>+���2��=i�W�>���+(��=?��셃>w�4���A@}�^�@@шUػ�B@�w��K*��j�a��@   LN2_BPM      MONI��i�D,
?+�&nr#�7V�o�ӝ��W���K����9����5[,�f��T�A����͢Ya��?�h�Z�=X�+Y?��=:���>�=���b��d>F�t��;��3��_?I�#خ$>��@նK=���T�p�=W�#�3�;>�Q��> ̕�g2=#$����=��sp;�E�K|�>y�^~�2> .f�?��;�z��f?��m��Io<CT'���<S���� >?�2:7�:?�F%!5?�7,)��?   n&q�>�zJ�+��?   v4�=��Pq�7��2:7�:��6/�\�3��i��    _�ʾ �˂Uz�    �\�S���� >?��o��4?�F%!5?�7,)��?   n&q�>�zJ�+��?   v4�=��i�D,
?͢Ya��?��3��_?>�Q��>�E�K|�>�z��f?CT'���<9�U�=�=�n1�k�>�3�����={͔n�u�>+���2��=i�W�>���+(��=?��셃>w�4���A@}�^�@@шUػ�B@�w��K*��A$��@   LN2_QD_BP_V03      QUAD�R'r�	?Z؏���c��%����%�N����5<��缽*I��e�R���<�����&��>��-n��="M�+��X=7�3��=�F厱R>��fzm�;WnL/r?��'����y�K=��&|�=���8B�;h�61*c�>�,[C�'�].�X7�ѽMίI�d�c�.#��>�ʱ:�2>E�E�A��;�z��f?��9��Io<�+~}��<�֝�a=?
���3%?���7�/5?���=�L?   ��q�>�zJ�+��?   6��=��+�7�
���3%� �qJ��3����=�L�   ���ʾ �˂Uz�   �l\��֝�a=?,��$�L ?���7�/5?.�g�a?   ��q�>�zJ�+��?   6��=�R'r�	?���&��>WnL/r?h�61*c�>c�.#��>�z��f?�+~}��<9�Ě��=R�z�~ӌ>��@�~�=}��{�X�>���1�$�=�� m���>�˿�$�=��u�y��>�@e�@@��p8�)@R���ΏB@�h?6z@�B�@   LN2_DR_QD_ED      DRIF
*��'	?!������J���ye���d�u����u#�zb��xU+�e�o��|������&��>�i习="M�+��X=�����=�F厱R>�`�rm�;O`�ZC?��o�V����� ��K=d���K�=S���x	�;h�61*c�>��^�O�'�].�X7�ѽ6(�J�d�<���>��2�2>Օ�;��;�z��f?�^���Io<ߗ�s��<�����<?
���3%?�J�� 5?���=�L?   ��q�>�zJ�+��?   �=c:���6�
���3%���
���3����=�L�   tڎʾ �˂Uz�   �D\������<?,��$�L ?�J�� 5?.�g�a?   ��q�>�zJ�+��?   �=
*��'	?���&��>O`�ZC?h�61*c�><���>�z��f?ߗ�s��<��Ě��=��z�~ӌ>�@�~�=���{�X�>���1�$�=�� m���>7�˿�$�=��u�y��>�����q@@;b��	B)@ymC�BB@L�#۞=@�N{M|� @   LN2_DR_BP_V4      DRIF����i?�7�h�<���MY�i������:���i�v'��� T�%.�b�c��R������&��>3� ��="M�+��X=������=�F厱R>��C<m�;���^??��}y���ν����I=�
���=�
��虆;h�61*c�>�EӠ�'�].�X7�ѽ��FP�d��@g��>yash2>C�����;�z��f?B���kIo<���4��<Em��:?
���3%?��	@$�3?���=�L?   ~s�>�zJ�+��?   *"�=�4��}4�
���3%��l��ļ2����=�L�   H�ʾ �˂Uz�   `[�Em��:?,��$�L ?��	@$�3?.�g�a?   ~s�>�zJ�+��?   *"�=����i?���&��>���^??h�61*c�>�@g��>�z��f?���4��<9�Ě��=R�z�~ӌ>��@�~�=i��{�X�>���1�$�=q� m���>�˿�$�=��u�y��>��:.�N:@P��T�&@Mre��^@@�Z��1�@����� @   LN2_DR_BP_0      DRIFI���Bx?�=u������ۈ����ۿ[���A�I�΍��: d�`���NJ������&��>,����="M�+��X=�y�!#��=�F厱R>�>�&m�;P[��?8?Q4�F����A�g��H=�P'.�=��M׎�;h�61*c�>������'�].�X7�ѽg�tT�d�L�rǧ�>�^2>���:���;�z��f?%3+Io<k�j���<QHr���7?
���3%?{_uV�63?���=�L?   V�s�>�zJ�+��?   ��=k9���2�
���3%��U�|��1����=�L�   8T�ʾ �˂Uz�   �Z�QHr���7?,��$�L ?{_uV�63?.�g�a?   V�s�>�zJ�+��?   ��=I���Bx?���&��>P[��?8?h�61*c�>L�rǧ�>�z��f?k�j���<9�Ě��=R�z�~ӌ>��@�~�=i��{�X�>���1�$�=� m���>��˿�$�=��u�y��>��{��5@�V(M��$@�Ύ��>@�S��@�N{M|!@   LN2_DR_BP_BL      DRIF��Xd��?4?�^2B���O�����2lF�ꍽ�����@���W_`� �Bh�&����&��>̇eXz"�="M�+��X=P�v&��=�F厱R>�Z�_m�;+5�H?���x'y���	���cH=�h���=W��L�;h�61*c�>RW��'�].�X7�ѽV�sU�d��b�8��>�d�2>B�����;�z��f?�w�Io<�*�s���<$�3�L\7?
���3%?,S\3?���=�L?   N7t�>�zJ�+��?   JA�=��-�4t2�
���3%����#}�1����=�L�   �7�ʾ �˂Uz�   �Z�$�3�L\7?,��$�L ?,S\3?.�g�a?   N7t�>�zJ�+��?   JA�=��Xd��?���&��>+5�H?h�61*c�>�b�8��>�z��f?�*�s���<��Ě��=��z�~ӌ>D�@�~�=κ�{�X�>���1�$�=�� m���>#�˿�$�=��u�y��>}�bA	�4@(���!$@�Ų��|=@��ɦ�P@��-/0!@   LN2_DR_SCA_ED      DRIF�*p�-?���RD�W.&F-�� @]�ϧ��&|�I봽��OQ-=_�'���"B����&��>���3�="M�+��X=L�u�*��=�F厱R>6]��l�;�8�̥?����z����{�j�G=A��� ��=
�y�܄;h�61*c�>p���'�].�X7�ѽ(.6W�d�C:r���>uK�2>������;�z��f?��* Io<���b��<6�B_^z6?
���3%?�+M0�2?���=�L?   (�t�>�zJ�+��?   蠨=!a�	��1�
���3%���h:�u1����=�L�   P�ʾ �˂Uz�   �vZ�6�B_^z6?,��$�L ?�+M0�2?.�g�a?   (�t�>�zJ�+��?   蠨=�*p�-?���&��>�8�̥?h�61*c�>C:r���>�z��f?���b��<l�Ě��=��z�~ӌ>޳@�~�=���{�X�>���1�$�=�� m���>7�˿�$�=��u�y��>�1a�S3@�iD��Q#@��\��q<@з��@�}/�BA"@   LN2_SCA0      RFDFC��=�>�F�k� �x~���x����������D>���Η%3��U�S������&��>�@��G��="M�+��X=u��I��=�F厱R>�a)Ēl�;����\?�9L�T�����d˴D=��;,l�=����}�;h�61*c�>�b��'�].�X7�ѽ�y�Hb�d�oH���>*P��D2>c�q���;�z��f?�D��SHo<H�_6n��<�ӵ���0?
���3%?W�L���0?���=�L?   ��v�>�zJ�+��?   ��=H
���*�
���3%���z`��.����=�L�   �؋ʾ �˂Uz�   \Y��ӵ���0?,��$�L ?W�L���0?.�g�a?   ��v�>�zJ�+��?   ��=C��=�>���&��>����\?h�61*c�>oH���>�z��f?H�_6n��<�Ě��=,�z�~ӌ>��@�~�==��{�X�>���1�$�=�� m���>�˿�$�=��u�y��>�~�L�$@�(��9@��C|66@Ϻ>6��@�}/�BA"@   LN2_SZW      WAKEC��=�>���o� �x~���x�T�6f����#����T-
²�U� F���#�^+��>���K��=}Ӟ��X=��H�/��=��$FR>1���l�;����\?Ѳ��Y��^$���D=1|�{�g�=k,,�}�;2��.c�>o?G��'����t�ѽ�J�f�d����>�@�*�2>]�,w��;����f?	vl�Bo<��c6n��<�ӵ���0?�����3%?W�L���0?
x��L?   2�v�>��vô�?   ��=H
���*������3%���z`��.�
x��L�   t��ʾ"���Wz�   TY��ӵ���0?�����L ?W�L���0?�Lm��a?   2�v�>��vô�?   ��=C��=�>�#�^+��>����\?2��.c�>���>����f?��c6n��<��sB���=��Byӌ>S�IGD~�=5=��BY�>���p�$�=�N%~��>����$�=XN�v��>�D��$@ɴ�9@� �v66@�y���@�}/�BA"@   LN2_STW      TRWAKEC��=�>��S�o� �x~���x�B�Yc����#����T-
²�U� F��έBa+��>����K��=��Ń��X=wF�/��=���4FR>�'"�l�;����\?�К�Y��^$���D=1|�{�g�=k,,�}�;Ο�.c�>�+�d��'�X��!t�ѽЎ)f�d����>�@�*�2>]�,w��;����f?	vl�Bo<��c6n��<�ӵ���0?��]��3%?W�L���0?*8��L?   2�v�>��vô�?   ��=H
���*���]��3%���z`��.�*8��L�   t��ʾ"���Wz�   TY��ӵ���0?����L ?W�L���0?��^��a?   2�v�>��vô�?   ��=C��=�>έBa+��>����\?Ο�.c�>���>����f?��c6n��<�""Y���=s�hcyӌ>��9OD~�=�|6�BY�>�4�p�$�=W�N%~��>�{��$�=B?�v��>�����$@^k�|9@2�v66@8
z���@����k"@   LN2_DR_SCA_ED      DRIFQ����h�>$_cG���`ѷ���q�t������㓈4#�����yT���N�L�έBa+��>��lb߭�=��Ń��X={�Q�4��=���4FR>�Uk˅l�;��?��}֬*�-�� n5D=ɓ"M��=5��Q��;Ο�.c�>��c��'�X��!t�ѽ�v�g�d��,غ�>
�}��2>��ȅs��;����f?��<��Bo<�:p*[��<_^�*6�/?��]��3%?r|���x0?*8��L?   Gw�>��vô�?   Fe�=T�� h)���]��3%��1�Xh1.�*8��L�   ��ʾ"���Wz�   �X�_^�*6�/?����L ?r|���x0?��^��a?   Gw�>��vô�?   Fe�=Q����h�>έBa+��>��?Ο�.c�>�,غ�>����f?�:p*[��<�""Y���=s�hcyӌ>��9OD~�=�|6�BY�>�4�p�$�=W�N%~��>�{��$�=B?�v��>�c��"@������@|_�CUO5@����j@�N{M|�"@   LN2_DR_BP_BL      DRIFǞ	r�>��y+�����w���j���t̔��txS&z��a�*S�O��=�:�έBa+��>OEnԷ�=��Ń��X=��<�7��=���4FR>���|l�;��t�� ?N�X�5��Z�"���C=̵|�8�=�3܌b�;Ο�.c�>�����'�X��!t�ѽ�2��h�d�jm5J��>^��v2>?�Yl��;����f?d��d�Bo<o��O��<n�[H��.?��]��3%?�N4}J0?*8��L?   �w�>��vô�?   ���=�6�.î(���]��3%��nنK�-�*8��L�   �o�ʾ"���Wz�   �X�n�[H��.?����L ?�N4}J0?��^��a?   �w�>��vô�?   ���=Ǟ	r�>έBa+��>��t�� ?Ο�.c�>jm5J��>����f?o��O��<�""Y���=��hcyӌ>�9OD~�=�|6�BY�>�4�p�$�=W�N%~��>�{��$�=B?�v��>�	�
�:!@B��K��@�[;'�4@��#@�����"@   LN2_DR_BP_0      DRIF�ʁ*���>t�?\���ڠ�RdQ=��������t�	����]��NO�E �����έBa+��>�Nt��ߡ=��Ń��X=�V�#C��=���4FR>D���Tl�;��;(���>�x�9Z���/�Q)�B=�=��`p�=��yW�;Ο�.c�>E��k��'�X��!t�ѽ�.&m�d������>��j2>(�O�O��;����f?�',�PBo<a���!��<���G�*?��]��3%?�.W��/?*8��L?   �cx�>��vô�?   j��=0�h/l%���]��3%�ud.@�F,�*8��L�   ���ʾ"���Wz�   $X����G�*?����L ?�.W��/?��^��a?   �cx�>��vô�?   j��=�ʁ*���>έBa+��>��;(���>Ο�.c�>�����>����f?a���!��<�""Y���=s�hcyӌ>��9OD~�=�|6�BY�>�4�p�$�=W�N%~��>�{��$�=B?�v��>����U�@�D� T�@��Q@A�2@���j@Á����#@	   LN2_DR_BP      DRIF+��T�>(�qͽZ�-y޺s�=�M��춉�/ᧁ��
�8}�@�,NZ���һέBa+��>Ya��K/�=��Ń��X=�7uCZ��=���4FR>�/��l�;觬��y�>'�=�E1�s���tS@=�M�[��=_�y��|;Ο�.c�>1�)d�'�X��!t�ѽ�{[�u�d������>���R-2>���J��;����f?�Zˣ�Ao<��wƉ�< ߉�0"?��]��3%?R4��0,?*8��L?   �+z�>��vô�?   �O�=)�����]��3%���}w)�*8��L�   ��ʾ"���Wz�   @ W� ߉�0"?����L ?R4��0,?��^��a?   �+z�>��vô�?   �O�=+��T�>έBa+��>觬��y�>Ο�.c�>�����>����f?��wƉ�<�""Y���=i�hcyӌ>��9OD~�=�|6�BY�>�4�p�$�=^�N%~��>�{��$�=I?�v��>,�$y@v>�C�@ �24��-@i9��%�@�����#@   LN2_DR_BP_VS      DRIF�A@��N�>*�a����3+�.�=>;�|Gg���Q�8���l/��:��mU��̻έBa+��>f�I5C�=��Ń��X=��S`��=���4FR> �vD�k�;�n4���>h{� j?����Qu?=��j�C��=5�␋w{;Ο�.c�>�����'�X��!t�ѽ|#�w�d������>��L42>�U����;����f?�C�X�Ao<u�����<{�����?��]��3%?ך+��u+?*8��L?   z�z�>��vô�?   �®=ϝ#�{����]��3%����"��(�*8��L�   ���ʾ"���Wz�   D�V�{�����?����L ?ך+��u+?��^��a?   z�z�>��vô�?   �®=�A@��N�>έBa+��>�n4���>Ο�.c�>�����>����f?u�����<�""Y���=s�hcyӌ>��9OD~�=�|6�BY�>�4�p�$�=l�N%~��>�{��$�=W?�v��>�H;CbL @��k��@e��,@�Aۯ�=@Á����$@	   LN2_DR_BP      DRIF�r{���>#G����ѽӡ�=�^�=v�W\�(��A��#�Dh=ҙ�Ï�>w12�3b�;έBa+��>�x��ڒ�=��Ń��X=�h�*w��=���4FR>�+��k�;?r�AA�>oP]>�w�Y\��d�:=��;S�m�=���5Kw;Ο�.c�>�Ӆ-��'�X��!t�ѽ@���d�Ļ� 
�>m��4P2>��7����;����f?Z=�-.Ao<�T��<�~k?O�?��]��3%?����(?*8��L?   .e|�>��vô�?   (��=�ڥY���]��3%����Ϊr&�*8��L�    ��ʾ"���Wz�   `�U��~k?O�?����L ?����(?��^��a?   .e|�>��vô�?   (��=�r{���>έBa+��>?r�AA�>Ο�.c�>Ļ� 
�>����f?�T��<�""Y���=s�hcyӌ>��9OD~�=�|6�BY�>�4�p�$�=l�N%~��>�{��$�=W?�v��>�2,�A�?�Ac~��?��!�%@�ƶ�@Á����$@   LN2_BP_WA_OU      WATCH�r{���>#G����ѽӡ�=�^�=v�W\�(��A��#�Dh=ҙ�Ï�>w12�3b�;έBa+��>�x��ڒ�=��Ń��X=�h�*w��=���4FR>�+��k�;?r�AA�>oP]>�w�Y\��d�:=��;S�m�=���5Kw;Ο�.c�>�Ӆ-��'�X��!t�ѽ@���d�Ļ� 
�>m��4P2>��7����;����f?Z=�-.Ao<�T��<�~k?O�?��]��3%?����(?*8��L?   .e|�>��vô�?   (��=�ڥY���]��3%����Ϊr&�*8��L�    ��ʾ"���Wz�   `�U��~k?O�?����L ?����(?��^��a?   .e|�>��vô�?   (��=�r{���>έBa+��>?r�AA�>Ο�.c�>Ļ� 
�>����f?�T��<�""Y���=s�hcyӌ>��9OD~�=�|6�BY�>�4�p�$�=l�N%~��>�{��$�=W?�v��>�2,�A�?�Ac~��?��!�%@�ƶ�@