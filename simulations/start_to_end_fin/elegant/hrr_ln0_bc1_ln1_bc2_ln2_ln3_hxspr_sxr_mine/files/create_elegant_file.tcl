

set fileele [open $filename.ele w]

puts $fileele "
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! beamline for XLS
!! matching along linac
!! [clock format [clock seconds] -format "%B %Y"]
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"

set watches $indist_name
set watches2 ""

#  linac 0
if {$case0 > 0 } {
    
    set indstindx [lindex $watches end end]
    set sectext   "ln0"
    set parname   "$sectext\_in_pars.sdds"
    
    set fileele   [open $filename\_$sectext.match.ele w]
    set fileeletr [open $filename\_$sectext.track.ele w]
    set elegant_match "$filename\_$sectext.match.ele"
    set elegant_track "$filename\_$sectext.track.ele"
    
puts $fileele "

!------------------------------------------------------------------------------
! tuning LN0 matching section
!------------------------------------------------------------------------------
&subprocess command = \"sddsanalyzebeam $indstindx $parname \" &end


&run_setup
    lattice       = \"$filename.$indx.lte\",
    use_beamline  = \"MATCHINS_$indx\",
    p_central     = \"\{sdds2stream $parname -col=pCentral\}\",
    default_order = 1, concat_order  = 1,    
&end

&run_control
    n_steps = 1, 
    reset_rf_for_each_step=1
&end


&rpn_load tag = input, filename = \"$parname\" &end

&twiss_output
    matched = 0,
    beta_x  = \"(input.betax)\",
    alpha_x = \"(input.alphax)\",
    beta_y  = \"(input.betay)\",
    alpha_y = \"(input.alphay)\",
    statistics = 1;  
    radiation_integrals = 1;  
&end

&optimization_setup
	matrix_order  = 2,
	mode          = \"minimize\", 
	method        = \"simplex\",
	target        = 0.0,
	tolerance     = 1e-16, 
	n_evaluations = 1000, 
	n_passes      = 5, 
	n_restarts    = 15,
	log_file      = \"/dev/tty\",  
	verbose       = 0,
	output_sparsing_factor = 20,
&end

&optimization_term     term = \"LN0_FITP1#1.betax LN0_FITP2#1.betay  1e-8 sene \" &end
&optimization_term     term = \"LN0_FITP1#1.betay LN0_FITP2#1.betax  1e-8 sene \" &end
&optimization_term     term = \"LN0_FITP1#1.betax LN0_FITP3#1.betax  1e-8 sene \" &end
&optimization_term     term = \"LN0_FITP1#1.betay LN0_FITP3#1.betay  1e-8 sene \" &end
&optimization_term     term = \"LN0_FITP1#1.betax LN0_FITP4#1.betay  1e-8 sene \" &end
&optimization_term     term = \"LN0_FITP1#1.betay LN0_FITP4#1.betax  1e-8 sene \" &end
&optimization_term     term = \"LN0_FITP2#1.betax LN0_FITP3#1.betay  1e-8 sene \" &end
&optimization_term     term = \"LN0_FITP2#1.betay LN0_FITP3#1.betax  1e-8 sene \" &end
&optimization_term     term = \"LN0_FITP2#1.betax LN0_FITP4#1.betax  1e-8 sene \" &end
&optimization_term     term = \"LN0_FITP2#1.betay LN0_FITP4#1.betay  1e-8 sene \" &end
&optimization_term     term = \"LN0_FITP3#1.betax LN0_FITP4#1.betay  1e-8 sene \" &end
&optimization_term     term = \"LN0_FITP3#1.betay LN0_FITP4#1.betax  1e-8 sene \" &end


&optimization_term     term = \"LN0_FITP1#1.betax 15 1e-5 segt  \" &end
&optimization_term     term = \"LN0_FITP1#1.betay 15 1e-5 segt  \" &end
&optimization_term     term = \"LN0_FITP2#1.betax 15 1e-5 segt  \" &end
&optimization_term     term = \"LN0_FITP2#1.betay 15 1e-5 segt  \" &end
&optimization_term     term = \"LN0_FITP3#1.betax 15 1e-5 segt  \" &end
&optimization_term     term = \"LN0_FITP3#1.betay 15 1e-5 segt  \" &end
&optimization_term     term = \"LN0_FITP4#1.betax 15 1e-5 segt  \" &end
&optimization_term     term = \"LN0_FITP4#1.betay 15 1e-5 segt  \" &end

&optimization_term     term = \"LN0_FITP1#1.alphax 0.0  1e-8 sene \" &end
&optimization_term     term = \"LN0_FITP1#1.alphay 0.0  1e-8 sene \" &end
&optimization_term     term = \"LN0_FITP2#1.alphax 0.0  1e-8 sene \" &end
&optimization_term     term = \"LN0_FITP2#1.alphay 0.0  1e-8 sene \" &end
&optimization_term     term = \"LN0_FITP3#1.alphax 0.0  1e-8 sene \" &end
&optimization_term     term = \"LN0_FITP3#1.alphay 0.0  1e-8 sene \" &end
&optimization_term     term = \"LN0_FITP4#1.alphax 0.0  1e-8 sene \" &end
&optimization_term     term = \"LN0_FITP4#1.alphay 0.0  1e-8 sene \" &end

&optimization_term     term = \"max.betax 40 0.1 segt \" &end
&optimization_term     term = \"max.betay 40 0.1 segt \" &end

&optimization_term     term = \"LN0_FITP01#1.betax 15 0.1 segt \" &end
&optimization_term     term = \"LN0_FITP01#1.betay 15 0.1 segt \" &end


&optimization_variable  name=LN0_QD_V01, item=K1, lower_limit=[expr -2*$ln0_quad_k], upper_limit=[expr 2*$ln0_quad_k], step_size=1.e-8  &end
&optimization_variable  name=LN0_QD_V02, item=K1, lower_limit=[expr -2*$ln0_quad_k], upper_limit=[expr 2*$ln0_quad_k], step_size=1.e-8  &end
&optimization_variable  name=LN0_QD_V03, item=K1, lower_limit=[expr -2*$ln0_quad_k], upper_limit=[expr 2*$ln0_quad_k], step_size=1.e-8  &end

&optimization_variable  name=LN0_QD_V04, item=K1, lower_limit=[expr -2*$ln0_quad_k], upper_limit=[expr 2*$ln0_quad_k], step_size=1.e-8  &end
&optimization_variable  name=LN0_QD_V05, item=K1, lower_limit=[expr -3*$ln0_quad_k], upper_limit=[expr 3*$ln0_quad_k], step_size=1.e-8  &end
&optimization_variable  name=LN0_QD_V06, item=K1, lower_limit=[expr -3*$ln0_quad_k], upper_limit=[expr 3*$ln0_quad_k], step_size=1.e-8  &end


!&optimization_variable  name=LN0_DR_V1, item=L, lower_limit=0.2, upper_limit=1.5, step_size=1.e-8  &end
!&optimization_variable  name=LN0_DR_V2, item=L, lower_limit=0.2, upper_limit=1, step_size=1.e-8  &end
!&optimization_variable  name=LN0_DR_V3, item=L, lower_limit=0.2, upper_limit=1, step_size=1.e-8  &end
!&optimization_variable  name=LN0_DR_V4, item=L, lower_limit=0.2, upper_limit=1.5, step_size=1.e-8  &end

!&optimization_variable  name=LN0_DR_V5, item=L, lower_limit=0.2, upper_limit=1.0, step_size=1.e-8  &end
!&optimization_variable  name=LN0_DR_V6, item=L, lower_limit=0.2, upper_limit=1.5, step_size=1.e-8  &end
!&optimization_variable  name=LN0_DR_V7, item=L, lower_limit=0.2, upper_limit=1.5, step_size=1.e-8  &end
!&optimization_variable  name=LN0_DR_V8, item=L, lower_limit=0.2, upper_limit=1.5, step_size=1.e-8  &end



&bunched_beam n_particles_per_bunch = 1 &end

&optimize summarize_setup=1 &end
"
incr indx
puts "matched index is $indx"
puts $fileele "
&save_lattice filename =\"$filename.$indx.lte\" &end
"
close $fileele

set twissfiles "$filename\_$sectext.$indx.twi"
set sigmafiles "$filename\_$sectext.$indx.sig"

puts $fileeletr "
!-----------------------------------------------------------------------------
! Computing twiss along ln0 section 
!------------------------------------------------------------------------------

&run_setup
    lattice      = \"$filename.$indx.lte\",
    use_beamline = \"TRACKINS_$indx\",
    p_central    = \"\{sdds2stream $parname -col=pCentral\}\",
    default_order= 2, concat_order  = 0, always_change_p0 = 1,
    final        = \"$filename\_$sectext.$indx.fin\"
    magnets      = \"$filename\_$sectext.$indx.mag\"
    parameters   = \"$filename\_$sectext.$indx.param\"
    centroid      = \"$filename\_$sectext.$indx.cent\"
    sigma         = \"$filename\_$sectext.$indx.sig\"
&end

&run_control n_steps = 1, reset_rf_for_each_step=1 &end

&rpn_load   tag = input, filename = \"$parname\" &end

&twiss_output
    matched = 0,
    beta_x  = \"(input.betax)\",
    alpha_x = \"(input.alphax)\",
    beta_y  = \"(input.betay)\",
    alpha_y = \"(input.alphay)\",
    filename = \"$filename\_$sectext.$indx.twi\",
    statistics = 1;  
    radiation_integrals = 1;  
&end


&matrix_output
    SDDS_output = \"$filename\_$sectext.$indx.mtr\", SDDS_output_order = 2
    printout    = \"$filename\_$sectext.$indx.mpr\", printout_order = 1
&end

&sdds_beam
    input = \"$indstindx\",
    center_arrival_time=0,
&end

&floor_coordinates
    filename = \"$filename\_$sectext.$indx.flr\",
&end

&track  &end

"
close $fileeletr
lappend watches  ln0_lh_ou.sdds ln0_lnz_in.sdds ln0_ou.sdds
lappend watches2 ln0_lh_oua.sdds ln0_lnz_ina.sdds ln0_oua.sdds
}


#  bc1 0
if {$case1 > 0 } {
    
    set indstindx [lindex $watches end end]
    set sectext   "bc1"
    set parname   "$sectext\_in_pars.sdds"
    
    set fileele   [open $filename\_$sectext.match.ele w]
    set fileeletr [open $filename\_$sectext.track.ele w]
    lappend elegant_match "$filename\_$sectext.match.ele"
    lappend elegant_track "$filename\_$sectext.track.ele"
    
puts $fileele "
!------------------------------------------------------------------------------
! tuning BC1 matching section
!------------------------------------------------------------------------------
&subprocess command = \"sddsanalyzebeam $indstindx $parname \" &end


&run_setup
    lattice       = \"$filename.$indx.lte\",
    use_beamline  = \"MATCHINS_$indx\",
    p_central     = \"\{sdds2stream $parname -col=pCentral\}\",
    default_order = 1, concat_order  = 1,    
&end

&run_control
    n_steps = 1, 
    reset_rf_for_each_step=1
&end


&rpn_load tag = input, filename = \"$parname\" &end

&twiss_output
    matched = 0,
    beta_x  = \"(input.betax)\",
    alpha_x = \"(input.alphax)\",
    beta_y  = \"(input.betay)\",
    alpha_y = \"(input.alphay)\",
    statistics = 1;  
    radiation_integrals = 1;  
&end

&optimization_setup
	matrix_order  = 2,
	mode          = \"minimize\", 
	method        = \"simplex\",
	target        = 0.0,
	tolerance     = 1e-16, 
	n_evaluations = 1000, 
	n_passes      = 5, 
	n_restarts    = 15,
	log_file      = \"/dev/tty\",  
	verbose       = 0,
	output_sparsing_factor = 20,
&end

! Match end of injector to end of bc

!&optimization_term     term = \"max.betax 45 0.1 segt \" &end
!&optimization_term     term = \"max.betay 45 0.1 segt \" &end

!&optimization_term     term = \"betax  FITT_BC1_1#1.betax FITT_BC1_2#1.betax  - 0    - sqr \" &end
!&optimization_term     term = \"betay  FITT_BC1_1#1.betay FITT_BC1_2#1.betay  - 0    - sqr \" &end
!&optimization_term     term = \"alphax FITT_BC1_1#1.alphax FITT_BC1_2#1.alphax  + 0  - sqr \" &end
!&optimization_term     term = \"alphay FITT_BC1_1#1.alphay FITT_BC1_2#1.alphay  + 0  - sqr \" &end

&optimization_term     term = \"betax  FITT_BC1_1#1.betax 40 0.1 segt\" &end
&optimization_term     term = \"betax  FITT_BC1_C#1.betax 5 0.01 segt \" &end
&optimization_term     term = \"betay  FITT_BC1_C#1.betay 15 0.1 segt\" &end
&optimization_term     term = \"betax  FITT_BC1_2#1.betax 0.4 0.001 segt \" &end
&optimization_term     term = \"betay  FITT_BC1_2#1.betay 15 0.1 segt\" &end

!&optimization_term     term = \"alphax FITT_BC1_2#1.alphax  sqr 0.05 1e-2 segt \" &end
!&optimization_term     term = \"alphay FITT_BC1_2#1.alphay  sqr 0.05 1e-2 segt \" &end

&optimization_variable  name=BC1_QD_V01, item=K1, lower_limit=[expr -2*$ln0_quad_k], upper_limit=[expr 2*$ln0_quad_k], step_size=1.e-5  &end
&optimization_variable  name=BC1_QD_V02, item=K1, lower_limit=[expr -2*$ln0_quad_k], upper_limit=[expr 2*$ln0_quad_k], step_size=1.e-5  &end
&optimization_variable  name=BC1_QD_V03, item=K1, lower_limit=[expr -2*$ln0_quad_k], upper_limit=[expr 2*$ln0_quad_k], step_size=1.e-5  &end
&optimization_variable  name=BC1_QD_V04, item=K1, lower_limit=[expr -2*$ln0_quad_k], upper_limit=[expr 2*$ln0_quad_k], step_size=1.e-5  &end

!&optimization_variable  name=BC1_DR_V1, item=l, lower_limit=0.2, upper_limit=10, step_size=1.e-5  &end
!&optimization_variable  name=BC1_DR_V2, item=l, lower_limit=0.2, upper_limit=10, step_size=1.e-5  &end
!&optimization_variable  name=BC1_DR_V3, item=l, lower_limit=0.2, upper_limit=10, step_size=1.e-5  &end
!&optimization_variable  name=BC1_DR_V4, item=l, lower_limit=0.2, upper_limit=10, step_size=1.e-5  &end
!&optimization_variable  name=BC1_DR_V5, item=l, lower_limit=0.2, upper_limit=10, step_size=1.e-5  &end

&bunched_beam n_particles_per_bunch = 1 &end

&optimize summarize_setup=1 &end
"
incr indx
puts "matched index is $indx"
puts $fileele "
&save_lattice filename =\"$filename.$indx.lte\" &end
"
close $fileele

lappend twissfiles "$filename\_$sectext.$indx.twi"
lappend sigmafiles "$filename\_$sectext.$indx.sig"

puts $fileeletr "
!-----------------------------------------------------------------------------
! Computing twiss and tracking along bc1 section 
!------------------------------------------------------------------------------

&run_setup
    lattice      = \"$filename.$indx.lte\",
    use_beamline = \"TRACKINS_$indx\",
    p_central    = \"\{sdds2stream $indstindx -par=pCentral\}\",
    default_order= 2, concat_order  = 0, always_change_p0 = 1,
    final        = \"$filename\_$sectext.$indx.fin\"
    magnets      = \"$filename\_$sectext.$indx.mag\"
    parameters   = \"$filename\_$sectext.$indx.param\"
    centroid      = \"$filename\_$sectext.$indx.cent\"
    sigma         = \"$filename\_$sectext.$indx.sig\"
&end

&run_control n_steps = 1, reset_rf_for_each_step=1 &end

&rpn_load   tag = input, filename = \"$parname\" &end

&twiss_output
    matched = 0,
    beta_x  = \"(input.betax)\",
    alpha_x = \"(input.alphax)\",
    beta_y  = \"(input.betay)\",
    alpha_y = \"(input.alphay)\",
    filename = \"$filename\_$sectext.$indx.twi\",
    statistics = 1;  
    radiation_integrals = 1;  
&end


&matrix_output
    SDDS_output = \"$filename\_$sectext.$indx.mtr\", SDDS_output_order = 2
    printout    = \"$filename\_$sectext.$indx.mpr\", printout_order = 1
&end

&sdds_beam
    input = \"$indstindx\",
    center_arrival_time=0,
&end

&floor_coordinates
    filename = \"$filename\_$sectext.$indx.flr\",
&end

&track  &end

"
close $fileeletr
lappend watches bc1_ou.sdds
lappend watches2 bc1_oua.sdds
}





#  bc1 diagnostic section
if {$case2 > 0 } {
    
    set indstindx [lindex $watches end end]
    set sectext   "bc1_di"
    set parname   "$sectext\_in_pars.sdds"
    
    set fileele   [open $filename\_$sectext.match.ele w]
    set fileeletr [open $filename\_$sectext.track.ele w]
    lappend elegant_match "$filename\_$sectext.match.ele"
    lappend elegant_track "$filename\_$sectext.track.ele"
    
puts $fileele "
!------------------------------------------------------------------------------
! tuning BC1 diagnostic matching section
!------------------------------------------------------------------------------
&subprocess command = \"sddsanalyzebeam $indstindx $parname \" &end

&run_setup
    lattice       = \"$filename.$indx.lte\",
    use_beamline  = \"MATCHINS_$indx\",
    p_central     = \"\{sdds2stream $parname -col=pCentral\}\",
    default_order = 1, concat_order  = 1,    
&end

&run_control
    n_steps = 1, 
    reset_rf_for_each_step=1
&end


&rpn_load tag = input, filename = \"$parname\" &end

&twiss_output
    matched = 0,
    beta_x  = \"(input.betax)\",
    alpha_x = \"(input.alphax)\",
    beta_y  = \"(input.betay)\",
    alpha_y = \"(input.alphay)\",
    statistics = 1;  
    radiation_integrals = 1;  
&end

&optimization_setup
	matrix_order  = 2,
	mode          = \"minimize\", 
	method        = \"simplex\",
	target        = 0.0,
	tolerance     = 1e-16, 
	n_evaluations = 1000, 
	n_passes      = 5, 
	n_restarts    = 15,
	log_file      = \"/dev/tty\",  
	verbose       = 0,
	output_sparsing_factor = 20,
&end

! Match end of injector to end of bc


&optimization_term  term = \"BC1_FITP1_DI#1.betax 20 .1 segt 1e3 * \" &end
&optimization_term  term = \"BC1_FITP1_DI#1.betay 20 .1 segt 1e3 *\" &end
&optimization_term  term = \"BC1_FITP2_DI#1.betax 20 .1 segt 1e3 *\" &end
&optimization_term  term = \"BC1_FITP2_DI#1.betay 20 .1 segt 1e3 *\" &end
&optimization_term  term = \"BC1_FITP3_DI#1.betax 20 .1 segt 1e3 *\" &end
&optimization_term  term = \"BC1_FITP3_DI#1.betay 20 .1 segt 1e3 * \" &end

!&optimization_term     term = \"BC1_FITP1_DI#1.betax  BC1_FITP1_DI#1.betay  1e-8 sene \" &end
!&optimization_term     term = \"BC1_FITP2_DI#1.betax  BC1_FITP2_DI#1.betay  1e-8 sene \" &end
!&optimization_term     term = \"BC1_FITP3_DI#1.betax  BC1_FITP3_DI#1.betay  1e-8 sene \" &end
!&optimization_term     term = \"BC1_FITP1_DI#1.alphax  BC1_FITP1_DI#1.alphay  1e-8 sene \" &end
!&optimization_term     term = \"BC1_FITP2_DI#1.alphax  BC1_FITP2_DI#1.alphay  1e-8 sene \" &end
!&optimization_term     term = \"BC1_FITP3_DI#1.alphax  BC1_FITP3_DI#1.alphay  1e-8 sene \" &end

!&optimization_term     term = \"BC1_FITP1_DI#1.betax  BC1_FITP3_DI#1.betax  1e-8 sene \" &end
!&optimization_term     term = \"BC1_FITP1_DI#1.betay  BC1_FITP3_DI#1.betay  1e-8 sene \" &end

&optimization_term     term = \"BC1_FITP1_DI#1.betax  BC1_FITP2_DI#1.betay  1e-8 sene \" &end
&optimization_term     term = \"BC1_FITP1_DI#1.betax  BC1_FITP3_DI#1.betax  1e-8 sene \" &end
&optimization_term     term = \"BC1_FITP1_DI#1.betax  BC1_FITP4_DI#1.betay  1e-8 sene \" &end

&optimization_term     term = \"BC1_FITP1_DI#1.betay  BC1_FITP2_DI#1.betax  1e-8 sene \" &end
&optimization_term     term = \"BC1_FITP1_DI#1.betay  BC1_FITP3_DI#1.betay  1e-8 sene \" &end
&optimization_term     term = \"BC1_FITP1_DI#1.betay  BC1_FITP4_DI#1.betax  1e-8 sene \" &end

&optimization_term     term = \"BC1_FITP2_DI#1.betax  BC1_FITP3_DI#1.betay  1e-8 sene \" &end
&optimization_term     term = \"BC1_FITP2_DI#1.betax  BC1_FITP4_DI#1.betax  1e-8 sene \" &end
&optimization_term     term = \"BC1_FITP3_DI#1.betax  BC1_FITP4_DI#1.betay  1e-8 sene \" &end

!&optimization_term  term = \"BC1_FITP1_DI#1.alphax 0.2 1e-3 sene  \" &end
!&optimization_term  term = \"BC1_FITP2_DI#1.alphax 0.2 1e-3 sene  \" &end
&optimization_term  term = \"BC1_FITP1_DI#1.alphay 0.2 1e-3 sene  \" &end
&optimization_term  term = \"BC1_FITP2_DI#1.alphay 0.2 1e-3 sene  \" &end

&optimization_variable  name=BC1_QD_DI_V01, item=K1, lower_limit=[expr -2*$ln0_quad_k], upper_limit=[expr 2*$ln0_quad_k], step_size=1.e-5  &end
&optimization_variable  name=BC1_QD_DI_V02, item=K1, lower_limit=[expr -2*$ln0_quad_k], upper_limit=[expr 2*$ln0_quad_k], step_size=1.e-5  &end
&optimization_variable  name=BC1_QD_DI_V03, item=K1, lower_limit=[expr -2*$ln0_quad_k], upper_limit=[expr 2*$ln0_quad_k], step_size=1.e-5  &end

!&optimization_variable  name=BC1_DR_DI_V1, item=l, lower_limit=0.2, upper_limit=3, step_size=1.e-5  &end
!&optimization_variable  name=BC1_DR_DI_V2, item=l, lower_limit=0.2, upper_limit=3, step_size=1.e-5  &end
!&optimization_variable  name=BC1_DR_DI_V3, item=l, lower_limit=0.2, upper_limit=3, step_size=1.e-5  &end



&bunched_beam n_particles_per_bunch = 1 &end

&optimize summarize_setup=1 &end
"
incr indx
puts "matched index is $indx"
puts $fileele "
&save_lattice filename =\"$filename.$indx.lte\" &end
"
close $fileele

lappend twissfiles "$filename\_$sectext.$indx.twi"
lappend sigmafiles "$filename\_$sectext.$indx.sig"

puts $fileeletr "
!-----------------------------------------------------------------------------
! Computing twiss and tracking along bc1 diagnostic section 
!------------------------------------------------------------------------------

&run_setup
    lattice      = \"$filename.$indx.lte\",
    use_beamline = \"TRACKINS_$indx\",
    p_central    = \"\{sdds2stream $indstindx -par=pCentral\}\",
    default_order= 2, concat_order  = 0, always_change_p0 = 1,
    final        = \"$filename\_$sectext.$indx.fin\"
    magnets      = \"$filename\_$sectext.$indx.mag\"
    parameters   = \"$filename\_$sectext.$indx.param\"
    centroid      = \"$filename\_$sectext.$indx.cent\"
    sigma         = \"$filename\_$sectext.$indx.sig\"
&end

&run_control n_steps = 1, reset_rf_for_each_step=1 &end

&rpn_load   tag = input, filename = \"$parname\" &end

&twiss_output
    matched = 0,
    beta_x  = \"(input.betax)\",
    alpha_x = \"(input.alphax)\",
    beta_y  = \"(input.betay)\",
    alpha_y = \"(input.alphay)\",
    filename = \"$filename\_$sectext.$indx.twi\",
    statistics = 1;  
    radiation_integrals = 1;  
&end


&matrix_output
    SDDS_output = \"$filename\_$sectext.$indx.mtr\", SDDS_output_order = 2
    printout    = \"$filename\_$sectext.$indx.mpr\", printout_order = 1
&end

&sdds_beam
    input = \"$indstindx\",
    center_arrival_time=0,
&end

&floor_coordinates
    filename = \"$filename\_$sectext.$indx.flr\",
&end

&track  &end

"
close $fileeletr
lappend watches bc1_dia_ou.sdds
lappend watches2 bc1_dia_oua.sdds
}



#  linac 1
if {$case3 > 0 } {
    
    set indstindx [lindex $watches end end]
    set sectext   "ln1"
    set parname   "$sectext\_in_pars.sdds"
    
    set fileele   [open $filename\_$sectext.match.ele w]
    set fileeletr [open $filename\_$sectext.track.ele w]
    lappend elegant_match "$filename\_$sectext.match.ele"
    lappend elegant_track "$filename\_$sectext.track.ele"
    
puts $fileele "

!------------------------------------------------------------------------------
! tuning LN1 matching section
!------------------------------------------------------------------------------
&subprocess command = \"sddsanalyzebeam $indstindx $parname \" &end

&run_setup
    lattice       = \"$filename.$indx.lte\",
    use_beamline  = \"MATCHINS_$indx\",
    p_central     = \"\{sdds2stream $parname -col=pCentral\}\",
    default_order = 1, concat_order  = 1,    
&end

&run_control
    n_steps = 1, 
    reset_rf_for_each_step=1
&end


&rpn_load tag = input, filename = \"$parname\" &end

&twiss_output
    matched = 0,
    beta_x  = \"(input.betax)\",
    alpha_x = \"(input.alphax)\",
    beta_y  = \"(input.betay)\",
    alpha_y = \"(input.alphay)\",
    statistics = 1;  
    radiation_integrals = 1;  
&end

&optimization_setup
	matrix_order  = 2,
	mode          = \"minimize\", 
	method        = \"simplex\",
	target        = 0.0,
	tolerance     = 1e-16, 
	n_evaluations = 1000, 
	n_passes      = 5, 
	n_restarts    = 15,
	log_file      = \"/dev/tty\",  
	verbose       = 0,
	output_sparsing_factor = 20,
&end

&optimization_term     term = \"LN1_FITP1#1.betax LN1_FITP2#1.betay  1e-8 sene \" &end
&optimization_term     term = \"LN1_FITP1#1.betay LN1_FITP2#1.betax  1e-8 sene \" &end

&optimization_term     term = \"LN1_FITP1#1.betax LN1_FITP3#1.betax  1e-8 sene \" &end
&optimization_term     term = \"LN1_FITP1#1.betay LN1_FITP3#1.betay  1e-8 sene \" &end

&optimization_term     term = \"LN1_FITP1#1.betax LN1_FITP4#1.betay  1e-8 sene \" &end
&optimization_term     term = \"LN1_FITP1#1.betay LN1_FITP4#1.betax  1e-8 sene \" &end

&optimization_term     term = \"LN1_FITP2#1.betax LN1_FITP3#1.betay  1e-8 sene \" &end
&optimization_term     term = \"LN1_FITP2#1.betay LN1_FITP3#1.betax  1e-8 sene \" &end

&optimization_term     term = \"LN1_FITP2#1.betax LN1_FITP4#1.betax  1e-8 sene \" &end
&optimization_term     term = \"LN1_FITP2#1.betay LN1_FITP4#1.betay  1e-8 sene \" &end

&optimization_term     term = \"LN1_FITP3#1.betax LN1_FITP4#1.betay  1e-8 sene \" &end
&optimization_term     term = \"LN1_FITP3#1.betay LN1_FITP4#1.betax  1e-8 sene \" &end

!&optimization_term     term = \"LN1_FITP1#1.alphax 0.0  1e-8 sene \" &end
!&optimization_term     term = \"LN1_FITP1#1.alphay 0.0  1e-8 sene \" &end
!&optimization_term     term = \"LN1_FITP2#1.alphax 0.0  1e-8 sene \" &end
!&optimization_term     term = \"LN1_FITP2#1.alphay 0.0  1e-8 sene \" &end
!&optimization_term     term = \"LN1_FITP3#1.alphax 0.0  1e-8 sene \" &end
!&optimization_term     term = \"LN1_FITP3#1.alphay 0.0  1e-8 sene \" &end
!&optimization_term     term = \"LN1_FITP4#1.alphax 0.0  1e-8 sene \" &end
!&optimization_term     term = \"LN1_FITP4#1.alphay 0.0  1e-8 sene \" &end

&optimization_term     term = \"max.betax 25 0.1 segt \" &end
&optimization_term     term = \"max.betay 24 0.1 segt \" &end

&optimization_variable  name=LN1_QD_V01, item=K1, lower_limit=[expr -2*$ln0_quad_k], upper_limit=[expr 2*$ln0_quad_k], step_size=1.e-8  &end
&optimization_variable  name=LN1_QD_V02, item=K1, lower_limit=[expr -2*$ln0_quad_k], upper_limit=[expr 2*$ln0_quad_k], step_size=1.e-8  &end
&optimization_variable  name=LN1_QD_V03, item=K1, lower_limit=[expr -2*$ln0_quad_k], upper_limit=[expr 2*$ln0_quad_k], step_size=1.e-8  &end

!&optimization_variable  name=LN1_DR_V1, item=L, lower_limit=0.2, upper_limit=3, step_size=1.e-8  &end
!&optimization_variable  name=LN1_DR_V2, item=L, lower_limit=0.2, upper_limit=3, step_size=1.e-8  &end
!&optimization_variable  name=LN1_DR_V3, item=L, lower_limit=0.2, upper_limit=3, step_size=1.e-8  &end
!&optimization_variable  name=LN1_DR_V4, item=L, lower_limit=0.2, upper_limit=3, step_size=1.e-8  &end

&bunched_beam n_particles_per_bunch = 1 &end

&optimize summarize_setup=1 &end
"
incr indx
puts "matched index is $indx"
puts $fileele "
&save_lattice filename =\"$filename.$indx.lte\" &end
"
close $fileele

lappend twissfiles "$filename\_$sectext.$indx.twi"
lappend sigmafiles "$filename\_$sectext.$indx.sig"

puts $fileeletr "
!-----------------------------------------------------------------------------
! Computing twiss and tracking along ln1 section 
!------------------------------------------------------------------------------

&run_setup
    lattice      = \"$filename.$indx.lte\",
    use_beamline = \"TRACKINS_$indx\",
    p_central    = \"\{sdds2stream $indstindx -par=pCentral\}\",
    default_order= 2, concat_order  = 0, always_change_p0 = 1,
    final        = \"$filename\_$sectext.$indx.fin\"
    magnets      = \"$filename\_$sectext.$indx.mag\"
    parameters   = \"$filename\_$sectext.$indx.param\"
    centroid      = \"$filename\_$sectext.$indx.cent\"
    sigma         = \"$filename\_$sectext.$indx.sig\"
&end

&run_control n_steps = 1, reset_rf_for_each_step=1 &end

&rpn_load   tag = input, filename = \"$parname\" &end

&twiss_output
    matched = 0,
    beta_x  = \"(input.betax)\",
    alpha_x = \"(input.alphax)\",
    beta_y  = \"(input.betay)\",
    alpha_y = \"(input.alphay)\",
    filename = \"$filename\_$sectext.$indx.twi\",
    statistics = 1;  
    radiation_integrals = 1;  
&end


&matrix_output
    SDDS_output = \"$filename\_$sectext.$indx.mtr\", SDDS_output_order = 2
    printout    = \"$filename\_$sectext.$indx.mpr\", printout_order = 1
&end

&sdds_beam
    input = \"$indstindx\",
    center_arrival_time=0,
&end

&floor_coordinates
    filename = \"$filename\_$sectext.$indx.flr\",
&end

&track  &end

"
close $fileeletr
lappend watches ln1_ou.sdds
lappend watches2 ln1_oua.sdds
}



#  bc2 0
if {$case4 > 0 } {
    
    set indstindx [lindex $watches end end]
    set sectext   "bc2"
    set parname   "$sectext\_in_pars.sdds"
    
    set fileele   [open $filename\_$sectext.match.ele w]
    set fileeletr [open $filename\_$sectext.track.ele w]
    lappend elegant_match "$filename\_$sectext.match.ele"
    lappend elegant_track "$filename\_$sectext.track.ele"
    
puts $fileele "
!------------------------------------------------------------------------------
! tuning BC2 matching section
!------------------------------------------------------------------------------
&subprocess command = \"sddsanalyzebeam $indstindx $parname \" &end

&run_setup
    lattice       = \"$filename.$indx.lte\",
    use_beamline  = \"MATCHINS_$indx\",
    p_central     = \"\{sdds2stream $parname -col=pCentral\}\",
    default_order = 1, concat_order  = 1,    
&end

&run_control
    n_steps = 1, 
    reset_rf_for_each_step=1
&end


&rpn_load tag = input, filename = \"$parname\" &end

&twiss_output
    matched = 0,
    beta_x  = \"(input.betax)\",
    alpha_x = \"(input.alphax)\",
    beta_y  = \"(input.betay)\",
    alpha_y = \"(input.alphay)\",
    statistics = 1;  
    radiation_integrals = 1;  
&end

&optimization_setup
	matrix_order  = 2,
	mode          = \"minimize\", 
	method        = \"simplex\",
	target        = 0.0,
	tolerance     = 1e-16, 
	n_evaluations = 1000, 
	n_passes      = 5, 
	n_restarts    = 15,
	log_file      = \"/dev/tty\",  
	verbose       = 0,
	output_sparsing_factor = 20,
&end

! Match end of ln1 to end of bc2


&optimization_term     term = \"max.betax 60 0.1 segt \" &end
&optimization_term     term = \"max.betay 60 0.1 segt \" &end

&optimization_term     term = \"betax  FITT_BC2_1#1.betax 40 0.1 segt\" &end
&optimization_term     term = \"betax  FITT_BC2_C#1.betax 5 0.01 segt \" &end
&optimization_term     term = \"betax  FITT_BC2_2#1.betax 0.4 0.001 segt \" &end

&optimization_term     term = \"betay  FITT_BC2_C#1.betay 25 0.1 segt\" &end
&optimization_term     term = \"betay  FITT_BC2_2#1.betay 15 0.1 segt\" &end

!&optimization_term     term = \"alphax FITT_BC2_2#1.alphax  sqr 0.05 1e-2 segt \" &end
!&optimization_term     term = \"alphay FITT_BC2_2#1.alphay  sqr 0.05 1e-2 segt \" &end


&optimization_variable  name=BC2_QD_V01, item=K1, lower_limit=[expr -2.*$ln1_quad_k], upper_limit=[expr 2.*$ln1_quad_k], step_size=1.e-5  &end
&optimization_variable  name=BC2_QD_V02, item=K1, lower_limit=[expr -2.*$ln1_quad_k], upper_limit=[expr 2.*$ln1_quad_k], step_size=1.e-5  &end
&optimization_variable  name=BC2_QD_V03, item=K1, lower_limit=[expr -2.*$ln1_quad_k], upper_limit=[expr 2.*$ln1_quad_k], step_size=1.e-5  &end


!&optimization_variable  name=BC2_DR_V1, item=l, lower_limit=0.2, upper_limit=10, step_size=1.e-5  &end
!&optimization_variable  name=BC2_DR_V2, item=l, lower_limit=0.2, upper_limit=10, step_size=1.e-5  &end
!&optimization_variable  name=BC2_DR_V3, item=l, lower_limit=0.2, upper_limit=10, step_size=1.e-5  &end
!&optimization_variable  name=BC2_DR_V4, item=l, lower_limit=0.2, upper_limit=10, step_size=1.e-5  &end


&bunched_beam n_particles_per_bunch = 1 &end

&optimize summarize_setup=1 &end
"
incr indx
puts "matched index is $indx"
puts $fileele "
&save_lattice filename =\"$filename.$indx.lte\" &end
"
close $fileele

lappend twissfiles "$filename\_$sectext.$indx.twi"
lappend sigmafiles "$filename\_$sectext.$indx.sig"

puts $fileeletr "
!-----------------------------------------------------------------------------
! Computing twiss and tracking along bc2 section 
!------------------------------------------------------------------------------

&run_setup
    lattice      = \"$filename.$indx.lte\",
    use_beamline = \"TRACKINS_$indx\",
    p_central    = \"\{sdds2stream $indstindx -par=pCentral\}\",
    default_order= 2, concat_order  = 0, always_change_p0 = 1,
    final        = \"$filename\_$sectext.$indx.fin\"
    magnets      = \"$filename\_$sectext.$indx.mag\"
    parameters   = \"$filename\_$sectext.$indx.param\"
    centroid      = \"$filename\_$sectext.$indx.cent\"
    sigma         = \"$filename\_$sectext.$indx.sig\"
&end

&run_control n_steps = 1, reset_rf_for_each_step=1 &end

&rpn_load   tag = input, filename = \"$parname\" &end

&twiss_output
    matched = 0,
    beta_x  = \"(input.betax)\",
    alpha_x = \"(input.alphax)\",
    beta_y  = \"(input.betay)\",
    alpha_y = \"(input.alphay)\",
    filename = \"$filename\_$sectext.$indx.twi\",
    statistics = 1;  
    radiation_integrals = 1;  
&end


&matrix_output
    SDDS_output = \"$filename\_$sectext.$indx.mtr\", SDDS_output_order = 2
    printout    = \"$filename\_$sectext.$indx.mpr\", printout_order = 1
&end

&sdds_beam
    input = \"$indstindx\",
    center_arrival_time=0,
&end

&floor_coordinates
    filename = \"$filename\_$sectext.$indx.flr\",
&end

&track  &end

"
close $fileeletr
lappend watches bc2_ou.sdds
lappend watches2 bc2_oua.sdds
}



#  linac 2
if {$case5 > 0 } {
    
    set indstindx [lindex $watches end end]
    set sectext   "ln2"
    set parname   "$sectext\_in_pars.sdds"
    
    set fileele   [open $filename\_$sectext.match.ele w]
    set fileeletr [open $filename\_$sectext.track.ele w]
    lappend elegant_match "$filename\_$sectext.match.ele"
    lappend elegant_track "$filename\_$sectext.track.ele"
    
puts $fileele "

!------------------------------------------------------------------------------
! tuning LN2 matching section
!------------------------------------------------------------------------------
&subprocess command = \"sddsanalyzebeam $indstindx $parname \" &end

&run_setup
    lattice       = \"$filename.$indx.lte\",
    use_beamline  = \"MATCHINS_$indx\",
    p_central     = \"\{sdds2stream $parname -col=pCentral\}\",
    default_order = 1, concat_order  = 1,    
&end

&run_control
    n_steps = 1, 
    reset_rf_for_each_step=1
&end


&rpn_load tag = input, filename = \"$parname\" &end

&twiss_output
    matched = 0,
    beta_x  = \"(input.betax)\",
    alpha_x = \"(input.alphax)\",
    beta_y  = \"(input.betay)\",
    alpha_y = \"(input.alphay)\",
    statistics = 1;  
    radiation_integrals = 1;  
&end

&optimization_setup
	matrix_order  = 2,
	mode          = \"minimize\", 
	method        = \"simplex\",
	target        = 0.0,
	tolerance     = 1e-16, 
	n_evaluations = 1000, 
	n_passes      = 5, 
	n_restarts    = 15,
	log_file      = \"/dev/tty\",  
	verbose       = 0,
	output_sparsing_factor = 20,
&end

&optimization_term     term = \"LN2_FITP1#1.betax LN2_FITP2#1.betay  1e-8 sene \" &end
&optimization_term     term = \"LN2_FITP1#1.betay LN2_FITP2#1.betax  1e-8 sene \" &end

&optimization_term     term = \"LN2_FITP1#1.betax LN2_FITP3#1.betax  1e-8 sene \" &end
&optimization_term     term = \"LN2_FITP1#1.betay LN2_FITP3#1.betay  1e-8 sene \" &end

&optimization_term     term = \"LN2_FITP1#1.betax LN2_FITP4#1.betay  1e-8 sene \" &end
&optimization_term     term = \"LN2_FITP1#1.betay LN2_FITP4#1.betax  1e-8 sene \" &end

&optimization_term     term = \"LN2_FITP2#1.betax LN2_FITP3#1.betay  1e-8 sene \" &end
&optimization_term     term = \"LN2_FITP2#1.betay LN2_FITP3#1.betax  1e-8 sene \" &end

&optimization_term     term = \"LN2_FITP2#1.betax LN2_FITP4#1.betax  1e-8 sene \" &end
&optimization_term     term = \"LN2_FITP2#1.betay LN2_FITP4#1.betay  1e-8 sene \" &end

&optimization_term     term = \"LN2_FITP3#1.betax LN2_FITP4#1.betay  1e-8 sene \" &end
&optimization_term     term = \"LN2_FITP3#1.betay LN2_FITP4#1.betax  1e-8 sene \" &end

!&optimization_term     term = \"LN2_FITP1#1.alphax 0.0  1e-8 sene \" &end
!&optimization_term     term = \"LN2_FITP1#1.alphay 0.0  1e-8 sene \" &end
!&optimization_term     term = \"LN2_FITP2#1.alphax 0.0  1e-8 sene \" &end
!&optimization_term     term = \"LN2_FITP2#1.alphay 0.0  1e-8 sene \" &end
!&optimization_term     term = \"LN2_FITP3#1.alphax 0.0  1e-8 sene \" &end
!&optimization_term     term = \"LN2_FITP3#1.alphay 0.0  1e-8 sene \" &end
!&optimization_term     term = \"LN2_FITP4#1.alphax 0.0  1e-8 sene \" &end
!&optimization_term     term = \"LN2_FITP4#1.alphay 0.0  1e-8 sene \" &end

&optimization_term     term = \"max.betax 25 0.1 segt \" &end
&optimization_term     term = \"max.betay 20 0.001 segt \" &end

&optimization_variable  name=LN2_QD_V01, item=K1, lower_limit=[expr -2*$ln0_quad_k], upper_limit=[expr 2*$ln0_quad_k], step_size=1.e-8  &end
&optimization_variable  name=LN2_QD_V02, item=K1, lower_limit=[expr -2*$ln0_quad_k], upper_limit=[expr 2*$ln0_quad_k], step_size=1.e-8  &end
&optimization_variable  name=LN2_QD_V03, item=K1, lower_limit=[expr -2*$ln0_quad_k], upper_limit=[expr 2*$ln0_quad_k], step_size=1.e-8  &end


!&optimization_variable  name=LN2_DR_V1, item=L, lower_limit=0.2, upper_limit=3, step_size=1.e-8  &end
!&optimization_variable  name=LN2_DR_V2, item=L, lower_limit=0.2, upper_limit=3, step_size=1.e-8  &end
!&optimization_variable  name=LN2_DR_V3, item=L, lower_limit=0.2, upper_limit=3, step_size=1.e-8  &end
!&optimization_variable  name=LN2_DR_V4, item=L, lower_limit=0.2, upper_limit=3, step_size=1.e-8  &end


&bunched_beam n_particles_per_bunch = 1 &end

&optimize summarize_setup=1 &end
"
incr indx
puts "matched index is $indx"
puts $fileele "
&save_lattice filename =\"$filename.$indx.lte\" &end
"
close $fileele

lappend twissfiles "$filename\_$sectext.$indx.twi"
lappend sigmafiles "$filename\_$sectext.$indx.sig"

puts $fileeletr "
!-----------------------------------------------------------------------------
! Computing twiss and tracking along ln2 section 
!------------------------------------------------------------------------------

&run_setup
    lattice      = \"$filename.$indx.lte\",
    use_beamline = \"TRACKINS_$indx\",
    p_central    = \"\{sdds2stream $indstindx -par=pCentral\}\",
    default_order= 2, concat_order  = 0, always_change_p0 = 1,
    final        = \"$filename\_$sectext.$indx.fin\"
    magnets      = \"$filename\_$sectext.$indx.mag\"
    parameters   = \"$filename\_$sectext.$indx.param\"
    centroid      = \"$filename\_$sectext.$indx.cent\"
    sigma         = \"$filename\_$sectext.$indx.sig\"
&end

&run_control n_steps = 1, reset_rf_for_each_step=1 &end

&rpn_load   tag = input, filename = \"$parname\" &end

&twiss_output
    matched = 0,
    beta_x  = \"(input.betax)\",
    alpha_x = \"(input.alphax)\",
    beta_y  = \"(input.betay)\",
    alpha_y = \"(input.alphay)\",
    filename = \"$filename\_$sectext.$indx.twi\",
    statistics = 1;  
    radiation_integrals = 1;  
&end


&matrix_output
    SDDS_output = \"$filename\_$sectext.$indx.mtr\", SDDS_output_order = 2
    printout    = \"$filename\_$sectext.$indx.mpr\", printout_order = 1
&end

&sdds_beam
    input = \"$indstindx\",
    center_arrival_time=0,
&end

&floor_coordinates
    filename = \"$filename\_$sectext.$indx.flr\",
&end

&track  &end

"
close $fileeletr
lappend watches ln2_ou.sdds
lappend watches2 ln2_oua.sdds
}




#  ln2 diagnostic section
if {$case6 > 0 } {
    
    set indstindx [lindex $watches end end]
    set sectext   "ln2_di"
    set parname   "$sectext\_in_pars.sdds"
    
    set fileele   [open $filename\_$sectext.match.ele w]
    set fileeletr [open $filename\_$sectext.track.ele w]
    lappend elegant_match "$filename\_$sectext.match.ele"
    lappend elegant_track "$filename\_$sectext.track.ele"
    
puts $fileele "
!------------------------------------------------------------------------------
! tuning LN2 diagnostic matching section
!------------------------------------------------------------------------------
&subprocess command = \"sddsanalyzebeam $indstindx $parname \" &end

&run_setup
    lattice       = \"$filename.$indx.lte\",
    use_beamline  = \"MATCHINS_$indx\",
    p_central     = \"\{sdds2stream $parname -col=pCentral\}\",
    default_order = 1, concat_order  = 1,    
&end

&run_control
    n_steps = 1, 
    reset_rf_for_each_step=1
&end


&rpn_load tag = input, filename = \"$parname\" &end

&twiss_output
    matched = 0,
    beta_x  = \"(input.betax)\",
    alpha_x = \"(input.alphax)\",
    beta_y  = \"(input.betay)\",
    alpha_y = \"(input.alphay)\",
    statistics = 1;  
    radiation_integrals = 1;  
&end


&optimization_setup
	matrix_order  = 2,
	mode          = \"minimize\", 
	method        = \"simplex\",
	target        = 0.0,
	tolerance     = 1e-16, 
	n_evaluations = 1000, 
	n_passes      = 5, 
	n_restarts    = 15,
	log_file      = \"/dev/tty\",  
	verbose       = 0,
	output_sparsing_factor = 20,
&end

! Match end of injector to end of bc


&optimization_term  term = \"LN2_FITP1_DI#1.betax 20 .1 segt 1e3 * \" &end
&optimization_term  term = \"LN2_FITP1_DI#1.betay 20 .1 segt 1e3 *\" &end
&optimization_term  term = \"LN2_FITP2_DI#1.betax 20 .1 segt 1e3 *\" &end
&optimization_term  term = \"LN2_FITP2_DI#1.betay 20 .1 segt 1e3 *\" &end
&optimization_term  term = \"LN2_FITP3_DI#1.betax 20 .1 segt 1e3 *\" &end
&optimization_term  term = \"LN2_FITP3_DI#1.betay 20 .1 segt 1e3 * \" &end

!&optimization_term     term = \"LN2_FITP1_DI#1.betax  LN2_FITP1_DI#1.betay  1e-8 sene \" &end
!&optimization_term     term = \"LN2_FITP2_DI#1.betax  LN2_FITP2_DI#1.betay  1e-8 sene \" &end
!&optimization_term     term = \"LN2_FITP3_DI#1.betax  LN2_FITP3_DI#1.betay  1e-8 sene \" &end
!&optimization_term     term = \"LN2_FITP1_DI#1.alphax  LN2_FITP1_DI#1.alphay  1e-8 sene \" &end
!&optimization_term     term = \"LN2_FITP2_DI#1.alphax  LN2_FITP2_DI#1.alphay  1e-8 sene \" &end
!&optimization_term     term = \"LN2_FITP3_DI#1.alphax  LN2_FITP3_DI#1.alphay  1e-8 sene \" &end

!&optimization_term     term = \"LN2_FITP1_DI#1.betax  LN2_FITP3_DI#1.betax  1e-8 sene \" &end
!&optimization_term     term = \"LN2_FITP1_DI#1.betay  LN2_FITP3_DI#1.betay  1e-8 sene \" &end

&optimization_term     term = \"LN2_FITP1_DI#1.betax  LN2_FITP2_DI#1.betay  1e-8 sene \" &end
&optimization_term     term = \"LN2_FITP1_DI#1.betax  LN2_FITP3_DI#1.betax  1e-8 sene \" &end
&optimization_term     term = \"LN2_FITP1_DI#1.betax  LN2_FITP4_DI#1.betay  1e-8 sene \" &end

&optimization_term     term = \"LN2_FITP1_DI#1.betay  LN2_FITP2_DI#1.betax  1e-8 sene \" &end
&optimization_term     term = \"LN2_FITP1_DI#1.betay  LN2_FITP3_DI#1.betay  1e-8 sene \" &end
&optimization_term     term = \"LN2_FITP1_DI#1.betay  LN2_FITP4_DI#1.betax  1e-8 sene \" &end

&optimization_term     term = \"LN2_FITP2_DI#1.betax  LN2_FITP3_DI#1.betay  1e-8 sene \" &end
&optimization_term     term = \"LN2_FITP2_DI#1.betax  LN2_FITP4_DI#1.betax  1e-8 sene \" &end
&optimization_term     term = \"LN2_FITP3_DI#1.betax  LN2_FITP4_DI#1.betay  1e-8 sene \" &end

!&optimization_term  term = \"LN2_FITP1_DI#1.alphax 0.2 1e-3 sene  \" &end
!&optimization_term  term = \"LN2_FITP2_DI#1.alphax 0.2 1e-3 sene  \" &end
&optimization_term  term = \"LN2_FITP1_DI#1.alphay 0.2 1e-3 sene  \" &end
&optimization_term  term = \"LN2_FITP2_DI#1.alphay 0.2 1e-3 sene  \" &end

&optimization_variable  name=LN2_QD_DI_V01, item=K1, lower_limit=[expr -2*$ln0_quad_k], upper_limit=[expr 2*$ln0_quad_k], step_size=1.e-5  &end
&optimization_variable  name=LN2_QD_DI_V02, item=K1, lower_limit=[expr -2*$ln0_quad_k], upper_limit=[expr 2*$ln0_quad_k], step_size=1.e-5  &end
&optimization_variable  name=LN2_QD_DI_V03, item=K1, lower_limit=[expr -2*$ln0_quad_k], upper_limit=[expr 2*$ln0_quad_k], step_size=1.e-5  &end


!&optimization_variable  name=LN2_DR_DI_V1, item=l, lower_limit=0.2, upper_limit=3, step_size=1.e-5  &end
!&optimization_variable  name=LN2_DR_DI_V2, item=l, lower_limit=0.2, upper_limit=3, step_size=1.e-5  &end
!&optimization_variable  name=LN2_DR_DI_V3, item=l, lower_limit=0.2, upper_limit=3, step_size=1.e-5  &end


&bunched_beam n_particles_per_bunch = 1 &end

&optimize summarize_setup=1 &end
"
incr indx
puts "matched index is $indx"
puts $fileele "
&save_lattice filename =\"$filename.$indx.lte\" &end
"
close $fileele

lappend twissfiles "$filename\_$sectext.$indx.twi"
lappend sigmafiles "$filename\_$sectext.$indx.sig"

puts $fileeletr "
!-----------------------------------------------------------------------------
! Computing twiss and tracking along ln2 diagnostic section 
!------------------------------------------------------------------------------

&run_setup
    lattice      = \"$filename.$indx.lte\",
    use_beamline = \"TRACKINS_$indx\",
    p_central    = \"\{sdds2stream $indstindx -par=pCentral\}\",
    default_order= 2, concat_order  = 0, always_change_p0 = 1,
    final        = \"$filename\_$sectext.$indx.fin\"
    magnets      = \"$filename\_$sectext.$indx.mag\"
    parameters   = \"$filename\_$sectext.$indx.param\"
    centroid      = \"$filename\_$sectext.$indx.cent\"
    sigma         = \"$filename\_$sectext.$indx.sig\"
&end

&run_control n_steps = 1, reset_rf_for_each_step=1 &end

&rpn_load   tag = input, filename = \"$parname\" &end

&twiss_output
    matched = 0,
    beta_x  = \"(input.betax)\",
    alpha_x = \"(input.alphax)\",
    beta_y  = \"(input.betay)\",
    alpha_y = \"(input.alphay)\",
    filename = \"$filename\_$sectext.$indx.twi\",
    statistics = 1;  
    radiation_integrals = 1;  
&end


&matrix_output
    SDDS_output = \"$filename\_$sectext.$indx.mtr\", SDDS_output_order = 2
    printout    = \"$filename\_$sectext.$indx.mpr\", printout_order = 1
&end

&sdds_beam
    input = \"$indstindx\",
    center_arrival_time=0,
&end

&floor_coordinates
    filename = \"$filename\_$sectext.$indx.flr\",
&end

&track  &end

"
close $fileeletr
lappend watches ln2_di_ou.sdds
lappend watches2 ln2_di_oua.sdds
}


#  linac 2 bypass line
if {$case7 > 0 } {
    
    set indstindx [lindex $watches end end]
    set sectext   "ln2_bp"
    set parname   "$sectext\_in_pars.sdds"
    
    set fileele   [open $filename\_$sectext.match.ele w]
    set fileeletr [open $filename\_$sectext.track.ele w]
    lappend elegant_match "$filename\_$sectext.match.ele"
    lappend elegant_track "$filename\_$sectext.track.ele"
    
puts $fileele "

!------------------------------------------------------------------------------
! tuning linac 2 SXR bypass matching section
!------------------------------------------------------------------------------
&subprocess command = \"sddsanalyzebeam $indstindx $parname \" &end

&run_setup
    lattice       = \"$filename.$indx.lte\",
    use_beamline  = \"MATCHINS_$indx\",
    p_central     = \"\{sdds2stream $parname -col=pCentral\}\",
    default_order = 1, concat_order  = 1,    
&end

&run_control
    n_steps = 1, 
    reset_rf_for_each_step=1
&end


&rpn_load tag = input, filename = \"$parname\" &end

&twiss_output
    matched = 0,
    beta_x  = \"(input.betax)\",
    alpha_x = \"(input.alphax)\",
    beta_y  = \"(input.betay)\",
    alpha_y = \"(input.alphay)\",
    statistics = 1;  
    radiation_integrals = 1;  
&end


&optimization_setup
	matrix_order  = 2,
	mode          = \"minimize\", 
	method        = \"simplex\",
	target        = 0.0,
	tolerance     = 1e-16, 
	n_evaluations = 1000, 
	n_passes      = 5, 
	n_restarts    = 15,
	log_file      = \"/dev/tty\",  
	verbose       = 0,
	output_sparsing_factor = 20,
&end

!&optimization_term     term = \"LN2_BP_FITP1#1.betax LN2_BP_FITP1#1.betay  1e-2 sene \" &end
!&optimization_term     term = \"LN2_BP_FITP2#1.betax LN2_BP_FITP2#1.betay  1e-2 sene \" &end
!&optimization_term     term = \"LN2_BP_FITP3#1.betax LN2_BP_FITP3#1.betay  1e-2 sene \" &end
!&optimization_term     term = \"LN2_BP_FITP4#1.betax LN2_BP_FITP4#1.betay  1e-2 sene \" &end


!&optimization_term     term = \"LN2_BP_FITP1#1.betax 10 0.1 segt \" &end
!&optimization_term     term = \"LN2_BP_FITP2#1.betax 8 1e-2 segt \" &end
!&optimization_term     term = \"LN2_BP_FITP3#1.betax 6 1e-3 segt \" &end
!&optimization_term     term = \"LN2_BP_FITP4#1.betax 6 1e-3 segt \" &end

!&optimization_term     term = \"max.betax 150 0.1 segt \" &end
!&optimization_term     term = \"max.betay 100 0.1 segt \" &end

&optimization_term     term = \"LN2_BP_FITP2#1.betax $btxs 1e-8 sene  \" &end
&optimization_term     term = \"LN2_BP_FITP2#1.alphax $alpxs  1e-8 sene \" &end
&optimization_term     term = \"LN2_BP_FITP2#1.betay $btys 1e-8 sene \" &end
&optimization_term     term = \"LN2_BP_FITP2#1.alphay $alpys 1e-8 sene \" &end

&optimization_variable name=LN2_QD_BP_V01, item=K1, lower_limit=[expr -2*$ln0_quad_k], upper_limit=[expr 2*$ln0_quad_k], step_size=1.e-8  &end
&optimization_variable name=LN2_QD_BP_V02, item=K1, lower_limit=[expr -2*$ln0_quad_k], upper_limit=[expr 2*$ln0_quad_k], step_size=1.e-8  &end
&optimization_variable name=LN2_QD_BP_V03, item=K1, lower_limit=[expr -2*$ln0_quad_k], upper_limit=[expr 2*$ln0_quad_k], step_size=1.e-8  &end

!&optimization_variable name=LN2_DR_BP_V1, item=L, lower_limit=0.2, upper_limit=4, step_size=1.e-8  &end
!&optimization_variable name=LN2_DR_BP_V2, item=L, lower_limit=0.2, upper_limit=4, step_size=1.e-8  &end
!&optimization_variable name=LN2_DR_BP_V3, item=L, lower_limit=0.2, upper_limit=4, step_size=1.e-8  &end
!&optimization_variable name=LN2_DR_BP_V4, item=L, lower_limit=0.2, upper_limit=4, step_size=1.e-8  &end


&bunched_beam n_particles_per_bunch = 1 &end

&optimize summarize_setup=1 &end
"
incr indx
puts "matched index is $indx"
puts $fileele "
&save_lattice filename =\"$filename.$indx.lte\" &end
"
close $fileele

lappend twissfiles "$filename\_$sectext.$indx.twi"
lappend sigmafiles "$filename\_$sectext.$indx.sig"

puts $fileeletr "
!-----------------------------------------------------------------------------
! Computing twiss and tracking along ln2 sxr bypass section 
!------------------------------------------------------------------------------

&run_setup
    lattice      = \"$filename.$indx.lte\",
    use_beamline = \"TRACKINS_$indx\",
    p_central    = \"\{sdds2stream $indstindx -par=pCentral\}\",
    default_order= 2, concat_order  = 0, always_change_p0 = 1,
    final        = \"$filename\_$sectext.$indx.fin\"
    magnets      = \"$filename\_$sectext.$indx.mag\"
    parameters   = \"$filename\_$sectext.$indx.param\"
    centroid      = \"$filename\_$sectext.$indx.cent\"
    sigma         = \"$filename\_$sectext.$indx.sig\"
&end

&run_control n_steps = 1, reset_rf_for_each_step=1 &end

&rpn_load   tag = input, filename = \"$parname\" &end

&twiss_output
    matched = 0,
    beta_x  = \"(input.betax)\",
    alpha_x = \"(input.alphax)\",
    beta_y  = \"(input.betay)\",
    alpha_y = \"(input.alphay)\",
    filename = \"$filename\_$sectext.$indx.twi\",
    statistics = 1;  
    radiation_integrals = 1;  
&end


&matrix_output
    SDDS_output = \"$filename\_$sectext.$indx.mtr\", SDDS_output_order = 2
    printout    = \"$filename\_$sectext.$indx.mpr\", printout_order = 1
&end

&sdds_beam
    input = \"$indstindx\",
    center_arrival_time=0,
&end

&floor_coordinates
    filename = \"$filename\_$sectext.$indx.flr\",
&end

&track  &end

"
close $fileeletr
lappend watches ln3_in.sdds
lappend watches2 ln3_ina.sdds
}



#  linac 3
if {$case8 > 0 } {
    
    set indstindx [lindex $watches end end]
    set sectext   "ln3"
    set parname   "$sectext\_in_pars.sdds"
    
    set fileele   [open $filename\_$sectext.match.ele w]
    set fileeletr [open $filename\_$sectext.track.ele w]
    lappend elegant_match "$filename\_$sectext.match.ele"
    lappend elegant_track "$filename\_$sectext.track.ele"
    
puts $fileele "

!------------------------------------------------------------------------------
! tuning LN3 matching section
!------------------------------------------------------------------------------
&subprocess command = \"sddsanalyzebeam $indstindx $parname \" &end

&run_setup
    lattice       = \"$filename.$indx.lte\",
    use_beamline  = \"MATCHINS_$indx\",
    p_central     = \"\{sdds2stream $parname -col=pCentral\}\",
    default_order = 1, concat_order  = 1,    
&end

&run_control
    n_steps = 1, 
    reset_rf_for_each_step=1
&end


&rpn_load tag = input, filename = \"$parname\" &end

&twiss_output
    matched = 0,
    beta_x  = \"(input.betax)\",
    alpha_x = \"(input.alphax)\",
    beta_y  = \"(input.betay)\",
    alpha_y = \"(input.alphay)\",
    statistics = 1;  
&end


&optimization_setup
	matrix_order  = 2,
	mode          = \"minimize\", 
	method        = \"simplex\",
	target        = 0.0,
	tolerance     = 1e-16, 
	n_evaluations = 1500, 
	n_passes      = 10, 
	n_restarts    = 20,
	log_file      = \"/dev/tty\",  
	verbose       = 0,
	output_sparsing_factor = 30,
&end

&optimization_term     term = \"LN3_FITP1#1.betax LN3_FITP2#1.betay  1e-8 sene \" &end
&optimization_term     term = \"LN3_FITP1#1.betay LN3_FITP2#1.betax  1e-8 sene \" &end

&optimization_term     term = \"LN3_FITP1#1.betax LN3_FITP3#1.betax  1e-8 sene \" &end
&optimization_term     term = \"LN3_FITP1#1.betay LN3_FITP3#1.betay  1e-8 sene \" &end

&optimization_term     term = \"LN3_FITP1#1.betax LN3_FITP4#1.betay  1e-8 sene \" &end
&optimization_term     term = \"LN3_FITP1#1.betay LN3_FITP4#1.betax  1e-8 sene \" &end

&optimization_term     term = \"LN3_FITP1#1.betax LN3_FITP5#1.betax  1e-8 sene \" &end
&optimization_term     term = \"LN3_FITP1#1.betay LN3_FITP5#1.betay  1e-8 sene \" &end

&optimization_term     term = \"LN3_FITP2#1.betax LN3_FITP3#1.betay  1e-8 sene \" &end
&optimization_term     term = \"LN3_FITP2#1.betay LN3_FITP3#1.betax  1e-8 sene \" &end

&optimization_term     term = \"LN3_FITP2#1.betax LN3_FITP4#1.betax  1e-8 sene \" &end
&optimization_term     term = \"LN3_FITP2#1.betay LN3_FITP4#1.betay  1e-8 sene \" &end

&optimization_term     term = \"LN3_FITP2#1.betax LN3_FITP5#1.betay  1e-8 sene \" &end
&optimization_term     term = \"LN3_FITP2#1.betay LN3_FITP5#1.betax  1e-8 sene \" &end

&optimization_term     term = \"LN3_FITP3#1.betax LN3_FITP4#1.betay  1e-8 sene \" &end
&optimization_term     term = \"LN3_FITP3#1.betay LN3_FITP4#1.betax  1e-8 sene \" &end

&optimization_term     term = \"LN3_FITP3#1.betax LN3_FITP5#1.betax  1e-8 sene \" &end
&optimization_term     term = \"LN3_FITP3#1.betay LN3_FITP5#1.betay  1e-8 sene \" &end

&optimization_term     term = \"LN3_FITP4#1.betax LN3_FITP5#1.betay  1e-8 sene \" &end
&optimization_term     term = \"LN3_FITP4#1.betay LN3_FITP5#1.betax  1e-8 sene \" &end


!&optimization_term     term = \"LN3_FITP1#1.alphax 0.0  1e-8 sene \" &end
!&optimization_term     term = \"LN3_FITP1#1.alphay 0.0  1e-8 sene \" &end
!&optimization_term     term = \"LN3_FITP2#1.alphax 0.0  1e-8 sene \" &end
!&optimization_term     term = \"LN3_FITP2#1.alphay 0.0  1e-8 sene \" &end
!&optimization_term     term = \"LN3_FITP3#1.alphax 0.0  1e-8 sene \" &end
!&optimization_term     term = \"LN3_FITP3#1.alphay 0.0  1e-8 sene \" &end
!&optimization_term     term = \"LN3_FITP4#1.alphax 0.0  1e-8 sene \" &end
!&optimization_term     term = \"LN3_FITP4#1.alphay 0.0  1e-8 sene \" &end

&optimization_term     term = \"max.betax 30 0.1 segt \" &end
&optimization_term     term = \"max.betay 30 0.1 segt \" &end

&optimization_variable  name=LN3_QD_V01, item=K1, lower_limit=[expr -4*$ln3_quad_k], upper_limit=[expr 3*$ln3_quad_k], step_size=1.e-8  &end
&optimization_variable  name=LN3_QD_V02, item=K1, lower_limit=[expr -3*$ln3_quad_k], upper_limit=[expr 4*$ln3_quad_k], step_size=1.e-8  &end
&optimization_variable  name=LN3_QD_V03, item=K1, lower_limit=[expr -4*$ln3_quad_k], upper_limit=[expr 3*$ln3_quad_k], step_size=1.e-8  &end
&optimization_variable  name=LN3_QD_V04, item=K1, lower_limit=[expr -4*$ln3_quad_k], upper_limit=[expr 3*$ln3_quad_k], step_size=1.e-8  &end

!&optimization_variable  name=LN3_DR_V1, item=L, lower_limit=0.05, upper_limit=4, step_size=1.e-8  &end
!&optimization_variable  name=LN3_DR_V2, item=L, lower_limit=0.05, upper_limit=4, step_size=1.e-8  &end
!&optimization_variable  name=LN3_DR_V3, item=L, lower_limit=0.05, upper_limit=4, step_size=1.e-8  &end
!&optimization_variable  name=LN3_DR_V4, item=L, lower_limit=0.05, upper_limit=4, step_size=1.e-8  &end
!&optimization_variable  name=LN3_DR_V5, item=L, lower_limit=0.05, upper_limit=4, step_size=1.e-8  &end

&bunched_beam n_particles_per_bunch = 1 &end

&optimize summarize_setup=1 &end
"
incr indx
puts "matched index is $indx"
puts $fileele "
&save_lattice filename =\"$filename.$indx.lte\" &end
"
close $fileele

lappend twissfiles "$filename\_$sectext.$indx.twi"
lappend sigmafiles "$filename\_$sectext.$indx.sig"

puts $fileeletr "
!-----------------------------------------------------------------------------
! Computing twiss and tracking along ln3 section 
!------------------------------------------------------------------------------

&run_setup
    lattice      = \"$filename.$indx.lte\",
    use_beamline = \"TRACKINS_$indx\",
    p_central    = \"\{sdds2stream $indstindx -par=pCentral\}\",
    default_order= 2, concat_order  = 0, always_change_p0 = 1,
    final        = \"$filename\_$sectext.$indx.fin\"
    magnets      = \"$filename\_$sectext.$indx.mag\"
    parameters   = \"$filename\_$sectext.$indx.param\"
    centroid      = \"$filename\_$sectext.$indx.cent\"
    sigma         = \"$filename\_$sectext.$indx.sig\"
&end

&run_control n_steps = 1, reset_rf_for_each_step=1 &end

&rpn_load   tag = input, filename = \"$parname\" &end

&twiss_output
    matched = 0,
    beta_x  = \"(input.betax)\",
    alpha_x = \"(input.alphax)\",
    beta_y  = \"(input.betay)\",
    alpha_y = \"(input.alphay)\",
    filename = \"$filename\_$sectext.$indx.twi\",
    statistics = 1;  
    radiation_integrals = 1;  
&end


&matrix_output
    SDDS_output = \"$filename\_$sectext.$indx.mtr\", SDDS_output_order = 2
    printout    = \"$filename\_$sectext.$indx.mpr\", printout_order = 1
&end

&sdds_beam
    input = \"$indstindx\",
    center_arrival_time=0,
&end

&floor_coordinates
    filename = \"$filename\_$sectext.$indx.flr\",
&end

&track  &end

"
close $fileeletr
lappend watches ln3_ou.sdds
lappend watches2 ln3_oua.sdds
}





#  hxr bypass line after ln3
if {$case9 > 0 } {
    
    set indstindx [lindex $watches end end]
    set sectext   "ln3_bp"
    set parname   "$sectext\_in_pars.sdds"
    
    set fileele   [open $filename\_$sectext.match.ele w]
    set fileeletr [open $filename\_$sectext.track.ele w]
    lappend elegant_match "$filename\_$sectext.match.ele"
    lappend elegant_track "$filename\_$sectext.track.ele"
    
puts $fileele "

!------------------------------------------------------------------------------
! tuning spreader  section after LN3
!------------------------------------------------------------------------------
&subprocess command = \"sddsanalyzebeam $indstindx $parname \" &end

&run_setup
    lattice       = \"$filename.$indx.lte\",
    use_beamline  = \"MATCHINS_$indx\",
    p_central     = \"\{sdds2stream $parname -col=pCentral\}\",
    default_order = 1, concat_order  = 1,    
&end

&run_control
    n_steps = 1, 
    reset_rf_for_each_step=1
&end


&rpn_load tag = input, filename = \"$parname\" &end

&twiss_output
    matched = 0,
    beta_x  = \"(input.betax)\",
    alpha_x = \"(input.alphax)\",
    beta_y  = \"(input.betay)\",
    alpha_y = \"(input.alphay)\",
    statistics = 1;  
    radiation_integrals = 1;  
&end


&optimization_setup
	matrix_order  = 2,
	mode          = \"minimize\", 
	method        = \"simplex\",
	target        = 0.0,
	tolerance     = 1e-16, 
	n_evaluations = 1000, 
	n_passes      = 5, 
	n_restarts    = 15,
	log_file      = \"/dev/tty\",  
	verbose       = 0,
	output_sparsing_factor = 20,
&end


!&optimization_term     term = \"max.betax 150 0.1 segt \" &end
!&optimization_term     term = \"max.betay 100 0.1 segt \" &end

&optimization_term     term = \"LN3_BP_FITP2#1.betax $btxh 1e-8 sene  \" &end
&optimization_term     term = \"LN3_BP_FITP2#1.alphax $alpxh  1e-8 sene \" &end
&optimization_term     term = \"LN3_BP_FITP2#1.betay $btyh 1e-8 sene \" &end
&optimization_term     term = \"LN3_BP_FITP2#1.alphay $alpyh 1e-8 sene \" &end

&optimization_variable name=LN3_QD_BP_V01, item=K1, lower_limit=[expr -2*$ln0_quad_k], upper_limit=[expr 2*$ln0_quad_k], step_size=1.e-8  &end
&optimization_variable name=LN3_QD_BP_V02, item=K1, lower_limit=[expr -2*$ln0_quad_k], upper_limit=[expr 2*$ln0_quad_k], step_size=1.e-8  &end
&optimization_variable name=LN3_QD_BP_V03, item=K1, lower_limit=[expr -2*$ln0_quad_k], upper_limit=[expr 2*$ln0_quad_k], step_size=1.e-8  &end

&optimization_variable name=LN3_DR_BP_V1, item=L, lower_limit=0.2, upper_limit=4, step_size=1.e-8  &end
&optimization_variable name=LN3_DR_BP_V2, item=L, lower_limit=0.2, upper_limit=4, step_size=1.e-8  &end
&optimization_variable name=LN3_DR_BP_V3, item=L, lower_limit=0.2, upper_limit=4, step_size=1.e-8  &end
&optimization_variable name=LN3_DR_BP_V4, item=L, lower_limit=0.2, upper_limit=4, step_size=1.e-8  &end

&bunched_beam n_particles_per_bunch = 1 &end

&optimize summarize_setup=1 &end
"
incr indx
puts "matched index is $indx"
puts $fileele "
&save_lattice filename =\"$filename.$indx.lte\" &end
"
close $fileele

lappend twissfiles "$filename\_$sectext.$indx.twi"
lappend sigmafiles "$filename\_$sectext.$indx.sig"

puts $fileeletr "
!-----------------------------------------------------------------------------
! Computing twiss and tracking along ln3 hxr bypass section 
!------------------------------------------------------------------------------

&run_setup
    lattice      = \"$filename.$indx.lte\",
    use_beamline = \"TRACKINS_$indx\",
    p_central    = \"\{sdds2stream $indstindx -par=pCentral\}\",
    default_order= 2, concat_order  = 0, always_change_p0 = 1,
    final        = \"$filename\_$sectext.$indx.fin\"
    magnets      = \"$filename\_$sectext.$indx.mag\"
    parameters   = \"$filename\_$sectext.$indx.param\"
    centroid      = \"$filename\_$sectext.$indx.cent\"
    sigma         = \"$filename\_$sectext.$indx.sig\"
&end

&run_control n_steps = 1, reset_rf_for_each_step=1 &end

&rpn_load   tag = input, filename = \"$parname\" &end

&twiss_output
    matched = 0,
    beta_x  = \"(input.betax)\",
    alpha_x = \"(input.alphax)\",
    beta_y  = \"(input.betay)\",
    alpha_y = \"(input.alphay)\",
    filename = \"$filename\_$sectext.$indx.twi\",
    statistics = 1;  
    radiation_integrals = 1;  
&end


&matrix_output
    SDDS_output = \"$filename\_$sectext.$indx.mtr\", SDDS_output_order = 2
    printout    = \"$filename\_$sectext.$indx.mpr\", printout_order = 1
&end

&sdds_beam
    input = \"$indstindx\",
    center_arrival_time=0,
&end

&floor_coordinates
    filename = \"$filename\_$sectext.$indx.flr\",
&end

&track  &end

"
close $fileeletr
lappend watches ln3_bp_ou.sdds 
lappend watches2 ln3_bp_oua.sdds
}




#  hxr bypass line section 1
if {$case10a > 0 } {

set indstindx [lindex $watches end end]
set sectext   "hbp_1"
set parname   "$sectext\_in_pars.sdds"

set fileele   [open $filename\_$sectext.match.ele w]
set fileeletr [open $filename\_$sectext.track.ele w]
lappend elegant_match "$filename\_$sectext.match.ele"
lappend elegant_track "$filename\_$sectext.track.ele"


    
puts $fileele "
!------------------------------------------------------------------------------
! tuning hxr spreader 1 bypass line section
!------------------------------------------------------------------------------
&subprocess command = \"sddsanalyzebeam $indstindx $parname\" &end

&run_setup
    lattice       = \"$filename.$indx.lte\",
    use_beamline  = \"MATCHINS_$indx\",
    p_central     = \"\{sdds2stream $indstindx -par=pCentral\}\",
    default_order = 2, concat_order  = 2,    
&end

&run_control n_steps = 1, reset_rf_for_each_step=1 &end

&rpn_load tag = input, filename = \"$parname\" &end

&twiss_output
    matched = 0,
    beta_x  = \"(input.betacx)\",
    alpha_x = \"(input.alphacx)\",
    beta_y  = \"(input.betacy)\",
    alpha_y = \"(input.alphacy)\",
    !eta_x  = \"(input.etax)\",
    !etap_x = \"(input.etaxp)\",
    statistics = 1;  
    radiation_integrals = 1;  
&end

&optimization_setup
	matrix_order  = 1,
	mode          = \"minimize\", method  = \"simplex\",
    target = 1e-16,
    tolerance = 1e-16, 
    n_passes = 10, 
    n_evaluations = 1500, 
    n_restarts = 10
    log_file      = \"/dev/tty\",
	verbose       = 0,
	output_sparsing_factor = 100,
&end

! Match spreader line


!side match
!&optimization_term     term = \"FITT_HBP_3#1.psix  pi 1e-8 sene \" &end

&optimization_term     term = \"R56 0.0 1e-12 sene \" &end

&optimization_variable  name=HBP_QD_V01, item=K1, lower_limit=[expr $hbp_quad_k*1], upper_limit=[expr 3*$hbp_quad_k], step_size=1.e-10  &end

&bunched_beam n_particles_per_bunch = 1 &end

&optimize summarize_setup=1 &end
"
incr indx
puts "matched index is $indx"
puts $fileele "
&save_lattice filename =\"$filename.$indx.lte\" &end
"
close $fileele

lappend  twissfiles "$filename\_$sectext.$indx.twi"
lappend sigmafiles "$filename\_$sectext.$indx.sig"
lappend flrfiles "$filename\_$sectext.$indx.flr"

puts $fileeletr "
!-----------------------------------------------------------------------------
! Computing twiss along hbp 1 section and tracking beam
!------------------------------------------------------------------------------

&run_setup
    lattice      = \"$filename.$indx.lte\",
    use_beamline = \"TRACKINS_$indx\",
    p_central    = \"\{sdds2stream $indstindx -par=pCentral\}\",
    default_order= 2, concat_order  = 0, always_change_p0 = 1,
    final        = \"$filename\_$sectext.$indx.fin\"
    magnets      = \"$filename\_$sectext.$indx.mag\"
    centroid      = \"$filename\_$sectext.$indx.cent\"
    sigma         = \"$filename\_$sectext.$indx.sig\"
    parameters    = \"$filename\_$sectext.$indx.param\"
&end

&run_control n_steps = 1, reset_rf_for_each_step=1 &end

&rpn_load   tag = input, filename = \"$parname\" &end

&twiss_output
    matched = 0,
    beta_x  = \"(input.betacx)\",
    alpha_x = \"(input.alphacx)\",
    beta_y  = \"(input.betacy)\",
    alpha_y = \"(input.alphacy)\",
    !eta_x  = \"(input.etax)\",
    !etap_x = \"(input.etaxp)\",
    filename = \"$filename\_$sectext.$indx.twi\",
    statistics = 1;  
    radiation_integrals = 1;
&end


&matrix_output
    SDDS_output = \"$filename\_$sectext.$indx.mtr\", SDDS_output_order = 2
    printout    = \"$filename\_$sectext.$indx.mpr\", printout_order = 1
&end

&sdds_beam
    input = \"$indstindx\",
    center_arrival_time=0,
&end

&floor_coordinates
    filename = \"$filename\_$sectext.$indx.flr\",
&end

&track  &end

"
lappend watches hbp_ou1.sdds
lappend watches2 hbp_ou1a.sdds
close $fileeletr

}


    
#  hxr bypass line section 2
if {$case10b > 0 } {

set indstindx [lindex $watches end end]
set sectext   "hbp_2"
set parname   "$sectext\_in_pars.sdds"

set fileele   [open $filename\_$sectext.match.ele w]
set fileeletr [open $filename\_$sectext.track.ele w]
lappend elegant_match "$filename\_$sectext.match.ele"
lappend elegant_track "$filename\_$sectext.track.ele"


    
puts $fileele "
!------------------------------------------------------------------------------
! tuning hxr spreader 2 bypass line section
!------------------------------------------------------------------------------
&subprocess command = \"sddsanalyzebeam $indstindx $parname\" &end

&run_setup
    lattice       = \"$filename.$indx.lte\",
    use_beamline  = \"MATCHINS_$indx\",
    p_central     = \"\{sdds2stream $indstindx -par=pCentral\}\",
    default_order = 2, concat_order  = 1,    
&end

&run_control n_steps = 1, reset_rf_for_each_step=1 &end

&rpn_load tag = input, filename = \"$parname\" &end

&twiss_output
    matched = 0,
    beta_x  = \"(input.betacx)\",
    alpha_x = \"(input.alphacx)\",
    beta_y  = \"(input.betacy)\",
    alpha_y = \"(input.alphacy)\",
    eta_x  = \"(input.etax)\",
    etap_x = \"(input.etaxp)\",
&end

&optimization_setup
	matrix_order  = 2,
	mode          = \"minimize\", method  = \"simplex\",
    target = 0.0,
    tolerance = 1e-16, 
    n_passes = 10, 
    n_evaluations = 1500, 
    n_restarts = 10
    log_file      = \"/dev/tty\",
	verbose       = 0,
	output_sparsing_factor = 20,
&end

! Match spreader line

&optimization_term     term = \"max.betax 120 0.01 segt \" &end
&optimization_term     term = \"max.betay 120 0.01 segt \" &end

! center match
!&optimization_term     term = \"FITT_HBP_5#1.betax FITT_HBP_5#1.betax 1e-12 sene  \" &end
!&optimization_term     term = \"FITT_HBP_5#1.alphax abs input.alphacx abs  - 0.0 1e-12 sene \" &end
!&optimization_term     term = \"FITT_HBP_5#1.betay input.betacy 1e-12 sene \" &end
!&optimization_term     term = \"FITT_HBP_5#1.alphay abs input.alphacy abs  - 0.0 1e-12 sene \" &end
!&optimization_term     term = \"FITT_HBP_5#1.etax input.etax 1e-12 sene  \" &end
!&optimization_term     term = \"FITT_HBP_5#1.etaxp input.etaxp  1e-12 sene \" &end
!&optimization_term     term = \"FITT_HBP_4#1.alphax  0.0 1e-12 sene \" &end
!&optimization_term     term = \"FITT_HBP_4#1.alphay  0.0 1e-12 sene \" &end
!&optimization_term     term = \"FITT_HBP_4#1.etax   0.0 1e-12 sene \" &end


&optimization_term     term = \"FITT_HBP_3#1.betax  FITT_HBP_5#1.betax 1e-8 sene  \" &end
&optimization_term     term = \"FITT_HBP_AA#1.betax  FITT_HBP_BB#1.betax 1e-8 sene  \" &end
&optimization_term     term = \"FITT_HBP_3#1.alphax abs FITT_HBP_5#1.alphax abs  1e-8 sene \" &end
&optimization_term     term = \"FITT_HBP_3#1.betay  FITT_HBP_5#1.betay 1e-8 sene \" &end
&optimization_term     term = \"FITT_HBP_3#1.alphay abs FITT_HBP_5#1.alphay abs   1e-8 sene \" &end
!&optimization_term     term = \"FITT_HBP_3#1.etax  abs  FITT_HBP_5#1.etax abs  1e-8 sene  \" &end
!&optimization_term     term = \"FITT_HBP_3#1.etaxp  abs FITT_HBP_5#1.etaxp abs 1e-5 sene \" &end

&optimization_term     term = \"FITT_HBP_4#1.alphax  0.0 1e-5 sene  \" &end
&optimization_term     term = \"FITT_HBP_4#1.alphay  0.0 1e-5 sene  \" &end
!&optimization_term     term = \"FITT_HBP_4#1.etax    0.0 1e-3 sene  \" &end

&optimization_variable  name=HBP_QD_V02, item=K1, lower_limit=[expr -3*$hbp_quad_k], upper_limit=[expr -0.5*$hbp_quad_k], step_size=1.e-8  &end
&optimization_variable  name=HBP_QD_V03, item=K1, lower_limit=[expr -2*$hbp_quad_k], upper_limit=[expr 2*$hbp_quad_k], step_size=1.e-8  &end
&optimization_variable  name=HBP_QD_V04, item=K1, lower_limit=[expr -2*$hbp_quad_k], upper_limit=[expr 2*$hbp_quad_k], step_size=1.e-8  &end

!&optimization_variable  name=HBP_QD_V05, item=K1, lower_limit=[expr -2*$hbp_quad_k], upper_limit=[expr 2*$hbp_quad_k], step_size=1.e-8  &end
!&optimization_variable  name=HBP_QD_V06, item=K1, lower_limit=[expr -3*$hbp_quad_k], upper_limit=[expr -0.5*$hbp_quad_k], step_size=1.e-8  &end

!&optimization_covariable name = HBP_QD_V05, item=K1, equation = \"HBP_QD_V03.K1\" &end
!&optimization_covariable name = HBP_QD_V06, item=K1, equation = \"HBP_QD_V02.K1\" &end


!&optimization_variable  name=HBP_DR_V3, item=l, lower_limit=0.00, upper_limit=4, step_size=1.e-6  &end
!&optimization_variable  name=HBP_DR_V4, item=l, lower_limit=0.00, upper_limit=4, step_size=1.e-6  &end
!&optimization_covariable name=HBP_DR_V5, item=L, equation = \"5.8138 HBP_DR_V3.L - HBP_DR_V4.L - \" &end



&bunched_beam n_particles_per_bunch = 1 &end

&optimize summarize_setup=1 &end
"
incr indx
puts "matched index is $indx"
puts $fileele "
&save_lattice filename =\"$filename.$indx.lte\" &end
"
close $fileele

lappend  twissfiles "$filename\_$sectext.$indx.twi"
lappend sigmafiles "$filename\_$sectext.$indx.sig"
lappend flrfiles "$filename\_$sectext.$indx.flr"

puts $fileeletr "
!-----------------------------------------------------------------------------
! Computing twiss along hbp 2 and 3 section and tracking beam
!------------------------------------------------------------------------------

&run_setup
    lattice      = \"$filename.$indx.lte\",
    use_beamline = \"TRACKINS_$indx\",
    p_central    = \"\{sdds2stream $indstindx -par=pCentral\}\",
    default_order= 2, concat_order  = 0, always_change_p0 = 1,
    final        = \"$filename\_$sectext.$indx.fin\"
    magnets      = \"$filename\_$sectext.$indx.mag\"
    centroid      = \"$filename\_$sectext.$indx.cent\"
    sigma         = \"$filename\_$sectext.$indx.sig\"
    parameters    = \"$filename\_$sectext.$indx.param\"
&end

&run_control n_steps = 1, reset_rf_for_each_step=1 &end

&rpn_load   tag = input, filename = \"$parname\" &end

&twiss_output
    matched = 0,
    beta_x  = \"(input.betacx)\",
    alpha_x = \"(input.alphacx)\",
    beta_y  = \"(input.betacy)\",
    alpha_y = \"(input.alphacy)\",
    eta_x  = \"(input.etax)\",
    etap_x = \"(input.etaxp)\",
    filename = \"$filename\_$sectext.$indx.twi\",
    statistics = 1;  
    radiation_integrals = 1;
&end


&matrix_output
    SDDS_output = \"$filename\_$sectext.$indx.mtr\", SDDS_output_order = 2
    printout    = \"$filename\_$sectext.$indx.mpr\", printout_order = 1
&end

&sdds_beam
    input = \"$indstindx\",
    center_arrival_time=0,
&end

&floor_coordinates
    filename = \"$filename\_$sectext.$indx.flr\",
&end

&track  &end

"
lappend watches hbp_ou2.sdds
lappend watches2 hbp_ou2a.sdds
lappend watches hbp_ou3.sdds
lappend watches2 hbp_ou3a.sdds
close $fileeletr

}


#  HXR BL
if {$case11 > 0 } {
    
    set indstindx [lindex $watches end end]
    set sectext   "hxr"
    set parname   "$sectext\_in_pars.sdds"
    
    set fileele   [open $filename\_$sectext.match.ele w]
    set fileeletr [open $filename\_$sectext.track.ele w]
    lappend elegant_match "$filename\_$sectext.match.ele"
    lappend elegant_track "$filename\_$sectext.track.ele"
    
puts $fileele "

!------------------------------------------------------------------------------
! tuning HXR matching section
!------------------------------------------------------------------------------
&subprocess command = \"sddsanalyzebeam $indstindx $parname \" &end

&run_setup
    lattice       = \"$filename.$indx.lte\",
    use_beamline  = \"MATCHINS_$indx\",
    p_central     = \"\{sdds2stream $parname -col=pCentral\}\",
    default_order = 1, concat_order  = 1,    
&end

&run_control
    n_steps = 1, 
    reset_rf_for_each_step=1
&end


&rpn_load tag = input, filename = \"$parname\" &end

&twiss_output
    matched = 0,
    beta_x  = \"(input.betax)\",
    alpha_x = \"(input.alphax)\",
    beta_y  = \"(input.betay)\",
    alpha_y = \"(input.alphay)\",
    statistics = 1;  
    radiation_integrals = 1;  
&end

&optimization_setup
	matrix_order  = 2,
	mode          = \"minimize\", 
	method        = \"simplex\",
	target        = 0.0,
	tolerance     = 1e-16, 
	n_evaluations = 1000, 
	n_passes      = 5, 
	n_restarts    = 15,
	log_file      = \"/dev/tty\",  
	verbose       = 0,
	output_sparsing_factor = 20,
&end

&optimization_term     term = \"HXR_FITP1#1.betax HXR_FITP2#1.betay  1e-8 sene \" &end
&optimization_term     term = \"HXR_FITP1#1.betay HXR_FITP2#1.betax  1e-8 sene \" &end

&optimization_term     term = \"HXR_FITP1#1.betax HXR_FITP3#1.betax  1e-8 sene \" &end
&optimization_term     term = \"HXR_FITP1#1.betay HXR_FITP3#1.betay  1e-8 sene \" &end

&optimization_term     term = \"HXR_FITP1#1.betax HXR_FITP4#1.betay  1e-8 sene \" &end
&optimization_term     term = \"HXR_FITP1#1.betay HXR_FITP4#1.betax  1e-8 sene \" &end

&optimization_term     term = \"HXR_FITP2#1.betax HXR_FITP3#1.betay  1e-8 sene \" &end
&optimization_term     term = \"HXR_FITP2#1.betay HXR_FITP3#1.betax  1e-8 sene \" &end

&optimization_term     term = \"HXR_FITP2#1.betax HXR_FITP4#1.betax  1e-8 sene \" &end
&optimization_term     term = \"HXR_FITP2#1.betay HXR_FITP4#1.betay  1e-8 sene \" &end

&optimization_term     term = \"HXR_FITP3#1.betax HXR_FITP4#1.betay  1e-8 sene \" &end
&optimization_term     term = \"HXR_FITP3#1.betay HXR_FITP4#1.betax  1e-8 sene \" &end

&optimization_term     term = \"HXR_FITP1#1.alphax 0.0  1e-5 sene \" &end
&optimization_term     term = \"HXR_FITP1#1.alphay 0.0  1e-5 sene \" &end
&optimization_term     term = \"HXR_FITP2#1.alphax 0.0  1e-5 sene \" &end
&optimization_term     term = \"HXR_FITP2#1.alphay 0.0  1e-5 sene \" &end
&optimization_term     term = \"HXR_FITP3#1.alphax 0.0  1e-5 sene \" &end
&optimization_term     term = \"HXR_FITP3#1.alphay 0.0  1e-5 sene \" &end
&optimization_term     term = \"HXR_FITP4#1.alphax 0.0  1e-5 sene \" &end
&optimization_term     term = \"HXR_FITP4#1.alphay 0.0  1e-5 sene \" &end

!&optimization_term     term = \"max.betax 25 0.1e-2 segt sqr \" &end
!&optimization_term     term = \"max.betay 25 0.1e-2 segt sqr \" &end

&optimization_variable  name=HXR_QD_V01, item=K1, lower_limit=[expr -3*$hxr_quad_k], upper_limit=[expr 3*$hxr_quad_k], step_size=1.e-8  &end
&optimization_variable  name=HXR_QD_V02, item=K1, lower_limit=[expr -3*$hxr_quad_k], upper_limit=[expr 3*$hxr_quad_k], step_size=1.e-8  &end
&optimization_variable  name=HXR_QD_V03, item=K1, lower_limit=[expr -3*$hxr_quad_k], upper_limit=[expr 3*$hxr_quad_k], step_size=1.e-8  &end

&optimization_variable  name=HXR_DR_V1, item=L, lower_limit=0.1, upper_limit=3, step_size=1.e-8  &end
&optimization_variable  name=HXR_DR_V2, item=L, lower_limit=0.1, upper_limit=3, step_size=1.e-8  &end
&optimization_variable  name=HXR_DR_V3, item=L, lower_limit=0.1, upper_limit=3, step_size=1.e-8  &end
&optimization_variable  name=HXR_DR_V4, item=L, lower_limit=0.1, upper_limit=3, step_size=1.e-8  &end

&bunched_beam n_particles_per_bunch = 1 &end

&optimize summarize_setup=1 &end
"
incr indx
puts "matched index is $indx"
puts $fileele "
&save_lattice filename =\"$filename.$indx.lte\" &end
"
close $fileele

lappend twissfiles "$filename\_$sectext.$indx.twi"
lappend sigmafiles "$filename\_$sectext.$indx.sig"

puts $fileeletr "
!-----------------------------------------------------------------------------
! Computing twiss and tracking along HXR section 
!------------------------------------------------------------------------------

&run_setup
    lattice      = \"$filename.$indx.lte\",
    use_beamline = \"TRACKINS_$indx\",
    p_central    = \"\{sdds2stream $indstindx -par=pCentral\}\",
    default_order= 2, concat_order  = 0, always_change_p0 = 1,
    final        = \"$filename\_$sectext.$indx.fin\"
    magnets      = \"$filename\_$sectext.$indx.mag\"
    parameters   = \"$filename\_$sectext.$indx.param\"
    centroid      = \"$filename\_$sectext.$indx.cent\"
    sigma         = \"$filename\_$sectext.$indx.sig\"
&end

&run_control n_steps = 1, reset_rf_for_each_step=1 &end

&rpn_load   tag = input, filename = \"$parname\" &end

&twiss_output
    matched = 0,
    beta_x  = \"(input.betax)\",
    alpha_x = \"(input.alphax)\",
    beta_y  = \"(input.betay)\",
    alpha_y = \"(input.alphay)\",
    filename = \"$filename\_$sectext.$indx.twi\",
    statistics = 1;  
    radiation_integrals = 1;  
&end


&matrix_output
    SDDS_output = \"$filename\_$sectext.$indx.mtr\", SDDS_output_order = 2
    printout    = \"$filename\_$sectext.$indx.mpr\", printout_order = 1
&end

&sdds_beam
    input = \"$indstindx\",
    center_arrival_time=0,
&end

&floor_coordinates
    filename = \"$filename\_$sectext.$indx.flr\",
&end

&track  &end

"
close $fileeletr
lappend watches hxr_m_ou.sdds hxr_ou.sdds
lappend watches2 hxr_m_oua.sdds hxr_oua.sdds
}



# 
# close $fileele

if {[info exists track_all] } {
    
    set indst $indist_s2e
    set sectext   "s2e"
    set parname   "$sectext\_in_pars.sdds"
    

    set fileeletr [open $filename\_$sectext.track.ele w]
    set elegant_track_all "$filename\_$sectext.track.ele"




puts $fileeletr "
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! beamline for XLS
!! tracking along linac
!! [clock format [clock seconds] -format "%B %Y"]
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

&subprocess command = \"sddsanalyzebeam $indst $parname \" &end

&run_setup
    lattice      = \"$filename.$indx.lte\",
    use_beamline = \"TRACKA_$indx\",
    p_central    = \"\{sdds2stream $indst -par=pCentral\}\",
    default_order= 2, concat_order  = 0, always_change_p0 = 1,
    final        = \"$filename\_$sectext.$indx.fin\"
    magnets      = \"$filename\_$sectext.$indx.mag\"
    parameters   = \"$filename\_$sectext.$indx.param\"
    centroid      = \"$filename\_$sectext.$indx.cent\"
    sigma         = \"$filename\_$sectext.$indx.sig\"
&end

&run_control
    n_steps = 1, 
    reset_rf_for_each_step=1
&end


&rpn_load tag = input, filename = \"$parname\" &end

&twiss_output
    matched = 0,
    beta_x  = \"(input.betax)\",
    alpha_x = \"(input.alphax)\",
    beta_y  = \"(input.betay)\",
    alpha_y = \"(input.alphay)\",
    filename = \"$filename\_$sectext.$indx.twi\",
    statistics = 1;  
    radiation_integrals = 1;  
&end

 

&sdds_beam
    input = \"$indst\",
    center_arrival_time=0,
&end

&matrix_output
    SDDS_output = \"$filename\_$sectext.$indx.mtr\",    SDDS_output_order = 2
    printout = \"$filename\_$sectext.$indx.mpr\", , printout_order = 1
&end

&floor_coordinates
    filename = \"$filename\_$sectext.$indx.flr\",
&end

&save_lattice
    filename =\"$filename\_$sectext.lattice_final.lte\"
    output_seq = 1
&end


&track

&end

"
close $fileeletr

set twissall "$filename\_$sectext.$indx.twi"
set sigmaall "$filename\_$sectext.$indx.sig"


}


