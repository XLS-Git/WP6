
!------------------------------------------------------------------------------
! tuning LN2 diagnostic matching section
!------------------------------------------------------------------------------
&subprocess command = "sddsanalyzebeam ln2_ou.sdds ln2_di_in_pars.sdds " &end

&run_setup
    lattice       = "xls_linac_tmc_sxr_maxe.6.lte",
    use_beamline  = "MATCHINS_6",
    p_central     = "{sdds2stream ln2_di_in_pars.sdds -col=pCentral}",
    default_order = 1, concat_order  = 1,    
&end

&run_control
    n_steps = 1, 
    reset_rf_for_each_step=1
&end


&rpn_load tag = input, filename = "ln2_di_in_pars.sdds" &end

&twiss_output
    matched = 0,
    beta_x  = "(input.betax)",
    alpha_x = "(input.alphax)",
    beta_y  = "(input.betay)",
    alpha_y = "(input.alphay)",
    statistics = 1;  
    radiation_integrals = 1;  
&end


&optimization_setup
	matrix_order  = 2,
	mode          = "minimize", 
	method        = "simplex",
	target        = 0.0,
	tolerance     = 1e-16, 
	n_evaluations = 1000, 
	n_passes      = 5, 
	n_restarts    = 15,
	log_file      = "/dev/tty",  
	verbose       = 0,
	output_sparsing_factor = 20,
&end

! Match end of injector to end of bc


&optimization_term  term = "LN2_FITP1_DI#1.betax 20 .1 segt 1e3 * " &end
&optimization_term  term = "LN2_FITP1_DI#1.betay 20 .1 segt 1e3 *" &end
&optimization_term  term = "LN2_FITP2_DI#1.betax 20 .1 segt 1e3 *" &end
&optimization_term  term = "LN2_FITP2_DI#1.betay 20 .1 segt 1e3 *" &end
&optimization_term  term = "LN2_FITP3_DI#1.betax 20 .1 segt 1e3 *" &end
&optimization_term  term = "LN2_FITP3_DI#1.betay 20 .1 segt 1e3 * " &end

!&optimization_term     term = "LN2_FITP1_DI#1.betax  LN2_FITP1_DI#1.betay  1e-8 sene " &end
!&optimization_term     term = "LN2_FITP2_DI#1.betax  LN2_FITP2_DI#1.betay  1e-8 sene " &end
!&optimization_term     term = "LN2_FITP3_DI#1.betax  LN2_FITP3_DI#1.betay  1e-8 sene " &end
!&optimization_term     term = "LN2_FITP1_DI#1.alphax  LN2_FITP1_DI#1.alphay  1e-8 sene " &end
!&optimization_term     term = "LN2_FITP2_DI#1.alphax  LN2_FITP2_DI#1.alphay  1e-8 sene " &end
!&optimization_term     term = "LN2_FITP3_DI#1.alphax  LN2_FITP3_DI#1.alphay  1e-8 sene " &end

!&optimization_term     term = "LN2_FITP1_DI#1.betax  LN2_FITP3_DI#1.betax  1e-8 sene " &end
!&optimization_term     term = "LN2_FITP1_DI#1.betay  LN2_FITP3_DI#1.betay  1e-8 sene " &end

&optimization_term     term = "LN2_FITP1_DI#1.betax  LN2_FITP2_DI#1.betay  1e-8 sene " &end
&optimization_term     term = "LN2_FITP1_DI#1.betax  LN2_FITP3_DI#1.betax  1e-8 sene " &end
&optimization_term     term = "LN2_FITP1_DI#1.betax  LN2_FITP4_DI#1.betay  1e-8 sene " &end

&optimization_term     term = "LN2_FITP1_DI#1.betay  LN2_FITP2_DI#1.betax  1e-8 sene " &end
&optimization_term     term = "LN2_FITP1_DI#1.betay  LN2_FITP3_DI#1.betay  1e-8 sene " &end
&optimization_term     term = "LN2_FITP1_DI#1.betay  LN2_FITP4_DI#1.betax  1e-8 sene " &end

&optimization_term     term = "LN2_FITP2_DI#1.betax  LN2_FITP3_DI#1.betay  1e-8 sene " &end
&optimization_term     term = "LN2_FITP2_DI#1.betax  LN2_FITP4_DI#1.betax  1e-8 sene " &end
&optimization_term     term = "LN2_FITP3_DI#1.betax  LN2_FITP4_DI#1.betay  1e-8 sene " &end

!&optimization_term  term = "LN2_FITP1_DI#1.alphax 0.2 1e-3 sene  " &end
!&optimization_term  term = "LN2_FITP2_DI#1.alphax 0.2 1e-3 sene  " &end
&optimization_term  term = "LN2_FITP1_DI#1.alphay 0.2 1e-3 sene  " &end
&optimization_term  term = "LN2_FITP2_DI#1.alphay 0.2 1e-3 sene  " &end

&optimization_variable  name=LN2_QD_DI_V01, item=K1, lower_limit=-14.0, upper_limit=14.0, step_size=1.e-5  &end
&optimization_variable  name=LN2_QD_DI_V02, item=K1, lower_limit=-14.0, upper_limit=14.0, step_size=1.e-5  &end
&optimization_variable  name=LN2_QD_DI_V03, item=K1, lower_limit=-14.0, upper_limit=14.0, step_size=1.e-5  &end


!&optimization_variable  name=LN2_DR_DI_V1, item=l, lower_limit=0.2, upper_limit=3, step_size=1.e-5  &end
!&optimization_variable  name=LN2_DR_DI_V2, item=l, lower_limit=0.2, upper_limit=3, step_size=1.e-5  &end
!&optimization_variable  name=LN2_DR_DI_V3, item=l, lower_limit=0.2, upper_limit=3, step_size=1.e-5  &end


&bunched_beam n_particles_per_bunch = 1 &end

&optimize summarize_setup=1 &end


&save_lattice filename ="xls_linac_tmc_sxr_maxe.7.lte" &end

