SDDS1
!# little-endian
&description text="sigma matrix--input: xls_linac_tmc_sxr_maxe_tmc.track.ele  lattice: xls_linac_tmc_sxr_maxe.11.lte", contents="sigma matrix", &end
&parameter name=Step, description="Simulation step", type=long, &end
&parameter name=SVNVersion, description="SVN version number", type=string, fixed_value=unknown, &end
&column name=s, units=m, description=Distance, type=double,  &end
&column name=ElementName, description="Element name", format_string=%10s, type=string,  &end
&column name=ElementOccurence, description="Occurence of element", format_string=%6ld, type=long,  &end
&column name=ElementType, description="Element-type name", format_string=%10s, type=string,  &end
&column name=s1, symbol="$gs$r$b1$n", units=m, description="sqrt(<x*x>)", type=double,  &end
&column name=s12, symbol="$gs$r$b12$n", units=m, description="<x*xp'>", type=double,  &end
&column name=s13, symbol="$gs$r$b13$n", units="m$a2$n", description="<x*y>", type=double,  &end
&column name=s14, symbol="$gs$r$b14$n", units=m, description="<x*y'>", type=double,  &end
&column name=s15, symbol="$gs$r$b15$n", units="m$a2$n", description="<x*s>", type=double,  &end
&column name=s16, symbol="$gs$r$b16$n", units=m, description="<x*delta>", type=double,  &end
&column name=s17, symbol="$gs$r$b17$n", units="m*s", description="<x*t>", type=double,  &end
&column name=s2, symbol="$gs$r$b2$n", description="sqrt(<x'*x'>)", type=double,  &end
&column name=s23, symbol="$gs$r$b23$n", units=m, description="<x'*y>", type=double,  &end
&column name=s24, symbol="$gs$r$b24$n", description="<x'*y'>", type=double,  &end
&column name=s25, symbol="$gs$r$b25$n", units=m, description="<x'*s>", type=double,  &end
&column name=s26, symbol="$gs$r$b26$n", description="<x'*delta>", type=double,  &end
&column name=s27, symbol="$gs$r$b27$n", units=s, description="<x'*t>", type=double,  &end
&column name=s3, symbol="$gs$r$b3$n", units=m, description="sqrt(<y*y>)", type=double,  &end
&column name=s34, symbol="$gs$r$b34$n", units=m, description="<y*y'>", type=double,  &end
&column name=s35, symbol="$gs$r$b35$n", units="m$a2$n", description="<y*s>", type=double,  &end
&column name=s36, symbol="$gs$r$b36$n", units=m, description="<y*delta>", type=double,  &end
&column name=s37, symbol="$gs$r$b37$n", units="m*s", description="<y*t>", type=double,  &end
&column name=s4, symbol="$gs$r$b4$n", description="sqrt(<y'*y')>", type=double,  &end
&column name=s45, symbol="$gs$r$b45$n", units=m, description="<y'*s>", type=double,  &end
&column name=s46, symbol="$gs$r$b46$n", description="<y'*delta>", type=double,  &end
&column name=s47, symbol="$gs$r$b47$n", units=s, description="<y'*t>", type=double,  &end
&column name=s5, symbol="$gs$r$b5$n", units=m, description="sqrt(<s*s>)", type=double,  &end
&column name=s56, symbol="$gs$r$b56$n", units=m, description="<s*delta>", type=double,  &end
&column name=s57, symbol="$gs$r$b57$n", units="m*s", description="<s*t>", type=double,  &end
&column name=s6, symbol="$gs$r$b6$n", description="sqrt(<delta*delta>)", type=double,  &end
&column name=s67, symbol="$gs$r$b67$n", units=s, description="<delta*t>", type=double,  &end
&column name=s7, symbol="$gs$r$b7$n", description="sqrt(<t*t>)", type=double,  &end
&column name=ma1, symbol="max$sb$ex$sb$e", units=m, description="maximum absolute value of x", type=double,  &end
&column name=ma2, symbol="max$sb$ex'$sb$e", description="maximum absolute value of x'", type=double,  &end
&column name=ma3, symbol="max$sb$ey$sb$e", units=m, description="maximum absolute value of y", type=double,  &end
&column name=ma4, symbol="max$sb$ey'$sb$e", description="maximum absolute value of y'", type=double,  &end
&column name=ma5, symbol="max$sb$e$gD$rs$sb$e", units=m, description="maximum absolute deviation of s", type=double,  &end
&column name=ma6, symbol="max$sb$e$gd$r$sb$e", description="maximum absolute value of delta", type=double,  &end
&column name=ma7, symbol="max$sb$e$gD$rt$sb$e", units=s, description="maximum absolute deviation of t", type=double,  &end
&column name=minimum1, symbol="x$bmin$n", units=m, type=double,  &end
&column name=minimum2, symbol="x'$bmin$n", units=m, type=double,  &end
&column name=minimum3, symbol="y$bmin$n", units=m, type=double,  &end
&column name=minimum4, symbol="y'$bmin$n", units=m, type=double,  &end
&column name=minimum5, symbol="$gD$rs$bmin$n", units=m, type=double,  &end
&column name=minimum6, symbol="$gd$r$bmin$n", units=m, type=double,  &end
&column name=minimum7, symbol="t$bmin$n", units=s, type=double,  &end
&column name=maximum1, symbol="x$bmax$n", units=m, type=double,  &end
&column name=maximum2, symbol="x'$bmax$n", units=m, type=double,  &end
&column name=maximum3, symbol="y$bmax$n", units=m, type=double,  &end
&column name=maximum4, symbol="y'$bmax$n", units=m, type=double,  &end
&column name=maximum5, symbol="$gD$rs$bmax$n", units=m, type=double,  &end
&column name=maximum6, symbol="$gd$r$bmax$n", units=m, type=double,  &end
&column name=maximum7, symbol="t$bmax$n", units=s, type=double,  &end
&column name=Sx, symbol="$gs$r$bx$n", units=m, description=sqrt(<(x-<x>)^2>), type=double,  &end
&column name=Sxp, symbol="$gs$r$bx'$n", description=sqrt(<(x'-<x'>)^2>), type=double,  &end
&column name=Sy, symbol="$gs$r$by$n", units=m, description=sqrt(<(y-<y>)^2>), type=double,  &end
&column name=Syp, symbol="$gs$r$by'$n", description=sqrt(<(y'-<y'>)^2>), type=double,  &end
&column name=Ss, symbol="$gs$r$bs$n", units=m, description=sqrt(<(s-<s>)^2>), type=double,  &end
&column name=Sdelta, symbol="$gs$bd$n$r", description=sqrt(<(delta-<delta>)^2>), type=double,  &end
&column name=St, symbol="$gs$r$bt$n", units=s, description=sqrt(<(t-<t>)^2>), type=double,  &end
&column name=ex, symbol="$ge$r$bx$n", units=m, description="geometric horizontal emittance", type=double,  &end
&column name=enx, symbol="$ge$r$bx,n$n", units=m, description="normalized horizontal emittance", type=double,  &end
&column name=ecx, symbol="$ge$r$bx,c$n", units=m, description="geometric horizontal emittance less dispersive contributions", type=double,  &end
&column name=ecnx, symbol="$ge$r$bx,cn$n", units=m, description="normalized horizontal emittance less dispersive contributions", type=double,  &end
&column name=ey, symbol="$ge$r$by$n", units=m, description="geometric vertical emittance", type=double,  &end
&column name=eny, symbol="$ge$r$by,n$n", units=m, description="normalized vertical emittance", type=double,  &end
&column name=ecy, symbol="$ge$r$by,c$n", units=m, description="geometric vertical emittance less dispersive contributions", type=double,  &end
&column name=ecny, symbol="$ge$r$by,cn$n", units=m, description="normalized vertical emittance less dispersive contributions", type=double,  &end
&column name=betaxBeam, symbol="$gb$r$bx,beam$n", units=m, description="betax for the beam, excluding dispersive contributions", type=double,  &end
&column name=alphaxBeam, symbol="$ga$r$bx,beam$n", description="alphax for the beam, excluding dispersive contributions", type=double,  &end
&column name=betayBeam, symbol="$gb$r$by,beam$n", units=m, description="betay for the beam, excluding dispersive contributions", type=double,  &end
&column name=alphayBeam, symbol="$ga$r$by,beam$n", description="alphay for the beam, excluding dispersive contributions", type=double,  &end
&data mode=binary, &end
&                 _BEG_      MARK�����?F\��P�>��%������ƒ�x��������=��x �h�`�<92�G>��>8�K4�����Va���_	ό!Q�=c�������9>��;��w���>g�o�E��=����m�=��5¢�s����"�Z;�QKlI�>�w܆�O=M�Uۭ���f�Iy��;�65���>S�������gT�X�;G�do2�B?v��繒6��ə��(=�t��9?��7�$?<��J�?Qo�~H�?   x��>44Q�ؽ�?   ���"=�t��9���7�$�E�������ھk�G�   x��44Q�ؽ��   ���"�e�z�17?��u+L�"?<��J�?Qo�~H�?   �#�>P?S��gt?   9�"=�����?92�G>��>��w���>�QKlI�>�65���>G�do2�B?�ə��(=���aӃ�=
�ly9Z�>��s�\�=�z2�1�>��A�p�=|�	�2�>���;�p�=ku[��2�>��r�6�3@�u�Z� ��c�B��?e���ο           BNCH      CHARGE�����?F\��P�>��%������ƒ�x��������=��x �h�`�<92�G>��>8�K4�����Va���_	ό!Q�=c�������9>��;��w���>g�o�E��=����m�=��5¢�s����"�Z;�QKlI�>�w܆�O=M�Uۭ���f�Iy��;�65���>S�������gT�X�;G�do2�B?v��繒6��ə��(=�t��9?��7�$?<��J�?Qo�~H�?   x��>44Q�ؽ�?   ���"=�t��9���7�$�E�������ھk�G�   x��44Q�ؽ��   ���"�e�z�17?��u+L�"?<��J�?Qo�~H�?   �#�>P?S��gt?   9�"=�����?92�G>��>��w���>�QKlI�>�65���>G�do2�B?�ə��(=���aӃ�=
�ly9Z�>��s�\�=�z2�1�>��A�p�=|�	�2�>���;�p�=ku[��2�>��r�6�3@�u�Z� ��c�B��?e���ο��C\��?	   TMC_DR_V1      DRIF�߰��[?�Ƃ��X>}�C��a�ذ����� �+�=U�sw**!�g��<92�G>��>ڃ2r	립��Va���6�)Q�=c������.�ߚ�;�~���>�{]�&o�=YFy~*4=s�89<K����	�r;�QKlI�>���$�O=M�Uۭ���e掋�;��/��>����s���)>���X�;G�do2�B?V��C֒6�Sd`�(=E���%7;?��7�$?�O�?Qo�~H�?   ���>44Q�ؽ�?   ���"=E���%7;���7�$���W����ھk�G�   ���44Q�ؽ��   ���"�W�5Y�9?��u+L�"?�O�?Qo�~H�?   ��>P?S��gt?   h+�"=�߰��[?92�G>��>�~���>�QKlI�>��/��>G�do2�B?Sd`�(=���aӃ�=<�ly9Z�>��s�\�=+z2�1�>��A�p�=|�	�2�>���;�p�=ku[��2�>��9�>7@���y�a!�^�����?j˂-��N3��?   TMC_DR_QD_ED      DRIF�ءǶ?�`5��>D�`����[�=I�[��0�_h�=5��r�g!��8�/�o<92�G>��>�3�������Va���"]A�*Q�=c�����������;|��I���>�!E�=���-��6=���c!��� ܅��t;�QKlI�>j	N�(�O=M�Uۭ���,{���;xX튮�>�`/�l���dHu��X�;G�do2�B?�h.Gܒ6��O��(=I�<w�;?��7�$?�/����?Qo�~H�?   �>44Q�ؽ�?   ?��"=I�<w�;���7�$��Ǳ���ھk�G�   �44Q�ؽ��   ?��"���~IR|9?��u+L�"?�/����?Qo�~H�?   �>P?S��gt?   k(�"=�ءǶ?92�G>��>|��I���>�QKlI�>xX튮�>G�do2�B?�O��(=���aӃ�=
�ly9Z�>��s�\�=�z2�1�>��A�p�=|�	�2�>���;�p�=ku[��2�>��9;��7@^ɋ�D�!���K��U�?Z��㿛Ü�9�?
   TMC_QD_V01      QUADiGNym�?��4�=��ƶ1������r!Ľ�B��<��=�x��!��,��z�<ׁ��[�>3P�	6`��'�����W;�s���=�x����mN��;o�δ�$�>�t$�
��=\��j��9=W�e�4茽���9��v;A��&�>�����%Q=H�^/�����=��Î;߼��>���i�����<�X�;G�do2�B?�-�\�6�Dj�(=���y�;?�pͤ��?Ys3& ?L��?   ���>44Q�ؽ�?   ���"=���y�;��pͤ���:RXto|���]#9��   ���44Q�ؽ��   ���"���CG�9?�O���?Ys3& ?L��?   ��>P?S��gt?   �%�"=iGNym�?ׁ��[�>o�δ�$�>A��&�>߼��>G�do2�B?Dj�(=8%����=�����\�>z�%�_�=�%�4�>Ky�q�=6z(��2�>Iq�=�􎄿2�>�h\C\}8@9�U��p��0
�<�?�c��$�뿛Ü�9�?   TMC_COR      KICKERiGNym�?��4�=��ƶ1������r!Ľ�B��<��=�x��!��,��z�<ׁ��[�>3P�	6`��'�����W;�s���=�x����mN��;o�δ�$�>�t$�
��=\��j��9=W�e�4茽���9��v;A��&�>�����%Q=H�^/�����=��Î;߼��>���i�����<�X�;G�do2�B?�-�\�6�Dj�(=���y�;?�pͤ��?Ys3& ?L��?   ���>44Q�ؽ�?   ���"=���y�;��pͤ���:RXto|���]#9��   ���44Q�ؽ��   ���"���CG�9?�O���?Ys3& ?L��?   ��>P?S��gt?   �%�"=iGNym�?ׁ��[�>o�δ�$�>A��&�>߼��>G�do2�B?Dj�(=8%����=�����\�>z�%�_�=�%�4�>Ky�q�=6z(��2�>Iq�=�􎄿2�>�h\C\}8@9�U��p��0
�<�?�c��$�뿛Ü�9�?   TMC_BPM      MONIiGNym�?��4�=��ƶ1������r!Ľ�B��<��=�x��!��,��z�<ׁ��[�>3P�	6`��'�����W;�s���=�x����mN��;o�δ�$�>�t$�
��=\��j��9=W�e�4茽���9��v;A��&�>�����%Q=H�^/�����=��Î;߼��>���i�����<�X�;G�do2�B?�-�\�6�Dj�(=���y�;?�pͤ��?Ys3& ?L��?   ���>44Q�ؽ�?   ���"=���y�;��pͤ���:RXto|���]#9��   ���44Q�ؽ��   ���"���CG�9?�O���?Ys3& ?L��?   ��>P?S��gt?   �%�"=iGNym�?ׁ��[�>o�δ�$�>A��&�>߼��>G�do2�B?Dj�(=8%����=�����\�>z�%�_�=�%�4�>Ky�q�=6z(��2�>Iq�=�􎄿2�>�h\C\}8@9�U��p��0
�<�?�c��$��*��땪�?
   TMC_QD_V01      QUAD�YUo�?,��޷׽E�8��½����K�ǽa�{c��=_Њ�Ǒ!�|tM���<�1G�>B)�=1�z"��o=�66W ��k�xQ���=�e�~	*ѻTX�)��>_�~�Q}�=���K�]<=H�5F 돽�27�ry;�J�S��>6���R=�hVx+���)�{��;
��k��>Z�bh�����ܣ�X�;G�do2�B?�`J�6�0#�G�(=� P;
<?��:�;W�>`-��ƒ?'��CA ?   a��>44Q�ؽ�?   ���"=� P;
<���:�;W�8j�������6�bl�   a��44Q�ؽ��   ���"������9?Ф�E�>`-��ƒ?'��CA ?   �>P?S��gt?   F#�"=�YUo�?�1G�>TX�)��>�J�S��>
��k��>G�do2�B?0#�G�(=���L`��=d���^�>�r� da�=��5�r6�>��7Qq�=�镀�2�>� rLq�=?a�q�2�>~&"ڗ8@(gi��?����j�?�%d_�����b�?   TMC_DR_QD_ED      DRIFt�Wg��?$�|Ƣ׽7Q�)�ý����ǽV��~a��=���
�!��r�^�<�1G�>��p�T�=1�z"��o=�]Y�V ��k�xQ���=�Y�z	*ѻ>�����>��li_�=宙K��?=������(�q�E|;�J�S��>��*X��R=�hVx+���m���;d�����>yִ$h����g���X�;G�do2�B?�.�Y��6�����(=NlFZ��;?��:�;W�>���:�'?'��CA ?   Θ�>44Q�ؽ�?   ���"=NlFZ��;���:�;W�ؘ_����6�bl�   Θ�44Q�ؽ��   ���"����2;�9?Ф�E�>���:�'?'��CA ?   ��>P?S��gt?   � �"=t�Wg��?�1G�>>�����>�J�S��>d�����>G�do2�B?����(=���L`��=b���^�>�r� da�=��5�r6�>��7Qq�=�镀�2�>� rLq�=?a�q�2�>���8@����t�?�j���?z�U�����F�[��	@	   TMC_DR_V2      DRIF�7�f|1? 0�/ҽnq��{S�ަL�Z�ƽ쳋%6�=�o�V0�[��+��<�1G�>�8���~�=1�z"��o=��`�4 ��k�xQ���=�c*ѻ�|!#��?�W�:`�>�+
ˀn=2��=&@���ݺ-4]�;�J�S��>ی6�Z�R=�hVx+������u��;]!�&��>���W�����mh�X�;G�do2�B?�Q��(�6�Qw�"�(=ߖo9?��:�;W�>uu0}B39?'��CA ?   {��>44Q�ؽ�?   -�"=ߖo9���:�;W���n�F5���6�bl�   {��44Q�ؽ��   -�"���I��6?Ф�E�>uu0}B39?'��CA ?   ���>P?S��gt?   
k�"=�7�f|1?�1G�>�|!#��?�J�S��>]!�&��>G�do2�B?Qw�"�(=���L`��=d���^�>�r� da�=��5�r6�>��7Qq�=�镀�2�>� rLq�=Ia�q�2�>\g��2@�A�\��?�@���
;@��dӐ%�f����1
@   TMC_DR_QD_ED      DRIFV7��'?��d.�ѽ6�����\x���|ƽ��!�=GF����V��k<�1G�>���Щ�=1�z"��o=��J4 ��k�xQ���=@��^*ѻ��[�5?�=m,�(>��4P��n=�]کwu��/�L����;�J�S��>S�_Da�R=�hVx+���VA�w��;Rޱ��>�\��W�����;x�X�;G�do2�B?�6�4�6�����(=�J����8?��:�;W�>���P�9?'��CA ?   ��>44Q�ؽ�?   �-�"=�J����8���:�;W��[��5���6�bl�   ��44Q�ؽ��   �-�"��� �6?Ф�E�>���P�9?'��CA ?   ��>P?S��gt?   Xh�"=V7��'?�1G�>��[�5?�J�S��>Rޱ��>G�do2�B?����(=���L`��=d���^�>�r� da�=��5�r6�>��7Qq�=�镀�2�>� rLq�=Ia�q�2�>*L���2@^�u_<��?�E	�R�;@��������
@
   TMC_QD_V02      QUAD'�PNG?��O\�>,d	�]��Ԓ��=��ΚP8�=�����#�Jտp̈́<���-Y��>+[sν��(#β�=�2k��='������T�s*WY�;jqD�iJ?�6������OZo=/J��G�������ԫ;J��\��>���!�7�L�6��=���� u�̻sƎ�>�tUV�����ކX�;G�do2�B?�	?�6��Y�(=S<�X(9?���L�!?�j�9?��-�}R?   ��>44Q�ؽ�?   �.�"=S<�X(9����L�!�7W��5��9�0J$�   ��44Q�ؽ��   �.�"��U.���6?�EYi{K?�j�9?��-�}R?   ���>P?S��gt?   f�"='�PNG?���-Y��>jqD�iJ?J��\��>̻sƎ�>G�do2�B?�Y�(=!
Js-��=6܌��\�>��GaN_�=��H4�>�p?Ew�=���@9�>ʹ�>Dw�=N��?9�>��x$&3@Ŕ?�'��{�{���;@uffX�@���
@   TMC_COR      KICKER'�PNG?��O\�>,d	�]��Ԓ��=��ΚP8�=�����#�Jտp̈́<���-Y��>+[sν��(#β�=�2k��='������T�s*WY�;jqD�iJ?�6������OZo=/J��G�������ԫ;J��\��>���!�7�L�6��=���� u�̻sƎ�>�tUV�����ކX�;G�do2�B?�	?�6��Y�(=S<�X(9?���L�!?�j�9?��-�}R?   ��>44Q�ؽ�?   �.�"=S<�X(9����L�!�7W��5��9�0J$�   ��44Q�ؽ��   �.�"��U.���6?�EYi{K?�j�9?��-�}R?   ���>P?S��gt?   f�"='�PNG?���-Y��>jqD�iJ?J��\��>̻sƎ�>G�do2�B?�Y�(=!
Js-��=6܌��\�>��GaN_�=��H4�>�p?Ew�=���@9�>ʹ�>Dw�=N��?9�>��x$&3@Ŕ?�'��{�{���;@uffX�@���
@   TMC_BPM      MONI'�PNG?��O\�>,d	�]��Ԓ��=��ΚP8�=�����#�Jտp̈́<���-Y��>+[sν��(#β�=�2k��='������T�s*WY�;jqD�iJ?�6������OZo=/J��G�������ԫ;J��\��>���!�7�L�6��=���� u�̻sƎ�>�tUV�����ކX�;G�do2�B?�	?�6��Y�(=S<�X(9?���L�!?�j�9?��-�}R?   ��>44Q�ؽ�?   �.�"=S<�X(9����L�!�7W��5��9�0J$�   ��44Q�ؽ��   �.�"��U.���6?�EYi{K?�j�9?��-�}R?   ���>P?S��gt?   f�"='�PNG?���-Y��>jqD�iJ?J��\��>̻sƎ�>G�do2�B?�Y�(=!
Js-��=6܌��\�>��GaN_�=��H4�>�p?Ew�=���@9�>ʹ�>Dw�=N��?9�>��x$&3@Ŕ?�'��{�{���;@uffX�@
[9���
@
   TMC_QD_V02      QUAD���Ǹ?�O�)�>�����使M@VB��=c�8i��=�9�GX������<��'�T�>��'wJ&߽�k#��=F�"wR�=;��JL���/9�n <`�yC?�X�gr��]c�a��n=´niZT��eln=ȁ�;,?��u�>�7?C7^��<�=�a]U����p���>�g|�K����1"]�X�;G�do2�B?S)CA�6���(=�³�s�9?>�E>�2?;�(Ab9?��(?   ���>44Q�ؽ�?   �/�"=�³�s�9�>�E>�2�u�uc�W5���(�   ���44Q�ؽ��   �/�"��=�r�?7?7/j��0?;�(Ab9?���"�%?   u��>P?S��gt?   �b�"=���Ǹ?��'�T�>`�yC?,?��u�>��p���>G�do2�B?��(=Y}��E��=��1�Z�>11���]�=um2�>la���}�=д���?�>A�/��}�=�XƄ�?�>ߊ/� 4@F���8-���S�&;@��x�9�+@�����,@   TMC_DR_QD_ED      DRIFH�P�f]?Ȇ܋4>�!f����jcp�=�4����=��ƅ�a���߯<��'�T�>��Yc)w޽�k#��=��[0R�=;��JL��J^��n <��+��?&�J|�t���K6In=��v2���t8��h�;,?��u�>�?c]7^��<�=`�rPm��[���>��eb8���x��X�;G�do2�B? �I<�6�H��h�(=�,�j��:?>�E>�2?�����8?��(?   ͷ�>44Q�ؽ�?   N1�"=�,�j��:�>�E>�2�+���?�4���(�   ͷ�44Q�ؽ��   N1�"�Ŀ�&�7?7/j��0?�����8?���"�%?   ���>P?S��gt?   �]�"=H�P�f]?��'�T�>��+��?,?��u�>[���>G�do2�B?H��h�(=�|��E��=զ1�Z�>s0���]�=Otm2�>a���}�=c����?�>خ/��}�=XƄ�?�>G�GC5@gӣ�".��n���9@_�l��+@D 5&�H@	   TMC_DR_V3      DRIF��d�2??�/��=�>�n�w�Z��o��=��S R�=`��!������<��'�T�>E��M6ؽ�k#��=���:'R�=;��JL����n�n <3Y}z0�?S��:
�H��='h=���bA���
���;,?��u�>���K8^��<�=�&%H���@�:��>��@����aWu�X�;G�do2�B?t����6�A�?��(=�\�T*�@?>�E>�2?ׅ�44?��(?   ���>44Q�ؽ�?   Y@�"=�\�T*�@�>�E>�2�f���d�0���(�   ���44Q�ؽ��   Y@�"��=��>?7/j��0?ׅ�44?���"�%?   ���>P?S��gt?   1�"=��d�2??��'�T�>3Y}z0�?,?��u�>�@�:��>G�do2�B?A�?��(=Y}��E��=��1�Z�>31���]�=um2�>la���}�=д���?�>A�/��}�=�XƄ�?�>j�Ե�KA@�AE֕;3����G:�0@bk\�/�%@��c��@   TMC_DR_QD_ED      DRIF�YB���?ħ?t�f>$�ڴ�Z彊֛��`�=˓��͵�=���z!���KV�<��'�T�>a�9��׽�k#��=��*R�=;��JL��@U��n <��D�x?���H�	�(�"�g=��x����V��;,?��u�>��	�e8^��<�=��K`��������>ɢ��t���<���X�;G�do2�B?�\�	�6�����(=ܑ@KXA?>�E>�2?���p�3?��(?   ��>44Q�ؽ�?   �A�"=ܑ@KXA�>�E>�2��e�.0���(�   ��44Q�ؽ��   �A�"�~t�{�:??7/j��0?���p�3?���"�%?   ���>P?S��gt?   ,�"=�YB���?��'�T�>��D�x?,?��u�>������>G�do2�B?����(=Y}��E��=��1�Z�>11���]�=um2�>7a���}�=�����?�>�/��}�=RXƄ�?�>���IB@x'J\?�3��'N�rP/@��P���$@m\���@
   TMC_QD_V03      QUAD��W�Q?#��[>�M3��:�{.ʳ�_�=/�����=n�q�s�!��~�9"<����Zr�>�A���ýE��O2��=�f�Hs�=J�J/����"��
��;!=�Rl?���������"�g=�&OQz��-�V�c��;���Gk�>��M��>O�V\j�O�=�;������$� ��>�및k�������X�;G�do2�B?�R���6���(=3Z_�%�A?$�>ќ�?bX)eV3?~�K��?   ���>44Q�ؽ�?   2C�"=3Z_�%�A�$�>ќ���&�,�/�<�ѯ�   ���44Q�ؽ��   2C�"���Cn*�??p�X^�?bX)eV3?~�K��?   ���>P?S��gt?   �(�"=��W�Q?����Zr�>!=�Rl?���Gk�>�$� ��>G�do2�B?��(=ɘ�����=��UJ]�>�n��`�=��Ds5�>5,y{�z�=Y�x�<�>��n|�z�=�E�o�<�>��ǳ1�B@x�$�� ��>o�.@ۅd.��@m\���@   TMC_COR      KICKER��W�Q?#��[>�M3��:�{.ʳ�_�=/�����=n�q�s�!��~�9"<����Zr�>�A���ýE��O2��=�f�Hs�=J�J/����"��
��;!=�Rl?���������"�g=�&OQz��-�V�c��;���Gk�>��M��>O�V\j�O�=�;������$� ��>�및k�������X�;G�do2�B?�R���6���(=3Z_�%�A?$�>ќ�?bX)eV3?~�K��?   ���>44Q�ؽ�?   2C�"=3Z_�%�A�$�>ќ���&�,�/�<�ѯ�   ���44Q�ؽ��   2C�"���Cn*�??p�X^�?bX)eV3?~�K��?   ���>P?S��gt?   �(�"=��W�Q?����Zr�>!=�Rl?���Gk�>�$� ��>G�do2�B?��(=ɘ�����=��UJ]�>�n��`�=��Ds5�>5,y{�z�=Y�x�<�>��n|�z�=�E�o�<�>��ǳ1�B@x�$�� ��>o�.@ۅd.��@m\���@   TMC_BPM      MONI��W�Q?#��[>�M3��:�{.ʳ�_�=/�����=n�q�s�!��~�9"<����Zr�>�A���ýE��O2��=�f�Hs�=J�J/����"��
��;!=�Rl?���������"�g=�&OQz��-�V�c��;���Gk�>��M��>O�V\j�O�=�;������$� ��>�및k�������X�;G�do2�B?�R���6���(=3Z_�%�A?$�>ќ�?bX)eV3?~�K��?   ���>44Q�ؽ�?   2C�"=3Z_�%�A�$�>ќ���&�,�/�<�ѯ�   ���44Q�ؽ��   2C�"���Cn*�??p�X^�?bX)eV3?~�K��?   ���>P?S��gt?   �(�"=��W�Q?����Zr�>!=�Rl?���Gk�>�$� ��>G�do2�B?��(=ɘ�����=��UJ]�>�n��`�=��Ds5�>5,y{�z�=Y�x�<�>��n|�z�=�E�o�<�>��ǳ1�B@x�$�� ��>o�.@ۅd.��@��mwC@
   TMC_QD_V03      QUAD1�e?'�o�2g��-^ϭ彺R����=��}��=�i}H�!�=�77�#<fq}���>m�u
sO�=���*RWv���XPx���]#��q��=/��w	޻�Iz!�� ? h��ν�r:��f=�H�GK��������;*� �a�>�SѾ���C+K�Li=�U�N"�Q��<R��>�	��j������X�;G�do2�B? &��6�C��Ǿ(=����A?����	?��{�83?��ǩ�>   ��>44Q�ؽ�?   �C�"=����A�`V�t���C�$�l/���ǩ��   ��44Q�ؽ��   �C�"�/
H���??����	?��{�83?�(� V��>   ���>P?S��gt?   K&�"=1�e?fq}���>�Iz!�� ?*� �a�>�<R��>G�do2�B?C��Ǿ(=�_z���=�'@#*a�>_|�.fc�=��#�8�>��1�w�=��h�9�>@y-1�w�=��@]�9�>tw�4��B@H+��}[@73-�-@��f�>�?��S���@   TMC_DR_QD_ED      DRIF�>��H?�RCZS��M{���n����=̐����=G� �!�Mj/�o<fq}���>���1�=���*RWv���hx���]#��q��=�[nx	޻i /��� ?�R.z,�ν��H��f="?m*gB��a,{�ʁ�;*� �a�>g]�����C+K�Li=��1�"�Q�Sda%��>��pj������X�;G�do2�B?Y0��#�6�E]���(=縁�A?����	?d�w��93?��ǩ�>   ��>44Q�ؽ�?   dD�"=縁�A�`V�t��)�?��X/���ǩ��   ��44Q�ؽ��   dD�"��a���??����	?d�w��93?�(� V��>   ���>P?S��gt?   �#�"=�>��H?fq}���>i /��� ?*� �a�>Sda%��>G�do2�B?E]���(=�_z���=�'@#*a�>k|�.fc�=��#�8�>��1�w�=��h�9�>@y-1�w�=��@]�9�>BC�ze�B@���>@��y��q-@P��ġ�?�v��@	   TMC_DR_V4      DRIF�kNF�?j4?k������OzL�	���Q�=ݩ1P�|�=c�N�-!�����^<fq}���>�3��=���*RWv����x���]#��q��=�;	Q|	޻?��n7� ?�D�z�ͽ�k�3��f=���F���*��e�;*� �a�>�l�����C+K�Li=X��@&�Q�>��p��>-R��g����{ӴX�;G�do2�B?L�2X[�6�5l� �(=���?�DA?����	?J��}q>3?��ǩ�>   ���>44Q�ؽ�?   �F�"=���?�DA�`V�t��{��z8�.���ǩ��   ���44Q�ؽ��   �F�"�����??����	?J��}q>3?�(� V��>   o��>P?S��gt?   �"=�kNF�?fq}���>?��n7� ?*� �a�>>��p��>G�do2�B?5l� �(=�_z���=�'@#*a�>k|�.fc�=��#�8�>��1�w�=��h�9�>@y-1�w�=��@]�9�>�t����A@ɬ>�
@d�.7i�,@^��b�d�?�v��@	   TMC_DR_20      DRIFZ�8$�y
?(Oy�+ y`��La�(�=Fv"e�p�=.e�7 �o��~<fq}���>2ʄ��<�=���*RWv��B!�y���]#��q��=�.��	޻�W4vQ ?eP���˽%)��nf=�������d8��;*� �a�>��	�Ë��C+K�Li=��\.�Q�� �-��>AO|�a���1A�,�X�;G�do2�B?O^G?�6��36��(=��
iu@?����	?�O&�J3?��ǩ�>   ,��>44Q�ؽ�?   �L�"=��
iu@�`V�t��H)�.���ǩ��   ,��44Q�ؽ��   �L�"��`1��=?����	?�O&�J3?�(� V��>   ���>P?S��gt?   ���"=Z�8$�y
?fq}���>�W4vQ ?*� �a�>� �-��>G�do2�B?�36��(=�_z���=�'@#*a�>k|�.fc�=��#�8�>��1�w�=��h�9�>@y-1�w�=��@]�9�>
�	bQ@@s���Y	@Ά��%[+@��m���?�s}��@   TMC_DP_DIP1   	   CSRCSBENDH9^	2	?X��H_�z3x�9�C�����=(��ı�=��m���� 
�@�
<��Q䯄�>�\-��=9��#Zv���}7��B�$�׿��t(���ۻ	����>��5iɽɣ5�j=_�ЃO�����|w_�;k��a�>��=%z�����Mi=Y�9��nZ�d�s�u�>Ֆ��e������w�?�;���o2�B?@�,&h~6�<�mZ�=G�V��7??.q�j)y?�i�͔V3?�$%kҩ�>   #B�>U���ؽ�?   :�"=G�V��7?���fD���4�&-��$%kҩ��   ���U���ؽ��   :�"� ��E�<?.q�j)y?�i�͔V3?���z`��>   #B�>�-��gt?   (Ҵ"=H9^	2	?��Q䯄�>	����>k��a�>d�s�u�>���o2�B?<�mZ�=����=>�1qÆ>�5�Pc�=��+r8�>mO�3�w�=��j�9�>�?3�w�=��@_�9�>o/X�c�=@�9����@��d��)@�(
�`��?�s}��@   TMC_DR_SIDE_C      CSRDRIFTܬ��>�?�U�Ant[�߽�Yh�!�=�k P��=_�E��g/<��<��Q䯄�>�>��{�=9��#Zv��+[S�7��B�$�׿���~��ۻ��MJ
�>f1BwFǽHW�Wٙi=m�aO궽z�{,���;k��a�>���+z�����Mi=l΋�nZ�d�L=�u�>��Қ_����ƥ��?�;���o2�B?��,��~6�m�.��=7��(?p=?.q�j)y?�D��b3?�$%kҩ�>   :B�>U���ؽ�?   l�"=7��(?p=���fD�����$-��$%kҩ��   ���U���ؽ��   l�"�lV��B�:?.q�j)y?�D��b3?���z`��>   :B�>�-��gt?   $��"=ܬ��>�?��Q䯄�>��MJ
�>k��a�>d�L=�u�>���o2�B?m�.��=����=>�1qÆ>�5�Pc�=��+r8�>mO�3�w�=��j�9�>�?3�w�=��@_�9�>��/�:@dLޖn�@	��ܾ(@����!�?�V��M!@   TMC_DR_SIDE      DRIF>a,��� ?�E��zg㽥' mҽ�D�`�~�=c��	�=�8�Y6�����{��;��Q䯄�>x�e<c4�=9��#Zv�"
�0�7��B�$�׿��������ۻ����m��>��� 촽����k�f=/�]�����FI�{�;k��a�>D�� Sz�����Mi=%�r��nZ������u�>P��9���#c���?�;���o2�B?{��d3�6��0�I�=��7Q��2?.q�j)y?��߼	�3?�$%kҩ�>   �B�>U���ؽ�?   �8�"=��7Q��2���fD���m���.��$%kҩ��   ��U���ؽ��   �8�"�B<�):1?.q�j)y?��߼	�3?���z`��>   �B�>�-��gt?   �"�"=>a,��� ?��Q䯄�>����m��>k��a�>�����u�>���o2�B?�0�I�=����=>�1qÆ>�5�Pc�=��+r8�>mO�3�w�=��j�9�>�?3�w�=��@_�9�>��V�sf)@,��@�?4�#:��#@��Ӳ�1�?E��M"@   TMC_DP_DIP2   	   CSRCSBEND�tB�8��>��$�R�ὖ���*�н�~�J���=w[+���=ŵ;���Ұgx �;E�����>�M���V�= ��rWv�B4ω��@�c�q��=�}��D�ݻ\��	�>�>t���ͦ������rd=���%����%��W�;Y��a�>��>ӊ���{jMi=�7(�V��M��z�>�v_+D�����L�;���p2�B?�x��ߛ6��[w�L$=!�V�1?}�`���	?X�sy�3?&�X�ө�>   c��>.k+�ؽ�?   ,�"=!�V�1�����t���Ϡ��/�&�X�ө��   c��.k+�ؽ��   ,�"�3([�[�/?}�`���	?X�sy�3?�/H�a��>   �&��>~/��gt?   x��"=�tB�8��>E�����>\��	�>�>Y��a�>�M��z�>���p2�B?�[w�L$=i3��=>�lY�>���^c�=�@�z�8�>��h5�w�=Cv�k�9�>y��4�w�=�h�`�9�>�hȡ)�%@�%�.lW�?� ��v#@����^�?E���"@   TMC_DR_CENT_C      CSRDRIFT
o[��>Bg����པ�W��н��*N�=�Iؼ��=�z5�h�9���3*�;E�����>��0�v�= ��rWv���*�ω��@�c�q��=���aI�ݻ-��>�
9����,�.�Ad=���*E��	���+�;Y��a�>����Ԋ���{jMi=�.�V�6���z�>|�A���w�@L�;���p2�B?�t(%�6��#�"N$=^'�@0?}�`���	?�W!�3?&�X�ө�>   ��>.k+�ؽ�?   *�"=^'�@0�����t�������0/�&�X�ө��   ��.k+�ؽ��   *�"���π.?}�`���	?�W!�3?�/H�a��>   o&��>~/��gt?   E��"=
o[��>E�����>-��>Y��a�>6���z�>���p2�B?�#�"N$=i3��=>�lY�>���^c�=�@�z�8�>��h5�w�=Cv�k�9�>y��4�w�=�h�`�9�>C��U$@0�)
��?����;�"@ex��&��?E���"@   TMC_BPM      MONI
o[��>Bg����པ�W��н��*N�=�Iؼ��=�z5�h�9���3*�;E�����>��0�v�= ��rWv���*�ω��@�c�q��=���aI�ݻ-��>�
9����,�.�Ad=���*E��	���+�;Y��a�>����Ԋ���{jMi=�.�V�6���z�>|�A���w�@L�;���p2�B?�t(%�6��#�"N$=^'�@0?}�`���	?�W!�3?&�X�ө�>   ��>.k+�ؽ�?   *�"=^'�@0�����t�������0/�&�X�ө��   ��.k+�ؽ��   *�"���π.?}�`���	?�W!�3?�/H�a��>   o&��>~/��gt?   E��"=
o[��>E�����>-��>Y��a�>6���z�>���p2�B?�#�"N$=i3��=>�lY�>���^c�=�@�z�8�>��h5�w�=Cv�k�9�>y��4�w�=�h�`�9�>C��U$@0�)
��?����;�"@ex��&��?E���#@   TMC_DR_CENT      DRIFL%�k�>1-��%޽_Y��ͽ��
�0��=�= ��=�)��.���.ܘt�;E�����>��B�&�= ��rWv�N(>�Љ��@�c�q��=Ժ|S�ݻ����J��>�-x�}������c=�������H�h;�ӡ;Y��a�>?�oc؊���{jMi=bK8�V��#V��z�>��P�:���c(�iL�;���p2�B?`�[���6�����P$=� �[D-?}�`���	?��j1�3?&�X�ө�>   z��>.k+�ؽ�?   1 �"=� �[D-�����t���Үo�v/�&�X�ө��   z��.k+�ؽ��   1 �"�#I���,?}�`���	?��j1�3?�/H�a��>   �%��>~/��gt?   ܆�"=L%�k�>E�����>����J��>Y��a�>�#V��z�>���p2�B?����P$=i3��=>�lY�>���^c�=�@�z�8�>��h5�w�=Cv�k�9�>y��4�w�=�h�`�9�>��f�0a!@�D�FH�?�x�0֋"@[3�&���?�!��$@   TMC_DP_DIP3   	   CSRCSBEND���A�>���-�ڽ?�Z�B˽�c�q�=��D��=]�Ԧ�����/�;���I��>�B�`FJ�=
�7�Tv���B�h��m0L�q�>C�M߻���,���>\���^嗽��Օ�a=��{-�(��_��;��`�a�>�W���*��O3ɦMi=4�^��R��@g�}�>��ʌ���4
��T�;-N�q2�B?,*=���6�$��Q'=Fh@�5�*?W�y��?�;��@�3?�ڴ�ϩ�>   "��>@̞�ؽ�?   B6�"=>��S�0*�W�y����~�b��/��ڴ�ϩ��   "��@̞�ؽ��   B6�"�Fh@�5�*?-{�cT?�;��@�3?zr�^��>   8X�>m����gt?   7��"=���A�>���I��>���,���>��`�a�>�@g�}�>-N�q2�B?$��Q'=�ۉ^��=�:zSӫ�>¨�^kc�=�.���8�>�;�6�w�=���l�9�>��5�w�=H�0b�9�>E�G�Ն@��>w��?���V"@�/���?�!��%@   TMC_DR_SIDE_C      CSRDRIFT'��2�>��t�׽B���Nɽ��ۅ=�:øキ=�S����x�)��;���I��>�O홮�=
�7�Tv�����h��m0L�q�>��/M߻�=O�%��>��.j�={��5�@�a=S�V��ò�×�,��;��`�a�>=c��*��O3ɦMi=��3#��R�i4[��}�>��}`���˔T�;-N�q2�B?�\�E�6�pO�T'=(����(?W�y��?gFi�O�3?�ڴ�ϩ�>   ^��>@̞�ؽ�?    <�"=چ9�F'�W�y����d([z0��ڴ�ϩ��   ^��@̞�ؽ��    <�"�(����(?-{�cT?gFi�O�3?zr�^��>   DW�>m����gt?   ܐ�"='��2�>���I��>�=O�%��>��`�a�>i4[��}�>-N�q2�B?pO�T'=�ۉ^��=�:zSӫ�>è�^kc�=�.���8�>�;�6�w�=���l�9�>��5�w�=H�0b�9�>W�i{��@�����?����I="@X c�?���0�+@   TMC_DR_SIDE      DRIF�r�5���>��"�����uP��Ľ�=���Fw�}�է󷦽s����%>]+�T�g����I��>�qfw�H=
�7�Tv����h��m0L�q�>����M߻�mu3��>1�M�%��=!�'X�;_=�&��[d���Ⱥ�;��`�a�>��*��O3ɦMi=���R��N��}�>��gU���n�[R�T�;-N�q2�B?�7�	��6���c'=N��4/�?W�y��?���)4?�ڴ�ϩ�>   ���>@̞�ؽ�?   D_�"=,Ɨ��W�y������[o�0��ڴ�ϩ��   ���@̞�ؽ��   D_�"�N��4/�?-{�cT?���)4?zr�^��>   �Q�>m����gt?   ��"=�r�5���>���I��>�mu3��>��`�a�>�N��}�>-N�q2�B?��c'=�ۉ^��=�:zSӫ�>Ĩ�^kc�=�.���8�>�;�6�w�=���l�9�>��5�w�=H�0b�9�>q?�4�@�"�T\C�?��kƥ�#@h�a�ӿ�#�2�,@   TMC_DP_DIP4   	   CSRCSBEND�3���>ra	<|��=0��kŽ��	R�8�����/�����~�5
>�s��y�ٚ����>+U��_��!g}zWv�J�r����E%�q��=KHf��޻0(ě�U�>R;IU2�=9�딕z`=ʼߥ@����/a	���;\����a�>B�C�&���$���Mi=����Q�����>`��z8���RJ�Z�;Z�p2�B?2�IN�6��ҺV)=����\�?(;���	?�1HԵ54?EҖ����>   �;�>���ؽ�?   ��"=��(w��M�B�t��{\:�l�0�EҖ�����   �;従��ؽ��   ��"�����\�?(;���	?�1HԵ54?'X4R��>   ~���> �g��gt?   s�"=�3���>ٚ����>0(ě�U�>\����a�>����>Z�p2�B?�ҺV)=��z���=�h|#*a�>I��.fc�=�i'$�8�>N<!7�w�=��fm�9�>p?Y6�w�=~�b�9�>pp�@�#OW$���<�"���$@pao{,׿�#�2�-@   TMC_DR_20_C      CSRDRIFT�E���>�i�_�=-�l,7Aƽ�c�Ά��n3{	����h{�>��"���ٚ����>&u5@s��!g}zWv�G�Gt����E%�q��=�sW�޻پX����>4�35�;�=;x�\,`=�*��3��Gzt;�;\����a�>埿*���$���Mi=\����Q�%��_��>��lM2�����!Z�;Z�p2�B?B�] ٿ6�J=�CY)=P0���?(;���	?׈���A4?EҖ����>   ;�>���ؽ�?   �"�"=1��e>��M�B�t��ӧY�i1�EҖ�����   ;従��ؽ��   �"�"�P0���?(;���	?׈���A4?'X4R��>   ����> �g��gt?   �W�"=�E���>ٚ����>پX����>\����a�>%��_��>Z�p2�B?J=�CY)=��z���=�h|#*a�>I��.fc�=�i'$�8�>N<!7�w�=��fm�9�>p?Y6�w�=~�b�9�>P�V���	@��9��ӿ�wmf%@���`-�ڿ�#�2�-@	   TMC_WA_OU      WATCH�E���>�i�_�=-�l,7Aƽ�c�Ά��n3{	����h{�>��"���ٚ����>&u5@s��!g}zWv�G�Gt����E%�q��=�sW�޻پX����>4�35�;�=;x�\,`=�*��3��Gzt;�;\����a�>埿*���$���Mi=\����Q�%��_��>��lM2�����!Z�;Z�p2�B?B�] ٿ6�J=�CY)=P0���?(;���	?׈���A4?EҖ����>   ;�>���ؽ�?   �"�"=1��e>��M�B�t��ӧY�i1�EҖ�����   ;従��ؽ��   �"�"�P0���?(;���	?׈���A4?'X4R��>   ����> �g��gt?   �W�"=�E���>ٚ����>پX����>\����a�>%��_��>Z�p2�B?J=�CY)=��z���=�h|#*a�>I��.fc�=�i'$�8�>N<!7�w�=��fm�9�>p?Y6�w�=~�b�9�>P�V���	@��9��ӿ�wmf%@���`-�ڿ