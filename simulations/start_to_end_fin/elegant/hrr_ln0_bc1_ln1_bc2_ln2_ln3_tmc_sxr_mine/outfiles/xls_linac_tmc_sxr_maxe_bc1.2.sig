SDDS1
!# little-endian
&description text="sigma matrix--input: xls_linac_tmc_sxr_maxe_bc1.track.ele  lattice: xls_linac_tmc_sxr_maxe.2.lte", contents="sigma matrix", &end
&parameter name=Step, description="Simulation step", type=long, &end
&parameter name=SVNVersion, description="SVN version number", type=string, fixed_value=unknown, &end
&column name=s, units=m, description=Distance, type=double,  &end
&column name=ElementName, description="Element name", format_string=%10s, type=string,  &end
&column name=ElementOccurence, description="Occurence of element", format_string=%6ld, type=long,  &end
&column name=ElementType, description="Element-type name", format_string=%10s, type=string,  &end
&column name=s1, symbol="$gs$r$b1$n", units=m, description="sqrt(<x*x>)", type=double,  &end
&column name=s12, symbol="$gs$r$b12$n", units=m, description="<x*xp'>", type=double,  &end
&column name=s13, symbol="$gs$r$b13$n", units="m$a2$n", description="<x*y>", type=double,  &end
&column name=s14, symbol="$gs$r$b14$n", units=m, description="<x*y'>", type=double,  &end
&column name=s15, symbol="$gs$r$b15$n", units="m$a2$n", description="<x*s>", type=double,  &end
&column name=s16, symbol="$gs$r$b16$n", units=m, description="<x*delta>", type=double,  &end
&column name=s17, symbol="$gs$r$b17$n", units="m*s", description="<x*t>", type=double,  &end
&column name=s2, symbol="$gs$r$b2$n", description="sqrt(<x'*x'>)", type=double,  &end
&column name=s23, symbol="$gs$r$b23$n", units=m, description="<x'*y>", type=double,  &end
&column name=s24, symbol="$gs$r$b24$n", description="<x'*y'>", type=double,  &end
&column name=s25, symbol="$gs$r$b25$n", units=m, description="<x'*s>", type=double,  &end
&column name=s26, symbol="$gs$r$b26$n", description="<x'*delta>", type=double,  &end
&column name=s27, symbol="$gs$r$b27$n", units=s, description="<x'*t>", type=double,  &end
&column name=s3, symbol="$gs$r$b3$n", units=m, description="sqrt(<y*y>)", type=double,  &end
&column name=s34, symbol="$gs$r$b34$n", units=m, description="<y*y'>", type=double,  &end
&column name=s35, symbol="$gs$r$b35$n", units="m$a2$n", description="<y*s>", type=double,  &end
&column name=s36, symbol="$gs$r$b36$n", units=m, description="<y*delta>", type=double,  &end
&column name=s37, symbol="$gs$r$b37$n", units="m*s", description="<y*t>", type=double,  &end
&column name=s4, symbol="$gs$r$b4$n", description="sqrt(<y'*y')>", type=double,  &end
&column name=s45, symbol="$gs$r$b45$n", units=m, description="<y'*s>", type=double,  &end
&column name=s46, symbol="$gs$r$b46$n", description="<y'*delta>", type=double,  &end
&column name=s47, symbol="$gs$r$b47$n", units=s, description="<y'*t>", type=double,  &end
&column name=s5, symbol="$gs$r$b5$n", units=m, description="sqrt(<s*s>)", type=double,  &end
&column name=s56, symbol="$gs$r$b56$n", units=m, description="<s*delta>", type=double,  &end
&column name=s57, symbol="$gs$r$b57$n", units="m*s", description="<s*t>", type=double,  &end
&column name=s6, symbol="$gs$r$b6$n", description="sqrt(<delta*delta>)", type=double,  &end
&column name=s67, symbol="$gs$r$b67$n", units=s, description="<delta*t>", type=double,  &end
&column name=s7, symbol="$gs$r$b7$n", description="sqrt(<t*t>)", type=double,  &end
&column name=ma1, symbol="max$sb$ex$sb$e", units=m, description="maximum absolute value of x", type=double,  &end
&column name=ma2, symbol="max$sb$ex'$sb$e", description="maximum absolute value of x'", type=double,  &end
&column name=ma3, symbol="max$sb$ey$sb$e", units=m, description="maximum absolute value of y", type=double,  &end
&column name=ma4, symbol="max$sb$ey'$sb$e", description="maximum absolute value of y'", type=double,  &end
&column name=ma5, symbol="max$sb$e$gD$rs$sb$e", units=m, description="maximum absolute deviation of s", type=double,  &end
&column name=ma6, symbol="max$sb$e$gd$r$sb$e", description="maximum absolute value of delta", type=double,  &end
&column name=ma7, symbol="max$sb$e$gD$rt$sb$e", units=s, description="maximum absolute deviation of t", type=double,  &end
&column name=minimum1, symbol="x$bmin$n", units=m, type=double,  &end
&column name=minimum2, symbol="x'$bmin$n", units=m, type=double,  &end
&column name=minimum3, symbol="y$bmin$n", units=m, type=double,  &end
&column name=minimum4, symbol="y'$bmin$n", units=m, type=double,  &end
&column name=minimum5, symbol="$gD$rs$bmin$n", units=m, type=double,  &end
&column name=minimum6, symbol="$gd$r$bmin$n", units=m, type=double,  &end
&column name=minimum7, symbol="t$bmin$n", units=s, type=double,  &end
&column name=maximum1, symbol="x$bmax$n", units=m, type=double,  &end
&column name=maximum2, symbol="x'$bmax$n", units=m, type=double,  &end
&column name=maximum3, symbol="y$bmax$n", units=m, type=double,  &end
&column name=maximum4, symbol="y'$bmax$n", units=m, type=double,  &end
&column name=maximum5, symbol="$gD$rs$bmax$n", units=m, type=double,  &end
&column name=maximum6, symbol="$gd$r$bmax$n", units=m, type=double,  &end
&column name=maximum7, symbol="t$bmax$n", units=s, type=double,  &end
&column name=Sx, symbol="$gs$r$bx$n", units=m, description=sqrt(<(x-<x>)^2>), type=double,  &end
&column name=Sxp, symbol="$gs$r$bx'$n", description=sqrt(<(x'-<x'>)^2>), type=double,  &end
&column name=Sy, symbol="$gs$r$by$n", units=m, description=sqrt(<(y-<y>)^2>), type=double,  &end
&column name=Syp, symbol="$gs$r$by'$n", description=sqrt(<(y'-<y'>)^2>), type=double,  &end
&column name=Ss, symbol="$gs$r$bs$n", units=m, description=sqrt(<(s-<s>)^2>), type=double,  &end
&column name=Sdelta, symbol="$gs$bd$n$r", description=sqrt(<(delta-<delta>)^2>), type=double,  &end
&column name=St, symbol="$gs$r$bt$n", units=s, description=sqrt(<(t-<t>)^2>), type=double,  &end
&column name=ex, symbol="$ge$r$bx$n", units=m, description="geometric horizontal emittance", type=double,  &end
&column name=enx, symbol="$ge$r$bx,n$n", units=m, description="normalized horizontal emittance", type=double,  &end
&column name=ecx, symbol="$ge$r$bx,c$n", units=m, description="geometric horizontal emittance less dispersive contributions", type=double,  &end
&column name=ecnx, symbol="$ge$r$bx,cn$n", units=m, description="normalized horizontal emittance less dispersive contributions", type=double,  &end
&column name=ey, symbol="$ge$r$by$n", units=m, description="geometric vertical emittance", type=double,  &end
&column name=eny, symbol="$ge$r$by,n$n", units=m, description="normalized vertical emittance", type=double,  &end
&column name=ecy, symbol="$ge$r$by,c$n", units=m, description="geometric vertical emittance less dispersive contributions", type=double,  &end
&column name=ecny, symbol="$ge$r$by,cn$n", units=m, description="normalized vertical emittance less dispersive contributions", type=double,  &end
&column name=betaxBeam, symbol="$gb$r$bx,beam$n", units=m, description="betax for the beam, excluding dispersive contributions", type=double,  &end
&column name=alphaxBeam, symbol="$ga$r$bx,beam$n", description="alphax for the beam, excluding dispersive contributions", type=double,  &end
&column name=betayBeam, symbol="$gb$r$by,beam$n", units=m, description="betay for the beam, excluding dispersive contributions", type=double,  &end
&column name=alphayBeam, symbol="$ga$r$by,beam$n", description="alphay for the beam, excluding dispersive contributions", type=double,  &end
&data mode=binary, &end
.                 _BEG_      MARK0*FU_� ?$'��#��=sz*j>kٽ���Fxڳ=8`��
�>����U>BN��[E<I����6�>���G&��zy�Dj��{��Q|9�=����>B>�-K2<��^�� ?<��N���oՄas��c�/N�z�糦-��#��N�>D��,7-�=/�.��X�=����E��;}2����4?���/�?�>F/��9ط<
+�ԨĂ?���=T�ִtr=�����6?�W�R��?ǐ�w5(?�� ? ��q�wF?b$��? ��%�=\��e�5�JZix�ǐ�w5(�>{S�Gl� �B c�D����O𷒿 �k�o��������6?�W�R��?��+�(?�� ? ��q�wF?b$��? ��%�=0*FU_� ?I����6�>��^�� ?#��N�>}2����4?
+�ԨĂ?T�ִtr=-V���=xR��r��>
�}��=۳�zs��>�*��!�=��o�*��>�uN��!�=_�ʏ$��>L�x��g@�׆n���旙t@�-��%�?           BNCH      CHARGE0*FU_� ?$'��#��=sz*j>kٽ���Fxڳ=8`��
�>����U>BN��[E<I����6�>���G&��zy�Dj��{��Q|9�=����>B>�-K2<��^�� ?<��N���oՄas��c�/N�z�糦-��#��N�>D��,7-�=/�.��X�=����E��;}2����4?���/�?�>F/��9ط<
+�ԨĂ?���=T�ִtr=�����6?�W�R��?ǐ�w5(?�� ? ��q�wF?b$��? ��%�=\��e�5�JZix�ǐ�w5(�>{S�Gl� �B c�D����O𷒿 �k�o��������6?�W�R��?��+�(?�� ? ��q�wF?b$��? ��%�=0*FU_� ?I����6�>��^�� ?#��N�>}2����4?
+�ԨĂ?T�ִtr=-V���=xR��r��>
�}��=۳�zs��>�*��!�=��o�*��>�uN��!�=_�ʏ$��>L�x��g@�׆n���旙t@�-��%�?        
   BC1_IN_CNT      CENTER0*FU_� ?$'��#��=sz*j>kٽ���Fxڳ=8`��
�>����U>BN��[E<I����6�>���G&��zy�Dj��{��Q|9�=����>B>�-K2<��^�� ?<��N���oՄas��c�/N�z�糦-��#��N�>F��,7-�=/�.��X�=����E��;}2����4?���/�?�>F/��9ط<
+�ԨĂ?���=T�ִtr=Q7�ƪ�6?���+��?)�z5(?.f\�� ? ��q�wF?b$��? ��%�=�Jx���5�2򵐤 �)�z5(���t�l� �B c�D����O𷒿 �k�o���Q7�ƪ�6?���+��?��=�(?.f\�� ? ��q�wF?b$��? ��%�=0*FU_� ?I����6�>��^�� ?#��N�>}2����4?
+�ԨĂ?T�ִtr=-V���=xR��r��>
�}��=۳�zs��>�*��!�=��o�*��>�uN��!�=_�ʏ$��>L�x��g@�׆n���旙t@�-��%�?V(�2�8�?	   BC1_DR_V1      DRIF�!|L�(?$d��� >d�+$�ٽ���R���=c��Q_>=�H�vzZ>0�3C-7J<I����6�>ˬ�����zy�Dj��erf�{9�=����>B>��/�2<_pNpt�>9Lrtz�i�R7T�����?����g<���#��N�>�ؤ�:-�=/�.��X�=-`V>���;I�����4?�V!�?�><O�׷<
+�ԨĂ?Ļ�(��=��o��sr=��
��s9?���+��?`A�^B`$?.f\�� ? �$S�wF?b$��? ����=4���8�2򵐤 �`A�^B`$���t�l� �:c�D����O𷒿 ���*�����
��s9?���+��?]9V���"?.f\�� ? �$S�wF?b$��? ����=�!|L�(?I����6�>_pNpt�>#��N�>I�����4?
+�ԨĂ?��o��sr=*V���=uR��r��>�}��=س�zs��>�*��!�=��o�*��>�uN��!�=_�ʏ$��>��}@�83~ch����g�K@�c�nd��?��C(&��?   BC1_DR_QD_ED      DRIF6�	�m?5���PT >�����ڽ��'����=�6@W�>�k;C��Z>�F��W�J<I����6�>��n�-B��zy�Dj��K�{�{9�=����>B>�%DL2<�g:��>gH��B[�|�������m�H{]�6raI[�#��N�>�^�<;-�=/�.��X�=�����;ꌓ���4?�d3 �?�>Z4e�׷<
+�ԨĂ?�7a㸤=#����sr=����9?���+��?���$?.f\�� ? ��P�wF?b$��?  z���=���u9�2򵐤 ����$���t�l� �*<c�D����O𷒿  mU%�������9?���+��?�����B"?.f\�� ? ��P�wF?b$��?  z���=6�	�m?I����6�>�g:��>#��N�>ꌓ���4?
+�ԨĂ?#����sr=*V���=uR��r��>�}��=س�zs��>�*��!�=��o�*��>�uN��!�=_�ʏ$��>����@#w����=��@��@+֯�O��?���<���?
   BC1_QD_V01      QUAD�LU䝧?9�m#�=���"ڽ3x�:�=O�c�N)>1��0[>#��e�J<K��"���>x��:����o�'e��,�BӠa�=9?.?�P<>"�',<�/����>j4TW������C�m�����������f=����>̱v��=����o�=/Λ���;A쳃��4?v�:�?�>d#`��׷<
+�ԨĂ?�a_���=��:��sr=�
�2��9?�ƤF'?�Bwl�#?Խ��i ?  �N�wF?b$��? ��9��=�Z2W@9�*�����Bwl�#��W���  >c�D����O𷒿 �77 ����
�2��9?�ƤF'?�M �� "?Խ��i ?  �N�wF?b$��? ��9��=�LU䝧?K��"���>�/����>f=����>A쳃��4?
+�ԨĂ?��:��sr=���*��=b������>l&���=]c����>���@!�=�
r����>����:!�=n�h�|��>���@Ʈl� >����3F�?@:�W�`�?���<���?   BC1_COR      KICKER�LU䝧?9�m#�=���"ڽ3x�:�=O�c�N)>1��0[>#��e�J<K��"���>x��:����o�'e��,�BӠa�=9?.?�P<>"�',<�/����>j4TW������C�m�����������f=����>̱v��=����o�=/Λ���;A쳃��4?v�:�?�>d#`��׷<
+�ԨĂ?�a_���=��:��sr=�
�2��9?�ƤF'?�Bwl�#?Խ��i ?  �N�wF?b$��? ��9��=�Z2W@9�*�����Bwl�#��W���  >c�D����O𷒿 �77 ����
�2��9?�ƤF'?�M �� "?Խ��i ?  �N�wF?b$��? ��9��=�LU䝧?K��"���>�/����>f=����>A쳃��4?
+�ԨĂ?��:��sr=���*��=b������>l&���=]c����>���@!�=�
r����>����:!�=n�h�|��>���@Ʈl� >����3F�?@:�W�`�?���<���?   BC1_BPM      MONI�LU䝧?9�m#�=���"ڽ3x�:�=O�c�N)>1��0[>#��e�J<K��"���>x��:����o�'e��,�BӠa�=9?.?�P<>"�',<�/����>j4TW������C�m�����������f=����>̱v��=����o�=/Λ���;A쳃��4?v�:�?�>d#`��׷<
+�ԨĂ?�a_���=��:��sr=�
�2��9?�ƤF'?�Bwl�#?Խ��i ?  �N�wF?b$��? ��9��=�Z2W@9�*�����Bwl�#��W���  >c�D����O𷒿 �77 ����
�2��9?�ƤF'?�M �� "?Խ��i ?  �N�wF?b$��? ��9��=�LU䝧?K��"���>�/����>f=����>A쳃��4?
+�ԨĂ?��:��sr=���*��=b������>l&���=]c����>���@!�=�
r����>����:!�=n�h�|��>���@Ʈl� >����3F�?@:�W�`�?Bz9Q�$�?
   BC1_QD_V01      QUADP��B��?+�� P�=� ��N8ڽ�� 8�X=s�۪�m>�:��n[>��?�(K<��8ʺ6�>,�?�����`�P���Ir8�=�&n�i4>���E��#<�D�7zz�>�ح�*���J���݇`�c��_ e�S�O���>6�q궼�=>������=�9��/�;_B ���4?hq�?�>d�b�׷<
+�ԨĂ?V���=����sr=��ژ�9?�1���?ɐ�5�#?$���� ?  M�wF?b$��? �Q���==��j_9��-���ɐ�5�#���'�o� ��?c�D����O𷒿 �������ژ�9?�1���?��^�I�!?$���� ?  M�wF?b$��? �Q���=P��B��?��8ʺ6�>�D�7zz�>S�O���>_B ���4?
+�ԨĂ?����sr=]��3	�=~�@2���>llD���=}H���>znSC� �=��K��>�;� �=��߾뻂>���s��@,dc�Ǟ�Ƿ&��@d���l�?�	�F���?   BC1_DR_QD_ED      DRIFjB����?S~�S�=�y�9"QڽQ��ߘ��$f�^�>$b4,�[>��w�^K<��8ʺ6�>��T�����`�P��ӆ�o8�=�&n�i4>�����#<�8���D�>���O�޽��1~)���=<Z���NjlU���S�O���>�a�4���=>������=a��0z/�;�zU���4? i��?�>���x�׷<
+�ԨĂ?�3VЦ�=&t~;�sr=�����9?�1���?��]�q#?$���� ? ��K�wF?b$��?  ���=xV���u9��-�����]�q#���'�o� ��Ac�D����O𷒿 ����������9?�1���?i߀�!?$���� ? ��K�wF?b$��?  ���=jB����?��8ʺ6�>�8���D�>S�O���>�zU���4?
+�ԨĂ?&t~;�sr=]��3	�=~�@2���>llD���=}H���>znSC� �=��K��>�;� �=��߾뻂>�S�H�L@�ف��x�}xO�@�%�8��?Ʊ�4j@	   BC1_DR_V2      DRIF�����?��>��	>��i����"��˽��;U�>�B1o�l>7T��K\<��8ʺ6�>R�@��Ͻ���`�P���!��8�=�&n�i4>��M�#<A!;���?����>E�	%R��=Ɠ%���>?�<�C<S�O���>2Bn߼�=>������=d9�[H,�;m�%��4?0$=��?�>���Է<
+�ԨĂ?LF�L@�=8�~�pr=T�2wgD?�1���?b�ReP>J?$���� ?  �r�wF?b$��?  L�
�=�x0���C��-�����G�F���'�o�  Xd�D����O𷒿  ��"���T�2wgD?�1���?b�ReP>J?$���� ?  �r�wF?b$��?  L�
�=�����?��8ʺ6�>A!;���?S�O���>m�%��4?
+�ԨĂ?8�~�pr=]��3	�=~�@2���>llD���=}H���>�nSC� �=ȨK��>�;� �=��߾뻂>�$}��<@��5�����72�/@���'����S��@   BC1_DR_QD_ED      DRIF�)��C�?�F�?6�	>1��T��`Y���6˽ ��>��D浸l>�z�e\<��8ʺ6�>����Ͻ���`�P��Ӡ �8�=�&n�i4>XU�y�#<O��x?O���>Fs*��6�=ָ�x� >o7jm}<S�O���>�l�߼�=>������=�\�wB,�;�%��4?��|��?�>��cԷ<
+�ԨĂ?!��:�=S���pr=Ҧ(�xD?�1���?4����kJ?$���� ?  Eq�wF?b$��?  �K~
�=DhB�L�C��-����@kd�F���'�o�  Zd�D����O𷒿  �B���Ҧ(�xD?�1���?4����kJ?$���� ?  Eq�wF?b$��?  �K~
�=�)��C�?��8ʺ6�>O��x?S�O���>�%��4?
+�ԨĂ?S���pr=`��3	�=��@2���>olD���=}H���>{nSC� �=��K��>�;� �=��߾뻂>T����<@(�}������>i�0@^��î��_0��@
   BC1_QD_V02      QUAD�os�?d��2P�.>�s������QeΘ��=����5 >�(���m>����K�\<�{V)�??�Ԥo�
�Qh��2�=Y�>s��>����Y>j�pY��I<��Ǌ�?t62�����
E�=��A5->a:=|�<3|����>�R+�c���צ�ݠ�W�sc�ֻ��$��4?�t��?�>(����ӷ<
+�ԨĂ?�x!4�=q2���pr=���Q�D?��l,q�1?���fJ?z�vVb�$?  $m�wF?b$��?  ��x
�=��,]+D�5�rU1�1���F�z�vVb�$�  0[d�D����O𷒿  �#������Q�D?��l,q�1?���fJ?'O(p.$?  $m�wF?b$��?  ��x
�=�os�?�{V)�??��Ǌ�?3|����>��$��4?
+�ԨĂ?q2���pr=�:��'��=�ܴm	s�>"�����=�\�Rg�>,ZI"1�=���UY��>aA��*�=�L��R��>u�u��=@����a+�K\���0@�{���@�_0��@   BC1_COR      KICKER�os�?d��2P�.>�s������QeΘ��=����5 >�(���m>����K�\<�{V)�??�Ԥo�
�Qh��2�=Y�>s��>����Y>j�pY��I<��Ǌ�?t62�����
E�=��A5->a:=|�<3|����>�R+�c���צ�ݠ�W�sc�ֻ��$��4?�t��?�>(����ӷ<
+�ԨĂ?�x!4�=q2���pr=���Q�D?��l,q�1?���fJ?z�vVb�$?  $m�wF?b$��?  ��x
�=��,]+D�5�rU1�1���F�z�vVb�$�  0[d�D����O𷒿  �#������Q�D?��l,q�1?���fJ?'O(p.$?  $m�wF?b$��?  ��x
�=�os�?�{V)�??��Ǌ�?3|����>��$��4?
+�ԨĂ?q2���pr=�:��'��=�ܴm	s�>"�����=�\�Rg�>,ZI"1�=���UY��>aA��*�=�L��R��>u�u��=@����a+�K\���0@�{���@�_0��@   BC1_BPM      MONI�os�?d��2P�.>�s������QeΘ��=����5 >�(���m>����K�\<�{V)�??�Ԥo�
�Qh��2�=Y�>s��>����Y>j�pY��I<��Ǌ�?t62�����
E�=��A5->a:=|�<3|����>�R+�c���צ�ݠ�W�sc�ֻ��$��4?�t��?�>(����ӷ<
+�ԨĂ?�x!4�=q2���pr=���Q�D?��l,q�1?���fJ?z�vVb�$?  $m�wF?b$��?  ��x
�=��,]+D�5�rU1�1���F�z�vVb�$�  0[d�D����O𷒿  �#������Q�D?��l,q�1?���fJ?'O(p.$?  $m�wF?b$��?  ��x
�=�os�?�{V)�??��Ǌ�?3|����>��$��4?
+�ԨĂ?q2���pr=�:��'��=�ܴm	s�>"�����=�\�Rg�>,ZI"1�=���UY��>aA��*�=�L��R��>u�u��=@����a+�K\���0@�{���@���x�@
   BC1_QD_V02      QUAD���`ϳ?��rC<>�����������8�=��-%% > ����m>�V�m]<�3�?5�����*����=�K�`>>㚬i��g>�_�8vhW<�FCd�?�YW2��#�v�����=���9R�>���l�C<f��?���="y��Y>zd���f�g���F+��4?V«�?�>�T}
�ӷ<
+�ԨĂ?:�3.�=䝬��pr=�J��\/E?/��V@?u҇�J?l��
=?  EW�wF?b$��?  ZFs
�=�M�:�D���p��@����FlF�l��
=�  �`d�D����O𷒿  �����J��\/E?/��V@?u҇�J?�G�w�:?  EW�wF?b$��?  ZFs
�=���`ϳ?�3�?�FCd�?f��?F+��4?
+�ԨĂ?䝬��pr=���0��=|��⎂>I&->���=�8�K��>����)�=W�ؔ7ł>���_�)�=1@��0ł>�Z4�8?@�s�J�!9�\Q!/@V.��j}!@�x��@   BC1_DR_QD_ED      DRIF�+~�d�?�mF�[;=>�Џ%F(����r1E��=@/?��!>W�k��n>��>Ql^<�3�?��-�$���*����=~�O�_>>㚬i��g>+�{�nhW<�L�P�i?��в_#�\ƐR�p�=�Ă�i>�]�-��<f��?ަ��&y��Y>zd�����5���(@1��4?���?�>�����ӷ<
+�ԨĂ?;2�'�=4&�2�pr=��%��E?/��V@?�0K��cI?l��
=?  ~/�wF?b$��?  [Tm
�=�7{�6XE���p��@�>�aU:�E�l��
=�  �kd�D����O𷒿  ʠ�����%��E?/��V@?�0K��cI?�G�w�:?  ~/�wF?b$��?  [Tm
�=�+~�d�?�3�?�L�P�i?f��?(@1��4?
+�ԨĂ?4&�2�pr=���0��=���⎂>g%->���=�8�K��>����)�=W�ؔ7ł>���_�)�=1@��0ł>3�S�v�@@;7��d�9��W���-@��=!@�5w�@	   BC1_DR_V3      DRIF>�W��F?&���uA>��2'y��d7X���=����cT$>_ƫg�Vr>�BAC�!b<�3�?�Hi�I*���*����=W&��Z>>㚬i��g>���xBhW<�~m��?/�$�� �H�h_c�=mpw��>n �ML<f��?"��By��Y>zd���a
)O���r�b���4?��*�?�>�����ӷ<
+�ԨĂ?���=�w�pr=���l �I?/��V@?@A����E?l��
=?  �E�wF?b$��?  JiJ
�=�FXWI���p��@�S:�c��B�l��
=�  ��d�D����O𷒿  e�훂����l �I?/��V@?@A����E?�G�w�:?  �E�wF?b$��?  JiJ
�=>�W��F?�3�?�~m��?f��?r�b���4?
+�ԨĂ?�w�pr=���0��=���⎂>i%->���=�8�K��>'���)�=��ؔ7ł>���_�)�=l@��0ł>y#���G@����:?����5!�%@|��r�)@��ԕ>@   BC1_DR_QD_ED      DRIF���f?'	ީ�A>�?oET�����=�{'�$>������r>��+;�b<�3�?$�3�|���*����=ˀ��Y>>㚬i��g> �y�:hW<qls��P?=�Gç �]�Β���=��s�Y>�1���<f��?gET�Fy��Y>zd����������܏���4?�u/�?�>�y{�ӷ<
+�ԨĂ?`i�V��=ّ�pr=�qo��J?/��V@?�iti�%E?l��
=?  �wF?b$��?  JwD
�=@����J���p��@��N'r6�A�l��
=�  ��d�D����O𷒿  Aw蛂��qo��J?/��V@?�iti�%E?�G�w�:?  �wF?b$��?  JwD
�=���f?�3�?qls��P?f��?�܏���4?
+�ԨĂ?ّ�pr=���0��=���⎂>k%->���=�8�K��>
���)�=t�ؔ7ł>���_�)�=N@��0ł>��W��'I@1�����?��Çm�$@�1�mbQ@��cXg@
   BC1_QD_V03      QUADN}�8��?j ��r
=>��$���I�)���=
H�,_[%>O �B7Ds>���oc<q�P8�8?����ڽ�+,��&�=in`��>��-���b>��� �uR<9���F�
?;m�/`p�mo�tn�=UXJ���>��m9<Y4���>�xM�Y��<k�%ڛ����\�	�.�����4?���	�?�>��ݵӷ<
+�ԨĂ?�oc��=g�b��pr=?��^<K?�ʘ��9?���D?� ��7?  q��wF?b$��?  ��>
�=L^�C�J���@�C9�� sA�� ��7�  P�d�D����O𷒿  _㛂�?��^<K?�ʘ��9?���D?��G&�6?  q��wF?b$��?  ��>
�=N}�8��?q�P8�8?9���F�
?Y4���>.�����4?
+�ԨĂ?g�b��pr=b�e|��=%�Wit�>����6��=J�Z�h�>H�[�$�=�K:�_��>1ɷ��$�=�bq�X��>�`�uJ@��q�J�9�o&z�2�#@vN���n@��cXg@   BC1_COR      KICKERN}�8��?j ��r
=>��$���I�)���=
H�,_[%>O �B7Ds>���oc<q�P8�8?����ڽ�+,��&�=in`��>��-���b>��� �uR<9���F�
?;m�/`p�mo�tn�=UXJ���>��m9<Y4���>�xM�Y��<k�%ڛ����\�	�.�����4?���	�?�>��ݵӷ<
+�ԨĂ?�oc��=g�b��pr=?��^<K?�ʘ��9?���D?� ��7?  q��wF?b$��?  ��>
�=L^�C�J���@�C9�� sA�� ��7�  P�d�D����O𷒿  _㛂�?��^<K?�ʘ��9?���D?��G&�6?  q��wF?b$��?  ��>
�=N}�8��?q�P8�8?9���F�
?Y4���>.�����4?
+�ԨĂ?g�b��pr=b�e|��=%�Wit�>����6��=J�Z�h�>H�[�$�=�K:�_��>1ɷ��$�=�bq�X��>�`�uJ@��q�J�9�o&z�2�#@vN���n@��cXg@   BC1_BPM      MONIN}�8��?j ��r
=>��$���I�)���=
H�,_[%>O �B7Ds>���oc<q�P8�8?����ڽ�+,��&�=in`��>��-���b>��� �uR<9���F�
?;m�/`p�mo�tn�=UXJ���>��m9<Y4���>�xM�Y��<k�%ڛ����\�	�.�����4?���	�?�>��ݵӷ<
+�ԨĂ?�oc��=g�b��pr=?��^<K?�ʘ��9?���D?� ��7?  q��wF?b$��?  ��>
�=L^�C�J���@�C9�� sA�� ��7�  P�d�D����O𷒿  _㛂�?��^<K?�ʘ��9?���D?��G&�6?  q��wF?b$��?  ��>
�=N}�8��?q�P8�8?9���F�
?Y4���>.�����4?
+�ԨĂ?g�b��pr=b�e|��=%�Wit�>����6��=J�Z�h�>H�[�$�=�K:�_��>1ɷ��$�=�bq�X��>�`�uJ@��q�J�9�o&z�2�#@vN���n@�?�	�@
   BC1_QD_V03      QUAD�=8aK?l�2|!�5>	�����������B�=bq���%>���˖s>�T^c<x��H?Y@��o�潛,S�!�=��k��>�\KdJ$[>6��P�J<�.֙
?ړ��YD�5�0�=�;�m�j>mK���< �L@F�>R������Q%hώ��[�s9��.^򃥬4?w0�?�>��L�ӷ<
+�ԨĂ?Mȷu�=~��pr=C7�K?���
�2?�[�*3D?�d��4u2?  ���wF?b$��?  O]9
�=�O/�K��td`2��f���A��d��4u2�  ��d�D����O𷒿  �Dޛ��C7�K?���
�2?�[�*3D?����~1?  ���wF?b$��?  O]9
�=�=8aK?x��H?�.֙
? �L@F�>.^򃥬4?
+�ԨĂ?~��pr=�/d
���=��՝x�>7�(��=λ`=�l�>K�o�!�=�A�R��>�S!�=��f�K��>�h��WK@�,T�I3���@���"@����@ڑ�9��@   BC1_DR_QD_ED      DRIF.a�u�?�:�5.�5>�H�ת��bM��C��=���<�&>�v�n��s>0�:��c<x��H?)]U�i潛,S�!�=r]Eb�>�\KdJ$[>E��H�J<M}�e �	?���-��YkSj���=!e�\%>+ ^�Ū< �L@F�>BR�v����Q%hώ�����)
�軅8�~��4?�!!��?�>��OX�ӷ<
+�ԨĂ?���+�=6��pr=��t��L?���
�2?ء���C?�d��4u2?  {��wF?b$��?  �3
�=�j��kK��td`2����ئ�@��d��4u2�  �d�D����O𷒿  ��؛����t��L?���
�2?ء���C?����~1?  {��wF?b$��?  �3
�=.a�u�?x��H?M}�e �	? �L@F�>�8�~��4?
+�ԨĂ?6��pr=�/d
���=��՝x�>7�(��=̻`=�l�>K�o�!�=�A�R��>�S!�=��f�K��>��]A+L@٨8Nד3�̉�f�"@��w��@�[�@	   BC1_DR_V4      DRIF2VR�L� ?N9W� p7>x��&��O�����=���щ'>gm��<u>F�X%^�d<x��H?Og��m�㽛,S�!�=$����>�\KdJ$[>��/"�J<%"�K�<?�d7����|dD�l�=B�D9�>� a��n < �L@F�>�6������Q%hώ���lJX��YM�d��4?�n���?�>�Ԟ�ӷ<
+�ԨĂ?�4��̠=�B�Uspr=�a��M?���
�2?^�'z<�A?�d��4u2?  ��wF?b$��?  ��
�=���;fBM��td`2���҂��=��d��4u2�  <�d�D����O𷒿  �O�����a��M?���
�2?^�'z<�A?����~1?  ��wF?b$��?  ��
�=2VR�L� ?x��H?%"�K�<? �L@F�>YM�d��4?
+�ԨĂ?�B�Uspr=w/d
���=L�՝x�>�6�(��=X�`=�l�>K�o�!�=�A�R��>�S!�=��f�K��>Ju�EP@Yf���4�h���@ޛ�D@�UD&�@   BC1_DR_QD_ED      DRIF��NFZ8!?���i�7>�<�������9��D�=�6w��'>n��ۅu>�B��SGe<x��H?�o�]㽛,S�!�=�����>�\KdJ$[>�#�J<8Nߴ!�?��8뛤��N�!�=�\����>q��{�+ < �L@F�>r��硩��Q%hώ��5�H��軌.t_��4?�t���?�>1���ӷ<
+�ԨĂ?�:�IƠ=����mpr=���e^XN?���
�2?n�nZӑA?�d��4u2?  ���wF?b$��?  �
�=��s�\�M��td`2����f-=��d��4u2�  ��d�D����O𷒿  �⹛�����e^XN?���
�2?n�nZӑA?����~1?  ���wF?b$��?  �
�=��NFZ8!?x��H?8Nߴ!�? �L@F�>�.t_��4?
+�ԨĂ?����mpr=�.d
���=^�՝x�>�5�(��=k�`=�l�>K�o�!�=�A�R��>�S!�=��f�K��>V����P@��DI;5�kw"5=�@\���s`@&�����@
   BC1_QD_V04      QUAD<�#�4^!?���[9: >(�*	b��I��zV�=z s½(>���s��u>�2�te<Q�>t1�>btKd̽R����=�`1���=�:�İA>loz|1<�hK�]?)�H\L_���4�;��=�pi>�<(���;\'��6��>��7���jd�H�n�h�o'�]ֻ1��\��4?�D?��?�>;D#$zӷ<
+�ԨĂ?麂`��=u ��hpr=L*�ڗN?jw��T?����EA?L}ua��&?  ��wF?b$��?  �
�=B[����M��)? :.���7Dʋ<�L}ua��&�  ��d�D����O𷒿  /ƴ���L*�ڗN?jw��T?����EA?��&&?  ��wF?b$��?  �
�=<�#�4^!?Q�>t1�>�hK�]?\'��6��>1��\��4?
+�ԨĂ?u ��hpr=DS��	E�=/F��~�>h��/9�=瀾?Ղ>E��
�=|�H� ��>%�bo�= �[��>-�7ʸzP@��i"`���u@�]��@&�����@   BC1_COR      KICKER<�#�4^!?���[9: >(�*	b��I��zV�=z s½(>���s��u>�2�te<Q�>t1�>btKd̽R����=�`1���=�:�İA>loz|1<�hK�]?)�H\L_���4�;��=�pi>�<(���;\'��6��>��7���jd�H�n�h�o'�]ֻ1��\��4?�D?��?�>;D#$zӷ<
+�ԨĂ?麂`��=u ��hpr=L*�ڗN?jw��T?����EA?L}ua��&?  ��wF?b$��?  �
�=B[����M��)? :.���7Dʋ<�L}ua��&�  ��d�D����O𷒿  /ƴ���L*�ڗN?jw��T?����EA?��&&?  ��wF?b$��?  �
�=<�#�4^!?Q�>t1�>�hK�]?\'��6��>1��\��4?
+�ԨĂ?u ��hpr=DS��	E�=/F��~�>h��/9�=瀾?Ղ>E��
�=|�H� ��>%�bo�= �[��>-�7ʸzP@��i"`���u@�]��@&�����@   BC1_BPM      MONI<�#�4^!?���[9: >(�*	b��I��zV�=z s½(>���s��u>�2�te<Q�>t1�>btKd̽R����=�`1���=�:�İA>loz|1<�hK�]?)�H\L_���4�;��=�pi>�<(���;\'��6��>��7���jd�H�n�h�o'�]ֻ1��\��4?�D?��?�>;D#$zӷ<
+�ԨĂ?麂`��=u ��hpr=L*�ڗN?jw��T?����EA?L}ua��&?  ��wF?b$��?  �
�=B[����M��)? :.���7Dʋ<�L}ua��&�  ��d�D����O𷒿  /ƴ���L*�ڗN?jw��T?����EA?��&&?  ��wF?b$��?  �
�=<�#�4^!?Q�>t1�>�hK�]?\'��6��>1��\��4?
+�ԨĂ?u ��hpr=DS��	E�=/F��~�>h��/9�=瀾?Ղ>E��
�=|�H� ��>%�bo�= �[��>-�7ʸzP@��i"`���u@�]��@Oc��@
   BC1_QD_V04      QUAD�k���^!?�G��:�+c�������-���=|0��(>���7�u>!v���se<���n�>�y@��=��@�A���-��� ��;q�*RC�ø^p��2��^/?���tv车�O�^��=���n�\>?K����;`H���>�uE�O�s=}��9�=�z�]Ӵ�;�[��4?�����?�>�J�sӷ<
+�ԨĂ?�y��=�(��cpr=a�����N?t?�Ϊ�? Q�PA?1u�>#�?  ���wF?b$��?  �!
�=��-�M��:�U����F/7<�=5�-�  ��d�D����O𷒿  m�����a�����N?t?�Ϊ�? Q�PA?1u�>#�?  ���wF?b$��?  �!
�=�k���^!?���n�>�^/?`H���>�[��4?
+�ԨĂ?�(��cpr=1�O�!�=3��`�Ń>��T�=B��u���>��'���=\��Ѳ�>_ӜE��=���&˲�>Q/˔�zO@�au�@�HK��2@(�e����?;`�P1@   BC1_DR_QD_ED      DRIF���	K!?�H{Z��x�H]���������=-�p�'>�ݍ,�u>���SZe<���n�>Z�>TT�=��@�A�����Z�� ��;q�*RC��1����2�	"�C$?�Z�	"��2���=�I�~&b>zf����;`H���>_��O�s=}��9�=���ʹ�;�&[��4?"Ʊ��?�>���lӷ<
+�ԨĂ?��v4��=&�^pr=ɜ��rN?t?�Ϊ�?܊,�A?1u�>#�?  ˁ�wF?b$��?  �Q
�=N�$��M��:�U��Ы�V�;�=5�-�  ��d�D����O𷒿  ]5����ɜ��rN?t?�Ϊ�?܊,�A?1u�>#�?  ˁ�wF?b$��?  �Q
�=���	K!?���n�>	"�C$?`H���>�&[��4?
+�ԨĂ?&�^pr=��O�!�=���`�Ń>e�T�=��u���>��'���=\��Ѳ�>_ӜE��=���&˲�>VG���3O@�9���@ z�v�@��հ_W�?cBS�@	   BC1_DR_V5      DRIFw����� ?a��Nm�,A*QH����!ȣz)�=�����i'>Y�,_u>l_��~�d<���n�>�GK%�=��@�A���F�1~� ��;q�*RC���u��2��;�m��?��2���潗G�$��=�%zE"|>��a� <`H���>�\:N�s=}��9�=�ў���;V�*W��4?�
+��?�>=�)Lӷ<
+�ԨĂ?�����=}Y6^Epr=AmN
�M?t?�Ϊ�?zȉ�@?1u�>#�?  �y�wF?b$��?  ���	�=���^M��:�U�Q� L��:�=5�-�  ��d�D����O𷒿  j�����AmN
�M?t?�Ϊ�?zȉ�@?1u�>#�?  �y�wF?b$��?  ���	�=w����� ?���n�>�;�m��?`H���>V�*W��4?
+�ԨĂ?}Y6^Epr=�O�!�=��`�Ń>r�T�=��u���>��'���=]��Ѳ�>`ӜE��=���&˲�>�����M@�� d@����@������?���) @	   BC1_DR_20      DRIF�o*	 ?t������D+�;�����&��=+P�ܠ&>E*�<H�s>�wy��c<���n�>��5��=��@�A����d�#� ��;q�*RC� �e���2�dv�P%�?���4�⽤��TW�=d.��>�b��K[ <`H���>t���J�s=}��9�=.� Gu��;�8M��4?D*���?�>֫���ҷ<
+�ԨĂ?-r1�L�=��Owpr=\,έ$L?t?�Ϊ�?I,�ݦ$??1u�>#�?  ce�wF?b$��?  ���	�=U. �K��:�U���=�8�=5�-�  �d�D����O𷒿  �}P���\,έ$L?t?�Ϊ�?I,�ݦ$??1u�>#�?  ce�wF?b$��?  ���	�=�o*	 ?���n�>dv�P%�?`H���>�8M��4?
+�ԨĂ?��Owpr=�O�!�=��`�Ń>r�T�=��u���>��'���=\��Ѳ�>_ӜE��=���&˲�> #�7��J@�O+x�@Ѳg��@U N�G��?��SeL!@   BC1_DP_DIP1   	   CSRCSBEND�����j%?��0�X{r>�9V��g� � �=�B���d���5u2���Y/�䡼�x_{D?r��,5�ǽ�UB��5�m�s)����˚	�׾��S��TǼ�l�J?��Yd����*����=���T��>��+�u	<a�v7A�>��}�'��/�-�J�=��3 ֻ��yQ7�4?��.Qfj�>�(",�<�@��Â?z)�^�==^ �_�r=67l,uQ?���뢾V?X�B {=?�<��?  �?�]G?0B���?  ~��ׄ=67l,uQ����뢾V���nг�7�����F�  �]Y�E��}������  �.�;���S�h�$O?��.9�+U?X�B {=?�<��?  �?�]G?0B���?  ~��ׄ=�����j%?�x_{D?�l�J?a�v7A�>��yQ7�4?�@��Â?=^ �_�r=E�<�&�s>�Rm�?? 6�-�	>iՀ%"ɚ>#Q^��=뎊O���>������=������>!(�W�1@a����� @�2�#$@U�c8D�?jk����!@   BC1_DR_SIDE_C      CSRDRIFT#��&��4?C�x�
m�>-��(������KR��=�n ���y��%��q�ƾ>b�����x_{D?9>��»ɽ�UB��m���)�� �OW�׾�ÕP�TǼ�,�V�?h"(�Ȁ޽'G�9��=�����	>�:�<a�v7A�>�γZ�(��S��J�=�5��l�ֻ�W�F�4?���l�i�>�$(,�<���hmÂ?�Ю�X�=���&H�r=v���V?���뢾V?�~�$<?�<��?  �6_G?��Ǔg�?  ��؄=v���V����뢾V�L���g�6�����F�  �E�9c4����  :��:���Œ.�T?��.9�+U?�~�$<?�<��?  �6_G?��Ǔg�?  ��؄=#��&��4?�x_{D?�,�V�?a�v7A�>�W�F�4?���hmÂ?���&H�r=C�<�&�s>�{��??�xe�0
>�2a�>#Q^��=3nQ𒳂>;4���=l<~U���>��]���0@K/� @�FFƚ@�n��O��?������'@   BC1_DR_SIDE      DRIF���Ua?W����õ>�f��U���\�`c��=�2�䘏���5��-P�����Q���x_{D?�e
�P׽�UB���2�eJ*�� �OW�׾Z���~SǼe�f�\?K��0f�=�wնK?�=�o]�a>�Gl�B <a�v7A�>wN�;�:��S��J�=Up��ֻؚ���_��4?^$�j�>8���+�<���hmÂ?jWKc�=7*�b�r=���$��s?���뢾V?ɮk�1?�<��?  }j6mG?��Ǔg�?  �%��=���$��s����뢾V�����P.�����F�  ��E�9c4����  �_G.��}n���Nr?��.9�+U?ɮk�1?�<��?  }j6mG?��Ǔg�?  �%��=���Ua?�x_{D?e�f�\?a�v7A�>���_��4?���hmÂ?7*�b�r=��<�&�s>�|��??�3�0
>6��`�>#Q^��=3nQ𒳂>;4���=l<~U���>�j+2@���ɔ��?:(;]�@1Z�R=�ο�s�ʜ(@   BC1_DP_DIP2   	   CSRCSBENDkXޮ�`b?9Lpr��>�@p͝�ޭ�e��=k�0ew����/�����.��_ �ּ5�e�V!�>i5�E����q��{���U(�R�z佂�z��tF�x���#"��;3�?�$&�9�=����F��=Cj�>��R��;��l��>�&P�\��{�Z��=�r�R{	Ļ����_&?���`_9�>�-)K	ɛ<�Y����?��lO�I�<qâ���c=��E<<�t?�B�U�?�!��`�0?&�U
?  bt�8?���l�?  Jhv=��E<<�t�6M~�Y�����.�D�{��  ��U�6�ũ�?Q���  a�Lt�2R���@s?�B�U�?�!��`�0?&�U
?  bt�8?���l�?  Jhv=kXޮ�`b?5�e�V!�>�;3�?��l��>����_&?�Y����?qâ���c=6]@���`>GT�C�>���9���=rt�O��>���D��=s)���>�_�ߎ�=r�����>U�mO�U'@gD�x�� @N�-��@�:����ѿK ���O)@   BC1_DR_CENT_C      CSRDRIFTȯ�Vab?5�a[�>B�3���ｮ$oX���=�6�\���8=�����L��z��ּ5�e�V!�>Gy��總�q��{����,UM�z你O<�sF�z�E�r#"�ĭ�!H?���G��=1H���c�=�$l7�>�Gs2c��;��l��>�>��\��u��w�=Rz��	Ļ#[�I�_&?Ӧ�9�>�}���ț<(7�a���?��O�6I�<j{
�I�c=)��JL�t?�B�U�?=̇}20?&�U
?  �`z�8?��K��?  h�v=)��JL�t�6M~�Y��A�(Vp/�D�{��  ��N�6���ϰ��  N�\Lt����9.s?�B�U�?=̇}20?&�U
?  �`z�8?��K��?  h�v=ȯ�Vab?5�e�V!�>ĭ�!H?��l��>#[�I�_&?(7�a���?j{
�I�c=6]@���`>:���C�>-��~a�=���U]�>���D��=3p0����>����=�I[���>:
� ^$@F�[#Y5�?����g�@�J'9�ֿK ���O)@   BC1_BPM      MONIȯ�Vab?5�a[�>B�3���ｮ$oX���=�6�\���8=�����L��z��ּ5�e�V!�>Gy��總�q��{����,UM�z你O<�sF�z�E�r#"�ĭ�!H?���G��=1H���c�=�$l7�>�Gs2c��;��l��>�>��\��u��w�=Rz��	Ļ#[�I�_&?Ӧ�9�>�}���ț<(7�a���?��O�6I�<j{
�I�c=)��JL�t?�B�U�?=̇}20?&�U
?  �`z�8?��K��?  h�v=)��JL�t�6M~�Y��A�(Vp/�D�{��  ��N�6���ϰ��  N�\Lt����9.s?�B�U�?=̇}20?&�U
?  �`z�8?��K��?  h�v=ȯ�Vab?5�e�V!�>ĭ�!H?��l��>#[�I�_&?(7�a���?j{
�I�c=6]@���`>:���C�>-��~a�=���U]�>���D��=3p0����>����=�I[���>:
� ^$@F�[#Y5�?����g�@�J'9�ֿ~3�+1*@   BC1_DR_CENT      DRIF�}"P�ab?1�RD�F>0%y6p���j0���=��2nA����DD�����z (���ּ5�e�V!�>#�/`�:���q��{����P���z你O<�sF�e��#"�㸖�!�?��i�X�=Pa��n�=�ۏ��*>Ht����;��l��>��B+�\��u��w�=��ߌ	Ļp;�_&?[ٔ�
9�>�zwnț<(7�a���?�t��H�<1� ���c=��Y\�t?�B�U�?�m݉:0?&�U
?  0m|�8?��K��?  �8�v=��Y\�t�6M~�Y���c�%�/�D�{��  �M�6���ϰ��  �#Lt�T"��$s?�B�U�?�m݉:0?&�U
?  0m|�8?��K��?  �8�v=�}"P�ab?5�e�V!�>㸖�!�?��l��>p;�_&?(7�a���?1� ���c=6]@���`>:���C�>���~a�=���U]�>���D��=3p0����>����=�I[���>h��!@�6�DĲ�?!��.9�@��T��ۿ��	%�*@   BC1_DP_DIP3   	   CSRCSBEND8���Wa?]O?M��� ���;콂P*7�P�=m��8�j�^B��J���P��Uy��>8�
	D?��e �5�=[(n��+���$t���N>�+���q�>�ь-��<#�ϩ�{?2gN(��=pl�HCJ|��P�os6>)�y�P���eHLS$�>D��
��J=�.&�է��*ʻ���;ꓧS���>[��i�>G�&X�@<���b��?Ok\:�K�<o��k��4=���,ls?�j��_�V?gk�NI0?D����?  0yQ�?7(��?  ��sG=���,ls�&-�
��T�gk�NI0�
7'�U��   7�	��k�5򟒿  �~/�E�Q�GM�q?�j��_�V?�0c��/?D����?  0yQ�?7(��?  ��sG=8���Wa?>8�
	D?#�ϩ�{?eHLS$�>ꓧS���>���b��?o��k��4=�`���sJ>�j=�U�>���-��=��O瘅>��/�G�=� �L��>�D�A�=v����>�rd��%@��8܊@���� -@��@�ݿ6�&��i+@   BC1_DR_SIDE_C      CSRDRIFT;��ϭ_?q�[�Nճ�%ԕ�齰@u1���=6�/S!jh��4���+J���>8�
	D?
۰��e�=[(n��+������N>B#7=�r�>`��<`���?�ۂ'���= Q<�c{�{ #�,>#���0��eHLS$�>@���ڤJ=����e���&���܇;ј;���>fӔ;�>���t@<�.�r,��?�s}BK�<�r�a�4=�4�q?�j��_�V?�P���v0?D����?  @���?Tk9Ǣ�?  phI�G=�4�q�&-�
��T��P���v0�
7'�U��  �˼	�=���p���  ���E��Lb��lp?�j��_�V?�څ��0?D����?  @���?Tk9Ǣ�?  phI�G=;��ϭ_?>8�
	D?`���?eHLS$�>ј;���>�.�r,��?�r�a�4=":���sJ>]�f-XU�>�$ϰ��=���`r�>��/�G�=tS�'���>���A�=/�����>&����g#@QKg���@
e�O�[@�6K���c�E�0@   BC1_DR_SIDE      DRIF��� ?�ri�t�(��н�c�!�=�bg�6)���,��4��q�#e�>8�
	D?��?ͷ7�=[(n��+����@[�N>B#7=�r�>��"Y8�<�S���>?~�[k��=�\��%s��OᎸ>�8`n���eHLS$�><�;��I=����e��R���h�;��	+:��>��P����>FLE4�@<�.�r,��?p�'=�<�"؈M�4=��#�;?�j��_�V?��$'W�5?D����?  ��1�?Tk9Ǣ�?  ��/EH=�ޯ/�\7�&-�
��T�T�]J�u3�
7'�U��  pet��=���p���  p��D���#�;?�j��_�V?��$'W�5?D����?  ��1�?Tk9Ǣ�?  ��/EH=��� ?>8�
	D?�S���>?eHLS$�>��	+:��>�.�r,��?�"؈M�4=/!���sJ>��f-XU�>��Z���=	d�`r�>��/�G�=tS�'���>���A�=/�����>[L�Q��?o�dE
�?�s�	&@���^H�r~�4i1@   BC1_DP_DIP4   	   CSRCSBEND4�/��u�>��a�Y�=�w4�Oӽ60І�Qt=�"|�ս���z]�/!U �[�6��E���>����Rӽ��~o8`���]���ܴ=]�6l�=>-؅G{�;VƬ��P?9��y��=w��Ί=AO}��>��b$��;xl�o�>h�C� �{O���Ȱ�W��q[�%Bn����>��4�>��j�cD<O]�<��?�_�AS�<ܳ���t7=�`�BK@/?���O�?�����7?�lu��?  ��~?��g4���?  �3��J=�`�BK@/����O������3�bK�f�  ��U
�f��e��  `'F�`Ɛ��-?4���?�����7?�lu��?  ��~?��g4���?  �3��J=4�/��u�>6��E���>VƬ��P?xl�o�>%Bn����>O]�<��?ܳ���t7=�!�T��=�%�C���>36۶��=�O��TM�>�C|��=5��a���>_LM
��=�:�����>Y����?<�`�Y�����:�'@g����?K�6]1@   BC1_DR_20_C      CSRDRIFT�_�җ<�>�Y �r�=�,�Cٽ�/������w�];ԽJ�U�>[�t�lj
�6��E���>d���)Խ��~o8`���!X+۴=r��F�`=>#�g�Uy�;��
=[$?��a�a �=�	�.���=
tl��>؍7S��;xl�o�>tb�
� ���,5���>� ?o[���V�!��>�'#�5�>e��aD<-~�Qǧ�?Q����R�<ؚ�D�r7=�Zˣ�1?���O�?B���bM9?�lu��?  � �?�U��
�?  `�g�J=�Zˣ�1����O�����FE`4�bK�f�  �]�S
�m�\b5`��  ��$F�0��_60?4���?B���bM9?�lu��?  � �?�U��
�?  `�g�J=�_�җ<�>6��E���>��
=[$?xl�o�>��V�!��>-~�Qǧ�?ؚ�D�r7=�!�T��=;4�j��>��N�^��=�#y�DM�>�C|��=\��KT��>ƒ1��=��'�M��>ME0�׺�?��W�ҿT)�)@���͢��?K�6]1@	   BC1_WA_OU      WATCH�_�җ<�>�Y �r�=�,�Cٽ�/������w�];ԽJ�U�>[�t�lj
�6��E���>d���)Խ��~o8`���!X+۴=r��F�`=>#�g�Uy�;��
=[$?��a�a �=�	�.���=
tl��>؍7S��;xl�o�>tb�
� ���,5���>� ?o[���V�!��>�'#�5�>e��aD<-~�Qǧ�?Q����R�<ؚ�D�r7=�Zˣ�1?���O�?B���bM9?�lu��?  � �?�U��
�?  `�g�J=�Zˣ�1����O�����FE`4�bK�f�  �]�S
�m�\b5`��  ��$F�0��_60?4���?B���bM9?�lu��?  � �?�U��
�?  `�g�J=�_�җ<�>6��E���>��
=[$?xl�o�>��V�!��>-~�Qǧ�?ؚ�D�r7=�!�T��=;4�j��>��N�^��=�#y�DM�>�C|��=\��KT��>ƒ1��=��'�M��>ME0�׺�?��W�ҿT)�)@���͢��