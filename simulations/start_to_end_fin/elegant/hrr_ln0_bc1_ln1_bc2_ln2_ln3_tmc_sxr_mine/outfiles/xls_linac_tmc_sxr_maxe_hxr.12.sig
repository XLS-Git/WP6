SDDS1
!# little-endian
&description text="sigma matrix--input: xls_linac_tmc_sxr_maxe_hxr.track.ele  lattice: xls_linac_tmc_sxr_maxe.12.lte", contents="sigma matrix", &end
&parameter name=Step, description="Simulation step", type=long, &end
&parameter name=SVNVersion, description="SVN version number", type=string, fixed_value=unknown, &end
&column name=s, units=m, description=Distance, type=double,  &end
&column name=ElementName, description="Element name", format_string=%10s, type=string,  &end
&column name=ElementOccurence, description="Occurence of element", format_string=%6ld, type=long,  &end
&column name=ElementType, description="Element-type name", format_string=%10s, type=string,  &end
&column name=s1, symbol="$gs$r$b1$n", units=m, description="sqrt(<x*x>)", type=double,  &end
&column name=s12, symbol="$gs$r$b12$n", units=m, description="<x*xp'>", type=double,  &end
&column name=s13, symbol="$gs$r$b13$n", units="m$a2$n", description="<x*y>", type=double,  &end
&column name=s14, symbol="$gs$r$b14$n", units=m, description="<x*y'>", type=double,  &end
&column name=s15, symbol="$gs$r$b15$n", units="m$a2$n", description="<x*s>", type=double,  &end
&column name=s16, symbol="$gs$r$b16$n", units=m, description="<x*delta>", type=double,  &end
&column name=s17, symbol="$gs$r$b17$n", units="m*s", description="<x*t>", type=double,  &end
&column name=s2, symbol="$gs$r$b2$n", description="sqrt(<x'*x'>)", type=double,  &end
&column name=s23, symbol="$gs$r$b23$n", units=m, description="<x'*y>", type=double,  &end
&column name=s24, symbol="$gs$r$b24$n", description="<x'*y'>", type=double,  &end
&column name=s25, symbol="$gs$r$b25$n", units=m, description="<x'*s>", type=double,  &end
&column name=s26, symbol="$gs$r$b26$n", description="<x'*delta>", type=double,  &end
&column name=s27, symbol="$gs$r$b27$n", units=s, description="<x'*t>", type=double,  &end
&column name=s3, symbol="$gs$r$b3$n", units=m, description="sqrt(<y*y>)", type=double,  &end
&column name=s34, symbol="$gs$r$b34$n", units=m, description="<y*y'>", type=double,  &end
&column name=s35, symbol="$gs$r$b35$n", units="m$a2$n", description="<y*s>", type=double,  &end
&column name=s36, symbol="$gs$r$b36$n", units=m, description="<y*delta>", type=double,  &end
&column name=s37, symbol="$gs$r$b37$n", units="m*s", description="<y*t>", type=double,  &end
&column name=s4, symbol="$gs$r$b4$n", description="sqrt(<y'*y')>", type=double,  &end
&column name=s45, symbol="$gs$r$b45$n", units=m, description="<y'*s>", type=double,  &end
&column name=s46, symbol="$gs$r$b46$n", description="<y'*delta>", type=double,  &end
&column name=s47, symbol="$gs$r$b47$n", units=s, description="<y'*t>", type=double,  &end
&column name=s5, symbol="$gs$r$b5$n", units=m, description="sqrt(<s*s>)", type=double,  &end
&column name=s56, symbol="$gs$r$b56$n", units=m, description="<s*delta>", type=double,  &end
&column name=s57, symbol="$gs$r$b57$n", units="m*s", description="<s*t>", type=double,  &end
&column name=s6, symbol="$gs$r$b6$n", description="sqrt(<delta*delta>)", type=double,  &end
&column name=s67, symbol="$gs$r$b67$n", units=s, description="<delta*t>", type=double,  &end
&column name=s7, symbol="$gs$r$b7$n", description="sqrt(<t*t>)", type=double,  &end
&column name=ma1, symbol="max$sb$ex$sb$e", units=m, description="maximum absolute value of x", type=double,  &end
&column name=ma2, symbol="max$sb$ex'$sb$e", description="maximum absolute value of x'", type=double,  &end
&column name=ma3, symbol="max$sb$ey$sb$e", units=m, description="maximum absolute value of y", type=double,  &end
&column name=ma4, symbol="max$sb$ey'$sb$e", description="maximum absolute value of y'", type=double,  &end
&column name=ma5, symbol="max$sb$e$gD$rs$sb$e", units=m, description="maximum absolute deviation of s", type=double,  &end
&column name=ma6, symbol="max$sb$e$gd$r$sb$e", description="maximum absolute value of delta", type=double,  &end
&column name=ma7, symbol="max$sb$e$gD$rt$sb$e", units=s, description="maximum absolute deviation of t", type=double,  &end
&column name=minimum1, symbol="x$bmin$n", units=m, type=double,  &end
&column name=minimum2, symbol="x'$bmin$n", units=m, type=double,  &end
&column name=minimum3, symbol="y$bmin$n", units=m, type=double,  &end
&column name=minimum4, symbol="y'$bmin$n", units=m, type=double,  &end
&column name=minimum5, symbol="$gD$rs$bmin$n", units=m, type=double,  &end
&column name=minimum6, symbol="$gd$r$bmin$n", units=m, type=double,  &end
&column name=minimum7, symbol="t$bmin$n", units=s, type=double,  &end
&column name=maximum1, symbol="x$bmax$n", units=m, type=double,  &end
&column name=maximum2, symbol="x'$bmax$n", units=m, type=double,  &end
&column name=maximum3, symbol="y$bmax$n", units=m, type=double,  &end
&column name=maximum4, symbol="y'$bmax$n", units=m, type=double,  &end
&column name=maximum5, symbol="$gD$rs$bmax$n", units=m, type=double,  &end
&column name=maximum6, symbol="$gd$r$bmax$n", units=m, type=double,  &end
&column name=maximum7, symbol="t$bmax$n", units=s, type=double,  &end
&column name=Sx, symbol="$gs$r$bx$n", units=m, description=sqrt(<(x-<x>)^2>), type=double,  &end
&column name=Sxp, symbol="$gs$r$bx'$n", description=sqrt(<(x'-<x'>)^2>), type=double,  &end
&column name=Sy, symbol="$gs$r$by$n", units=m, description=sqrt(<(y-<y>)^2>), type=double,  &end
&column name=Syp, symbol="$gs$r$by'$n", description=sqrt(<(y'-<y'>)^2>), type=double,  &end
&column name=Ss, symbol="$gs$r$bs$n", units=m, description=sqrt(<(s-<s>)^2>), type=double,  &end
&column name=Sdelta, symbol="$gs$bd$n$r", description=sqrt(<(delta-<delta>)^2>), type=double,  &end
&column name=St, symbol="$gs$r$bt$n", units=s, description=sqrt(<(t-<t>)^2>), type=double,  &end
&column name=ex, symbol="$ge$r$bx$n", units=m, description="geometric horizontal emittance", type=double,  &end
&column name=enx, symbol="$ge$r$bx,n$n", units=m, description="normalized horizontal emittance", type=double,  &end
&column name=ecx, symbol="$ge$r$bx,c$n", units=m, description="geometric horizontal emittance less dispersive contributions", type=double,  &end
&column name=ecnx, symbol="$ge$r$bx,cn$n", units=m, description="normalized horizontal emittance less dispersive contributions", type=double,  &end
&column name=ey, symbol="$ge$r$by$n", units=m, description="geometric vertical emittance", type=double,  &end
&column name=eny, symbol="$ge$r$by,n$n", units=m, description="normalized vertical emittance", type=double,  &end
&column name=ecy, symbol="$ge$r$by,c$n", units=m, description="geometric vertical emittance less dispersive contributions", type=double,  &end
&column name=ecny, symbol="$ge$r$by,cn$n", units=m, description="normalized vertical emittance less dispersive contributions", type=double,  &end
&column name=betaxBeam, symbol="$gb$r$bx,beam$n", units=m, description="betax for the beam, excluding dispersive contributions", type=double,  &end
&column name=alphaxBeam, symbol="$ga$r$bx,beam$n", description="alphax for the beam, excluding dispersive contributions", type=double,  &end
&column name=betayBeam, symbol="$gb$r$by,beam$n", units=m, description="betay for the beam, excluding dispersive contributions", type=double,  &end
&column name=alphayBeam, symbol="$ga$r$by,beam$n", description="alphay for the beam, excluding dispersive contributions", type=double,  &end
&data mode=binary, &end
/                 _BEG_      MARK�E���>�i�_�=-�l,7Aƽ�c�Ά��[*{	����h{�>����ٚ����>&u5@s��!g}zWv���At����E%�q��=JW�޻پX����>4�35�;�=�.�\,`=�*��3��S6x;�;\����a�>�e
*��M���Mi=�B��Q��Ϸ_��> �{M2��5X��!Z�;Z�p2�B?k ٿ6��G�CY)=P0���?(;���	?׈���A4?EҖ����>   ;�>P��ؽ�?   �"�"=1��e>��M�B�t��ӧY�i1�EҖ�����   ;�P��ؽ��   �"�"�P0���?(;���	?׈���A4?'X4R��>   ����>��g��gt?   �W�"=�E���>ٚ����>پX����>\����a�>�Ϸ_��>Z�p2�B?�G�CY)=��z���=�h|#*a�>I��.fc�=�i'$�8�>N<!7�w�=��fm�9�>p?Y6�w�=~�b�9�>P�V���	@��9��ӿ�wmf%@���`-�ڿ           BNCH      CHARGE�E���>�i�_�=-�l,7Aƽ�c�Ά��[*{	����h{�>����ٚ����>&u5@s��!g}zWv���At����E%�q��=JW�޻پX����>4�35�;�=�.�\,`=�*��3��S6x;�;\����a�>�e
*��M���Mi=�B��Q��Ϸ_��> �{M2��5X��!Z�;Z�p2�B?k ٿ6��G�CY)=P0���?(;���	?׈���A4?EҖ����>   ;�>P��ؽ�?   �"�"=1��e>��M�B�t��ӧY�i1�EҖ�����   ;�P��ؽ��   �"�"�P0���?(;���	?׈���A4?'X4R��>   ����>��g��gt?   �W�"=�E���>ٚ����>پX����>\����a�>�Ϸ_��>Z�p2�B?�G�CY)=��z���=�h|#*a�>I��.fc�=�i'$�8�>N<!7�w�=��fm�9�>p?Y6�w�=~�b�9�>P�V���	@��9��ӿ�wmf%@���`-�ڿ윙����?	   HXR_DR_V1      DRIF�}���>�@&���=�N���ƽ����}
����
 �����\:�X>O�St�~�ٚ����>7���%�w��!g}zWv�PwP�t����E%�q��=���9�޻���Y��>!�o՛�=_�`=�f�R�⮽�$̜;\����a�>r$�h+��M���Mi=	F��Q�E���>Ə��/��bإ�#Z�;Z�p2�B?�C��6��GZ)=r�fA?(;���	?����F4?EҖ����>   �:�>P��ؽ�?   e%�"=���J���M�B�t��ƒ��h'1�EҖ�����   �:�P��ؽ��   e%�"�r�fA?(;���	?����F4?'X4R��>   l���>��g��gt?   M�"=�}���>ٚ����>���Y��>\����a�>E���>Z�p2�B?�GZ)=��z���=�h|#*a�>J��.fc�=�i'$�8�>N<!7�w�=��fm�9�>p?Y6�w�=~�b�9�>�s���
@C���	r׿io�f�%@��u�ܿ�䥛� �?   HXR_DR_QD_ED      DRIF�<�� �>v�m)��=�i�
�ƽf�p2���ЎF��%��;B,�<�>�0�ě��ٚ����>B�J4��x��!g}zWv��Y��t����E%�q��=�nZ<�޻܆v=(�>�A[xO�=����`=�w���ͮ���"�{��;\����a�>����+��M���Mi=�M5 ��Q������>�Ey0/���Y�($Z�;Z�p2�B?�7�6��D��Z)=Ҟlao?(;���	?�"��G4?EҖ����>   �:�>P��ؽ�?   &�"=�ex]:p�M�B�t���@�|+1�EҖ�����   �:�P��ؽ��   &�"�Ҟlao?(;���	?�"��G4?'X4R��>   X���>��g��gt?   .J�"=�<�� �>ٚ����>܆v=(�>\����a�>�����>Z�p2�B?�D��Z)=��z���=�h|#*a�>I��.fc�=�i'$�8�>N<!7�w�=��fm�9�>p?Y6�w�=~�b�9�>�"1�"@&B\:�ؿ9w��%@漢�tܿ�*\����?
   HXR_QD_V01      QUAD�:4��>�<_)нx+�r��ƽP��l�����H��5�����X�>�~�u��x�n]^,�>i��7.��=.��%U�=
�5��m���N@��ν1�q"�੻n�`�<�>��j�=�p��`=�i�n-ڮ�
���ɜ;����>�s2"!C=���w#��G�EC�(�;+9[���>����.����Le$Z�;Z�p2�B?$w?Q"�6��Z��Z)=����i?�	�>6
??��uS4?�w�;Z�?   �:�>P��ؽ�?   ;&�"=�1<�[��	�>6
�چ̌51�(�B�(�   �:�P��ؽ��   ;&�"�����i?�6&R??��uS4?�w�;Z�?   V���>��g��gt?   yI�"=�:4��>x�n]^,�>n�`�<�>����>+9[���>Z�p2�B?�Z��Z)=�Z6E��=Pe+b�`�>�Ӏ+c�=C]K-L8�>VH�*�w�=I�ĉ9�>!�>�w�=���ψ9�>��� R@EN9��?�a�P�%@a�%����*\����?   HXR_COR      KICKER�:4��>�<_)нx+�r��ƽP��l�����H��5�����X�>�~�u��x�n]^,�>i��7.��=.��%U�=
�5��m���N@��ν1�q"�੻n�`�<�>��j�=�p��`=�i�n-ڮ�
���ɜ;����>�s2"!C=���w#��G�EC�(�;+9[���>����.����Le$Z�;Z�p2�B?$w?Q"�6��Z��Z)=����i?�	�>6
??��uS4?�w�;Z�?   �:�>P��ؽ�?   ;&�"=�1<�[��	�>6
�چ̌51�(�B�(�   �:�P��ؽ��   ;&�"�����i?�6&R??��uS4?�w�;Z�?   V���>��g��gt?   yI�"=�:4��>x�n]^,�>n�`�<�>����>+9[���>Z�p2�B?�Z��Z)=�Z6E��=Pe+b�`�>�Ӏ+c�=C]K-L8�>VH�*�w�=I�ĉ9�>!�>�w�=���ψ9�>��� R@EN9��?�a�P�%@a�%����*\����?   HXR_BPM      MONI�:4��>�<_)нx+�r��ƽP��l�����H��5�����X�>�~�u��x�n]^,�>i��7.��=.��%U�=
�5��m���N@��ν1�q"�੻n�`�<�>��j�=�p��`=�i�n-ڮ�
���ɜ;����>�s2"!C=���w#��G�EC�(�;+9[���>����.����Le$Z�;Z�p2�B?$w?Q"�6��Z��Z)=����i?�	�>6
??��uS4?�w�;Z�?   �:�>P��ؽ�?   ;&�"=�1<�[��	�>6
�چ̌51�(�B�(�   �:�P��ؽ��   ;&�"�����i?�6&R??��uS4?�w�;Z�?   V���>��g��gt?   yI�"=�:4��>x�n]^,�>n�`�<�>����>+9[���>Z�p2�B?�Z��Z)=�Z6E��=Pe+b�`�>�Ӏ+c�=C]K-L8�>VH�*�w�=I�ĉ9�>!�>�w�=���ψ9�>��� R@EN9��?�a�P�%@a�%���Ap����?
   HXR_QD_V01      QUADA.���>7�с<佷����ƽ��\H���_��+��ݴ|~ڏ>��>���/窉�>�I%C�]�={H�Uj�=v���?�=�y�k�.����J��;%�1�q�>��B��7>�3�_�$`=<�J��	��ٷ����;�y4e�>��S��dT=�t��q����
�[[K�;FD��>R���,��4d�%Z�;Z�p2�B?��:$�6����Z)=v��$B?��8R�?�.�þu4?���p+?   ;�>P��ؽ�?   �&�"=���V>)���M�&Y��M�VGS1�����@�'�   ;�P��ؽ��   �&�"�v��$B?��8R�?�.�þu4?���p+?   H���>��g��gt?   �H�"=A.���>/窉�>%�1�q�>�y4e�>FD��>Z�p2�B?���Z)=��,G��=#n-V�`�>ͺ�g�b�=�	!�8�>�:�a[w�=s{�W9�>��2�Zw�=�2�4V9�>�֧o�
@�_�n��?}�ES�D&@/J�J,��Ȇ�Q��?   HXR_DR_QD_ED      DRIF.�s��>�<��{�Y���ƽ���e^��u���>Ե�bb�v�8>oY~{��/窉�>�3�9Mm�={H�Uj�=��&�?�=�y�k�.��X�=lK��;�oҲ��>�d���>�/`��`=������W�)�e�;�y4e�>1���dT=�t��q���m�3	}K�;���N��>bs�����Ԫx(Z�;Z�p2�B?w�'�6�aq�8\)=@�v_?��8R�?Ȓ�a,5?���p+?   �<�>P��ؽ�?   �(�"=�hFj?%���M�&Y�Q���1�����@�'�   �<�P��ؽ��   �(�"�@�v_?��8R�?Ȓ�a,5?���p+?   ԩ��>��g��gt?   �E�"=.�s��>/窉�>�oҲ��>�y4e�>���N��>Z�p2�B?aq�8\)=��,G��=#n-V�`�>ͺ�g�b�=�	!�8�>�:�a[w�=s{�W9�>��2�Zw�=�2�4V9�>�>i��J	@H.�R��?J���'@'a� c ��D<j�?	   HXR_DR_V2      DRIF3\�zW��>��p���սC��ĽB�7X���f��_���p�(���>~����/窉�>N+�a`:�={H�Uj�=� a @�=�y�k�.�����8Z��;���Jo?���>�Bۏ}�f=��ocԵ����(�Z�;�y4e�>]YN��fT=�t��q���H�"��L�;���(��>6�g���:/�OZ�;Z�p2�B?���L�6��f~Ik)=r �4�V?��8R�?�:��A=?���p+?   NO�>P��ؽ�?   A�"=� �����M�&Y�J%��8�����@�'�   NO�P��ؽ��   A�"�r �4�V?��8R�?�:��A=?���p+?   ����>��g��gt?   �!�"=3\�zW��>/窉�>���Jo?�y4e�>���(��>Z�p2�B?�f~Ik)=��,G��=!n-V�`�>ʺ�g�b�=�	!�8�>O:�a[w�=!{�W9�>m�2�Zw�=k2�4V9�>��\b���?5�x2�?���/�7@EZ�"C'�Lw~+8�?   HXR_DR_QD_ED      DRIF�op[��>{�<�_Խ�ͣ�I�ý,�@�����&H����z��q>��n�ɢ�/窉�>d�E�B��={H�Uj�=awZ� @�=�y�k�.����L�[��;ˋ��?��(w�>VR�^n8g=�iӟ�V��aV2IԤ;�y4e�>�����fT=�t��q�����_M�;Q�u*��>E:!�}��	.0SZ�;Z�p2�B?�87P�6�6��l)=
�JJt?��8R�?:�'ڀ�=?���p+?   �P�>P��ؽ�?   EC�"=ח%f���M�&Y������9�����@�'�   �P�P��ؽ��   EC�"�
�JJt?��8R�?:�'ڀ�=?���p+?   >���>��g��gt?   ��"=�op[��>/窉�>ˋ��?�y4e�>Q�u*��>Z�p2�B?6��l)=��,G��="n-V�`�>˺�g�b�=�	!�8�>�:�a[w�=�{�W9�>֫2�Zw�=�2�4V9�>����_�?�J�%�?m�c��8@�J��ͯ'��Y%�~�?
   HXR_QD_V02      QUADh�
����>w=(j_���k
��GXý���u����痔���(>t�>aV��ﻋܓ!���>�\-�g�=�A���%{=�����R�.���3�ݽ#6Hq~����eAÇ?M>T����=EhMNKg=�G���h��G�M�m�;�ۍ�0��>���"=��*��l��V��2`;�[�/*��>;y�_|��&�O�SZ�;Z�p2�B?��^iR�6��I��l)=`����G?\�\[t?�/��>?F���?   $Q�>P��ؽ�?   �C�"=��Â��\�\[t�E�����9�F����   $Q�P��ؽ��   �C�"�`����G?��%��?�/��>?%k뀽?   .���>��g��gt?   ��"=h�
����>�ܓ!���>�eAÇ?�ۍ�0��>�[�/*��>Z�p2�B?�I��l)=��L��=�h�`�>�#`�b�=a{b�7�>��Tw�=�v	P9�>yS݆Sw�=y��N9�>��kN(�?��� ��?�0$B 9@���9��Y%�~�?   HXR_COR      KICKERh�
����>w=(j_���k
��GXý���u����痔���(>t�>aV��ﻋܓ!���>�\-�g�=�A���%{=�����R�.���3�ݽ#6Hq~����eAÇ?M>T����=EhMNKg=�G���h��G�M�m�;�ۍ�0��>���"=��*��l��V��2`;�[�/*��>;y�_|��&�O�SZ�;Z�p2�B?��^iR�6��I��l)=`����G?\�\[t?�/��>?F���?   $Q�>P��ؽ�?   �C�"=��Â��\�\[t�E�����9�F����   $Q�P��ؽ��   �C�"�`����G?��%��?�/��>?%k뀽?   .���>��g��gt?   ��"=h�
����>�ܓ!���>�eAÇ?�ۍ�0��>�[�/*��>Z�p2�B?�I��l)=��L��=�h�`�>�#`�b�=a{b�7�>��Tw�=�v	P9�>yS݆Sw�=y��N9�>��kN(�?��� ��?�0$B 9@���9��Y%�~�?   HXR_BPM      MONIh�
����>w=(j_���k
��GXý���u����痔���(>t�>aV��ﻋܓ!���>�\-�g�=�A���%{=�����R�.���3�ݽ#6Hq~����eAÇ?M>T����=EhMNKg=�G���h��G�M�m�;�ۍ�0��>���"=��*��l��V��2`;�[�/*��>;y�_|��&�O�SZ�;Z�p2�B?��^iR�6��I��l)=`����G?\�\[t?�/��>?F���?   $Q�>P��ؽ�?   �C�"=��Â��\�\[t�E�����9�F����   $Q�P��ؽ��   �C�"�`����G?��%��?�/��>?%k뀽?   .���>��g��gt?   ��"=h�
����>�ܓ!���>�eAÇ?�ۍ�0��>�[�/*��>Z�p2�B?�I��l)=��L��=�h�`�>�#`�b�=a{b�7�>��Tw�=�v	P9�>yS݆Sw�=y��N9�>��kN(�?��� ��?�0$B 9@���9��46��?
   HXR_QD_V02      QUADw�����>�Q�9�=(Ƅ(0ý��Q�A�=������0W����>�I�?��m�{=��>3�p�ݪ=�δ�-���b���|��f�=��:�E�ٻ��T��?�fR�)r�����?g=R�B��\��H֐5?ۤ;}E����>9��O���Ln�=���{����aA*��>f��{��*� �SZ�;Z�p2�B?eS��U�6�-��l)=�Ț2?�@si`?}��;>?�s�̵3#?   8Q�>P��ؽ�?   �C�"=� d���tCs��El$��9��s�̵3#�   8Q�P��ؽ��   �C�"��Ț2?�@si`?}��;>?����$ ?   "���>��g��gt?   �"=w�����>m�{=��>��T��?}E����>�aA*��>Z�p2�B?-��l)=�F7���=��`_`�>�!��b�=댌��7�>�
��w�=p���9�>X1�t�w�=*Z6�9�>ak4cG�?�����Ŀ�h,֚�8@���Q@�P���?   HXR_DR_QD_ED      DRIF����D�>����m�=�5S*ӑ½��qд��=�5�F ���w8�>>fifT�(�m�{=��>^�.��Q�=�δ�-��.c�c���|��f�=�]e�F�ٻ�i��a�?sh*�2&��3�4�f=�3���C�n`|�;}E����>
�����O���Ln�=��{"�����8�*��>�2^u������UZ�;Z�p2�B?��l�^�6����m)=o��E?�@si`?	�6�l�=?�s�̵3#?   �Q�>P��ؽ�?   &E�"=;�^�*���tCs����4(9��s�̵3#�   �Q�P��ؽ��   &E�"�o��E?�@si`?	�6�l�=?����$ ?   Σ��>��g��gt?   �"=����D�>m�{=��>�i��a�?}E����>�8�*��>Z�p2�B?���m)=�F7���=��`_`�>�!��b�=댌��7�>�
��w�=9p���9�>r1�t�w�=EZ6�9�>i���Y�?bXAg�~ɿ� �j%8@�[���@j�2Ռ@	   HXR_DR_V3      DRIF/A�dnG�>g?�l��=�ک���8�g�`�=��"��1��K�TҦ>���5@���m�{=��>i������=�δ�-�� �I�x���|��f�=�U�)e�ٻ*��f��>*a3cF<���	�|[=�~GjDy��5��n��;}E����>̷9 ��O���Ln�=��YH���'Mغ9��>��.]���2B,��Z�;Z�p2�B?c�0�&�6��&�&)=��y�X!?�@si`?�$��J�2?�s�̵3#?   �`�>P��ؽ�?   �a�"=��� ��tCs�� ,'�/��s�̵3#�   �`�P��ؽ��   �a�"���y�X!?�@si`?�$��J�2?����$ ?   ����>��g��gt?   �֖"=/A�dnG�>m�{=��>*��f��>}E����>'Mغ9��>Z�p2�B?�&�&)=�F7���=��`_`�>�!��b�=댌��7�>�
��w�=p���9�>X1�t�w�=*Z6�9�>�_g�k�@��f����I���J$@_��@;��+T�@   HXR_DR_QD_ED      DRIF�����>>���m�=�(x1H���_3f\I�=��D�Ԑ����E]�>Ɂ�u����m�{=��>!x��聛=�δ�-��sj�y���|��f�=Ѝ	�f�ٻ�KO��k�>�d�W���hx�u�Z=��(T���a�l��;}E����>҈���O���Ln�=�)e\����o
g:��>S �����:��Z�;Z�p2�B?JϏ�/�6��G��)=���B��!?�@si`?\�q�{	2?�s�̵3#?   �a�>P��ؽ�?   �b�"=���� ��tCs���5�W>�.��s�̵3#�   �a�P��ؽ��   �b�"����B��!?�@si`?\�q�{	2?����$ ?   L���>��g��gt?   �Ӗ"=�����>m�{=��>�KO��k�>}E����>�o
g:��>Z�p2�B?�G��)=�F7���=��`_`�>�!��b�=쌌��7�>��
��w�=+p���9�>e1�t�w�=7Z6�9�>�zX�e�@ŵ7qo񿊣Nt�O#@��Zԛ@�c��@
   HXR_QD_V03      QUAD.��>H����=�*�ta����:��=z=��UwI�����*���>B3�X���e|��S��>�$3\�=��P������E���GKü=F�=|�"2��ǻG��IF�>3���ｂ��u�yZ=n�Ւs��[�3Uؿ�;�V�h�>]��)H���j�'�=i�)G���,Ju�:��>y�)������D�Z�;Z�p2�B?U�2�6��(�)=�
g���!?J$B�z�?�:��1?=��?   �a�>P��ؽ�?   :c�"=�J_� �X'�s�
��H;���.�=���   �a�P��ؽ��   :c�"��
g���!?J$B�z�?�:��1?X���k?   :���>��g��gt?   (Ӗ"=.��>e|��S��>G��IF�>�V�h�>,Ju�:��>Z�p2�B?�(�)=�����=�J4�`�>���b�=�6�(8�>$����w�=6뤏�9�>1����w�=��15�9�>���(�@̀"wG���Ys#@��#�	@�c��@   HXR_COR      KICKER.��>H����=�*�ta����:��=z=��UwI�����*���>B3�X���e|��S��>�$3\�=��P������E���GKü=F�=|�"2��ǻG��IF�>3���ｂ��u�yZ=n�Ւs��[�3Uؿ�;�V�h�>]��)H���j�'�=i�)G���,Ju�:��>y�)������D�Z�;Z�p2�B?U�2�6��(�)=�
g���!?J$B�z�?�:��1?=��?   �a�>P��ؽ�?   :c�"=�J_� �X'�s�
��H;���.�=���   �a�P��ؽ��   :c�"��
g���!?J$B�z�?�:��1?X���k?   :���>��g��gt?   (Ӗ"=.��>e|��S��>G��IF�>�V�h�>,Ju�:��>Z�p2�B?�(�)=�����=�J4�`�>���b�=�6�(8�>$����w�=6뤏�9�>1����w�=��15�9�>���(�@̀"wG���Ys#@��#�	@�c��@   HXR_BPM      MONI.��>H����=�*�ta����:��=z=��UwI�����*���>B3�X���e|��S��>�$3\�=��P������E���GKü=F�=|�"2��ǻG��IF�>3���ｂ��u�yZ=n�Ւs��[�3Uؿ�;�V�h�>]��)H���j�'�=i�)G���,Ju�:��>y�)������D�Z�;Z�p2�B?U�2�6��(�)=�
g���!?J$B�z�?�:��1?=��?   �a�>P��ؽ�?   :c�"=�J_� �X'�s�
��H;���.�=���   �a�P��ؽ��   :c�"��
g���!?J$B�z�?�:��1?X���k?   :���>��g��gt?   (Ӗ"=.��>e|��S��>G��IF�>�V�h�>,Ju�:��>Z�p2�B?�(�)=�����=�J4�`�>���b�=�6�(8�>$����w�=6뤏�9�>1����w�=��15�9�>���(�@̀"wG���Ys#@��#�	@��ڨ�,@
   HXR_QD_V03      QUAD�7��J	�>����_/�=�`H;=��lQ�v�9h�Q;8�������n���>D��������x�!�!�>��9$L��=%��F^���[]bڧ`={��@�ڽrg�k5�;b*���-�>�<��/�.�XZ=Z7RnP��j Xu��;zD��I�>�E�)�@����3Q��=��N3U�}��ᚏ:��>(KV����!2e,�Z�;Z�p2�B?=Zy�5�6����.�)=,���"?!
a�? <����1?�R���?   �a�>P��ؽ�?   xc�"=�\�!� �!
a��(��F�.��R����   �a�P��ؽ��   xc�"�,���"?v~Ʈ_? <����1?�ЮZ�1?   .���>��g��gt?   nҖ"=�7��J	�>�x�!�!�>b*���-�>zD��I�>�ᚏ:��>Z�p2�B?���.�)=t���Z��=���a�>����?c�=0�Oa8�>=�T�w�=�����9�>*r��w�=�d�9�>tyh�M�@�q��п���"@�������?t����@   HXR_DR_QD_ED      DRIF��|��>E����ƶ=u��Qa���͔�*[m�q�Z�I���{ф.��>���z����x�!�!�>�}@9��=%��F^���=���`={��@�ڽw$_5�;gn���>�/�N���`����Y=�7ڧ���C��>�;zD��I�>����@����3Q��=NH]Y�}�X���:��>Dʎ���A�D��Z�;Z�p2�B?�ۓC�6�?�i�)=���t?"?!
a�?�I-D�1?�R���?   b�>P��ؽ�?   Yd�"=޷��� �!
a��|�
TP.��R����   b�P��ؽ��   Yd�"����t?"?v~Ʈ_?�I-D�1?�ЮZ�1?   ���>��g��gt?   �ϖ"=��|��>�x�!�!�>gn���>zD��I�>X���:��>Z�p2�B??�i�)=u���Z��=���a�>����?c�=0�Oa8�>@�T�w�=�����9�>-r��w�=�d�9�>�����@8v;Bѿ�� ��"@�qp)��?V�ŧ�U@	   HXR_DR_V4      DRIFB�����>84*����=�T4o𤽞�y�٢��2I�<���]t���>�;��$����x�!�!�>Y4�@Sy�=%��F^����	��`={��@�ڽ�d�	6�;���z]�>~r��4�ĽNe�Xf{(=;��R�R=� ���e;zD��I�>:}��J�@����3Q��=�N�$�}�z��<��>DkM�������Z�;Z�p2�B?�U �6�w.B��)=�^�M�8.?!
a�?Z8è�!?�R���?   fm�>P��ؽ�?   ���"=�#ZQ]�*�!
a��Z8è�!��R����   fm�P��ؽ��   ���"��^�M�8.?v~Ʈ_?���g?�ЮZ�1?   Z���>��g��gt?   ^6�"=B�����>�x�!�!�>���z]�>zD��I�>z��<��>Z�p2�B?w.B��)=u���Z��=���a�>����?c�=0�Oa8�>A�T�w�=�����9�>.r��w�=�d�9�>��&�@3��)r�������@��h1�?�;,��@	   HXR_DR_CT      DRIF0����L�>����ϔ�=Û�њR����lw���D�/��o��	P>.�v����x�!�!�>�nk��=%��F^���}��`={��@�ڽ��)�6�;��D�]��>���Oý �6�Z�!={z/�g=�e9���_;zD��I�>�?��M�@����3Q��=P��$,�}��1J�<��>�W�����ƷC�Z�;Z�p2�B?�5�:�6�"�`�)=��kq٧.?!
a�?ǯ`X!?�R���?   �m�>P��ؽ�?   ^��"=<�?�qJ+�!
a��ǯ`X!��R����   �m�P��ؽ��   ^��"���kq٧.?v~Ʈ_?N.�W?�ЮZ�1?   ���>��g��gt?   �0�"=0����L�>�x�!�!�>��D�]��>zD��I�>�1J�<��>Z�p2�B?"�`�)=t���Z��=���a�>����?c�=0�Oa8�>@�T�w�=�����9�>-r��w�=�d�9�>�L.���@Ǉy�ݫ��ލ~�J�@�ss~�Z�?�;,��@   HXR_WA_M_OU      WATCH0����L�>����ϔ�=Û�њR����lw���D�/��o��	P>.�v����x�!�!�>�nk��=%��F^���}��`={��@�ڽ��)�6�;��D�]��>���Oý �6�Z�!={z/�g=�e9���_;zD��I�>�?��M�@����3Q��=P��$,�}��1J�<��>�W�����ƷC�Z�;Z�p2�B?�5�:�6�"�`�)=��kq٧.?!
a�?ǯ`X!?�R���?   �m�>P��ؽ�?   ^��"=<�?�qJ+�!
a��ǯ`X!��R����   �m�P��ؽ��   ^��"���kq٧.?v~Ʈ_?N.�W?�ЮZ�1?   ���>��g��gt?   �0�"=0����L�>�x�!�!�>��D�]��>zD��I�>�1J�<��>Z�p2�B?"�`�)=t���Z��=���a�>����?c�=0�Oa8�>@�T�w�=�����9�>-r��w�=�d�9�>�L.���@Ǉy�ݫ��ލ~�J�@�ss~�Z�?$��5�@   HXR_DR_QD_ED      DRIF�]R���>���l���=��E�l���ȍɣ�r�H\�(��V̴v�#>��c����x�!�!�>�Oyw���=%��F^���`:�!�`={��@�ڽ����!6�;$(�ͳJ�>�;>%�½�����=�yE�c�n=������Y;zD��I�>���O�@����3Q��="�?�/�}��'_�<��>)�R���Ɍ���Z�;Z�p2�B?^|pH�6��9G�)=�2��.?!
a�?bD)ǵ;!?�R���?   n�>P��ؽ�?   >��"=o\���z+�!
a��bD)ǵ;!��R����   n�P��ؽ��   >��"��2��.?v~Ʈ_?�RL��?�ЮZ�1?   ���>��g��gt?   �-�"=�]R���>�x�!�!�>$(�ͳJ�>zD��I�>�'_�<��>Z�p2�B?�9G�)=t���Z��=���a�>����?c�=0�Oa8�>A�T�w�=�����9�>.r��w�=�d�9�>����h@<@+*���}7t��@��AA��?~1Uj��@	   HXR_QD_FH      QUAD�#BV��>DS��]&���9�l�I��U�Y1�Z���d� �����;�>���������Wk�b�>���Q�=��Է_jP�D8���=I������/�fi�; h��B�>(�Ks�'�=��n�#=��Mjp=��oAX;�+�'M�>%�_eU�?���!�^�=��׀|�?���<��>�)����?���Z�;Z�p2�B?F�L�6�DO�P�)=<!���.?Du�-�>@��F::!? ���A�?   n�>P��ؽ�?   m��"=�r�[~+�Du�-��@��F::!� ���A��   n�P��ؽ��   m��"�<!���.?-R4&\��>4��
2�?7U;r[.?   ����>��g��gt?   3-�"=�#BV��>��Wk�b�> h��B�>�+�'M�>?���<��>Z�p2�B?DO�P�)=0�h��=^_�8a�>���Hc�=�3-�j8�>Sd3�Wx�=7.p�]:�>U(�Vx�=��<\:�>�d�w@6<[����?U�ڂ@�U36ܫ��~1Uj��@   HXR_COR      KICKER�#BV��>DS��]&���9�l�I��U�Y1�Z���d� �����;�>���������Wk�b�>���Q�=��Է_jP�D8���=I������/�fi�; h��B�>(�Ks�'�=��n�#=��Mjp=��oAX;�+�'M�>%�_eU�?���!�^�=��׀|�?���<��>�)����?���Z�;Z�p2�B?F�L�6�DO�P�)=<!���.?Du�-�>@��F::!? ���A�?   n�>P��ؽ�?   m��"=�r�[~+�Du�-��@��F::!� ���A��   n�P��ؽ��   m��"�<!���.?-R4&\��>4��
2�?7U;r[.?   ����>��g��gt?   3-�"=�#BV��>��Wk�b�> h��B�>�+�'M�>?���<��>Z�p2�B?DO�P�)=0�h��=^_�8a�>���Hc�=�3-�j8�>Sd3�Wx�=7.p�]:�>U(�Vx�=��<\:�>�d�w@6<[����?U�ڂ@�U36ܫ��~1Uj��@   HXR_BPM      MONI�#BV��>DS��]&���9�l�I��U�Y1�Z���d� �����;�>���������Wk�b�>���Q�=��Է_jP�D8���=I������/�fi�; h��B�>(�Ks�'�=��n�#=��Mjp=��oAX;�+�'M�>%�_eU�?���!�^�=��׀|�?���<��>�)����?���Z�;Z�p2�B?F�L�6�DO�P�)=<!���.?Du�-�>@��F::!? ���A�?   n�>P��ؽ�?   m��"=�r�[~+�Du�-��@��F::!� ���A��   n�P��ؽ��   m��"�<!���.?-R4&\��>4��
2�?7U;r[.?   ����>��g��gt?   3-�"=�#BV��>��Wk�b�> h��B�>�+�'M�>?���<��>Z�p2�B?DO�P�)=0�h��=^_�8a�>���Hc�=�3-�j8�>Sd3�Wx�=7.p�]:�>U(�Vx�=��<\:�>�d�w@6<[����?U�ڂ@�U36ܫ��ؕ�I�@	   HXR_QD_FH      QUAD�?����>*�ދA��rF�0b|�����&6ଽ�Ab���QU��h�>�îp��!' ��>���y��=��v���=RD�/�=�y�������jy�;�%Un�M�>	[h�F�=�y�̈́=�n�vcq=H��Ե�V;-�Σ�>E�Y��z>��$VK��=�Zz�Z{��P/�<��>y����N�ϩ�Z�;Z�p2�B?Uȋ�O�6�h��W�)=��P*�.?�+�5j?�k"��C!?�2�Ӫ�?   n�>P��ؽ�?   ���"=P^l�so+��+�5j��k"��C!�$�x��   n�P��ؽ��   ���"���P*�.?Js�GA:?[����?�2�Ӫ�?   ֓��>��g��gt?   v,�"=�?����>!' ��>�%Un�M�>-�Σ�>�P/�<��>Z�p2�B?h��W�)=@Kl�x��=��W!a�> B.oTc�=g߇�v8�>�*��x�=�K��:�>A�DG�x�=����:�>f�L]_@z$*=Ei�?��gI�@f���{�@'~�A@   HXR_DR_QD_ED      DRIFo�%49�>P�Tb�v�����@��˫/K7������&�{����r��X>m��b#���!' ��>~OeUw<�=��v���=��7�=�y��������jy�;?���0��>����L�=i1��-=|*�[u=�2c�Q;-�Σ�>�7g��z>��$VK��=�a�[{��ۨ�<��>o~�j������֗Z�;Z�p2�B?�n%)^�6���F��)=�$�ѭn.?�+�5j?q_X�k�!?�2�Ӫ�?   n�>P��ؽ�?   E��"=�g�+��+�5j�q_X�k�!�$�x��   n�P��ؽ��   E��"��$�ѭn.?Js�GA:?��Հ�S?�2�Ӫ�?   ����>��g��gt?   �)�"=o�%49�>!' ��>?���0��>-�Σ�>�ۨ�<��>Z�p2�B?��F��)=@Kl�x��=��W!a�> B.oTc�=g߇�v8�>�*��x�=�K��:�>A�DG�x�=����:�>XtT �@F�� �?��{R7	@B��-�R⿈���@   HXR_DR_WIG_ED      DRIF���'�>���Ɔn�fDPWU̪��?|�;+��)�
z��A�e�&>2UG�(��!' ��>*'��n�=��v���=�9W�=�y�����E��ly�;�$v����>a�)�"�=���I5b����=�I*H�0�-�Σ�>T���z>��$VK��="���[{�l��<��>x�����@�t��Z�;Z�p2�B?��_@��6� � �)=��Aכ�,?�+�5j?����g"?�2�Ӫ�?   �m�>P��ؽ�?   �"=���A;�)��+�5j�����g"�$�x��   �m�P��ؽ��   �"���Aכ�,?Js�GA:?̭�Jz ?�2�Ӫ�?   ̒��>��g��gt?   p�"=���'�>!' ��>�$v����>-�Σ�>l��<��>Z�p2�B? � �)==Kl�x��=��W!a�>B.oTc�=d߇�v8�>�*��x�=�K��:�>B�DG�x�=����:�>���i�:@~q0��x�?�3�(��@�� ��y�gX�r1@	   HXR_WIG_0      WIGGLERf@�D ��>��	EJ̽`��YÜ���x���򜽣Ib3����s�LKVҽA� c��λ!' ��>)`��=ms�NVi�=u,��ᖤ=�y������Ş��y�;u
���~�>S쫅l��=V�*j3K����jH�=ڙ ��l��y]���>b l=���S,��=3��{hz��Tc@H��>̑V�;!����r�Z�;Z�p2�B?�B���6�ɉ/ß)=�So ?�+�5j?2��0n�)?��'��?   ��>P��ؽ�?   `��"=�So ��+�5j�2��0n�)���'���   ��P��ؽ��   `��"�=�n�#?Js�GA:?p�v��'?�a�6?   ����>��g��gt?   �$�"=f@�D ��>!' ��>u
���~�>y]���>�Tc@H��>Z�p2�B?ɉ/ß)=?Kl�x��=��W!a�>B.oTc�=f߇�v8�>�*��x�=�K��:�>@�DG�x�=����:�>�J���u@�t�Q1�?XT`��@�)�g�T���9���@   HXR_DR_WIG_ED      DRIF3�4R^\�>�?�:)Ƚ�[!|\`?F���Lc�FV&��V���*�ϓ��0��!' ��>Ԟ��i9�=ms�NVi�=���ᖤ=�y�������Ϡ�y�;)�M�g�>ϗ����=Cd�W�N�i�&Ѧ=�+v1 ��y]���>&?,��l=���S,��=���hz���2H��>��U9!���:kW�Z�;Z�p2�B?K����6�K�sr�)=�>\�w?�+�5j?���o��*?��'��?   ���>P��ؽ�?   ���"=�>\�w��+�5j����o��*���'���   ���P��ؽ��   ���"���f5$?Js�GA:?ԓk�JY(?�a�6?   ���>��g��gt?   ��"=3�4R^\�>!' ��>)�M�g�>y]���>��2H��>Z�p2�B?K�sr�)=?Kl�x��=��W!a�>B.oTc�=f߇�v8�>�*��x�=�K��:�>B�DG�x�=����:�>�t?��@?YVu!�?.Ϋ9�!@�������@   HXR_DR_QD_ED	      DRIF��޶	�>Yd��Yǽ���A#������QKӤ �{�!"-�+�你������!' ��>@ጧ��=ms�NVi�=.��ᖤ=�y������,�y�;�a�gn��>��t0��==�j���N��9�6�I�=J[/�����y]���>��Lt�l=���S,��=B��V�hz�z�?/H��>J|�I8!��^����Z�;Z�p2�B?��Z&�6���-��)=��P�K?�+�5j?���*?��'��?   ���>P��ؽ�?   v��"=��P�K��+�5j����*���'���   ���P��ؽ��   v��"� ��Л?Js�GA:?��I�(?�a�6?   ����>��g��gt?   ��"=��޶	�>!' ��>�a�gn��>y]���>z�?/H��>Z�p2�B?��-��)=?Kl�x��=��W!a�>B.oTc�=f߇�v8�>�*��x�=�K��:�>A�DG�x�=����:�>d;,n(@��}P�?2�$���@��H'��q/*�n%@	   HXR_QD_DH      QUADLH��~��>8�u��!|�I^�D諒�L"�PG��V�I��y��lC7p0归bD}Ͷ�CUMp���>w��YF�=���(qb��$�a�=�����;<i��;@iև��>߷�H�=��mJ��N��7�1`�=J2S�ҋ�*���S�>P�V�8_1�Gq���9�=�*_Z,o�M�[.H��>�a"8!��d��Z�;Z�p2�B?�G�)�6���ެ�)=!�/;P�?
H��s�?�����*?z��;b?   ���>P��ؽ�?   ���"=!�/;P��
H��s�������*�z��;b�   ���P��ؽ��   ���"��R߀�?>1]D�T?���
�(?w��e#��>   ����>��g��gt?   @�"=LH��~��>CUMp���>@iև��>*���S�>M�[.H��>Z�p2�B?��ެ�)=����Ȋ�=�6cta�>�Ȥc�=i�-�8�>?p�\x�=��L&:�>Q"�x�=�d��:�>t�1��@T���T�?=D[�@ŏV@X���q/*�n%@   HXR_COR      KICKERLH��~��>8�u��!|�I^�D諒�L"�PG��V�I��y��lC7p0归bD}Ͷ�CUMp���>w��YF�=���(qb��$�a�=�����;<i��;@iև��>߷�H�=��mJ��N��7�1`�=J2S�ҋ�*���S�>P�V�8_1�Gq���9�=�*_Z,o�M�[.H��>�a"8!��d��Z�;Z�p2�B?�G�)�6���ެ�)=!�/;P�?
H��s�?�����*?z��;b?   ���>P��ؽ�?   ���"=!�/;P��
H��s�������*�z��;b�   ���P��ؽ��   ���"��R߀�?>1]D�T?���
�(?w��e#��>   ����>��g��gt?   @�"=LH��~��>CUMp���>@iև��>*���S�>M�[.H��>Z�p2�B?��ެ�)=����Ȋ�=�6cta�>�Ȥc�=i�-�8�>?p�\x�=��L&:�>Q"�x�=�d��:�>t�1��@T���T�?=D[�@ŏV@X���q/*�n%@   HXR_BPM      MONILH��~��>8�u��!|�I^�D諒�L"�PG��V�I��y��lC7p0归bD}Ͷ�CUMp���>w��YF�=���(qb��$�a�=�����;<i��;@iև��>߷�H�=��mJ��N��7�1`�=J2S�ҋ�*���S�>P�V�8_1�Gq���9�=�*_Z,o�M�[.H��>�a"8!��d��Z�;Z�p2�B?�G�)�6���ެ�)=!�/;P�?
H��s�?�����*?z��;b?   ���>P��ؽ�?   ���"=!�/;P��
H��s�������*�z��;b�   ���P��ؽ��   ���"��R߀�?>1]D�T?���
�(?w��e#��>   ����>��g��gt?   @�"=LH��~��>CUMp���>@iև��>*���S�>M�[.H��>Z�p2�B?��ެ�)=����Ȋ�=�6cta�>�Ȥc�=i�-�8�>?p�\x�=��L&:�>Q"�x�=�d��:�>t�1��@T���T�?=D[�@ŏV@X���˓e¾2@	   HXR_QD_DH      QUAD�3��>�����J�=	ë�^����yjA���[�ʧٕw��$<�u���U�9����aO.z��>��ȯ���=7�%������^�V�=�w9)3����:��\�;���H��>D=�u=�ڽ�[�O��ȡ�g�=��U��㋻�l[D�>�>9,�S4���m��=���z��R�؍\,H��>A
18!��X�
��Z�;Z�p2�B?tAa-�6��E���)=NgB��?�t��2?�����*?n���
?   ���>P��ؽ�?   ���"=NgB����t��2������*�'<��>	�   ���P��ؽ��   ���"�Ɗ^.�|?�-��l?N��ʋ(?n���
?   ����>��g��gt?   ��"=�3��>�aO.z��>���H��>�l[D�>�>؍\,H��>Z�p2�B?�E���)=-t����= �4��a�>bW:��c�=���|9�>�o�iw�=���Ue9�>��O�gw�=>�Td9�>��(@��S��߿�nO��@m�a���?3%S?�g@   HXR_DR_QD_ED
      DRIF�QU)�T�>t���[�=e��N���]$$��C�ۯ�o�t�D���载Z�]~���aO.z��>�*����=7�%������]'�V�=�w9)3���W2�\�;N��k�>���Pڽo�nv�2O��hř�h�=B�a�K���l[D�>�>��A�5���m��=��"���R���H��>��z7!���[��Z�;Z�p2�B?����;�6���Vޠ)=kd@�<?�t��2?�I��Ό*?n���
?   ���>P��ؽ�?   a��"=kd@�<��t��2��I��Ό*�'<��>	�   ���P��ؽ��   a��"���Q�?�-��l?L�{��B(?n���
?   ����>��g��gt?   ��"=�QU)�T�>�aO.z��>N��k�>�l[D�>�>��H��>Z�p2�B?��Vޠ)=-t����= �4��a�>bW:��c�=���|9�>�o�iw�=���Ue9�>��O�gw�==�Td9�>-*H�3~@��~D�࿃@k3�-@���at�?{�S�/@   HXR_DR_WIG_ED      DRIF^Z����>�B{�Z�=Cz�x�N��}a*n�e��v�I�Am=��Q`!��7>\y��;�aO.z��>�G�.�L�=7�%�����TRҺ�V�=�w9)3���=�\�;�;���>���X�ؽ àE\�O���;k�=S��_Fy���l[D�>�>\��9���m��=������R�����G��>�O5!��R\�Q�Z�;Z�p2�B?�A|r�6�:��o�)=Q��	 ?�t��2?)nt|M)?n���
?   d��>P��ؽ�?   ���"=Q��	 ��t��2�)nt|M)�'<��>	�   d��P��ؽ��   ���"��{"6�?�-��l?Ե��51'?n���
?   :���>��g��gt?   ��"=^Z����>�aO.z��>�;���>�l[D�>�>����G��>Z�p2�B?:��o�)=,t����=�4��a�>aW:��c�=���|9�>�o�iw�=���Ue9�>��O�gw�=>�Td9�>���3B@p�?=��㿔�@�%@�w�$7�?-��u!#@	   HXR_WIG_0      WIGGLERF�ݲƼ�>���ɵJ�=���E롽2i.�T�.Q��=>��o�F�Ju�.�;�aO.z��>�J��%��=+`��̌���֧�V�=�w9)3���@	]�;�����>�^��ɽ������Q�V��ڌ��=Ae��㨏�P\��=�>'�1�'���rbrZ�!�:�ǃC��}+�S��>'��c�$��#qUX�Z�;Z�p2�B?a�(��6�\*u�)=w��W%?�t��2?���BB�?)�w��?   P��>P��ؽ�?   b�"=w��W%��t��2��� sʊ��-��Ǜ
�   P��P��ؽ��   b�"�>�h�4%?�-��l?���BB�?)�w��?   B5��>��g��gt?   u�"=F�ݲƼ�>�aO.z��>�����>P\��=�>�}+�S��>Z�p2�B?\*u�)=,t����=�4��a�>aW:��c�=���|9�>�o�iw�=���Ue9�>��O�gw�=>�Td9�>"э�d2@���*���qj��k@O��9�?���#@   HXR_DR_WIG_ED      DRIF���>�Ն��=-Bj"����K��X����C�t�=�0���@v�vK��;�aO.z��>���q�=+`��̌�s�\�V�=�w9)3���RGK
]�;+Xg���>��Z=ƽ��?���Q�^w��K��=M61��叻P\��=�>��6� ���rbrZ���᧿�C�(`3iS��>��qu�$��є���Z�;Z�p2�B?~D����6�I��)=�I
\�L&?�t��2?��( ?)�w��?   .��>P��ؽ�?   ��"=�I
\�L&��t��2��[�T8��-��Ǜ
�   .��P��ؽ��   ��"�Y�m	&?�-��l?��( ?)�w��?   �4��>��g��gt?   ��"=���>�aO.z��>+Xg���>P\��=�>(`3iS��>Z�p2�B?I��)=-t����= �4��a�>bW:��c�=���|9�>�o�iw�=���Ue9�>��O�gw�==�Td9�>'+� �@�
֡-�����2,�~@Eh�G�?��a>��#@   HXR_DR_QD_ED      DRIF��b�N�>-�/�I��=(�P��T�����s����>��-f�=ME�y���:��S�;�aO.z��>R�	���=+`��̌�Uޙ�V�=�w9)3��%�F�
]�;`�'=I�>�^��HŽ
ș��Q�M�S7K��=��c����P\��=�>�d�����rbrZ��dp��C��K�\S��>%Ӵ��$���8��Z�;Z�p2�B?o�2R�6�#�г)=ҹ�b�&?�t��2?j�����?)�w��?   $��>P��ؽ�?   x�"=ҹ�b�&��t��2�^�`���-��Ǜ
�   $��P��ؽ��   x�"��
%W;&?�-��l?j�����?)�w��?   �4��>��g��gt?   � �"=��b�N�>�aO.z��>`�'=I�>P\��=�>�K�\S��>Z�p2�B?#�г)=*t����=�4��a�>_W:��c�=���|9�>�o�iw�=���Ue9�>��O�gw�=>�Td9�>m��]Z,@�&N�����UE��@C���~�?���-,�#@	   HXR_QD_FH      QUAD�M�&�>�4P�ݩ=��,Ո��-�m����瀀5��=��j�7.��9���j�;���n��>�>�!.�=Tpߋh�k��k��?��=p>��U꽋�mkf�;�DC#x?�>����[��e�t��Q��l�ڊ�=)�)�?���%C*7L�>W5��0�@�d�|=�ἴ��m�Q�H`S��>��#��$���|��Z�;Z�p2�B?A%���6��Z�߳)=����g�&?�4N� ?]�;_�?dER=R	?   $��>P��ؽ�?   ��"=����g�&��4N� �&�s;��dER=R	�   $��P��ؽ��   ��"���ve�A&?. �_�>]�;_�?4�@!4?   �4��>��g��gt?   ) �"=�M�&�>���n��>�DC#x?�>�%C*7L�>Q�H`S��>Z�p2�B?�Z�߳)=���T9��=_��.�`�>w���c�=E0��78�>3�0=w�=��з79�>���;w�=��f69�>�'?9KD@A��T¿`|�@�z�?v?���-,�#@	   HXR_WA_OU      WATCH�M�&�>�4P�ݩ=��,Ո��-�m����瀀5��=��j�7.��9���j�;���n��>�>�!.�=Tpߋh�k��k��?��=p>��U꽋�mkf�;�DC#x?�>����[��e�t��Q��l�ڊ�=)�)�?���%C*7L�>W5��0�@�d�|=�ἴ��m�Q�H`S��>��#��$���|��Z�;Z�p2�B?A%���6��Z�߳)=����g�&?�4N� ?]�;_�?dER=R	?   $��>P��ؽ�?   ��"=����g�&��4N� �&�s;��dER=R	�   $��P��ؽ��   ��"���ve�A&?. �_�>]�;_�?4�@!4?   �4��>��g��gt?   ) �"=�M�&�>���n��>�DC#x?�>�%C*7L�>Q�H`S��>Z�p2�B?�Z�߳)=���T9��=_��.�`�>w���c�=E0��78�>3�0=w�=��з79�>���;w�=��f69�>�'?9KD@A��T¿`|�@�z�?v?