SDDS1
!# little-endian
&description text="sigma matrix--input: xls_linac_tmc_sxr_maxe_ln2_bp.track.ele  lattice: xls_linac_tmc_sxr_maxe.8.lte", contents="sigma matrix", &end
&parameter name=Step, description="Simulation step", type=long, &end
&parameter name=SVNVersion, description="SVN version number", type=string, fixed_value=unknown, &end
&column name=s, units=m, description=Distance, type=double,  &end
&column name=ElementName, description="Element name", format_string=%10s, type=string,  &end
&column name=ElementOccurence, description="Occurence of element", format_string=%6ld, type=long,  &end
&column name=ElementType, description="Element-type name", format_string=%10s, type=string,  &end
&column name=s1, symbol="$gs$r$b1$n", units=m, description="sqrt(<x*x>)", type=double,  &end
&column name=s12, symbol="$gs$r$b12$n", units=m, description="<x*xp'>", type=double,  &end
&column name=s13, symbol="$gs$r$b13$n", units="m$a2$n", description="<x*y>", type=double,  &end
&column name=s14, symbol="$gs$r$b14$n", units=m, description="<x*y'>", type=double,  &end
&column name=s15, symbol="$gs$r$b15$n", units="m$a2$n", description="<x*s>", type=double,  &end
&column name=s16, symbol="$gs$r$b16$n", units=m, description="<x*delta>", type=double,  &end
&column name=s17, symbol="$gs$r$b17$n", units="m*s", description="<x*t>", type=double,  &end
&column name=s2, symbol="$gs$r$b2$n", description="sqrt(<x'*x'>)", type=double,  &end
&column name=s23, symbol="$gs$r$b23$n", units=m, description="<x'*y>", type=double,  &end
&column name=s24, symbol="$gs$r$b24$n", description="<x'*y'>", type=double,  &end
&column name=s25, symbol="$gs$r$b25$n", units=m, description="<x'*s>", type=double,  &end
&column name=s26, symbol="$gs$r$b26$n", description="<x'*delta>", type=double,  &end
&column name=s27, symbol="$gs$r$b27$n", units=s, description="<x'*t>", type=double,  &end
&column name=s3, symbol="$gs$r$b3$n", units=m, description="sqrt(<y*y>)", type=double,  &end
&column name=s34, symbol="$gs$r$b34$n", units=m, description="<y*y'>", type=double,  &end
&column name=s35, symbol="$gs$r$b35$n", units="m$a2$n", description="<y*s>", type=double,  &end
&column name=s36, symbol="$gs$r$b36$n", units=m, description="<y*delta>", type=double,  &end
&column name=s37, symbol="$gs$r$b37$n", units="m*s", description="<y*t>", type=double,  &end
&column name=s4, symbol="$gs$r$b4$n", description="sqrt(<y'*y')>", type=double,  &end
&column name=s45, symbol="$gs$r$b45$n", units=m, description="<y'*s>", type=double,  &end
&column name=s46, symbol="$gs$r$b46$n", description="<y'*delta>", type=double,  &end
&column name=s47, symbol="$gs$r$b47$n", units=s, description="<y'*t>", type=double,  &end
&column name=s5, symbol="$gs$r$b5$n", units=m, description="sqrt(<s*s>)", type=double,  &end
&column name=s56, symbol="$gs$r$b56$n", units=m, description="<s*delta>", type=double,  &end
&column name=s57, symbol="$gs$r$b57$n", units="m*s", description="<s*t>", type=double,  &end
&column name=s6, symbol="$gs$r$b6$n", description="sqrt(<delta*delta>)", type=double,  &end
&column name=s67, symbol="$gs$r$b67$n", units=s, description="<delta*t>", type=double,  &end
&column name=s7, symbol="$gs$r$b7$n", description="sqrt(<t*t>)", type=double,  &end
&column name=ma1, symbol="max$sb$ex$sb$e", units=m, description="maximum absolute value of x", type=double,  &end
&column name=ma2, symbol="max$sb$ex'$sb$e", description="maximum absolute value of x'", type=double,  &end
&column name=ma3, symbol="max$sb$ey$sb$e", units=m, description="maximum absolute value of y", type=double,  &end
&column name=ma4, symbol="max$sb$ey'$sb$e", description="maximum absolute value of y'", type=double,  &end
&column name=ma5, symbol="max$sb$e$gD$rs$sb$e", units=m, description="maximum absolute deviation of s", type=double,  &end
&column name=ma6, symbol="max$sb$e$gd$r$sb$e", description="maximum absolute value of delta", type=double,  &end
&column name=ma7, symbol="max$sb$e$gD$rt$sb$e", units=s, description="maximum absolute deviation of t", type=double,  &end
&column name=minimum1, symbol="x$bmin$n", units=m, type=double,  &end
&column name=minimum2, symbol="x'$bmin$n", units=m, type=double,  &end
&column name=minimum3, symbol="y$bmin$n", units=m, type=double,  &end
&column name=minimum4, symbol="y'$bmin$n", units=m, type=double,  &end
&column name=minimum5, symbol="$gD$rs$bmin$n", units=m, type=double,  &end
&column name=minimum6, symbol="$gd$r$bmin$n", units=m, type=double,  &end
&column name=minimum7, symbol="t$bmin$n", units=s, type=double,  &end
&column name=maximum1, symbol="x$bmax$n", units=m, type=double,  &end
&column name=maximum2, symbol="x'$bmax$n", units=m, type=double,  &end
&column name=maximum3, symbol="y$bmax$n", units=m, type=double,  &end
&column name=maximum4, symbol="y'$bmax$n", units=m, type=double,  &end
&column name=maximum5, symbol="$gD$rs$bmax$n", units=m, type=double,  &end
&column name=maximum6, symbol="$gd$r$bmax$n", units=m, type=double,  &end
&column name=maximum7, symbol="t$bmax$n", units=s, type=double,  &end
&column name=Sx, symbol="$gs$r$bx$n", units=m, description=sqrt(<(x-<x>)^2>), type=double,  &end
&column name=Sxp, symbol="$gs$r$bx'$n", description=sqrt(<(x'-<x'>)^2>), type=double,  &end
&column name=Sy, symbol="$gs$r$by$n", units=m, description=sqrt(<(y-<y>)^2>), type=double,  &end
&column name=Syp, symbol="$gs$r$by'$n", description=sqrt(<(y'-<y'>)^2>), type=double,  &end
&column name=Ss, symbol="$gs$r$bs$n", units=m, description=sqrt(<(s-<s>)^2>), type=double,  &end
&column name=Sdelta, symbol="$gs$bd$n$r", description=sqrt(<(delta-<delta>)^2>), type=double,  &end
&column name=St, symbol="$gs$r$bt$n", units=s, description=sqrt(<(t-<t>)^2>), type=double,  &end
&column name=ex, symbol="$ge$r$bx$n", units=m, description="geometric horizontal emittance", type=double,  &end
&column name=enx, symbol="$ge$r$bx,n$n", units=m, description="normalized horizontal emittance", type=double,  &end
&column name=ecx, symbol="$ge$r$bx,c$n", units=m, description="geometric horizontal emittance less dispersive contributions", type=double,  &end
&column name=ecnx, symbol="$ge$r$bx,cn$n", units=m, description="normalized horizontal emittance less dispersive contributions", type=double,  &end
&column name=ey, symbol="$ge$r$by$n", units=m, description="geometric vertical emittance", type=double,  &end
&column name=eny, symbol="$ge$r$by,n$n", units=m, description="normalized vertical emittance", type=double,  &end
&column name=ecy, symbol="$ge$r$by,c$n", units=m, description="geometric vertical emittance less dispersive contributions", type=double,  &end
&column name=ecny, symbol="$ge$r$by,cn$n", units=m, description="normalized vertical emittance less dispersive contributions", type=double,  &end
&column name=betaxBeam, symbol="$gb$r$bx,beam$n", units=m, description="betax for the beam, excluding dispersive contributions", type=double,  &end
&column name=alphaxBeam, symbol="$ga$r$bx,beam$n", description="alphax for the beam, excluding dispersive contributions", type=double,  &end
&column name=betayBeam, symbol="$gb$r$by,beam$n", units=m, description="betay for the beam, excluding dispersive contributions", type=double,  &end
&column name=alphayBeam, symbol="$ga$r$by,beam$n", description="alphay for the beam, excluding dispersive contributions", type=double,  &end
&data mode=binary, &end
/                 _BEG_      MARK��2s�R�>�|�:>��=g�:IF��=X�e��������Ć��稂vm�K���L�)��Q��S���>� ���Ҧ=_^}��n���!ª�B�$�D=��8E�c:� �%�2�>�,�>{佼�<�C%>�1&v��yƽ����nz��Rkn�y�>���/M�A=�<͊���=�c�k>;C���o�>��r�S>������;����Iak?�rN'�<Kd>
�P=���!?���$�?���B��+?s��(VL?   V��>����a	�?  ���#=���!�8
�����*�@2�\+� =��   3*�循(TF��}�  ���#��	�!S> ?���$�?���B��+?s��(VL?   V��>����a	�?  ���"=��2s�R�>Q��S���> �%�2�>�Rkn�y�>C���o�>����Iak?Kd>
�P=W�0=Q��=�zWhf��>�\Fh��=�ٮ*�׃>T��Pqq�=efB4ڤ�>�)j�eq�=�!KѤ�>	zi��r@Z�Cl��	�(q!'@3�����?           BNCH      CHARGE��2s�R�>�|�:>��=g�:IF��=X�e��������Ć��稂vm�K���L�)��Q��S���>� ���Ҧ=_^}��n���!ª�B�$�D=��8E�c:� �%�2�>�,�>{佼�<�C%>�1&v��yƽ����nz��Rkn�y�>���/M�A=�<͊���=�c�k>;C���o�>��r�S>������;����Iak?�rN'�<Kd>
�P=���!?���$�?���B��+?s��(VL?   V��>����a	�?  ���#=���!�8
�����*�@2�\+� =��   3*�循(TF��}�  ���#��	�!S> ?���$�?���B��+?s��(VL?   V��>����a	�?  ���"=��2s�R�>Q��S���> �%�2�>�Rkn�y�>C���o�>����Iak?Kd>
�P=W�0=Q��=�zWhf��>�\Fh��=�ٮ*�׃>T��Pqq�=efB4ڤ�>�)j�eq�=�!KѤ�>	zi��r@Z�Cl��	�(q!'@3�����?           LN2_IN_CNT_BP      CENTER��2s�R�>�|�:>��=g�:IF��=X�e��������Ć��稂vm�K���L�)��Q��S���>� ���Ҧ=_^}��n���!ª�B�$�D=��8E�c:� �%�2�>�,�>{���<�C%>�<&v��yƽ����nz��Rkn�y�>���/M�A=�<͊���=�c�k>;C���o�>��r�S>������;����Iak?�rN'�<Kd>
�P=���U��!?.|
c�?E}���+?���SL?   V��>����a	�?  ���#=���U��!��N-#��l�̕\+��0����   3*�循(TF��}�  ���#�����= ?.|
c�?E}���+?���SL?   V��>����a	�?  ���"=��2s�R�>Q��S���> �%�2�>�Rkn�y�>C���o�>����Iak?Kd>
�P=W�0=Q��=�zWhf��>�\Fh��=�ٮ*�׃>T��Pqq�=efB4ڤ�>�)j�eq�=�!KѤ�>	zi��r@Z�Cl��	�(q!'@3�����?��Y���@   LN2_DR_BP_V1      DRIF>��}�?��P@�=��#/�ܲ=R���毽�8�Y$�ҽ}�Bӟ�d����O�Q��S���>���[E�=_^}��n����Eª�B�$�D=�M��h5绑ܨx���>iBCKC��=�c�Y=�(�`��=�i�	c�;�Rkn�y�>L�:i�A=�<͊���=~���9;����o�>\��
.�S>k�y ���;����Iak?���+-
�<UG�L=��8��9?.|
c�?���S�*?���SL?   �H��>����a	�?  �,#=��8��9��N-#���ڙ�%��0����   �'�循(TF��}�  �,#��`��{Z6?.|
c�?���S�*?���SL?   �H��>����a	�?  ����"=>��}�?Q��S���>�ܨx���>�Rkn�y�>����o�>����Iak?UG�L=T�0=Q��=�zWhf��>�\Fh��=�ٮ*�׃>W��Pqq�=gfB4ڤ�>�)j�eq�=��!KѤ�>����M"1@�/�=�	�
���k	@����j��X����@   LN2_DR_QD_ED      DRIFh�#x��?Г���M�=��c�=A<R�����1u����ҽd��n�d���^�4o�Q��S���>�Oa!��=_^}��n����EFª�B�$�D=�GzlFZ5��1j �D�>g�y!��=�-��6Z=$O��\�=�h?�췖;�Rkn�y�>��F{l�A=�<͊���=�Z�c�9;�;�o�>�"X^.�S>r|���;����Iak?��z�!
�<~V�/�K=�\���=9?.|
c�?�����*?���SL?   \H��>����a	�?   ��#=�\���=9��N-#����:�&��0����   �'�循(TF��}�   ��#��q�d�6?.|
c�?�����*?���SL?   \H��>����a	�?  ����"=h�#x��?Q��S���>�1j �D�>�Rkn�y�>�;�o�>����Iak?~V�/�K=]�0=Q��=�zWhf��>�\Fh��=�ٮ*�׃>W��Pqq�=gfB4ڤ�>�)j�eq�=��!KѤ�>O�eh1@����	���\p;
@��4U�O�[�_@   LN2_QD_BP_V01      QUADl*W.?,�.I$�=DE%�/�=�e�U+氽�� �Eӽy�"���d��j��Ð���[/6��>�՗;�=mJzC:р�r`�?㰽R:8wB�&��HK�� t놅�>�ۋ���=���ő�Z=��-Q��=\ݳnl�;��[H3�>��0�?=;\,���=�F�'{;�,��o�>��Z�.�S>:�к�;����Iak?i�#�
�<��ؓ�K=!w���p9?f8�"�6?8O�`�C+?��y�ow?   .H��>����a	�?  �N�#=!w���p9�f8�"�6����@�X&�I����   }'�循(TF��}�  �N�#�|ߑ �6?��lΨG?8O�`�C+?��y�ow?   .H��>����a	�?   ���"=l*W.?��[/6��> t놅�>��[H3�>�,��o�>����Iak?��ؓ�K=30���=X��>�P�V��=O���Ӄ>e�)7s�=B�њ8��>Ta��+s�=���/��>j���S�1@U,��@�'g�\�
@�=#R�O�[�_@   LN2_COR      KICKERl*W.?,�.I$�=DE%�/�=�e�U+氽�� �Eӽy�"���d��j��Ð���[/6��>�՗;�=mJzC:р�r`�?㰽R:8wB�&��HK�� t놅�>�ۋ���=���ő�Z=��-Q��=\ݳnl�;��[H3�>��0�?=;\,���=�F�'{;�,��o�>��Z�.�S>:�к�;����Iak?i�#�
�<��ؓ�K=!w���p9?f8�"�6?8O�`�C+?��y�ow?   .H��>����a	�?  �N�#=!w���p9�f8�"�6����@�X&�I����   }'�循(TF��}�  �N�#�|ߑ �6?��lΨG?8O�`�C+?��y�ow?   .H��>����a	�?   ���"=l*W.?��[/6��> t놅�>��[H3�>�,��o�>����Iak?��ؓ�K=30���=X��>�P�V��=O���Ӄ>e�)7s�=B�њ8��>Ta��+s�=���/��>j���S�1@U,��@�'g�\�
@�=#R�O�[�_@   LN2_BPM      MONIl*W.?,�.I$�=DE%�/�=�e�U+氽�� �Eӽy�"���d��j��Ð���[/6��>�՗;�=mJzC:р�r`�?㰽R:8wB�&��HK�� t놅�>�ۋ���=���ő�Z=��-Q��=\ݳnl�;��[H3�>��0�?=;\,���=�F�'{;�,��o�>��Z�.�S>:�к�;����Iak?i�#�
�<��ؓ�K=!w���p9?f8�"�6?8O�`�C+?��y�ow?   .H��>����a	�?  �N�#=!w���p9�f8�"�6����@�X&�I����   }'�循(TF��}�  �N�#�|ߑ �6?��lΨG?8O�`�C+?��y�ow?   .H��>����a	�?   ���"=l*W.?��[/6��> t놅�>��[H3�>�,��o�>����Iak?��ؓ�K=30���=X��>�P�V��=O���Ӄ>e�)7s�=B�њ8��>Ta��+s�=���/��>j���S�1@U,��@�'g�\�
@�=#R��ᙱ@   LN2_QD_BP_V01      QUAD,�ǐg?�u��>�# ��ر=޼��kֱ��wMpIӽ��� e����%��]�]�&��>g�X��=D~媂���M֧Vm����(v�TF��}�|���C;R�e��>˘`�_(�=q�Y��Z=33��r��=8�$�B�;Ck�Q1��>���p$:=1�^
K�=����v;�z���o�>��f/�S>]�ξ��;����Iak?���
�<  b�K="�ꄯ9?�3UV�?F���+?-%��?   �G��>����a	�?   ��#="�ꄯ9��3UV������S�&�Bc��l�   8'�循(TF��}�   ��#�4�,0B�6?h8"t?F���+?-%��?   �G��>����a	�?   ��"=,�ǐg?]�]�&��>C;R�e��>Ck�Q1��>�z���o�>����Iak?  b�K=N��Ǵ�=+:�(~��>�d�
|��=ǮFEYσ>��	u�=�!>����>�O�\�t�=(�����>5
9@2@��d����in�@��݀�]�ӫx�@   LN2_DR_QD_ED      DRIF�m���?�Bȩ�L>�Go�z�=���y����&���ӽ��_m�Me���O�K��]�]�&��>$h���H�=D~媂����!Wm����(v�TF�J�ox��a' ���>��o���=*�Y��[=iDJ�F�=��+���;Ck�Q1��>j��s$:=1�^
K�=��j���v;c�V�o�>?Q�/�S>���ƫ��;����Iak?g 
�<�w��K=��v���9?�3UV�?�o}�+?-%��?   �G��>����a	�?   ��#=��v���9��3UV��\�$�&�Bc��l�   �&�循(TF��}�   ��#�;As��07?h8"t?�o}�+?-%��?   �G��>����a	�?  ���"=�m���?]�]�&��>a' ���>Ck�Q1��>c�V�o�>����Iak?�w��K=N��Ǵ�=+:�(~��>�d�
|��=ĮFEYσ>��	u�=�!>����>�O�\�t�=+�����>�e��[�2@�U#���o#�F)A@>ٖ��^�
ǡ@   LN2_DR_BP_V2      DRIF`% 4?��e��->y�"��{˽�_�����2P|�⽙�6�t����qB �]�]�&��>(�Z�h��D~媂��g�@�~m����(v�TF�s�v�񻀙��.�?Td��1��=�΀|i=���N���=A��å;Ck�Q1��>���փ%:=1�^
K�=���@�v;! 2��o�>��p�R�S>����;����Iak?�ef:@�<�E�n{G=nʱ��H?�3UV�?�̠	��8?-%��?   V.��>����a	�?   �#=nʱ��H��3UV���/(vV6�Bc��l�   ��循(TF��}�   �#���}�aF?h8"t?�̠	��8?-%��?   V.��>����a	�?   �#�"=`% 4?]�]�&��>����.�?Ck�Q1��>! 2��o�>����Iak?�E�n{G=a��Ǵ�=9:�(~��>�d�
|��=ӮFEYσ>��	u�=�!>����>�O�\�t�='�����>�����Q@��wv��#��{��@1@�~�PT�Ⰻ)L�@   LN2_DR_QD_ED      DRIFԕo�U?��4'H>����+̽7�J�н�����⽩t8�ݞt����zZ �]�]�&��>+u'L���D~媂��+�m����(v�TF�l ݳ��T�?��V��=�믐�:i= \҅F��=	J����;Ck�Q1��>d\��%:=1�^
K�=ޓݝ5�v;��e0�o�>�b�`S�S>b��zl��;����Iak?��ު4�<z&�nG=9�b9�H?�3UV�?[�?n�8?-%��?   .��>����a	�?   Q#=9�b9�H��3UV����kGl�6�Bc��l�   )�循(TF��}�   Q#�I%E��F?h8"t?[�?n�8?-%��?   .��>����a	�?   ��"=ԕo�U?]�]�&��>�T�?Ck�Q1��>��e0�o�>����Iak?z&�nG=���Ǵ�=�9�(~��>d�
|��=b�FEYσ>��	u�=�!>����>�O�\�t�=.�����>
6�K�R@�۵Uo$��E@���1@�*�B���A�@   LN2_QD_BP_V02      QUAD�J�V6?_O�ڔ)�l�W�̽O�v1|oν^=�d���'jB��t����B ��{�p?J�)�=J�}����=�Ȼfv�=ӹ;D�.a>�|C�D<�:��{?�{}��>���5�i=^%84N�=���j7=�;Iʑ}0��>���Pa=e7�")9�=�S�N�;]����o�>�T�T�S>=B�	]��;����Iak?��۲*�<�8�fbG=^j�i�H?�&���4?�e�ѣ
9?��ef��0?   4-��>����a	�?   �	#=^j�i�H���g*�2� �[�87��W#F9/�   ��循(TF��}�   �	#���s�cF?�&���4?�e�ѣ
9?��ef��0?   4-��>����a	�?   ��"=�J�V6?�{�p?�:��{?Iʑ}0��>]����o�>����Iak?�8�fbG=K����=�:����>.��N��=i�@��>����p7�=]�4x�>�E�e7�=n�s	x�>�h���Q@�)���T<@��^s�M2@ք��*��A�@   LN2_COR      KICKER�J�V6?_O�ڔ)�l�W�̽O�v1|oν^=�d���'jB��t����B ��{�p?J�)�=J�}����=�Ȼfv�=ӹ;D�.a>�|C�D<�:��{?�{}��>���5�i=^%84N�=���j7=�;Iʑ}0��>���Pa=e7�")9�=�S�N�;]����o�>�T�T�S>=B�	]��;����Iak?��۲*�<�8�fbG=^j�i�H?�&���4?�e�ѣ
9?��ef��0?   4-��>����a	�?   �	#=^j�i�H���g*�2� �[�87��W#F9/�   ��循(TF��}�   �	#���s�cF?�&���4?�e�ѣ
9?��ef��0?   4-��>����a	�?   ��"=�J�V6?�{�p?�:��{?Iʑ}0��>]����o�>����Iak?�8�fbG=K����=�:����>.��N��=i�@��>����p7�=]�4x�>�E�e7�=n�s	x�>�h���Q@�)���T<@��^s�M2@ք��*��A�@   LN2_BPM      MONI�J�V6?_O�ڔ)�l�W�̽O�v1|oν^=�d���'jB��t����B ��{�p?J�)�=J�}����=�Ȼfv�=ӹ;D�.a>�|C�D<�:��{?�{}��>���5�i=^%84N�=���j7=�;Iʑ}0��>���Pa=e7�")9�=�S�N�;]����o�>�T�T�S>=B�	]��;����Iak?��۲*�<�8�fbG=^j�i�H?�&���4?�e�ѣ
9?��ef��0?   4-��>����a	�?   �	#=^j�i�H���g*�2� �[�87��W#F9/�   ��循(TF��}�   �	#���s�cF?�&���4?�e�ѣ
9?��ef��0?   4-��>����a	�?   ��"=�J�V6?�{�p?�:��{?Iʑ}0��>]����o�>����Iak?�8�fbG=K����=�:����>.��N��=i�@��>����p7�=]�4x�>�E�e7�=n�s	x�>�h���Q@�)���T<@��^s�M2@ք��*�4i��7@   LN2_QD_BP_V02      QUAD)��ظ�?�2��<��}���iͽ��h�ֽFy��c>���6��s��O+���#4C`R?�A��%��=�md��=�	�{�=�]f���s>~�s_d<�/M�U?"�L�v#>��d��j="0�;�V�=E�Z��;~tE�Z?:�/�V�o=�dV� >x}�֊�;��� p�>(���^�S>>�$da��;����Iak?��=(�<��܊]G=���F�*H?Ml�W�G?Ǯ�m��9?��i�C�>?   �$��>����a	�?   �#=���F�*H�kAg"�dE���R�7�(���o<�   ��循(TF��}�   �#�D0����E?Ml�W�G?Ǯ�m��9?��i�C�>?   �$��>����a	�?   =��"=)��ظ�?#4C`R?�/M�U?~tE�Z?��� p�>����Iak?��܊]G=��"'��=hd�פ�>��v�=���|X��>c��R��=��C�N�>�U�3��=eT���N�>D؊(,gN@����o�M@��{�3@e�E�h8� �bͼJ@   LN2_DR_QD_ED      DRIFJ1��ű?��%ߒ;��}���ͽb��Z�ս���Sdy�ZE�
Ps�����-I�#4C`R?������=�md��=�S���=�]f���s>50 d<���~?��O��$>�����k=-�n��=s+y�C�;~tE�Z?I�.��o=�dV� >�J,pP��;�lB9p�>����p�S>N�-�w��;����Iak?��Pu,�<WC�^G="(���'G?Ml�W�G?��{	�G;?��i�C�>?   Q��>����a	�?   ��#="(���'G�kAg"�dE����p�9�(���o<�   3��循(TF��}�   ��#��p���D?Ml�W�G?��{	�G;?��i�C�>?   Q��>����a	�?   ���"=J1��ű?#4C`R?���~?~tE�Z?�lB9p�>����Iak?WC�^G=��"'��=hd�פ�>���v�=@��|X��>���R��=��C�N�>�V�3��=aU���N�>VN[1�K@n=�g�L@C���{6@R����9�p�K��@   LN2_DR_BP_V3      DRIF��/(%n?z#���5��$]"��ͽ:�$���н��4Q�۽��jpFn��s���#4C`R?Ǳ�����=�md��=����=�]f���s>���d<E����?���A�(>�#1��*q=�+�LZ=>/�_�ǭ;~tE�Z?�����o=�dV� >B�9����;;*/{_p�>�ֽ�ųS>��2��;����Iak?�?�Q@�<y��tcG=Л#mfB?Ml�W�G?Q��*��@?��i�C�>?   ����>����a	�?   �V#=Л#mfB�kAg"�dE��˹f��>�(���o<�   x��循(TF��}�   �V#�ڦS���@?Ml�W�G?Q��*��@?��i�C�>?   ����>����a	�?   8�"=��/(%n?#4C`R?E����?~tE�Z?;*/{_p�>����Iak?y��tcG=��"'��=cn�פ�>n��v�=.��|X��>��R��=��C�N�>cT�3��=hS���N�>?8Ig�A@���"��F@{���@@85(�?�\6�C@   LN2_DR_QD_ED      DRIFJ��Jp?|���4�����Vͽ/��ߕϽ�ƠNE*ڽEk��l���N��#4C`R?h����=�md��=�G�S��=�]f���s>H�Ȋd<E�Q�V ?e��j��)>�4x���q=T�����>�n���;~tE�Z?���f�o=�dV� >���N��;�x�pp�>?�̩׳S>H�-����;����Iak?��D�<d�7�dG=�C��cA?Ml�W�G?��� XA?��i�C�>?   ����>����a	�?   �:#=�C��cA�kAg"�dE�8qL!
�?�(���o<�   �~�循(TF��}�   �:#���?�Y??Ml�W�G?��� XA?��i�C�>?   ����>����a	�?   ��"=J��Jp?#4C`R?E�Q�V ?~tE�Z?�x�pp�>����Iak?d�7�dG=1�"'��=k�פ�>���v�=���|X��>9��R��=��C�N�>�Y�3��=[W���N�>c��-@k?@i���E@�(B��%B@���%x@�����l@   LN2_QD_BP_V03      QUAD\�&R�?�d���g(�
��]��̽V���z��:ojh�ؽ	�M,]Pk�y]�h��YÑ��V
?���E	&�=�y��=~&	ڱt�=���I�h>+�f��u<�hf�y�?�$:Ed
>���)�Hr=2'C��j>i�t���;�O��c��>�v�#�rX=������=�~��K6�;�%k�yp�>�̳��S>�$���;����Iak?ɱ��B�<A�h�_G=�>x� �@?f�E
5e=?u�`��A?P����'?   v���>����a	�?   �$#=�>x� �@���)�k:��{[8�c@��T�i�-&�   �q�循(TF��}�   �$#�<O���=?f�E
5e=?u�`��A?P����'?   v���>����a	�?   ���"=\�&R�?YÑ��V
?�hf�y�?�O��c��>�%k�yp�>����Iak?A�h�_G==f`��=��5
;�> ����=�4k��Q�>��Tm�=�2~k���>K+Im�=�������>P^K¿�=@���\+6:@��έ,�B@f=�^�z*�����l@   LN2_COR      KICKER\�&R�?�d���g(�
��]��̽V���z��:ojh�ؽ	�M,]Pk�y]�h��YÑ��V
?���E	&�=�y��=~&	ڱt�=���I�h>+�f��u<�hf�y�?�$:Ed
>���)�Hr=2'C��j>i�t���;�O��c��>�v�#�rX=������=�~��K6�;�%k�yp�>�̳��S>�$���;����Iak?ɱ��B�<A�h�_G=�>x� �@?f�E
5e=?u�`��A?P����'?   v���>����a	�?   �$#=�>x� �@���)�k:��{[8�c@��T�i�-&�   �q�循(TF��}�   �$#�<O���=?f�E
5e=?u�`��A?P����'?   v���>����a	�?   ���"=\�&R�?YÑ��V
?�hf�y�?�O��c��>�%k�yp�>����Iak?A�h�_G==f`��=��5
;�> ����=�4k��Q�>��Tm�=�2~k���>K+Im�=�������>P^K¿�=@���\+6:@��έ,�B@f=�^�z*�����l@   LN2_BPM      MONI\�&R�?�d���g(�
��]��̽V���z��:ojh�ؽ	�M,]Pk�y]�h��YÑ��V
?���E	&�=�y��=~&	ڱt�=���I�h>+�f��u<�hf�y�?�$:Ed
>���)�Hr=2'C��j>i�t���;�O��c��>�v�#�rX=������=�~��K6�;�%k�yp�>�̳��S>�$���;����Iak?ɱ��B�<A�h�_G=�>x� �@?f�E
5e=?u�`��A?P����'?   v���>����a	�?   �$#=�>x� �@���)�k:��{[8�c@��T�i�-&�   �q�循(TF��}�   �$#�<O���=?f�E
5e=?u�`��A?P����'?   v���>����a	�?   ���"=\�&R�?YÑ��V
?�hf�y�?�O��c��>�%k�yp�>����Iak?A�h�_G==f`��=��5
;�> ����=�4k��Q�>��Tm�=�2~k���>K+Im�=�������>P^K¿�=@���\+6:@��έ,�B@f=�^�z*���"M�@   LN2_QD_BP_V03      QUAD�hzdt�?8{ENm ��D_�E̽��1�x=�1���\ؽ��:�ܟj�kݭXL�j[N��=�>̕~t(�=�^��6����Ŵ�W�=
P�IT>8 `4^��;�HJ��?�6}��e�{
ǣ�Tr=�T\��v>&��@�̯;����]�>j�6�RN�^��L�1��I��әL�{p�>���:�S>�6U��;����Iak?ܸLO9�<�2s�TG=IE���5@?Hԑ�'?܁R��A?LiQ�8?   ����>����a	�?   �#=IE���5@��m�-=%��O��o@�LiQ�8�   �l�循(TF��}�   �#�K��j�:=?Hԑ�'?܁R��A?p���5�?   ����>����a	�?   ���"=�hzdt�?j[N��=�>�HJ��?����]�>әL�{p�>����Iak?�2s�TG=�T���=��M)��>_T$���=l�,t˄>�.u���=�+����>|5i���=�7Zx�>��G���<@��p�%@yASuB@��=���@�@�k��@   LN2_DR_QD_ED      DRIFl+נd|?����������ǝV˽2YQW��v=O���׽A87�2j��VW>���j[N��=�>�@�M�=�^��6���;��W�=
P�IT>!��rJ��;�W��ӿ?��f7��@�+r=�R��J>ޑ�?��;����]�>��M�RN�^��L��Ob��I����mB|p�>�;
�S>���޴�;����Iak?��.�<��GG=�\I91�??Hԑ�'?�&D;�A?LiQ�8?   ����>����a	�?   C	#=�\I91�?��m�-=%�;9��[L@�LiQ�8�   pk�循(TF��}�   C	#���3�<?Hԑ�'?�&D;�A?p���5�?   ����>����a	�?   ���"=l+נd|?j[N��=�>�W��ӿ?����]�>��mB|p�>����Iak?��GG=�T���=��M)��>_T$���=l�,t˄>/u���=�+����>�5i���=7Zx�>��?��;@9�Am��$@64�*B@���?�Q@=����g @   LN2_DR_BP_V4      DRIF�Iar�	?H~�4��q��EWǽ��+4�e=:���ǋսՐE�g��?d,��j[N��=�>5+:�N�=�^��6������W�=
P�IT>·iu���;�c�8?v�8x�Eʣ��*q=
�o��6>dioёǭ;����]�>l~[�#RN�^��L�~M�DI����u��p�>���S>Ȯ_�k��;����Iak?{c�B��<�xE�F=��LC�<?Hԑ�'?4�~�ְ@?LiQ�8?   ����>����a	�?   ֻ#=��LC�<��m�-=%��A�>�LiQ�8�   �b�循(TF��}�   ֻ#�������9?Hԑ�'?4�~�ְ@?p���5�?   ����>����a	�?   n�"=�Iar�	?j[N��=�>�c�8?����]�>��u��p�>����Iak?�xE�F=bT���=q�M)��>T$���=4�,t˄>�.u���=�+����>�5i���=�7Zx�>Y��m��6@�,hLg�"@Ȯ��g@@	߲�S�@�e�!� @   LN2_DR_BP_0      DRIF����Г?���ـ��KV:��ý#��It�@=1�/a6�ӽl���^�e�c�F��j[N��=�>�eq�ֿ�=�^��6��τZl�W�=
P�IT>�q�}r��;4쌈?�o�5<�;Ss��hp=�}��f>*�Ը�v�;����]�>�L6RN�^��Lཤ���I��u�݃p�>�����S>�W����;����Iak?�Az��<��쬸F=��Pf:?Hԑ�'?��_��??LiQ�8?   ����>����a	�?   P�#=��Pf:��m�-=%��	����=�LiQ�8�   a\�循(TF��}�   P�#������7?Hԑ�'?��_��??p���5�?   ����>����a	�?   ��"=����Г?j[N��=�>4쌈?����]�>u�݃p�>����Iak?��쬸F=�T���=��M)��>zT$���=��,t˄>/u���=�+����>�5i���=7Zx�>"�/u�*3@�xEj!@N�0NE>@@�4��@=����� @   LN2_DR_BP_BL      DRIF�/0��?i��f��Q��ý���,2�
*���@ӽ��=e�nSZS��j[N��=�>�4?�K��=�^��6��� k��W�=
P�IT>�[�?[��;����E�?����x���h8p=s��e�1>-O�"�;����]�>'u�:RN�^��L�7*���H���dw��p�>�_��S>"�8 ��;����Iak?���G��<���F�F=i�z!^�9?Hԑ�'?�e��,�??LiQ�8?   ]���>����a	�?   �r#=i�z!^�9��m�-=%�i���A=�LiQ�8�   �Z�循(TF��}�   �r#��7�P7?Hԑ�'?�e��,�??p���5�?   ]���>����a	�?   *�"=�/0��?j[N��=�>����E�?����]�>�dw��p�>����Iak?���F�F=�T���=��M)��>�T$���=��,t˄>/u���=�+����>�5i���=�7Zx�>�aR2@�˼��� @.�Ҁ�=@,O����@\6Ou[!@   LN2_DR_SCA_ED      DRIFͥ��+?hO
�9��
�H���j;�L�N�,��[}ҽ �D�6d�pH����j[N��=�>�;�La�=�^��6���5���W�=
P�IT>�|W�4��;ϥzjL?�������¦��M�o=d[�S� >6��8���;����]�>߸I-BRN�^��LཾQ- �H��a�{�p�>ߟs�S>��,Wܳ�;����Iak?�'O��<����F=���^�8?Hԑ�'?�i�~�>?LiQ�8?   <���>����a	�?   RZ#=���^�8��m�-=%���� 9�<�LiQ�8�   X�循(TF��}�   RZ#����1�m6?Hԑ�'?�i�~�>?p���5�?   <���>����a	�?   ���"=ͥ��+?j[N��=�>ϥzjL?����]�>a�{�p�>����Iak?����F=U���=��M)��>�T$���=��,t˄>/u���=�+����>�5i���=�7Zx�>ӆL���0@���� @]u�n�<@2�
��.@�.Q�#"@   LN2_SCA0      RFDF��N� ?4o��ܳ�w�\��`���O�G�{��S���1˽����u�]��[�7|��j[N��=�>�zm��=�^��6��Q]%��W�=
P�IT><h�<��;$Y���:?��+�c��pA�k=�{��\�=��v!��;����]�>	�e�rRN�^��Lཏ�/�7H����lɎp�>������S>*��f���;����Iak?�Ŝ��<��)U�E=Kiy�|2?Hԑ�'?4�D��;?LiQ�8?   ����>����a	�?   5�#=Kiy�|2��m�-=%�{QB��@9�LiQ�8�   �F�循(TF��}�   5�#� K8y�0?Hԑ�'?4�D��;?p���5�?   ����>����a	�?   ��"=��N� ?j[N��=�>$Y���:?����]�>��lɎp�>����Iak?��)U�E=�T���=��M)��>|T$���=��,t˄>/u���=�+����>�5i���=�7Zx�>�J���"@�%wʬ�@W捄�6@��֓xb@�.Q�#"@   LN2_SZW      WAKE��N� ?},9��w�\��`���wP�{�{�[)S1˽�>,h�]���7|�������=�>wۇ���=�tut7��s�W�=R���S>ŅB�F��;$Y���:?��� d��Xq�k=��	�U�=go"��;��T��]�>��A�7RN�x���GH����=H����|�Zp�>F�?��S>�������;����Zk?�.h�� �<�'U�E=Kiy�|2?�FzA�'?4�D��;? ���8?   �`��>�7z�?   5�#=Kiy�|2�G���=%�{QB��@9� ���8�   +��[�`�U�}�   5�#� K8y�0?�FzA�'?4�D��;?��k�E�?   �`��>�7z�?   ��"=��N� ?�����=�>$Y���:?��T��]�>��|�Zp�>����Zk?�'U�E=LnQ��=l�@���>��R���==H�s˄>`����=��ʴw�>��R���=ۓsn�>'�����"@������@QL���6@q꽢�b@�.Q�#"@   LN2_STW      TRWAKE��N� ?탦E��w�\��`��|�v�O�{�{�[)S1˽�>,h�]���7|��D?��=�>&�B���=�l�7��j5�W�=r�"N��S>��BvG��;$Y���:?]� d��Xq�k=��	�U�=go"��;XV��]�>�=�9RN���e�HH�b�tR?H����|�Zp�>F�?��S>�������;����Zk?�.h�� �<�'U�E=Kiy�|2?a&Q�'?4�D��;?���8?   �`��>�7z�?   5�#=Kiy�|2�����=%�{QB��@9����8�   +��[�`�U�}�   5�#� K8y�0?a&Q�'?4�D��;?/���E�?   �`��>�7z�?   ��"=��N� ?D?��=�>$Y���:?XV��]�>��|�Zp�>����Zk?�'U�E=2k�x��=������>.a����=H�,�s˄>�M ���=��δw�>�NS���=4i�sn�>�b���"@b�����@�����6@�����b@�e�!N"@   LN2_DR_SCA_ED      DRIF��f
`�>3K�Df�����~"��*/��7B���$i�ɽ��$�+\�����;�D?��=�>{!a`���=�l�7���[���W�=r�"N��S>E�l� ��;�%ޅQ�?�_�	��$6΀"k={�W���=�bGIv��;XV��]�>}f�(ARN���e�HHཆ���%H���Ϡ)\p�>���տ�S>�����;����Zk?�ܟ� �<'�f��E=dO�1?a&Q�'?EO+q:?���8?   �^��>�7z�?   ߥ#=dO�1�����=%�� �N�8����8�   x��[�`�U�}�   ߥ#��t�?N�/?a&Q�'?EO+q:?/���E�?   �^��>�7z�?   T��"=��f
`�>D?��=�>�%ޅQ�?XV��]�>�Ϡ)\p�>����Zk?'�f��E=Ek�x��=������>Aa����=W�,�s˄>{M ���=��δw�>�NS���=i�sn�>�j/9c!@��C5қ@�^z�5@B~	s��@=����g"@   LN2_DR_BP_BL      DRIF6�R4,T�>|
�������ޒWΰ��Fv���� S��Ƚ��<P-[�9ą>p�D?��=�>�t�/��=�l�7��NJ�,�W�=r�"N��S>����	��;;z�n�v?A_vG�� � ��'�j=dV�R?�=
�m9P4�;XV��]�>�B|�ERN���e�HHླྀH�H��TAT�\p�>y�����S>0O&{��;����Zk?74et~ �<UuV�E=|n��"�0?a&Q�'?\e
�:?���8?   \]��>�7z�?   A�#=|n��"�0�����=%��Q7c8����8�   ���[�`�U�}�   A�#����s�.?a&Q�'?\e
�:?/���E�?   \]��>�7z�?   ���"=6�R4,T�>D?��=�>;z�n�v?XV��]�>TAT�\p�>����Zk?UuV�E= k�x��=������>a����=;�,�s˄>�M ���=��δw�>�NS���=4i�sn�>=����@��:���@U=�?(5@�����@�e�!�"@   LN2_DR_BP_0      DRIFWO�'�>��(�Iȵ�B��?���.��ZPC��Ž�:�@3W�a��oB�D?��=�>��e���=�l�7��X��W�=r�"N��S>��n����;>G*G�Q?c��a7���q?kw=i=v��~��=��ٻ�;XV��]�>��V�WRN���e�HH�<e��G���,B`p�>q�=�ĮS>�D�%��;����Zk?'��I �<u��΄E=�/Up�-?a&Q�'?�C����8?���8?   @X��>�7z�?   �\#=�/Up�-�����=%�.�8�7����8�   ^��[�`�U�}�   �\#��H�
s*?a&Q�'?�C����8?/���E�?   @X��>�7z�?   �{�"=WO�'�>D?��=�>>G*G�Q?XV��]�>�,B`p�>����Zk?u��΄E=2k�x��=������>0a����=I�,�s˄>�M ���=��δw�>�NS���=-i�sn�>?���@�?$��@�\F$3@F��@p2���#@	   LN2_DR_BP      DRIF�������>��]���O�`�$��Y�?����m̻|���q�B~N��m�z���D?��=�>>Y���ܺ=�l�7���f���W�=r�"N��S>�P>����;I�Z��?$�������D�b5f=�?p��\�=2�C��B�;XV��]�>Ty�R|RN���e�HH�@D��[G��Z	�fp�>u�F;̮S>Bfp�x��;����Zk?jC�j���<���	E=��E�w�#?a&Q�'?jmn4K�5?���8?   N��>�7z�?   ��
#=��E�w�#�����=%�CR� ~4����8�   h ��[�`�U�}�   ��
#���P�"?a&Q�'?jmn4K�5?/���E�?   N��>�7z�?   i��"=�������>D?��=�>I�Z��?XV��]�>Z	�fp�>����Zk?���	E=2k�x��=������>0a����=I�,�s˄>�M ���=��δw�>�NS���=-i�sn�>����:f@5��wP	@�����.@����@=����g$@	   LN2_DR_BP      DRIF�� �:�>s��m�潛ϩ�i�=�J�rBz��ӣY�쟩�Y�h,5<��x�=�/�D?��=�>���AM��=�l�7��W�vz�W�=r�"N��S>��C9��;��}�?�t������KF-c=(���-�=���a���;XV��]�>�6J��RN���e�HH�Dj��F��6-X^mp�>x1O�ӮS>pd�̰�;����Zk?*�,w��<����D=�762o?a&Q�'?���c�E3?���8?   �C��>�7z�?   �r
#=�x4:�x�����=%�X���e�1����8�   q���[�`�U�}�   �r
#��762o?a&Q�'?���c�E3?/���E�?   �C��>�7z�?   O/�"=�� �:�>D?��=�>��}�?XV��]�>6-X^mp�>����Zk?����D=9k�x��=������>6a����=N�,�s˄>�M ���=��δw�>�NS���=%i�sn�>�Xv�^�?xA!NMi�?�R�O@(@0X�Α�@p2���$@   LN2_DR_BP_VS      DRIF��ü.��>��=�A�9σ�{ӡ=j��ǜ���%�i#�I����'�754������߻D?��=�>ԗ��6y�=�l�7���.�h�W�=r�"N��S>�@K�
��;� ���2?U���B��
�>kb=�ז��J�=��(���;XV��]�>�^
ةRN���e�HH��[}��F���c�op�>ّ�ծS>c�m����;����Zk?�Q@�\��<��oD=�[�֓�?a&Q�'?�H/��2?���8?   ?A��>�7z�?   �U
#=���%�M�����=%���J7?1����8�   3���[�`�U�}�   �U
#��[�֓�?a&Q�'?�H/��2?/���E�?   ?A��>�7z�?   ��"=��ü.��>D?��=�>� ���2?XV��]�>�c�op�>����Zk?��oD=<k�x��=������>9a����=P�,�s˄>�M ���=��δw�>�NS���=-i�sn�>����P2�?���e�7�?���E��&@��fF�@�e�!�$@   LN2_DR_BP_DR      DRIF�	��Ar�>��/��;׽K,�c�ե=������;�ح�敽��͓�j(��4����һD?��=�>�=x 2�=�l�7��u��V�W�=r�"N��S>��ze���;����� ?ī\�5h����v5�a=')8�Yz�=�ֹf��;XV��]�>����RN���e�HH���m��F��ė�pp�>��Ԭ׮S>�!}�v��;����Zk?ŵTaB��<�3\)QD=`J{?a&Q�'?^1���1?���8?   �>��>�7z�?   s8
#=t��"�����=%��R��0����8�   ����[�`�U�}�   s8
#�`J{?a&Q�'?^1���1?/���E�?   �>��>�7z�?   C��"=�	��Ar�>D?��=�>����� ?XV��]�>ė�pp�>����Zk?�3\)QD=;k�x��=������>8a����=P�,�s˄>�M ���=��δw�>�NS���=;i�sn�>�nÈ�~�?�$�F�?�e�X�D%@��r�@�e�!N%@   LN2_DP_SEPM      DRIF<�2�x�>WKi�L޳=��Q�_�=Ɯt��y����˧Z��=���z*>�H�jR��;D?��=�><�|�g��=�l�7����O��W�=r�"N��S>��Ph��;�x��`w�>ݧ�5F����N�;�_=/uˎPq�=�eRY�;XV��]�>�A���RN���e�HHལ�SrTF��&ľtp�>&{qܮS>7�
��;����Zk?_^� ��<�vP@D=V�G-��
?a&Q�'?�m{x[Z0?���8?   M8��>�7z�?   ^�	#=��=~�N
�����=%���(�-����8�   ����[�`�U�}�   ^�	#�V�G-��
?a&Q�'?�m{x[Z0?/���E�?   M8��>�7z�?   rt�"=<�2�x�>D?��=�>�x��`w�>XV��]�>&ľtp�>����Zk?�vP@D=:k�x��=������>6a����=N�,�s˄>�M ���=��δw�>�NS���=4i�sn�>�uQ����?L�9��ÿ7�kn��!@�ZH�)
@p2���&@	   LN2_DR_BP      DRIF���̟�>i@5�=p.�|���=�:�CB뜽'�q_�^�=bٟ�7�C>%:�����;D?��=�>�u�Md�=�l�7���c�W�=r�"N��S>'�U����;O��&~�>��έ���a�[�wY=�v��P_�=������;XV��]�>t�9�RN���e�HHཐpo�E��lղM{p�>*���S>�ε^��;����Zk?�W,���<�z2�C=Y=��~?a&Q�'?�5!OՃ+?���8?   .��>�7z�?   nz	#=��U-U������=%�Hp�]��(����8�   ����[�`�U�}�   nz	#�Y=��~?a&Q�'?�5!OՃ+?/���E�?   .��>�7z�?   Y��"=���̟�>D?��=�>O��&~�>XV��]�>lղM{p�>����Zk?�z2�C=9k�x��=������>6a����=N�,�s˄>�M ���=��δw�>�NS���=4i�sn�>�� a��?.8>h:���.�@�@���@=�����&@	   LN2_DR_BP      DRIF�9����>��L2��=8.jٱ��=��O�U�����'����=x�ߏ�Q>����ȝ�;D?��=�>,���G�=�l�7���H��W�=r�"N��S> �.����;���x��>�6=� �콐�Xo~gS=��� ��=�'a֐;XV��]�>���SN���e�HH�l\E��G �܁p�>-b���S>�=�����;����Zk?>���-��<�|)$C=s}9�=�#?a&Q�'?��K��R&?���8?   �#��>�7z�?   ~	#=�'��n}!�����=%�H����$����8�   ����[�`�U�}�   ~	#�s}9�=�#?a&Q�'?��K��R&?/���E�?   �#��>�7z�?   ?(�"=�9����>D?��=�>���x��>XV��]�>G �܁p�>����Zk?�|)$C=Ek�x��=������>Ba����=W�,�s˄>�M ���=��δw�>�NS���=1i�sn�>�[�`��@}���	��<��@�&����@
�i`��'@	   LN2_DR_BP      DRIF������>�[��l�>�m�߮��=���t
碽�=ɨ���=?�����Y>�Fe�̨<D?��=�>��,�X+�=�l�7������W�=r�"N��S>�7�;��;���U���>?*���潸`���J=" my`��=3�:f*�;XV��]�>�z-�6SN���e�HH�sCh�D��h�l�p�>0�T�S>/��O��;����Zk?���t���<X��B=z�Hcq-?a&Q�'?�r�� "?���8?   ���>�7z�?   ��#=�boF��)�����=%��׬ԝ ����8�   ����[�`�U�}�   ��#�z�Hcq-?a&Q�'?�r�� "?/���E�?   ���>�7z�?   %��"=������>D?��=�>���U���>XV��]�>h�l�p�>����Zk?X��B=2k�x��=������>0a����=I�,�s˄>�M ���=��δw�>�NS���=4i�sn�>(/��5�@�}��*1�� hn�@��
�$�?ט6-U�(@	   LN2_DR_BP      DRIF{��ߨ?Ya�?.>�����=��e���K{��S2�=y	<�`>����j<D?��=�>?gO��=�l�7���M��W�=r�"N��S>�B�c���;�u*��s�>�;�|.޽�d��==�r�U�=�+,��Py;XV��]�>W8�][SN���e�HHཐx�ddD����n��p�>3�����S>S�[Z��;����Zk?�(D[��<�\=	B=����M3?a&Q�'?��T�0?���8?   h��>�7z�?   �#=��9L
1�����=%�(�+������8�    ���[�`�U�}�   �#�����M3?a&Q�'?��T�0?/���E�?   h��>�7z�?   ��"={��ߨ?D?��=�>�u*��s�>XV��]�>��n��p�>����Zk?�\=	B= k�x��=������>a����=;�,�s˄>�M ���=��δw�>�NS���=4i�sn�>���S�S%@N���b�Nm��X��?�W�&��?�e�!N)@	   LN2_DR_BP      DRIF��c���?g�G�>L��!��=�Y��sX���6REUD�=���6�d>���!߭<D?��=�>�ס���=�l�7��mF�W�=r�"N��S>\������;I���:�>�":��н�.�?T_=fN�郄=B���6Q;XV��]�>�� �SN���e�HH�JŜb�C��j����p�>6o���S>g�$뭬�;����Zk?Mg�����<�G=��A=�ųkl8?a&Q�'?�eF���?���8?   .��>�7z�?   ��#=�x��I5�����=%��eF�������8�   
���[�`�U�}�   ��#��ųkl8?a&Q�'?�ǝ�}_?/���E�?   .��>�7z�?   �5�"=��c���?D?��=�>I���:�>XV��]�>j����p�>����Zk?�G=��A= k�x��=������>a����=<�,�s˄>�M ���=��δw�>�NS���=4i�sn�>���3\0@m-�ԓ�ʧP�?$d�U��?ט6-U�)@   LN2_DR_BP_VS      DRIFT��ԉ�?��;>�����=zS�槽Vʟ^ /�=!��4�e>���y<D?��=�>�}pe���=�l�7����<4�W�=r�"N��S>�+�9���;\�_̧�>�8tU�ʽ�A\�Z��8�F줽A��U�_.�XV��]�>���SN���e�HHཱུЇa�C���W�0�p�>��~�S>R�ނ��;����Zk?஧f���<3
9~A=���R=9?a&Q�'?X5�r��?���8?   ���>�7z�?   u�#=���`�Y6�����=%�X5�r������8�   ̠��[�`�U�}�   u�#����R=9?a&Q�'?Д;�?/���E�?   ���>�7z�?   k�"=T��ԉ�?D?��=�>\�_̧�>XV��]�>�W�0�p�>����Zk?3
9~A= k�x��=������>a����=:�,�s˄>�M ���=��δw�>�NS���=4i�sn�>���bM�1@���� ���R����?,JL���?�e�!N*@	   LN2_DR_BP      DRIF
ou��%?$��m�>�X��$�=��ݛU��v�M��=�2�.�i>w-\+�<D?��=�>(�3��=�l�7��&5���W�=r�"N��S><X�~���;�����>�\�ZW�k=8���T{9�曜�RHϽ��t�/�u�XV��]�>f�ZM�SN���e�HHལBC^MC����c��p�>����S>na̬֫�;����Zk?{��
n��<�Xl,A=
^����=?a&Q�'?7s$E�V?���8?   g���>�7z�?   �#=�����:�����=%�7s$E�V����8�   Փ��[�`�U�}�   �#�
^����=?a&Q�'?�N錮$?/���E�?   g���>�7z�?   Qf�"=
ou��%?D?��=�>�����>XV��]�>��c��p�>����Zk?�Xl,A=Ek�x��=������>Ia����=]�,�s˄>�M ���=��δw�>�NS���=4i�sn�>X��D�89@>a�n��#����̃g�?m�f����e�!N*@   LN2_BP_WA_OU_      WATCH
ou��%?$��m�>�X��$�=��ݛU��v�M��=�2�.�i>w-\+�<D?��=�>(�3��=�l�7��&5���W�=r�"N��S><X�~���;�����>�\�ZW�k=8���T{9�曜�RHϽ��t�/�u�XV��]�>f�ZM�SN���e�HHལBC^MC����c��p�>����S>na̬֫�;����Zk?{��
n��<�Xl,A=
^����=?a&Q�'?7s$E�V?���8?   g���>�7z�?   �#=�����:�����=%�7s$E�V����8�   Փ��[�`�U�}�   �#�
^����=?a&Q�'?�N錮$?/���E�?   g���>�7z�?   Qf�"=
ou��%?D?��=�>�����>XV��]�>��c��p�>����Zk?�Xl,A=Ek�x��=������>Ia����=]�,�s˄>�M ���=��δw�>�NS���=4i�sn�>X��D�89@>a�n��#����̃g�?m�f���