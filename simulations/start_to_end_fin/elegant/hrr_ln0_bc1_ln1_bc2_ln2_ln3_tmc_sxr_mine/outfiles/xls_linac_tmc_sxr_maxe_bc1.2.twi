SDDS1
!# little-endian
&description text="Twiss parameters--input: xls_linac_tmc_sxr_maxe_bc1.track.ele  lattice: xls_linac_tmc_sxr_maxe.2.lte", contents="Twiss parameters", &end
&parameter name=Step, description="Simulation step", type=long, &end
&parameter name=SVNVersion, description="SVN version number", type=string, fixed_value=unknown, &end
&parameter name=nux, symbol="$gn$r$bx$n", units="1/(2$gp$r)", description="Horizontal tune", type=double, &end
&parameter name=dnux/dp, symbol="$gx$r$bx$n", units="1/(2$gp$r)", description="Horizontal chromaticity", type=double, &end
&parameter name=dnux/dp2, symbol="$gx$r$bx2$n", units="1/(2$gp$r)", description="Horizontal 2nd-order chromaticity", type=double, &end
&parameter name=dnux/dp3, symbol="$gx$r$bx3$n", units="1/(2$gp$r)", description="Horizontal 3rd-order chromaticity", type=double, &end
&parameter name=Ax, symbol="A$bx$n", units=m, description="Horizontal acceptance", type=double, &end
&parameter name=AxLocation, units=m, description="Location of horizontal acceptance limit", type=double, &end
&parameter name=nuy, symbol="$gn$r$by$n", units="1/(2$gp$r)", description="Vertical tune", type=double, &end
&parameter name=dnuy/dp, symbol="$gx$r$by$n", units="1/(2$gp$r)", description="Vertical chromaticity", type=double, &end
&parameter name=dnuy/dp2, symbol="$gx$r$by2$n", units="1/(2$gp$r)", description="Vertical 2nd-order chromaticity", type=double, &end
&parameter name=dnuy/dp3, symbol="$gx$r$by3$n", units="1/(2$gp$r)", description="Vertical 3rd-order chromaticity", type=double, &end
&parameter name=Ay, symbol="A$by$n", units=m, description="Vertical acceptance", type=double, &end
&parameter name=AyLocation, units=m, description="Location of vertical acceptance limit", type=double, &end
&parameter name=deltaHalfRange, symbol="$gDd$r/2", description="Half range of momentum offset for chromatic tune spread evaluation", type=double, &end
&parameter name=nuxChromUpper, symbol="$gx$r$bu$n", description="Upper limit of x tune due to chromaticity and deltaRange", type=double, &end
&parameter name=nuxChromLower, symbol="$gx$r$bu$n", description="Lower limit of x tune due to chromaticity and deltaRange", type=double, &end
&parameter name=nuyChromUpper, symbol="$gy$r$bu$n", description="Upper limit of y tune due to chromaticity and deltaRange", type=double, &end
&parameter name=nuyChromLower, symbol="$gy$r$bu$n", description="Lower limit of y tune due to chromaticity and deltaRange", type=double, &end
&parameter name=Stage, description="Stage of computation", type=string, &end
&parameter name=pCentral, units="m$be$nc", description="Central momentum", type=double, &end
&parameter name=dbetax/dp, units=m, description="Derivative of betax with momentum offset", type=double, &end
&parameter name=dbetay/dp, units=m, description="Derivative of betay with momentum offset", type=double, &end
&parameter name=dalphax/dp, description="Derivative of alphax with momentum offset", type=double, &end
&parameter name=dalphay/dp, description="Derivative of alphay with momentum offset", type=double, &end
&parameter name=etax2, symbol="$gc$r$bx2$n", units=m, description="Second-order dispersion (for matched or periodic case only)", type=double, &end
&parameter name=etay2, symbol="$gc$r$by2$n", units=m, description="Second-order dispersion (for matched or periodic case only)", type=double, &end
&parameter name=etax3, symbol="$gc$r$bx3$n", units=m, description="Third-order dispersion (for matched or periodic case only)", type=double, &end
&parameter name=etay3, symbol="$gc$r$by3$n", units=m, description="Third-order dispersion (for matched or periodic case only)", type=double, &end
&parameter name=etaxp2, symbol="$gc$r$bx2$n$a'$n", units=m, description="Second-order dispersion (for matched or periodic case only)", type=double, &end
&parameter name=etayp2, symbol="$gc$r$by2$n$a'$n", units=m, description="Second-order dispersion (for matched or periodic case only)", type=double, &end
&parameter name=etaxp3, symbol="$gc$r$bx3$n$a'$n", units=m, description="Third-order dispersion (for matched or periodic case only)", type=double, &end
&parameter name=etayp3, symbol="$gc$r$by3$n$a'$n", units=m, description="Third-order dispersion (for matched or periodic case only)", type=double, &end
&parameter name=betaxMin, units=m, description="Minimum betax", type=double, &end
&parameter name=betaxAve, units=m, description="Average betax", type=double, &end
&parameter name=betaxMax, units=m, description="Maximum betax", type=double, &end
&parameter name=betayMin, units=m, description="Minimum betay", type=double, &end
&parameter name=betayAve, units=m, description="Average betay", type=double, &end
&parameter name=betayMax, units=m, description="Maximum betay", type=double, &end
&parameter name=etaxMax, units=m, description="Maximum absolute value of etax", type=double, &end
&parameter name=etayMax, units=m, description="Maximum absolute value of etay", type=double, &end
&parameter name=waistsx, description="Number of changes in the sign of alphax", type=long, &end
&parameter name=waistsy, description="Number of changes in the sign of alphay", type=long, &end
&parameter name=dnux/dAx, units=1/m, description="Horizontal tune shift with horizontal amplitude", type=double, &end
&parameter name=dnux/dAy, units=1/m, description="Horizontal tune shift with vertical amplitude", type=double, &end
&parameter name=dnuy/dAx, units=1/m, description="Vertical tune shift with horizontal amplitude", type=double, &end
&parameter name=dnuy/dAy, units=1/m, description="Vertical tune shift with vertical amplitude", type=double, &end
&parameter name=dnux/dAx2, units="1/m$a2$n", description="Horizontal tune shift with horizontal amplitude", type=double, &end
&parameter name=dnux/dAy2, units="1/m$a2$n", description="Horizontal tune shift with vertical amplitude", type=double, &end
&parameter name=dnux/dAxAy, units="1/m$a2$n", description="Horizontal tune shift with horizontal and vertical amplitude", type=double, &end
&parameter name=dnuy/dAx2, units="1/m$a2$n", description="Vertical tune shift with horizontal amplitude", type=double, &end
&parameter name=dnuy/dAy2, units="1/m$a2$n", description="Vertical tune shift with vertical amplitude", type=double, &end
&parameter name=dnuy/dAxAy, units="1/m$a2$n", description="Vertical tune shift with horizontal and vertical amplitude", type=double, &end
&parameter name=nuxTswaLower, description="Minimum horizontal tune from tune-shift-with-amplitude calculations", type=double, &end
&parameter name=nuxTswaUpper, description="Maximum horizontal tune from tune-shift-with-amplitude calculations", type=double, &end
&parameter name=nuyTswaLower, description="Minimum vertical tune from tune-shift-with-amplitude calculations", type=double, &end
&parameter name=nuyTswaUpper, description="Maximum vertical tune from tune-shift-with-amplitude calculations", type=double, &end
&parameter name=couplingIntegral, description="Coupling integral for difference resonance", type=double, &end
&parameter name=couplingDelta, description="Distance from difference resonance", type=double, &end
&parameter name=emittanceRatio, description="Emittance ratio from coupling integral", type=double, &end
&parameter name=alphac2, symbol="$ga$r$bc2$n", description="2nd-order momentum compaction factor", type=double, &end
&parameter name=alphac, symbol="$ga$r$bc$n", description="Momentum compaction factor", type=double, &end
&parameter name=I1, units=m, description="Radiation integral 1", type=double, &end
&parameter name=I2, units=1/m, description="Radiation integral 2", type=double, &end
&parameter name=I3, units="1/m$a2$n", description="Radiation integral 3", type=double, &end
&parameter name=I4, units=1/m, description="Radiation integral 4", type=double, &end
&parameter name=I5, units=1/m, description="Radiation integral 5", type=double, &end
&parameter name=ex0, units="$gp$rm", description="Damped horizontal emittance", type=double, &end
&parameter name=enx0, units="m$be$nc $gp$rm", description="Damped normalized horizontal emittance", type=double, &end
&parameter name=taux, units=s, description="Horizontal damping time", type=double, &end
&parameter name=Jx, description="Horizontal damping partition number", type=double, &end
&parameter name=tauy, units=s, description="Vertical damping time", type=double, &end
&parameter name=Jy, description="Vertical damping partition number", type=double, &end
&parameter name=Sdelta0, description="RMS fractional energy spread", type=double, &end
&parameter name=taudelta, units=s, description="Longitudinal damping time", type=double, &end
&parameter name=Jdelta, description="Longitudinal damping partition number", type=double, &end
&parameter name=U0, units=MeV, description="Energy loss per turn", type=double, &end
&column name=s, units=m, description=Distance, type=double,  &end
&column name=betax, symbol="$gb$r$bx$n", units=m, description="Horizontal beta-function", type=double,  &end
&column name=alphax, symbol="$ga$r$bx$n", description="Horizontal alpha-function", type=double,  &end
&column name=psix, symbol="$gy$r$bx$n", units=rad, description="Horizontal phase advance", type=double,  &end
&column name=etax, symbol="$gc$r$bx$n", units=m, description="Horizontal dispersion", type=double,  &end
&column name=etaxp, symbol="$gc$r$bx$n$a'$n", description="Slope of horizontal dispersion", type=double,  &end
&column name=xAperture, symbol="a$bx,eff$n", units=m, description="Effective horizontal aperture", type=double,  &end
&column name=betay, symbol="$gb$r$by$n", units=m, description="Vertical beta-function", type=double,  &end
&column name=alphay, symbol="$ga$r$by$n", description="Vertical alpha-function", type=double,  &end
&column name=psiy, symbol="$gy$r$by$n", units=rad, description="Vertical phase advance", type=double,  &end
&column name=etay, symbol="$gc$r$by$n", units=m, description="Vertical dispersion", type=double,  &end
&column name=etayp, symbol="$gc$r$by$n$a'$n", description="Slope of vertical dispersion", type=double,  &end
&column name=yAperture, symbol="a$by,eff$n", units=m, description="Effective vertical aperture", type=double,  &end
&column name=pCentral0, symbol="p$bcent$n", units="m$be$nc", description="Initial central momentum", type=double,  &end
&column name=ElementName, description="Element name", format_string=%10s, type=string,  &end
&column name=ElementOccurence, description="Occurence of element", format_string=%6ld, type=long,  &end
&column name=ElementType, description="Element-type name", format_string=%10s, type=string,  &end
&column name=ChamberShape, type=string,  &end
&column name=dI1, units=m, description="Contribution to radiation integral 1", type=double,  &end
&column name=dI2, units=1/m, description="Contribution to radiation integral 2", type=double,  &end
&column name=dI3, units="1/m$a2$n", description="Contribution to radiation integral 3", type=double,  &end
&column name=dI4, units=1/m, description="Contribution to radiation integral 4", type=double,  &end
&column name=dI5, units=1/m, description="Contribution to radiation integral 5", type=double,  &end
&data mode=binary, &end
.       �Ŕw2��?�S`\-
�                        ����������~�R�?�=H+>��                        ��������        �Ŕw2��?�Ŕw2��?��~�R�?��~�R�?   tunes uncorrected�����@��o!~#��'碟k<��`�<�4�|C�O�L�                                                                ��J�?Z��չ�5@� �.h�P@[]�@Ru��;j@"�fw0@~��(�g�?                                                                                                                                      ���˅ɿ        ���sf?P�>��]��J-,����ޮvܦ?Ů8yv�~?�+��w�5r�W�;?�k��۔>����m+�>�lح��"@�ϝ�?ś�!ݤ"@      �?�T����?�p�em�@�0b���?��5t/�>        ߷��y@�{�1���                              $@�d�t@�#�Q�%�?                              $@�����@   _BEG_      MARK                                                    ߷��y@�{�1���                              $@�d�t@�#�Q�%�?                              $@�����@   BNCH      CHARGE   ?                                                ߷��y@�{�1���                              $@�d�t@�#�Q�%�?                              $@�����@
   BC1_IN_CNT      CENTER   ?                                        V(�2�8�?"�Z���@��+�}�������R�?                      $@�Ǩ��K@��q��?XP��X��?                      $@�����@	   BC1_DR_V1      DRIF   ?                                        ��C(&��?�tA�'@�
9�����d��Ku:�?                      $@9����@�4��[��?��z���?                      $@�����@   BC1_DR_QD_ED      DRIF   ?                                        ���<���?��={�@���ȶS��NMo�}�?                      $@[ s�>@��Ń]�?�=�V)�?                      $@�����@
   BC1_QD_V01      QUAD   ?                                        ���<���?��={�@���ȶS��NMo�}�?                      $@[ s�>@��Ń]�?�=�V)�?                      $@�����@   BC1_COR      KICKER   ?                                        ���<���?��={�@���ȶS��NMo�}�?                      $@[ s�>@��Ń]�?�=�V)�?                      $@�����@   BC1_BPM      MONI   ?                                        Bz9Q�$�?�U|�$@�����@�X�?                      $@��7ԓ�@F��!a�?/l#��m�?                      $@�����@
   BC1_QD_V01      QUAD   ?                                        �	�F���?�"_z`@c�fx9���L��>�?                      $@[]�@/G�H���?�+���p�?                      $@�����@   BC1_DR_QD_ED      DRIF   ?                                        Ʊ�4j@Z��k�<@�Dk�����#���W�?                      $@��Oel�/@V�9�������_�?                      $@�����@	   BC1_DR_V2      DRIF   ?                                        ��S��@p��� =@�����{( �c�?                      $@"�fw0@���>���~E�?                      $@�����@   BC1_DR_QD_ED      DRIF   ?                                        �_0��@)���Ш=@��K/�0+��=(o�?                      $@MX})0@w��}�@�Gbo�?                      $@�����@
   BC1_QD_V02      QUAD   ?                                        �_0��@)���Ш=@��K/�0+��=(o�?                      $@MX})0@w��}�@�Gbo�?                      $@�����@   BC1_COR      KICKER   ?                                        �_0��@)���Ш=@��K/�0+��=(o�?                      $@MX})0@w��}�@�Gbo�?                      $@�����@   BC1_BPM      MONI   ?                                        ���x�@x333?@���9����L�y�?                      $@��*3/@;�3�Ay!@H�� �?                      $@�����@
   BC1_QD_V02      QUAD   ?                                        �x��@��Oj�@@�IX��9�S�����?                      $@[ƽ���-@@}c!@h�/,�?                      $@�����@   BC1_DR_QD_ED      DRIF   ?                                        �5w�@���v��G@�c�W�?������?                      $@CXl�Y�%@�=Q5�'@~#q|�?                      $@�����@	   BC1_DR_V3      DRIF   ?                                        ��ԕ>@��C	"I@�2.��?��-K,��?                      $@jS(���$@L[�K�O@v3[�ٌ�?                      $@�����@   BC1_DR_QD_ED      DRIF   ?                                        ��cXg@6:���IJ@�k?=M�9��5�����?                      $@�����#@΅΀Tf@�&*��?                      $@�����@
   BC1_QD_V03      QUAD   ?                                        ��cXg@6:���IJ@�k?=M�9��5�����?                      $@�����#@΅΀Tf@�&*��?                      $@�����@   BC1_COR      KICKER   ?                                        ��cXg@6:���IJ@�k?=M�9��5�����?                      $@�����#@΅΀Tf@�&*��?                      $@�����@   BC1_BPM      MONI   ?                                        �?�	�@]~Ŋu0K@�;W�,3�.!?���?                      $@j�sQa�"@q����@�S��=��?                      $@�����@
   BC1_QD_V03      QUAD   ?                                        ڑ�9��@6���L@Ƒ/كv3�vJk����?                      $@��p�"@�8�4�@�!�S��?                      $@�����@   BC1_DR_QD_ED      DRIF   ?                                        �[�@����P@ Ⱥk��4���ьQ��?                      $@\����@���� @��I�$�?                      $@�����@	   BC1_DR_V4      DRIF   ?                                        �UD&�@H��u�vP@߮��f5�<�Б���?                      $@ĿJD�@<i��Q@�W	Շ<�?                      $@�����@   BC1_DR_QD_ED      DRIF   ?                                        &�����@WuW���P@hMS`���^�����?                      $@v�х�@�E�x�@�'�S�?                      $@�����@
   BC1_QD_V04      QUAD   ?                                        &�����@WuW���P@hMS`���^�����?                      $@v�х�@�E�x�@�'�S�?                      $@�����@   BC1_COR      KICKER   ?                                        &�����@WuW���P@hMS`���^�����?                      $@v�х�@�E�x�@�'�S�?                      $@�����@   BC1_BPM      MONI   ?                                        Oc��@� �.h�P@�lTM�@�HC�|��?                      $@���w�A@�e��F9�?�~��k�?                      $@�����@
   BC1_QD_V04      QUAD   ?                                        ;`�P1@�I�КP@����@�����?                      $@�p�.@*�e���?jn`�_��?                      $@�����@   BC1_DR_QD_ED      DRIF   ?                                        cBS�@8�V\�O@�>�C�@� �t��?                      $@�&)#�@�Ҁ"��?� ��  @                      $@�����@	   BC1_DR_V5      DRIF   ?                                        ���) @�r2��L@�*��@��}�^�?                      $@@
�@����3�?��b� @                      $@�����@	   BC1_DR_20      DRIF   ?                                        ��SeL!@�����I@ �� #\@*l���?Bm�c��WR�
]#��      $@����M@Tx����?^�Ә�3@                      $@�����@   BC1_DP_DIP1   	   CSRCSBEND   ?��{ n�3?��ޮv܆?Ů8yv�^?
,��w񾼞��!$?jk����!@�N��&H@�3��z@
�0���?f�f!��WR�
]#��      $@�Z6m;@#>�q�?��l��@                      $@�����@   BC1_DR_SIDE_C      CSRDRIFT   ?                                        ������'@�L]�� 4@�A�ﭗ@���a���?�0{��ͿWR�
]#��      $@6�r�,@R�l�M�ο,y�g�@                      $@�����@   BC1_DR_SIDE      DRIF   ?                                        �s�ʜ(@�m��>1@�0�?@�>�{��?~��(�gϿ              $@����7�@�8|��ѿ��J�O@                      $@�����@   BC1_DP_DIP2   	   CSRCSBEND   ?۞��#z����ޮv܆?Ů8yv�^?�+��w�q+Xfn/?K ���O)@+��f�-@�zy�3	@a�M�;�?~��(�gϿ              $@���2�@?~�K�ֿ9Ƿo��@                      $@�����@   BC1_DR_CENT_C      CSRDRIFT   ?                                        K ���O)@+��f�-@�zy�3	@a�M�;�?~��(�gϿ              $@���2�@?~�K�ֿ9Ƿo��@                      $@�����@   BC1_BPM      MONI   ?                                        ~3�+1*@Y�&�)@Q��/(@�k�
�?~��(�gϿ              $@b��Fk�@zʵZۿ,oفc@                      $@�����@   BC1_DR_CENT      DRIF   ?                                        ��	%�*@Z �*%@H�o+$�@�%��#�?�0{��ͿWR�
]#�?      $@��I�1e@�i	�ݿ�א�@                      $@�����@   BC1_DP_DIP3   	   CSRCSBEND   ?���#z����ޮv܆?Ů8yv�^?�+��w�(i�Ø4�>6�&��i+@{��DR-"@X�pO(@�%N��?;~t��˿WR�
]#�?      $@�*&Ȏ@��T8"}�Ɩ�#[	@                      $@�����@   BC1_DR_SIDE_C      CSRDRIFT   ?                                        c�E�0@�w���?[����?��6��?�m�c��WR�
]#�?      $@��Q��%@���}d���:J�hM@                      $@�����@   BC1_DR_SIDE      DRIF   ?                                        r~�4i1@��J�?��Q}���e�ay@      |�              $@�	�g�'@�m��[�L%��V�@                      $@�����@   BC1_DP_DIP4   	   CSRCSBEND   ?��{ n�3?��ޮv܆?Ů8yv�^?,��w�i�����>?K�6]1@���o���?�!�C%Kӿ��F�@      |�              $@0�@-�(@��8_>�E=���@                      $@�����@   BC1_DR_20_C      CSRDRIFT   ?                                        ?K�6]1@���o���?�!�C%Kӿ��F�@      |�              $@0�@-�(@��8_>�E=���@                      $@�����@	   BC1_WA_OU      WATCH   ?                                        