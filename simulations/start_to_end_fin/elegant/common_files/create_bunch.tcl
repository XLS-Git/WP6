#########################################################################################
#  some constands
#----------------------------------------------------------------------------------------
set clight 2.9979246e+08
set Z0 376.99112
set pi [expr acos(-1.)]
set mc2 0.51099893


#########################################################################################
#  loading required libraries 
#----------------------------------------------------------------------------------------
package require math::statistics
package require math::fourier 
package require math::bigfloat
package require math::special
package require Tcl 
package require struct
package require math::interpolate 
package require math::calculus
namespace import ::math::bigfloat::*



#########################################################################################
# proc for random normal number with mean zero and standard deviation 1.0
#----------------------------------------------------------------------------------------
proc random {} {
     variable last
     if { [info exists last] } {
		  set retval $last
		  unset last
		  return $retval
     }
     set v1 [expr  2. * rand() - 1 ]
     set v2 [expr  2. * rand() - 1 ]
     set rsq [expr  $v1*$v1 + $v2*$v2 ]
     while { $rsq > 1. } {
		  set v1 [expr   2. * rand() - 1  ]
		  set v2 [expr   2. * rand() - 1  ]
		  set rsq [expr   $v1*$v1 + $v2*$v2  ]
     }
     set fac [expr   sqrt( -2. * log( $rsq ) / $rsq )  ]
     set last [expr   $v1 * $fac  ]
     return [expr   $v2 * $fac  ]
 } 

#########################################################################################
# proc for uniform number between -1.0 - 1.0
#----------------------------------------------------------------------------------------
proc randomuni {} {
   return [expr -1.+ 2.*rand()]
}




#########################################################################################
#  gaussian function variance sigma and mean 0. 
#----------------------------------------------------------------------------------------
proc gaussian {s sigma} {
	global pi
	return [expr 1./$sigma/sqrt(2.*$pi)*exp(-($s*$s)/2./$sigma/$sigma)]
}

#########################################################################################
#  plateau function with mean zoro and variance sigma 
#----------------------------------------------------------------------------------------
proc plateau {s sigma} {
	global pi
	set ll [expr 3.1321*$sigma] 
	set rt [expr $ll/8.]
	return [expr 1./$ll/(1.+exp(2./$rt*(2.*abs($s)-$ll)))]
}

#########################################################################################
#  inverse parabola function
#----------------------------------------------------------------------------------------
proc parabola {s sigma} {

	set zmax [expr  1.0*sqrt(5.)*$sigma] 
	set zmin [expr -1.0*$zmax] 
	if {$s>=$zmin && $s<= $zmax} {
		set tmp [expr 3./4./$zmax*(1.-$s*$s/$zmax/$zmax)]
	} else {
		set tmp 0.0
	}
	return $tmp
}

#########################################################################################
#  step function 
#----------------------------------------------------------------------------------------
proc uniform {s sigma} {
	global pi
	set fwhm [expr $sigma*2.*sqrt(3)]
	set zmax [expr $fwhm*0.5] 
	set zmin [expr -1.0*$zmax] 
	
	if {$zmin<$s<$zmax} {
		set tmp [expr 1./$fwhm]
	} else {
		set tmp 0.0
	}
	return $tmp
}



proc gaussList { parameters } {
    
    upvar $parameters params 
    
    if {[info exist params(mu)]<1} { set mu 0.0 } else { set mu $params(mu) }
    if {[info exist params(sigma)]<1} { set sigma 1.0 } else { set sigma $params(sigma) }
# 	if {[info exist params(sigma_cut)]<1} { set sigma_cut [expr 4] } else { set sigma_cut $params(sigma_cut) }
	if {[info exist params(n_slice)]<1} {set n_slice 21 } else {set n_slice $params(n_slice)}
	if {[info exist params(npart)]<1} {set npart 10000 } else {set npart $params(npart)}
	
	set sigma_cut 3.3
	set minz [expr -$sigma_cut*$sigma]
	set dz [expr 2.*$sigma_cut*$sigma/($n_slice)]
	
    set maxz 0.0
	while { [expr [gaussian $maxz $sigma]*$npart] > [expr $npart*0.004] } {
		set maxz [expr $maxz+$dz]
	}
	
	set minz -$maxz
	set dz [expr ($maxz - $minz)/$n_slice]
	
	set sum 0.0
	set al  {}
	set zl  {}
	for {set i 0} {$i<[expr $n_slice]} {incr i } {
		set z   [expr $minz+($i+0.5)*$dz]
		set zz  [expr $z+$mu]
		set a   [expr [gaussian $z $sigma]]
		set sum [expr $sum+$a]
		lappend zl  "$zz "
		lappend al  "$a "
	}
	
	set norm [expr 1./$sum]
	
	set tmp {}
		foreach z  $zl a $al {
		set a [expr $a*$norm]
		lappend tmp "$z $a"
	}
	return $tmp
}




proc plateauList { parameters } {
	
    upvar $parameters params 

	if {[info exist params(mu)]<1 } { set mu 0.0 } else { set mu $params(mu) }
	if {[info exist params(sigma)]<1} {set sigma 1.0 } else {  set sigma $params(sigma) }
	if {[info exist params(n_slice)]<1} { set n_slice 11 } else { set n_slice $params(n_slice) }
    if {[info exist params(npart)]<1} {set npart 10000 } else {set npart $params(npart)}

	set sigma_cut 2.5
	set minz [expr -$sigma_cut*$sigma]
	set dz  [expr 2*$sigma_cut*$sigma/($n_slice)]
	
    set maxz 0.0
	while { [expr [plateau $maxz $sigma]*$npart] > [expr $npart*0.004] } {
		set maxz [expr $maxz+$dz]
	}
	
# 	puts $maxz

	set minz -$maxz
	set dz [expr ($maxz - $minz)/$n_slice]

	set sum 0.0
	set al  {}
	set zl  {}
	set maxa -1e10
	for {set i 0} {$i<[expr $n_slice]} {incr i } {
		set z   [expr $minz+($i+0.5)*$dz]
		set zz  [expr $z+$mu]
		set a   [expr [plateau $z $sigma]]
		if {$a>$maxa} {set maxa $a} else {set maxa $maxa}
		set sum [expr $sum+$a]
		lappend zl  "$zz "
		lappend al  "$a "
	}
	
	set norm [expr 1./$maxa]
	
	set tmp {}
	foreach z  $zl a $al {
		set a [expr $a*$norm]
		lappend tmp "$z $a"
	}
	return $tmp
}


proc parabolaList { parameters } {
	upvar $parameters params 

	if {[info exist params(mu)]<1} {set mu 0.0} else {set mu $params(mu)}
	if {[info exist params(sigma)]<1} {set sigma 1.0} else {set sigma $params(sigma)}
	if {[info exist params(n_slice)]<1} {set n_slice 11} else {set n_slice $params(n_slice)}
    if {[info exist params(npart)]<1} {set npart 10000 } else {set npart $params(npart)}
	
    set sigma_cut [expr sqrt(5.)]
    
    set minz [expr -$sigma_cut*$sigma]
    set maxz [expr $sigma_cut*$sigma]
	set dz  [expr 2*$sigma_cut*$sigma/($n_slice)]

	while { [expr [parabola $maxz $sigma]*$npart] > [expr $npart*0.004] } {
		set maxz [expr $maxz+$dz]
	}
	
    set minz -$maxz
	set dz [expr ($maxz - $minz)/$n_slice]
	
	set sum 0.0
	set al  {}
	set zl  {}
	set maxa -1e-6
	for {set i 0} {$i<[expr $n_slice]} {incr i } {
		set z   [expr $minz+($i+0.5)*$dz]
		set zz  [expr $z+$mu]
		set a   [parabola $z $sigma]
		if {$a>$maxa} {set maxa $a} else {set maxa $maxa}
		set sum [expr $sum+$a]
		lappend zl  $zz 
		lappend al  $a 
	}
	
	set norm [expr 1./$maxa]
	
	set tmp {}
    foreach z  $zl a $al {
		set a [expr $a*$norm]
		lappend tmp "$z $a"
	}
	return $tmp
}


proc uniformList { parameters } {
	upvar $parameters params 

	if {[info exist params(mu)]<1} {set mu 0.0} else {set mu $params(mu)}	
	if {[info exist params(sigma)]<1} {set sigma 1.0} else {set sigma $params(sigma)}
	if {[info exist params(n_slice)]<1} {set n_slice 11} else {set n_slice $params(n_slice)}
    if {[info exist params(npart)]<1} {set npart 10000 } else {set npart $params(npart)}
	
    set sigma_cut [expr 2.*sqrt(3)]
	set minz [expr -0.5*$sigma_cut*$sigma]
	set dz  [expr $sigma_cut*$sigma/$n_slice]

	set sum 0.0
	set al  {}
	set zl  {}
	for {set i 0} {$i<[expr $n_slice]} {incr i } {
		set z   [expr $minz+($i+0.5)*$dz]
		set zz  [expr $z+$mu]
		set a   [expr [uniform $z $sigma]]
		set sum [expr $sum+$a]
		lappend zl  "$zz "
		lappend al  "$a "
	}
	
	set norm [expr 1./$sum]
	
	set tmp {}
		foreach z  $zl a $al {
		set a [expr $a*$norm]
		lappend tmp "$z $a"
	}
	return $tmp
}






#########################################################################################
# proc for integration of an two dimensional array trapezidoal 
#----------------------------------------------------------------------------------------
proc trapz {array} {

    set z {}
    set p {}

    foreach ln $array {
        lappend z [lindex $ln 0]
        lappend p [lindex $ln 1]
    }
    set nstep [llength $array]

    set int 0.0
        
    for {set i 0} {$i< [expr $nstep-1]} {incr i} {
        
        set z_ii [lindex $z [expr $i]]
        set z_jj [lindex $z [expr $i+1]]
        set p_ii  [lindex $p [expr $i]]
        set p_jj  [lindex $p [expr $i+1]]
        set int [expr $int + ($z_jj-$z_ii)*($p_ii + $p_jj)*0.5]
        
    }

    return $int

}


proc generate_zlist { parameters} {
    upvar $parameters params 
	set params(mu) 0.0
    set params(sigma) 1.0
	if {[info exist params(n_slice)]<1} {set params(n_slice) 11}  
    if {[info exist params(npart)]<1} {set params(npart) 10000 }  
    if {[info exist params(type)]<1} {set params(type)  "gaussian" }  
    
    if { $params(type) == "gaussian" } {
        set vect [gaussList params ]
    } elseif { $params(type) == "plateau" } {
        set vect [plateauList params]
    } elseif {  $params(type) == "parabola" } {
       set vect [parabolaList params ]
    } elseif {  $params(type) == "uniform" } {
        set vect [uniformList params ]
    } else {
        puts "no matching with known distribution --> gaussian type is taken"
        set vect [gaussList params ]
    }
	
     return $vect;
}



proc randompdf {bparray npart} {

# % RANDPDF
# % Random numbers from a user defined distribution
# % SYNTAX:
# %   x = randpdf(A, npart)
# % INPUT : A is the column of z,p
# %   p     - probability density,
# %   z     - values for probability density,
# %   npart - dimension for the output .
# % OUTPUT:
# %   x   - random numbers.

    set int [trapz $bparray]
    
    set z {}
    set p {}
    set zp {}
    foreach ln $bparray {
        lappend z [lindex $ln 0]
        lappend p [expr [lindex $ln 1]/$int]
        lappend zp [lindex $ln 0] [expr [lindex $ln 1]/$int]
    }
    set nstep [llength $bparray]
    
    set minz [::math::statistics::min $z] 
	set maxz [::math::statistics::max $z] 
	
# % make interpoiation	1000 is enough
	set n_sliceint 1000
	set dz [expr ($maxz-$minz)/$n_sliceint]
	set z2 {}
    for {set i 0} {$i< [expr $n_sliceint]} {incr i} {
        lappend z2 [expr $minz+($i+0.5)*$dz]
      }    
    
    set ptmp {}
    set nrmf 0.0
    foreach  z $z2 {
        set a [::math::interpolate::interp-linear $zp $z]
        lappend  ptmp $a
        set nrmf [expr $nrmf + $a ]
    }


# % normalize the function to have an area of 1.0 under it
    set p2 {}
    foreach  pt $ptmp {
        lappend  p2 [expr $pt/$nrmf]
    }
    
# % the integral of PDF is the cumulative distribution function
    set cdfp {}
    set sum 0.0
    foreach pp $p2 {
        set sum [expr $sum+$pp]
        lappend cdfp $sum        
    }
    
# %    remove unique elements 
    set cdfp [lsort -increasing -real $cdfp]
    set indexs [lsort -indices -increasing -integer -unique -real $cdfp]
   
    set z3 {}
    set p3 {}
    set cpdfz {}

    foreach a $indexs {
        lappend p3 [lindex $p2 $a]
        lappend z3 [lindex $z2 $a]
        lappend cpdfz [lindex $cdfp $a] [lindex $z2 $a]
    }
    
    set uniformDistNum {}
    for {set i 0} {$i < $npart } {incr i} {
        lappend uniformDistNum [expr rand()]
    }
 
   set mydist {}
   foreach x $uniformDistNum {
      set tmp [::math::interpolate::interp-linear $cpdfz $x]
      lappend mydist $tmp
    }

    return $mydist

}



proc create_distribution { parameters } {
    
    upvar $parameters beamparams 
    
    set type $beamparams(type)
    set bx   $beamparams(beta_x)
    set ax   $beamparams(alpha_x)
    set exn  $beamparams(emitt_x)
    set by   $beamparams(beta_y)
    set ay   $beamparams(alpha_y)
    set eyn  $beamparams(emitt_y)
    set sz   $beamparams(sigma_z)
    set e0   $beamparams(energy)
    set de   $beamparams(e_spread)
    set npart  $beamparams(npart)
    set filename $beamparams(filename)

    if {[info exist beamparams(e_offset)]<1} { set eoff 0. } else { set eoff $beamparams(e_offset) }
    if {[info exist beamparams(x_offset)]<1} { set xoff 0. } else { set xoff $beamparams(x_offset) }
    if {[info exist beamparams(y_offset)]<1} { set yoff 0. } else { set yoff $beamparams(y_offset) }
    if {[info exist beamparams(z_offset)]<1} { set zoff 0. } else { set zoff $beamparams(z_offset) }
    if {[info exist beamparams(xp_offset)]<1} { set xpoff 0. } else { set xpoff $beamparams(xp_offset) }
    if {[info exist beamparams(yp_offset)]<1} { set ypoff 0. } else { set ypoff $beamparams(yp_offset) }
    
    
    set mc2 0.51099893
    set clight 2.9979246e+08
    set g0  [expr $e0/$mc2+1]

    set ex  [expr $exn/$g0]
    set ey  [expr $eyn/$g0]
    set sx  [expr sqrt($ex*$bx)]
    set spx [expr sqrt($ex/$bx)]
    set sy  [expr sqrt($ey*$by)]
    set spy [expr sqrt($ey/$by)]
    set se  [expr 0.01*$de]
    set p0  [expr sqrt($e0*$e0+2*$e0*$mc2)/$mc2]
#     set p0  [expr $e0/$mc2]
    set t0  [expr $zoff/$clight]
    
    if { $type != "uniform"  } {
        set zlist  [generate_zlist beamparams]
        set zall   [randompdf $zlist $npart]
    } elseif { $type == "uniform" } {
        set zall {}
        for  {set i 0} {$i<$npart} {incr i} {            
            set zt [expr -1. + 2. * rand()]
            lappend zall [expr sqrt(3)*$zt]
        }
    } else {
        puts "unknown distibution"
        exit
    }
    
    set x0  $xoff
    set xp0 $xpoff
    set y0  $yoff
    set yp0 $ypoff  
    set z0  $zoff
    set t00 [expr $zoff/$clight]
    set e00 [expr $e0+$eoff]
    set p00 [expr sqrt($e00*$e00+2*$e00*$mc2)/$mc2]
    
    set ref "$x0 $xp0 $y0 $yp0 $t00 $p00 $z0 $e00"
	  

    
    set beam {}
    
	for {set i 0} {$i<$npart} {incr i} {
	    set wt [random]
	    set wt [expr (1.0+$wt*$se)*$e0+$eoff]
# 	    set w [lappend w $wt]
        set pt [expr sqrt($wt*$wt+2.0*$wt*$mc2)/$mc2]
# 	    set p [lappend p [expr sqrt($wt*$wt+2*$wt*$mc2)/$mc2]]
        set zt [expr [lindex $zall $i]*$sz+$zoff]
#         set z [lappend z $zt]
        set tt [expr $zt/$clight]
	    set xt  [expr [random]*$sx+$xoff]
# 	    set x [lappend x $xt]
	    set xpt [expr [random]*$spx-$ax*$xt/$sx*$spx+$xpoff]
# 	    set xp [lappend xp $xpt]
	    set yt  [expr [random]*$sy+$yoff]
# 	    set y [lappend y $yt]
	    set ypt [expr [random]*$spy-$ay*$yt/$sy*$spy+$ypoff]
# 	    set yp [lappend yp $ypt]
	    set ind $i
        set beam [lappend beam "$xt  $xpt $yt $ypt $tt $pt $zt $wt" ]

	}
	
	set beam [lsort  -real -index 4 $beam]
	
	set tmin [lindex  [lindex $beam 0] 4]
# 	puts $tmin
	set zmin [lindex  [lindex $beam 0] 6]
	
	set ref [lreplace $ref 4 4 $tmin]
    set ref [lreplace $ref 6 6 $zmin]
	
	set beam [lreplace $beam 0 0 $ref]
    
set fou [open tmpp w]
  
puts $fou "SDDS1
&parameter name=Description, format_string=\%s, type=string, &end
&parameter name=pCentral, symbol=\"p\$bcen\$n\", units=\"m\$be\$nc\", description=\"Reference beta*gamma\", type=double, &end
&parameter name=Particles, description=\"Number of particles\", type=long, &end
&parameter name=PassCentralTime, units=s, type=double, &end
&column name=x, units=m, type=double,  &end
&column name=xp, type=double,  &end
&column name=y, units=m, type=double,  &end
&column name=yp, type=double,  &end
&column name=t, units=s, type=double,  &end
&column name=p, units=\"m\$be\$nc\", type=double,  &end
&column name=particleID, type=long,  &end
&data mode=ascii, &end
! page number 1
placet2elegant $filename"

puts $fou [format "%22.15e" $p0]
puts $fou [format " %d" $npart]  
puts $fou [format "%22.15e" $t0]
puts $fou [format " %18d" $npart] 
set ind 0
foreach bl $beam {
# $xt  $xpt $yt $ypt $tt $pt $zt $wt 
    set x  [lindex $bl 0]
    set xp [lindex $bl 1]
    set y  [lindex $bl 2]
    set yp [lindex $bl 3]
    set t  [lindex $bl 4]
    set p  [lindex $bl 5]
    set z  [lindex $bl 6]
    set e  [lindex $bl 7]
    
    puts $fou [format "%22.15e %22.15e %22.15e %22.15e %22.15e %22.15e %d" \
        $x $xp $y $yp $t $p $ind ]
        incr ind
    }
    close $fou
    #   
    exec sddsconvert tmpp $filename -binary 
    exec rm tmpp
    
}



proc create_distribution_slice { parameters } {
    
    upvar $parameters beamparams 
    
    set type $beamparams(type)
    set bx   $beamparams(beta_x)
    set ax   $beamparams(alpha_x)
    set exn  $beamparams(emitt_x)
    set by   $beamparams(beta_y)
    set ay   $beamparams(alpha_y)
    set eyn  $beamparams(emitt_y)
    set sz   $beamparams(sigma_z)
    set e0   $beamparams(energy)
    set de   $beamparams(e_spread)
    set npart  $beamparams(npart)
    set filename $beamparams(filename)

    if {[info exist beamparams(e_offset)]<1} { set eoff 0. } else { set eoff $beamparams(e_offset) }
    if {[info exist beamparams(x_offset)]<1} { set xoff 0. } else { set xoff $beamparams(x_offset) }
    if {[info exist beamparams(y_offset)]<1} { set yoff 0. } else { set yoff $beamparams(y_offset) }
    if {[info exist beamparams(z_offset)]<1} { set zoff 0. } else { set zoff $beamparams(z_offset) }
    if {[info exist beamparams(xp_offset)]<1} { set xpoff 0. } else { set xpoff $beamparams(xp_offset) }
    if {[info exist beamparams(yp_offset)]<1} { set ypoff 0. } else { set ypoff $beamparams(yp_offset) }
    
    
    set mc2 0.51099893
    set clight 2.9979246e+08
    set g0  [expr $e0/$mc2+1]

    set ex  [expr $exn/$g0]
    set ey  [expr $eyn/$g0]
    set sx  [expr sqrt($ex*$bx)]
    set spx [expr sqrt($ex/$bx)]
    set sy  [expr sqrt($ey*$by)]
    set spy [expr sqrt($ey/$by)]
    set se  [expr 0.01*$de]
#     set p0  [expr sqrt($e0*$e0+2*$e0*$mc2)/$mc2]
    set p0  [expr $e0/$mc2]
    set t0  [expr $zoff/$clight]
    
    if { $type != "uniform"  } {
        set zlist  [generate_zlist beamparams]
        set zall   [randompdf $zlist $npart]
    } elseif { $type == "uniform" } {
        set zall {}
        for  {set i 0} {$i<$npart} {incr i} {            
            set zt [expr -1. + 2. * rand()]
            lappend zall [expr sqrt(3)*$zt]
        }
    } else {
        puts "unknown distibution"
        exit
    }
    
    set x0  $xoff
    set xp0 $xpoff
    set y0  $yoff
    set yp0 $ypoff  
    set z0  $zoff
    set t00 [expr $zoff/$clight]
    set e00 [expr $e0+$eoff]
    set p00 [expr sqrt($e00*$e00+2*$e00*$mc2)/$mc2]
    
    set ref "$x0 $xp0 $y0 $yp0 $t00 $p00 $z0 $e00"
	  

    
    set beam {}
    
	for {set i 0} {$i<$npart} {incr i} {
	    set wt [random]
	    set wt [expr (1.0+$wt*$se)*$e0+$eoff]
# 	    set w [lappend w $wt]
        set pt [expr sqrt($wt*$wt+2*$wt*$mc2)/$mc2]
# 	    set p [lappend p [expr sqrt($wt*$wt+2*$wt*$mc2)/$mc2]]
        set zt [expr [lindex $zall $i]*$sz+$zoff]
#         set z [lappend z $zt]
        set tt [expr $zt/$clight]
# 	    set xt  [expr [random]*$sx+$xoff]
        set xt  [expr $xoff]
# 	    set x [lappend x $xt]
# 	    set xpt [expr [random]*$spx-$ax*$xt/$sx*$spx+$xpoff]
        set xpt [expr $xpoff]
# 	    set xp [lappend xp $xpt]
# 	    set yt  [expr [random]*$sy+$yoff]
	    set yt  [expr  $yoff]
# 	    set y [lappend y $yt]
# 	    set ypt [expr [random]*$spy-$ay*$yt/$sy*$spy+$ypoff]
	    set ypt [expr $ypoff]
# 	    set yp [lappend yp $ypt]
	    set ind $i
        set beam [lappend beam "$xt  $xpt $yt $ypt $tt $pt $zt $wt" ]

	}
	
	set beam [lsort  -real -index 4 $beam]
	
	set tmin [lindex  [lindex $beam 0] 4]
# 	puts $tmin
	set zmin [lindex  [lindex $beam 0] 6]
	
	set ref [lreplace $ref 4 4 $tmin]
    set ref [lreplace $ref 6 6 $zmin]
	
	set beam [lreplace $beam 0 0 $ref]
    
set fou [open tmpp w]
  
puts $fou "SDDS1
&parameter name=Description, format_string=\%s, type=string, &end
&parameter name=pCentral, symbol=\"p\$bcen\$n\", units=\"m\$be\$nc\", description=\"Reference beta*gamma\", type=double, &end
&parameter name=Particles, description=\"Number of particles\", type=long, &end
&parameter name=PassCentralTime, units=s, type=double, &end
&column name=x, units=m, type=double,  &end
&column name=xp, type=double,  &end
&column name=y, units=m, type=double,  &end
&column name=yp, type=double,  &end
&column name=t, units=s, type=double,  &end
&column name=p, units=\"m\$be\$nc\", type=double,  &end
&column name=particleID, type=long,  &end
&data mode=ascii, &end
! page number 1
placet2elegant $filename"

puts $fou [format "%22.15e" $p0]
puts $fou [format " %d" $npart]  
puts $fou [format "%22.15e" $t0]
puts $fou [format " %18d" $npart] 
set ind 0
foreach bl $beam {
# $xt  $xpt $yt $ypt $tt $pt $zt $wt 
    set x  [lindex $bl 0]
    set xp [lindex $bl 1]
    set y  [lindex $bl 2]
    set yp [lindex $bl 3]
    set t  [lindex $bl 4]
    set p  [lindex $bl 5]
    set z  [lindex $bl 6]
    set e  [lindex $bl 7]
    
    puts $fou [format "%22.15e %22.15e %22.15e %22.15e %22.15e %22.15e %d" \
        $x $xp $y $yp $t $p $ind ]
        incr ind
    }
    close $fou
    #   
    exec sddsconvert tmpp $filename -binary 
    exec rm tmpp
    
}





proc create_bunch {bparray} {

    upvar $bparray beamparams
        
    if {[info exist beamparams(type)]<1} {
        puts "\n *** warning: no longitudinal charge distibution is given \n *** gaussian type of distibution for 6-d is taken account"
        set beamparams(type) gaussian
    } 
    
    if {[info exist beamparams(filename)]<1} {
        puts "\n *** warning: no filename for store is given \n *** particles_in.sdds is going to be filename"
        set beamparams(filename) particles_in.sdds
       } 
    
    if {[info exist beamparams(n_slice)]<1} { 
        puts "\n *** warning: slice number is not defined \n **** n_slice=101 is going to be used"
        set beamparams(n_slice) 101
    }
    
    if {[info exist beamparams(n_macro)]<1} { 
        puts "warning: n_macro per slice  number is not defined \n n_macro=101 is going to be used"
        set beamparams(n_macro) 101
    }
    
    if {[info exist beamparams(npart)]<1} { 
        set beamparams(npart) [expr $beamparams(n_slice)*$beamparams(n_macro)]
    } 
    
    if {[info exist beamparams(beta_x)]<1} { set beamparams(beta_x) 1.0 } 
    if {[info exist beamparams(beta_y)]<1} { set beamparams(beta_y) 1.0 } 
    if {[info exist beamparams(alpha_x)]<1} { set beamparams(alpha_x) 0.0 } 
    if {[info exist beamparams(alpha_y)]<1} { set beamparams(alpha_y) 0.0 } 
    if {[info exist beamparams(emitt_x)]<1} { set beamparams(emitt_x) 1.0 } 
    if {[info exist beamparams(emitt_y)]<1} { set beamparams(emitt_y) 1.0 } 
    if {[info exist beamparams(energy)]<1} { set beamparams(energy) 100.0 }   
    if {[info exist beamparams(e_spread)]<1} { set beamparams(e_spread) 1.0}  
    if {[info exist beamparams(sigma_z)]<1} { set beamparams(sigma_z) 1.0e-3}  

	create_distribution beamparams
	
}



proc create_bunch_slice {bparray} {

    upvar $bparray beamparams
        
    if {[info exist beamparams(type)]<1} {
        puts "\n *** warning: no longitudinal charge distibution is given \n *** gaussian type of distibution for 6-d is taken account"
        set beamparams(type) gaussian
    } 
    
    if {[info exist beamparams(filename)]<1} {
        puts "\n *** warning: no filename for store is given \n *** particles_in.sdds is going to be filename"
        set beamparams(filename) particles_in.sdds
       } 
    
    if {[info exist beamparams(n_slice)]<1} { 
        puts "\n *** warning: slice number is not defined \n **** n_slice=101 is going to be used"
        set beamparams(n_slice) 101
    }
    
    if {[info exist beamparams(n_macro)]<1} { 
        puts "warning: n_macro per slice  number is not defined \n n_macro=101 is going to be used"
        set beamparams(n_macro) 101
    }
    
    if {[info exist beamparams(npart)]<1} { 
        set beamparams(npart) [expr $beamparams(n_slice)*$beamparams(n_macro)]
    } 
    
    if {[info exist beamparams(beta_x)]<1} { set beamparams(beta_x) 1.0 } 
    if {[info exist beamparams(beta_y)]<1} { set beamparams(beta_y) 1.0 } 
    if {[info exist beamparams(alpha_x)]<1} { set beamparams(alpha_x) 0.0 } 
    if {[info exist beamparams(alpha_y)]<1} { set beamparams(alpha_y) 0.0 } 
    if {[info exist beamparams(emitt_x)]<1} { set beamparams(emitt_x) 1.0 } 
    if {[info exist beamparams(emitt_y)]<1} { set beamparams(emitt_y) 1.0 } 
    if {[info exist beamparams(energy)]<1} { set beamparams(energy) 100.0 }   
    if {[info exist beamparams(e_spread)]<1} { set beamparams(e_spread) 1.0}  
    if {[info exist beamparams(sigma_z)]<1} { set beamparams(sigma_z) 1.0e-3}  

	create_distribution_slice beamparams
	
}

# 
# set fl par.sdds
# 
# array set params "sigma_x 0.5e-3 n_slice 101 npart [expr 201*101] beta_x 12 beta_y 5 alpha_x 0 alpha_y -1 emitt_x .5 emitt_y .5 energy 250"
# array set params "e_spread 0.2 n_macro 101 type gaussian filename $fl"
# 
# 
# 
# create_bunch params
# 
# source modify_output.tcl
# 
# modifyfile $fl
# exec sddsmultihist  $fl $fl.hist -columns=dz,dE,x,E,xp,y,yp -bins=201 -sides -sep
# #   lappend histfiles $fl.hist
# 
# print_parameters $fl
# #      normalize_bunch  $fl $filename.$indx.twi
# # 
#     exec sddsplot -layout=2,2 -sep  \
# 	-ticksettings=xgrid,ygrid,xlinetype=13,ylinetype=13,xthickness=1,ythickness=1 -linetypedefault=0,thickness=1 \
# 	-col=dz,dzFrequency -graphic=line $fl.hist \
#         -parameter=zero,E0 -legend=par=Description -lSpace=0.2,0.75,0,1 -nolabel -noscales -noborder $fl\
# 	-col=dz,dE -graph=dot -nolabel  $fl \
# 	-col=dEFrequency,dE -graphic=line $fl.hist  &
# 	
#     exec sddsplot -layout=2,2 -sep  \
# 	-ticksettings=xgrid,ygrid,xlinetype=13,ylinetype=13,xthickness=1,ythickness=1 -linetypedefault=0,thickness=1 \
# 	-col=x,xFrequency -graphic=line,vary $fl.hist\
#     -parameter=zero,E0 -legend=par=Description -lSpace=0.2,0.75,0,1 -nolabel -noscales -noborder $fl\
# 	-col=x,xp -graph=dot,vary -nolabel  $fl \
# 	-col=xpFrequency,xp -graphic=line,vary $fl.hist &
# 	
#     exec sddsplot -layout=2,2 -sep  \
# 	-ticksettings=xgrid,ygrid,xlinetype=13,ylinetype=13,xthickness=1,ythickness=1 -linetypedefault=0,thickness=1 \
#     -col=y,yFrequency -graphic=line,vary $fl.hist\
#     -parameter=zero,E0 -legend=par=Description -lSpace=0.2,0.75,0,1 -nolabel -noscales -noborder $fl\
# 	-col=y,yp -graph=dot,vary -nolabel  $fl \
# 	-col=ypFrequency,yp -graphic=line,vary $fl.hist &
# 
# 
# 
# 
# exit






