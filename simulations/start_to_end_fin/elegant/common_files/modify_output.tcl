


proc modifyfile {indist } {
# 	global sddsbeam

  set mc2 0.510998910  ;# MeV/c^2
  set c   299792458.e6 ;# um
  
  if { [file exists $indist.0] > 0 } {
      file copy -force $indist.0 $indist
      puts "$indist.0 exists copying file"
  } 

  file copy -force $indist $indist.0


  set pref  [exec sddsprocess $indist -pipe=out -process=p,average,pref -nowarning | sddsprintout -pipe -par=pref,format=\%.15e  -notitle -nolabel ]
  set tref  [exec sddsprocess $indist -pipe=out -process=t,average,tref -nowarning | sddsprintout -pipe -par=tref,format=\%.15e  -notitle -nolabel ]


  set t0 $tref
  set z00 0.0
  set p0 $pref
  set E0 [expr $p0*$mc2]

  set dT  "t $t0 - 1e15 * "
  set dZ  "t $t0 - $c * "
  set dP  "p $p0 -"
  set ee  "p $mc2 *"
  set dE  "p $p0 - $p0 / 100 *"
  set dE0 "E $E0 - "
  set ec  "pCentral $mc2 *" 
  
  set check [exec sddsprintout $indist -par=tMean -notitle -nolabel -nowarning]
#   puts $check
#   puts [llength $check]
  
  
  if { [llength $check] < 1 } {
  
exec sddsprocess -process=t,average,tMean -process=p,average,pMean -define=par,t0,$t0,units=s \
           -define=par,p0,$p0,units=m\$be\$nc \
		   -define=par,ECentral,$ec,units=MeV -define=par,E0,$E0,units=MeV -redefine=column,dt,$dT,units=fs \
		   -define=column,dz,$dZ,units=\$gm\$rm  -define=par,zero,$z00,units=\$gm\$rm -define=column,dp,$dP,units=m\$be\$n \
		   -define=column,E,$ee,units=MeV -define=column,dE,$dE,units=% -define=column,dE0,$dE0,units=MeV -noWarnings  $indist $indist.tmp

  set form "format=\%.6e"
  set params [eval exec sddsanalyzebeam $indist -pipe=out -nowarning | sddsprintout -pipe \
	  -col=enx,$form  -col=eny,$form -col=Sx,$form -col=Sy,$form  -col=St,$form -col=pCentral,$form -col=ecnx,$form  -col=ecny,$form -nolabel -notitle]
    set enx [expr [lindex $params 0]*1e6]
    set eny [expr [lindex $params 1]*1e6]
    set sx  [expr [lindex $params 2]*1e3]
    set sy  [expr [lindex $params 3]*1e3]
    set st  [lindex $params 4] 
    set pc  [lindex $params 5] 
    set ecnx [expr [lindex $params 6]*1e6]
    set ecny [expr [lindex $params 7]*1e6]
  
#   puts "emittance $enx"
  
  
  exec sddsprocess  -def=par,emtnx,$enx,units=umrad -def=par,emtny,$eny,units=umrad -def=par,emtcnx,$ecnx,units=umrad -def=par,emtcny,$ecny,units=umrad -def=par,sx,$sx,units=mm -def=par,sy,$sy,units=mm -process=dz,rms,sz  -process=dt,rms,st   -process=dE0,rms,sE  -noWarnings $indist.tmp $indist
  

  file delete -force $indist.tmp
  file delete -force $indist.0
  } else {
     puts " parameters exists nothing to do"
  }

}

proc modifytwiss {indist } {

  set mc2 0.510998910  ;# MeV/c^2
  set c   299792458.e3 ;# mm

  set ec  "pCentral0 $mc2 *"
  
  set aa [exec sddsprintout $indist -col=E -nowarning -noTitle -noLabels]
  
  if {[llength $aa ] < 0} { 
       exec sddsprocess  -define=column,E,$ec,units=MeV -noWarnings  $indist $indist.tmp -nowarning
  } else {
      exec sddsprocess  -redefine=column,E,$ec,units=MeV -noWarnings  $indist $indist.tmp -nowarning
  }
  after 250
  exec mv $indist.tmp $indist

}


proc get_slice_info {indist nslice} {


  exec elegant2genesis $indist tmp1 -slice=[expr $nslice+1]
  
  set desc  [exec sddsprintout $indist -par=Description,format=%s  -noTitle -noLabels]
# set desc  "[lindex [exec sddsprintout $indist -par=Description,format=%s  -noTitle -noLabels] 0]"

  regsub {\n$} $desc {} desc

#   puts $desc 
  
    eval exec sed 24d  tmp1 > tmp2
    eval exec sed 23s/[expr $nslice+1]/$nslice/ tmp2 > tmp1
#     exec rm tmp1 tmp2

  set mc2 0.510998910  ;# MeV/c^2
  set c   299792458.e6 ;# um

  set dT  "t 1e12 * "
  set dZ  "t $c * "
  set dZ2 "s 1e6 * "
  set ee  "gamma $mc2 *"
  set dE  "dgamma $mc2 *"
  set t2  "t  -1. *"
  set s2  "s  -1. *"



#   exec sddsprocess -redefine=column,s,$s2,units=m -redefine=column,t,$t2,units=s  tmp1 tmp2

  exec sddsprocess  -define=column,dz,$dZ,units=\$gm\$rm -define=column,dz2,$dZ2,units=\$gm\$rm   -define=column,dt,$dT,units=ps \
                   -define=column,E,$ee,units=MeV -define=column,dE,$dE,units=MeV \
                    tmp1 tmp2
  after 250
  exec sddsprocess  -print=par,Description,$desc,units=%s,type=string -process=dz,rms,sz  -process=dt,rms,st   -process=dE,rms,sE -noWarnings  tmp2  $indist.slice
  file delete -force tmp1
  file delete -force tmp2



}


proc color {foreground text} {
    return [exec tput setaf $foreground]$text[exec tput sgr0]
}




proc print_ellapsed_time {t0} {

set t1 [clock clicks -milliseconds] 
set dt_usec_all [expr $t1-$t0]
set dt_sec_all [expr int($dt_usec_all/1000)]
set dt_min_all [expr int($dt_sec_all/60)]
set dt_hou_all [expr int($dt_min_all/60)]
set dt_day_all [expr int($dt_hou_all/24)]

set dt_use [expr $dt_usec_all-$dt_sec_all*1000]
set dt_sec [expr $dt_sec_all-$dt_min_all*60]
set dt_min [expr $dt_min_all-$dt_hou_all*60]
set dt_hou [expr $dt_hou_all-$dt_day_all*24]

puts "[color 1 ==========================================================]"
puts [color 2 [format   "%2d hour %2d min %2d sec %3d usec" $dt_hou $dt_min $dt_sec $dt_use ]]
puts "[color 1 ==========================================================]"

}

 
proc print_parameters  {indist } {
    
    set form "format=\%.6e"
    
    set position [eval exec sddsprintout $indist -par=Description,label=location -nolabel -notitle ]
        
    set position [regsub -all {\s+} $position " "]
    
    puts "
    [color 4 $position ]"
    
    set params [eval exec sddsanalyzebeam $indist -pipe=out -nowarning | sddsprintout -pipe \
	  -col=en?,$form  -col=beta?,$form -col=alpha?,$form  -col=eta?,$form -col=eta?p,$form -col=pMean,$form -col=p0,$form \
	  -col=ECentral,$form  -col=E0,$form -col=sz,$form  -col=st,$form -col=sE,$form  -col=ecn?,$form -col=betac?,$form -nolabel -notitle]
    set enx [lindex $params 0] 
    set eny [lindex $params 1] 
    set btx [lindex $params 2] 
    set bty [lindex $params 3] 
    set alx [lindex $params 4] 
    set aly [lindex $params 5] 
    set etx [lindex $params 6] 
    set ety [lindex $params 7]
    set etxp [lindex $params 8] 
    set etyp [lindex $params 9]   
    set PM [lindex $params 10] 
    set P0 [lindex $params 11] 
    set EC [lindex  $params 12] 
    set E0 [lindex  $params 13] 
    set sz [lindex  $params 14] 
    set st [lindex  $params 15] 
    set sE [lindex  $params 16]   
    set encx [lindex $params 17] 
    set ency [lindex $params 18]  
    set btcx [lindex $params 19] 
    set btcy [lindex $params 20] 
#     puts "[color 2 E0=]$E0 [color 3 sig_z=]$sz $st [color 4 emit_x,y=]$enx $eny"
#     puts "[color 2 beta_x,y=]$btx $bty [color 2 alpha_x,y=]$alx $aly "

    puts "[color 2 Energy=]$E0  MeV      [color 2 Moment=]$P0  GeV/c " 
    puts "[color 2 emit_x=]$enx  mm.mrad  [color 2 emit_y=]$eny mm.mrad"
    puts "[color 2 emit_x=]$encx  mm.mrad  [color 2 emit_y=]$ency mm.mrad"
    puts "[color 2 sig_z=]$sz   um       [color 2  sig_t=]$st  fs"
    puts "[color 2 sig_E=]$sE   MeV      [color 2  dE=][expr $sE/$E0*100] \%"
    puts "[color 2 beta_x=]$btx  m        [color 2 beta_y=]$bty m"
    puts "[color 2 beta_x=]$btcx  m        [color 2 beta_y=]$btcy m"
    puts "[color 2 alph_x=]$alx          [color 2 alph_y=]$aly "
    puts "[color 2 eta_x=]$etx            [color 2 eta_xp=]$etxp "

  
 }
 

 proc plot_long_space { {fl} {disp 1} } {
 
       
        modifyfile $fl

        exec sddsmultihist  $fl $fl.tmph -columns=dt,dz,dE,E -bins=101 -sides -sep
#         exec sddssmooth $fl.tmph0 $fl.tmph -columns=dtFrequency,dzFrequency,dEFrequency,EFrequency \
#                 -SavitzkyGolay=3,3,3  -despike 
#         file delete -force $fl.tmph0
        
        lassign [exec sddsprocess $fl.tmph -pipe=out -process=dt,minimum,tmin -process=dt,maximum,tmax -process=dt,count,nline -nowarning | sddsprintout -pipe -par=tmax -par=tmin -par=nline  -notitle -nolabel]  tmax tmin nline
   
        set binstep [expr ($tmax-$tmin)/($nline-1)*1e-12]
   
        set curcalc  " Charge Particles / dtFrequency *  $binstep / " ;#kA conversation
   
        exec sddsprocess $fl.tmph $fl.hist -define=col,I,$curcalc,units=kA  -process=I,average,Iav
       
#         exec sddsprocess -pipe=out $fl.hist -filter=col,I,1,1000  | sddsprocess $fl.hist -process=I,average,Iav -nowarning
        
        file delete -force $fl.tmph
        
 
        print_parameters $fl

        if {$disp > 0} {
            exec sddsplot -thick=2  -layout=2,2 -sep  \
            -ticksettings=xgrid,ygrid,xlinetype=13,ylinetype=13,xthickness=1,ythickness=1 -linetypedefault=0,thickness=1 \
            -col=dt,I -graphic=line $fl.hist \
            -parameter=zero,E0  -lSpace=0.2,0.75,0,1 -nolabel -noscales -noborder \
            -string=Name=,pCoordinate=0,qCoordinate=1.0,scale=2 -string=@Description,pCoordinate=0.4,qCoordinate=1,scale=2 \
            -string=E0/MeV=,pCoordinate=0,qCoordinate=0.9,scale=2 -string=@E0,pCoordinate=0.4,qCoordinate=0.9,scale=2 \
            -string=St/fs=,pCoordinate=0,qCoordinate=0.8,scale=2 -string=@st,pCoordinate=0.4,qCoordinate=0.8,scale=2 \
            -string=Sz/um=,pCoordinate=0,qCoordinate=0.7,scale=2 -string=@sz,pCoordinate=0.4,qCoordinate=0.7,scale=2 \
            -string=Ib/kA=,pCoordinate=0,qCoordinate=0.6,scale=2 -string=@Iav,pCoordinate=0.4,qCoordinate=0.6,scale=2 \
            -string=SE/MeV=,pCoordinate=0,qCoordinate=0.5,scale=2 -string=@sE,pCoordinate=0.4,qCoordinate=0.5,scale=2 \
            -string=Sx/mm=,pCoordinate=0,qCoordinate=0.4,scale=2 -string=@sx,pCoordinate=0.4,qCoordinate=0.4,scale=2 \
            -string=Sy/mm=,pCoordinate=0,qCoordinate=0.3,scale=2 -string=@sy,pCoordinate=0.4,qCoordinate=0.3,scale=2 \
            -string=Emx/urad=,pCoordinate=0,qCoordinate=0.2,scale=2 -string=@emtnx,pCoordinate=0.4,qCoordinate=0.2,scale=2 \
            -string=Emy/urad=,pCoordinate=0,qCoordinate=0.1,scale=2 -string=@emtny,pCoordinate=0.4,qCoordinate=0.1,scale=2 \
            -string=Emcx/urad=,pCoordinate=0,qCoordinate=0.0,scale=2 -string=@emtcnx,pCoordinate=0.4,qCoordinate=0.0,scale=2 \
            -string=Emcy/urad=,pCoordinate=0,qCoordinate=-0.1,scale=2 -string=@emtcny,pCoordinate=0.4,qCoordinate=-0.1,scale=2 \
            $fl.hist\
            -col=dt,dE -graph=dot -nolabel  $fl \
            -col=dEFrequency,dE -graphic=line $fl.hist  &
        } else {
            exec sddsplot -thick=4  -layout=2,2 -sep  \
            -ticksettings=xgrid,ygrid,xlinetype=13,ylinetype=13,xthickness=1,ythickness=1 -linetypedefault=0,thickness=1 \
            -col=dt,I -graphic=line $fl.hist \
            -parameter=zero,E0  -lSpace=0.2,0.75,0,1 -nolabel -noscales -noborder \
            -string=Name=,pCoordinate=0,qCoordinate=1.0,scale=2 -string=@Description,pCoordinate=0.4,qCoordinate=1,scale=2 \
            -string=E0/MeV=,pCoordinate=0,qCoordinate=0.9,scale=2 -string=@E0,pCoordinate=0.4,qCoordinate=0.9,scale=2 \
            -string=St/fs=,pCoordinate=0,qCoordinate=0.8,scale=2 -string=@st,pCoordinate=0.4,qCoordinate=0.8,scale=2 \
            -string=Sz/um=,pCoordinate=0,qCoordinate=0.7,scale=2 -string=@sz,pCoordinate=0.4,qCoordinate=0.7,scale=2 \
            -string=Ib/kA=,pCoordinate=0,qCoordinate=0.6,scale=2 -string=@Iav,pCoordinate=0.4,qCoordinate=0.6,scale=2 \
            -string=SE/MeV=,pCoordinate=0,qCoordinate=0.5,scale=2 -string=@sE,pCoordinate=0.4,qCoordinate=0.5,scale=2 \
            -string=Sx/mm=,pCoordinate=0,qCoordinate=0.4,scale=2 -string=@sx,pCoordinate=0.4,qCoordinate=0.4,scale=2 \
            -string=Sy/mm=,pCoordinate=0,qCoordinate=0.3,scale=2 -string=@sy,pCoordinate=0.4,qCoordinate=0.3,scale=2 \
            -string=Emx/urad=,pCoordinate=0,qCoordinate=0.2,scale=2 -string=@emtnx,pCoordinate=0.4,qCoordinate=0.2,scale=2 \
            -string=Emy/urad=,pCoordinate=0,qCoordinate=0.1,scale=2 -string=@emtny,pCoordinate=0.4,qCoordinate=0.1,scale=2 \
            -string=Emcx/urad=,pCoordinate=0,qCoordinate=0.0,scale=2 -string=@emtcnx,pCoordinate=0.4,qCoordinate=0.0,scale=2 \
            -string=Emcy/urad=,pCoordinate=0,qCoordinate=-0.1,scale=2 -string=@emtcny,pCoordinate=0.4,qCoordinate=-0.1,scale=2 \
            $fl.hist\
            -col=dt,dE -graph=dot -nolabel  $fl \
            -col=dEFrequency,dE -graphic=line $fl.hist \
            -device=gpng,onwhite -output=$fl.png &
            
        }
    
 }
 
 
 
 
 proc calc_slice { {fl} {nslice 51} {minpart 5} } {
     
    set flname [file rootname $fl]
    set fout $flname.slc.sdds
    
    set mc2 0.51099891  ;# MeV/c^2
    set c   299792458e3      ;# mm
    set toum 1e-6

    set p0  [exec sddsprocess $fl -pipe=out -process=p,average,pref -nowarning | sddsprintout -pipe -par=pref,format=\%.15e  -notitle -nolabel ]
    set t0  [exec sddsprocess $fl -pipe=out -process=t,average,tref -nowarning | sddsprintout -pipe -par=tref,format=\%.15e  -notitle -nolabel ]
    set q0  [exec sddsprintout $fl  -par=Charge,format=\%.15e  -notitle -nolabel ]
    set txt  [exec sddsprintout $fl  -par=Description,format=\%s  -notitle -nolabel ]
    
    set t2t    "t $t0 -  " ;# s
    set t2z    "t $t0 - $c *  " ;# mm
    set p2e    "p $mc2 * "      ;# MeV
    set x2x    "x  1e3 * "      ;# mm
    set y2y    "y  1e3 * "      ;# mm
    set xp2xp  "xp  1e3 * "     ;# mrad
    set yp2yp  "yp  1e3 * "     ;# mrad
  

exec sddsprocess -redefine=col,dz,$t2z,units=mm -redefine=col,t,$t2t,units=s -redefine=col,E,$p2e,units=MeV \
		 -redefine=col,x,$x2x,units=mm  -redefine=col,xp,$xp2xp,units=mrad  \
		 -redefine=col,y,$y2y,units=mm  -redefine=col,yp,$yp2yp,units=mrad  \
					  $fl tmp0 -nowarnings
    
    
    set sepe " "
    exec  sdds2plaindata tmp0 tmp1 -outputMode=ascii -noRowCount -separator=$sepe \
        -column=x,format=%15.8e \
        -column=y,format=%15.8e \
        -column=dz,format=%15.8e \
        -column=xp,format=%15.8e \
        -column=yp,format=%15.8e \
        -column=E,format=%15.8e  \
        -column=t,format=%15.8e  

    
    exec octave --path "../files" --eval "slice_calc('tmp1', 'tmp0',$nslice,$q0,$minpart)" >&@stdout
   
   set lineno [lindex  [exec wc -l tmp0] 0]

  
 set fou [open tmp1 w]
  
 puts $fou "SDDS1
&parameter name=Description, format_string=\%s, type=string, &end
&parameter name=pCentral, symbol=\"p\$bcen\$n\", units=\"m\$be\$nc\", description=\"Reference beta*gamma\", type=double, &end
&parameter name=Charge,   symbol=\"Q\" description=\"Totla charge\", type=double, &end
&column name=t, units=fs, type=double, symbol=\"\$gs\$r\$bt\$n\",  &end
&column name=s, units=mm, type=double, symbol=\"\$gs\$r\$bz\$n\",  &end
&column name=E, units=GeV, type=double, symbol=\"E\",  &end
&column name=dE, units=MeV, type=double, symbol=\"energy spread\",  &end
&column name=gamma, units=\"m\$be\$nc\$u2\", type=double, symbol=\"\$gg\$r\",  &end
&column name=dgamma, units=\"m\$be\$nc\$u2\", type=double, symbol=\"d\$gg\$r\",  &end
&column name=Current, units=kA, type=double, symbol=\"I\",  &end
&column name=x, units=\"\$gm\$rm\", type=double, symbol=\"<x>\",  &end
&column name=y, units=\"\$gm\$rm\", type=double, symbol=\"<y>\",  &end
&column name=Sx, units=\"\$gm\$rm\", type=double, symbol=\"\$gs\$r\$bx\$n\",  &end
&column name=Sy, units=\"\$gm\$rm\", type=double, symbol=\"\$gs\$r\$by\$n\",  &end
&column name=enx, units=mm.mrad, type=double, symbol=\"\$ge\$r\$bnx\$n\",  &end
&column name=eny, units=mm.mrad, type=double, symbol=\"\$ge\$r\$bny\$n\",  &end
&column name=betax, units=m, type=double, symbol=\"\$gb\$r\$bx\$n\",  &end
&column name=betay, units=m, type=double, symbol=\"\$gb\$r\$by\$n\",  &end
&column name=alphax,  type=double, symbol=\"\$ga\$r\$bx\$n\",  &end
&column name=alphay,   type=double, symbol=\"\$ga\$r\$by\$n\",  &end
&data mode=ascii, &end
! page number 1
$fl"
puts $fou [format "%15.8e" $p0]
puts $fou [format "%15.8e" $q0]  
puts $fou [format " %18d" $lineno]

  set fin [open tmp0 r]
#   set inc 0
  while {[gets $fin line] >= 0}  {
#      incr inc
    set t0 [lindex $line 0]
    set z0 [lindex $line 1]
    set e0 [lindex $line 2]
    set de [lindex $line 3]
    set g0 [lindex $line 4]
    set dg [lindex $line 5]    
    set i0 [lindex $line 6]
    set x0 [lindex $line 7]
    set y0 [lindex $line 8]
    set sx [lindex $line 9]
    set sy [lindex $line 10]
    set enx [lindex $line 11]
    set eny [lindex $line 12]
    set bx [lindex $line 13]
    set by [lindex $line 14]
    set ax [lindex $line 15]
    set ay [lindex $line 16]
#     puts $inc
    
# puts " $t0 $z0 $e0 $de $g0 $dg $i0 $x0 $y0 $sx $sy $enx $eny $bx $by $ax $ay "
puts $fou [format "%15.8e %15.8e %15.8e %15.8e %15.8e %15.8e %15.8e %15.8e %15.8e %15.8e %15.8e %15.8e %15.8e %15.8e %15.8e %15.8e %15.8e" \
        $t0 $z0 $e0 $de $g0 $dg $i0 $x0 $y0 $sx $sy $enx $eny $bx $by $ax $ay ]
  }
  close $fou
  close $fin
#   
exec sddsconvert tmp1 $fout -binary 
exec cp tmp0 $flname.slc.dat
exec rm tmp0 tmp1

 }
 
 
proc plot_slice { {fl} {disp 1} } {
     
       set flname [file rootname $fl]
     
 

         set posx  "x  1e3 * "
         set posy  "y  1e3 * "


         
        if {$disp > 0} {
           exec sddsplot -thick=2 -graph=line,vary,thick=2  -layout=2,2,limit=4  -topline=$flname \
            -ticksettings=xgrid,ygrid,xlinetype=13,ylinetype=13,xthickness=1,ythickness=1 -linetypedefault=0,thickness=1.5 \
            -col=s,en?   -sep=2   $fl -legend   \
            -col=s,(x,y)   -sep=2  $fl -legend   \
            -col=s,dgamma  -sep=1  $fl -legend   \
            -col=s,Current -sep=1  $fl -legend   &
            
        } else {
           exec sddsplot -thick=2 -graph=line,vary,thick=2 -layout=2,2,limit=4  -topline=$flname \
            -ticksettings=xgrid,ygrid,xlinetype=13,ylinetype=13,xthickness=1,ythickness=1 -linetypedefault=0,thickness=1.5 \
            -col=s,en?   -sep=2  $fl -legend   \
            -col=s,(x,y)   -sep=2 $fl -legend   \
            -col=s,dgamma  -sep=1 $fl -legend   \
            -col=s,Current -sep=1  $fl -legend   \
            -device=gpng,onwhite -output=$flname.png &
            
        }
    
 }
 

 proc plottwiss { {fl} {disp 1} } {
 
    modifytwiss $fl
    
     set flname [file rootname $fl]
     set mg $flname.mag

    if {$disp > 0} {
    exec sddsplot   -thick=2 -graph=line,vary,thick=2 -topline=$flname \
                    -column=s,beta? -yscale=id=1 $fl -leg \
                    -column=s,E -yscale=id=2 $fl -legend \
                    -column=s,Profile -overlay=xmode=norm,yfact=0.04 -graph=line,var $mg  &
            
#     exec sddsplot   -thick=2 -graph=line,vary,thick=2 -title=$flname  \
#                     -column=s,(etay,etax) -yscale=id=1 $fl -legend \
#                     -column=s,(etayp,etaxp) -yscale=id=2 $fl -legend \
#                     -column=s,Profile -overlay=xmode=norm,yfact=0.04 -graph=line,var $mg -nolabel &
    } else {
        exec sddsplot   -thick=4 -graph=line,vary,thick=2 \
                    -column=s,beta? -yscale=id=1 $fl -leg \
                    -column=s,E -yscale=id=2 $fl -legend \
                    -column=s,Profile -overlay=xmode=norm,yfact=0.04 -graph=line,var $mg  \
                    -device=gpng,onwhite  -output=$fl.twi.png  
            
        exec sddsplot   -thick=4 -graph=line,vary,thick=2 \
                    -column=s,(etay,etax) -yscale=id=1 $fl -legend \
                    -column=s,(etayp,etaxp) -yscale=id=2 $fl -legend \
                    -column=s,Profile -overlay=xmode=norm,yfact=0.04 -graph=line,var $mg  \
                    -device=gpng,onwhite -output=$fl.eta.png  &
    }

}


 proc plotr56 { {fl} {disp 1} } {
 
    
     set flname [file rootname $fl]
     set mg $flname.mag
     set fl $flname.mat
    if {$disp > 0} {
    exec sddsplot   -thick=2 -graph=line,vary,thick=2 -topline=$flname \
                    -column=s,R56 -yscale=id=1 $fl -leg \
                    -column=s,T566 -yscale=id=2 $fl -legend \
                    -column=s,Profile -overlay=xmode=norm,yfact=0.04 -graph=line,var $mg  &
            
    } else {
    exec sddsplot   -thick=2 -graph=line,vary,thick=2 -topline=$flname \
                    -column=s,R56 -yscale=id=1 $fl -leg \
                    -column=s,T566 -yscale=id=2 $fl -legend \
                    -column=s,Profile -overlay=xmode=norm,yfact=0.04 -graph=line,var $mg \
                    -device=gpng,onwhite -output=$fl.r56.png  &
    }

    
    set r56l  [lindex [exec sddsprocess $fl -pipe=out -process=R56,last,R56l -nowarning | sddsprintout -pipe -par=R56l,format=\%.15e  -notitle -nolabel ] 0]
    set t566l [lindex [exec sddsprocess $fl -pipe=out -process=T566,last,T566l -nowarning | sddsprintout -pipe -par=T566l,format=\%.15e  -notitle -nolabel] 0]

    
    puts "
    [color 4 $fl ]"
    puts "[color 2 R56=] $r56l m      [color 2 T566=] $t566l  m " 
}
 
 proc plotsigma { {fl} {disp 1} } {
     set flname [file rootname $fl]
     set cl $flname.cent
     set mg $flname.mag
    if {$disp > 0} {
        exec sddsplot -thick=2 -graph=line,vary,thick=2 -topline=$flname \
              -column=s,(Sx,Sy) -yscale=id=1 $fl -legend \
              -column=s,(enx,eny) -yscale=id=2 $fl -legend \
              -column=s,Ss -yscale=id=3 $fl -legend \
              -column=s,Profile -overlay=xmode=norm,yfact=0.04 -graph=line,var $mg  &
    } else {
        exec sddsplot -thick=2 -graph=line,vary,thick=2 -topline=$flname \
              -column=s,(Sx,Sy) -yscale=id=1 $fl -legend \
              -column=s,(enx,eny) -yscale=id=2 $fl -legend \
              -column=s,Ss -yscale=id=3 $fl -legend \
              -column=s,Profile -overlay=xmode=norm,yfact=0.04 -graph=line,var $mg  \
             -device=lpng,onwhite -output=$flname.sigma.png  &
        
    }
 }

 
 
 proc plottwiss2 { {fl} {disp 1} } {
     set flname [file rootname $fl]
     set cl $flname.cent
     set mg $flname.mag
    if {$disp > 0} {
        exec sddsplot -thick=2 -graph=line,vary,thick=2 -topline=$flname \
              -column=s,(betaxBeam,betayBeam) -yscale=id=1 $fl -legend \
              -column=s,pCentral -yscale=id=2 $cl -legend \
              -column=s,Profile -overlay=xmode=norm,yfact=0.04 -graph=line,var $mg  &
    } else {
        exec sddsplot -thick=2 -graph=line,vary,thick=2 -topline=$flname \
              -column=s,(betaxBeam,betayBeam) -yscale=id=1 $fl -legend \
              -column=s,pCentral -yscale=id=2 $cl -legend \
              -column=s,Profile -overlay=xmode=norm,yfact=0.04 -graph=line,var $mg  \
             -device=lpng,onwhite -output=$flname.sigma.png  &
        
    }
 }
 
 proc  elegant2plot { indist } {
     
  set flname [file rootname $indist]
  set plotfile $flname\_all.png

  set pref  [exec sddsprocess $indist -pipe=out -process=p,average,pref -nowarning | sddsprintout -pipe -par=pref,format=\%.15e  -notitle -nolabel ]
  set tref  [exec sddsprocess $indist -pipe=out -process=t,average,tref -nowarning | sddsprintout -pipe -par=tref,format=\%.15e  -notitle -nolabel ]
  
  set qbunch [exec sddsprintout $indist -par=Charge,format=\%.15e  -notitle -noWarnings -nolabel ]

  puts $pref
  puts $tref


  set mc2 0.51099891e-3  ;# GeV/c^2
  set c   299792458e6      ;# um
  set toum 1e-6

  set t0 $tref
  set p0 $pref
  
  set t2z    "t $t0 - 1e15 *  " ;# fs
  set p2e    "p $mc2 * "      ;# GeV
  set x2x    "x  1e3 * "    ;# mm
  set y2y    "y  1e3 * "    ;# mm
  set xp2xp  "xp  1e3 * "   ;# mrad
  set yp2yp  "yp  1e3 * "   ;# mrad
  

exec sddsprocess -define=col,dtt,$t2z,units=ps  -define=col,E2,$p2e,units=GeV \
		 -redefine=col,x,$x2x,units=mm  -redefine=col,xp,$xp2xp,units=mrad  \
		 -redefine=col,y,$y2y,units=mm  -redefine=col,yp,$yp2yp,units=mrad  \
					  $indist $indist.tmp


set sepe " "

exec  sdds2plaindata $indist.tmp $indist.tmp2 -outputMode=ascii -noRowCount -separator=$sepe \
    -column=x,format=%18.12e \
    -column=xp,format=%18.12e \
    -column=y,format=%18.12e \
    -column=yp,format=%18.12e \
    -column=dtt,format=%18.12e \
    -column=E2,format=%18.12e 
    
    
     exec ../files/elegant_phase_plot_current $indist.tmp2 $plotfile [expr $qbunch*1e12]  >&@stdout
    
    exec rm  $indist.tmp $indist.tmp2
    
    
}

