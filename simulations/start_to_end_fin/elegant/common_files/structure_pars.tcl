
# *****************************************************************************************
# structure parameters
# *****************************************************************************************

set clight  299792458.0
set pi [expr acos(-1.)]


# sband deflecting structure
set sdfreq      2.997e9
set sdcell_l    [format " %.6e " [expr $clight/$sdfreq/3.0] ]
set sdbore_r    12e-3
set sddia       [format " %.6e " 44e-3 ]
set sdgap       [format " %.6e " [expr $sdcell_l - 5e-3]]
set sdno_of_cel 16
set sdlambda    [format " %.6e " [expr  $clight/$sdfreq]]
set sdactv_l    [format " %.6e " [expr  $sdno_of_cel*$sdcell_l ]]
set sdtot_l     [format " %.6e " [expr  0.7]]
set sdedge_l    [format " %.6e " [expr  0.5*($sdtot_l-$sdactv_l)]]
set sdgrad      1e6
set sdvolt      [expr $sdactv_l*$sdgrad]
set sdtrwkfact  1.0
set sdlnwkfact  1.0

array set sdband_par   "freq $sdfreq aper  $sdbore_r gap $sdgap cell_l $sdcell_l grad $sdgrad volt $sdvolt "
array set sdband_par   "dia  $sddia n_cell $sdno_of_cel active_l $sdactv_l edge_l $sdedge_l"
array set sdband_par   "total_l $sdtot_l lambda $sdlambda wakefile sdwake.sdds trwakefact $sdtrwkfact lnwakefact $sdlnwkfact"

# puts [array get sdband_par]


# cband structure
set cfreq      5.9971e9
set ccell_l    [format " %.6e " [expr $clight/$cfreq/3.0] ]
set cbore_r    6.3e-3
set cdia       [format " %.6e " 22.194e-3 ]
set cgap       [format " %.6e " [expr $ccell_l - 2.5e-3]]
set cno_of_cel 114
set clambda    [format " %.6e " [expr  $clight/$cfreq]]
set cactv_l    [format " %.6e " [expr  $cno_of_cel*$ccell_l ]]
set ctot_l     [format " %.6e " [expr  2.0]]
set cedge_l    [format " %.6e " [expr  0.5*($ctot_l-$cactv_l)]]
set cgrad      40e6
set cvolt      [expr $cactv_l*$cgrad]
set ctrwkfact  1.0
set clnwkfact  1.0

array set cband_par   "freq $cfreq aper  $cbore_r gap $cgap cell_l $ccell_l grad $cgrad volt $cvolt "
array set cband_par   "dia  $cdia n_cell $cno_of_cel active_l $cactv_l edge_l $cedge_l"
array set cband_par   "total_l $ctot_l lambda $clambda wakefile cwake.sdds trwakefact $ctrwkfact lnwakefact $clnwkfact"




# xband structure
set xfreq      11.9942e9
set xcell_l    [format " %.6e " [expr $clight/$xfreq/3.0] ]
set xbore_r    3.5e-3
set xdia       [format " %.6e " 10.139e-3 ]
set xgap       [format " %.6e " [expr $xcell_l - 2.0e-3]]
set xno_of_cel 110
set xlambda    [format " %.6e " [expr  $clight/$xfreq]]
set xactv_l    [format " %.6e " [expr  $xno_of_cel*$xcell_l ]]
set xtot_l     [format " %.6e " [expr  1.03]]
set xedge_l    [format " %.6e " [expr  0.5*($xtot_l-$xactv_l)]]
set xgrad      65e6
set xvolt      [expr $xactv_l*$xgrad]
set xtrwkfact  1.0
set xlnwkfact  1.0

array set xband_par   "freq $xfreq aper $xbore_r gap $xgap cell_l $xcell_l grad $xgrad volt $xvolt "
array set xband_par   "dia $xdia n_cell $xno_of_cel active_l $xactv_l edge_l $xedge_l"
array set xband_par   "total_l $xtot_l lambda $xlambda wakefile xwake.sdds trwakefact $xtrwkfact lnwakefact $xlnwkfact"


# kband structure
set kfreq       [expr 3.0*$xfreq]
set kcell_l     [format " %.6e " [expr $clight/$kfreq/3.0]]
set kbore_r     2.0e-3
set kdia        [format " %.6e " [expr $xdia/3.0]]
set kgap        [format " %.6e " [expr $kcell_l - 0.6e-3]]
set kno_of_cel  110
set klambda     [format " %.6e " [expr  $clight/$kfreq]]
set kactv_l     [format " %.6e " [expr  $kno_of_cel*$kcell_l ]]
set ktot_l      [format " %.6e " [expr  0.35]]
set kedge_l     [format " %.6e " [expr  0.5*($ktot_l-$kactv_l)]]
set kgrad       15e6
set kvolt       [expr $kactv_l*$kgrad]
set ktrwkfact  1.0
set klnwkfact  1.0

array set kband_par   "freq $kfreq aper $kbore_r gap $kgap cell_l $kcell_l grad $kgrad volt $kvolt "
array set kband_par   "dia $kdia n_cell $kno_of_cel active_l $kactv_l edge_l $kedge_l"
array set kband_par   "total_l $ktot_l lambda $klambda wakefile kwake.sdds trwakefact $ktrwkfact lnwakefact $klnwkfact"

# puts [array get kband_par]


# xband deflecting structure
set xdfreq      11.9942e9
set xdcell_l    [format " %.6e " [expr $clight/$xdfreq/3.0] ]
set xdbore_r    4.0e-3
set xddia       [format " %.6e " 10.0e-3 ]
set xdgap       [format " %.6e " [expr $xdcell_l - 2.6e-3]]
set xdno_of_cel 120
set xdlambda    [format " %.6e " [expr  $clight/$xdfreq]]
set xdactv_l    [format " %.6e " [expr  $xdno_of_cel*$xdcell_l ]]
set xdtot_l     [format " %.6e " [expr  1.2]]
set xdedge_l    [format " %.6e " [expr  0.5*($xdtot_l-$xdactv_l)]]
set xdgrad      20e6
set xdvolt      [expr $xactv_l*$xgrad]
set xdtrwkfact  1.0
set xdlnwkfact  1.0

array set xdband_par   "freq $xdfreq aper $xdbore_r gap $xdgap cell_l $xdcell_l grad $xdgrad volt $xdvolt "
array set xdband_par   "dia $xddia n_cell $xdno_of_cel active_l $xdactv_l edge_l $xdedge_l"
array set xdband_par   "total_l $xdtot_l lambda $xdlambda wakefile xdwake.sdds trwakefact $xdtrwkfact lnwakefact $xdlnwkfact"



