function slice_calc(filein, fileout,nslice,charge,minpart) 

%     source(["slice_analysis.m"]);
    
   B=load(filein);
   [zs,id]=sort(B(:,2));
   B=B(id,:);
   [nl,nc]=size(B);
   qpp=charge/nl;
   minz=min(B(:,3));
   maxz=max(B(:,3));
   dz=(maxz-minz)/nslice;
   dt=dz/299792458e3;
   zstp  =linspace(minz-dz*0.5,maxz+dz*0.5,nslice);
   m0c2=0.51099893;
   e00=mean(B(:,6));
   g00=e00/m0c2+1;
   
   
   
    %----------------------------------------
    % calculates the variance of a vector
    function suu=var1d(u,um) 
%          um=mean(u);
        nl=length(u);
        if (nl > 0)
            suu=sum((u-um).^2)/length(u);
        else 
            suu=0;
        end
    endfunction


    %----------------------------------------
    % calculates the variance of two vector
    function suv=var2d(u,um,v,vm) 
        lu=length(u);
        if (lu > 0)
            suv=sum((u-um).*(v-vm))/lu;
        else 
            suv=0;
        end
    end
    
    function NewSignal = smoothing(signal)
        NewSignal = signal;
        for i = 2 : length(signal)-1
            NewSignal(i,:) = (NewSignal(i,:)+NewSignal(i-1,:)+NewSignal(i+1,:))./3;
        end
%          for i = 3 : length(signal)-2
%              NewSignal(i,:) = (NewSignal(i,:)+NewSignal(i-1,:)+NewSignal(i-2,:)+NewSignal(i+1,:)+NewSignal(i+2,:))./5;
%          end
    end
   
   result=[];
   
   for j=1:nslice-1
       zst=zstp(j);
       zed=zstp(j+1);
       Bst=B(B(:,3)>=zst & B(:,3)<=zed,:);
       [nl,nc]=size(Bst);
        
       if (nl > minpart)
           xj=Bst(:,1);
           yj=Bst(:,2);
           zj=Bst(:,3);
           xpj=Bst(:,4);
           ypj=Bst(:,5);
           ej=Bst(:,6);
           tj=Bst(:,7);
        
           i0j=qpp*nl/dt*1e-3;         
           x0j=mean(xj);
           y0j=mean(yj);
           z0j=(zst+zed)*0.5;
           xp0j=mean(xpj);
           yp0j=mean(ypj);
           e0j=mean(ej);
           g0j=e0j/m0c2+1;
           t0j=z0j/299792458*-1e15; 
        
           sxx=var1d(xj,x0j);
           sx=sqrt(sxx);
           sxpxp=var1d(xpj,xp0j);
           sxxp=var2d(xj,x0j,xpj,xp0j);
           epsx=sqrt(sxx*sxpxp-sxxp*sxxp);
           bx=sxx/epsx;
           ax=-sxxp/epsx;
           epsnx=epsx*g0j;
        
           syy=var1d(yj,y0j);
           sy=sqrt(syy);
           sypyp=var1d(ypj,yp0j);
           syyp=var2d(yj,y0j,ypj,yp0j);
           epsy=sqrt(syy*sypyp-syyp*syyp);
           by=syy/epsy;
           ay=-syyp/epsy;
           epsny=epsy*g0j;
        
           dg=g0j-g00;
           de=e0j-e00;
        
           result=[result; t0j,z0j,e0j*1e-3,de,g0j,dg,i0j,x0j*1e3,y0j*1e3,sx*1e3,sy*1e3,epsnx,epsny,bx,by,ax,ay];
       else
           disp("not enough particle to compute slice")
       end
   end
%     size(result)
%         for k=3:17;
%              result(:,k)=smoothing(result(:,k));
%         end
%         result(:,8)=smoothing(result(:,8));
    
   save("-text", "-ascii",fileout,"result");
   
end 
