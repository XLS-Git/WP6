set filename xls_linac_hxrbp_hxr
set indist_read  cband_out_4ps_0.18mm_100k_120MeV.sdds
# set indist_s2e  cband_out_4ps_0.18mm_1M_120MeV.sdds
set indist_s2e  cband_out_4ps_0.18mm_100k_120MeV.sdds


set indist_name $indist_read

lassign [exec sddsanalyzebeam $script_dir2/$indist_read  -pipe=out | sddsprintout -pipe -col=pAverage,format=\%.6e \
        -col=beta?,format=\%.6e -col=alpha?,format=\%.6e -col=en?,format=\%.6e -col=St,format=\%.6e \
        -col=Sdelta,format=\%.6e  \
        -not -nol] p0 btx bty alx aly emtx emty st de 


set flange 0.025
set belowl 0.05
set vacpor 0.1
set viewsc 0.1
set quad_a 0.02

set csr_flag 1
set isr_flag 1

set zwake_flag 1
set twake_flag 1 

# the twiss parameters at by pass locations
lassign {0.33953 10.791 0.8569 3.696} btxs btys alpxs alpys
lassign {1.1547  12.8882 1.6341 4.3846} btxh btyh alpxh alpyh






# 
# linac 0
# ----------------------------------------------------
set ln0_cvolt [expr $cband_par(active_l)*15.0e6]
set ln0_cphs  26

set ln0_xvolt [expr $xband_par(active_l)*30.0e6]
set ln0_xphs  [expr 18.5]


set kband_grad 24.e6
set k_phase_t  12.0

set ln0_kphs  [expr 180+$k_phase_t]
set ln0_kvolt [expr $kband_grad*$kband_par(active_l)/cos($pi*$k_phase_t/180)]


set ln0_quad_l 0.08
set ln0_quad_edge [expr (0.165-$ln0_quad_l)*0.5]
set ln0_quad_k 7.0
set ln0_s_dipole 0.1
set ln0_angle_rd [format "%10.8f" [expr $pi/180.*5.0]]
set use_wiggler 1
set laser_power 0.5e6
set laser_wavel 532e-9
set und_fld    0.4
set flag_scatt 0.0   ;# 1e-4 Flag for LH SCATTER element (1e-4 is 10keV rms at 100MeV). Use alternative to UND_LH

set f0_csr_ln0 0.2
set f1_csr_ln0 0.25


# ln0 diagnostic 
# ----------------------------------------------------
set ln0_xvolt_di [expr $xdband_par(active_l)*10e6*0]
set ln0_xphs_di 0.0
set ln0_xtilt_di [expr $pi/2]
set ln0_xkick_di 21
# set ln0_di_dp_angle [format "%10.8f" [expr 0.0*$pi/180.0]]


# BC1
# ----------------------------------------------------
set bc1_l_dip    0.4
set bc1_d_cent   0.7
set bc1_angle    3.83
set bc1_d_side   3.25
set bc1_angle_rd 	[format "%10.8f" [expr $pi/180.*$bc1_angle]]
set bc1_s_dipole 	[format "%10.8f" [expr $bc1_angle_rd*$bc1_l_dip/sin($bc1_angle_rd)]]
set bc1_d_longed	[format "%10.8f" [expr $bc1_d_side/cos($bc1_angle_rd)]]
set bc1_angle_zr	[format "%10.8f" 0.]
set f0_csr_bc1 0.2
set f1_csr_bc1 0.25




# bc1 diagnostic 
# ----------------------------------------------------
set bc1_dia_quad_k1 8.0
set bc1_dia_fodol   0.15
set bc1_xvolt [expr $xdband_par(active_l)*10e6*0]
set bc1_xphs 0.0
set bc1_xtilt [expr $pi/2]
set bc1_xkick 21
set bc1_di_dp_angle [format "%10.8f" [expr 0.0*$pi/180.0]]


# linac 1
# ----------------------------------------------------
set ln1_nmodule 4
set ln1_xvolt [expr $xband_par(active_l)*65e6]
set ln1_xphs 35
set ln1_quad_edge $ln0_quad_edge
set ln1_quad_l $ln0_quad_l
set ln1_quad_k 6.6



# BC2
# ----------------------------------------------------
set bc2_l_dip    0.6
set bc2_d_cent   0.5
set bc2_angle    1.375
set bc2_d_side   3.7
set bc2_angle_rd 	[format "%10.8f" [expr $pi/180.*$bc2_angle]]
set bc2_s_dipole 	[format "%10.8f" [expr $bc2_angle_rd*$bc2_l_dip/sin($bc2_angle_rd)]]
set bc2_d_longed	[format "%10.8f" [expr $bc2_d_side/cos($bc2_angle_rd)]]
set bc2_angle_zr	[format "%10.8f" 0.]
set f0_csr_bc2 0.2
set f1_csr_bc2 0.25




# ln2 diagnostic 
# ----------------------------------------------------
set ln2_dia_quad_k1 8.0
set ln2_dia_fodol   0.15
set ln2_di_xvolt [expr $xdband_par(active_l)*10e6*0]
set ln2_di_xphs 0.0
set ln2_di_xtilt [expr $pi/2]
set ln2_di_xkick 21
set ln2_di_dp_angle [format "%10.8f" [expr 0.0*$pi/180.0]]


# linac 2
# ----------------------------------------------------
set ln2_nmodule 2
set ln2_xvolt [expr $xband_par(active_l)*65e6]
set ln2_xphs 10
set ln2_quad_edge $ln0_quad_edge
set ln2_quad_l $ln0_quad_l
set ln2_quad_k 6.7


# trhough SEPTUM
# ----------------------------------------------------
set ln2_sdefvolt [expr 2.45e6*0]
set ln2_sdefvolt 0.0
set ln2_sdefphs 0.0
set ln2_sdefkicks 0
set ln2_sdeftitl 0.0
set ln2_swi_trip_dir    0.4



# sxr bypass line 
# ----------------------------------------------------
set sbp_quad_edge $ln0_quad_edge
set sbp_quad_l    $ln0_quad_l
set sbp_quad_k 6.0
set f0_csr_sxrbp 0.2
set f1_csr_sxrbp 0.25
set sbp_swi_trip_dir    0.3
set sbp_swi_l_dip       0.25
set sbp_swi_angle       4.0
set sbp_ksext 50
set sbp_swi_angle_rd 	[format "%10.8f" [expr $pi/180.*$sbp_swi_angle]]
set sbp_swi_s_dipole 	[format "%10.8f" [expr $sbp_swi_angle_rd*$sbp_swi_l_dip/sin($sbp_swi_angle_rd)]]

set pc [expr $p0*0.511*1e-3]

set rho [expr $sbp_swi_l_dip*0.5/sin($sbp_swi_angle_rd*0.5)]
set bfl [expr $pc/0.3/$rho]
puts "  angle =$sbp_swi_angle_rd $pi"
puts "  rho =$rho  B field $bfl"



# linac 3
# ----------------------------------------------------
set ln3_nmodule 8
set ln3_xvolt [expr $xband_par(active_l)*65e6]
set ln3_xphs $ln2_xphs
set ln3_quad_edge $ln0_quad_edge
set ln3_quad_l $ln0_quad_l
set ln3_quad_k 4.20


set ln3_sdefvolt 1e6
set ln3_sdefphs 0.0
set ln3_sdefkicks 21
set ln3_sdeftitl 0.0


# linac 4
set ln4_nstr 2
set ln4_nmodule 2
set ln4_xvolt [expr $xband_par(active_l)*65e6]
set ln4_xphs 10.0
set ln4_quad_edge 0.05
set ln4_quad_l 0.08
set ln4_quad_k 6.6
puts "integrated strength of lin4quad is B=[expr $ln4_quad_k*1.5*$quad_a/0.3] \[T\]\n"
# set ln4_btwn_qds [expr $ln4_nstr*($xband_par(total_l)+2.0*$flange)+2*$ln2_quad_edge+$vacpor]




#### trhough SEPTUM after LN3
set ln3_sdefvolt [expr 2.45e6*0]
set ln3_sdefvolt 0.0
set ln3_sdefphs 0.0
set ln3_sdefkicks 0
set ln3_sdeftitl 0.0
set ln3_swi_trip_dir    0.4

# hxr bypass line 
set hbp_swi_trip_dir    0.3
set hbp_swi_l_dip       0.64
set hbp_swi_angle       4.0
set hbp_swi_angle_rd 	[format "%10.8f" [expr $pi/180.*$hbp_swi_angle]]
set hbp_swi_s_dipole 	[format "%10.8f" [expr $hbp_swi_angle_rd*$hbp_swi_l_dip/sin($hbp_swi_angle_rd)]]

set pc [expr $p0*0.511*1e-3]
set ksext 0
set rho [expr $hbp_swi_l_dip*0.5/sin($hbp_swi_angle_rd*0.5)]
set bfl [expr $pc/0.3/$rho]
puts "  angle =$hbp_swi_angle_rd $pi"
puts "  rho =$rho  B field $bfl"


set hbp_quad_edge $ln0_quad_edge
set hbp_quad_l    $ln0_quad_l
set hbp_quad_k0 5.0
set hbp_quad_k 6.0
set f0_csr_hxrbp 0.2
set f1_csr_hxrbp 0.25

# Timing chicane
set tmc_quad_edge $ln0_quad_edge
set tmc_quad_l $ln0_quad_l
set tmc_quad_k 7.0
set tmc_l_dip    0.5
set tmc_d_cent   1.0
set tmc_angle    0.05
set tmc_d_side   3.5
set tmc_angle_rd 	[format "%10.8f" [expr $pi/180.*$tmc_angle]]
set tmc_s_dipole 	[format "%10.8f" [expr $tmc_angle_rd*$tmc_l_dip/sin($tmc_angle_rd)]]
set tmc_d_longed	[format "%10.8f" [expr $tmc_d_side/cos($tmc_angle_rd)]]
set tmc_angle_zr	[format "%10.8f" 0.]
set f0_csr_tmc 0.25
set f1_csr_tmc 0.33




#### hard xray BL

set hxr_pl  0.013
set hxr_quad_edge [expr $hxr_pl*4.0]
set hxr_quad_l [expr $hxr_pl*2.0]
set hxr_quad_k 15.0
set hxr_np  136
set hxr_wiged [expr $hxr_pl*15.0]
set hxr_nmodule 1













# after 1000

# 
# lassign [exec sddsanalyzebeam $indist_name  -pipe=out | sddsprintout -pipe -col=pAverage,format=\%.6e \
#         -col=beta?,format=\%.6e -col=alpha?,format=\%.6e -col=en?,format=\%.6e -col=St,format=\%.6e \
#         -col=Sdelta,format=\%.6e  \
#         -not -nol] p01 btx1 bty1 alx1 aly1 emtx1 emty1 st1 de1 
# 
#         puts "$p0 $p01"
#         puts "$btx $btx1"
#         puts "$btx $btx1"
#         puts "$bty $bty1"
#         puts "$alx $alx1"
#         puts "$aly $aly1" 
#         puts "$st  $st1"
#         puts "$de  $de1"
