SDDS1
!# little-endian
&description text="sigma matrix--input: xls_linac_hxrbp_hxr_bc1.track.ele  lattice: xls_linac_hxrbp_hxr.2.lte", contents="sigma matrix", &end
&parameter name=Step, description="Simulation step", type=long, &end
&parameter name=SVNVersion, description="SVN version number", type=string, fixed_value=unknown, &end
&column name=s, units=m, description=Distance, type=double,  &end
&column name=ElementName, description="Element name", format_string=%10s, type=string,  &end
&column name=ElementOccurence, description="Occurence of element", format_string=%6ld, type=long,  &end
&column name=ElementType, description="Element-type name", format_string=%10s, type=string,  &end
&column name=s1, symbol="$gs$r$b1$n", units=m, description="sqrt(<x*x>)", type=double,  &end
&column name=s12, symbol="$gs$r$b12$n", units=m, description="<x*xp'>", type=double,  &end
&column name=s13, symbol="$gs$r$b13$n", units="m$a2$n", description="<x*y>", type=double,  &end
&column name=s14, symbol="$gs$r$b14$n", units=m, description="<x*y'>", type=double,  &end
&column name=s15, symbol="$gs$r$b15$n", units="m$a2$n", description="<x*s>", type=double,  &end
&column name=s16, symbol="$gs$r$b16$n", units=m, description="<x*delta>", type=double,  &end
&column name=s17, symbol="$gs$r$b17$n", units="m*s", description="<x*t>", type=double,  &end
&column name=s2, symbol="$gs$r$b2$n", description="sqrt(<x'*x'>)", type=double,  &end
&column name=s23, symbol="$gs$r$b23$n", units=m, description="<x'*y>", type=double,  &end
&column name=s24, symbol="$gs$r$b24$n", description="<x'*y'>", type=double,  &end
&column name=s25, symbol="$gs$r$b25$n", units=m, description="<x'*s>", type=double,  &end
&column name=s26, symbol="$gs$r$b26$n", description="<x'*delta>", type=double,  &end
&column name=s27, symbol="$gs$r$b27$n", units=s, description="<x'*t>", type=double,  &end
&column name=s3, symbol="$gs$r$b3$n", units=m, description="sqrt(<y*y>)", type=double,  &end
&column name=s34, symbol="$gs$r$b34$n", units=m, description="<y*y'>", type=double,  &end
&column name=s35, symbol="$gs$r$b35$n", units="m$a2$n", description="<y*s>", type=double,  &end
&column name=s36, symbol="$gs$r$b36$n", units=m, description="<y*delta>", type=double,  &end
&column name=s37, symbol="$gs$r$b37$n", units="m*s", description="<y*t>", type=double,  &end
&column name=s4, symbol="$gs$r$b4$n", description="sqrt(<y'*y')>", type=double,  &end
&column name=s45, symbol="$gs$r$b45$n", units=m, description="<y'*s>", type=double,  &end
&column name=s46, symbol="$gs$r$b46$n", description="<y'*delta>", type=double,  &end
&column name=s47, symbol="$gs$r$b47$n", units=s, description="<y'*t>", type=double,  &end
&column name=s5, symbol="$gs$r$b5$n", units=m, description="sqrt(<s*s>)", type=double,  &end
&column name=s56, symbol="$gs$r$b56$n", units=m, description="<s*delta>", type=double,  &end
&column name=s57, symbol="$gs$r$b57$n", units="m*s", description="<s*t>", type=double,  &end
&column name=s6, symbol="$gs$r$b6$n", description="sqrt(<delta*delta>)", type=double,  &end
&column name=s67, symbol="$gs$r$b67$n", units=s, description="<delta*t>", type=double,  &end
&column name=s7, symbol="$gs$r$b7$n", description="sqrt(<t*t>)", type=double,  &end
&column name=ma1, symbol="max$sb$ex$sb$e", units=m, description="maximum absolute value of x", type=double,  &end
&column name=ma2, symbol="max$sb$ex'$sb$e", description="maximum absolute value of x'", type=double,  &end
&column name=ma3, symbol="max$sb$ey$sb$e", units=m, description="maximum absolute value of y", type=double,  &end
&column name=ma4, symbol="max$sb$ey'$sb$e", description="maximum absolute value of y'", type=double,  &end
&column name=ma5, symbol="max$sb$e$gD$rs$sb$e", units=m, description="maximum absolute deviation of s", type=double,  &end
&column name=ma6, symbol="max$sb$e$gd$r$sb$e", description="maximum absolute value of delta", type=double,  &end
&column name=ma7, symbol="max$sb$e$gD$rt$sb$e", units=s, description="maximum absolute deviation of t", type=double,  &end
&column name=minimum1, symbol="x$bmin$n", units=m, type=double,  &end
&column name=minimum2, symbol="x'$bmin$n", units=m, type=double,  &end
&column name=minimum3, symbol="y$bmin$n", units=m, type=double,  &end
&column name=minimum4, symbol="y'$bmin$n", units=m, type=double,  &end
&column name=minimum5, symbol="$gD$rs$bmin$n", units=m, type=double,  &end
&column name=minimum6, symbol="$gd$r$bmin$n", units=m, type=double,  &end
&column name=minimum7, symbol="t$bmin$n", units=s, type=double,  &end
&column name=maximum1, symbol="x$bmax$n", units=m, type=double,  &end
&column name=maximum2, symbol="x'$bmax$n", units=m, type=double,  &end
&column name=maximum3, symbol="y$bmax$n", units=m, type=double,  &end
&column name=maximum4, symbol="y'$bmax$n", units=m, type=double,  &end
&column name=maximum5, symbol="$gD$rs$bmax$n", units=m, type=double,  &end
&column name=maximum6, symbol="$gd$r$bmax$n", units=m, type=double,  &end
&column name=maximum7, symbol="t$bmax$n", units=s, type=double,  &end
&column name=Sx, symbol="$gs$r$bx$n", units=m, description=sqrt(<(x-<x>)^2>), type=double,  &end
&column name=Sxp, symbol="$gs$r$bx'$n", description=sqrt(<(x'-<x'>)^2>), type=double,  &end
&column name=Sy, symbol="$gs$r$by$n", units=m, description=sqrt(<(y-<y>)^2>), type=double,  &end
&column name=Syp, symbol="$gs$r$by'$n", description=sqrt(<(y'-<y'>)^2>), type=double,  &end
&column name=Ss, symbol="$gs$r$bs$n", units=m, description=sqrt(<(s-<s>)^2>), type=double,  &end
&column name=Sdelta, symbol="$gs$bd$n$r", description=sqrt(<(delta-<delta>)^2>), type=double,  &end
&column name=St, symbol="$gs$r$bt$n", units=s, description=sqrt(<(t-<t>)^2>), type=double,  &end
&column name=ex, symbol="$ge$r$bx$n", units=m, description="geometric horizontal emittance", type=double,  &end
&column name=enx, symbol="$ge$r$bx,n$n", units=m, description="normalized horizontal emittance", type=double,  &end
&column name=ecx, symbol="$ge$r$bx,c$n", units=m, description="geometric horizontal emittance less dispersive contributions", type=double,  &end
&column name=ecnx, symbol="$ge$r$bx,cn$n", units=m, description="normalized horizontal emittance less dispersive contributions", type=double,  &end
&column name=ey, symbol="$ge$r$by$n", units=m, description="geometric vertical emittance", type=double,  &end
&column name=eny, symbol="$ge$r$by,n$n", units=m, description="normalized vertical emittance", type=double,  &end
&column name=ecy, symbol="$ge$r$by,c$n", units=m, description="geometric vertical emittance less dispersive contributions", type=double,  &end
&column name=ecny, symbol="$ge$r$by,cn$n", units=m, description="normalized vertical emittance less dispersive contributions", type=double,  &end
&column name=betaxBeam, symbol="$gb$r$bx,beam$n", units=m, description="betax for the beam, excluding dispersive contributions", type=double,  &end
&column name=alphaxBeam, symbol="$ga$r$bx,beam$n", description="alphax for the beam, excluding dispersive contributions", type=double,  &end
&column name=betayBeam, symbol="$gb$r$by,beam$n", units=m, description="betay for the beam, excluding dispersive contributions", type=double,  &end
&column name=alphayBeam, symbol="$ga$r$by,beam$n", description="alphay for the beam, excluding dispersive contributions", type=double,  &end
&data mode=binary, &end
.                 _BEG_      MARK0*FU_� ?$'��#��=sz*j>kٽ���Fxڳ=8`��
�>����U>BN��[E<I����6�>���G&��zy�Dj��{��Q|9�=����>B>�-K2<��^�� ?<��N���oՄas��c�/N�z�糦-��#��N�>D��,7-�=/�.��X�=����E��;}2����4?���/�?�>F/��9ط<
+�ԨĂ?���=T�ִtr=�����6?�W�R��?ǐ�w5(?�� ? ��q�wF?b$��? ��%�=\��e�5�JZix�ǐ�w5(�>{S�Gl� �B c�D����O𷒿 �k�o��������6?�W�R��?��+�(?�� ? ��q�wF?b$��? ��%�=0*FU_� ?I����6�>��^�� ?#��N�>}2����4?
+�ԨĂ?T�ִtr=-V���=xR��r��>
�}��=۳�zs��>�*��!�=��o�*��>�uN��!�=_�ʏ$��>L�x��g@�׆n���旙t@�-��%�?           BNCH      CHARGE0*FU_� ?$'��#��=sz*j>kٽ���Fxڳ=8`��
�>����U>BN��[E<I����6�>���G&��zy�Dj��{��Q|9�=����>B>�-K2<��^�� ?<��N���oՄas��c�/N�z�糦-��#��N�>D��,7-�=/�.��X�=����E��;}2����4?���/�?�>F/��9ط<
+�ԨĂ?���=T�ִtr=�����6?�W�R��?ǐ�w5(?�� ? ��q�wF?b$��? ��%�=\��e�5�JZix�ǐ�w5(�>{S�Gl� �B c�D����O𷒿 �k�o��������6?�W�R��?��+�(?�� ? ��q�wF?b$��? ��%�=0*FU_� ?I����6�>��^�� ?#��N�>}2����4?
+�ԨĂ?T�ִtr=-V���=xR��r��>
�}��=۳�zs��>�*��!�=��o�*��>�uN��!�=_�ʏ$��>L�x��g@�׆n���旙t@�-��%�?        
   BC1_IN_CNT      CENTER0*FU_� ?$'��#��=sz*j>kٽ���Fxڳ=8`��
�>����U>BN��[E<I����6�>���G&��zy�Dj��{��Q|9�=����>B>�-K2<��^�� ?<��N���oՄas��c�/N�z�糦-��#��N�>F��,7-�=/�.��X�=����E��;}2����4?���/�?�>F/��9ط<
+�ԨĂ?���=T�ִtr=Q7�ƪ�6?���+��?)�z5(?.f\�� ? ��q�wF?b$��? ��%�=�Jx���5�2򵐤 �)�z5(���t�l� �B c�D����O𷒿 �k�o���Q7�ƪ�6?���+��?��=�(?.f\�� ? ��q�wF?b$��? ��%�=0*FU_� ?I����6�>��^�� ?#��N�>}2����4?
+�ԨĂ?T�ִtr=-V���=xR��r��>
�}��=۳�zs��>�*��!�=��o�*��>�uN��!�=_�ʏ$��>L�x��g@�׆n���旙t@�-��%�?V(�2�8�?	   BC1_DR_V1      DRIF�!|L�(?$d��� >d�+$�ٽ���R���=c��Q_>=�H�vzZ>0�3C-7J<I����6�>ˬ�����zy�Dj��erf�{9�=����>B>��/�2<_pNpt�>9Lrtz�i�R7T�����?����g<���#��N�>�ؤ�:-�=/�.��X�=-`V>���;I�����4?�V!�?�><O�׷<
+�ԨĂ?Ļ�(��=��o��sr=��
��s9?���+��?`A�^B`$?.f\�� ? �$S�wF?b$��? ����=4���8�2򵐤 �`A�^B`$���t�l� �:c�D����O𷒿 ���*�����
��s9?���+��?]9V���"?.f\�� ? �$S�wF?b$��? ����=�!|L�(?I����6�>_pNpt�>#��N�>I�����4?
+�ԨĂ?��o��sr=*V���=uR��r��>�}��=س�zs��>�*��!�=��o�*��>�uN��!�=_�ʏ$��>��}@�83~ch����g�K@�c�nd��?��C(&��?   BC1_DR_QD_ED      DRIF6�	�m?5���PT >�����ڽ��'����=�6@W�>�k;C��Z>�F��W�J<I����6�>��n�-B��zy�Dj��K�{�{9�=����>B>�%DL2<�g:��>gH��B[�|�������m�H{]�6raI[�#��N�>�^�<;-�=/�.��X�=�����;ꌓ���4?�d3 �?�>Z4e�׷<
+�ԨĂ?�7a㸤=#����sr=����9?���+��?���$?.f\�� ? ��P�wF?b$��?  z���=���u9�2򵐤 ����$���t�l� �*<c�D����O𷒿  mU%�������9?���+��?�����B"?.f\�� ? ��P�wF?b$��?  z���=6�	�m?I����6�>�g:��>#��N�>ꌓ���4?
+�ԨĂ?#����sr=*V���=uR��r��>�}��=س�zs��>�*��!�=��o�*��>�uN��!�=_�ʏ$��>����@#w����=��@��@+֯�O��?���<���?
   BC1_QD_V01      QUAD�艈�?�:��m�=S�iU�"ڽB��L/�=[�U/)>����0[> ��I�J<W뺮��>��)���Y��ۛy��7�b2I�=��M�:<>���X�+<�q��9��>���L罒�Z��r	��������W]��>��YX��=�E�g�=1�$���;J$����4?�:�?�>*�`��׷<
+�ԨĂ?<�_���=�:��sr=�@q��9?
$��?�W��#?�c���j ? ��N�wF?b$��? ��9��=C
u�<@9�K�p���W��#�1�G �� �>c�D����O𷒿 �77 ����@q��9?
$��?s���� "?�c���j ? ��N�wF?b$��? ��9��=�艈�?W뺮��>�q��9��>�W]��>J$����4?
+�ԨĂ?�:��sr=�,L@��=2I����>�" ���=K��@���>�O�>!�=��<���>��A�8!�=�h��z��>ΰy��@im���*��3u�+�?@���G�Q�?���<���?   BC1_COR      KICKER�艈�?�:��m�=S�iU�"ڽB��L/�=[�U/)>����0[> ��I�J<W뺮��>��)���Y��ۛy��7�b2I�=��M�:<>���X�+<�q��9��>���L罒�Z��r	��������W]��>��YX��=�E�g�=1�$���;J$����4?�:�?�>*�`��׷<
+�ԨĂ?<�_���=�:��sr=�@q��9?
$��?�W��#?�c���j ? ��N�wF?b$��? ��9��=C
u�<@9�K�p���W��#�1�G �� �>c�D����O𷒿 �77 ����@q��9?
$��?s���� "?�c���j ? ��N�wF?b$��? ��9��=�艈�?W뺮��>�q��9��>�W]��>J$����4?
+�ԨĂ?�:��sr=�,L@��=2I����>�" ���=K��@���>�O�>!�=��<���>��A�8!�=�h��z��>ΰy��@im���*��3u�+�?@���G�Q�?���<���?   BC1_BPM      MONI�艈�?�:��m�=S�iU�"ڽB��L/�=[�U/)>����0[> ��I�J<W뺮��>��)���Y��ۛy��7�b2I�=��M�:<>���X�+<�q��9��>���L罒�Z��r	��������W]��>��YX��=�E�g�=1�$���;J$����4?�:�?�>*�`��׷<
+�ԨĂ?<�_���=�:��sr=�@q��9?
$��?�W��#?�c���j ? ��N�wF?b$��? ��9��=C
u�<@9�K�p���W��#�1�G �� �>c�D����O𷒿 �77 ����@q��9?
$��?s���� "?�c���j ? ��N�wF?b$��? ��9��=�艈�?W뺮��>�q��9��>�W]��>J$����4?
+�ԨĂ?�:��sr=�,L@��=2I����>�" ���=K��@���>�O�>!�=��<���>��A�8!�=�h��z��>ΰy��@im���*��3u�+�?@���G�Q�?Bz9Q�$�?
   BC1_QD_V01      QUAD�%�?�?��M�$�=�-�O8ڽ>ˣ�K�K=j��lqm>#M�-n[>��R+.(K<F�V���>U���꡽�)��^X����lE��=�k�3�3>��ݨ#<�݇�z�>�cw�O�߽��c�����p��oc����rpe�}����><���Q��=����#��=�����;s���4?��q�?�> _b�׷<
+�ԨĂ?�����=�U��sr=\GQ,�9?R|4?e���i�#?\֛��� ? �M�wF?b$��? �S���=�|d� _9����d��e���i�#�I�m�r�  �?c�D����O𷒿  ����\GQ,�9?R|4?1��y��!?\֛��� ? �M�wF?b$��? �S���=�%�?�?F�V���>�݇�z�>}����>s���4?
+�ԨĂ?�U��sr=� ��<	�=(RDc���>�X'���=�#!w���>��&� �=���>2u� �=IvÇ軂>���f+�@m �_	Q�tr$?�@a{'�O2�?�	�F���?   BC1_DR_QD_ED      DRIFT|�&7�?R�+Hb�=Hq��%Qڽ��B*�F�:3�m[�>`�eC�[>�����]K<F�V���>��f����)��^X�����B��=�k�3�3>
x~�ר#<�*�u�E�>��Z�sM޽G���������B��+{-���}����><:�R��=����#��=�����;�V���4?�Ա�?�>�x�׷<
+�ԨĂ?czWЦ�=0�;�sr=����9?R|4?m��3r#?\֛��� ? ��K�wF?b$��? ����=܅ u9����d��m��3r#�I�m�r� ��Ac�D����O𷒿 �ߨ�������9?R|4?_y)Yy�!?\֛��� ? ��K�wF?b$��? ����=T|�&7�?F�V���>�*�u�E�>}����>�V���4?
+�ԨĂ?0�;�sr=� ��<	�='RDc���>�X'���=�#!w���>��&� �=���>3u� �=JvÇ軂>��(\7K@�7�&I����o��@^S�پ�?Ʊ�4j@	   BC1_DR_V2      DRIFaI�T�?������	>l�/�m��	ˡ$�%˽�f����>�h?C_|l>����P*\<F�V���>i���$�Ͻ�)��^X��D;m���=�k�3�3>�,G���#<�5c/R�?YMr��>Uf�-M��=�?V���>��Ǻ�,<}����>�:Fz��=����#��=��.Y��;(�&��4?�Օ��?�>8YԷ<
+�ԨĂ?C�M@�=FJ��pr=_��ŉMD?R|4?*(r7AJ?\֛��� ?  �s�wF?b$��?   �
�=��ǟt�C����d��b<��V�F�I�m�r�  �Wd�D����O𷒿  ϲ"���_��ŉMD?R|4?*(r7AJ?\֛��� ?  �s�wF?b$��?   �
�=aI�T�?F�V���>�5c/R�?}����>(�&��4?
+�ԨĂ?FJ��pr=� ��<	�=-RDc���>�X'���=�#!w���>��&� �=���>1u� �=GvÇ軂>(蹈�o<@�4�U��bE��M�/@癛r����S��@   BC1_DR_QD_ED      DRIF�^�)��?��1�	>�`���\����~DvW˽����>B�K�i�l>���)E\<F�V���>���
�Ͻ�)��^X�������=�k�3�3>��溥#<��?��t}�>D$b��==�H	>��ؑf<}����>A<�z��=����#��=�Şy��;ԗc%��4?��մ�?�>�*Է<
+�ԨĂ?\��:�=,�5��pr=����^D?R|4?ל���nJ?\֛��� ?  r�wF?b$��?  ;L~
�=�^Ҭ��C����d��#?��0�F�I�m�r�  �Yd�D����O𷒿  �B�������^D?R|4?ל���nJ?\֛��� ?  r�wF?b$��?  ;L~
�=�^�)��?F�V���>��?}����>ԗc%��4?
+�ԨĂ?,�5��pr=� ��<	�=)RDc���>�X'���=�#!w���>��&� �=���>1u� �=GvÇ軂>NP�o�<@������c�j�K0@�����_0��@
   BC1_QD_V02      QUAD�Z�?A�Ǆ��.>��@b����}���(�=���� >%l�ȵ�l>���]�\< �� 1O?���@���_����=�+���>޴ڝ�
Z>NzDAF�I<��b�?M0+��,J�p*�=��%u>�)=Lr<A�#��>����ߙ��-!7���z�&׻��$��4?:�o��?�>n^v��ӷ<
+�ԨĂ?�g!4�=-J��pr=;�)�.�D?���1?t2%� iJ?����$%?  �m�wF?b$��?  ��x
�=I<��D�W���^�1�~Ŀ�F�F�����$%�  [d�D����O𷒿  �#���;�)�.�D?���1?t2%� iJ?�mǀ$?  �m�wF?b$��?  ��x
�=�Z�? �� 1O?��b�?A�#��>��$��4?
+�ԨĂ?-J��pr=�/���=!��$�r�>b�Q����=)�e�Eg�>`J���=���|�>YU��=@T��鸂>��y��=@�
c->[+�D�p��0@�ȟ�-	@�_0��@   BC1_COR      KICKER�Z�?A�Ǆ��.>��@b����}���(�=���� >%l�ȵ�l>���]�\< �� 1O?���@���_����=�+���>޴ڝ�
Z>NzDAF�I<��b�?M0+��,J�p*�=��%u>�)=Lr<A�#��>����ߙ��-!7���z�&׻��$��4?:�o��?�>n^v��ӷ<
+�ԨĂ?�g!4�=-J��pr=;�)�.�D?���1?t2%� iJ?����$%?  �m�wF?b$��?  ��x
�=I<��D�W���^�1�~Ŀ�F�F�����$%�  [d�D����O𷒿  �#���;�)�.�D?���1?t2%� iJ?�mǀ$?  �m�wF?b$��?  ��x
�=�Z�? �� 1O?��b�?A�#��>��$��4?
+�ԨĂ?-J��pr=�/���=!��$�r�>b�Q����=)�e�Eg�>`J���=���|�>YU��=@T��鸂>��y��=@�
c->[+�D�p��0@�ȟ�-	@�_0��@   BC1_BPM      MONI�Z�?A�Ǆ��.>��@b����}���(�=���� >%l�ȵ�l>���]�\< �� 1O?���@���_����=�+���>޴ڝ�
Z>NzDAF�I<��b�?M0+��,J�p*�=��%u>�)=Lr<A�#��>����ߙ��-!7���z�&׻��$��4?:�o��?�>n^v��ӷ<
+�ԨĂ?�g!4�=-J��pr=;�)�.�D?���1?t2%� iJ?����$%?  �m�wF?b$��?  ��x
�=I<��D�W���^�1�~Ŀ�F�F�����$%�  [d�D����O𷒿  �#���;�)�.�D?���1?t2%� iJ?�mǀ$?  �m�wF?b$��?  ��x
�=�Z�? �� 1O?��b�?A�#��>��$��4?
+�ԨĂ?-J��pr=�/���=!��$�r�>b�Q����=)�e�Eg�>`J���=���|�>YU��=@T��鸂>��y��=@�
c->[+�D�p��0@�ȟ�-	@���x�@
   BC1_QD_V02      QUAD�-W2�?�r�Q�E<>�*{)�������?Ip�=�v-�l >�@Ҽ�m>S�>�<M]<K�p+?�){��@W���;�=1;�!S>
m�/|�g>X�	��zW<Y�����?�A�'$��,W����=��a���>ȜK�+<��?�p<?N��߭��2,yE̸���R)���N��4?
���?�>d�(�ӷ<
+�ԨĂ?���3.�=5+��pr=Jh5��E?�m�+�`@?s����J?��WXb=?  �W�wF?b$��?  �Fs
�=�_���D��ɀ�m@�e�+ypF���WXb=�  �`d�D����O𷒿  	���Jh5��E?�m�+�`@?s����J?�6�J�;?  �W�wF?b$��?  �Fs
�=�-W2�?K�p+?Y�����?��?�p<?�N��4?
+�ԨĂ?5+��pr=2m��3��=	�g����>������=����f��>�H)�=�͹ڝĂ>��)�=�����Ă>��?DA�>@[�<��#9����o/@�4�kï!@�x��@   BC1_DR_QD_ED      DRIF�kzjn?�_?=>��H�0��a���� �=���Z*� >wk���n> ��e�L^<K�p+?an��W��@W���;�=�־+ S>
m�/|�g>�9c%�zW<Pɡ��e?lуhX�#����_T�=]��!/P>S#`~P�<��?�p<?Yy��㭸�2,yE̸����7'��KW��4?]�'��?�>�S��ӷ<
+�ԨĂ?j=R�'�=���2�pr=�㡫��E?�m�+�`@?�+g�bI?��WXb=?  q/�wF?b$��?  PTm
�=}�j�@E��ɀ�m@������E���WXb=�  Uld�D����O𷒿  f�����㡫��E?�m�+�`@?�+g�bI?�6�J�;?  q/�wF?b$��?  PTm
�=�kzjn?K�p+?Pɡ��e?��?�p<?KW��4?
+�ԨĂ?���2�pr=�n��3��=�g����>������=����f��>��H)�=�͹ڝĂ>���)�=m����Ă>��wݕ@@�Ϯ��:���D�-@����@!@�5w�@	   BC1_DR_V3      DRIF$�ؑ�6?ᐍ�c}A>�ݪG{��,BBC.�=��A�<E$>�^�QIr>Z��Ngb<K�p+?�*`I�J��@W���;�=M�7S>
m�/|�g>�۶��zW<O5�?�W�o� �:����?�=|/��5z>��K��<��?�p<?�&� ���2,yE̸�Y�S���b�����4?�@!*�?�>;:`��ӷ<
+�ԨĂ?��+��=Ů w�pr=I���N�I?�m�+�`@?�2A-�E?��WXb=?  �B�wF?b$��?  �fJ
�=��p�BI��ɀ�m@��Co�FzB���WXb=�  X�d�D����O𷒿  ��훂�I���N�I?�m�+�`@?�2A-�E?�6�J�;?  �B�wF?b$��?  �fJ
�=$�ؑ�6?K�p+?O5�?��?�p<?b�����4?
+�ԨĂ?Ů w�pr=n��3��=��g����>������=����f��>��H)�=�͹ڝĂ>Э�)�=�����Ă>�:윶G@d��	�?�M͚�W�%@����k@��ԕ>@   BC1_DR_QD_ED      DRIFS���?��(P�A>��#rUT���X����=�زp�$>�l��r>[����b<K�p+?��I����@W���;�=�V_S>
m�/|�g>�84�zW<�ۻ�8?�����4 ��V{V���=2@c���>گ��2|<��?�p<?�ۏ����2,yE̸������]m����4?��9�?�>�*�y�ӷ<
+�ԨĂ?x��U��={���pr=�o=΁�J?�m�+�`@?V��.E?��WXb=?  m�wF?b$��?  tD
�=+���V�I��ɀ�m@�IT��A���WXb=�  ��d�D����O𷒿  �z蛂��o=΁�J?�m�+�`@?V��.E?�6�J�;?  m�wF?b$��?  tD
�=S���?K�p+?�ۻ�8?��?�p<?]m����4?
+�ԨĂ?{���pr=�o��3��=͢g����>������=r���f��>��H)�=�͹ڝĂ>���)�=m����Ă>y���I@"�Y���?�l�z>�f$@�p��&�@��cXg@
   BC1_QD_V03      QUAD�m��?k�&&�$=>�򌒽"��R䶽{Q�=�\��M%>i�g�^7s>�7���b<QJX�b?�:0���I���ҡ�=P��N�>����)�b>�8g4��R<r�8�
?O�P�n��~��I�=�7y�Җ>�ujܴ<�Ճ���>2����X��iwT3���!TgA�l|����4?�6��?�>J&x۵ӷ<
+�ԨĂ?�(ob��=�����pr=I��Ҡ&K?�����9?�zE�t�D?7ɯ8?  X��wF?b$��?  B�>
�=[S���J�(����_9���(��gA�7ɯ8�  ��d�D����O𷒿   c㛂�I��Ҡ&K?�����9?�zE�t�D?؜q��l6?  X��wF?b$��?  B�>
�=�m��?QJX�b?r�8�
?�Ճ���>l|����4?
+�ԨĂ?�����pr= u�{���=}�I�rt�>��ҡ@��=s�xa�h�>�+�J}$�=�:alۿ�>y���v$�=��Կ�>�]l�\J@�r���:�[�f#@f�ѝ�@��cXg@   BC1_COR      KICKER�m��?k�&&�$=>�򌒽"��R䶽{Q�=�\��M%>i�g�^7s>�7���b<QJX�b?�:0���I���ҡ�=P��N�>����)�b>�8g4��R<r�8�
?O�P�n��~��I�=�7y�Җ>�ujܴ<�Ճ���>2����X��iwT3���!TgA�l|����4?�6��?�>J&x۵ӷ<
+�ԨĂ?�(ob��=�����pr=I��Ҡ&K?�����9?�zE�t�D?7ɯ8?  X��wF?b$��?  B�>
�=[S���J�(����_9���(��gA�7ɯ8�  ��d�D����O𷒿   c㛂�I��Ҡ&K?�����9?�zE�t�D?؜q��l6?  X��wF?b$��?  B�>
�=�m��?QJX�b?r�8�
?�Ճ���>l|����4?
+�ԨĂ?�����pr= u�{���=}�I�rt�>��ҡ@��=s�xa�h�>�+�J}$�=�:alۿ�>y���v$�=��Կ�>�]l�\J@�r���:�[�f#@f�ѝ�@��cXg@   BC1_BPM      MONI�m��?k�&&�$=>�򌒽"��R䶽{Q�=�\��M%>i�g�^7s>�7���b<QJX�b?�:0���I���ҡ�=P��N�>����)�b>�8g4��R<r�8�
?O�P�n��~��I�=�7y�Җ>�ujܴ<�Ճ���>2����X��iwT3���!TgA�l|����4?�6��?�>J&x۵ӷ<
+�ԨĂ?�(ob��=�����pr=I��Ҡ&K?�����9?�zE�t�D?7ɯ8?  X��wF?b$��?  B�>
�=[S���J�(����_9���(��gA�7ɯ8�  ��d�D����O𷒿   c㛂�I��Ҡ&K?�����9?�zE�t�D?؜q��l6?  X��wF?b$��?  B�>
�=�m��?QJX�b?r�8�
?�Ճ���>l|����4?
+�ԨĂ?�����pr= u�{���=}�I�rt�>��ҡ@��=s�xa�h�>�+�J}$�=�:alۿ�>y���v$�=��Կ�>�]l�\J@�r���:�[�f#@f�ѝ�@�?�	�@
   BC1_QD_V03      QUAD:��v�=?�h�N�5>�-�6����ԪJ2��=h� �,�%> �l�s>��D�Qc<.�! �G?����#罐�Z��=5��HN>�]�zIW[>'={��K<�B���	?9sZ�����J6m��=�|)>�G>WnWS�<�wO�7��>�Rk:���ns����DY@-�b�Ղ��4?J���?�>���I�ӷ<
+�ԨĂ?�
�t�=��pr=n ��K?�����2?F�;�!D?�.��p�2?  Z��wF?b$��?  NY9
�=�|*���J�~H�Y�2�t;MY� A��.��p�2�  V�d�D����O𷒿  
Iޛ��n ��K?�����2?F�;�!D?���KB�1?  Z��wF?b$��?  NY9
�=:��v�=?.�! �G?�B���	?�wO�7��>b�Ղ��4?
+�ԨĂ?��pr=����L��=R��~Zx�>DK�����=�B���l�>��,~� �=7ч�ໂ>'��� �=#�d�ٻ�>�ZI(C@K@8f�]i3����~Ý"@�	��I@ڑ�9��@   BC1_DR_QD_ED      DRIF���ԯ�?T�g9�6>>�M�����p�r���=��/��%>=tx���s>Y��N�c<.�! �G?�����潐�Z��=��]HN>�]�zIW[>��o��K<t��m�w	?9�_��-�$�ۘ���=e~�� >�I�HԆ<�wO�7��>6�^�m:���ns���1���:-黉0A}��4?\���?�>n�lU�ӷ<
+�ԨĂ?=
~*�=����pr=�ǊY�K?�����2?<� �E�C?�.��p�2?  ���wF?b$��?  �}3
�=��A�YK�~H�Y�2��>!ϟ@��.��p�2�  1�d�D����O𷒿  ^�؛���ǊY�K?�����2?<� �E�C?���KB�1?  ���wF?b$��?  �}3
�=���ԯ�?.�! �G?t��m�w	?�wO�7��>�0A}��4?
+�ԨĂ?����pr=ũ��L��=+��~Zx�>M�����=�D���l�>��,~� �=?ч�ໂ>.��� �=*�d�ٻ�>��S&L@Pgɴ3�I�P��!@:����@�[�@	   BC1_DR_V4      DRIF����3� ?��\�ݚ7>�����ph�k�*�=�u�Ѓ'>p���2u>�58H-�d<.�! �G?���� 佐�Z��=.���FN>�]�zIW[>%\U�K<L�;ip?�6�IC���ۑY=�=�VS�>Gʢ�D <�wO�7��>@P �y:���ns���E2
#-� z�b��4?� s��?�>D�o��ӷ<
+�ԨĂ?6ߝ�̠=o`�Sspr=��߫|�M?�����2?��e� �A?�.��p�2?  t��wF?b$��?  �
�=,��Z3M�~H�Y�2�q|)��=��.��p�2�  ��d�D����O𷒿  qU������߫|�M?�����2?��e� �A?���KB�1?  t��wF?b$��?  �
�=����3� ?.�! �G?L�;ip?�wO�7��> z�b��4?
+�ԨĂ?o`�Sspr=����L��=R��~Zx�>DK�����=�B���l�>��,~� �=7ч�ໂ>'��� �=#�d�ٻ�>�䒙�P@����5����iX@��/PCy@�UD&�@   BC1_DR_QD_ED      DRIF�%7`�4!?*�[5C�7>�^�����zғ�p�=��8��'>����}u>$1X�>e<.�! �G?ſWVCt㽐�Z��=����FN>�]�zIW[>*�8K<�̈;?�?�s����r�����=�q�l>Y�^  <�wO�7��>Z��|:���ns���ϣ��-�K�i]��4?:�;��?�>��楀ӷ<
+�ԨĂ?�އGƠ=���mpr=�n��GN?�����2?�x��rA?�.��p�2?  ���wF?b$��?  �
�=d�2�M�~H�Y�2�-x$���<��.��p�2�  ��d�D����O𷒿  �蹛���n��GN?�����2?�x��rA?���KB�1?  ���wF?b$��?  �
�=�%7`�4!?.�! �G?�̈;?�?�wO�7��>K�i]��4?
+�ԨĂ?���mpr=����L��=��~Zx�>�M�����=�E���l�>��,~� �=7ч�ໂ>'��� �=#�d�ٻ�>'ܵ%M�P@��=� c5��,���@�Eo?�@&�����@
   BC1_QD_V04      QUAD���[!?{��G�� >�f�܍D����
E�&�=&m�(>��Ӹ�u>�J��le<�����>^��˖s̽e%d\�1�=H�(�O�=Ha�B>�t����1<o��j4,?�ϊ��(�����=n���:>��j]��;LX�d��>�tP{���C�tƹ�*I"C�׻ԩYZ��4?����?�>r�(zӷ<
+�ԨĂ?b�N^��=>��hpr=�oR��N?"a ��?S�7��$A?��}�A�'?  �}�wF?b$��?  ��
�=�7i�>�M���;�����F�T<���}�A�'�  ��d�D����O𷒿  u̴����oR��N?"a ��?S�7��$A?���GKc'?  �}�wF?b$��?  ��
�=���[!?�����>o��j4,?LX�d��>ԩYZ��4?
+�ԨĂ?>��hpr=��g|6D�=0��I���>�8]8�=>�LseԂ>:����=��ƶ�> b�G��=��T񿶂>�cBxuP@0<�i���x�?&@�H�vU�@&�����@   BC1_COR      KICKER���[!?{��G�� >�f�܍D����
E�&�=&m�(>��Ӹ�u>�J��le<�����>^��˖s̽e%d\�1�=H�(�O�=Ha�B>�t����1<o��j4,?�ϊ��(�����=n���:>��j]��;LX�d��>�tP{���C�tƹ�*I"C�׻ԩYZ��4?����?�>r�(zӷ<
+�ԨĂ?b�N^��=>��hpr=�oR��N?"a ��?S�7��$A?��}�A�'?  �}�wF?b$��?  ��
�=�7i�>�M���;�����F�T<���}�A�'�  ��d�D����O𷒿  u̴����oR��N?"a ��?S�7��$A?���GKc'?  �}�wF?b$��?  ��
�=���[!?�����>o��j4,?LX�d��>ԩYZ��4?
+�ԨĂ?>��hpr=��g|6D�=0��I���>�8]8�=>�LseԂ>:����=��ƶ�> b�G��=��T񿶂>�cBxuP@0<�i���x�?&@�H�vU�@&�����@   BC1_BPM      MONI���[!?{��G�� >�f�܍D����
E�&�=&m�(>��Ӹ�u>�J��le<�����>^��˖s̽e%d\�1�=H�(�O�=Ha�B>�t����1<o��j4,?�ϊ��(�����=n���:>��j]��;LX�d��>�tP{���C�tƹ�*I"C�׻ԩYZ��4?����?�>r�(zӷ<
+�ԨĂ?b�N^��=>��hpr=�oR��N?"a ��?S�7��$A?��}�A�'?  �}�wF?b$��?  ��
�=�7i�>�M���;�����F�T<���}�A�'�  ��d�D����O𷒿  u̴����oR��N?"a ��?S�7��$A?���GKc'?  �}�wF?b$��?  ��
�=���[!?�����>o��j4,?LX�d��>ԩYZ��4?
+�ԨĂ?>��hpr=��g|6D�=0��I���>�8]8�=>�LseԂ>:����=��ƶ�> b�G��=��T񿶂>�cBxuP@0<�i���x�?&@�H�vU�@Oc��@
   BC1_QD_V04      QUAD����[!?z V�B��M������t�.S��=�4q�'(>�g�U�u>Ƿo�Lle<%EڄH��>}�	
��=:}Ʒ�B��Kw�LY���'��G�B��N@��y2�S�|x�?������CX}���=0TJ�,>���Fۆ�;�̵��>ُz�60j=D!US�=fZ�\l[�;���Y��4?^!��?�>"o/�sӷ<
+�ԨĂ??@�w��=J��cpr=�_��s�N?:%ЕW?ʄ����@?��E�4�?  R|�wF?b$��?  5
�=?}p�)�M�YX��*��iI5���;�$@��t��  �d�D����O𷒿  ϭ�����_��s�N?:%ЕW?ʄ����@?��E�4�?  R|�wF?b$��?  5
�=����[!?%EڄH��>S�|x�?�̵��>���Y��4?
+�ԨĂ?J��cpr=�
�ز �=n/Ԁă>��"(�=�R����>�����=������>eX����=�����>�s[zrO@��u,�@�#�1
�@�v��p�?;`�P1@   BC1_DR_QD_ED      DRIF��=��H!?]Ԛҫq�.d�1�p��%�Q��u�=-�~R��'>߈	��u>i���%Se<%EڄH��>�M��%��=:}Ʒ�B��C�EY���'��G�B�qWOQ�y2�-Y�#9�?jl2�E�7�C$��=M9!0>j�6Î�;�̵��>���"60j=D!US�=%z�d[�;O��X��4?z'��?�>]��lӷ<
+�ԨĂ?%H02��=r��^pr=�A"w[dN?:%ЕW?`�i���@?��E�4�?  �z�wF?b$��?  CK
�=�8|e�M�YX��*��p�@��;�$@��t��  ��d�D����O𷒿  �;�����A"w[dN?:%ЕW?`�i���@?��E�4�?  �z�wF?b$��?  CK
�=��=��H!?%EڄH��>-Y�#9�?�̵��>O��X��4?
+�ԨĂ?r��^pr=�
�ز �={/Ԁă>��"(�=�R����>�����=������>fX����=�����>��S4-O@O�^�Ni@����p@��~�J�?cBS�@	   BC1_DR_V5      DRIF��^� ?��������2�:z����8/$��=��t!Cd'>\�%@u>ХE��d<%EڄH��>_T}��=:}Ʒ�B��ے�#Y���'��G�B�ה���y2��ޛ�	b?�76����x�[��=�Kn�+@>� ���;�̵��>�%��20j=D!US�=�&<B[�;��T��4?`U���?�>}Ci$Lӷ<
+�ԨĂ?뽯���=J�,\Epr=���M?:%ЕW?ÃOtQk@?��E�4�?  [r�wF?b$��?  ��	�=PX�VM�YX��*��)w��Q�:�$@��t��  ��d�D����O𷒿  J��������M?:%ЕW?ÃOtQk@?��E�4�?  [r�wF?b$��?  ��	�=��^� ?%EڄH��>�ޛ�	b?�̵��>��T��4?
+�ԨĂ?J�,\Epr=�
�ز �=_/Ԁă>��"(�=�R����>�����=������>fX����=�����>t�ͤ�M@�\S�@k�X��E@m�6?��?���) @	   BC1_DR_20      DRIF$֖. ?P�`�7���a������?�=a8�&>����[�s>B�Wʴc<%EڄH��>���SƼ=:}Ʒ�B��7�X���'��G�B������y2���K�;B?��QC彾�RU~��=?R�<h>�k�ƀ <�̵��>s�*0j=D!US�=�:yr�Z�;��J��4?'{��?�>�F��ҷ<
+�ԨĂ?T���L�=WV'upr={�^/&"L?:%ЕW?9%�>?��E�4�?  �]�wF?b$��?  ܈�	�=�6cA�K�YX��*��� /�%"8�$@��t��  ��d�D����O𷒿  ��P���{�^/&"L?:%ЕW?9%�>?��E�4�?  �]�wF?b$��?  ܈�	�=$֖. ?%EڄH��>��K�;B?�̵��>��J��4?
+�ԨĂ?WV'upr=�
�ز �={/Ԁă>��"(�=�R����>�����=������>fX����=�����>~���J@pu跍@�ҟ�F�@���u���?��SeL!@   BC1_DP_DIP1   	   CSRCSBEND����p%?�53��|r>|*:7�E�4zq����=����d�Uv������1d4�䡼i3m�;D?]�K�vǽ:�A�G�����A)��K#U-��׾.���TǼA\�
�x?�k��6��,�8����=~��86�>{�"Q'�<�j�d���>S�Q9������+.��=9'r��ٻ����6�4?� �-Kj�>��;!,�<@w��Â?�`�@��=¢^_�r=	�[R!Q?�w�a�V?xSX�<?<L�d?  ߟl]G?������?  ��Zׄ=	�[R!Q��w�a�V�c5���6����N� �  C�j�E�!�+Wʶ��  ���;��Z��#*O?��
<�%U?xSX�<?<L�d?  ߟl]G?������?  ��Zׄ=����p%?i3m�;D?A\�
�x?�j�d���>����6�4?@w��Â?¢^_�r=�����s>V$��G?�L���	>j�)>њ>"h�SP�=z�>�E��>}��I�=\�r?��>�=�pa�1@(5�˧ @�zڅ��@�Y5CԤ�?��SeL"@   BC1_DR_SIDE_C      CSRDRIFT��=���<?�`/YF�>��E~�S�D��=+�"������>4о.�SQU#��i3m�;D?�����ʽ:�A�G��K�b)���S��׾��ocTǼ���'�?%�=���޽g�ֱ��=\ÿ@ٖ>iæ��1<�j�d���>�;���� �Ez��=�qF��ٻq���P�4?�~ʚi�>�I�3,�<K�FÂ?��  �=�E�>8�r=���*eZ?�w�a�V?׀�M6:?<L�d?  �P�_G?aǡX��?  0�<ل=���*eZ��w�a�V�T�zy5����N� �  ��]�E�֜k�ȴ��  �h�9��"�!Y�X?��
<�%U?׀�M6:?<L�d?  �P�_G?aǡX��?  0�<ل=��=���<?i3m�;D?���'�?�j�d���>q���P�4?K�FÂ?�E�>8�r=����s>ξA��G?��7�S
>����m5�>#h�SP�=/t�A��>�1*�I�=A��:��>�=[[m/@�9ѽz�?�Y3|��@9!r�8R�?������'@   BC1_DR_SIDE      DRIF%8�Ta?4:�6õ>i���T��ۺ�A��=�Q�gp���+%'�O����P{��i3m�;D?�� ��ֽ:�A�G���^*���S��׾�5��RSǼ�́m��?[IF���=���a�={!���>�ee���;�j�d���>� �ǜ� �Ez��=q[���ٻ�L����4?n��Jaj�>(զ)�+�<K�FÂ?�u�x3�=��b�r=I
�?�s?�w�a�V?-۳�>1/?<L�d?  �@mG?aǡX��?  s���=I
�?�s��w�a�V���K�U�,����N� �  ��E�֜k�ȴ��  H�N.������Rr?��
<�%U?-۳�>1/?<L�d?  �@mG?aǡX��?  s���=%8�Ta?i3m�;D?�́m��?�j�d���>�L����4?K�FÂ?��b�r=�����s>ȿA��G?N�8�S
>g��m5�>#h�SP�=/t�A��>�1*�I�=A��:��>|ʭ5��@̉��
��?�b'F&@K�Z�п�s�ʜ(@   BC1_DP_DIP2   	   CSRCSBEND����`b?|�>L!W�"K�� ���=l��g���w��5u����,��ּ�~���>`��-f����ga������-����@+}F����h"�<��
?�%��b�=F�[�:=�=f���N�>فs@���;6��'a��>�ն"(����*`ؓ���r��Zƻ#����_&?��L�@9�>.�pYeɛ<�##���?���,�I�<��<��c=|�~=��t?ɂ3�3�?��y��:.?Dl(x�?   l�8?P�V��?  ��tv=|�~=��t��"r�����2�i-���BHq�  Rv��6��2X2���  
�wJt����u]Es?ɂ3�3�?��y��:.?Dl(x�?   l�8?P�V��?  ��tv=����`b?�~���><��
?6��'a��>#����_&?�##���?��<��c=�nxO``>�����>����^��=���+ȍ>V�Y��=�K�	��>���=�0�k��>jrAwOD(@�'n�� @�����@;Ǌ��ӿK ���O)@   BC1_DR_CENT      DRIF<:ab?����~,>u��	���Л�W��=�㟒P���d�q���z�K�|�ּ�~���>���),׻���ga����I�,����@+}F��{|٥g"���^7ކ?*ʪ�ݏ�=˪�	�=����>O��~Q��;6��'a��>\b�)����*`ؓ��͢��Zƻ	S�t�_&?D��@9�>���9�ț<�##���?��&gI�<חF>k�c=v*�,g�t?ɂ3�3�?oO���-?Dl(x�?  d�8?P�V��?  ,,v=v*�,g�t��"r���oO���-���BHq�  ����6��2X2���  TcJt�q��$<2s?ɂ3�3�?�+C�-?Dl(x�?  d�8?P�V��?  ,,v=<:ab?�~���>��^7ކ?6��'a��>	S�t�_&?�##���?חF>k�c=�nxO``>�����>y��^��=p��+ȍ>V�Y��=�K�	��>���=�0�k��>L�!Av%@��t����?"#�]4�@��X�>@ٿK ���O)@   BC1_BPM      MONI<:ab?����~,>u��	���Л�W��=�㟒P���d�q���z�K�|�ּ�~���>���),׻���ga����I�,����@+}F��{|٥g"���^7ކ?*ʪ�ݏ�=˪�	�=����>O��~Q��;6��'a��>\b�)����*`ؓ��͢��Zƻ	S�t�_&?D��@9�>���9�ț<�##���?��&gI�<חF>k�c=v*�,g�t?ɂ3�3�?oO���-?Dl(x�?  d�8?P�V��?  ,,v=v*�,g�t��"r���oO���-���BHq�  ����6��2X2���  TcJt�q��$<2s?ɂ3�3�?�+C�-?Dl(x�?  d�8?P�V��?  ,,v=<:ab?�~���>��^7ކ?6��'a��>	S�t�_&?�##���?חF>k�c=�nxO``>�����>y��^��=p��+ȍ>V�Y��=�K�	��>���=�0�k��>L�!Av%@��t����?"#�]4�@��X�>@ٿ~3�+1*@   BC1_DR_CENT      DRIF5a���ab?ԡ	K�I>��m��~��|؄�=�g89����:<�l���;�<���ּ�~���>= �V$����ga����],����@+}F�j$;g"����^�?P7�%k^�=����ި=�~hmA�>��0~���;6��'a��>�M�+����*`ؓ��qS)z�Zƻ�
�e�_&?�>��@9�>`L�oț<�##���?�͝��H�<M��?�c=p�u��t?ɂ3�3�?\Ǥ�/G.?Dl(x�?  6\�8?P�V��?  t��
v=p�u��t��"r���\Ǥ�/G.���BHq�  c��6��2X2���  ���It�Ԁ�"s?ɂ3�3�?���άs-?Dl(x�?  6\�8?P�V��?  t��
v=5a���ab?�~���>���^�?6��'a��>�
�e�_&?�##���?M��?�c=�nxO``>�����>����^��=���+ȍ>V�Y��=�K�	��>���=�0�k��>�	��G�"@��<^�?�j�0@���e�޿��	%�*@   BC1_DP_DIP3   	   CSRCSBEND�M��vWa?Ư�n�����	����"�q���=�I����j�x���I��_	I��z���LS�D?�&����=^K�w�������N>a���p�>W���7��<����?�s���=�O�r�r�9�@�>��	���Ή$��`�>r�k5nY=�wo]�}���ً�;$�DLV��>d��U9�>�Vb��@<u�|�โ?�e��>N�<�B�'
�4=��<�qs?2Q��0�V?�5T$��.?�>=v�`?  ��<?��+�?  �ܫG=��<�qs�^����T��5T$��.���M"
^�  ���d	����u ���  �7pE�*�r?2Q��0�V?7B�A��.?�>=v�`?  ��<?��+�?  �ܫG=�M��vWa?�LS�D?����?Ή$��`�>$�DLV��>u�|�โ?�B�'
�4=Q۞��5K>��i��>�s�����=���>��>��Xw��=�C��Y��>y8
��=�Z�R��>�銆��&@�ZW�[�@�źe�@!S'��࿜�	%�+@   BC1_DR_SIDE_C      CSRDRIFT�x&oϬ]?�RokГ������׺��)���=�e����f���y[�vm�$9���LS�D?�I_<6��=^K�w����)�N>3�<xp�>鋱�M��<�	�3Q�?$Y�����=<��҃(o��a��۬>���_�i��Ή$��`�>���q�HY=��@,�����zd�Ö;�.�Gu��>I�����>�,�q@<ZLc¹�?���F�K�<v"�1�4=,{4��p?2Q��0�V?[.JqE0?�>=v�`?  �r/? ^L���?  P0��G=,{4��p�^����T�1̇[[9/���M"
^�  ���7	����j	���  �d�FE�;ۨ>�n?2Q��0�V?[.JqE0?�>=v�`?  �r/? ^L���?  P0��G=�x&oϬ]?�LS�D?�	�3Q�?Ή$��`�>�.�Gu��>ZLc¹�?v"�1�4="���5K>�Z\���>�y���=��"�޳�>��Xw��=�yF�㳂>z=A
��=-"b�ܳ�>n���d"@�`K�@W�-@�9c�3;�c�E�0@   BC1_DR_SIDE      DRIF�r��� ?�'��.�t��+haL,ýB`!6��=*�ZY)��Bp��3��{{`]u e��LS�D? ����^K�w�����/n�N>3�<xp�>�_9�<���{�?�C����={�En\=�Ȉ��>�7y��Ή$��`�>��(��X=��@,�����z����;>W����>Al�:$��>��i��@<ZLc¹�?�Ҙ>�<��_��4=ؒ���7;?2Q��0�V? 6�Y�9?�>=v�`?  ��X? ^L���?  ��{H=�!��78�^����T��z���3���M"
^�  �mTN����j	���  வ`D�ؒ���7;?2Q��0�V? 6�Y�9?�>=v�`?  ��X? ^L���?  ��{H=�r��� ?�LS�D?���{�?Ή$��`�>>W����>ZLc¹�?��_��4=�����5K>n%\���>��G���=���޳�>��Xw��=�yF�㳂>z=A
��=-"b�ܳ�> 1���9�?��w_��?���4�&@2���T��r~�4i1@   BC1_DP_DIP4   	   CSRCSBENDZ"|���>����?@��7��d��ʽk����d�=��� ��ս��%�u<]���\)>�_��]�>�ߴ�SԽ���Ȥ��������=;�X7��B>F�1h��;ԉ4���?<!�6�4�=���ѷ�=�����4>P׫u��;P͢�i��>��r��1�U#$��罽��c��um�y������>�9�3�>��ohD<�E��?&��lS�<j�F�w7=���dڟ-?��(��?�n��;?��Ԯ*1?  �8��?�9V���?  ����J=���dڟ-���(�����J�=�5�����r�  `ͽQ
���-#�c��  �#&F���G�g�+?�+�b�?�n��;?��Ԯ*1?  �8��?�9V���?  ����J=Z"|���>_��]�>ԉ4���?P͢�i��>y������>�E��?j�F�w7=+��=���=��cM��>�V�tV��=�ߚR�>�dY�P�=C��1��>T��UJ�=7FZ?+��>SS�0��?��H@��?i6�4�|(@�qu��r~�4i�1@   BC1_DR_20_C      CSRDRIFT��Z�ٜ�>P�!`�J�=��Zd��׽wt쵫�t���~�Gҽ�秔{}X�ME�W���_��]�>ڠAսս���Ȥ��K�v1��=�W��B>��S�D��;�j;�H?�r���=β$�To�=ٗ-�5�>�O�K��;P͢�i��>�B��1�(tm�(q��9�`��qm�+o�<!��>b���95�>e�ˋ)eD<=\�#g��?�>7�P�<==C��s7=����z2?��(��?sU���>?��Ԯ*1?  ��ϊ?� ���?  ��O�J=����z2���(���`u����8�����r�   ��O
�(���^��   cU"F���2��0?�+�b�?sU���>?��Ԯ*1?  ��ϊ?� ���?  ��O�J=��Z�ٜ�>_��]�>�j;�H?P͢�i��>+o�<!��>=\�#g��?==C��s7=+��=���=�-?Fݓ�>�WR���=\CmyxR�>�dY�P�=QR�̱�>\t�YJ�=֑}iű�>T�o��?Z�ԃ�|ӿ��Nx+@9r�+�E��r~�4i�1@	   BC1_WA_OU      WATCH��Z�ٜ�>P�!`�J�=��Zd��׽wt쵫�t���~�Gҽ�秔{}X�ME�W���_��]�>ڠAսս���Ȥ��K�v1��=�W��B>��S�D��;�j;�H?�r���=β$�To�=ٗ-�5�>�O�K��;P͢�i��>�B��1�(tm�(q��9�`��qm�+o�<!��>b���95�>e�ˋ)eD<=\�#g��?�>7�P�<==C��s7=����z2?��(��?sU���>?��Ԯ*1?  ��ϊ?� ���?  ��O�J=����z2���(���`u����8�����r�   ��O
�(���^��   cU"F���2��0?�+�b�?sU���>?��Ԯ*1?  ��ϊ?� ���?  ��O�J=��Z�ٜ�>_��]�>�j;�H?P͢�i��>+o�<!��>=\�#g��?==C��s7=+��=���=�-?Fݓ�>�WR���=\CmyxR�>�dY�P�=QR�̱�>\t�YJ�=֑}iű�>T�o��?Z�ԃ�|ӿ��Nx+@9r�+�E��