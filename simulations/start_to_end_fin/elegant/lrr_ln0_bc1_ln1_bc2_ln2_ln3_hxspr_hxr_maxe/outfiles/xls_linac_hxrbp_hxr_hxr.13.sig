SDDS1
!# little-endian
&description text="sigma matrix--input: xls_linac_hxrbp_hxr_hxr.track.ele  lattice: xls_linac_hxrbp_hxr.13.lte", contents="sigma matrix", &end
&parameter name=Step, description="Simulation step", type=long, &end
&parameter name=SVNVersion, description="SVN version number", type=string, fixed_value=unknown, &end
&column name=s, units=m, description=Distance, type=double,  &end
&column name=ElementName, description="Element name", format_string=%10s, type=string,  &end
&column name=ElementOccurence, description="Occurence of element", format_string=%6ld, type=long,  &end
&column name=ElementType, description="Element-type name", format_string=%10s, type=string,  &end
&column name=s1, symbol="$gs$r$b1$n", units=m, description="sqrt(<x*x>)", type=double,  &end
&column name=s12, symbol="$gs$r$b12$n", units=m, description="<x*xp'>", type=double,  &end
&column name=s13, symbol="$gs$r$b13$n", units="m$a2$n", description="<x*y>", type=double,  &end
&column name=s14, symbol="$gs$r$b14$n", units=m, description="<x*y'>", type=double,  &end
&column name=s15, symbol="$gs$r$b15$n", units="m$a2$n", description="<x*s>", type=double,  &end
&column name=s16, symbol="$gs$r$b16$n", units=m, description="<x*delta>", type=double,  &end
&column name=s17, symbol="$gs$r$b17$n", units="m*s", description="<x*t>", type=double,  &end
&column name=s2, symbol="$gs$r$b2$n", description="sqrt(<x'*x'>)", type=double,  &end
&column name=s23, symbol="$gs$r$b23$n", units=m, description="<x'*y>", type=double,  &end
&column name=s24, symbol="$gs$r$b24$n", description="<x'*y'>", type=double,  &end
&column name=s25, symbol="$gs$r$b25$n", units=m, description="<x'*s>", type=double,  &end
&column name=s26, symbol="$gs$r$b26$n", description="<x'*delta>", type=double,  &end
&column name=s27, symbol="$gs$r$b27$n", units=s, description="<x'*t>", type=double,  &end
&column name=s3, symbol="$gs$r$b3$n", units=m, description="sqrt(<y*y>)", type=double,  &end
&column name=s34, symbol="$gs$r$b34$n", units=m, description="<y*y'>", type=double,  &end
&column name=s35, symbol="$gs$r$b35$n", units="m$a2$n", description="<y*s>", type=double,  &end
&column name=s36, symbol="$gs$r$b36$n", units=m, description="<y*delta>", type=double,  &end
&column name=s37, symbol="$gs$r$b37$n", units="m*s", description="<y*t>", type=double,  &end
&column name=s4, symbol="$gs$r$b4$n", description="sqrt(<y'*y')>", type=double,  &end
&column name=s45, symbol="$gs$r$b45$n", units=m, description="<y'*s>", type=double,  &end
&column name=s46, symbol="$gs$r$b46$n", description="<y'*delta>", type=double,  &end
&column name=s47, symbol="$gs$r$b47$n", units=s, description="<y'*t>", type=double,  &end
&column name=s5, symbol="$gs$r$b5$n", units=m, description="sqrt(<s*s>)", type=double,  &end
&column name=s56, symbol="$gs$r$b56$n", units=m, description="<s*delta>", type=double,  &end
&column name=s57, symbol="$gs$r$b57$n", units="m*s", description="<s*t>", type=double,  &end
&column name=s6, symbol="$gs$r$b6$n", description="sqrt(<delta*delta>)", type=double,  &end
&column name=s67, symbol="$gs$r$b67$n", units=s, description="<delta*t>", type=double,  &end
&column name=s7, symbol="$gs$r$b7$n", description="sqrt(<t*t>)", type=double,  &end
&column name=ma1, symbol="max$sb$ex$sb$e", units=m, description="maximum absolute value of x", type=double,  &end
&column name=ma2, symbol="max$sb$ex'$sb$e", description="maximum absolute value of x'", type=double,  &end
&column name=ma3, symbol="max$sb$ey$sb$e", units=m, description="maximum absolute value of y", type=double,  &end
&column name=ma4, symbol="max$sb$ey'$sb$e", description="maximum absolute value of y'", type=double,  &end
&column name=ma5, symbol="max$sb$e$gD$rs$sb$e", units=m, description="maximum absolute deviation of s", type=double,  &end
&column name=ma6, symbol="max$sb$e$gd$r$sb$e", description="maximum absolute value of delta", type=double,  &end
&column name=ma7, symbol="max$sb$e$gD$rt$sb$e", units=s, description="maximum absolute deviation of t", type=double,  &end
&column name=minimum1, symbol="x$bmin$n", units=m, type=double,  &end
&column name=minimum2, symbol="x'$bmin$n", units=m, type=double,  &end
&column name=minimum3, symbol="y$bmin$n", units=m, type=double,  &end
&column name=minimum4, symbol="y'$bmin$n", units=m, type=double,  &end
&column name=minimum5, symbol="$gD$rs$bmin$n", units=m, type=double,  &end
&column name=minimum6, symbol="$gd$r$bmin$n", units=m, type=double,  &end
&column name=minimum7, symbol="t$bmin$n", units=s, type=double,  &end
&column name=maximum1, symbol="x$bmax$n", units=m, type=double,  &end
&column name=maximum2, symbol="x'$bmax$n", units=m, type=double,  &end
&column name=maximum3, symbol="y$bmax$n", units=m, type=double,  &end
&column name=maximum4, symbol="y'$bmax$n", units=m, type=double,  &end
&column name=maximum5, symbol="$gD$rs$bmax$n", units=m, type=double,  &end
&column name=maximum6, symbol="$gd$r$bmax$n", units=m, type=double,  &end
&column name=maximum7, symbol="t$bmax$n", units=s, type=double,  &end
&column name=Sx, symbol="$gs$r$bx$n", units=m, description=sqrt(<(x-<x>)^2>), type=double,  &end
&column name=Sxp, symbol="$gs$r$bx'$n", description=sqrt(<(x'-<x'>)^2>), type=double,  &end
&column name=Sy, symbol="$gs$r$by$n", units=m, description=sqrt(<(y-<y>)^2>), type=double,  &end
&column name=Syp, symbol="$gs$r$by'$n", description=sqrt(<(y'-<y'>)^2>), type=double,  &end
&column name=Ss, symbol="$gs$r$bs$n", units=m, description=sqrt(<(s-<s>)^2>), type=double,  &end
&column name=Sdelta, symbol="$gs$bd$n$r", description=sqrt(<(delta-<delta>)^2>), type=double,  &end
&column name=St, symbol="$gs$r$bt$n", units=s, description=sqrt(<(t-<t>)^2>), type=double,  &end
&column name=ex, symbol="$ge$r$bx$n", units=m, description="geometric horizontal emittance", type=double,  &end
&column name=enx, symbol="$ge$r$bx,n$n", units=m, description="normalized horizontal emittance", type=double,  &end
&column name=ecx, symbol="$ge$r$bx,c$n", units=m, description="geometric horizontal emittance less dispersive contributions", type=double,  &end
&column name=ecnx, symbol="$ge$r$bx,cn$n", units=m, description="normalized horizontal emittance less dispersive contributions", type=double,  &end
&column name=ey, symbol="$ge$r$by$n", units=m, description="geometric vertical emittance", type=double,  &end
&column name=eny, symbol="$ge$r$by,n$n", units=m, description="normalized vertical emittance", type=double,  &end
&column name=ecy, symbol="$ge$r$by,c$n", units=m, description="geometric vertical emittance less dispersive contributions", type=double,  &end
&column name=ecny, symbol="$ge$r$by,cn$n", units=m, description="normalized vertical emittance less dispersive contributions", type=double,  &end
&column name=betaxBeam, symbol="$gb$r$bx,beam$n", units=m, description="betax for the beam, excluding dispersive contributions", type=double,  &end
&column name=alphaxBeam, symbol="$ga$r$bx,beam$n", description="alphax for the beam, excluding dispersive contributions", type=double,  &end
&column name=betayBeam, symbol="$gb$r$by,beam$n", units=m, description="betay for the beam, excluding dispersive contributions", type=double,  &end
&column name=alphayBeam, symbol="$ga$r$by,beam$n", description="alphay for the beam, excluding dispersive contributions", type=double,  &end
&data mode=binary, &end
/                 _BEG_      MARK������>�H%�5�=��Y����>m&s{�5�W���[=��%���	��:���ؘ;g\�1���>?A�z�3��ρ*[���>�?g���T�r�u>e	~�bs˻�H:���>g�{�=��(��=���.�=����@;��cm�>4��Z�hܼ�*�����=�����`��>0�DVF����~L���;���PW�D?QJ�"9��>���4�<�K�
�� ?8�߯H�?��(�� ?���Q�"?   Yc�>����]0q?   �f7=�K�
�� �{<����(�� ����Q�"�   p�zȾ"�xE��e�   -���)�HY� ?8�߯H�?�����?�r�Z�,?   Yc�>����]0q?   �f7=������>g\�1���>�H:���>��cm�>�`��>���PW�D?�>���4�<$t,H׾=�5t��;�>���"�M�=H��]C�>+��;%�=���Q�ƃ>TS�I%�=w�ƃ>�0Ŀw$�?�;Z����R5��)@!kx��~�           BNCH      CHARGE������>�H%�5�=��Y����>m&s{�5�W���[=��%���	��:���ؘ;g\�1���>?A�z�3��ρ*[���>�?g���T�r�u>e	~�bs˻�H:���>g�{�=��(��=���.�=����@;��cm�>4��Z�hܼ�*�����=�����`��>0�DVF����~L���;���PW�D?QJ�"9��>���4�<�K�
�� ?8�߯H�?��(�� ?���Q�"?   Yc�>����]0q?   �f7=�K�
�� �{<����(�� ����Q�"�   p�zȾ"�xE��e�   -���)�HY� ?8�߯H�?�����?�r�Z�,?   Yc�>����]0q?   �f7=������>g\�1���>�H:���>��cm�>�`��>���PW�D?�>���4�<$t,H׾=�5t��;�>���"�M�=H��]C�>+��;%�=���Q�ƃ>TS�I%�=w�ƃ>�0Ŀw$�?�;Z����R5��)@!kx��~�0ߙ��?	   HXR_DR_V1      DRIF�����5�>��\S��=���R�閽�
�{��k��t�)=�Ƴ�Ep�)@�92g;g\�1���>��qX�D��ρ*[�� �,.g���T�r�u>�];scs˻������>�v��=r@��w=���w�̢=�� @;��cm�>�{��hܼ�*�����=�v�D���Z��>%h��@��R��ݏ��;���PW�D?V��7"9��- �4�<Ee�!�?8�߯H�?W����� ?���Q�"?   T�c�>����]0q?   ��7=Ee�!��{<��W����� ����Q�"�    �zȾ"�xE��e�    ��zl��x�?8�߯H�?2�(���?�r�Z�,?   T�c�>����]0q?   ��7=�����5�>g\�1���>������>��cm�>�Z��>���PW�D?�- �4�<$t,H׾=�5t��;�>���"�M�=D��]C�>��;%�=諢Q�ƃ>CS�I%�=w�ƃ><I��,n�?�f���������+@�}���03��t�?   HXR_DR_QD_ED      DRIF>tDL��>����� �=1^v�v���˪Dǆ���՚(f:C��o�#1��J$�!���g\�1���>�S�� Ӛ�ρ*[��Ԑ%g���T�r�u>_^�cs˻�F3��I�>b~���`�=յ<�yH=�P)���=q+�_uT@;��cm�>����hܼ�*�����=v�EF*��P�A �>����=��������;���PW�D?��{K"9����4�<}k�)?8�߯H�?N�C��� ?���Q�"?   �c�>����]0q?   ��7=}k�)�{<��N�C��� ����Q�"�   �zȾ"�xE��e�   \�����?8�߯H�?B��M4 ?�r�Z�,?   �c�>����]0q?   ��7=>tDL��>g\�1���>�F3��I�>��cm�>P�A �>���PW�D?���4�<$t,H׾=�5t��;�>���"�M�=D��]C�>��;%�=諢Q�ƃ>CS�I%�=w�ƃ>c�G���?�1����^6N��t,@�bEYdr�껟t��?
   HXR_QD_V01      QUAD�Z2��>�~\�3�=�P5�P.���!#�9E=ګj��I�K{��3��Ur�҆��nU�BY�>`ո���*��{?�L��{$��
�E�*�>/�D��˻��82fW�>.��ؓӣ���[�1=@��B'�=�eR��?@;�g����>��s��K�CM�^�1<�|N2���Z �>Ӈ��<���1�ؐ��;���PW�D?~�ۯ"9��l���4�<~�!9�?<��wtT?�y�1�� ?���Q��>   T�c�>����]0q?   �7=~�!9����C�B��y�1�� ����Q��   0�zȾ"�xE��e�   ���F����R?<��wtT?����; ?��V���>   T�c�>����]0q?   �7=�Z2��>�nU�BY�>��82fW�>�g����>��Z �>���PW�D?�l���4�<|�N��־=KkL;�>��E�rM�=��tE0C�>�ȃ]"�=�g�s�ă>���+\"�=V����ă>yߎ���?����v��6B%kő,@/cwm��?껟t��?   HXR_COR      KICKER�Z2��>�~\�3�=�P5�P.���!#�9E=ګj��I�K{��3��Ur�҆��nU�BY�>`ո���*��{?�L��{$��
�E�*�>/�D��˻��82fW�>.��ؓӣ���[�1=@��B'�=�eR��?@;�g����>��s��K�CM�^�1<�|N2���Z �>Ӈ��<���1�ؐ��;���PW�D?~�ۯ"9��l���4�<~�!9�?<��wtT?�y�1�� ?���Q��>   T�c�>����]0q?   �7=~�!9����C�B��y�1�� ����Q��   0�zȾ"�xE��e�   ���F����R?<��wtT?����; ?��V���>   T�c�>����]0q?   �7=�Z2��>�nU�BY�>��82fW�>�g����>��Z �>���PW�D?�l���4�<|�N��־=KkL;�>��E�rM�=��tE0C�>�ȃ]"�=�g�s�ă>���+\"�=V����ă>yߎ���?����v��6B%kő,@/cwm��?껟t��?   HXR_BPM      MONI�Z2��>�~\�3�=�P5�P.���!#�9E=ګj��I�K{��3��Ur�҆��nU�BY�>`ո���*��{?�L��{$��
�E�*�>/�D��˻��82fW�>.��ؓӣ���[�1=@��B'�=�eR��?@;�g����>��s��K�CM�^�1<�|N2���Z �>Ӈ��<���1�ؐ��;���PW�D?~�ۯ"9��l���4�<~�!9�?<��wtT?�y�1�� ?���Q��>   T�c�>����]0q?   �7=~�!9����C�B��y�1�� ����Q��   0�zȾ"�xE��e�   ���F����R?<��wtT?����; ?��V���>   T�c�>����]0q?   �7=�Z2��>�nU�BY�>��82fW�>�g����>��Z �>���PW�D?�l���4�<|�N��־=KkL;�>��E�rM�=��tE0C�>�ȃ]"�=�g�s�ă>���+\"�=V����ă>yߎ���?����v��6B%kő,@/cwm��?.G\���?
   HXR_QD_V01      QUADs'��֢�>�7����=�vO�(���rb,�Q�=�3��P�5fsE� �DC~7i�����E��f�>���	����͞����=����Ώ��F]��>�����̻L�	��@�>�pbKIֽ�F�x=6ٙ�.�=&�F��@;B
��n��>���k� ��]�.��I.�W�#>��K�� �>+�ڈ;��R�����;���PW�D?e�Z�"9�����4�<�ObJ?�)�cv�?ev�� ?���%>?   0�c�>����]0q?   Կ7=�ObJ��)�cv��ev�� ��f��
�   `�zȾ"�xE��e�   ���90�? 1�y�?C�#��/ ?���%>?   0�c�>����]0q?   Կ7=s'��֢�>��E��f�>L�	��@�>B
��n��>�K�� �>���PW�D?����4�<H3�i־=���\;�>V��OM�={���C�>7��%��=��:;Ã>���1��=7�7d:Ã>��Pk���?�s"�\S
��k��Rh,@Iy�s��@<t���p�?   HXR_DR_QD_ED      DRIFh��w�>�����=d-^�En���?Z�6�=�3 (T]�E��n����A! 3����E��f�>�Vg�\I��͞����=`��Ώ��F]��>m3� ��̻I�褞�>�u�>%�սP9X@9$=����|��=4��m�>;B
��n��>�|��m� ��]�.�����#>���l!�>jO�5���������;���PW�D?n��I"9��ľ��4�<�S��OF?�)�cv�?=-�� ?���%>?   @�c�>����]0q?   �7=�S��OF��)�cv��=-�� ��f��
�   }zȾ"�xE��e�   �������
? 1�y�?�5AN�?���%>?   @�c�>����]0q?   �7=h��w�>��E��f�>I�褞�>B
��n��>��l!�>���PW�D?�ľ��4�<H3�i־=���\;�>U��OM�=z���C�>Y��%��=��:;Ã>���1��=M�7d:Ã>e"{�E @m�u�pD�UC/�0+@�ϧt%@v���d�?	   HXR_DR_V2      DRIFµ��W�>���3�=����P����ڡ=���6o���;K�3>�m�ad�ֻ��E��f�>�����⓽͞����=�bo�=Ϗ��F]��>�4bހ̻&K����>S��I��O�>�|�！�H� ��=��Bs{,�B
��n��>�,���� ��]�.��7O�$>�Q�P�;�>�	E� ����s���;���PW�D?���M�!9�����4�<�~%&�,,?�)�cv�?�~����	?���%>?   9j�>����]0q?   ��==�~%&�,,��)�cv���~����	��f��
�   ��xȾ"�xE��e�   �P��y�4�,? 1�y�?��UԣZ?���%>?   9j�>����]0q?   ��==µ��W�>��E��f�>&K����>B
��n��>Q�P�;�>���PW�D?����4�<�2�i־=;��\;�>���OM�=����C�>R��%��=��:;Ã>���1��=H�7d:Ã>q��U��<@��5&�^,�m*:�+��?�<u� @ ��bT�?   HXR_DR_QD_ED      DRIFy���$ �>�-{g��=��g$᣽]^N#I�=��q,V��hxO@;>�H��׻��E��f�>C�x��͞����=�� &AϏ��F]��>jS���̻&neJؾ�>��R.nL���r��ɟ�����8׈=��u�r�/�B
��n��>'��� ��]�.���H=:$>�Vu��<�>�vt� ���[���;���PW�D?�G�ԍ!9�"���4�<-����,?�)�cv�?Fϟ"#k?���%>?   qj�>����]0q?   ��==-����,��)�cv��Fϟ"#k��f��
�   P}xȾ"�xE��e�   �A������>�,? 1�y�?��2��?���%>?   qj�>����]0q?   ��==y���$ �>��E��f�>&neJؾ�>B
��n��>Vu��<�>���PW�D?"���4�<3�i־=g��\;�>���OM�=���C�>J��%��=��:;Ã>���1��=C�7d:Ã>�#w>@�s�(�-�� ����?%���|�?���D���?
   HXR_QD_V02      QUAD;c���>�.XUh��=�T�]�ã�@��z�=%��n~|����RU.p>iz�w�׻x9咎�>ߙ[u�v�5�̿�c==ɪ=�u|����)
>u�)���k9|́�>}�Ӂ�����9�����Uy�=�MY��;0��8}mP��>~ D�����LG��j�G�.�A�jȑ�<�>T�\�� ��
�����;���PW�D?���\�!9�#�[��4�<���Cv-?b���9�?T+۰�?�9��'�?   \xj�>����]0q?   X�==���Cv-���m`U��T+۰���9��'��   8{xȾ"�xE��e�   �?��X�ji��,?b���9�?�ˬ޷�?V�ٚ$?   \xj�>����]0q?   X�==;c���>x9咎�>k9|́�>�8}mP��>jȑ�<�>���PW�D?#�[��4�<��
��ܾ=��5�6?�>�]��L�=�c`�sB�>�k����=G���GÃ>,���=F�<1GÃ>�-�X?W>@��hK���@~��?�z�4D?�?���D���?   HXR_COR      KICKER;c���>�.XUh��=�T�]�ã�@��z�=%��n~|����RU.p>iz�w�׻x9咎�>ߙ[u�v�5�̿�c==ɪ=�u|����)
>u�)���k9|́�>}�Ӂ�����9�����Uy�=�MY��;0��8}mP��>~ D�����LG��j�G�.�A�jȑ�<�>T�\�� ��
�����;���PW�D?���\�!9�#�[��4�<���Cv-?b���9�?T+۰�?�9��'�?   \xj�>����]0q?   X�==���Cv-���m`U��T+۰���9��'��   8{xȾ"�xE��e�   �?��X�ji��,?b���9�?�ˬ޷�?V�ٚ$?   \xj�>����]0q?   X�==;c���>x9咎�>k9|́�>�8}mP��>jȑ�<�>���PW�D?#�[��4�<��
��ܾ=��5�6?�>�]��L�=�c`�sB�>�k����=G���GÃ>,���=F�<1GÃ>�-�X?W>@��hK���@~��?�z�4D?�?���D���?   HXR_BPM      MONI;c���>�.XUh��=�T�]�ã�@��z�=%��n~|����RU.p>iz�w�׻x9咎�>ߙ[u�v�5�̿�c==ɪ=�u|����)
>u�)���k9|́�>}�Ӂ�����9�����Uy�=�MY��;0��8}mP��>~ D�����LG��j�G�.�A�jȑ�<�>T�\�� ��
�����;���PW�D?���\�!9�#�[��4�<���Cv-?b���9�?T+۰�?�9��'�?   \xj�>����]0q?   X�==���Cv-���m`U��T+۰���9��'��   8{xȾ"�xE��e�   �?��X�ji��,?b���9�?�ˬ޷�?V�ٚ$?   \xj�>����]0q?   X�==;c���>x9咎�>k9|́�>�8}mP��>jȑ�<�>���PW�D?#�[��4�<��
��ܾ=��5�6?�>�]��L�=�c`�sB�>�k����=G���GÃ>,���=F�<1GÃ>�-�X?W>@��hK���@~��?�z�4D?�?�B����?
   HXR_QD_V02      QUAD��N��>���'g�޽�8�&
����ɯ����=����������2�>KUϊ�׻�{��0�>y�!�}=:�^��f�/4��J[=��/�Q��=�%�n�f�;"\E��[�>ٔ�LpN����%e*�����6�=�dU)G�0��iPR��>0�� �����N�����(f�D�S�<�>�9m�� ��n��6���;���PW�D?Y^t�!9�O^��4�<[E�=�
-?���?/9r��?���7?   �yj�>����]0q?   v�==[E�=�
-� 6�_�t�/9r������7�   {xȾ"�xE��e�   �?���`κ�,?���?� �=wr?�m�U8�>   �yj�>����]0q?   v�==��N��>�{��0�>"\E��[�>�iPR��>S�<�>���PW�D?O^��4�<��0��=�<ϗC�>h���L�=��hLB�>�e���=�.��SÃ>`�����=o�!SÃ>|h#[�S>@� �e�M@�E:�L��?4)N����?��9�ߓ�?   HXR_DR_QD_ED      DRIF�"Z���>��t-V�޽_�PM�0��g槨a�=�xi��n���J@�>�>�j��>�׻�{��0�>�HpU}=:�^��f�s%�i�J[=��/�Q��=��1عf�;�3 ��>\z�N���^��
��o��c�=#ʰ���2��iPR��>���#�����N���c�s��D��'�=�>cև ��бh���;���PW�D?��T|�!9�C��4�<�O�,�,?���?����W?���7?   t�j�>����]0q?   ��==�O�,�,� 6�_�t�����W����7�   �yxȾ"�xE��e�   �>��_�0���,?���?�X����?�m�U8�>   t�j�>����]0q?   ��==�"Z���>�{��0�>�3 ��>�iPR��>�'�=�>���PW�D?C��4�<��0��=�<ϗC�>t���L�=%��hLB�>�e���=�.��SÃ>b�����=p�!SÃ>#��2�=@J��4?@y%�څn�?�����)�?��%OH@	   HXR_DR_V3      DRIF!�����>g"72��ɽz�c(ˇ��8�]Z=�*�]��";��H#">L2��	&ӻ�{��0�>*ؔ��rT�:�^��f���GB�K[=��/�Q��=�7��|g�;)e��p��>�5F�]�=��q�>�#�KD�{函ߤ"���a��iPR��>����������N���^���D�$D��@�>����Z ����|���;���PW�D?!��H�!9�S�%>�4�<�m���?���?dp�4^?���7?    Cl�>����]0q?   j?=������ 6�_�t�dp�4^����7�    xȾ"�xE��e�   � ���m���?���?^Ysx
?�m�U8�>    Cl�>����]0q?   j?=!�����>�{��0�>)e��p��>�iPR��>$D��@�>���PW�D?S�%>�4�<��0��=�<ϗC�>����L�=6��hLB�>�e���=�.��SÃ>d�����=q�!SÃ>1�q�@c�M@�A!�T�@�Ҙ��!��R.Ţ�}@   HXR_DR_QD_ED      DRIF�¢�<�>kk=�,ɽTJS�JŇ�*���W=�B�'bF���F�N?">W޿��ӻ�{��0�>������V�:�^��f���+��K[=��/�Q��=]D0�g�;>��0�S�>�L�F�=w�$͒�#��[�VE�����{��a��iPR��>���N������N����aۯ��D�S�R A�>F��Z ����k����;���PW�D?<��P�!9��	�k�4�<W\j��?���?7_p$�?���7?   �Jl�>����]0q?   :q?=���2?� 6�_�t�7_p$�����7�   �xȾ"�xE��e�   ����W\j��?���?Q�� �k?�m�U8�>   �Jl�>����]0q?   :q?=�¢�<�>�{��0�>>��0�S�>�iPR��>S�R A�>���PW�D?�	�k�4�<��0��=�<ϗC�>����L�=6��hLB�>�e���=�.��SÃ>d�����=q�!SÃ>�;�3�%@~����@<��W:L@��P������ �ފ@
   HXR_QD_V03      QUADh�RZ,�>��4�p���'�lŇ�NA�w�0s=n�i��J��\��g�N">ގF�ӻ�F��U�>���Պ!s��{P���Q�v�����p�Zo��\�>�3#�St���*yM!`�>[�
��-�=�;Z$�����∽�����a�.o~���>��J/���ǣ0L�q�3VL=�l A�>#Z ��S�$����;���PW�D?'��j�!9�s�#s�4�<ЩR^i�?�Z�����>V$��?
�6;#o�>   �Kl�>����]0q?   "r?=��Vv�*����,�?�V$����w$���   hxȾ"�xE��e�   ����ЩR^i�?�Z�����>�����z?
�6;#o�>   �Kl�>����]0q?   "r?=h�RZ,�>�F��U�>�*yM!`�>.o~���>�l A�>���PW�D?s�#s�4�<I���=�.�g1C�>��r�aM�=A�mC�>�Zr>��=���NÃ>(�ZL��=!Q$NÃ> ���@�M�+�1�?�{-��j@�V�׿�� �ފ@   HXR_COR      KICKERh�RZ,�>��4�p���'�lŇ�NA�w�0s=n�i��J��\��g�N">ގF�ӻ�F��U�>���Պ!s��{P���Q�v�����p�Zo��\�>�3#�St���*yM!`�>[�
��-�=�;Z$�����∽�����a�.o~���>��J/���ǣ0L�q�3VL=�l A�>#Z ��S�$����;���PW�D?'��j�!9�s�#s�4�<ЩR^i�?�Z�����>V$��?
�6;#o�>   �Kl�>����]0q?   "r?=��Vv�*����,�?�V$����w$���   hxȾ"�xE��e�   ����ЩR^i�?�Z�����>�����z?
�6;#o�>   �Kl�>����]0q?   "r?=h�RZ,�>�F��U�>�*yM!`�>.o~���>�l A�>���PW�D?s�#s�4�<I���=�.�g1C�>��r�aM�=A�mC�>�Zr>��=���NÃ>(�ZL��=!Q$NÃ> ���@�M�+�1�?�{-��j@�V�׿�� �ފ@   HXR_BPM      MONIh�RZ,�>��4�p���'�lŇ�NA�w�0s=n�i��J��\��g�N">ގF�ӻ�F��U�>���Պ!s��{P���Q�v�����p�Zo��\�>�3#�St���*yM!`�>[�
��-�=�;Z$�����∽�����a�.o~���>��J/���ǣ0L�q�3VL=�l A�>#Z ��S�$����;���PW�D?'��j�!9�s�#s�4�<ЩR^i�?�Z�����>V$��?
�6;#o�>   �Kl�>����]0q?   "r?=��Vv�*����,�?�V$����w$���   hxȾ"�xE��e�   ����ЩR^i�?�Z�����>�����z?
�6;#o�>   �Kl�>����]0q?   "r?=h�RZ,�>�F��U�>�*yM!`�>.o~���>�l A�>���PW�D?s�#s�4�<I���=�.�g1C�>��r�aM�=A�mC�>�Zr>��=���NÃ>(�ZL��=!Q$NÃ> ���@�M�+�1�?�{-��j@�V�׿�;a.�@
   HXR_QD_V03      QUAD�fO�2�>��>�ſ=
D���ć���l�hC�=�}�b��p��Zo">�a�4+ӻD��u}L�>υWK�4���J�v��c=Ń��j��PG�6��>6��0N»Ǣ[�>f�����ȶ9���#�A���􈽭�+,��a��2(�x�>��z�<�=�@���O�,�����C;Y���@�>X)�Y ��S�����;���PW�D?J	|��!9����t�4�<���Uc�?�
P��f�>��7Q �?����2�>   Ll�>����]0q?   Nr?=����//���������7Q ����_dC�   hxȾ"�xE��e�   �������Uc�?�
P��f�>���Hv?����2�>   Ll�>����]0q?   Nr?=�fO�2�>D��u}L�>Ǣ[�>�2(�x�>Y���@�>���PW�D?���t�4�<N<�.L�=�%r��B�>Օ	�N�=_��a�C�>�����=���JÃ>(���=�]SJÃ>�j}�@�v�0���u��_@R���$�?n�)�m�@   HXR_DR_QD_ED      DRIF�@���x�>N��+?<�=��������yyf����=�0�ꕽ��s�#>��Ò�ӻD��u}L�>$��n ���J�v��c=�qt�j��PG�6��>�01N»�.��'�>y�6�̭����%��#��|�a�������a��2(�x�>2�'v=�=�@���O���,��C;э4�@�>�HwY ���&����;���PW�D?ftG׏!9����s�4�<��>��?�
P��f�>ti��s�?����2�>   `Ml�>����]0q?   �s?=��=u�t�������ti��s����_dC�   �xȾ"�xE��e�   P�����>��?�
P��f�> 9QE??����2�>   `Ml�>����]0q?   �s?=�@���x�>D��u}L�>�.��'�>�2(�x�>э4�@�>���PW�D?���s�4�<M<�.L�=�%r��B�>Օ	�N�=_��a�C�>�����=���JÃ>(���=�]SJÃ>���^~s@�*���T�w��@�s����?����6V@	   HXR_DR_V4      DRIF.#ѫO�>�d@'k��=��g񴀽�B�)�=�y�D�����Ɖ0>�~�޻D��u}L�>X��C֜t��J�v��c=��P_�i��PG�6��> ��4N»o�	�F}�>��d�������B���e�coP��7�-�.X��2(�x�>���vL�=�@���O� f�䳆C;���=�>�j�N ��Qx1o���;���PW�D?�k&�!9�}�#f�4�<l���"?�
P��f�>���(��?����2�>   �jl�>����]0q?   ֏?=�wL	� ����������(�����_dC�   `	xȾ"�xE��e�   ����l���"?�
P��f�>�v�?����2�>   �jl�>����]0q?   ֏?=.#ѫO�>D��u}L�>o�	�F}�>�2(�x�>���=�>���PW�D?}�#f�4�<J<�.L�=�%r��B�>ҕ	�N�=\��a�C�>�����=���JÃ>(���=�]SJÃ>�PϐA�!@�3on���B�Y�
@�-�a���?	":��@	   HXR_DR_CT      DRIF�s�MT��>�ֈ5��=qܥ�T�~�#˒�2��=j���@��a��'1>u1�W�޻D��u}L�>��b
�s��J�v��c=I����i��PG�6��>�+ �4N»�l���>q�Q�4����9�����0<W���[{���4W��2(�x�>;"��M�=�@���O��3?��C;�W4�=�>h��M ��$�D>���;���PW�D?�E���!9�1~�d�4�<�{Ð[,#?�
P��f�>3{aB�
?����2�>   hml�>����]0q?   V�?=����� �������3{aB�
���_dC�   �xȾ"�xE��e�   �����{Ð[,#?�
P��f�>4Şu]L?����2�>   hml�>����]0q?   V�?=�s�MT��>D��u}L�>�l���>�2(�x�>�W4�=�>���PW�D?1~�d�4�<N<�.L�=�%r��B�>Օ	�N�=_��a�C�>�����=���JÃ>(���=�]SJÃ>�X����"@�l��b�����4N�	@RƔ��f�?	":��@   HXR_WA_M_OU      WATCH�s�MT��>�ֈ5��=qܥ�T�~�#˒�2��=j���@��a��'1>u1�W�޻D��u}L�>��b
�s��J�v��c=I����i��PG�6��>�+ �4N»�l���>q�Q�4����9�����0<W���[{���4W��2(�x�>;"��M�=�@���O��3?��C;�W4�=�>h��M ��$�D>���;���PW�D?�E���!9�1~�d�4�<�{Ð[,#?�
P��f�>3{aB�
?����2�>   hml�>����]0q?   V�?=����� �������3{aB�
���_dC�   �xȾ"�xE��e�   �����{Ð[,#?�
P��f�>4Şu]L?����2�>   hml�>����]0q?   V�?=�s�MT��>D��u}L�>�l���>�2(�x�>�W4�=�>���PW�D?1~�d�4�<N<�.L�=�%r��B�>Օ	�N�=_��a�C�>�����=���JÃ>(���=�]SJÃ>�X����"@�l��b�����4N�	@RƔ��f�?i����@   HXR_DR_QD_ED      DRIFB|����>N��`��=�8C�}�ߌ��q�=���ڄ�����z1>�C78/i߻D��u}L�>Xao�s��J�v��c=k����i��PG�6��>6�5N»'�r�Ĥ�>��}���R��[�Ok��ϝ����Ͼ��V��2(�x�>��wN�=�@���O��5µ�C;��Ԋ=�>��%M ��gP�$���;���PW�D?����!9�)��c�4�<�!T�^#?�
P��f�>C��I�?����2�>   �nl�>����]0q?   ��?=��*F`!�������C��I����_dC�   �xȾ"�xE��e�   t����!T�^#?�
P��f�>.��V�?����2�>   �nl�>����]0q?   ��?=B|����>D��u}L�>'�r�Ĥ�>�2(�x�>��Ԋ=�>���PW�D?)��c�4�<L<�.L�=�%r��B�>ҕ	�N�=\��a�C�>�����=���JÃ>(���=�]SJÃ>�;R�"@�-X �G�����]	@��ͥ���?��J�,�@	   HXR_QD_FH      QUAD|�Z�* �>�c��=.��:D;}������=�O'���l���1>J��s}߻�� �>�'��q�j���?�s9��vS�	�j��y�$>3=|�?1��$j ���>׍I�t:=�-Hh�?�W�̃l���������V��)-qu�>f �M��<����T�m��A��i5;���=�>,�kM ��T�+ ���;���PW�D?t��!�!9����d�4�<���Je#?���`ͼ�>�:p�b�?W�hв��>   �nl�>����]0q?   Ɠ?=�(l9~!�C����D��:p�b��4τ���   �xȾ"�xE��e�   �������Je#?���`ͼ�>�{x�?W�hв��>   �nl�>����]0q?   Ɠ?=|�Z�* �>�� �>$j ���>�)-qu�>���=�>���PW�D?���d�4�<�e��N�=����B�>>���L�=?#
KB�>\|&+ �=��rÃ>���3* �=��(rÃ>N��xW�"@��*���?�J��M	@%��L�|���J�,�@   HXR_COR      KICKER|�Z�* �>�c��=.��:D;}������=�O'���l���1>J��s}߻�� �>�'��q�j���?�s9��vS�	�j��y�$>3=|�?1��$j ���>׍I�t:=�-Hh�?�W�̃l���������V��)-qu�>f �M��<����T�m��A��i5;���=�>,�kM ��T�+ ���;���PW�D?t��!�!9����d�4�<���Je#?���`ͼ�>�:p�b�?W�hв��>   �nl�>����]0q?   Ɠ?=�(l9~!�C����D��:p�b��4τ���   �xȾ"�xE��e�   �������Je#?���`ͼ�>�{x�?W�hв��>   �nl�>����]0q?   Ɠ?=|�Z�* �>�� �>$j ���>�)-qu�>���=�>���PW�D?���d�4�<�e��N�=����B�>>���L�=?#
KB�>\|&+ �=��rÃ>���3* �=��(rÃ>N��xW�"@��*���?�J��M	@%��L�|���J�,�@   HXR_BPM      MONI|�Z�* �>�c��=.��:D;}������=�O'���l���1>J��s}߻�� �>�'��q�j���?�s9��vS�	�j��y�$>3=|�?1��$j ���>׍I�t:=�-Hh�?�W�̃l���������V��)-qu�>f �M��<����T�m��A��i5;���=�>,�kM ��T�+ ���;���PW�D?t��!�!9����d�4�<���Je#?���`ͼ�>�:p�b�?W�hв��>   �nl�>����]0q?   Ɠ?=�(l9~!�C����D��:p�b��4τ���   �xȾ"�xE��e�   �������Je#?���`ͼ�>�{x�?W�hв��>   �nl�>����]0q?   Ɠ?=|�Z�* �>�� �>$j ���>�)-qu�>���=�>���PW�D?���d�4�<�e��N�=����B�>>���L�=?#
KB�>\|&+ �=��rÃ>���3* �=��(rÃ>N��xW�"@��*���?�J��M	@%��L�|�c�u|@	   HXR_QD_FH      QUAD������>,�k�Ƚ�o���|�Y����3�=cY�����c��~m�1>�^�N}߻���`WY�>8�l�
�^��]KX��e��e�R\�k=��s����-�L�;���*��>h���=���3��z`4ϋ�@m#G�V��G�����>̂N��<jW�9��y��ׁ��;c΂=�>8(ZM ���^%���;���PW�D?_i�E�!9����k�4�<��k	_#?w��-C ?����?�8z�I`�>   �ol�>����]0q?   `�?=��G�!�L�5<� �������w�xR��   �xȾ"�xE��e�   ������k	_#?w��-C ?����?�8z�I`�>   �ol�>����]0q?   `�?=������>���`WY�>���*��>�G�����>c΂=�>���PW�D?���k�4�<��R�S�=�T��B�>��~ǇK�=iX�[�@�>��Hi �=q���Ã>@��Uh �=�y��Ã>lԓ��"@��H�E@s�œ>^	@��_俅�s�A@   HXR_DR_QD_ED      DRIF�"z��>�N2
אȽ�0a�T�{�hf���=����x���7C��1>��:�S߻���`WY�>�lh�x�`��]KX��e�+�\�k=��s������ ��;�Ox���>�Z�TQ��=�J�%��}A[�y��/8F�Y�V��G�����>=���<jW�9��y�#�1���;���=�>���L ��Vu�X���;���PW�D?�����!9�|��4�<E�5��,#?w��-C ?y˽�	�?�8z�I`�>   �tl�>����]0q?   :�?=�5�8c� �L�5<� �y˽�	���w�xR��   �xȾ"�xE��e�   ����E�5��,#?w��-C ?T��x�?�8z�I`�>   �tl�>����]0q?   :�?=�"z��>���`WY�>�Ox���>�G�����>���=�>���PW�D?|��4�<��R�S�=�T��B�>��~ǇK�=iX�[�@�>��Hi �=q���Ã>A��Uh �=�y��Ã>ie��f"@����@�<��	@�q3�������j	@   HXR_DR_WIG_ED      DRIFm�<�lo�>ީ	��@ǽU���}�v�D� I�=jV-<9"���%��b1>��T��޻���`WY�>��C֪�d��]KX��e�`{�_�k=��s�������l�;_Ym<0�>'�7�=���B���cJ�����;����RV��G�����> "�<jW�9��y��G��ĕ;$���=�>�T��J ������;���PW�D?�h��!9��n�I�4�<sl�tp"?w��-C ?�6��*?�8z�I`�>   ؈l�>����]0q?   p�?=��>�n �L�5<� ��6��*��w�xR��   � xȾ"�xE��e�   ����sl�tp"?w��-C ?�9|\�?�8z�I`�>   ؈l�>����]0q?   p�?=m�<�lo�>���`WY�>_Ym<0�>�G�����>$���=�>���PW�D?�n�I�4�<��R�S�=�T��B�>��~ǇK�=iX�[�@�>��Hi �=q���Ã>@��Uh �=�y��Ã>NK�z�� @Z��� @�.��J@�,>�����>O� @	   HXR_WIG_0      WIGGLERQ�A/C��>�ܺ�A���G�sDas��C�A�h=�Y� �����90>p6�8ٻ���`WY�>'�kޯ}��Sq~f�e��6��g�k=��s����`�7<�;#�Km9�>,���F��=;p�t���Y'̚��vrڮ�T���^����>y��ݕ�<��>�؄y�����ʚ;���7E�>�/My ��)v ����;���PW�D?3��e�!9��ٱ��4�<����t�?w��-C ?����?&�UVH�>   P�l�>����]0q?   P�?=�+�]�L�5<� ������sW�8��   �xȾ"�xE��e�   (������t�?w��-C ?6�S��)?&�UVH�>   P�l�>����]0q?   P�?=Q�A/C��>���`WY�>#�Km9�>��^����>���7E�>���PW�D?�ٱ��4�<��R�S�=�T��B�>��~ǇK�=mX�[�@�>��Hi �=u���Ã>G��Uh �=�y��Ã>�aܣK	@����Y�?�L��@m��$��z�{Y�� @   HXR_DR_WIG_ED      DRIFV*�K��>I�i�d�����&w�_Ψ��d=1�s�r��Y���%0>�rVϥ�ػ���`WY�>��%�����Sq~f�e�j)j�k=��s����5�`��;�Q��y�>�S�*��=p���,��Ѽ8F�
�����^iT���^����>�ʩ���<��>�؄y�-H���;�^}E�>W��w ����m����;���PW�D? y��!9�J
��4�<S��4�?w��-C ?'v>2�?&�UVH�>   @�l�>����]0q?   x�?=�@���L�5<� �'v>2��sW�8��   �xȾ"�xE��e�   $��S��4�?w��-C ?6	���X?&�UVH�>   @�l�>����]0q?   x�?=V*�K��>���`WY�>�Q��y�>��^����>�^}E�>���PW�D?J
��4�<��R�S�=�T��B�>��~ǇK�=mX�[�@�>��Hi �=q���Ã>A��Uh �=�y��Ã>@Q�@G�J�/��?��g��( @�#��p��.L�c!@   HXR_DR_QD_ED	      DRIF[$�$��>Bv�:c��ԸM&Lx��j8�]�c=�j�S^D���.��i0>�jD�7sػ���`WY�>U�n�/���Sq~f�e��h�j�k=��s�����I[�;Ԥ�eJ�>���5K�=F�^���#�H��_��'��8\T���^����>qa����<��>�؄y�����;��E�>���<w ���;�����;���PW�D?�%&�!9��\���4�<���K�?w��-C ?�r�%%?&�UVH�>   ��l�>����]0q?   R�?=�����L�5<� ��r�%%�sW�8��   �xȾ"�xE��e�   \�����K�?w��-C ?��;ʦ�?&�UVH�>   ��l�>����]0q?   R�?=[$�$��>���`WY�>Ԥ�eJ�>��^����>��E�>���PW�D?�\���4�<��R�S�=�T��B�>��~ǇK�=mX�[�@�>��Hi �=q���Ã>A��Uh �=�y��Ã>Zk�m��@k[KA>��?Q�h%~ @��_����[��!@	   HXR_QD_DH      QUAD�*�����>�(:'�4����α�x������l=�aẤA��n5:p0>�����pػ�_����>d�Џ���\�J���;�����a]��>:6�>	��Le���0-Y�Q�>,��1�3=�A$����]B�k��RbeYRT�x���4��>��=�<`&�l�I����zc�3;	|�E�>�b�*w ���Hx����;���PW�D? %�)�!9�j����4�<܃��7�?(�z�H�>K��0?U�C�Ә�>   \�l�>����]0q?   �?=��H����6Z��K��0�j�]��/�   XxȾ"�xE��e�   H��܃��7�?(�z�H�>뿅�y�?U�C�Ә�>   \�l�>����]0q?   �?=�*�����>�_����>�0-Y�Q�>x���4��>	|�E�>���PW�D?j����4�<�����=�Oz�(C�>Up��.L�=�m��A�>->�bj�=�>GR�>3��pi�="���>���e��@b0A1��?��#]x� @�л��t�[��!@   HXR_COR      KICKER�*�����>�(:'�4����α�x������l=�aẤA��n5:p0>�����pػ�_����>d�Џ���\�J���;�����a]��>:6�>	��Le���0-Y�Q�>,��1�3=�A$����]B�k��RbeYRT�x���4��>��=�<`&�l�I����zc�3;	|�E�>�b�*w ���Hx����;���PW�D? %�)�!9�j����4�<܃��7�?(�z�H�>K��0?U�C�Ә�>   \�l�>����]0q?   �?=��H����6Z��K��0�j�]��/�   XxȾ"�xE��e�   H��܃��7�?(�z�H�>뿅�y�?U�C�Ә�>   \�l�>����]0q?   �?=�*�����>�_����>�0-Y�Q�>x���4��>	|�E�>���PW�D?j����4�<�����=�Oz�(C�>Up��.L�=�m��A�>->�bj�=�>GR�>3��pi�="���>���e��@b0A1��?��#]x� @�л��t�[��!@   HXR_BPM      MONI�*�����>�(:'�4����α�x������l=�aẤA��n5:p0>�����pػ�_����>d�Џ���\�J���;�����a]��>:6�>	��Le���0-Y�Q�>,��1�3=�A$����]B�k��RbeYRT�x���4��>��=�<`&�l�I����zc�3;	|�E�>�b�*w ���Hx����;���PW�D? %�)�!9�j����4�<܃��7�?(�z�H�>K��0?U�C�Ә�>   \�l�>����]0q?   �?=��H����6Z��K��0�j�]��/�   XxȾ"�xE��e�   H��܃��7�?(�z�H�>뿅�y�?U�C�Ә�>   \�l�>����]0q?   �?=�*�����>�_����>�0-Y�Q�>x���4��>	|�E�>���PW�D?j����4�<�����=�Oz�(C�>Up��.L�=�m��A�>->�bj�=�>GR�>3��pi�="���>���e��@b0A1��?��#]x� @�л��t���-��!@	   HXR_QD_DH      QUAD�'�C��>����Y�=K�ih`�x��)`��Us=�����P��s��� 0>q�A4~ػ@'h�U�>�ā� ��C4j�J�b=�{n�q�|�G�xX�s>�.�"�����Y^]|J�>/j*�R$���U,Aќ��O��d���)bU;T�kDblml�>�̺�2�=W�]fds=����I�A;wh��E�>���w ����#����;���PW�D?�YM�!9��M���4�<�P;�.�?��[%�G�>�ߔ,?l!����>   ��l�>����]0q?   N�?=�3�c���r؍`��ߔ,�|Pݮt��   XxȾ"�xE��e�   `���P;�.�?��[%�G�>�<<a��?l!����>   ��l�>����]0q?   N�?=�'�C��>@'h�U�>�Y^]|J�>kDblml�>wh��E�>���PW�D?�M���4�<=���[�=*;[�C�>�0��L�=5��cB�>C�n�=���N>��)m�=�w�2N>�)+�9�@�i5�^Vѿ��] @+/�lS��?<y��R4!@   HXR_DR_QD_ED
      DRIF:�U$��>Rv���=�a�)�
z���2
��s=��ͯ���>��k0>��P��ػ@'h�U�>�����C4j�J�b=4�p]q�|�G�xX�s>I-�8����~$JO��>�r��ͷ������2.z�%��yѴ��S�kDblml�>�@�4�=W�]fds=�&K�A;Qr�\E�> ž�v ����R����;���PW�D?�����!9�ﴃ��4�<�վ�?��[%�G�>��z��?l!����>   t�l�>����]0q?   (�?=�~6���r؍`���z���|Pݮt��   �xȾ"�xE��e�   ����վ�?��[%�G�>��'U0�?l!����>   t�l�>����]0q?   (�?=:�U$��>@'h�U�>~$JO��>kDblml�>Qr�\E�>���PW�D?ﴃ��4�<=���[�=*;[�C�>�0��L�=5��cB�>?�n�=���N>��)m�=�w�2N>ȿ��@	!�$�ҿ5��F�* @��3U%J�?���)�!@   HXR_DR_WIG_ED      DRIFfF�����>�P��ث�=��<���}����>�u=S��������m�σ1>�g>��ڻ@'h�U�>��/�ۃ�C4j�J�b=�b�.o�|�G�xX�s>{fR��������>;�>��)B5����^�G7)�v�0��7��W���	R�kDblml�>��j�:�=W�]fds=��N�P�A;`^]�D�>3ʞ�u ���E���;���PW�D?M:m��!9�����4�<;��f��?��[%�G�>��]�-O?l!����>   ��l�>����]0q?   X�?=��Ɋr1�r؍`���]�-O�|Pݮt��   �xȾ"�xE��e�    ��;��f��?��[%�G�>q��)?l!����>   ��l�>����]0q?   X�?=fF�����>@'h�U�>���>;�>kDblml�>`^]�D�>���PW�D?����4�<>���[�=*;[�C�>�0��L�=5��cB�>@�n�=���N>��)m�=�w�2N>�A5e��@��c�׿e�(���@�_�}x��?Б�a!%@	   HXR_WIG_0      WIGGLERhr�l�>*��'�8�=~� 
�`��*����=C:�w�ܤ�B�QDp;>�
\���@'h�U�>�}?w�3ɋP,�b=yg��|�G�xX�s>��h2���u%�z5.�>1�^A�'��O�������o{�Ē�7��t�|"���`��>��|{��=��w�G.s=x�C��A;uv�L�>�o-� ���G+���;���PW�D?��h&""9���55�<�QB�Md?��[%�G�>j�Z�?���Yk��>   $�l�>����]0q?   6�?=2��E��r؍`��y&�u��o�ӊC���   �'xȾ"�xE��e�   �/���QB�Md?��[%�G�>j�Z�?���Yk��>   $�l�>����]0q?   6�?=hr�l�>@'h�U�>u%�z5.�>��`��>uv�L�>���PW�D?��55�<;���[�=(;[�C�>~0��L�=1��cB�>?�n�=���N>��)m�=�w�2N>b��`�@�y���wE@�Yr%��?t�8�%@   HXR_DR_WIG_ED      DRIF`p;j��>r�[<���=/� �~}�"Ō���=-��|�����|�v�<>!j�3V�@'h�U�>��vTmu�3ɋP,�b=9��|�G�xX�s>�
��2���H%���
�>9��7����c�Z�ļ]�[-iՑ������i���`��>8�����=��w�G.s={�l'�A;!D
�K�>3p[.� ���=����;���PW�D?҂_$"9��� 5�<C�B5�?��[%�G�>�eNOy?���Yk��>   \�l�>����]0q?   n�?=)�����r؍`��j����o�ӊC���   �(xȾ"�xE��e�   �1��C�B5�?��[%�G�>�eNOy?���Yk��>   \�l�>����]0q?   n�?=`p;j��>@'h�U�>H%���
�>��`��>!D
�K�>���PW�D?�� 5�<?���[�=+;[�C�>�0��L�=5��cB�>?�n�=���N>��)m�=�w�2N>Q��_@j�T+�O�@�i�
@)�%���?(�\�ן%@   HXR_DR_QD_ED      DRIFn���3��>����=��h퐁|���-:�=Y4�����4�2�.�<>l8���@'h�U�>��\'��t�3ɋP,�b=�E���|�G�xX�s>0��2������nV��>�Q�]颽�\�M����_LÓ���Sh@%�ܺ��`��>o����=��w�G.s=K�N�	�A;ʻ�YK�>�~d� ��љy����;���PW�D? �҃$"9�6�45�<!��.�?��[%�G�>gL��S?���Yk��>   <�l�>����]0q?   L�?=��}��r؍`���<zz�o�ӊC���   �(xȾ"�xE��e�   P2��!��.�?��[%�G�>gL��S?���Yk��>   <�l�>����]0q?   L�?=n���3��>@'h�U�>���nV��>��`��>ʻ�YK�>���PW�D?6�45�<;���[�=(;[�C�>~0��L�=1��cB�>?�n�=���N>��)m�=�w�2N>,�8��@C�����h�r��	@���?U}���%@	   HXR_QD_FH      QUAD���h���>����Z  =N�g�b@|�`J���{�=����å�j�g ��<>sZsB��8zS��>��δ�n������8�m���@BU=�����>���	�;E�~A��>�T}�8P��:���^���\���q�π��:x����b�>�>�dy�=���eކU=Jq�C��A;㥝SK�>a1Xݢ ���x���;���PW�D?��4�$"9�wU5�<��B�A�?4�3����>jm�vO?��DD:�>   t�l�>����]0q?   ��?=#%ْ����Ŗ2[�bq��ul�b^�3��   �(xȾ"�xE��e�   h2����B�A�?4�3����>jm�vO?��DD:�>   t�l�>����]0q?   ��?=���h���>8zS��>E�~A��>x����b�>㥝SK�>���PW�D?wU5�<��]��=f�G�lB�>$	�K�=N���@�>����=�X�ss>����=����r>��}�@g�~��k�O]�Ђ	@Av��N?U}���%@	   HXR_WA_OU      WATCH���h���>����Z  =N�g�b@|�`J���{�=����å�j�g ��<>sZsB��8zS��>��δ�n������8�m���@BU=�����>���	�;E�~A��>�T}�8P��:���^���\���q�π��:x����b�>�>�dy�=���eކU=Jq�C��A;㥝SK�>a1Xݢ ���x���;���PW�D?��4�$"9�wU5�<��B�A�?4�3����>jm�vO?��DD:�>   t�l�>����]0q?   ��?=#%ْ����Ŗ2[�bq��ul�b^�3��   �(xȾ"�xE��e�   h2����B�A�?4�3����>jm�vO?��DD:�>   t�l�>����]0q?   ��?=���h���>8zS��>E�~A��>x����b�>㥝SK�>���PW�D?wU5�<��]��=f�G�lB�>$	�K�=N���@�>����=�X�ss>����=����r>��}�@g�~��k�O]�Ђ	@Av��N?