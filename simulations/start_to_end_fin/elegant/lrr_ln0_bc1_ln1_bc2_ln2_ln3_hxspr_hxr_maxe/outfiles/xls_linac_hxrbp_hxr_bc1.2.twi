SDDS1
!# little-endian
&description text="Twiss parameters--input: xls_linac_hxrbp_hxr_bc1.track.ele  lattice: xls_linac_hxrbp_hxr.2.lte", contents="Twiss parameters", &end
&parameter name=Step, description="Simulation step", type=long, &end
&parameter name=SVNVersion, description="SVN version number", type=string, fixed_value=unknown, &end
&parameter name=nux, symbol="$gn$r$bx$n", units="1/(2$gp$r)", description="Horizontal tune", type=double, &end
&parameter name=dnux/dp, symbol="$gx$r$bx$n", units="1/(2$gp$r)", description="Horizontal chromaticity", type=double, &end
&parameter name=dnux/dp2, symbol="$gx$r$bx2$n", units="1/(2$gp$r)", description="Horizontal 2nd-order chromaticity", type=double, &end
&parameter name=dnux/dp3, symbol="$gx$r$bx3$n", units="1/(2$gp$r)", description="Horizontal 3rd-order chromaticity", type=double, &end
&parameter name=Ax, symbol="A$bx$n", units=m, description="Horizontal acceptance", type=double, &end
&parameter name=AxLocation, units=m, description="Location of horizontal acceptance limit", type=double, &end
&parameter name=nuy, symbol="$gn$r$by$n", units="1/(2$gp$r)", description="Vertical tune", type=double, &end
&parameter name=dnuy/dp, symbol="$gx$r$by$n", units="1/(2$gp$r)", description="Vertical chromaticity", type=double, &end
&parameter name=dnuy/dp2, symbol="$gx$r$by2$n", units="1/(2$gp$r)", description="Vertical 2nd-order chromaticity", type=double, &end
&parameter name=dnuy/dp3, symbol="$gx$r$by3$n", units="1/(2$gp$r)", description="Vertical 3rd-order chromaticity", type=double, &end
&parameter name=Ay, symbol="A$by$n", units=m, description="Vertical acceptance", type=double, &end
&parameter name=AyLocation, units=m, description="Location of vertical acceptance limit", type=double, &end
&parameter name=deltaHalfRange, symbol="$gDd$r/2", description="Half range of momentum offset for chromatic tune spread evaluation", type=double, &end
&parameter name=nuxChromUpper, symbol="$gx$r$bu$n", description="Upper limit of x tune due to chromaticity and deltaRange", type=double, &end
&parameter name=nuxChromLower, symbol="$gx$r$bu$n", description="Lower limit of x tune due to chromaticity and deltaRange", type=double, &end
&parameter name=nuyChromUpper, symbol="$gy$r$bu$n", description="Upper limit of y tune due to chromaticity and deltaRange", type=double, &end
&parameter name=nuyChromLower, symbol="$gy$r$bu$n", description="Lower limit of y tune due to chromaticity and deltaRange", type=double, &end
&parameter name=Stage, description="Stage of computation", type=string, &end
&parameter name=pCentral, units="m$be$nc", description="Central momentum", type=double, &end
&parameter name=dbetax/dp, units=m, description="Derivative of betax with momentum offset", type=double, &end
&parameter name=dbetay/dp, units=m, description="Derivative of betay with momentum offset", type=double, &end
&parameter name=dalphax/dp, description="Derivative of alphax with momentum offset", type=double, &end
&parameter name=dalphay/dp, description="Derivative of alphay with momentum offset", type=double, &end
&parameter name=etax2, symbol="$gc$r$bx2$n", units=m, description="Second-order dispersion (for matched or periodic case only)", type=double, &end
&parameter name=etay2, symbol="$gc$r$by2$n", units=m, description="Second-order dispersion (for matched or periodic case only)", type=double, &end
&parameter name=etax3, symbol="$gc$r$bx3$n", units=m, description="Third-order dispersion (for matched or periodic case only)", type=double, &end
&parameter name=etay3, symbol="$gc$r$by3$n", units=m, description="Third-order dispersion (for matched or periodic case only)", type=double, &end
&parameter name=etaxp2, symbol="$gc$r$bx2$n$a'$n", units=m, description="Second-order dispersion (for matched or periodic case only)", type=double, &end
&parameter name=etayp2, symbol="$gc$r$by2$n$a'$n", units=m, description="Second-order dispersion (for matched or periodic case only)", type=double, &end
&parameter name=etaxp3, symbol="$gc$r$bx3$n$a'$n", units=m, description="Third-order dispersion (for matched or periodic case only)", type=double, &end
&parameter name=etayp3, symbol="$gc$r$by3$n$a'$n", units=m, description="Third-order dispersion (for matched or periodic case only)", type=double, &end
&parameter name=betaxMin, units=m, description="Minimum betax", type=double, &end
&parameter name=betaxAve, units=m, description="Average betax", type=double, &end
&parameter name=betaxMax, units=m, description="Maximum betax", type=double, &end
&parameter name=betayMin, units=m, description="Minimum betay", type=double, &end
&parameter name=betayAve, units=m, description="Average betay", type=double, &end
&parameter name=betayMax, units=m, description="Maximum betay", type=double, &end
&parameter name=etaxMax, units=m, description="Maximum absolute value of etax", type=double, &end
&parameter name=etayMax, units=m, description="Maximum absolute value of etay", type=double, &end
&parameter name=waistsx, description="Number of changes in the sign of alphax", type=long, &end
&parameter name=waistsy, description="Number of changes in the sign of alphay", type=long, &end
&parameter name=dnux/dAx, units=1/m, description="Horizontal tune shift with horizontal amplitude", type=double, &end
&parameter name=dnux/dAy, units=1/m, description="Horizontal tune shift with vertical amplitude", type=double, &end
&parameter name=dnuy/dAx, units=1/m, description="Vertical tune shift with horizontal amplitude", type=double, &end
&parameter name=dnuy/dAy, units=1/m, description="Vertical tune shift with vertical amplitude", type=double, &end
&parameter name=dnux/dAx2, units="1/m$a2$n", description="Horizontal tune shift with horizontal amplitude", type=double, &end
&parameter name=dnux/dAy2, units="1/m$a2$n", description="Horizontal tune shift with vertical amplitude", type=double, &end
&parameter name=dnux/dAxAy, units="1/m$a2$n", description="Horizontal tune shift with horizontal and vertical amplitude", type=double, &end
&parameter name=dnuy/dAx2, units="1/m$a2$n", description="Vertical tune shift with horizontal amplitude", type=double, &end
&parameter name=dnuy/dAy2, units="1/m$a2$n", description="Vertical tune shift with vertical amplitude", type=double, &end
&parameter name=dnuy/dAxAy, units="1/m$a2$n", description="Vertical tune shift with horizontal and vertical amplitude", type=double, &end
&parameter name=nuxTswaLower, description="Minimum horizontal tune from tune-shift-with-amplitude calculations", type=double, &end
&parameter name=nuxTswaUpper, description="Maximum horizontal tune from tune-shift-with-amplitude calculations", type=double, &end
&parameter name=nuyTswaLower, description="Minimum vertical tune from tune-shift-with-amplitude calculations", type=double, &end
&parameter name=nuyTswaUpper, description="Maximum vertical tune from tune-shift-with-amplitude calculations", type=double, &end
&parameter name=couplingIntegral, description="Coupling integral for difference resonance", type=double, &end
&parameter name=couplingDelta, description="Distance from difference resonance", type=double, &end
&parameter name=emittanceRatio, description="Emittance ratio from coupling integral", type=double, &end
&parameter name=alphac2, symbol="$ga$r$bc2$n", description="2nd-order momentum compaction factor", type=double, &end
&parameter name=alphac, symbol="$ga$r$bc$n", description="Momentum compaction factor", type=double, &end
&parameter name=I1, units=m, description="Radiation integral 1", type=double, &end
&parameter name=I2, units=1/m, description="Radiation integral 2", type=double, &end
&parameter name=I3, units="1/m$a2$n", description="Radiation integral 3", type=double, &end
&parameter name=I4, units=1/m, description="Radiation integral 4", type=double, &end
&parameter name=I5, units=1/m, description="Radiation integral 5", type=double, &end
&parameter name=ex0, units="$gp$rm", description="Damped horizontal emittance", type=double, &end
&parameter name=enx0, units="m$be$nc $gp$rm", description="Damped normalized horizontal emittance", type=double, &end
&parameter name=taux, units=s, description="Horizontal damping time", type=double, &end
&parameter name=Jx, description="Horizontal damping partition number", type=double, &end
&parameter name=tauy, units=s, description="Vertical damping time", type=double, &end
&parameter name=Jy, description="Vertical damping partition number", type=double, &end
&parameter name=Sdelta0, description="RMS fractional energy spread", type=double, &end
&parameter name=taudelta, units=s, description="Longitudinal damping time", type=double, &end
&parameter name=Jdelta, description="Longitudinal damping partition number", type=double, &end
&parameter name=U0, units=MeV, description="Energy loss per turn", type=double, &end
&column name=s, units=m, description=Distance, type=double,  &end
&column name=betax, symbol="$gb$r$bx$n", units=m, description="Horizontal beta-function", type=double,  &end
&column name=alphax, symbol="$ga$r$bx$n", description="Horizontal alpha-function", type=double,  &end
&column name=psix, symbol="$gy$r$bx$n", units=rad, description="Horizontal phase advance", type=double,  &end
&column name=etax, symbol="$gc$r$bx$n", units=m, description="Horizontal dispersion", type=double,  &end
&column name=etaxp, symbol="$gc$r$bx$n$a'$n", description="Slope of horizontal dispersion", type=double,  &end
&column name=xAperture, symbol="a$bx,eff$n", units=m, description="Effective horizontal aperture", type=double,  &end
&column name=betay, symbol="$gb$r$by$n", units=m, description="Vertical beta-function", type=double,  &end
&column name=alphay, symbol="$ga$r$by$n", description="Vertical alpha-function", type=double,  &end
&column name=psiy, symbol="$gy$r$by$n", units=rad, description="Vertical phase advance", type=double,  &end
&column name=etay, symbol="$gc$r$by$n", units=m, description="Vertical dispersion", type=double,  &end
&column name=etayp, symbol="$gc$r$by$n$a'$n", description="Slope of vertical dispersion", type=double,  &end
&column name=yAperture, symbol="a$by,eff$n", units=m, description="Effective vertical aperture", type=double,  &end
&column name=pCentral0, symbol="p$bcent$n", units="m$be$nc", description="Initial central momentum", type=double,  &end
&column name=ElementName, description="Element name", format_string=%10s, type=string,  &end
&column name=ElementOccurence, description="Occurence of element", format_string=%6ld, type=long,  &end
&column name=ElementType, description="Element-type name", format_string=%10s, type=string,  &end
&column name=ChamberShape, type=string,  &end
&column name=dI1, units=m, description="Contribution to radiation integral 1", type=double,  &end
&column name=dI2, units=1/m, description="Contribution to radiation integral 2", type=double,  &end
&column name=dI3, units="1/m$a2$n", description="Contribution to radiation integral 3", type=double,  &end
&column name=dI4, units=1/m, description="Contribution to radiation integral 4", type=double,  &end
&column name=dI5, units=1/m, description="Contribution to radiation integral 5", type=double,  &end
&data mode=binary, &end
.       NW���?�����
�                        ����������0�V��?XZ�L����                        ��������        NW���?NW���?��0�V��?��0�V��?   tunes uncorrected�����@D{����#��5S�I�	��Y;�4��h�b�                                                                �@�v��?e�{�ӊ5@�ʅ�P@p���@8�;�C@�$6�0@~��(�g�?                                                                                                                                      ��3�˿        ���Ȗ1f?�8�֫v]��J-,����ޮvܦ?Ů8yv�~?�+��w�^����;?��b�[t>x}��	�>�60��"@�ϝ�?�K�s��"@      �?�T����?>��9q�@�0b���?��5t/�>        ߷��y@�{�1���                              $@�d�t@�#�Q�%�?                              $@�����@   _BEG_      MARK                                                    ߷��y@�{�1���                              $@�d�t@�#�Q�%�?                              $@�����@   BNCH      CHARGE   ?                                                ߷��y@�{�1���                              $@�d�t@�#�Q�%�?                              $@�����@
   BC1_IN_CNT      CENTER   ?                                        V(�2�8�?"�Z���@��+�}�������R�?                      $@�Ǩ��K@��q��?XP��X��?                      $@�����@	   BC1_DR_V1      DRIF   ?                                        ��C(&��?�tA�'@�
9�����d��Ku:�?                      $@9����@�4��[��?��z���?                      $@�����@   BC1_DR_QD_ED      DRIF   ?                                        ���<���?�CWbJ�@�b���@���iۄ}�?                      $@��ZU?@FnǮN�?���U)�?                      $@�����@
   BC1_QD_V01      QUAD   ?                                        ���<���?�CWbJ�@�b���@���iۄ}�?                      $@��ZU?@FnǮN�?���U)�?                      $@�����@   BC1_COR      KICKER   ?                                        ���<���?�CWbJ�@�b���@���iۄ}�?                      $@��ZU?@FnǮN�?���U)�?                      $@�����@   BC1_BPM      MONI   ?                                        Bz9Q�$�?�B2_@�Wh�{�7����X�?                      $@����*�@/�r��&�?BG��m�?                      $@�����@
   BC1_QD_V01      QUAD   ?                                        �	�F���?k���^@;���5���ݼ�>�?                      $@p���@G��i��?��ۺ�p�?                      $@�����@   BC1_DR_QD_ED      DRIF   ?                                        Ʊ�4j@R���X�<@��.	��pfT�h�?                      $@y9=T��/@����
��:(^���?                      $@�����@	   BC1_DR_V2      DRIF   ?                                        ��S��@*�ԭA�<@ñҧ��P%R8t�?                      $@�$6�0@ҿUv�fqw���?                      $@�����@   BC1_DR_QD_ED      DRIF   ?                                        �_0��@𼈕�o=@t���B*+�s��K��?                      $@O�2
0@9q��	@����?                      $@�����@
   BC1_QD_V02      QUAD   ?                                        �_0��@𼈕�o=@t���B*+�s��K��?                      $@O�2
0@9q��	@����?                      $@�����@   BC1_COR      KICKER   ?                                        �_0��@𼈕�o=@t���B*+�s��K��?                      $@O�2
0@9q��	@����?                      $@�����@   BC1_BPM      MONI   ?                                        ���x�@{2��
�>@�) ��9�
 }b��?                      $@0����)/@x;8�!@L�2��?                      $@�����@
   BC1_QD_V02      QUAD   ?                                        �x��@t?̷��@@��W)��9�6�Ѓ?��?                      $@s��E��-@p����<!@HՋ~�?                      $@�����@   BC1_DR_QD_ED      DRIF   ?                                        �5w�@qM��G@� �?�qF����?                      $@�(#*�%@$����h@Kb��n�?                      $@�����@	   BC1_DR_V3      DRIF   ?                                        ��ԕ>@���aI@ѤN|`�?�d��]���?                      $@����y$@;{��@NI���~�?                      $@�����@   BC1_DR_QD_ED      DRIF   ?                                        ��cXg@g{��0J@�=��9��w��a��?                      $@e���s#@�� �§@�̋��?                      $@�����@
   BC1_QD_V03      QUAD   ?                                        ��cXg@g{��0J@�=��9��w��a��?                      $@e���s#@�� �§@�̋��?                      $@�����@   BC1_COR      KICKER   ?                                        ��cXg@g{��0J@�=��9��w��a��?                      $@e���s#@�� �§@�̋��?                      $@�����@   BC1_BPM      MONI   ?                                        �?�	�@q�!۰K@���@L3�rb����?                      $@������"@�3� =@S��9��?                      $@�����@
   BC1_QD_V03      QUAD   ?                                        ڑ�9��@sQ9�<�K@��Mg	�3��}H3���?                      $@P�;�a�!@ '����@���B��?                      $@�����@   BC1_DR_QD_ED      DRIF   ?                                        �[�@β:��O@�L{ ��4���?UE��?                      $@�wT?�q@䶫�#h@O#i��?                      $@�����@	   BC1_DR_V4      DRIF   ?                                        �UD&�@T��RpP@̤	'�B5�D�0���?                      $@]�LZ+@�����@a��I0�?                      $@�����@   BC1_DR_QD_ED      DRIF   ?                                        &�����@l�h���P@��;��t�"sƒ�?                      $@��'�9@�=�Ms�@��bh�G�?                      $@�����@
   BC1_QD_V04      QUAD   ?                                        &�����@l�h���P@��;��t�"sƒ�?                      $@��'�9@�=�Ms�@��bh�G�?                      $@�����@   BC1_COR      KICKER   ?                                        &�����@l�h���P@��;��t�"sƒ�?                      $@��'�9@�=�Ms�@��bh�G�?                      $@�����@   BC1_BPM      MONI   ?                                        Oc��@�ʅ�P@kQ=" @�g�v�?                      $@���+��@�-����?��vNM`�?                      $@�����@
   BC1_QD_V04      QUAD   ?                                        ;`�P1@��yGK�P@(��@]=_A��?                      $@�T6���@n(2]v��?�s4�sz�?                      $@�����@   BC1_DR_QD_ED      DRIF   ?                                        cBS�@H�5�(�O@�����q@��Y��+�?                      $@r�ߍnZ@���6�?��1I��?                      $@�����@	   BC1_DR_V5      DRIF   ?                                        ���) @8���k�L@S]�*
@��]�o�?                      $@5���@�\�{x�?	��� @                      $@�����@	   BC1_DR_20      DRIF   ?                                        ��SeL!@�$�ˆJ@�����@�oh1ǫ�?Bm�c��WR�
]#��      $@�[Ȯ@= �j:]�?>�X�<@                      $@�����@   BC1_DP_DIP1   	   CSRCSBEND   ?��{ n�3?��ޮv܆?Ů8yv�^?
,��w����D"$?��SeL"@)���%G@�w9ڀ@�no�.��?h���X���WR�
]#��      $@�4a�/@FcJX��?!te�p@                      $@�����@   BC1_DR_SIDE_C      CSRDRIFT   ?                                        ������'@̺�,�4@���<s@�R����?�0{��ͿWR�
]#��      $@��#ea@�_NNfп7��}8@                      $@�����@   BC1_DR_SIDE      DRIF   ?                                        �s�ʜ(@#�	�1@|S��2@)�䎅�?~��(�gϿ              $@�NN/3@.�Ȟpӿ�z��@                      $@�����@   BC1_DP_DIP2   	   CSRCSBEND   ?۞��#z����ޮv܆?Ů8yv�^?�+��w��쒬�3/?K ���O)@^��\�//@�,��;	@���?1�?~��(�gϿ              $@�;i��*@ ��B�ؿ=!Ǌ@                      $@�����@   BC1_DR_CENT      DRIF   ?                                        K ���O)@^��\�//@�,��;	@���?1�?~��(�gϿ              $@�;i��*@ ��B�ؿ=!Ǌ@                      $@�����@   BC1_BPM      MONI   ?                                        ~3�+1*@�a��h�*@ ?AD@�U*���?~��(�gϿ              $@�~�<�^@��9��1޿Þg@ 	@                      $@�����@   BC1_DR_CENT      DRIF   ?                                        ��	%�*@��?��g&@~�6�@��%���?�0{��ͿWR�
]#�?      $@�>��	@LF٫�3�U ���	@                      $@�����@   BC1_DP_DIP3   	   CSRCSBEND   ?���#z����ޮv܆?Ů8yv�^?�+��w�r����>��	%�+@��!@��~��1@�Ŗ����?e\]8�VɿWR�
]#�?      $@�ޙ�I@ѽ������OO2h
@                      $@�����@   BC1_DR_SIDE_C      CSRDRIFT   ?                                        c�E�0@4)'�?��)O�?�B����?�m�c��WR�
]#�?      $@bU�]/t&@r��ҋ��ye�!@                      $@�����@   BC1_DR_SIDE      DRIF   ?                                        r~�4i1@�@�v��?�6�8���?
-V� @      |�              $@�2��NQ(@����D�,��۱g@                      $@�����@   BC1_DP_DIP4   	   CSRCSBEND   ?��{ n�3?��ޮv܆?Ů8yv�^?,��w񾸻�ْ��>r~�4i�1@�������?H���aӿ�����@      |�              $@��@��*@�򢹡���t�� �@                      $@�����@   BC1_DR_20_C      CSRDRIFT   ?                                        r~�4i�1@�������?H���aӿ�����@      |�              $@��@��*@�򢹡���t�� �@                      $@�����@	   BC1_WA_OU      WATCH   ?                                        