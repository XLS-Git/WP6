SDDS1
!# little-endian
&description text="sigma matrix--input: xls_linac_hxrbp_hxr_hbp_1.track.ele  lattice: xls_linac_hxrbp_hxr.11.lte", contents="sigma matrix", &end
&parameter name=Step, description="Simulation step", type=long, &end
&parameter name=SVNVersion, description="SVN version number", type=string, fixed_value=unknown, &end
&column name=s, units=m, description=Distance, type=double,  &end
&column name=ElementName, description="Element name", format_string=%10s, type=string,  &end
&column name=ElementOccurence, description="Occurence of element", format_string=%6ld, type=long,  &end
&column name=ElementType, description="Element-type name", format_string=%10s, type=string,  &end
&column name=s1, symbol="$gs$r$b1$n", units=m, description="sqrt(<x*x>)", type=double,  &end
&column name=s12, symbol="$gs$r$b12$n", units=m, description="<x*xp'>", type=double,  &end
&column name=s13, symbol="$gs$r$b13$n", units="m$a2$n", description="<x*y>", type=double,  &end
&column name=s14, symbol="$gs$r$b14$n", units=m, description="<x*y'>", type=double,  &end
&column name=s15, symbol="$gs$r$b15$n", units="m$a2$n", description="<x*s>", type=double,  &end
&column name=s16, symbol="$gs$r$b16$n", units=m, description="<x*delta>", type=double,  &end
&column name=s17, symbol="$gs$r$b17$n", units="m*s", description="<x*t>", type=double,  &end
&column name=s2, symbol="$gs$r$b2$n", description="sqrt(<x'*x'>)", type=double,  &end
&column name=s23, symbol="$gs$r$b23$n", units=m, description="<x'*y>", type=double,  &end
&column name=s24, symbol="$gs$r$b24$n", description="<x'*y'>", type=double,  &end
&column name=s25, symbol="$gs$r$b25$n", units=m, description="<x'*s>", type=double,  &end
&column name=s26, symbol="$gs$r$b26$n", description="<x'*delta>", type=double,  &end
&column name=s27, symbol="$gs$r$b27$n", units=s, description="<x'*t>", type=double,  &end
&column name=s3, symbol="$gs$r$b3$n", units=m, description="sqrt(<y*y>)", type=double,  &end
&column name=s34, symbol="$gs$r$b34$n", units=m, description="<y*y'>", type=double,  &end
&column name=s35, symbol="$gs$r$b35$n", units="m$a2$n", description="<y*s>", type=double,  &end
&column name=s36, symbol="$gs$r$b36$n", units=m, description="<y*delta>", type=double,  &end
&column name=s37, symbol="$gs$r$b37$n", units="m*s", description="<y*t>", type=double,  &end
&column name=s4, symbol="$gs$r$b4$n", description="sqrt(<y'*y')>", type=double,  &end
&column name=s45, symbol="$gs$r$b45$n", units=m, description="<y'*s>", type=double,  &end
&column name=s46, symbol="$gs$r$b46$n", description="<y'*delta>", type=double,  &end
&column name=s47, symbol="$gs$r$b47$n", units=s, description="<y'*t>", type=double,  &end
&column name=s5, symbol="$gs$r$b5$n", units=m, description="sqrt(<s*s>)", type=double,  &end
&column name=s56, symbol="$gs$r$b56$n", units=m, description="<s*delta>", type=double,  &end
&column name=s57, symbol="$gs$r$b57$n", units="m*s", description="<s*t>", type=double,  &end
&column name=s6, symbol="$gs$r$b6$n", description="sqrt(<delta*delta>)", type=double,  &end
&column name=s67, symbol="$gs$r$b67$n", units=s, description="<delta*t>", type=double,  &end
&column name=s7, symbol="$gs$r$b7$n", description="sqrt(<t*t>)", type=double,  &end
&column name=ma1, symbol="max$sb$ex$sb$e", units=m, description="maximum absolute value of x", type=double,  &end
&column name=ma2, symbol="max$sb$ex'$sb$e", description="maximum absolute value of x'", type=double,  &end
&column name=ma3, symbol="max$sb$ey$sb$e", units=m, description="maximum absolute value of y", type=double,  &end
&column name=ma4, symbol="max$sb$ey'$sb$e", description="maximum absolute value of y'", type=double,  &end
&column name=ma5, symbol="max$sb$e$gD$rs$sb$e", units=m, description="maximum absolute deviation of s", type=double,  &end
&column name=ma6, symbol="max$sb$e$gd$r$sb$e", description="maximum absolute value of delta", type=double,  &end
&column name=ma7, symbol="max$sb$e$gD$rt$sb$e", units=s, description="maximum absolute deviation of t", type=double,  &end
&column name=minimum1, symbol="x$bmin$n", units=m, type=double,  &end
&column name=minimum2, symbol="x'$bmin$n", units=m, type=double,  &end
&column name=minimum3, symbol="y$bmin$n", units=m, type=double,  &end
&column name=minimum4, symbol="y'$bmin$n", units=m, type=double,  &end
&column name=minimum5, symbol="$gD$rs$bmin$n", units=m, type=double,  &end
&column name=minimum6, symbol="$gd$r$bmin$n", units=m, type=double,  &end
&column name=minimum7, symbol="t$bmin$n", units=s, type=double,  &end
&column name=maximum1, symbol="x$bmax$n", units=m, type=double,  &end
&column name=maximum2, symbol="x'$bmax$n", units=m, type=double,  &end
&column name=maximum3, symbol="y$bmax$n", units=m, type=double,  &end
&column name=maximum4, symbol="y'$bmax$n", units=m, type=double,  &end
&column name=maximum5, symbol="$gD$rs$bmax$n", units=m, type=double,  &end
&column name=maximum6, symbol="$gd$r$bmax$n", units=m, type=double,  &end
&column name=maximum7, symbol="t$bmax$n", units=s, type=double,  &end
&column name=Sx, symbol="$gs$r$bx$n", units=m, description=sqrt(<(x-<x>)^2>), type=double,  &end
&column name=Sxp, symbol="$gs$r$bx'$n", description=sqrt(<(x'-<x'>)^2>), type=double,  &end
&column name=Sy, symbol="$gs$r$by$n", units=m, description=sqrt(<(y-<y>)^2>), type=double,  &end
&column name=Syp, symbol="$gs$r$by'$n", description=sqrt(<(y'-<y'>)^2>), type=double,  &end
&column name=Ss, symbol="$gs$r$bs$n", units=m, description=sqrt(<(s-<s>)^2>), type=double,  &end
&column name=Sdelta, symbol="$gs$bd$n$r", description=sqrt(<(delta-<delta>)^2>), type=double,  &end
&column name=St, symbol="$gs$r$bt$n", units=s, description=sqrt(<(t-<t>)^2>), type=double,  &end
&column name=ex, symbol="$ge$r$bx$n", units=m, description="geometric horizontal emittance", type=double,  &end
&column name=enx, symbol="$ge$r$bx,n$n", units=m, description="normalized horizontal emittance", type=double,  &end
&column name=ecx, symbol="$ge$r$bx,c$n", units=m, description="geometric horizontal emittance less dispersive contributions", type=double,  &end
&column name=ecnx, symbol="$ge$r$bx,cn$n", units=m, description="normalized horizontal emittance less dispersive contributions", type=double,  &end
&column name=ey, symbol="$ge$r$by$n", units=m, description="geometric vertical emittance", type=double,  &end
&column name=eny, symbol="$ge$r$by,n$n", units=m, description="normalized vertical emittance", type=double,  &end
&column name=ecy, symbol="$ge$r$by,c$n", units=m, description="geometric vertical emittance less dispersive contributions", type=double,  &end
&column name=ecny, symbol="$ge$r$by,cn$n", units=m, description="normalized vertical emittance less dispersive contributions", type=double,  &end
&column name=betaxBeam, symbol="$gb$r$bx,beam$n", units=m, description="betax for the beam, excluding dispersive contributions", type=double,  &end
&column name=alphaxBeam, symbol="$ga$r$bx,beam$n", description="alphax for the beam, excluding dispersive contributions", type=double,  &end
&column name=betayBeam, symbol="$gb$r$by,beam$n", units=m, description="betay for the beam, excluding dispersive contributions", type=double,  &end
&column name=alphayBeam, symbol="$ga$r$by,beam$n", description="alphay for the beam, excluding dispersive contributions", type=double,  &end
&data mode=binary, &end
&                 _BEG_      MARK�`,��V�>����C½�0�6�����50���=c"?Ҷ0w=�%�hѽ�n�3LĴ;�L	��>�?�?L�=7/Ep[���R�\�D4�=�=�-6�=+�mR�;'�Z����>zDwlsн�2�� 6�&5�3���	mJ��s��;Wi�>y��F�=.���ʃ=a!'�@\;ԓY��3�>ɫ�I3�=g��O�5�;���B��3?N�[p��"<վ�u�<��SY5M?�RVn�_?%Tp+�+"?xl�=�	?   \#�>���q��k?   Ā =,Ñ����RVn�_�%Tp+�+"�}O]�S�   $��ɾ���df�   $R6���SY5M?@�g�o?��e�N�?xl�=�	?   \#�>���q��k?   Ā =�`,��V�>�L	��>'�Z����>�;Wi�>ԓY��3�>���B��3?վ�u�<N )�T.�=Nn~Tx#�>�����=X�4��>������=j��`Ƀ>j�S��=��Զ]Ƀ>'"�N���?O#kmU�?p��D�)@��`�y@           BNCH      CHARGE�`,��V�>����C½�0�6�����50���=c"?Ҷ0w=�%�hѽ�n�3LĴ;�L	��>�?�?L�=7/Ep[���R�\�D4�=�=�-6�=+�mR�;'�Z����>zDwlsн�2�� 6�&5�3���	mJ��s��;Wi�>y��F�=.���ʃ=a!'�@\;ԓY��3�>ɫ�I3�=g��O�5�;���B��3?N�[p��"<վ�u�<��SY5M?�RVn�_?%Tp+�+"?xl�=�	?   \#�>���q��k?   Ā =,Ñ����RVn�_�%Tp+�+"�}O]�S�   $��ɾ���df�   $R6���SY5M?@�g�o?��e�N�?xl�=�	?   \#�>���q��k?   Ā =�`,��V�>�L	��>'�Z����>�;Wi�>ԓY��3�>���B��3?վ�u�<N )�T.�=Nn~Tx#�>�����=X�4��>������=j��`Ƀ>j�S��=��Զ]Ƀ>'"�N���?O#kmU�?p��D�)@��`�y@        
   FITT_HBP_0      MARK�`,��V�>����C½�0�6�����50���=c"?Ҷ0w=�%�hѽ�n�3LĴ;�L	��>�?�?L�=7/Ep[���R�\�D4�=�=�-6�=+�mR�;'�Z����>zDwlsн�2�� 6�&5�3���	mJ��s��;Wi�>y��F�=.���ʃ=a!'�@\;ԓY��3�>ɫ�I3�=g��O�5�;���B��3?N�[p��"<վ�u�<��SY5M?�RVn�_?%Tp+�+"?xl�=�	?   \#�>���q��k?   Ā =,Ñ����RVn�_�%Tp+�+"�}O]�S�   $��ɾ���df�   $R6���SY5M?@�g�o?��e�N�?xl�=�	?   \#�>���q��k?   Ā =�`,��V�>�L	��>'�Z����>�;Wi�>ԓY��3�>���B��3?վ�u�<N )�T.�=Nn~Tx#�>�����=X�4��>������=j��`Ƀ>j�S��=��Զ]Ƀ>'"�N���?O#kmU�?p��D�)@��`�y@           HBP_CNT0      DRIF�`,��V�>����C½�0�6�����50���=c"?Ҷ0w=�%�hѽ�n�3LĴ;�L	��>�?�?L�=7/Ep[���R�\�D4�=�=�-6�=+�mR�;'�Z����>zDwlsн�2�� 6�&5�3���	mJ��s��;Wi�>y��F�=.���ʃ=a!'�@\;ԓY��3�>ɫ�I3�=g��O�5�;���B��3?N�[p��"<վ�u�<��SY5M?�RVn�_?%Tp+�+"?xl�=�	?   \#�>���q��k?   Ā =,Ñ����RVn�_�%Tp+�+"�}O]�S�   $��ɾ���df�   $R6���SY5M?@�g�o?��e�N�?xl�=�	?   \#�>���q��k?   Ā =�`,��V�>�L	��>'�Z����>�;Wi�>ԓY��3�>���B��3?վ�u�<N )�T.�=Nn~Tx#�>�����=X�4��>������=j��`Ƀ>j�S��=��Զ]Ƀ>'"�N���?O#kmU�?p��D�)@��`�y@��0-$�?   HBP_DP_SEPM1   	   CSRCSBENDK7�0:�>��A0ř=_�rT�ʊ��؈��t=� n j=�<[h���5�n�;C�����>��h�W�=�h��U�����~����4�+�8�0	阻���d�\��>s�S݋!ͽ��,��4=�om���U[���q;SzPi�>>��F�� � )hG�=���9�]���k�ʹ>��[_c
�=�{��Ǜ�;��D�84?�6|���<��F��<�>B��V�>$G��t$?�l#� ?|�?�	?   ��b�>�.z�~�l?   ��[=�l�����3DW���!��l#� �⺷2�S�   ��ʾl���,.f�   0kV��>B��V�>$G��t$?Z#�7?|�?�	?   ��b�>�.z�~�l?   ��[=K7�0:�>C�����>��d�\��>SzPi�>��k�ʹ>��D�84?��F��<�$���^�=�r�t�>F��hnU�=���g-e�>e��6��=��[��ȃ>ʮ ��=�㲡�ȃ>
��S,��?1Lj��?���C�k$@�ψn��@��0-$�?   HBP_DP_SEPM1   	   CSRCSBENDZ�PW��>���I�=Q4��^�p�� �R��K=Qpլ:w�S��a��#��z��Z����e$��>.��ƈ�=$���Zb�����❛�1�U>�]����ػ@�#���>,w�=\ɽ?��퐝A=h�@[lg���y^W�;c:��i�>�7�	�5.�k���c�=^[)#�k��KV�ι>�����=�;��;�����5?k{�ou	<���<�D��i?�^��0?�y��?�@����	?   dr��>�۸(�m?   �ȣ=�D��i��^��0��y���s�З�S�   ��ʾ�P)� f�   ("Y�e0�<?�C@f"A0?<7Nb��?�@����	?   dr��>�۸(�m?   �ȣ=Z�PW��>�e$��>@�#���>c:��i�>�KV�ι>�����5?���<��5;T�=�)v�-¦>��u�Uu�=�23�Hފ>I2��=Ձ

ȃ>5�����=<~7�ȃ>��]��]�?�ܐ���ܿH
5N�@I��rs�
@@�ƽ�?   HBP_DR_BP_C      CSRDRIFT���x.�>��Ic�?�=at��b=�h��z�i���&40��� �A�3��B(�*ɻ�e$��>��K9m�=$���Zb���H2y����*�Z��>�a��)�ػ���r��>�Ѿ�Ž��viE�>=*s
&��y3ݞ�~{;c:��i�>迍i�5.����A �=Z\Kk�F�/_4Ϲ>f�0���=<5=�P��;�����6?�l�*�<#����<=���5(%?�^��0?�	f)`3?�@����	?   lP��>^#���_m?   ��==���5(%��^��0��	f)`3�s�З�S�   lJʾ�zn�e�   �X�����"?�C@f"A0?�KU�ܙ?�@����	?   lP��>^#���_m?   ��=���x.�>�e$��>���r��>c:��i�>F�/_4Ϲ>�����6?#����<��5;T�=!�H7���>"�0�JX�=�{���>�I2��=�Q���ǃ>�S:���=v\�'�ǃ>eS����?���Ku�� |N�s@4r�!�/@m2�+��?	   HBP_DR_BP      DRIFFh\����>:P��x��=Ȃ�П�x=�H�s\}��p�a��$��7�<��*NjS�ӻ�e$��>�~��Q�=$���Zb����{7����*�Z��>�gGEH��ػ<���!O�>�+��K½�hp`[+:=*�� �H���nZ�ow;c:��i�>��s�5.����A �=L�h$k�
�?\Ϲ>�3o~��=]����;�����6?eb��<4ʦf��<>�/?�^��0?ǙBE�S?�@����	?   �
��>^#���_m?   �=>�/��^��0�ǙBE�S�s�З�S�   ��ʾ�zn�e�   �MX�&�K�g�,?�C@f"A0?`\��f?�@����	?   �
��>^#���_m?   �=Fh\����>�e$��><���!O�>c:��i�>
�?\Ϲ>�����6?4ʦf��<��5;T�=>�H7���>B�0�JX�=h}���>�I2��=�Q���ǃ>�S:���=�\�'�ǃ>�5�=�k@~�Q��'+�6��@��Cdo@:��|���?	   HBP_DR_BP      DRIF������>Y���:>�����~=�n�T冽L@�>!�����%C�)���cۻ�e$��>���@<l�=$���Zb��'}��<���*�Z��>���uf*�ػ�<���R�>:�����iJ��j�5=Z����k�����	as;c:��i�>�o�}�5.����A �=_D��8k���T��Ϲ>������=8k3gã�;�����6?0�}I��<�vY��<v��T4?�^��0?�)ajt?�@����	?   `���>^#���_m?   >/�=v��T4��^��0��)ajt�s�З�S�   �[ʾ�zn�e�   |�W���R03?�C@f"A0?���h?�@����	?   `���>^#���_m?   >/�=������>�e$��>�<���R�>c:��i�>��T��Ϲ>�����6?�vY��<׮5;T�=��H7���>H�0�JX�=p}���>�I2��=�Q���ǃ>�S:���=�\�'�ǃ>��hd�`@1��}����o�@�ȸ]�?��I�r�?	   HBP_DR_BP      DRIF���L��?�䈽��
>S��V��z=�'p���V�Z{��k�L��G���U��e��e$��>fi�!5�=$���Zb��f�ݠ��*�Z��>�|U�ػ�0�s:K�>GH�k�v��đfxs1=���ٿ��K?��q�n;c:��i�>��-�6.����A �=�v�Lk��o��Ϲ>.z�zE��=\Ӗ����;�����6?b�x'�<<��m,�<��
��9?�^��0?'s���)	?�@����	?   ���>^#���_m?   jQ�=��
��9��^��0�'s���)	�s�З�S�   �ʾ�zn�e�   @yW���1b8?�C@f"A0?��t&?�@����	?   ���>^#���_m?   jQ�=���L��?�e$��>�0�s:K�>c:��i�>�o��Ϲ>�����6?<��m,�<`�5;T�=��H7���>=�0�JX�=�u���>�I2��=�Q���ǃ>�S:���=�\�'�ǃ>��V��W@Q:�T��>�M(� @�*<���?��R��� @	   HBP_DR_V1      DRIFZ<ST�?8�Е�->k�3k��m=���'�� �M����tAĸX�K�L�!D٤��e$��>�H�qr=$���Zb���<Z�j���*�Z��>��{�p/�ػ�ޞG�>2Ȼ�E��H�6��E*=t�)����F�ƛ�g;c:��i�>��
�#6.����A �=P��9^k�Ʈ���Ϲ>
�A����=��Y0/��;�����6?'&�?q�<���K�<������=?�^��0?�j�U
`?�@����	?   ���>^#���_m?   "Ѽ=������=��^��0��j�U
`�s�З�S�   `{ʾ�zn�e�   $W�-7WhV<?�C@f"A0?��,	S� ?�@����	?   ���>^#���_m?   "Ѽ=Z<ST�?�e$��>�ޞG�>c:��i�>Ʈ���Ϲ>�����6?���K�<׮5;T�=��H7���>N�0�JX�=h����>�I2��=�Q���ǃ>�S:���=\�'�ǃ>hK	�� $@�:�@���D!�-�?*��I�?��R��� @   HBP_CNT0      DRIFZ<ST�?8�Е�->k�3k��m=���'�� �M����tAĸX�K�L�!D٤��e$��>�H�qr=$���Zb���<Z�j���*�Z��>��{�p/�ػ�ޞG�>2Ȼ�E��H�6��E*=t�)����F�ƛ�g;c:��i�>��
�#6.����A �=P��9^k�Ʈ���Ϲ>
�A����=��Y0/��;�����6?'&�?q�<���K�<������=?�^��0?�j�U
`?�@����	?   ���>^#���_m?   "Ѽ=������=��^��0��j�U
`�s�З�S�   `{ʾ�zn�e�   $W�-7WhV<?�C@f"A0?��,	S� ?�@����	?   ���>^#���_m?   "Ѽ=Z<ST�?�e$��>�ޞG�>c:��i�>Ʈ���Ϲ>�����6?���K�<׮5;T�=��H7���>N�0�JX�=h����>�I2��=�Q���ǃ>�S:���=\�'�ǃ>hK	�� $@�:�@���D!�-�?*��I�?�����*@   HBP_DR_QD_ED      DRIF���	�?^m&��>	F��h=�U�����������8%�Bo`L���|k-+��e$��>{x_KB`o=$���Zb��o��X����*�Z��>�l�>&C�ػX��DX��>g�f:���|���(=��#�G���}6�Daf;c:��i�>����&6.����A �=Km�ak��=�E�Ϲ>�f�̪��=���U7��;�����6?�n-}�<�̤P�<0@?ng�>?�^��0?�m0�Jq?�@����	?   �T��>^#���_m?   P��=0@?ng�>��^��0��m0�Jq�s�З�S�   tjʾ�zn�e�   W��<A=?�C@f"A0?�.��9 ?�@����	?   �T��>^#���_m?   P��=���	�?�e$��>X��DX��>c:��i�>�=�E�Ϲ>�����6?�̤P�<׮5;T�=��H7���>7�0�JX�=xu���>�I2��=�Q���ǃ>�S:���=�\�'�ǃ>�g��1%@�����[�:[vP��?���8�?�<�a�|@
   HBP_QD_V01      QUAD��El��?�kq-K��=	�����c=����d\��SS�W�����bn��L�-ɞo��^��D��>��Njp�e=��f�|��RA������B�-��Y1���Ȼ�ʦʧ<�>�n�3���-Y-I��'= ��������]�ule;�?c�O�>�R8-�;'��<���8�=�U5���d���qK�Ϲ>��&M���=O6:�;��;�����6?�:<��<;��US�<OzB{j??^����t?�0�B>A?v�*{U
?   ���>^#���_m?   ��=OzB{j?��okHL:��0�B>A�P����   �`ʾ�zn�e�   xW�u��!܂=?^����t?E����2 ?v�*{U
?   ���>^#���_m?   ��=��El��?^��D��>�ʦʧ<�>�?c�O�>��qK�Ϲ>�����6?;��US�<�H��U�=T��/�æ>��6
�Y�=��p��	�>sUNk��=#N)'�ǃ>��s$��=ϊ���ǃ>bfՁ��%@w��5�
�@�Ʋ��?ojͣ/:�?�<�a�|@   HBP_COR      KICKER��El��?�kq-K��=	�����c=����d\��SS�W�����bn��L�-ɞo��^��D��>��Njp�e=��f�|��RA������B�-��Y1���Ȼ�ʦʧ<�>�n�3���-Y-I��'= ��������]�ule;�?c�O�>�R8-�;'��<���8�=�U5���d���qK�Ϲ>��&M���=O6:�;��;�����6?�:<��<;��US�<OzB{j??^����t?�0�B>A?v�*{U
?   ���>^#���_m?   ��=OzB{j?��okHL:��0�B>A�P����   �`ʾ�zn�e�   xW�u��!܂=?^����t?E����2 ?v�*{U
?   ���>^#���_m?   ��=��El��?^��D��>�ʦʧ<�>�?c�O�>��qK�Ϲ>�����6?;��US�<�H��U�=T��/�æ>��6
�Y�=��p��	�>sUNk��=#N)'�ǃ>��s$��=ϊ���ǃ>bfՁ��%@w��5�
�@�Ʋ��?ojͣ/:�?�<�a�|@   HBP_BPM      MONI��El��?�kq-K��=	�����c=����d\��SS�W�����bn��L�-ɞo��^��D��>��Njp�e=��f�|��RA������B�-��Y1���Ȼ�ʦʧ<�>�n�3���-Y-I��'= ��������]�ule;�?c�O�>�R8-�;'��<���8�=�U5���d���qK�Ϲ>��&M���=O6:�;��;�����6?�:<��<;��US�<OzB{j??^����t?�0�B>A?v�*{U
?   ���>^#���_m?   ��=OzB{j?��okHL:��0�B>A�P����   �`ʾ�zn�e�   xW�u��!܂=?^����t?E����2 ?v�*{U
?   ���>^#���_m?   ��=��El��?^��D��>�ʦʧ<�>�?c�O�>��qK�Ϲ>�����6?;��US�<�H��U�=T��/�æ>��6
�Y�=��p��	�>sUNk��=#N)'�ǃ>��s$��=ϊ���ǃ>bfՁ��%@w��5�
�@�Ʋ��?ojͣ/:�??� ��@
   HBP_QD_V01      QUADE���?v=�c��ʽ��9sBR\=�2�1�뒽F����B��Y�i1�L�p4�gŪ�N3��[�>�?�},_=�L�t�P��#�]�!#��O'ј�=(C��a�=����>�y���󉽗+j�T '=��b�:���a�Y�d;L�|M��>>��� ��[��}=��&Hb�]�.��Ϲ>��x���=��r<��;�����6?��N��<&���S�<�P��1??�+���>����C?�=s-� ?   6���>^#���_m?   ���=�P��1?�x���������C���Ѕ/X��   �^ʾ�zn�e�   �W��V�B�=?�+���>jm���[ ?�=s-� ?   6���>^#���_m?   ���=E���?N3��[�>=����>L�|M��>.��Ϲ>�����6?&���S�<X�o_�W�=]h�XƦ>��:.X[�=��|���>�: � �=�~#��ǃ>'�����=_Q�*�ǃ>q/��=&@0cJNU�ɿ���xh4�?n�ߒ]��?�q$�%@   HBP_DR_QD_ED      DRIF��1��?x	�l�ʽ�|�	ͿP=�U<jV���-8딡C�����X�L� y�G���N3��[�>��b*v^=�L�t�P�����!#��O'ј�=p�Ē�a���I���>��a}���"�A�l&=b/}�듽�h�_d;L�|M��>��� ��[��}=H-�`�]�E�kj�Ϲ>�|%���=h�q�;��;�����6?RY\s��<��9iS�<�^p�p&??�+���>B�)�[a?�=s-� ?   F���>^#���_m?   ���=�^p�p&?�x�����B�)�[a���Ѕ/X��   �^ʾ�zn�e�   �W�uZ M�=?�+���>F�� ?�=s-� ?   F���>^#���_m?   ���=��1��?N3��[�>��I���>L�|M��>E�kj�Ϲ>�����6?��9iS�<W�o_�W�=[h�XƦ>��:.X[�=x�|���>�: � �=�~#��ǃ>(�����=_Q�*�ǃ>�LbW�F&@u���_ʿ5�ؑ���?���\L��??� ��@	   HBP_DR_V2      DRIF�:�J��?0���@ʽ��AOd���O�.��*pI��G��/���L����9(��N3��[�>�l�	��Z=�L�t�P�fY���!#��O'ј�=�~�%��a����w��>ʕ�B��#=s����"=��7:*i��D]��na;L�|M��> QC_�� ��[��}=1�QX�]��,p��Ϲ>������=�p:�:��;�����6?��RD~�<i��R�<R%��>?�+���>�s�-��?�=s-� ?   ����>^#���_m?   ���=R%��>�x������s�-�����Ѕ/X��   `^ʾ�zn�e�   W�������=?�+���>,c��8�?�=s-� ?   ����>^#���_m?   ���=�:�J��?N3��[�>���w��>L�|M��>�,p��Ϲ>�����6?i��R�<W�o_�W�=[h�XƦ>��:.X[�=`�|���>�: � �=�~#��ǃ>(�����=_Q�*�ǃ>�.L�t&@����̿�-3Z�?��s6e�hQ���w@	   HBP_DR_V2      DRIF���$�?�:��ɽV� ��x���W5�f��'�/��K��W%nr�L����7̲�N3��[�>����́W=�L�t�P��Q0�!#��O'ј�=���˟a��<~}��>;�w�R�=���g�"=/�@��搽_i|���[;L�|M��>������ ��[��}=?�P�]�Y�t��Ϲ>r�5���=���y9��;�����6?bLiz�<>��Q�<�i@��>?�+���>��D�A?�=s-� ?   ����>^#���_m?   � �=�i@��>�x�����F:�������Ѕ/X��   �]ʾ�zn�e�   8W�����_=?�+���>��D�A?�=s-� ?   ����>^#���_m?   � �=���$�?N3��[�>�<~}��>L�|M��>Y�t��Ϲ>�����6?>��Q�<W�o_�W�=[h�XƦ>��:.X[�=h�|���>�: � �=�~#��ǃ>'�����=_Q�*�ǃ>�D𜧦&@}eXϿ�&����?'y-�{ǿhQ���w@   HBP_CNT0      DRIF���$�?�:��ɽV� ��x���W5�f��'�/��K��W%nr�L����7̲�N3��[�>����́W=�L�t�P��Q0�!#��O'ј�=���˟a��<~}��>;�w�R�=���g�"=/�@��搽_i|���[;L�|M��>������ ��[��}=?�P�]�Y�t��Ϲ>r�5���=���y9��;�����6?bLiz�<>��Q�<�i@��>?�+���>��D�A?�=s-� ?   ����>^#���_m?   � �=�i@��>�x�����F:�������Ѕ/X��   �]ʾ�zn�e�   8W�����_=?�+���>��D�A?�=s-� ?   ����>^#���_m?   � �=���$�?N3��[�>�<~}��>L�|M��>Y�t��Ϲ>�����6?>��Q�<W�o_�W�=[h�XƦ>��:.X[�=h�|���>�: � �=�~#��ǃ>'�����=_Q�*�ǃ>�D𜧦&@}eXϿ�&����?'y-�{ǿ?� ��@   HBP_DR_QD_ED      DRIFl���?�G���ɽ?Q����{�+�mr���zǦ_L��M��vL������N3��[�>}��,z�V=�L�t�P���s�!#��O'ј�=��nVџa�a?��>Z�@��=�X�=�������::�Z;L�|M��>ê}��� ��[��}=WF_N�]�s�ٵ�Ϲ>�,���=[S�;9��;�����6?���9y�<p�Q�<ĥ�r&�>?�+���>�XE��?�=s-� ?   ����>^#���_m?   �=ĥ�r&�>�x�����j+q������Ѕ/X��   �]ʾ�zn�e�   <W�0b�"Y=?�+���>�XE��?�=s-� ?   ����>^#���_m?   �=l���?N3��[�>a?��>L�|M��>s�ٵ�Ϲ>�����6?p�Q�<W�o_�W�=[h�XƦ>��:.X[�=h�|���>�: � �=�~#��ǃ>(�����=_Q�*�ǃ>�f�&@�Sy��ϿH�/�>�?n~F�9̿��l� @
   HBP_QD_V01      QUADdD�?!L.9�Q4�sP�~�� �B�t�����K�)����`�CL��z���fT-aE�>�>�(��l=���� v=��\c�=��:�02>����s�;�~w�	B�>:(y~(H�=A㐓=�YT.e��$�x#q�Y;�Ԑ>�U�>1��Ĺ���)A|d=BYPs$V�x�Y�Ϲ>. ;���=�� �9��;�����6?l���y�<	+R�<���èr>?[u��{�#?a$3���?��O�x�>   &���>^#���_m?   b�=���èr>����� "���t7�����O�x��    ]ʾ�zn�e�   �W�DD��'=?[u��{�#?a$3���?,��Jn��>   &���>^#���_m?   b�=dD�?fT-aE�>�~w�	B�>�Ԑ>�U�>x�Y�Ϲ>�����6?	+R�<�����Y�=�w��Ȧ>���A]�=���m�>�p�R �=�{S�ȃ>/u��G �=C8.fȃ>*�zw&@�/Lc?�@��~C���?^V�Hk㿑�l� @   HBP_COR      KICKERdD�?!L.9�Q4�sP�~�� �B�t�����K�)����`�CL��z���fT-aE�>�>�(��l=���� v=��\c�=��:�02>����s�;�~w�	B�>:(y~(H�=A㐓=�YT.e��$�x#q�Y;�Ԑ>�U�>1��Ĺ���)A|d=BYPs$V�x�Y�Ϲ>. ;���=�� �9��;�����6?l���y�<	+R�<���èr>?[u��{�#?a$3���?��O�x�>   &���>^#���_m?   b�=���èr>����� "���t7�����O�x��    ]ʾ�zn�e�   �W�DD��'=?[u��{�#?a$3���?,��Jn��>   &���>^#���_m?   b�=dD�?fT-aE�>�~w�	B�>�Ԑ>�U�>x�Y�Ϲ>�����6?	+R�<�����Y�=�w��Ȧ>���A]�=���m�>�p�R �=�{S�ȃ>/u��G �=C8.fȃ>*�zw&@�/Lc?�@��~C���?^V�Hk㿑�l� @   HBP_BPM      MONIdD�?!L.9�Q4�sP�~�� �B�t�����K�)����`�CL��z���fT-aE�>�>�(��l=���� v=��\c�=��:�02>����s�;�~w�	B�>:(y~(H�=A㐓=�YT.e��$�x#q�Y;�Ԑ>�U�>1��Ĺ���)A|d=BYPs$V�x�Y�Ϲ>. ;���=�� �9��;�����6?l���y�<	+R�<���èr>?[u��{�#?a$3���?��O�x�>   &���>^#���_m?   b�=���èr>����� "���t7�����O�x��    ]ʾ�zn�e�   �W�DD��'=?[u��{�#?a$3���?,��Jn��>   &���>^#���_m?   b�=dD�?fT-aE�>�~w�	B�>�Ԑ>�U�>x�Y�Ϲ>�����6?	+R�<�����Y�=�w��Ȧ>���A]�=���m�>�p�R �=�{S�ȃ>/u��G �=C8.fȃ>*�zw&@�/Lc?�@��~C���?^V�Hk��e>�r@
   HBP_QD_V01      QUAD?m�!>$?!u�� �=��G0̀�v���a���z�9w�����aɼK��5���6��G���>�w�ͺy=r����=:��=�a�=��<�jA>O� qs�;<ZGR���>�9�y®=w`�fĿ=Qd��b��1���X;���jt�>h�����A��?�a�*I@��M�K|K�Ϲ>�[�����=�:c>��;�����6?�_����<!c��T�<�)���=?Jf0)_\2?�P"*�|?��$
�?   �e��>^#���_m?   :��=�)���=�̓8H�H1�����(E���$
��   DVʾ�zn�e�   ��V��O��#�<?Jf0)_\2?�P"*�|?S�\�_"?   �e��>^#���_m?   :��=?m�!>$?�G���><ZGR���>���jt�>K|K�Ϲ>�����6?!c��T�<ę�B�[�=�`���˦>���_�=�u�_�>.�9�� �=�m�%Dȃ>r��y� �=h�-�<ȃ>}k��׻%@P� '/@��WJA��?��U�U�	�.��@   HBP_DR_QD_ED      DRIF!֎�>�?�-Lď��s��"4�����і���W�+��,m�K��������G���>�5U:�\{=r����=2=�a�a�=��<�jA>y�T��s�;_H�SN�>_Y�b�=s�|E
=�F��z��k���K8X;���jt�>�nFt���A��?�a���NƼ�M� ��5�Ϲ>
�����=	b��F��;�����6?t2c[��<3�7+Z�<AaϏ�=?Jf0)_\2?=���=?��$
�?   �E��>^#���_m?   �J�=AaϏ�=�̓8H�H1����;����$
��   ,Iʾ�zn�e�   �V� ��7�;?Jf0)_\2?=���=?S�\�_"?   �E��>^#���_m?   �J�=!֎�>�?�G���>_H�SN�>���jt�> ��5�Ϲ>�����6?3�7+Z�<N��B�[�=9`���˦>���_�=	�u�_�>/�9�� �=�m�%Dȃ>s��y� �=h�-�<ȃ>y"�m�$@t̔��@��
W>�?�� �Tg�z)�@	   HBP_DR_V1      DRIF��К6?�|5����}(;�È���.��Q���} ����>��܃F�F�0{��G���>�7�':��=r����=�`b�=��<�jA>;�t�;�/��W�>\�GĚ�=�fV۽�=����,	��G�y�@KT;���jt�>��k黯�A��?�a��\N�M��ʥ�й>���'��=-�@�{��;�����6?Ɩ.���<�!.�z�<�w0��F8?Jf0)_\2?�YZ��	?��$
�?   Ь��>^#���_m?   ,"�=�w0��F8�̓8H�H1�px��f���$
��   H�ʾ�zn�e�   4�V�N�B��Y7?Jf0)_\2?�YZ��	?S�\�_"?   Ь��>^#���_m?   ,"�=��К6?�G���>�/��W�>���jt�>�ʥ�й>�����6?�!.�z�<���B�[�="a���˦>� �_�=֞u�_�>.�9�� �=�m�%Dȃ>r��y� �=h�-�<ȃ>H.��@���W�@�E�ڐ @!�Mt���}��qJ@	   HBP_DR_BP      DRIFSs�*y��>!����l�j��I�bZ"�ާ���"a�}��^�bA��}R��ۻ�G���>~�-���=r����=�!c�=��<�jA>��L�t�;O9e�c�>t�����=�c�=��źr����RDHΡO;���jt�>=΄Ү�A��?�a�0�F��M�����-й>#an���=������;�����6?:Y�6�<Z?�/��<�%� ��2?Jf0)_\2?$vy�1<?��$
�?   @���>^#���_m?   ���=�%� ��2�̓8H�H1�r!׶�����$
��   ��ʾ�zn�e�   DWV�*�1�n*2?Jf0)_\2?$vy�1<?S�\�_"?   @���>^#���_m?   ���=Ss�*y��>�G���>O9e�c�>���jt�>����-й>�����6?Z?�/��<��B�[�=Ha���˦>���_�=ٖu�_�>3�9�� �=�m�%Dȃ>w��y� �=k�-�<ȃ>kapU@@���2d�@F���<�@�A�����F���װ@	   HBP_DR_BP      DRIF�uK�
�>o�<�a��L�T�����s�c���-.܂򷖽ytт8�&R;�WKԻ�G���>�n4�R�=r����= &(��c�=��<�jA>�2��-u�;dₐ�Y�>k�)^f�=�t�c(O	=�����O��!�F;���jt�>��0 ��A��?�a�@��$
�M�����Wй>�5]����=�O�w���;�����6?s�>��<��^���<���(s�*?Jf0)_\2?S�L��G?��$
�?   ���>^#���_m?   /�=���(s�*�̓8H�H1��=�!_���$
��   �?ʾ�zn�e�   XV�4AB�)?Jf0)_\2?S�L��G?S�\�_"?   ���>^#���_m?   /�=�uK�
�>�G���>dₐ�Y�>���jt�>����Wй>�����6?��^���<���B�[�="a���˦>���_�=��u�_�>+�9�� �=�m�%Dȃ>o��y� �=f�-�<ȃ>�U��8@�71�V@�q�B�@�fn���V�'.�@	   HBP_DR_BP      DRIFyE�>���>�2^c���:�+�`��iW��a}�ٽS̘��龎T��,������ɻ�G���>-�q�O�=r����=x82�_d�=��<�jA>�i��u�;Q�L�>�;J�}��=_c�E��<��[l�򒽵�ⰱv;;���jt�>Ⓕ���A��?�a��N.�g�M��RZ~�й>�	�@f��=���0��;�����6?�⎘��<�Qc��<��>��.?Jf0)_\2?�]%��?��$
�?   ,��>^#���_m?   n��=�z t�̓8H�H1���^y�����$
��   T�ʾ�zn�e�   h�U���>��.?Jf0)_\2?�]%��?S�\�_"?   ,��>^#���_m?   n��=yE�>���>�G���>Q�L�>���jt�>�RZ~�й>�����6?�Qc��<��B�[�=+a���˦>*��_�=ؘu�_�>8�9�� �=�m�%Dȃ>|��y� �=n�-�<ȃ>��u���?�q�
��?rL�@�,@G,���O���Za�>@	   HBP_DR_BP      DRIF˼�p�D�>��-<�սd��p�U��]��j��x,, %y��CuW��{����{���G���>��o�M�=r����=�J<Y
e�=��<�jA>Q�3Tv�;L��)��>��j��~�=�r-h�L�<�&ED���`wY&#;���jt�>�4�V��A��?�a�n����M�L���й>g�����=�w��m��;�����6?�_��E�<�����<����?Jf0)_\2?�QmMM�?��$
�?   �V��>^#���_m?   �;�=����̓8H�H1�Ï�~�i���$
��    �ʾ�zn�e�   |aU�����?Jf0)_\2?�QmMM�?S�\�_"?   �V��>^#���_m?   �;�=˼�p�D�>�G���>L��)��>���jt�>L���й>�����6?�����<��B�[�=6a���˦>6��_�=�u�_�>/�9�� �=�m�%Dȃ>s��y� �=h�-�<ȃ>d����?/›��?o����@򵀍���Za�>@   FITT_HBP_PI      MARK˼�p�D�>��-<�սd��p�U��]��j��x,, %y��CuW��{����{���G���>��o�M�=r����=�J<Y
e�=��<�jA>Q�3Tv�;L��)��>��j��~�=�r-h�L�<�&ED���`wY&#;���jt�>�4�V��A��?�a�n����M�L���й>g�����=�w��m��;�����6?�_��E�<�����<����?Jf0)_\2?�QmMM�?��$
�?   �V��>^#���_m?   �;�=����̓8H�H1�Ï�~�i���$
��    �ʾ�zn�e�   |aU�����?Jf0)_\2?�QmMM�?S�\�_"?   �V��>^#���_m?   �;�=˼�p�D�>�G���>L��)��>���jt�>L���й>�����6?�����<��B�[�=6a���˦>6��_�=�u�_�>/�9�� �=�m�%Dȃ>s��y� �=h�-�<ȃ>d����?/›��?o����@򵀍���Za�>@   HBP_CNT0      DRIF˼�p�D�>��-<�սd��p�U��]��j��x,, %y��CuW��{����{���G���>��o�M�=r����=�J<Y
e�=��<�jA>Q�3Tv�;L��)��>��j��~�=�r-h�L�<�&ED���`wY&#;���jt�>�4�V��A��?�a�n����M�L���й>g�����=�w��m��;�����6?�_��E�<�����<����?Jf0)_\2?�QmMM�?��$
�?   �V��>^#���_m?   �;�=����̓8H�H1�Ï�~�i���$
��    �ʾ�zn�e�   |aU�����?Jf0)_\2?�QmMM�?S�\�_"?   �V��>^#���_m?   �;�=˼�p�D�>�G���>L��)��>���jt�>L���й>�����6?�����<��B�[�=6a���˦>6��_�=�u�_�>/�9�� �=�m�%Dȃ>s��y� �=h�-�<ȃ>d����?/›��?o����@򵀍���-�Ć@   HBP_DP_SEPM1   	   CSRCSBEND
_� q�>O{���=p�	!Ss=K	�{fM=H���+1={w�g<>���Ѯ�m;:�e��>ߘ���Q�=n�V���=N��i�=�����D0>�4Ѕ]$�;v�|���>�'��H�=�ʆ�]rּ����HE�����O���>�9�gt�>ލ���B͐a� ��7�4��a�,�>d��z�q=E��q �;^��%�8?6�:>*�;m�pdU�<Y;�-9H?R&�ej&?#�K(	s?��#��?   ���>Ո�h�l?   .� =)�����R&�ej&��o�89)���#���   �h�Ǿ��P�gf�   �|y�Y;�-9H?G!Ek3%?#�K(	s?�\�Y"?   ���>Ո�h�l?   .� =
_� q�>:�e��>v�|���>>�9�gt�>�a�,�>^��%�8?m�pdU�<RˏEV+�=%�o��{�>S��Ÿ=��0Z,C�>��K� �=���ǃ>k�*� �=-����ǃ>M�>�ߦ�?v�a즛ؿ7�F��$@m���=� ��@   HBP_DP_SEPM1   	   CSRCSBEND��'��>�*�Jyټ=��(i6��=tͣ��t=��Ll�i=�+so6T>_��yW�;�j�G�>��7�]>�=�HA���=¶�}P3�=	��W�E ��<Ei��;�*{�%��>�-#��=�e���9���{c͙��P>��yv�2�Wht�>���b� ���Z�_���J��]������>��d�{%��/i����;��؟O�;?gWp+�����/�<��b	c?9���=`?R�6��J?�}��?   y�3�>��7�fm?   :�="�����9���=`�^�㠯���}���   ��-ʾQO����f�   $Jt���b	c?�7�	�?R�6��J?41�,""?   y�3�>��7�fm?   :�=��'��>�j�G�>�*{�%��>2�Wht�>�����>��؟O�;?����/�<�c0���=�
���>K�{Ӈ��=�e-�ѐ>D�(� �=����ƃ>�񆈑 �=&����ƃ>���3��?`uS,q��u�8�)@�G��=� ��@   HBP_CNT      CENTER��'��>�*�Jyټ=��(i6��=tͣ��t=��Ll�i=�+so6T>_��yW�;�j�G�>��7�]>�=�HA���=¶�}P3�=	��W�E ��<Ei��;�*{�%��>�-#��=�e���9���{c͙��P>��yv�2�Wht�>���b� ���Z�_���J��]������>��d�{%��/i����;��؟O�;?gWp+�����/�<�F���?�ZB��W?9�1�J?�����?   y�3�>��7�fm?   :�=NӨ*���ZB��W�w��/���������   ��-ʾQO����f�   $Jt��F���?}�<K~'?9�1�J?����/"?   y�3�>��7�fm?   :�=��'��>�j�G�>�*{�%��>2�Wht�>�����>��؟O�;?����/�<�c0���=�
���>K�{Ӈ��=�e-�ѐ>D�(� �=����ƃ>�񆈑 �=&����ƃ>���3��?`uS,q��u�8�)@�G��=� ��@
   HBP_WA_OU1      WATCH��'��>�*�Jyټ=��(i6��=tͣ��t=��Ll�i=�+so6T>_��yW�;�j�G�>��7�]>�=�HA���=¶�}P3�=	��W�E ��<Ei��;�*{�%��>�-#��=�e���9���{c͙��P>��yv�2�Wht�>���b� ���Z�_���J��]������>��d�{%��/i����;��؟O�;?gWp+�����/�<�F���?�ZB��W?9�1�J?�����?   y�3�>��7�fm?   :�=NӨ*���ZB��W�w��/���������   ��-ʾQO����f�   $Jt��F���?}�<K~'?9�1�J?����/"?   y�3�>��7�fm?   :�=��'��>�j�G�>�*{�%��>2�Wht�>�����>��؟O�;?����/�<�c0���=�
���>K�{Ӈ��=�e-�ѐ>D�(� �=����ƃ>�񆈑 �=&����ƃ>���3��?`uS,q��u�8�)@�G��