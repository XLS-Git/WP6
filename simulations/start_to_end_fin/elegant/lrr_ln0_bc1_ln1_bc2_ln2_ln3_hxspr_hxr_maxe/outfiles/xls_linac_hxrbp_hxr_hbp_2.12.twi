SDDS1
!# little-endian
&description text="Twiss parameters--input: xls_linac_hxrbp_hxr_hbp_2.track.ele  lattice: xls_linac_hxrbp_hxr.12.lte", contents="Twiss parameters", &end
&parameter name=Step, description="Simulation step", type=long, &end
&parameter name=SVNVersion, description="SVN version number", type=string, fixed_value=unknown, &end
&parameter name=nux, symbol="$gn$r$bx$n", units="1/(2$gp$r)", description="Horizontal tune", type=double, &end
&parameter name=dnux/dp, symbol="$gx$r$bx$n", units="1/(2$gp$r)", description="Horizontal chromaticity", type=double, &end
&parameter name=dnux/dp2, symbol="$gx$r$bx2$n", units="1/(2$gp$r)", description="Horizontal 2nd-order chromaticity", type=double, &end
&parameter name=dnux/dp3, symbol="$gx$r$bx3$n", units="1/(2$gp$r)", description="Horizontal 3rd-order chromaticity", type=double, &end
&parameter name=Ax, symbol="A$bx$n", units=m, description="Horizontal acceptance", type=double, &end
&parameter name=AxLocation, units=m, description="Location of horizontal acceptance limit", type=double, &end
&parameter name=nuy, symbol="$gn$r$by$n", units="1/(2$gp$r)", description="Vertical tune", type=double, &end
&parameter name=dnuy/dp, symbol="$gx$r$by$n", units="1/(2$gp$r)", description="Vertical chromaticity", type=double, &end
&parameter name=dnuy/dp2, symbol="$gx$r$by2$n", units="1/(2$gp$r)", description="Vertical 2nd-order chromaticity", type=double, &end
&parameter name=dnuy/dp3, symbol="$gx$r$by3$n", units="1/(2$gp$r)", description="Vertical 3rd-order chromaticity", type=double, &end
&parameter name=Ay, symbol="A$by$n", units=m, description="Vertical acceptance", type=double, &end
&parameter name=AyLocation, units=m, description="Location of vertical acceptance limit", type=double, &end
&parameter name=deltaHalfRange, symbol="$gDd$r/2", description="Half range of momentum offset for chromatic tune spread evaluation", type=double, &end
&parameter name=nuxChromUpper, symbol="$gx$r$bu$n", description="Upper limit of x tune due to chromaticity and deltaRange", type=double, &end
&parameter name=nuxChromLower, symbol="$gx$r$bu$n", description="Lower limit of x tune due to chromaticity and deltaRange", type=double, &end
&parameter name=nuyChromUpper, symbol="$gy$r$bu$n", description="Upper limit of y tune due to chromaticity and deltaRange", type=double, &end
&parameter name=nuyChromLower, symbol="$gy$r$bu$n", description="Lower limit of y tune due to chromaticity and deltaRange", type=double, &end
&parameter name=Stage, description="Stage of computation", type=string, &end
&parameter name=pCentral, units="m$be$nc", description="Central momentum", type=double, &end
&parameter name=dbetax/dp, units=m, description="Derivative of betax with momentum offset", type=double, &end
&parameter name=dbetay/dp, units=m, description="Derivative of betay with momentum offset", type=double, &end
&parameter name=dalphax/dp, description="Derivative of alphax with momentum offset", type=double, &end
&parameter name=dalphay/dp, description="Derivative of alphay with momentum offset", type=double, &end
&parameter name=etax2, symbol="$gc$r$bx2$n", units=m, description="Second-order dispersion (for matched or periodic case only)", type=double, &end
&parameter name=etay2, symbol="$gc$r$by2$n", units=m, description="Second-order dispersion (for matched or periodic case only)", type=double, &end
&parameter name=etax3, symbol="$gc$r$bx3$n", units=m, description="Third-order dispersion (for matched or periodic case only)", type=double, &end
&parameter name=etay3, symbol="$gc$r$by3$n", units=m, description="Third-order dispersion (for matched or periodic case only)", type=double, &end
&parameter name=etaxp2, symbol="$gc$r$bx2$n$a'$n", units=m, description="Second-order dispersion (for matched or periodic case only)", type=double, &end
&parameter name=etayp2, symbol="$gc$r$by2$n$a'$n", units=m, description="Second-order dispersion (for matched or periodic case only)", type=double, &end
&parameter name=etaxp3, symbol="$gc$r$bx3$n$a'$n", units=m, description="Third-order dispersion (for matched or periodic case only)", type=double, &end
&parameter name=etayp3, symbol="$gc$r$by3$n$a'$n", units=m, description="Third-order dispersion (for matched or periodic case only)", type=double, &end
&parameter name=betaxMin, units=m, description="Minimum betax", type=double, &end
&parameter name=betaxAve, units=m, description="Average betax", type=double, &end
&parameter name=betaxMax, units=m, description="Maximum betax", type=double, &end
&parameter name=betayMin, units=m, description="Minimum betay", type=double, &end
&parameter name=betayAve, units=m, description="Average betay", type=double, &end
&parameter name=betayMax, units=m, description="Maximum betay", type=double, &end
&parameter name=etaxMax, units=m, description="Maximum absolute value of etax", type=double, &end
&parameter name=etayMax, units=m, description="Maximum absolute value of etay", type=double, &end
&parameter name=waistsx, description="Number of changes in the sign of alphax", type=long, &end
&parameter name=waistsy, description="Number of changes in the sign of alphay", type=long, &end
&parameter name=dnux/dAx, units=1/m, description="Horizontal tune shift with horizontal amplitude", type=double, &end
&parameter name=dnux/dAy, units=1/m, description="Horizontal tune shift with vertical amplitude", type=double, &end
&parameter name=dnuy/dAx, units=1/m, description="Vertical tune shift with horizontal amplitude", type=double, &end
&parameter name=dnuy/dAy, units=1/m, description="Vertical tune shift with vertical amplitude", type=double, &end
&parameter name=dnux/dAx2, units="1/m$a2$n", description="Horizontal tune shift with horizontal amplitude", type=double, &end
&parameter name=dnux/dAy2, units="1/m$a2$n", description="Horizontal tune shift with vertical amplitude", type=double, &end
&parameter name=dnux/dAxAy, units="1/m$a2$n", description="Horizontal tune shift with horizontal and vertical amplitude", type=double, &end
&parameter name=dnuy/dAx2, units="1/m$a2$n", description="Vertical tune shift with horizontal amplitude", type=double, &end
&parameter name=dnuy/dAy2, units="1/m$a2$n", description="Vertical tune shift with vertical amplitude", type=double, &end
&parameter name=dnuy/dAxAy, units="1/m$a2$n", description="Vertical tune shift with horizontal and vertical amplitude", type=double, &end
&parameter name=nuxTswaLower, description="Minimum horizontal tune from tune-shift-with-amplitude calculations", type=double, &end
&parameter name=nuxTswaUpper, description="Maximum horizontal tune from tune-shift-with-amplitude calculations", type=double, &end
&parameter name=nuyTswaLower, description="Minimum vertical tune from tune-shift-with-amplitude calculations", type=double, &end
&parameter name=nuyTswaUpper, description="Maximum vertical tune from tune-shift-with-amplitude calculations", type=double, &end
&parameter name=couplingIntegral, description="Coupling integral for difference resonance", type=double, &end
&parameter name=couplingDelta, description="Distance from difference resonance", type=double, &end
&parameter name=emittanceRatio, description="Emittance ratio from coupling integral", type=double, &end
&parameter name=alphac2, symbol="$ga$r$bc2$n", description="2nd-order momentum compaction factor", type=double, &end
&parameter name=alphac, symbol="$ga$r$bc$n", description="Momentum compaction factor", type=double, &end
&parameter name=I1, units=m, description="Radiation integral 1", type=double, &end
&parameter name=I2, units=1/m, description="Radiation integral 2", type=double, &end
&parameter name=I3, units="1/m$a2$n", description="Radiation integral 3", type=double, &end
&parameter name=I4, units=1/m, description="Radiation integral 4", type=double, &end
&parameter name=I5, units=1/m, description="Radiation integral 5", type=double, &end
&parameter name=ex0, units="$gp$rm", description="Damped horizontal emittance", type=double, &end
&parameter name=enx0, units="m$be$nc $gp$rm", description="Damped normalized horizontal emittance", type=double, &end
&parameter name=taux, units=s, description="Horizontal damping time", type=double, &end
&parameter name=Jx, description="Horizontal damping partition number", type=double, &end
&parameter name=tauy, units=s, description="Vertical damping time", type=double, &end
&parameter name=Jy, description="Vertical damping partition number", type=double, &end
&parameter name=Sdelta0, description="RMS fractional energy spread", type=double, &end
&parameter name=taudelta, units=s, description="Longitudinal damping time", type=double, &end
&parameter name=Jdelta, description="Longitudinal damping partition number", type=double, &end
&parameter name=U0, units=MeV, description="Energy loss per turn", type=double, &end
&column name=s, units=m, description=Distance, type=double,  &end
&column name=betax, symbol="$gb$r$bx$n", units=m, description="Horizontal beta-function", type=double,  &end
&column name=alphax, symbol="$ga$r$bx$n", description="Horizontal alpha-function", type=double,  &end
&column name=psix, symbol="$gy$r$bx$n", units=rad, description="Horizontal phase advance", type=double,  &end
&column name=etax, symbol="$gc$r$bx$n", units=m, description="Horizontal dispersion", type=double,  &end
&column name=etaxp, symbol="$gc$r$bx$n$a'$n", description="Slope of horizontal dispersion", type=double,  &end
&column name=xAperture, symbol="a$bx,eff$n", units=m, description="Effective horizontal aperture", type=double,  &end
&column name=betay, symbol="$gb$r$by$n", units=m, description="Vertical beta-function", type=double,  &end
&column name=alphay, symbol="$ga$r$by$n", description="Vertical alpha-function", type=double,  &end
&column name=psiy, symbol="$gy$r$by$n", units=rad, description="Vertical phase advance", type=double,  &end
&column name=etay, symbol="$gc$r$by$n", units=m, description="Vertical dispersion", type=double,  &end
&column name=etayp, symbol="$gc$r$by$n$a'$n", description="Slope of vertical dispersion", type=double,  &end
&column name=yAperture, symbol="a$by,eff$n", units=m, description="Effective vertical aperture", type=double,  &end
&column name=pCentral0, symbol="p$bcent$n", units="m$be$nc", description="Initial central momentum", type=double,  &end
&column name=ElementName, description="Element name", format_string=%10s, type=string,  &end
&column name=ElementOccurence, description="Occurence of element", format_string=%6ld, type=long,  &end
&column name=ElementType, description="Element-type name", format_string=%10s, type=string,  &end
&column name=ChamberShape, type=string,  &end
&column name=dI1, units=m, description="Contribution to radiation integral 1", type=double,  &end
&column name=dI2, units=1/m, description="Contribution to radiation integral 2", type=double,  &end
&column name=dI3, units="1/m$a2$n", description="Contribution to radiation integral 3", type=double,  &end
&column name=dI4, units=1/m, description="Contribution to radiation integral 4", type=double,  &end
&column name=dI5, units=1/m, description="Contribution to radiation integral 5", type=double,  &end
&data mode=binary, &end
N       k��:q�?����Vj�                        �������� ���c��?u�^ܮ?�                        ��������        k��:q�?k��:q�? ���c��? ���c��?   tunes uncorrected?؀.��@����G�N��Lh��.98�c2��8C�,�                                                                Z���3�?&��ʧ1@m�j\/�B@�k�HZ�?�! �&3@��<dBFI@��ͧ��?                                                                                                                                      ������ٿ        �̡���_?�45�_��2?�؆#�y_ö�*�?k_��:-[?��RQ����H?�m��>��:Y2>�.�?֠qm$�k?ywk�y �?��=��k?      �?Jg?�wY?$2�`�[?������?�ˡ���?        ���3��?�^uS,q��        �/�'#k�?U�ǜ����      $@��8�)@u�G��                              $@?؀.��@   _BEG_      MARK                                                    ���3��?�^uS,q��        �/�'#k�?U�ǜ����      $@��8�)@u�G��                              $@?؀.��@   BNCH      CHARGE   ?                                        333333�?�R�j��@������%c�E��?�.�W��s?U�ǜ����      $@L�!Ma/@�gh��r�� @3��?                      $@?؀.��@   HBP_DR_BP_C      CSRDRIFT   ?                                        �$����?.���W@a ��!�y>B��?���B?"s?U�ǜ����      $@K����/@s�U����#\�?                      $@?؀.��@	   HBP_DR_V3      DRIF   ?                                        Q/��3��?b�M��s@��{	����Y��?;~r�MDq?U�ǜ����      $@�y�^90@$m��=��eK����?                      $@?؀.��@   HBP_DR_QD_ED      DRIF   ?                                        ��Ώ>�?��ܰO�@+�vo�rU��d�?,�i�R/o?��      $@\;孠u0@"]�\����n�V��?                      $@?؀.��@
   HBP_QD_V02      QUAD   ?                                        ��Ώ>�?��ܰO�@+�vo�rU��d�?,�i�R/o?��      $@\;孠u0@"]�\����n�V��?                      $@?؀.��@   HBP_COR      KICKER   ?                                        ��Ώ>�?��ܰO�@+�vo�rU��d�?,�i�R/o?��      $@\;孠u0@"]�\����n�V��?                      $@?؀.��@   HBP_BPM      MONI   ?                                        o������?��6�
�@Jn_+���1ϋ�=��?��#@%l?M+���      $@պ��^0@r�ϵ�0	@A���?                      $@?؀.��@
   HBP_QD_V02      QUAD   ?                                        '��=��?��de@EG�'�lM���n�?K��i?M+���      $@U��a~0@u�4���@2��$`�?                      $@?؀.��@   HBP_DR_QD_ED      DRIF   ?                                        '��=��?��de@EG�'�lM���n�?K��i?M+���      $@U��a~0@u�4���@2��$`�?                      $@?؀.��@   FITT_HBP_AA      MARK   ?                                        ���?��?�\���IA@����K�0�n�.�P�?~=ώ����M+���      $@�>u���!@EA����@#Q�w��?                      $@?؀.��@	   HBP_DR_V4      DRIF   ?                                        �"�>Tc�?��@�MB@Wr�1��"�Md�?��2s�w��M+���      $@�*��&!@H�A�|`@}�񒶟�?                      $@?؀.��@   HBP_DR_QD_ED      DRIF   ?                                        o��H+�?��q��|B@#�� Q��,r#;v�?�@o�����9,�+/y�      $@>��7�� @���`�ʿ�A�-�9�?                      $@?؀.��@
   HBP_QD_V03      QUAD   ?                                        o��H+�?��q��|B@#�� Q��,r#;v�?�@o�����9,�+/y�      $@>��7�� @���`�ʿ�A�-�9�?                      $@?؀.��@   HBP_COR      KICKER   ?                                        o��H+�?��q��|B@#�� Q��,r#;v�?�@o�����9,�+/y�      $@>��7�� @���`�ʿ�A�-�9�?                      $@?؀.��@   HBP_BPM      MONI   ?                                        (S��?y�\/�B@���{1
@m����?*x�7�x������k�      $@�ϓ��7!@�b����Sy�-��?                      $@?؀.��@
   HBP_QD_V03      QUAD   ?                                        �g�, @:Vӕ�lB@?\��
@��OT���?/`���Ą�����k�      $@�$�Zʩ!@Y 	�Ǫ�^�r�?                      $@?؀.��@   HBP_DR_QD_ED      DRIF   ?                                        ��ׁs�@�+���],@�T"�)��?m�����?��Q��������k�      $@>)�C�H@�]i�r�4	P�U�?                      $@?؀.��@	   HBP_DR_V5      DRIF   ?                                        �-����@�=��,@�F}\5��?JrD� �?��晿����k�      $@%��.'$I@#�C�>���1��c�?                      $@?؀.��@   HBP_DR_QD_ED      DRIF   ?                                        	�c�@���[*�+@   ���.��8�?��EѲ��:ʗL&�{�      $@��<dBFI@   ����=vj.��o�?                      $@?؀.��@
   HBP_QD_V04      QUAD   ?                                        	�c�@���[*�+@   ���.��8�?��EѲ��:ʗL&�{�      $@��<dBFI@   ����=vj.��o�?                      $@?؀.��@   HBP_COR      KICKER   ?                                        	�c�@���[*�+@   ���.��8�?��EѲ��:ʗL&�{�      $@��<dBFI@   ����=vj.��o�?                      $@?؀.��@   HBP_BPM      MONI   ?                                        2�%�C@�"��,@�;]5���Hs�-�O�?8�&t���G�Z���      $@n��.'$I@��O�>�@����|�?                      $@?؀.��@
   HBP_QD_V04      QUAD   ?                                        8gDio@����],@�+�)������>h�?TP	嚿�G�Z���      $@A�C�H@b�u�r@�����?                      $@?؀.��@   HBP_DR_QD_ED      DRIF   ?                                        �`^��&@�,(��lB@�Y`�
����ޢ�?�M9�1���G�Z���      $@.T�Zʩ!@nhT 	@?��M�?                      $@?؀.��@	   HBP_DR_V5      DRIF   ?                                        ����%&@m�j\/�B@x� |1
���O�H��?i�9��M���G�Z���      $@�}��7!@����@���y;;�?                      $@?؀.��@   HBP_DR_QD_ED      DRIF   ?                                        �7�:&@����|B@�� Q@Ё���?Q��gN��8�p�D��?      $@q��7�� @T�R�`��?�@�9�a�?                      $@?؀.��@
   HBP_QD_V03      QUAD   ?                                        �7�:&@����|B@�� Q@Ё���?Q��gN��8�p�D��?      $@q��7�� @T�R�`��?�@�9�a�?                      $@?؀.��@   HBP_COR      KICKER   ?                                        �7�:&@����|B@�� Q@Ё���?Q��gN��8�p�D��?      $@q��7�� @T�R�`��?�@�9�a�?                      $@?؀.��@   HBP_BPM      MONI   ?                                        ��IۃN&@qڔ�MB@h7
1@�jE1��?\������ ��mA�?      $@���&!@���|`�ew ��?                      $@?؀.��@
   HBP_QD_V03      QUAD   ?                                        ��jFd&@!���IA@(��K�0@�����?V7q�ȱ� ��mA�?      $@��\���!@�k�������Lg<��?                      $@?؀.��@   HBP_DR_QD_ED      DRIF   ?                                        o�t�&)@�<�de@�H�G�'@w�����?Θ�\�c�� ��mA�?      $@ E�a~0@��麟���J���i�?                      $@?؀.��@	   HBP_DR_V4      DRIF   ?                                        o�t�&)@�<�de@�H�G�'@w�����?Θ�\�c�� ��mA�?      $@ E�a~0@��麟���J���i�?                      $@?؀.��@   FITT_HBP_BB      MARK   ?                                        e�b<)@^�
�@���+��@b����?��5}d�� ��mA�?      $@¦�^0@�P���0	��<�o�?                      $@?؀.��@   HBP_DR_QD_ED	      DRIF   ?                                        y����P)@����O�@8��vo@^��W�?��C����}a�u\m�?      $@��í�u0@�m�\�?��]��?                      $@?؀.��@
   HBP_QD_V02      QUAD   ?                                        y����P)@����O�@8��vo@^��W�?��C����}a�u\m�?      $@��í�u0@�m�\�?��]��?                      $@?؀.��@   HBP_COR      KICKER   ?                                        y����P)@����O�@8��vo@^��W�?��C����}a�u\m�?      $@��í�u0@�m�\�?��]��?                      $@?؀.��@   HBP_BPM      MONI   ?                                        �@��te)@�����s@[���{	@c��c��?Lw'{�9��n/]4Ր?      $@͉X�^90@p�f�=�@��-�a��?                      $@?؀.��@
   HBP_QD_V02      QUAD   ?                                        �iNU7{)@�r)��W@���!@Q<#O�H�?$��U����n/]4Ր?      $@%;@���/@�X�U�@^�	���?                      $@?؀.��@   HBP_DR_QD_ED
      DRIF   ?                                        n���T�)@E��k��@r� ���@i(5��o�?��9�3O��n/]4Ր?      $@�ޫ!Ma/@��I��r@��9+V��?                      $@?؀.��@	   HBP_DR_V3      DRIF   ?                                        �c�*@(IҀ3��?c�VT,q�?'�8�?wx�pB��n/]4Ր?      $@?M�7�)@N�G�@:<��o�?                      $@?؀.��@	   HBP_DR_BP      DRIF   ?                                        �c�*@(IҀ3��?c�VT,q�?'�8�?wx�pB��n/]4Ր?      $@?M�7�)@N�G�@:<��o�?                      $@?؀.��@   HBP_CNT      CENTER   ?                                        �c�*@(IҀ3��?c�VT,q�?'�8�?wx�pB��n/]4Ր?      $@?M�7�)@N�G�@:<��o�?                      $@?؀.��@
   HBP_WA_OU2      WATCH   ?                                        �c�*@(IҀ3��?c�VT,q�?'�8�?wx�pB��n/]4Ր?      $@?M�7�)@N�G�@:<��o�?                      $@?؀.��@
   FITT_HBP_5      MARK   ?                                        5���*@Z���3�?��(4�?ʊ�qD��?&�w�D��.Q�4�Q�?      $@�~t#��$@�Pcl�@�>^�S�?                      $@?؀.��@   HBP_DP_SEPM2   	   CSRCSBEND   ? 54��{A�y_ö�*o?k_��:-;?����ھ�B(7���>b���b+@/|:j��?��
�����غ�@�D(��?�!�i�?      $@�<ܔ��@́���@���H�v�?                      $@?؀.��@   HBP_DP_SEPM2   	   CSRCSBEND   ?���aQ�?y_ö�*o?k_��:-;?	��}�Y�>'��ٸ+�>�1�?z�+@��<ba|�?}��>a��
o��b
@F{����?�!�i�?      $@<t ��,@����O@� �}���?                      $@?؀.��@   HBP_DR_BP_C      CSRDRIFT   ?                                        ��%��,@�Ej٘@�-5�x=	�2L��@d�{�\��?�!�i�?      $@�5��H�@z�^�@h����?                      $@?؀.��@	   HBP_DR_BP      DRIF   ?                                        0e�r�/-@�~c�@��{,2e��9�Y@%�5�0?�?�!�i�?      $@���E�@�::r&��?�8�V%�?                      $@?؀.��@	   HBP_DR_BP      DRIF   ?                                        ��XG�-@�D�;��@�L���+��fj4�@�(���?�!�i�?      $@(���� @S�:'|�?*��
�?                      $@?؀.��@	   HBP_DR_BP      DRIF   ?                                        :B���O.@��]R&@��ߒ�Z���$	�@�vX\���?�!�i�?      $@�]� `>�?\�k\g�?`�\Ić�?                      $@?؀.��@	   HBP_DR_V1      DRIF   ?                                        :B���O.@��]R&@��ߒ�Z���$	�@�vX\���?�!�i�?      $@�]� `>�?\�k\g�?`�\Ić�?                      $@?؀.��@   HBP_CNT0      DRIF   ?                                        0kPZ�e.@�9D
�t'@��-!.�S�����@�%���O�?�!�i�?      $@IUٚI��?PC?��U�?��X?	�?                      $@?؀.��@   HBP_DR_QD_ED      DRIF   ?                                        D�;!z.@�z��fF(@Q�뚀�W�@�^���?vi�-�?      $@$�����?� ���j�?�e�E���?                      $@?؀.��@
   HBP_QD_V01      QUAD   ?                                        D�;!z.@�z��fF(@Q�뚀�W�@�^���?vi�-�?      $@$�����?� ���j�?�e�E���?                      $@?؀.��@   HBP_COR      KICKER   ?                                        D�;!z.@�z��fF(@Q�뚀�W�@�^���?vi�-�?      $@$�����?� ���j�?�e�E���?                      $@?؀.��@   HBP_BPM      MONI   ?                                        X����.@6�d�F�(@�N�?̃�
2	@Rp+���?�{�*ьj?      $@��@̓>�?>�g�7�?;}�k�?                      $@?؀.��@
   HBP_QD_V01      QUAD   ?                                        N�;�^�.@_�ب�(@i�EK�$�?z8�I@_����?�{�*ьj?      $@ص�F��?��$�y�?����9��?                      $@?؀.��@   HBP_DR_QD_ED      DRIF   ?                                        X���/@W����(@CҚ7�?4���2@�����?�{�*ьj?      $@�k�HZ�?�>Ҥod?�s^(��?                      $@?؀.��@	   HBP_DR_V2      DRIF   ?                                        b����x/@�Dƣ˂(@@�����Y?��x��U@�UKX$��?�{�*ьj?      $@�u����?f���ƿ�/�N]�?                      $@?؀.��@	   HBP_DR_V2      DRIF   ?                                        b����x/@�Dƣ˂(@@�����Y?��x��U@�UKX$��?�{�*ьj?      $@�u����?f���ƿ�/�N]�?                      $@?؀.��@   HBP_CNT0      DRIF   ?                                        X����/@�-Ik͂(@��g�z_��	ʵ\@��ͧ��?�{�*ьj?      $@����[4�?!���%�˿hV��c��?                      $@?؀.��@   HBP_DR_QD_ED      DRIF   ?                                        lu'��/@�����;(@1x���@��kc@ѶH�*��?R�x�����      $@!h� ���?7xg�<;�Fr�\v�?                      $@?؀.��@
   HBP_QD_V01      QUAD   ?                                        lu'��/@�����;(@1x���@��kc@ѶH�*��?R�x�����      $@!h� ���?7xg�<;�Fr�\v�?                      $@?؀.��@   HBP_COR      KICKER   ?                                        lu'��/@�����;(@1x���@��kc@ѶH�*��?R�x�����      $@!h� ���?7xg�<;�Fr�\v�?                      $@?؀.��@   HBP_BPM      MONI   ?                                        �#oߑ�/@	r��h'@7�0�+ @�~1Ij@؞�}��?���3+���      $@��V�9��?����9�t60��?                      $@?؀.��@
   HBP_QD_V01      QUAD   ?                                        vL�nT�/@t���cE&@[��Vvq@0L��q@DF�#�?���3+���      $@�q�-�?"li�I�39�dz�?                      $@?؀.��@   HBP_DR_QD_ED      DRIF   ?                                        �G���)0@%�f�Y�@�{Q�8@��>>1�@"3aU�վ?���3+���      $@Gm� @ȼW�������d��| @                      $@?؀.��@	   HBP_DR_V1      DRIF   ?                                        ��c�v0@D댰�@�D�;sg@լ�@G������?���3+���      $@s�I�@���._��u
<7dp@                      $@?؀.��@	   HBP_DR_BP      DRIF   ?                                        ��L0��0@�qUY��@� Mp,	@J��V��@l��9i�?���3+���      $@w��N�@_8�NNp���0�@                      $@?؀.��@	   HBP_DR_BP      DRIF   ?                                        Z��^1@�6 ���?�[�D��?S~��@"���e�?���3+���      $@��n�N@]e�T1�������@                      $@?؀.��@	   HBP_DR_BP      DRIF   ?                                        '{��+]1@x��Lf��?4�����?��#��@�.+�?���3+���      $@���a��@[��Z��
����Y0�@                      $@?؀.��@	   HBP_DR_BP      DRIF   ?                                        �>�Z(�1@C<A-��?�`�@�����)i@�b-�i?����0���      $@V���hl$@7��T�����A0@                      $@?؀.��@   HBP_DP_SEPM2   	   CSRCSBEND   ?�"�J7<?y_ö�*o?k_��:-;?ͮ��`�>�T�B��>UP�$2@��v[�?���^D��g~ٱ@8Օ�_�x�f���      $@ �3�I�)@����vz�xN�Z�i@                      $@?؀.��@   HBP_DP_SEPM2   	   CSRCSBEND   ?>�M�h�y_ö�*o?k_��:-;?rd�,	��������>UP�$2@��v[�?���^D��g~ٱ@8Օ�_�x�f���      $@ �3�I�)@����vz�xN�Z�i@                      $@?؀.��@
   FITT_HBP_8      MARK   ?                                        UP�$2@��v[�?���^D��g~ٱ@8Օ�_�x�f���      $@ �3�I�)@����vz�xN�Z�i@                      $@?؀.��@   HBP_CNT      CENTER   ?                                        UP�$2@��v[�?���^D��g~ٱ@8Օ�_�x�f���      $@ �3�I�)@����vz�xN�Z�i@                      $@?؀.��@
   HBP_WA_OU3      WATCH   ?                                        