SDDS1
!# little-endian
&description text="Twiss parameters--input: xls_linac_tmc_hxr_hxr.track.ele  lattice: xls_linac_tmc_hxr.12.lte", contents="Twiss parameters", &end
&parameter name=Step, description="Simulation step", type=long, &end
&parameter name=SVNVersion, description="SVN version number", type=string, fixed_value=unknown, &end
&parameter name=nux, symbol="$gn$r$bx$n", units="1/(2$gp$r)", description="Horizontal tune", type=double, &end
&parameter name=dnux/dp, symbol="$gx$r$bx$n", units="1/(2$gp$r)", description="Horizontal chromaticity", type=double, &end
&parameter name=dnux/dp2, symbol="$gx$r$bx2$n", units="1/(2$gp$r)", description="Horizontal 2nd-order chromaticity", type=double, &end
&parameter name=dnux/dp3, symbol="$gx$r$bx3$n", units="1/(2$gp$r)", description="Horizontal 3rd-order chromaticity", type=double, &end
&parameter name=Ax, symbol="A$bx$n", units=m, description="Horizontal acceptance", type=double, &end
&parameter name=AxLocation, units=m, description="Location of horizontal acceptance limit", type=double, &end
&parameter name=nuy, symbol="$gn$r$by$n", units="1/(2$gp$r)", description="Vertical tune", type=double, &end
&parameter name=dnuy/dp, symbol="$gx$r$by$n", units="1/(2$gp$r)", description="Vertical chromaticity", type=double, &end
&parameter name=dnuy/dp2, symbol="$gx$r$by2$n", units="1/(2$gp$r)", description="Vertical 2nd-order chromaticity", type=double, &end
&parameter name=dnuy/dp3, symbol="$gx$r$by3$n", units="1/(2$gp$r)", description="Vertical 3rd-order chromaticity", type=double, &end
&parameter name=Ay, symbol="A$by$n", units=m, description="Vertical acceptance", type=double, &end
&parameter name=AyLocation, units=m, description="Location of vertical acceptance limit", type=double, &end
&parameter name=deltaHalfRange, symbol="$gDd$r/2", description="Half range of momentum offset for chromatic tune spread evaluation", type=double, &end
&parameter name=nuxChromUpper, symbol="$gx$r$bu$n", description="Upper limit of x tune due to chromaticity and deltaRange", type=double, &end
&parameter name=nuxChromLower, symbol="$gx$r$bu$n", description="Lower limit of x tune due to chromaticity and deltaRange", type=double, &end
&parameter name=nuyChromUpper, symbol="$gy$r$bu$n", description="Upper limit of y tune due to chromaticity and deltaRange", type=double, &end
&parameter name=nuyChromLower, symbol="$gy$r$bu$n", description="Lower limit of y tune due to chromaticity and deltaRange", type=double, &end
&parameter name=Stage, description="Stage of computation", type=string, &end
&parameter name=pCentral, units="m$be$nc", description="Central momentum", type=double, &end
&parameter name=dbetax/dp, units=m, description="Derivative of betax with momentum offset", type=double, &end
&parameter name=dbetay/dp, units=m, description="Derivative of betay with momentum offset", type=double, &end
&parameter name=dalphax/dp, description="Derivative of alphax with momentum offset", type=double, &end
&parameter name=dalphay/dp, description="Derivative of alphay with momentum offset", type=double, &end
&parameter name=etax2, symbol="$gc$r$bx2$n", units=m, description="Second-order dispersion (for matched or periodic case only)", type=double, &end
&parameter name=etay2, symbol="$gc$r$by2$n", units=m, description="Second-order dispersion (for matched or periodic case only)", type=double, &end
&parameter name=etax3, symbol="$gc$r$bx3$n", units=m, description="Third-order dispersion (for matched or periodic case only)", type=double, &end
&parameter name=etay3, symbol="$gc$r$by3$n", units=m, description="Third-order dispersion (for matched or periodic case only)", type=double, &end
&parameter name=etaxp2, symbol="$gc$r$bx2$n$a'$n", units=m, description="Second-order dispersion (for matched or periodic case only)", type=double, &end
&parameter name=etayp2, symbol="$gc$r$by2$n$a'$n", units=m, description="Second-order dispersion (for matched or periodic case only)", type=double, &end
&parameter name=etaxp3, symbol="$gc$r$bx3$n$a'$n", units=m, description="Third-order dispersion (for matched or periodic case only)", type=double, &end
&parameter name=etayp3, symbol="$gc$r$by3$n$a'$n", units=m, description="Third-order dispersion (for matched or periodic case only)", type=double, &end
&parameter name=betaxMin, units=m, description="Minimum betax", type=double, &end
&parameter name=betaxAve, units=m, description="Average betax", type=double, &end
&parameter name=betaxMax, units=m, description="Maximum betax", type=double, &end
&parameter name=betayMin, units=m, description="Minimum betay", type=double, &end
&parameter name=betayAve, units=m, description="Average betay", type=double, &end
&parameter name=betayMax, units=m, description="Maximum betay", type=double, &end
&parameter name=etaxMax, units=m, description="Maximum absolute value of etax", type=double, &end
&parameter name=etayMax, units=m, description="Maximum absolute value of etay", type=double, &end
&parameter name=waistsx, description="Number of changes in the sign of alphax", type=long, &end
&parameter name=waistsy, description="Number of changes in the sign of alphay", type=long, &end
&parameter name=dnux/dAx, units=1/m, description="Horizontal tune shift with horizontal amplitude", type=double, &end
&parameter name=dnux/dAy, units=1/m, description="Horizontal tune shift with vertical amplitude", type=double, &end
&parameter name=dnuy/dAx, units=1/m, description="Vertical tune shift with horizontal amplitude", type=double, &end
&parameter name=dnuy/dAy, units=1/m, description="Vertical tune shift with vertical amplitude", type=double, &end
&parameter name=dnux/dAx2, units="1/m$a2$n", description="Horizontal tune shift with horizontal amplitude", type=double, &end
&parameter name=dnux/dAy2, units="1/m$a2$n", description="Horizontal tune shift with vertical amplitude", type=double, &end
&parameter name=dnux/dAxAy, units="1/m$a2$n", description="Horizontal tune shift with horizontal and vertical amplitude", type=double, &end
&parameter name=dnuy/dAx2, units="1/m$a2$n", description="Vertical tune shift with horizontal amplitude", type=double, &end
&parameter name=dnuy/dAy2, units="1/m$a2$n", description="Vertical tune shift with vertical amplitude", type=double, &end
&parameter name=dnuy/dAxAy, units="1/m$a2$n", description="Vertical tune shift with horizontal and vertical amplitude", type=double, &end
&parameter name=nuxTswaLower, description="Minimum horizontal tune from tune-shift-with-amplitude calculations", type=double, &end
&parameter name=nuxTswaUpper, description="Maximum horizontal tune from tune-shift-with-amplitude calculations", type=double, &end
&parameter name=nuyTswaLower, description="Minimum vertical tune from tune-shift-with-amplitude calculations", type=double, &end
&parameter name=nuyTswaUpper, description="Maximum vertical tune from tune-shift-with-amplitude calculations", type=double, &end
&parameter name=couplingIntegral, description="Coupling integral for difference resonance", type=double, &end
&parameter name=couplingDelta, description="Distance from difference resonance", type=double, &end
&parameter name=emittanceRatio, description="Emittance ratio from coupling integral", type=double, &end
&parameter name=alphac2, symbol="$ga$r$bc2$n", description="2nd-order momentum compaction factor", type=double, &end
&parameter name=alphac, symbol="$ga$r$bc$n", description="Momentum compaction factor", type=double, &end
&parameter name=I1, units=m, description="Radiation integral 1", type=double, &end
&parameter name=I2, units=1/m, description="Radiation integral 2", type=double, &end
&parameter name=I3, units="1/m$a2$n", description="Radiation integral 3", type=double, &end
&parameter name=I4, units=1/m, description="Radiation integral 4", type=double, &end
&parameter name=I5, units=1/m, description="Radiation integral 5", type=double, &end
&parameter name=ex0, units="$gp$rm", description="Damped horizontal emittance", type=double, &end
&parameter name=enx0, units="m$be$nc $gp$rm", description="Damped normalized horizontal emittance", type=double, &end
&parameter name=taux, units=s, description="Horizontal damping time", type=double, &end
&parameter name=Jx, description="Horizontal damping partition number", type=double, &end
&parameter name=tauy, units=s, description="Vertical damping time", type=double, &end
&parameter name=Jy, description="Vertical damping partition number", type=double, &end
&parameter name=Sdelta0, description="RMS fractional energy spread", type=double, &end
&parameter name=taudelta, units=s, description="Longitudinal damping time", type=double, &end
&parameter name=Jdelta, description="Longitudinal damping partition number", type=double, &end
&parameter name=U0, units=MeV, description="Energy loss per turn", type=double, &end
&column name=s, units=m, description=Distance, type=double,  &end
&column name=betax, symbol="$gb$r$bx$n", units=m, description="Horizontal beta-function", type=double,  &end
&column name=alphax, symbol="$ga$r$bx$n", description="Horizontal alpha-function", type=double,  &end
&column name=psix, symbol="$gy$r$bx$n", units=rad, description="Horizontal phase advance", type=double,  &end
&column name=etax, symbol="$gc$r$bx$n", units=m, description="Horizontal dispersion", type=double,  &end
&column name=etaxp, symbol="$gc$r$bx$n$a'$n", description="Slope of horizontal dispersion", type=double,  &end
&column name=xAperture, symbol="a$bx,eff$n", units=m, description="Effective horizontal aperture", type=double,  &end
&column name=betay, symbol="$gb$r$by$n", units=m, description="Vertical beta-function", type=double,  &end
&column name=alphay, symbol="$ga$r$by$n", description="Vertical alpha-function", type=double,  &end
&column name=psiy, symbol="$gy$r$by$n", units=rad, description="Vertical phase advance", type=double,  &end
&column name=etay, symbol="$gc$r$by$n", units=m, description="Vertical dispersion", type=double,  &end
&column name=etayp, symbol="$gc$r$by$n$a'$n", description="Slope of vertical dispersion", type=double,  &end
&column name=yAperture, symbol="a$by,eff$n", units=m, description="Effective vertical aperture", type=double,  &end
&column name=pCentral0, symbol="p$bcent$n", units="m$be$nc", description="Initial central momentum", type=double,  &end
&column name=ElementName, description="Element name", format_string=%10s, type=string,  &end
&column name=ElementOccurence, description="Occurence of element", format_string=%6ld, type=long,  &end
&column name=ElementType, description="Element-type name", format_string=%10s, type=string,  &end
&column name=ChamberShape, type=string,  &end
&column name=dI1, units=m, description="Contribution to radiation integral 1", type=double,  &end
&column name=dI2, units=1/m, description="Contribution to radiation integral 2", type=double,  &end
&column name=dI3, units="1/m$a2$n", description="Contribution to radiation integral 3", type=double,  &end
&column name=dI4, units=1/m, description="Contribution to radiation integral 4", type=double,  &end
&column name=dI5, units=1/m, description="Contribution to radiation integral 5", type=double,  &end
&data mode=binary, &end
/       N�sqP��?K�hf~�ٿ                        ���������`]%x��?T�˅s��                        ��������        N�sqP��?N�sqP��?�`]%x��?�`]%x��?   tunes uncorrected� 2�@Gߝl��"@ݏ* �Y@��D��H���-��3#�                                                                ��+q��?��s��d@\�0l @�^�e��@ݳ���4!@Z^�nh:@                                                                                                                                              F�,��i�?                �x@�V�S���v����D�?���'_�J?        '3Mu��=4#���8=�i�^e >��([�?      �?��([�?      �?#����ZD?��([�?       @i���1��?        OL����	@րt��ӿ                              $@/�R���%@�;�g%ڿ                              $@� 2�@   _BEG_      MARK                                                    OL����	@րt��ӿ                              $@/�R���%@�;�g%ڿ                              $@� 2�@   BNCH      CHARGE   ?                                        윙����?��b>7�
@6� G��׿������?                      $@Q5�K�Q&@Dr��D�ۿ T^~�?                      $@� 2�@	   HXR_DR_V1      DRIF   ?                                        �䥛� �?b����A@�蕏�	ٿ&S v�?                      $@�A�h&@H�Զ�ۿ��;A�?                      $@� 2�@   HXR_DR_QD_ED      DRIF   ?                                        �*\����?P���6@�O����?5n~1p�?                      $@�$�&@\017��,��ަp�?                      $@� 2�@
   HXR_QD_V01      QUAD   ?                                        �*\����?P���6@�O����?5n~1p�?                      $@�$�&@\017��,��ަp�?                      $@� 2�@   HXR_COR      KICKER   ?                                        �*\����?P���6@�O����?5n~1p�?                      $@�$�&@\017��,��ަp�?                      $@� 2�@   HXR_BPM      MONI   ?                                        Ap����?|�L�
@�P٫��?5/��k�?                      $@@���_�&@��o�� ������?                      $@� 2�@
   HXR_QD_V01      QUAD   ?                                        Ȇ�Q��?f}[��L	@ԙbT��?�_}��?                      $@>~M�s�(@�>���!����?                      $@� 2�@   HXR_DR_QD_ED      DRIF   ?                                        �D<j�?�����?����ݶ�?��*�fh�?                      $@E����8@�����)�@㦒 #�?                      $@� 2�@	   HXR_DR_V2      DRIF   ?                                        Lw~+8�?c�����?��3h��?>U�J���?                      $@����4:@������)��_E�q��?                      $@� 2�@   HXR_DR_QD_ED      DRIF   ?                                        �Y%�~�?���N���?-g�e�i�?K�#S{�?                      $@Z^�nh:@��7q����t?��Ȱ?                      $@� 2�@
   HXR_QD_V02      QUAD   ?                                        �Y%�~�?���N���?-g�e�i�?K�#S{�?                      $@Z^�nh:@��7q����t?��Ȱ?                      $@� 2�@   HXR_COR      KICKER   ?                                        �Y%�~�?���N���?-g�e�i�?K�#S{�?                      $@Z^�nh:@��7q����t?��Ȱ?                      $@� 2�@   HXR_BPM      MONI   ?                                        �46��?��+q��?�
)A8��M��#��?                      $@��RՙW:@����@�g��?                      $@� 2�@
   HXR_QD_V02      QUAD   ?                                        �P���?�`J���?o`�������n'ϰ�?                      $@Y���9@���o23@���dol�?                      $@� 2�@   HXR_DR_QD_ED      DRIF   ?                                        j�2Ռ@W���ޛ@P N�W�81i�y�?                      $@\�4�{&@�8��@�Z:A�\�?                      $@� 2�@	   HXR_DR_V3      DRIF   ?                                        ;��+T�@���I�u@�%�����NR0���?                      $@���[w%@����U@�9!>���?                      $@� 2�@   HXR_DR_QD_ED      DRIF   ?                                        �c��@X��֌�@�~,^���ۯg`��?                      $@C�K�@%@��o`�
@�؏V��?                      $@� 2�@
   HXR_QD_V03      QUAD   ?                                        �c��@X��֌�@�~,^���ۯg`��?                      $@C�K�@%@��o`�
@�؏V��?                      $@� 2�@   HXR_COR      KICKER   ?                                        �c��@X��֌�@�~,^���ۯg`��?                      $@C�K�@%@��o`�
@�؏V��?                      $@� 2�@   HXR_BPM      MONI   ?                                        ��ڨ�,@�6O[�@������Կ�'+l��?                      $@S)��%@��ݓ�?�J�
H�?                      $@� 2�@
   HXR_QD_V03      QUAD   ?                                        t����@po�X&@�-�$��տC�8��?�?                      $@n����$@���6�?�m�����?                      $@� 2�@   HXR_DR_QD_ED      DRIF   ?                                        V�ŧ�U@_o��@�o��qj����Ho��?                      $@�5���[
@|����??�7�x�?                      $@� 2�@	   HXR_DR_V4      DRIF   ?                                        �;,��@'t� @ڽ�����->�Hb��?                      $@�qx:Q	@�
���?�(\��v�?                      $@� 2�@	   HXR_DR_CT      DRIF   ?                                        �;,��@'t� @ڽ�����->�Hb��?                      $@�qx:Q	@�
���?�(\��v�?                      $@� 2�@   HXR_WA_M_OU      WATCH   ?                                        $��5�@���^b @#���;g���Bg'���?                      $@2�`w��@�5-|a]�?~>O���?                      $@� 2�@   HXR_DR_QD_ED      DRIF   ?                                        ~1Uj��@\�0l @���_�o�?G2O� ��?                      $@�����@�&*[�i�C���!�?                      $@� 2�@	   HXR_QD_FH      QUAD   ?                                        ~1Uj��@\�0l @���_�o�?G2O� ��?                      $@�����@�&*[�i�C���!�?                      $@� 2�@   HXR_COR      KICKER   ?                                        ~1Uj��@\�0l @���_�o�?G2O� ��?                      $@�����@�&*[�i�C���!�?                      $@� 2�@   HXR_BPM      MONI   ?                                        ؕ�I�@��t` @�u�X<��?�L>���?                      $@��Y;�@�ka!+��C�S��C�?                      $@� 2�@	   HXR_QD_FH      QUAD   ?                                        @'~�A@	�]@ @��V�O�?8ile���?                      $@Aa���R	@YѮ5�M俶� k}��?                      $@� 2�@   HXR_DR_QD_ED      DRIF   ?                                        ����@�ʸ>��@�	��?�;�I�^�?                      $@�_�:p@с�翾�Ӕ��?                      $@� 2�@   HXR_DR_WIG_ED      DRIF   ?                                        gX�r1@�Ayr�
@�!B�x��?�� L@                      $@��c�N9@�RF*	l��6���.�?                      $@� 2�@	   HXR_WIG_0      WIGGLER   ?S���v{���Dx?���'_�:?        ^ٸ.3�=�9���@��r�|�@���G��?6���@                      $@~�I���@���������+<I_��?                      $@� 2�@   HXR_DR_WIG_ED      DRIF   ?                                        ��@��p#0	@DI��l��?�~�!�@                      $@[r��X @���B����7f��?                      $@� 2�@   HXR_DR_QD_ED	      DRIF   ?                                        q/*�n%@.�߷�@ ��&�?�~��@                      $@��*��& @pvI<S�8?(-Ů��?                      $@� 2�@	   HXR_QD_DH      QUAD   ?                                        q/*�n%@.�߷�@ ��&�?�~��@                      $@��*��& @pvI<S�8?(-Ů��?                      $@� 2�@   HXR_COR      KICKER   ?                                        q/*�n%@.�߷�@ ��&�?�~��@                      $@��*��& @pvI<S�8?(-Ů��?                      $@� 2�@   HXR_BPM      MONI   ?                                        ˓e¾2@�wG�@�Nf���ῤ^���@                      $@��}V @/�%�^"�?��x$H��?                      $@� 2�@	   HXR_QD_DH      QUAD   ?                                        3%S?�g@�K�3m~@�\�dy���C��@                      $@��(��@�#�����?�#vN���?                      $@� 2�@   HXR_DR_QD_ED
      DRIF   ?                                        {�S�/@.�Ryn
@�ϼ�;忁;�x�j@                      $@p���8@?M���n�?�g�/1�?                      $@� 2�@   HXR_DR_WIG_ED      DRIF   ?                                        -��u!#@�X1�?@�O������'0�l@                      $@V�y�ml@Ly7��?¶����?                      $@� 2�@	   HXR_WIG_0      WIGGLER   ?S���v{���Dx?���'_�:?        �����4�=���#@�2j[@A	�nO��}�>���@                      $@AW	O	@Ƌ�{�N�?^�F8��?                      $@� 2�@   HXR_DR_WIG_ED      DRIF   ?                                        ��a>��#@&��@�B���)8b��@                      $@�GSoR�@��|đ�?��<�?,�?                      $@� 2�@   HXR_DR_QD_ED      DRIF   ?                                        ���-,�#@�껷�@DbG�����mȅ��@                      $@�^�e��@��nHcm?�fFYv=�?                      $@� 2�@	   HXR_QD_FH      QUAD   ?                                        ���-,�#@�껷�@DbG�����mȅ��@                      $@�^�e��@��nHcm?�fFYv=�?                      $@� 2�@	   HXR_WA_OU      WATCH   ?                                        