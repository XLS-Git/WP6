SDDS1
!# little-endian
&description text="sigma matrix--input: xls_linac_tmc_hxr_bc2.track.ele  lattice: xls_linac_tmc_hxr.5.lte", contents="sigma matrix", &end
&parameter name=Step, description="Simulation step", type=long, &end
&parameter name=SVNVersion, description="SVN version number", type=string, fixed_value=unknown, &end
&column name=s, units=m, description=Distance, type=double,  &end
&column name=ElementName, description="Element name", format_string=%10s, type=string,  &end
&column name=ElementOccurence, description="Occurence of element", format_string=%6ld, type=long,  &end
&column name=ElementType, description="Element-type name", format_string=%10s, type=string,  &end
&column name=s1, symbol="$gs$r$b1$n", units=m, description="sqrt(<x*x>)", type=double,  &end
&column name=s12, symbol="$gs$r$b12$n", units=m, description="<x*xp'>", type=double,  &end
&column name=s13, symbol="$gs$r$b13$n", units="m$a2$n", description="<x*y>", type=double,  &end
&column name=s14, symbol="$gs$r$b14$n", units=m, description="<x*y'>", type=double,  &end
&column name=s15, symbol="$gs$r$b15$n", units="m$a2$n", description="<x*s>", type=double,  &end
&column name=s16, symbol="$gs$r$b16$n", units=m, description="<x*delta>", type=double,  &end
&column name=s17, symbol="$gs$r$b17$n", units="m*s", description="<x*t>", type=double,  &end
&column name=s2, symbol="$gs$r$b2$n", description="sqrt(<x'*x'>)", type=double,  &end
&column name=s23, symbol="$gs$r$b23$n", units=m, description="<x'*y>", type=double,  &end
&column name=s24, symbol="$gs$r$b24$n", description="<x'*y'>", type=double,  &end
&column name=s25, symbol="$gs$r$b25$n", units=m, description="<x'*s>", type=double,  &end
&column name=s26, symbol="$gs$r$b26$n", description="<x'*delta>", type=double,  &end
&column name=s27, symbol="$gs$r$b27$n", units=s, description="<x'*t>", type=double,  &end
&column name=s3, symbol="$gs$r$b3$n", units=m, description="sqrt(<y*y>)", type=double,  &end
&column name=s34, symbol="$gs$r$b34$n", units=m, description="<y*y'>", type=double,  &end
&column name=s35, symbol="$gs$r$b35$n", units="m$a2$n", description="<y*s>", type=double,  &end
&column name=s36, symbol="$gs$r$b36$n", units=m, description="<y*delta>", type=double,  &end
&column name=s37, symbol="$gs$r$b37$n", units="m*s", description="<y*t>", type=double,  &end
&column name=s4, symbol="$gs$r$b4$n", description="sqrt(<y'*y')>", type=double,  &end
&column name=s45, symbol="$gs$r$b45$n", units=m, description="<y'*s>", type=double,  &end
&column name=s46, symbol="$gs$r$b46$n", description="<y'*delta>", type=double,  &end
&column name=s47, symbol="$gs$r$b47$n", units=s, description="<y'*t>", type=double,  &end
&column name=s5, symbol="$gs$r$b5$n", units=m, description="sqrt(<s*s>)", type=double,  &end
&column name=s56, symbol="$gs$r$b56$n", units=m, description="<s*delta>", type=double,  &end
&column name=s57, symbol="$gs$r$b57$n", units="m*s", description="<s*t>", type=double,  &end
&column name=s6, symbol="$gs$r$b6$n", description="sqrt(<delta*delta>)", type=double,  &end
&column name=s67, symbol="$gs$r$b67$n", units=s, description="<delta*t>", type=double,  &end
&column name=s7, symbol="$gs$r$b7$n", description="sqrt(<t*t>)", type=double,  &end
&column name=ma1, symbol="max$sb$ex$sb$e", units=m, description="maximum absolute value of x", type=double,  &end
&column name=ma2, symbol="max$sb$ex'$sb$e", description="maximum absolute value of x'", type=double,  &end
&column name=ma3, symbol="max$sb$ey$sb$e", units=m, description="maximum absolute value of y", type=double,  &end
&column name=ma4, symbol="max$sb$ey'$sb$e", description="maximum absolute value of y'", type=double,  &end
&column name=ma5, symbol="max$sb$e$gD$rs$sb$e", units=m, description="maximum absolute deviation of s", type=double,  &end
&column name=ma6, symbol="max$sb$e$gd$r$sb$e", description="maximum absolute value of delta", type=double,  &end
&column name=ma7, symbol="max$sb$e$gD$rt$sb$e", units=s, description="maximum absolute deviation of t", type=double,  &end
&column name=minimum1, symbol="x$bmin$n", units=m, type=double,  &end
&column name=minimum2, symbol="x'$bmin$n", units=m, type=double,  &end
&column name=minimum3, symbol="y$bmin$n", units=m, type=double,  &end
&column name=minimum4, symbol="y'$bmin$n", units=m, type=double,  &end
&column name=minimum5, symbol="$gD$rs$bmin$n", units=m, type=double,  &end
&column name=minimum6, symbol="$gd$r$bmin$n", units=m, type=double,  &end
&column name=minimum7, symbol="t$bmin$n", units=s, type=double,  &end
&column name=maximum1, symbol="x$bmax$n", units=m, type=double,  &end
&column name=maximum2, symbol="x'$bmax$n", units=m, type=double,  &end
&column name=maximum3, symbol="y$bmax$n", units=m, type=double,  &end
&column name=maximum4, symbol="y'$bmax$n", units=m, type=double,  &end
&column name=maximum5, symbol="$gD$rs$bmax$n", units=m, type=double,  &end
&column name=maximum6, symbol="$gd$r$bmax$n", units=m, type=double,  &end
&column name=maximum7, symbol="t$bmax$n", units=s, type=double,  &end
&column name=Sx, symbol="$gs$r$bx$n", units=m, description=sqrt(<(x-<x>)^2>), type=double,  &end
&column name=Sxp, symbol="$gs$r$bx'$n", description=sqrt(<(x'-<x'>)^2>), type=double,  &end
&column name=Sy, symbol="$gs$r$by$n", units=m, description=sqrt(<(y-<y>)^2>), type=double,  &end
&column name=Syp, symbol="$gs$r$by'$n", description=sqrt(<(y'-<y'>)^2>), type=double,  &end
&column name=Ss, symbol="$gs$r$bs$n", units=m, description=sqrt(<(s-<s>)^2>), type=double,  &end
&column name=Sdelta, symbol="$gs$bd$n$r", description=sqrt(<(delta-<delta>)^2>), type=double,  &end
&column name=St, symbol="$gs$r$bt$n", units=s, description=sqrt(<(t-<t>)^2>), type=double,  &end
&column name=ex, symbol="$ge$r$bx$n", units=m, description="geometric horizontal emittance", type=double,  &end
&column name=enx, symbol="$ge$r$bx,n$n", units=m, description="normalized horizontal emittance", type=double,  &end
&column name=ecx, symbol="$ge$r$bx,c$n", units=m, description="geometric horizontal emittance less dispersive contributions", type=double,  &end
&column name=ecnx, symbol="$ge$r$bx,cn$n", units=m, description="normalized horizontal emittance less dispersive contributions", type=double,  &end
&column name=ey, symbol="$ge$r$by$n", units=m, description="geometric vertical emittance", type=double,  &end
&column name=eny, symbol="$ge$r$by,n$n", units=m, description="normalized vertical emittance", type=double,  &end
&column name=ecy, symbol="$ge$r$by,c$n", units=m, description="geometric vertical emittance less dispersive contributions", type=double,  &end
&column name=ecny, symbol="$ge$r$by,cn$n", units=m, description="normalized vertical emittance less dispersive contributions", type=double,  &end
&column name=betaxBeam, symbol="$gb$r$bx,beam$n", units=m, description="betax for the beam, excluding dispersive contributions", type=double,  &end
&column name=alphaxBeam, symbol="$ga$r$bx,beam$n", description="alphax for the beam, excluding dispersive contributions", type=double,  &end
&column name=betayBeam, symbol="$gb$r$by,beam$n", units=m, description="betay for the beam, excluding dispersive contributions", type=double,  &end
&column name=alphayBeam, symbol="$ga$r$by,beam$n", description="alphay for the beam, excluding dispersive contributions", type=double,  &end
&data mode=binary, &end
)                 _BEG_      MARK���M�>Ͼ6G�=��wCF����mkB4��G�,zz���j�e��;�$�*���en
%J�>e�Aʉl�=�j��򜔽z�4���=
���*u >O.��L��; ��F���>[��̡�ڽ2rnԖp=c�f��n�=D�r"���;L�׺[�>���ց���ΟC�� �M��rR���_��>L����>���} KB<v�z�S�s?+�^��W�<�ʛ��6=)͋`,?�ֽd��?1�3^T+?�,a�?  ���?
`���_�?  ����I=)͋`,��ֽd���-��=%���r|��   �I�E����  �=�{E�'l��&*?��Mu�?1�3^T+?�,a�?  ���?
`���_�?  ����I=���M�>en
%J�> ��F���>L�׺[�>��_��>v�z�S�s?�ʛ��6=D����$�=���u4�>魾|���=[�e��>�U0hv�=,��]���>�aa�_v�=��Wɲ��>�%OmQ@Xz�! ����%�l�@%�H�	�?           BNCH      CHARGE���M�>Ͼ6G�=��wCF����mkB4��G�,zz���j�e��;�$�*���en
%J�>e�Aʉl�=�j��򜔽z�4���=
���*u >O.��L��; ��F���>[��̡�ڽ2rnԖp=c�f��n�=D�r"���;L�׺[�>���ց���ΟC�� �M��rR���_��>L����>���} KB<v�z�S�s?+�^��W�<�ʛ��6=)͋`,?�ֽd��?1�3^T+?�,a�?  ���?
`���_�?  ����I=)͋`,��ֽd���-��=%���r|��   �I�E����  �=�{E�'l��&*?��Mu�?1�3^T+?�,a�?  ���?
`���_�?  ����I=���M�>en
%J�> ��F���>L�׺[�>��_��>v�z�S�s?�ʛ��6=D����$�=���u4�>魾|���=[�e��>�U0hv�=,��]���>�aa�_v�=��Wɲ��>�%OmQ@Xz�! ����%�l�@%�H�	�?        
   BC2_IN_CNT      DRIF���M�>Ͼ6G�=��wCF����mkB4��G�,zz���j�e��;�$�*���en
%J�>e�Aʉl�=�j��򜔽z�4���=
���*u >O.��L��; ��F���>[��̡�ڽ2rnԖp=c�f��n�=D�r"���;L�׺[�>���ց���ΟC�� �M��rR���_��>L����>���} KB<v�z�S�s?+�^��W�<�ʛ��6=)͋`,?�ֽd��?1�3^T+?�,a�?  ���?
`���_�?  ����I=)͋`,��ֽd���-��=%���r|��   �I�E����  �=�{E�'l��&*?��Mu�?1�3^T+?�,a�?  ���?
`���_�?  ����I=���M�>en
%J�> ��F���>L�׺[�>��_��>v�z�S�s?�ʛ��6=D����$�=���u4�>魾|���=[�e��>�U0hv�=,��]���>�aa�_v�=��Wɲ��>�%OmQ@Xz�! ����%�l�@%�H�	�?��;E3�?	   BC2_DR_V1      DRIFR���b��>45�'M�=�=V�<ђ�yo�#����I9�E���-�9�:��967'���en
%J�>qRą��=�j��򜔽�J�����=
���*u >��T�&��;FTm:��>�����:Խ��2�
�o=���=w��5R�;L�׺[�>@Pt\���ΟC��WV�&sR�G���>� W��>��xfKB<v�z�S�s?�}3�W�<$˟`��6=T~��0?�ֽd��?ہЧ�(?�,a�?   �?
`���_�?  �Qd�I=T~��0��ֽd�����¥�!���r|��  ���I�E����   ��{E���6m�-?��Mu�?ہЧ�(?�,a�?   �?
`���_�?  �Qd�I=R���b��>en
%J�>FTm:��>L�׺[�>G���>v�z�S�s?$˟`��6=B����$�=���u4�>签|���=Y�e��>�U0hv�=.��]���>�aa�_v�=�Wɲ��>��;5��@�\F��3���ׁ�p"@cZ����?(_`�u�?   BC2_DR_QD_ED      DRIFg*ЫR3�>���S��=Uo'&���L�Eg���� �6��t���׌:�.�1����en
%J�>t��@Ґ=�j��򜔽.\3���=
���*u >�FUm#��;���2e�>f��kG�ӽ*m5� �o=]��o+�=0h�p�K�;L�׺[�>������ΟC��V��.sR������>Բ5N��>�B��KB<v�z�S�s?p�E�W�<�V�Z��6=Y$�� <0?�ֽd��?&\��'?�,a�?  @�?
`���_�?  ��`�I=Y$�� <0��ֽd�����9/� ���r|��  ���I�E����  @ɪ{E��O�%.?��Mu�?&\��'?�,a�?  @�?
`���_�?  ��`�I=g*ЫR3�>en
%J�>���2e�>L�׺[�>�����>v�z�S�s?�V�Z��6=B����$�=���u4�>签|���=Y�e��>�U0hv�=.��]���>�aa�_v�=�Wɲ��>��C~v@�<)����Z^Abef@�G�m	�?p@�,_��?
   BC2_QD_V01      QUADb"ϱ�q�>�s�M��=��!v���k�	�����`�G'�����ǲu:�������<tx�,�>��m�R�=kg��ȓ��)|�D-�=����'�>~≦W�;��Z�{��>���(�Oҽ߷o=ni�m�
�=n��;H�;W�����>���\����6+HBC���_&�32�n�����>��|F��>Cዪ KB<v�z�S�s?����W�<A�����6=�ۍ-�c0?��H�?L!��u'?Bݹ�0l?  ��?
`���_�?  @u]�I=�ۍ-�c0���H���|D�� ����e��  ���I�E����  ��{E�W�
,s.?v� ��5?L!��u'?Bݹ�0l?  ��?
`���_�?  @u]�I=b"ϱ�q�><tx�,�>��Z�{��>W�����>n�����>v�z�S�s?A�����6=�(�{G$�=�!�1�3�>�o��e��=�y�����>�7�xyv�=����̄�>�3��pv�=h�7Ą�>�ES��@]9nd���$��@�Ü��?p@�,_��?   BC2_COR      KICKERb"ϱ�q�>�s�M��=��!v���k�	�����`�G'�����ǲu:�������<tx�,�>��m�R�=kg��ȓ��)|�D-�=����'�>~≦W�;��Z�{��>���(�Oҽ߷o=ni�m�
�=n��;H�;W�����>���\����6+HBC���_&�32�n�����>��|F��>Cዪ KB<v�z�S�s?����W�<A�����6=�ۍ-�c0?��H�?L!��u'?Bݹ�0l?  ��?
`���_�?  @u]�I=�ۍ-�c0���H���|D�� ����e��  ���I�E����  ��{E�W�
,s.?v� ��5?L!��u'?Bݹ�0l?  ��?
`���_�?  @u]�I=b"ϱ�q�><tx�,�>��Z�{��>W�����>n�����>v�z�S�s?A�����6=�(�{G$�=�!�1�3�>�o��e��=�y�����>�7�xyv�=����̄�>�3��pv�=h�7Ą�>�ES��@]9nd���$��@�Ü��?p@�,_��?   BC2_BPM      MONIb"ϱ�q�>�s�M��=��!v���k�	�����`�G'�����ǲu:�������<tx�,�>��m�R�=kg��ȓ��)|�D-�=����'�>~≦W�;��Z�{��>���(�Oҽ߷o=ni�m�
�=n��;H�;W�����>���\����6+HBC���_&�32�n�����>��|F��>Cዪ KB<v�z�S�s?����W�<A�����6=�ۍ-�c0?��H�?L!��u'?Bݹ�0l?  ��?
`���_�?  @u]�I=�ۍ-�c0���H���|D�� ����e��  ���I�E����  ��{E�W�
,s.?v� ��5?L!��u'?Bݹ�0l?  ��?
`���_�?  @u]�I=b"ϱ�q�><tx�,�>��Z�{��>W�����>n�����>v�z�S�s?A�����6=�(�{G$�=�!�1�3�>�o��e��=�y�����>�7�xyv�=����̄�>�3��pv�=h�7Ą�>�ES��@]9nd���$��@�Ü��?�!VA�?
   BC2_QD_V01      QUAD�l�xP��>y3O��=O���Q[�����<���!Zߋ/���nyhZ:�[�h�w��dc�>��S�˻�=�W�T�񒽜?=}^�=خ���>ֲ���;?�����>W�6�ѽ{ԨԺ�o=v�o]	�=N���H�;��\�>���=�ҥҤ�Z=����B;����>�s�?��>$)~`�JB<v�z�S�s?�me7�W�<I
_���6=i�iVl�0?���XD�?��d�3'?(A�k�?  `ۀ?
`���_�?  �"Z�I=i�iVl�0����XD���>�k�� ����Ql�   ��I�E����  �k�{E����~��.?���g�P?��d�3'?(A�k�?  `ۀ?
`���_�?  �"Z�I=�l�xP��>dc�>?�����>��\�>����>v�z�S�s?I
_���6=|��U�#�=����Z3�>QJ����=_�R��>�.Q�v�=r˵�ۄ�>^*��v�=j��ӄ�>а�e�~@��T]����tY�!@�$;x�{�?�76a�?   BC2_DR_QD_ED      DRIF�JIk���>؆����=�rs�_��~U�(L飽;%�� ���G�Q6;:�����R��dc�>��[��=�W�T��{�G|^�=خ���>�4�k�;tLB/�>2��m|н��BJ�o=��;��	�=iTZ(L�;��\�>絡��=�ҥҤ�Z=�Tx�B;a����>I�W9��>-�r��JB<v�z�S�s?���x�W�<�g���6=#��/�0?���XD�?�d�Jj�&?(A�k�?  �π?
`���_�?  ��V�I=#��/�0����XD���b�9o ����Ql�   ��I�E����  ���{E���R��/?���g�P?�d�Jj�&?(A�k�?  �π?
`���_�?  ��V�I=�JIk���>dc�>tLB/�>��\�>a����>v�z�S�s?�g���6=|��U�#�=����Z3�>QJ����=_�R��>�.Q�v�=r˵�ۄ�>^*��v�=j��ӄ�>2�6��@��,����r���2�@Ȍ�6���?��& �	@	   BC2_DR_V2      DRIF��keک?�O<����=q_��ݨ齔:�n�½up7Sm���`��"�_'"���dc�>*[]D���W�T�񒽄��^�=خ���>�Ӹ�;���@�� ?E�v͙��=� P%��p=o$�)sX�=�>'M� �;��\�>�5�G�=�ҥҤ�Z=�n�k�B;��A����>�:Z���>��3�IB<v�z�S�s?|�c�U�<Hk��F�6=����B?���XD�?��B��V4?(A�k�?  `�z?
`���_�?   o�I=����B����XD��tg�FqP2����Ql�  ���I�E����  `0zE�J�ޗ��A?���g�P?��B��V4?(A�k�?  `�z?
`���_�?   o�I=��keک?dc�>���@�� ?��\�>��A����>v�z�S�s?Hk��F�6=}��U�#�=����Z3�>SJ����=a�R��>�.Q�v�=q˵�ۄ�>]*��v�=i��ӄ�>i:#�EF@�<������ޗ��.@@��лA����>5@   BC2_DR_QD_ED      DRIF�����?��%�Ͱ�=��#6��r՚�1�½�1���Qy��q�"��(ݳs��dc�>�<L-�?���W�T��c��1^�=خ���>�9����;v&�� ?W`�\�@�=:
�)h�p=�E�Y�=Tt���;��\�>��M�5�=�ҥҤ�Z=P�[�B;������>NQڰ��>uW�ũIB<v�z�S�s?ş�\�U�<� a�C�6=_}�_�B?���XD�?��|@;�4?(A�k�?  �{z?
`���_�?   �k�I=_}�_�B����XD���J����2����Ql�  ���I�E����  �UzE�� ����A?���g�P?��|@;�4?(A�k�?  �{z?
`���_�?   �k�I=�����?dc�>v&�� ?��\�>������>v�z�S�s?� a�C�6=���U�#�=����Z3�>_J����=m�R��>�.Q�v�=u˵�ۄ�>a*��v�=m��ӄ�>�LI�JF@����z��ZC|�*/@A"R��{��8n�]@
   BC2_QD_V02      QUADm��*7?<�K1eP >�\��L꽢����=�.��I������Yy"�|�Ч0�仢�%�SJ?�q�l$P޽l~�=�+�

r�A�΍�������3�d�� ?>(���`id��p=o@{+��=%����;wT̷U��>�Jؿ��^�GZGW�׽�A��-~����p����>$Y$[��>� !�IB<v�z�S�s?�qn��U�<��W�@�6=�S�n�B?�$��4?�%�5a�4?����(�?  `>z?
`���_�?  �h�I=�S�n�B��$��4�$}�7�2�����(��  ���I�E����   �zE���#9B?z�c�|<4?�%�5a�4?p'x�LX?  `>z?
`���_�?  �h�I=m��*7?��%�SJ?3�d�� ?wT̷U��>��p����>v�z�S�s?��W�@�6=ј���=��V �(�>��1����=��PQ�>����[�=
��`0j�>����[�=	��b'j�>���� G@mֳ�N:��xp�/@G�p��@�8n�]@   BC2_COR      KICKERm��*7?<�K1eP >�\��L꽢����=�.��I������Yy"�|�Ч0�仢�%�SJ?�q�l$P޽l~�=�+�

r�A�΍�������3�d�� ?>(���`id��p=o@{+��=%����;wT̷U��>�Jؿ��^�GZGW�׽�A��-~����p����>$Y$[��>� !�IB<v�z�S�s?�qn��U�<��W�@�6=�S�n�B?�$��4?�%�5a�4?����(�?  `>z?
`���_�?  �h�I=�S�n�B��$��4�$}�7�2�����(��  ���I�E����   �zE���#9B?z�c�|<4?�%�5a�4?p'x�LX?  `>z?
`���_�?  �h�I=m��*7?��%�SJ?3�d�� ?wT̷U��>��p����>v�z�S�s?��W�@�6=ј���=��V �(�>��1����=��PQ�>����[�=
��`0j�>����[�=	��b'j�>���� G@mֳ�N:��xp�/@G�p��@�8n�]@   BC2_BPM      MONIm��*7?<�K1eP >�\��L꽢����=�.��I������Yy"�|�Ч0�仢�%�SJ?�q�l$P޽l~�=�+�

r�A�΍�������3�d�� ?>(���`id��p=o@{+��=%����;wT̷U��>�Jؿ��^�GZGW�׽�A��-~����p����>$Y$[��>� !�IB<v�z�S�s?�qn��U�<��W�@�6=�S�n�B?�$��4?�%�5a�4?����(�?  `>z?
`���_�?  �h�I=�S�n�B��$��4�$}�7�2�����(��  ���I�E����   �zE���#9B?z�c�|<4?�%�5a�4?p'x�LX?  `>z?
`���_�?  �h�I=m��*7?��%�SJ?3�d�� ?wT̷U��>��p����>v�z�S�s?��W�@�6=ј���=��V �(�>��1����=��PQ�>����[�=
��`0j�>����[�=	��b'j�>���� G@mֳ�N:��xp�/@G�p��@�����@
   BC2_QD_V02      QUAD�0���0?�,���.>�od�)�꽑�э�5�=Q--I��j�q�"�����仦�`��?�`Cʿ ���j�{�=h�J?���B0UJ�p�r�e�ջ�)Ϣ�� ?���Ty	���%��7p=[���	h�=ϛ�7�;�q�����>S�Nq��n�`�$�Gp�k�hG�����YU���>�0ⵞ�>p!Z��IB<v�z�S�s?>e���U�<����<�6=٥]ՍrC?#�>�tC?�arX)&4?aό��,?  @�x?
`���_�?  ��c�I=٥]ՍrC�#�>�tC��� /2�aό��,�  �k�I�E����  ��zE�n�Y�l�B?h���B?�arX)&4?~<f�Kg)?  @�x?
`���_�?  ��c�I=�0���0?��`��?�)Ϣ�� ?�q�����>��YU���>v�z�S�s?����<�6=m�pG�E�=/�2�RU�>�T��w��=1�E��>6;��G�=\
&V�>Sj��G�=o�V�>�����YH@U�y��H����.@���K&@���x�@   BC2_DR_QD_ED      DRIF�8��!D?ָ.ht0>�k������ ���=��o�Cͧ�<y���&#��S���9廦�`��?8��!���j�{�=���S�?���B0UJ���9��ջ룆�	 ?�Gt[����܅��o=�>��i�=-��u���;�q�����>�\�2s�n�`�$�Gp�o�gf憫� �x���>�W�ۛ�>��ZR�IB<v�z�S�s?��4O�U�<!4|�7�6=�Y�>=FD?#�>�tC?c"j��3?aό��,?  `�v?
`���_�?  ��]�I=�Y�>=FD�#�>�tC�?%� �1�aό��,�  @7 �I�E����    zE�-��vC?h���B?c"j��3?~<f�Kg)?  `�v?
`���_�?  ��]�I=�8��!D?��`��?룆�	 ?�q�����> �x���>v�z�S�s?!4|�7�6=m�pG�E�=/�2�RU�>�T��w��=1�E��>n;��G�=9\
&V�>�j��G�=<o�V�>ix��}J@���i �I�C�z�U$,@ɅOL�%@@��E@	   BC2_DR_V3      DRIF�j�D5l?`� �(3>��A�	�AlVBH-�=��]}<���q�A.%��\� ue绦�`��?ۗ�/	���j�{�=g�NC���B0UJ��k`��ջR� ��(�>�T!B���� �rM�h=���.���=�R�=�;�q�����>E�bJ��n�`�$�Gp�1���^����wR��>���p��>	�M��IB<v�z�S�s?�+�ǄU�<_���6=��g*H?#�>�tC?�}fN�0?aό��,?   Yk?
`���_�?  ��C�I=��g*H�#�>�tC��(�>55.�aό��,�  �]-�I�E����  �<zE���|,�4G?h���B?�}fN�0?~<f�Kg)?   Yk?
`���_�?  ��C�I=�j�D5l?��`��?R� ��(�>�q�����>�wR��>v�z�S�s?_���6=�pG�E�=��2�RU�>cN��w��=ąE��>6;��G�=\
&V�>Sj��G�=o�V�>V��R@��t⃱N�����,$@5k�#�>"@,cL�ʪ@   BC2_DR_QD_ED      DRIF9���?[�X�A�3>Mg�	��꽒��G"�=q�ƞx������碜%��wj��绦�`��?�Q�U*���j�{�=����D���B0UJ�7폥ջq�i���>���E	�>�{C�g=z�"���=ת�� �;�q�����>�4���n�`�$�Gp�b�R�A����<[
��>������>���}IB<v�z�S�s?sP��~U�<��}��6=v���H?#�>�tC?��t 0?aό��,?  ��h?
`���_�?  ��=�I=v���H�#�>�tC��@(b� -�aό��,�  �(0�I�E����  @�zE�M�+ H?h���B?��t 0?~<f�Kg)?  ��h?
`���_�?  ��=�I=9���?��`��?q�i���>�q�����>�<[
��>v�z�S�s?��}��6=�pG�E�=��2�RU�>cN��w��=ąE��>;��G�=�[
&V�>7j��G�=�n�V�>��V6T@�5p���O��)����"@�c�E�!@U�ې��@
   BC2_QD_V03      QUADEj}��?���2��!>P�'���Y�vS��=�w:-��D!y��%�j�q��̘{iV��>�]���Lҽ���sQo�=��ni�i���rj���x��v��F�D\[�>quFZ������ڙf=�����=�wɠF&�;G�H��>�m"��d��2�B��߽ZX0������Z���>��� ��>�<yIB<v�z�S�s?mE�yU�<�9�6=�ԭ?�I?U�}z�0?��l~�./?#�4�\?  ��g?
`���_�?  ��9�I=�ԭ?�I�U�}z�0�١W>�\,�#�4�\�  ��1�I�E����  @jzE�ȯ�Z��H?�f�8�Q0?��l~�./?��ñ��?  ��g?
`���_�?  ��9�I=Ej}��?̘{iV��>F�D\[�>G�H��>��Z���>v�z�S�s?�9�6=��O`7�=;kbb�-�>�4K���=�'ٻw�>��VDS�=��@|a�>6/�/;S�=t�gsa�>��1�:U@F��<�f��T�!@�!�gv�@U�ې��@   BC2_COR      KICKEREj}��?���2��!>P�'���Y�vS��=�w:-��D!y��%�j�q��̘{iV��>�]���Lҽ���sQo�=��ni�i���rj���x��v��F�D\[�>quFZ������ڙf=�����=�wɠF&�;G�H��>�m"��d��2�B��߽ZX0������Z���>��� ��>�<yIB<v�z�S�s?mE�yU�<�9�6=�ԭ?�I?U�}z�0?��l~�./?#�4�\?  ��g?
`���_�?  ��9�I=�ԭ?�I�U�}z�0�١W>�\,�#�4�\�  ��1�I�E����  @jzE�ȯ�Z��H?�f�8�Q0?��l~�./?��ñ��?  ��g?
`���_�?  ��9�I=Ej}��?̘{iV��>F�D\[�>G�H��>��Z���>v�z�S�s?�9�6=��O`7�=;kbb�-�>�4K���=�'ٻw�>��VDS�=��@|a�>6/�/;S�=t�gsa�>��1�:U@F��<�f��T�!@�!�gv�@U�ې��@   BC2_BPM      MONIEj}��?���2��!>P�'���Y�vS��=�w:-��D!y��%�j�q��̘{iV��>�]���Lҽ���sQo�=��ni�i���rj���x��v��F�D\[�>quFZ������ڙf=�����=�wɠF&�;G�H��>�m"��d��2�B��߽ZX0������Z���>��� ��>�<yIB<v�z�S�s?mE�yU�<�9�6=�ԭ?�I?U�}z�0?��l~�./?#�4�\?  ��g?
`���_�?  ��9�I=�ԭ?�I�U�}z�0�١W>�\,�#�4�\�  ��1�I�E����  @jzE�ȯ�Z��H?�f�8�Q0?��l~�./?��ñ��?  ��g?
`���_�?  ��9�I=Ej}��?̘{iV��>F�D\[�>G�H��>��Z���>v�z�S�s?�9�6=��O`7�=;kbb�-�>�4K���=�'ٻw�>��VDS�=��@|a�>6/�/;S�=t�gsa�>��1�:U@F��<�f��T�!@�!�gv�@~kS��@
   BC2_QD_V03      QUAD��0���?f�_/���.W�!i���x�8�=��_��᪽�T��%��1d�P��O�� ���>@8��3Ͳ=��/��W;0Gԑ=����>�^	W���;�]u�	�>٪B|pн��(�Y�e=�h�:Cs�=t+���;��S����>���uWLV��j5�Lѽ^�;`����s����>�x?��>Y ��vIB<v�z�S�s?�vU�<���"�6=���|��I?<��s�g?#�n�.?_�����>  ��g?
`���_�?  `6�I=���|��I��3�)xU���k2-,�_������  `�1�I�E����  `�
zE��+OʦH?<��s�g?#�n�.?"� lzq�>  ��g?
`���_�?  `6�I=��0���?O�� ���>�]u�	�>��S����>s����>v�z�S�s?���"�6=될�̔�=���Ĥ�>H09��k�=b?w��{�>�]A[�_�=��^#>n�>�kHj�_�=j�u+5n�>�����T@j�T�@?ɂ!@/� ���?jm#r;(@   BC2_DR_QD_ED      DRIF�icߑ?eL�׸�<�lj(���x�ȼ='�5ɀ��*d�7r%���wբ�O�� ���>��譲=��/����FIGԑ=����>��k����;���a���>
�FFн�H}e=��(	(�=��@](�;��S����>�v{WLV��j5�Lѽm��à���m*H���>+9���>FN9tIB<v�z�S�s?���RrU�<i�s�6=��vUэI?<��s�g?��[���.?_�����>  ��g?
`���_�?  `�2�I=��vUэI��3�)xU�
茰,�_������   �1�I�E����  ��zE�����"�H?<��s�g?��[���.?"� lzq�>  ��g?
`���_�?  `�2�I=�icߑ?O�� ���>���a���>��S����>m*H���>v�z�S�s?i�s�6=될�̔�=���Ĥ�>H09��k�=b?w��{�>�]A[�_�=��^#>n�>�kHj�_�=j�u+5n�>���(�T@O:*:�@h�_�\� @�L�ЗW�?=:�>�@	   BC2_DR_V4      DRIF\�y�/?LE�Y���IS��($x׬5�=��Ea���Q�gE$��UG���O�� ���>uu�ͧ�=��/��?AHԑ=����>ҏEף��;bi*E�h�>��I�ν���c4Bc=fm	n���=d -�s+�;��S����>����WLV��j5�Lѽ�`�{��������>�����>v���hIB<v�z�S�s?����`U�<�)��6=^\^_�I?<��s�g?;l���l.?_�����>  �Wg?
`���_�?  ��!�I=^\^_�I��3�)xU�J��	u�+�_������  �2�I�E����  ��yE�����H?<��s�g?;l���l.?"� lzq�>  �Wg?
`���_�?  ��!�I=\�y�/?O�� ���>bi*E�h�>��S����>����>v�z�S�s?�)��6=Ґ��̔�=���Ĥ�>.09��k�=G?w��{�>�]A[�_�=��^#>n�>�kHj�_�=j�u+5n�>�޶��S@�]/JP@�K�L6 @+�2���?=:�>�@	   BC2_DR_20      DRIF�5'�r:?�'ߎ�i�>�r�O.潎�v߉ź=�B�\C���:/� �"wv}�O�� ���>���Մ��=��/���}Jԑ=����>�JX�w��;?#!�0�>ҋI� ˽��u 2^[=_iT�W�=��of�;��S����>����WLV��j5�Lѽ�|M�u���t����>_G(0��>3ڷ�KIB<v�z�S�s?w;nU4U�<�(�'��6=j!�[�G?<��s�g?~�
ގ-?_�����>  ��f?
`���_�?  `���I=j!�[�G��3�)xU�n�'A`�+�_������  ��2�I�E����  ���yE���H[��F?<��s�g?~�
ގ-?"� lzq�>  ��f?
`���_�?  `���I=�5'�r:?O�� ���>?#!�0�>��S����>t����>v�z�S�s?�(�'��6=����̔�=ꀍ�Ĥ�>09��k�=.?w��{�>�]A[�_�=��^#>n�>�kHj�_�=k�u+5n�>	�)��R@�5s�q�@���B@憥��?׋g߾� @   BC2_DP_DIP1   	   CSRCSBENDӭ�ԎH?tuH݇�.>'��z�tL@T>��=q������Mxֆ�yM�/�8N�-�xƱy?if�&���=ڣ��c�s��)(�,�(�2�O�kע������e��qw���>�x��ƽ���v��=������=��܅v��;�b����>�ضHHKo�n�\�G9ѽ΃�V������ ���>���D�>p����B<�*��)�s?h[ �N��<R���)7=�D�%)G?Wz�J'3?{tᯇ�,?��V�W��>  `ڋ)?�xV1�`�?  �v��L=(��`�5D�Wz�J'3�\q�Z�*+���V�W���  `��I�FG�  ��W>I��D�%)G?*1KV��1?{tᯇ�,?�h�R�b�>  `ڋ)?�xV1�`�?  �v��L=ӭ�ԎH?-�xƱy?�qw���>�b����>�� ���>�*��)�s?R���)7=]�� ͺA>ު����>1�k���=
�J���>x|ϒ�_�=~%�g&n�>Ƃ���_�=��yn�>6���M@F�X�q@��+�@#��5��?׋g߾�!@   BC2_DR_SIDE_C      CSRDRIFTzҹ���?˸� .F>�K&���T��4S�=�"g�ٻ$�&ݙRD���m�|b�-�xƱy?:,G�ê=ڣ��c�s�t�vZS�(��6Pf�آ���k��e���-���>΅�:�½�I:T��=��竼�=d0r�;�b����>�ֽ&"Ko�0��"z4ѽ�W�����v�l���>q+��N�>pJ�q�B<�y�V�s?��&����<�x� �)7=�t��I?Wz�J'3?�hq�+?��V�W��>  ��*?����Zg�?  �-��L=�0ޗYF�Wz�J'3���n2�*���V�W���  `��I�[	dMQ肿  @E6>I��t��I?*1KV��1?�hq�+?�h�R�b�>  ��*?����Zg�?  �-��L=zҹ���?-�xƱy?��-���>�b����>�v�l���>�y�V�s?�x� �)7=\�� ͺA>Z�c�x��>"�ZY���=�{]W"��>x|ϒ�_�=ohn�>n����_�=T�~n�>{�}g��I@Y�V�R@��'� �@㗌	�\�?�=���(@   BC2_DR_SIDE      DRIF�۪l8�>?�V��l>�{�����k�d�%�=�QzN^�H����p�¾���p��-�xƱy?S��Gt�=ڣ��c�s���R*>�(��6Pf�آ��Ӳ4��e�ꏪ�+��>���۽�=��b�qW=�0ٮ;5�J���eH�;�b����>� ��EJo�0��"z4ѽ�&�%����o�L���>]^���>tt!���B<�y�V�s?i ����<�E�S�)7=�g8�TU?Wz�J'3?��bg-?��V�W��>  �?�,?����Zg�?  `^��L=�g8�TU�Wz�J'3���bg-���V�W���   ��I�[	dMQ肿  @�_=I��!���R?*1KV��1?�Ql?u�%?�h�R�b�>  �?�,?����Zg�?  `^��L=�۪l8�>?-�xƱy?ꏪ�+��>�b����>o�L���>�y�V�s?�E�S�)7=[�� ͺA>Y�c�x��>� VY���=��XW"��>x|ϒ�_�=ohn�>n����_�=T�~n�>`7�̙Y5@XkҦS@*�7�_@��i��ſf����G)@   BC2_DP_DIP2   	   CSRCSBEND���t�i@?�x�mu�?6L{Y��M e�=�xv	<�R��6c>ľ>?���x�p�F�m��>@h&��=� Ǧ)��`c���=,X�����=- �S�^�;�B�Ld�>o1��`�=�mO�@Z�ŏ�)�
?�:n(��m��9��>p����^��W��\�н�[T�����<���d[�>��b��p>dN�w�$<[�Rt�s?��
��<á��mH(=~*{�>V?e|kg?=B&�q/?ፇZ��>  �d`��>�B.3�l�?  �眅;=~*{�>V�v�#��=B&�q/�ፇZ���  ���e��y�؁x΂�  �n�Z8�o����R?e|kg?�@Đ'�%?�94�W<�>  �d`��>�B.3�l�?  �眅;=���t�i@?p�F�m��>�B�Ld�>m��9��><���d[�>[�Rt�s?á��mH(=�����/>|��.��>�܀���=
�����>Oݷ*�_�=Ά��m�>�I\�_�=��m�>� �#�2@`h��8@ ��"w@� KM�ҿf�����)@   BC2_DR_CENT_C      CSRDRIFTMh'ǒh@?� �Ȇ�FO4�q�E����7�=^$�<��聍�?ľ�k%V_�x�p�F�m��>P�t�|=� Ǧ)���!���=;iOQ�=�.�(�^�;h�_���>��]�E�=.��G?�`��ɇ�9� �Y��m��9��>j!�`��^�L�-���н�^�{���7tDi[�>��N��p>:3\�[�$<.�$���s?T������<�qJH(=�r�O��U?e|kg?����%0?ፇZ��>  �i���>�)��0u�?  @o��;=�r�O��U�v�#������%0�ፇZ���  ��ue�����\ǂ�  ��:Z8�����R?e|kg?U�eP
�&?�94�W<�>  �i���>�)��0u�?  @o��;=Mh'ǒh@?p�F�m��>h�_���>m��9��>7tDi[�>.�$���s?�qJH(=�����/>xz��
��>Ef��>�=/���Q&�>Oݷ*�_�=��	��m�>�z`�_�=�غ��m�>�����0@2��w
@_��.�@|܈�ֿf�����)@   BC2_BPM      MONIMh'ǒh@?� �Ȇ�FO4�q�E����7�=^$�<��聍�?ľ�k%V_�x�p�F�m��>P�t�|=� Ǧ)���!���=;iOQ�=�.�(�^�;h�_���>��]�E�=.��G?�`��ɇ�9� �Y��m��9��>j!�`��^�L�-���н�^�{���7tDi[�>��N��p>:3\�[�$<.�$���s?T������<�qJH(=�r�O��U?e|kg?����%0?ፇZ��>  �i���>�)��0u�?  @o��;=�r�O��U�v�#������%0�ፇZ���  ��ue�����\ǂ�  ��:Z8�����R?e|kg?U�eP
�&?�94�W<�>  �i���>�)��0u�?  @o��;=Mh'ǒh@?p�F�m��>h�_���>m��9��>7tDi[�>.�$���s?�qJH(=�����/>xz��
��>Ef��>�=/���Q&�>Oݷ*�_�=��	��m�>�z`�_�=�غ��m�>�����0@2��w
@_��.�@|܈�ֿf����G*@   BC2_DR_CENT      DRIF�����g@?����#���yr�ҏ=>���r�=�\~�Z<�#�l��?ľ����x�p�F�m��>�υ��q=� Ǧ)���+����=;iOQ�=�ȳ��^�;���g�>�-�	�*�=�1g6�d�rq� GI��[4y��m��9��>6&hg��^�L�-���н|��hl���z*ۘh[�>d��	��p>�s�+=�$<.�$���s?Q������<775�&H(=�O���U?e|kg?���|��0?ፇZ��>  �2���>�)��0u�?  ��q�;=�O���U�v�#�����|��0�ፇZ���  �De�����\ǂ�  �&�Y8�6�v�ZR?e|kg?$��'?�94�W<�>  �2���>�)��0u�?  ��q�;=�����g@?p�F�m��>���g�>m��9��>z*ۘh[�>.�$���s?775�&H(=�����/>xz��
��>Ef��>�=/���Q&�>Oݷ*�_�=��	��m�>�z`�_�=�غ��m�>�͜��.@&�/��	@s��%<�@P�&��eٿ�t&{+@   BC2_DP_DIP3   	   CSRCSBEND�$�">?l��l�Fy��Đ�=�DƼfC�=C������Nu��¾r���A�D���[?��5�3��C�����]�o[m��=�&�k�>9L�q�� <�u���>��o��=�����Jc����L�%�}~Y���5��^Ā�>�:�(R0�W�kcaϽſ7��-l��f㠓�>�����:>M�Dʮʼ;C�E'us?ۣ�#0�u<�Ǿ;���<�G�M8T?hE;i"3?��?lǗ1?"�`�i�>   ud!�>��(�J�?   ͒�=�G�M8T�R����1���?lǗ1�"�`�i��   p��̾*�d�u}��   �_���T��P?hE;i"3?�O�w)?!Z�D�>   ud!�>��(�J�?   ͒�=�$�">?D���[?�u���>5��^Ā�>�f㠓�>C�E'us?�Ǿ;���<��н4>�?��R�>d�z���=_S7�_�>a��[�_�=c)9�l�>����_�=9�l�l�>@*����)@;�C8l�@�?��@G��*����t&{,@   BC2_DR_SIDE_C      CSRDRIFT���U��:?�N��*Vi��x����=m��ϓ	�=A��]5�kʶ�6��>�j�)�=�D���[?���!���C������6����=��Y~�l�>�1��,� <xO��g��>�!�'��=T�U�!Pd�a��+H��{)���5��^Ā�>����
0��a�KϽ�ۮHn'l�������>E��c�:>@7��ʼ;�/�!ys?�ԥ���u<W�铛��<�)&�Q?hE;i"3?�0B�\o2?"�`�i�>   5.�>&���`.�?   6��=�)&�Q�R����1��0B�\o2�"�`�i��   v�f̾7p4�~��   �L��_�i�L?hE;i"3?��ei`+?!Z�D�>   5.�>&���`.�?   6��=���U��:?D���[?xO��g��>5��^Ā�>������>�/�!ys?W�铛��<:�н4>x�[H�>^tp����=Ox,`��>a��[�_�=S�6f�j�>��31�_�=���6�j�>�Da)H'@�y�sd@/���U@�*�7���{��Dq1@   BC2_DR_SIDE      DRIF�e��
?�2W2���M ڴ���k<}V���;��nɽz^�t�����Yn��D���[?�F��O7��C�������9�c��=��Y~�l�>ܾ�-�� < �;�X��>���|���= >����j��T�g�Eok�K���5��^Ā�>���G0��a�KϽ�Ke�> l���\w���>/Tw4��:>�K^Q2̼;�/�!ys?[bO�7�u<�IQ=���<d���ě#?hE;i"3?Hc��7?"�`�i�>   �9>�>&���`.�?   qn=d���ě#�R����1�Hc��7�"�`�i��   F��˾7p4�~��   @��
��k;|�5 ?hE;i"3?����2?!Z�D�>   �9>�>&���`.�?   qn=�e��
?D���[? �;�X��>5��^Ā�>��\w���>�/�!ys?�IQ=���<6�н4>r�[H�>��j����=��&`��>`��[�_�=R�6f�j�>��31�_�=���6�j�>4w�;�?1Z�<t�?�T5K_z)@i��.���ם���
2@   BC2_DP_DIP4   	   CSRCSBEND:�-�Z��>��𶜖�=M��w���Xo���������\b��Ɯ��5���0ei����bV?�>�3�a�,��CDqmd�����"�q�=���F�U>�S����;ָ@�.> ?������=�~�I����"���J����K�]#Q�>b�|3܌��y*�uʽ�S�U�k�Q^�>Te�>>��,���;�Qcm6r?{�Fؗ�y<���u���<ǋ�r�P?�x��G6?K����8?�\�:�+�>   }g�>(@x���?   E�=�?�v��x��G6�K����8��\�:�+��   z�˾\;e��   0���ǋ�r�P?�>p-��?��m�3?�%t����>   }g�>(@x���?   E�=:�-�Z��>��bV?�>ָ@�.> ?K�]#Q�>k�Q^�>�Qcm6r?���u���<f�IX��=dp�t���>A�1��'�=FPc�2�>��M�_�=��{�h�>li+�_�=��zdh�>����?q�����ɿ�t`�,@b�M���ם���
2@
   BC2_IN_CNT      DRIF:�-�Z��>��𶜖�=M��w���Xo���������\b��Ɯ��5���0ei����bV?�>�3�a�,��CDqmd�����"�q�=���F�U>�S����;ָ@�.> ?������=�~�I����"���J����K�]#Q�>b�|3܌��y*�uʽ�S�U�k�Q^�>Te�>>��,���;�Qcm6r?{�Fؗ�y<���u���<ǋ�r�P?�x��G6?K����8?�\�:�+�>   }g�>(@x���?   E�=�?�v��x��G6�K����8��\�:�+��   z�˾\;e��   0���ǋ�r�P?�>p-��?��m�3?�%t����>   }g�>(@x���?   E�=:�-�Z��>��bV?�>ָ@�.> ?K�]#Q�>k�Q^�>�Qcm6r?���u���<f�IX��=dp�t���>A�1��'�=FPc�2�>��M�_�=��{�h�>li+�_�=��zdh�>����?q�����ɿ�t`�,@b�M���ם����2@   BC2_DR_20_C      CSRDRIFTa+��b�>����q��=I��y�Ž��g}��a� -��=LqdG|�5>�6��0�;��bV?�>.�w����CDqmd���9V)��=�b7�[U>�����;���r	?�*�)��=���J�d=:�����[|���r��K�]#Q�>Y	����î�[ʽ�Ж��U���[��\�>W�#FM�>>AN��9�;b�1r?؉�uy<h�����<
z�5F)#?�x��G6?l��+��9?�\�:�+�>   �Ip�>r���eF�?   WE	=u"o�!��x��G6�l��+��9��\�:�+��   *{˾r���   ���
z�5F)#?�>p-��?�ބ�4?�%t����>   �Ip�>r���eF�?   WE	=a+��b�>��bV?�>���r	?K�]#Q�>��[��\�>b�1r?h�����<f�IX��=l��ɯ��>�p��?�=T�c� H�>��M�_�=����f�>���G�_�=)�ښf�>6e���L�?��:���<?JO��/@�	�KB��ם����2@
   BC2_IN_CNT      DRIFa+��b�>����q��=I��y�Ž��g}��a� -��=LqdG|�5>�6��0�;��bV?�>.�w����CDqmd���9V)��=�b7�[U>�����;���r	?�*�)��=���J�d=:�����[|���r��K�]#Q�>Y	����î�[ʽ�Ж��U���[��\�>W�#FM�>>AN��9�;b�1r?؉�uy<h�����<
z�5F)#?�x��G6?l��+��9?�\�:�+�>   �Ip�>r���eF�?   WE	=u"o�!��x��G6�l��+��9��\�:�+��   *{˾r���   ���
z�5F)#?�>p-��?�ބ�4?�%t����>   �Ip�>r���eF�?   WE	=a+��b�>��bV?�>���r	?K�]#Q�>��[��\�>b�1r?h�����<f�IX��=l��ɯ��>�p��?�=T�c� H�>��M�_�=����f�>���G�_�=)�ښf�>6e���L�?��:���<?JO��/@�	�KB��ם����2@	   BC2_WA_OU      WATCHa+��b�>����q��=I��y�Ž��g}��a� -��=LqdG|�5>�6��0�;��bV?�>.�w����CDqmd���9V)��=�b7�[U>�����;���r	?�*�)��=���J�d=:�����[|���r��K�]#Q�>Y	����î�[ʽ�Ж��U���[��\�>W�#FM�>>AN��9�;b�1r?؉�uy<h�����<
z�5F)#?�x��G6?l��+��9?�\�:�+�>   �Ip�>r���eF�?   WE	=u"o�!��x��G6�l��+��9��\�:�+��   *{˾r���   ���
z�5F)#?�>p-��?�ބ�4?�%t����>   �Ip�>r���eF�?   WE	=a+��b�>��bV?�>���r	?K�]#Q�>��[��\�>b�1r?h�����<f�IX��=l��ɯ��>�p��?�=T�c� H�>��M�_�=����f�>���G�_�=)�ښf�>6e���L�?��:���<?JO��/@�	�KB��