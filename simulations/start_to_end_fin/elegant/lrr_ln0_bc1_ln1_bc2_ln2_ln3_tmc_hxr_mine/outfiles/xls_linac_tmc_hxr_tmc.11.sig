SDDS1
!# little-endian
&description text="sigma matrix--input: xls_linac_tmc_hxr_tmc.track.ele  lattice: xls_linac_tmc_hxr.11.lte", contents="sigma matrix", &end
&parameter name=Step, description="Simulation step", type=long, &end
&parameter name=SVNVersion, description="SVN version number", type=string, fixed_value=unknown, &end
&column name=s, units=m, description=Distance, type=double,  &end
&column name=ElementName, description="Element name", format_string=%10s, type=string,  &end
&column name=ElementOccurence, description="Occurence of element", format_string=%6ld, type=long,  &end
&column name=ElementType, description="Element-type name", format_string=%10s, type=string,  &end
&column name=s1, symbol="$gs$r$b1$n", units=m, description="sqrt(<x*x>)", type=double,  &end
&column name=s12, symbol="$gs$r$b12$n", units=m, description="<x*xp'>", type=double,  &end
&column name=s13, symbol="$gs$r$b13$n", units="m$a2$n", description="<x*y>", type=double,  &end
&column name=s14, symbol="$gs$r$b14$n", units=m, description="<x*y'>", type=double,  &end
&column name=s15, symbol="$gs$r$b15$n", units="m$a2$n", description="<x*s>", type=double,  &end
&column name=s16, symbol="$gs$r$b16$n", units=m, description="<x*delta>", type=double,  &end
&column name=s17, symbol="$gs$r$b17$n", units="m*s", description="<x*t>", type=double,  &end
&column name=s2, symbol="$gs$r$b2$n", description="sqrt(<x'*x'>)", type=double,  &end
&column name=s23, symbol="$gs$r$b23$n", units=m, description="<x'*y>", type=double,  &end
&column name=s24, symbol="$gs$r$b24$n", description="<x'*y'>", type=double,  &end
&column name=s25, symbol="$gs$r$b25$n", units=m, description="<x'*s>", type=double,  &end
&column name=s26, symbol="$gs$r$b26$n", description="<x'*delta>", type=double,  &end
&column name=s27, symbol="$gs$r$b27$n", units=s, description="<x'*t>", type=double,  &end
&column name=s3, symbol="$gs$r$b3$n", units=m, description="sqrt(<y*y>)", type=double,  &end
&column name=s34, symbol="$gs$r$b34$n", units=m, description="<y*y'>", type=double,  &end
&column name=s35, symbol="$gs$r$b35$n", units="m$a2$n", description="<y*s>", type=double,  &end
&column name=s36, symbol="$gs$r$b36$n", units=m, description="<y*delta>", type=double,  &end
&column name=s37, symbol="$gs$r$b37$n", units="m*s", description="<y*t>", type=double,  &end
&column name=s4, symbol="$gs$r$b4$n", description="sqrt(<y'*y')>", type=double,  &end
&column name=s45, symbol="$gs$r$b45$n", units=m, description="<y'*s>", type=double,  &end
&column name=s46, symbol="$gs$r$b46$n", description="<y'*delta>", type=double,  &end
&column name=s47, symbol="$gs$r$b47$n", units=s, description="<y'*t>", type=double,  &end
&column name=s5, symbol="$gs$r$b5$n", units=m, description="sqrt(<s*s>)", type=double,  &end
&column name=s56, symbol="$gs$r$b56$n", units=m, description="<s*delta>", type=double,  &end
&column name=s57, symbol="$gs$r$b57$n", units="m*s", description="<s*t>", type=double,  &end
&column name=s6, symbol="$gs$r$b6$n", description="sqrt(<delta*delta>)", type=double,  &end
&column name=s67, symbol="$gs$r$b67$n", units=s, description="<delta*t>", type=double,  &end
&column name=s7, symbol="$gs$r$b7$n", description="sqrt(<t*t>)", type=double,  &end
&column name=ma1, symbol="max$sb$ex$sb$e", units=m, description="maximum absolute value of x", type=double,  &end
&column name=ma2, symbol="max$sb$ex'$sb$e", description="maximum absolute value of x'", type=double,  &end
&column name=ma3, symbol="max$sb$ey$sb$e", units=m, description="maximum absolute value of y", type=double,  &end
&column name=ma4, symbol="max$sb$ey'$sb$e", description="maximum absolute value of y'", type=double,  &end
&column name=ma5, symbol="max$sb$e$gD$rs$sb$e", units=m, description="maximum absolute deviation of s", type=double,  &end
&column name=ma6, symbol="max$sb$e$gd$r$sb$e", description="maximum absolute value of delta", type=double,  &end
&column name=ma7, symbol="max$sb$e$gD$rt$sb$e", units=s, description="maximum absolute deviation of t", type=double,  &end
&column name=minimum1, symbol="x$bmin$n", units=m, type=double,  &end
&column name=minimum2, symbol="x'$bmin$n", units=m, type=double,  &end
&column name=minimum3, symbol="y$bmin$n", units=m, type=double,  &end
&column name=minimum4, symbol="y'$bmin$n", units=m, type=double,  &end
&column name=minimum5, symbol="$gD$rs$bmin$n", units=m, type=double,  &end
&column name=minimum6, symbol="$gd$r$bmin$n", units=m, type=double,  &end
&column name=minimum7, symbol="t$bmin$n", units=s, type=double,  &end
&column name=maximum1, symbol="x$bmax$n", units=m, type=double,  &end
&column name=maximum2, symbol="x'$bmax$n", units=m, type=double,  &end
&column name=maximum3, symbol="y$bmax$n", units=m, type=double,  &end
&column name=maximum4, symbol="y'$bmax$n", units=m, type=double,  &end
&column name=maximum5, symbol="$gD$rs$bmax$n", units=m, type=double,  &end
&column name=maximum6, symbol="$gd$r$bmax$n", units=m, type=double,  &end
&column name=maximum7, symbol="t$bmax$n", units=s, type=double,  &end
&column name=Sx, symbol="$gs$r$bx$n", units=m, description=sqrt(<(x-<x>)^2>), type=double,  &end
&column name=Sxp, symbol="$gs$r$bx'$n", description=sqrt(<(x'-<x'>)^2>), type=double,  &end
&column name=Sy, symbol="$gs$r$by$n", units=m, description=sqrt(<(y-<y>)^2>), type=double,  &end
&column name=Syp, symbol="$gs$r$by'$n", description=sqrt(<(y'-<y'>)^2>), type=double,  &end
&column name=Ss, symbol="$gs$r$bs$n", units=m, description=sqrt(<(s-<s>)^2>), type=double,  &end
&column name=Sdelta, symbol="$gs$bd$n$r", description=sqrt(<(delta-<delta>)^2>), type=double,  &end
&column name=St, symbol="$gs$r$bt$n", units=s, description=sqrt(<(t-<t>)^2>), type=double,  &end
&column name=ex, symbol="$ge$r$bx$n", units=m, description="geometric horizontal emittance", type=double,  &end
&column name=enx, symbol="$ge$r$bx,n$n", units=m, description="normalized horizontal emittance", type=double,  &end
&column name=ecx, symbol="$ge$r$bx,c$n", units=m, description="geometric horizontal emittance less dispersive contributions", type=double,  &end
&column name=ecnx, symbol="$ge$r$bx,cn$n", units=m, description="normalized horizontal emittance less dispersive contributions", type=double,  &end
&column name=ey, symbol="$ge$r$by$n", units=m, description="geometric vertical emittance", type=double,  &end
&column name=eny, symbol="$ge$r$by,n$n", units=m, description="normalized vertical emittance", type=double,  &end
&column name=ecy, symbol="$ge$r$by,c$n", units=m, description="geometric vertical emittance less dispersive contributions", type=double,  &end
&column name=ecny, symbol="$ge$r$by,cn$n", units=m, description="normalized vertical emittance less dispersive contributions", type=double,  &end
&column name=betaxBeam, symbol="$gb$r$bx,beam$n", units=m, description="betax for the beam, excluding dispersive contributions", type=double,  &end
&column name=alphaxBeam, symbol="$ga$r$bx,beam$n", description="alphax for the beam, excluding dispersive contributions", type=double,  &end
&column name=betayBeam, symbol="$gb$r$by,beam$n", units=m, description="betay for the beam, excluding dispersive contributions", type=double,  &end
&column name=alphayBeam, symbol="$ga$r$by,beam$n", description="alphay for the beam, excluding dispersive contributions", type=double,  &end
&data mode=binary, &end
&                 _BEG_      MARK���P��>C��%�z�=X0;8����=6:D����_�ȥ=��o�
>��v�}�;)�t;���>dv��]���LT6�~����hI��=�\+:��=�C��l�;&�iVc(�>�}�~|��=�����<!�vN�ۛ���A��o4;����v�>�����~'=c��n%W�=�>��ye;:�O����>.�1B" �=<$���;R>��&cC?=��&<,M��3�<9s����3?�O��;!?�!y��d?w��Ka�?   
�f�>��v��|?   �r\=9s����3��O��;!���ʈ���x��   Լ�ɾ$̖f��v�   �0��6XBӆZ1?�T�\�?�!y��d?w��Ka�?   
�f�>��v��|?   �r\=���P��>)�t;���>&�iVc(�>����v�>:�O����>R>��&cC?,M��3�<_)7�[��=���ۍ>�wL��=P��̍>�օ�T�="������>��(�T�=�����>q!�^d�4@71��� ��i7đ��?P�ߧ�Bο           BNCH      CHARGE���P��>C��%�z�=X0;8����=6:D����_�ȥ=��o�
>��v�}�;)�t;���>dv��]���LT6�~����hI��=�\+:��=�C��l�;&�iVc(�>�}�~|��=�����<!�vN�ۛ���A��o4;����v�>�����~'=c��n%W�=�>��ye;:�O����>.�1B" �=<$���;R>��&cC?=��&<,M��3�<9s����3?�O��;!?�!y��d?w��Ka�?   
�f�>��v��|?   �r\=9s����3��O��;!���ʈ���x��   Լ�ɾ$̖f��v�   �0��6XBӆZ1?�T�\�?�!y��d?w��Ka�?   
�f�>��v��|?   �r\=���P��>)�t;���>&�iVc(�>����v�>:�O����>R>��&cC?,M��3�<_)7�[��=���ۍ>�wL��=P��̍>�օ�T�="������>��(�T�=�����>q!�^d�4@71��� ��i7đ��?P�ߧ�Bο��C\��?	   TMC_DR_V1      DRIF�ؖ,� ?�L0�Nf�=+�&�NŬ�S��L>y����zF��=����,�>_��M���;)�t;���>f*ڑ�$��LT6�~���i4����=�\+:��=�?��l�;�i��%&�>�����=��)o =hS� ��'�@hK;����v�>���?�~'=c��n%W�=�9ҥ�e;�������>P�&E �=ĺp���;R>��&cC?�]���&<4q[V�3�<My$�5?�O��;!?����~�?w��Ka�?   �4g�>��v��|?   
]=My$�5��O��;!�����~���x��   ��ɾ$̖f��v�   �1�����m�2?�T�\�?�݊E+�?w��Ka�?   �4g�>��v��|?   
]=�ؖ,� ?)�t;���>�i��%&�>����v�>�������>R>��&cC?4q[V�3�<)7�[��=����ۍ>LwL��=�P��̍>�օ�T�="������>��(�T�=�����>�д�8@y:�<V�!� 7v2��?�&��@��N3��?   TMC_DR_QD_ED      DRIF�,��� ?j�����={w8�����y�4°�i�@s=�=��"@�(>��Fu���;)�t;���>Co�1�G��LT6�~����c�}��=�\+:��=n�'�l�;J_M�0��>F~J4���=���HF=��W�E�����~��N;����v�>����~'=c��n%W�=?ɢ��e;�6B7���>8`�aL �=`p�o���;R>��&cC?9g���&<�{�3�<��s��5?�O��;!?r,?w��Ka�?   (Vg�>��v��|?   �']=��s��5��O��;!�r,��x��   ػ�ɾ$̖f��v�   �1����+�	3?�T�\�?1��C��?w��Ka�?   (Vg�>��v��|?   �']=�,��� ?)�t;���>J_M�0��>����v�>�6B7���>R>��&cC?�{�3�<)7�[��=����ۍ>MwL��=�P��̍>�օ�T�="������>��(�T�=Î�����>�l�WX�8@)��/"��Y��Y�?�7AtZ�㿛Ü�9�?
   TMC_QD_V01      QUAD�2pw]� ?I��1��=��W��9��C%N$9��aA-b�=���-Z[>p�S���;LHZ�p�>-3w�=��<�Gj޺�������b=(\㸐i�=*"��cӠ;P��j�>��_�f�=���` =��/�j^��@���Q;wik�X
�>�7�Z)=�s��E+o=S���B�f;�����>1eP �=Α�-���;R>��&cC?��!{��&<�QKܚ3�<9I�E�6?��Κ��	?λ�!��?#�T��?   lgg�>��v��|?   �8]=9I�E�6���Κ��	�λ�!�����iY��   d��ɾ$̖f��v�   �1����`ƥ<3?��&K��?�0⌈?#�T��?   lgg�>��v��|?   �8]=�2pw]� ?LHZ�p�>P��j�>wik�X
�>�����>R>��&cC?�QKܚ3�<O�(�Ʊ�=T}��M��>��B�`��=��?)Lэ>~�z��T�=`{���>�f��T�=�T���>���>�L9@K�����,��>�?��#���뿛Ü�9�?   TMC_COR      KICKER�2pw]� ?I��1��=��W��9��C%N$9��aA-b�=���-Z[>p�S���;LHZ�p�>-3w�=��<�Gj޺�������b=(\㸐i�=*"��cӠ;P��j�>��_�f�=���` =��/�j^��@���Q;wik�X
�>�7�Z)=�s��E+o=S���B�f;�����>1eP �=Α�-���;R>��&cC?��!{��&<�QKܚ3�<9I�E�6?��Κ��	?λ�!��?#�T��?   lgg�>��v��|?   �8]=9I�E�6���Κ��	�λ�!�����iY��   d��ɾ$̖f��v�   �1����`ƥ<3?��&K��?�0⌈?#�T��?   lgg�>��v��|?   �8]=�2pw]� ?LHZ�p�>P��j�>wik�X
�>�����>R>��&cC?�QKܚ3�<O�(�Ʊ�=T}��M��>��B�`��=��?)Lэ>~�z��T�=`{���>�f��T�=�T���>���>�L9@K�����,��>�?��#���뿛Ü�9�?   TMC_BPM      MONI�2pw]� ?I��1��=��W��9��C%N$9��aA-b�=���-Z[>p�S���;LHZ�p�>-3w�=��<�Gj޺�������b=(\㸐i�=*"��cӠ;P��j�>��_�f�=���` =��/�j^��@���Q;wik�X
�>�7�Z)=�s��E+o=S���B�f;�����>1eP �=Α�-���;R>��&cC?��!{��&<�QKܚ3�<9I�E�6?��Κ��	?λ�!��?#�T��?   lgg�>��v��|?   �8]=9I�E�6���Κ��	�λ�!�����iY��   d��ɾ$̖f��v�   �1����`ƥ<3?��&K��?�0⌈?#�T��?   lgg�>��v��|?   �8]=�2pw]� ?LHZ�p�>P��j�>wik�X
�>�����>R>��&cC?�QKܚ3�<O�(�Ʊ�=T}��M��>��B�`��=��?)Lэ>~�z��T�=`{���>�f��T�=�T���>���>�L9@K�����,��>�?��#����*��땪�?
   TMC_QD_V01      QUAD�I��� ?kn����˽Z�F8y#��U=�׺��M���U�=����i>���qe��;cޞ+���>*���'k=t�nXd=�>8km%}�
��C)���%j��*��}��JP�>��_�h��=DPB� =1�x_Q���Q��;�R;�㈒V��>��֏�j+=C�V�U�=�`��h;���y���>H�D�O �=�4����;R>��&cC?)�]��&< �c�3�<�I0�6?t�5���>��)H�?7ux7�s?   ig�>��v��|?   �;]=�I0�6��'�5�g���)H��y��TW�   0��ɾ$̖f��v�   1��1����F3?t�5���>�(���n?7ux7�s?   ig�>��v��|?   �;]=�I��� ?cޞ+���>}��JP�>�㈒V��>���y���>R>��&cC? �c�3�<��Sck��=��gk�>�$����=:.S�1֍>�n�U�=����%��>]+֪�T�=;$k!��>�H6��d9@��θ���?l��5j�?f�}"}����b�?   TMC_DR_QD_ED      DRIFT�>�� ?s��}�˽`��-��H~'�쳷�_���-�=iߐ1�d>�j5���;cޞ+���>�k"U��k=t�nXd=�߷�l%}�
��C)�����N�)����A�hf�>�@���=O�#v�s=p15�`���V�M}U;�㈒V��>����j+=C�V�U�C�[��h;Q����>1x*�N �=$D����;R>��&cC?��L��&<��#��3�<�;6?t�5���>+�A�q?7ux7�s?   �ig�>��v��|?   �=]=�;6��'�5�g�+�A�q�y��TW�   d��ɾ$̖f��v�   �/����D>r;3?t�5���>�?#qz?7ux7�s?   �ig�>��v��|?   �=]=T�>�� ?cޞ+���>��A�hf�>�㈒V��>Q����>R>��&cC?��#��3�<��Sck��=��gk�>�$����=:.S�1֍>�n�U�=����%��>Z+֪�T�=9$k!��>R�0J9@����M��?�A�]�?ܿ��_	���F�[��	@	   TMC_DR_V2      DRIF�4I�vp�>���rRrŽ�?��J�ӽ���}]ᵽ��m�x�=).�4>/h�R��;cޞ+���>���6���=t�nXd=:�o�3%}�
��C)���kd*�����p�:���>.r���|�=�o��T�F=`��}i<���ހj;�;�㈒V��>�Y��0m+=C�V�U�TA�3�h;ﶴ���>n�� �=�jy~��;R>��&cC?�΅�J�&<X�v�m3�<����-2?t�5���>Hk����.?7ux7�s?    �g�>��v��|?   ��]=����-2��'�5�g�	�נa*�y��TW�   �>�ɾ$̖f��v�   P�����xK0?t�5���>Hk����.?7ux7�s?    �g�>��v��|?   ��]=�4I�vp�>cޞ+���>�p�:���>�㈒V��>ﶴ���>R>��&cC?X�v�m3�<��Sck��=��gk�>�$����=:.S�1֍>�n�U�=����%��>e+֪�T�=@$k!��>���3@߸U��:�?dX1��;@�F4h��f����1
@   TMC_DR_QD_ED      DRIF�QJ�`�>�����YŽ��5h�ӽ��rڵ�l���;)�=�^���/>�:�X�;cޞ+���>#�%�Å=t�nXd=�"��2%}�
��C)����׷����l�$I��>�������=�����F=E���K���X��>~�;�㈒V��>�7:m+=C�V�U����	<�h;��7+���>W��� �=��>�}��;R>��&cC?/\zE�&<pq�l3�<ۻ��W2?t�5���>"�v�x/?7ux7�s?   |�g�>��v��|?   ��]=ۻ��W2��'�5�g��`��*�y��TW�   ,=�ɾ$̖f��v�   ���7���?0?t�5���>"�v�x/?7ux7�s?   |�g�>��v��|?   ��]=�QJ�`�>cޞ+���>�l�$I��>�㈒V��>��7+���>R>��&cC?pq�l3�<��Sck��=��gk�>�$����=:.S�1֍>�n�U�=����%��>e+֪�T�=@$k!��>��q�W�2@�PH�?��{B%�;@H8w��~����
@
   TMC_QD_V02      QUAD�W�LÌ�>OYW��=�F{;q�ӽ0�><���=l��=�HV�K>��j��i�;S���f�>L�+-x��I�$�a��=6gC<zhd=��&-��='��^�-�;��!ya�>��H�ֽ�m$�F=��O����	?HⓄ;��U�A��>g.��!�jѹ+I��=�9��1�N�kG�S���>?��" �=1l�}��;R>��&cC?��B�&<Vd�l3�<�s�1'62?��Zh�7?�(��*/?|���D�>   �g�>��v��|?   4�]=�s�1'62���Zh�7����[D�*�����5%��   =�ɾ$̖f��v�   D���߻0/4V0?�F~�w�?�(��*/?|���D�>   �g�>��v��|?   4�]=�W�LÌ�>S���f�>��!ya�>��U�A��>kG�S���>R>��&cC?Vd�l3�<Q����=Ĩ�o��>V�4��=oэ>/���g�=�$�a�>Y�/1�g�=����[�>m7K{�63@Ռ˖	9��&��;@4�{&@���
@   TMC_COR      KICKER�W�LÌ�>OYW��=�F{;q�ӽ0�><���=l��=�HV�K>��j��i�;S���f�>L�+-x��I�$�a��=6gC<zhd=��&-��='��^�-�;��!ya�>��H�ֽ�m$�F=��O����	?HⓄ;��U�A��>g.��!�jѹ+I��=�9��1�N�kG�S���>?��" �=1l�}��;R>��&cC?��B�&<Vd�l3�<�s�1'62?��Zh�7?�(��*/?|���D�>   �g�>��v��|?   4�]=�s�1'62���Zh�7����[D�*�����5%��   =�ɾ$̖f��v�   D���߻0/4V0?�F~�w�?�(��*/?|���D�>   �g�>��v��|?   4�]=�W�LÌ�>S���f�>��!ya�>��U�A��>kG�S���>R>��&cC?Vd�l3�<Q����=Ĩ�o��>V�4��=oэ>/���g�=�$�a�>Y�/1�g�=����[�>m7K{�63@Ռ˖	9��&��;@4�{&@���
@   TMC_BPM      MONI�W�LÌ�>OYW��=�F{;q�ӽ0�><���=l��=�HV�K>��j��i�;S���f�>L�+-x��I�$�a��=6gC<zhd=��&-��='��^�-�;��!ya�>��H�ֽ�m$�F=��O����	?HⓄ;��U�A��>g.��!�jѹ+I��=�9��1�N�kG�S���>?��" �=1l�}��;R>��&cC?��B�&<Vd�l3�<�s�1'62?��Zh�7?�(��*/?|���D�>   �g�>��v��|?   4�]=�s�1'62���Zh�7����[D�*�����5%��   =�ɾ$̖f��v�   D���߻0/4V0?�F~�w�?�(��*/?|���D�>   �g�>��v��|?   4�]=�W�LÌ�>S���f�>��!ya�>��U�A��>kG�S���>R>��&cC?Vd�l3�<Q����=Ĩ�o��>V�4��=oэ>/���g�=�$�a�>Y�/1�g�=����[�>m7K{�63@Ռ˖	9��&��;@4�{&@
[9���
@
   TMC_QD_V02      QUAD�f�J�/�>,U�dً>m+��f*Խ���>J��=\�O�]�=E����>p92���;}D�� ��>�>�ӫ�ͽ�*%�-[�=�n!hۈ=���>�SK5�;�%�t��>*o>c��.Mh}<�F=u�e/�w����=q
W�;n�*��>��v�=66��a�vaܕ=�|�VJ�s����.���>BR^� �=B	W3���;R>��&cC?���G�&<^p�sn3�<޿�爖2?T����*??`�/��.?�  ���?   ��g�>��v��|?   �]=޿�爖2�T����*�N���3q*��  ����   �=�ɾ$̖f��v�   D������1��0?�.�H��'??`�/��.?<��}�A?   ��g�>��v��|?   �]=�f�J�/�>}D�� ��>�%�t��>n�*��>���.���>R>��&cC?^p�sn3�<}�L*��=����ۍ>�`�ꦢ�=G.��d̍>R2��z�=̏���>"��z�=�B	5��>�Ȉ�4@-�&�,�n��
;@Q��v�S+@�����,@   TMC_DR_QD_ED      DRIF'M���>sd.��->�k���WԽ��smX�=���"��=Pn���0>�=�d�&�;}D�� ��>�а��	ͽ�*%�-[�=���uۈ=���>9d��V5�;�a2="�>l�8��w����cZr<F=�������o��;n�*��>�@�H66��a�vaܕ=�^T�s�c?I���>�ĕ�" �=J1�����;R>��&cC?�ٱ�R�&<���q3�<i,�u$3?T����*?d����5.?�  ���?   �#h�>��v��|?   |U^=i,�u$3�T����*�������)��  ����    >�ɾ$̖f��v�   ����B���/1?�.�H��'?d����5.?<��}�A?   �#h�>��v��|?   |U^='M���>}D�� ��>�a2="�>n�*��>c?I���>R>��&cC?���q3�<t}�L*��=R���ۍ>2a�ꦢ�=�.��d̍>�1��z�=t����>���z�=xB	5��>�,�O5@GI^�-�0��A��9@ع����*@D 5&�H@	   TMC_DR_V3      DRIF��k�<�?������
>^��u �ԽI� A!>�=�1����=PJ�>f�z�w�;}D�� ��>)B���#ǽ�*%�-[�=�cZ��ۈ=���>F�K��5�;)TQ/z�>�
�/�U������A=��8����`\�p�;n�*��>���[�66��a�vaܕ=VĖ��s�d�K����>S�:�� �=��"\���;R>��&cC?�a��&<��Έ3�<�7���58?T����*?I��B��(?�  ���?   ̚k�>��v��|?   P~a=�7���58�T����*�@+N�N�$��  ����   �?�ɾ$̖f��v�   t���*��i��5?�.�H��'?I��B��(?<��}�A?   ̚k�>��v��|?   P~a=��k�<�?}D�� ��>)TQ/z�>n�*��>d�K����>R>��&cC?��Έ3�<f|�L*��=����ۍ>%`�ꦢ�=[-��d̍>�2��z�=%����>���z�=*C	5��>#�v��>A@>���]�2�Q��a�0@@�pY%@��c��@   TMC_DR_QD_ED      DRIF�1���@?��0k��>��},�ԽP	vD��=oqT�@�=Ky>�������;}D�� ��>"Ԛ��~ƽ�*%�-[�=yv�C ܈=���>�F�>�5�;B,���>,ݡ����WpjBIsA=�5������pj/kB;n�*��>���'�66��a�vaܕ=}`��s���m����>�dr�� �=�tW����;R>��&cC?�]����&<:��g�3�<7�����8?T����*?nL$�'�'?�  ���?   ��k�>��v��|?   ��a=7�����8�T����*���&��#��  ����   �?�ɾ$̖f��v�   ������B�N6?�.�H��'?nL$�'�'?<��}�A?   ��k�>��v��|?   ��a=�1���@?}D�� ��>B,���>n�*��>��m����>R>��&cC?:��g�3�<f|�L*��=����ۍ>%`�ꦢ�=[-��d̍>3��z�=Q����>���z�=VC	5��>H.[
�B@nu<��q3���a�\/@��U��$@m\���@
   TMC_QD_V03      QUAD�t~|��?�e�?T�=���cyԽ�g�0�߾=�u���=\�r��>E�ώ5�;]7`�\I�>y[`�����!\��7��=�� �
e=dۯg�E�=�Q�p���;Z���k�>>>�K���E��2A=�,�[��y�֛'�~;�9[I�p�>�^�Y�&�6Қ
�5�=V�L�d����%���>
z�w� �=dv����;R>��&cC?�����&<��w�3�<��=܄!9?=��R2�?��4��'?��3L�?   ",l�>��v��|?   �b=��=܄!9�=��R2���3&+�#��}?���   �?�ɾ$̖f��v�   8���Ѵ,��6?���"_Y?��4��'?��3L�?   ",l�>��v��|?   �b=�t~|��?]7`�\I�>Z���k�>�9[I�p�>���%���>R>��&cC?��w�3�<�A�p%��=ک�O�>��Ƨ�=�<�"Ӎ>p�w��q�=��9*
�>T����q�=���
�><��s�B@N�{�*k ��&�".@l���@m\���@   TMC_COR      KICKER�t~|��?�e�?T�=���cyԽ�g�0�߾=�u���=\�r��>E�ώ5�;]7`�\I�>y[`�����!\��7��=�� �
e=dۯg�E�=�Q�p���;Z���k�>>>�K���E��2A=�,�[��y�֛'�~;�9[I�p�>�^�Y�&�6Қ
�5�=V�L�d����%���>
z�w� �=dv����;R>��&cC?�����&<��w�3�<��=܄!9?=��R2�?��4��'?��3L�?   ",l�>��v��|?   �b=��=܄!9�=��R2���3&+�#��}?���   �?�ɾ$̖f��v�   8���Ѵ,��6?���"_Y?��4��'?��3L�?   ",l�>��v��|?   �b=�t~|��?]7`�\I�>Z���k�>�9[I�p�>���%���>R>��&cC?��w�3�<�A�p%��=ک�O�>��Ƨ�=�<�"Ӎ>p�w��q�=��9*
�>T����q�=���
�><��s�B@N�{�*k ��&�".@l���@m\���@   TMC_BPM      MONI�t~|��?�e�?T�=���cyԽ�g�0�߾=�u���=\�r��>E�ώ5�;]7`�\I�>y[`�����!\��7��=�� �
e=dۯg�E�=�Q�p���;Z���k�>>>�K���E��2A=�,�[��y�֛'�~;�9[I�p�>�^�Y�&�6Қ
�5�=V�L�d����%���>
z�w� �=dv����;R>��&cC?�����&<��w�3�<��=܄!9?=��R2�?��4��'?��3L�?   ",l�>��v��|?   �b=��=܄!9�=��R2���3&+�#��}?���   �?�ɾ$̖f��v�   8���Ѵ,��6?���"_Y?��4��'?��3L�?   ",l�>��v��|?   �b=�t~|��?]7`�\I�>Z���k�>�9[I�p�>���%���>R>��&cC?��w�3�<�A�p%��=ک�O�>��Ƨ�=�<�"Ӎ>p�w��q�=��9*
�>T����q�=���
�><��s�B@N�{�*k ��&�".@l���@��mwC@
   TMC_QD_V03      QUAD������?��Q%<f�rI���YԽ��B�=���\�v�==rHN�>��H$� �;��nl`�>���D�=�Y��Vb�.���|��0|�ہݽ=i,x�ǹ���X�=�>;IN�����T��@=k��Lѐ�\���o~;������>.Yӆ&�@�uӒ��=ɣfF(^(������>�TX�� �= �6b���;R>��&cC?)����&<��E�3�<�I{��.9?JV��{H?
�Ｍe'?if��>   *0l�>��v��|?   �b=�I{��.9�\x[�=&���^��c#�if���   �?�ɾ$̖f��v�   L�����i۱6?JV��{H?
�Ｍe'?H=S�"�>   *0l�>��v��|?   �b=������?��nl`�>��X�=�>������>�����>R>��&cC?��E�3�<�
�-��=]h�0�>�O����=)�d-Aۍ>ƕLWEi�=����y�>��N~=i�=uQH�t�>��@�}�B@��c��J@���K֬-@�	�E�D�?��S���@   TMC_DR_QD_ED      DRIF�W���?��~PQ�c���w8Խ�#�=�*fG(�=n�ͨ=�>��5�j��;��nl`�>���]�+�=�Y��Vb�/����|��0|�ہݽX�t��ǹ����02�>Z���춽���D&�@=m��V�[���\��xg~;������>��h9&�@�uӒ��=��V�)^(�`�?���>�2�� �=[�p���;R>��&cC?�R��&<[�8�3�<����9?JV��{H?6�3`�f'?if��>   �3l�>��v��|?   �b=����9�\x[�=&�u����X#�if���   P?�ɾ$̖f��v�   p����'9�,�6?JV��{H?6�3`�f'?H=S�"�>   �3l�>��v��|?   �b=�W���?��nl`�>���02�>������>`�?���>R>��&cC?[�8�3�<�
�-��=]h�0�>�O����=)�d-Aۍ>ŕLWEi�=����y�>��N~=i�=uQH�t�>�=�s�B@� �f-@�e�X�-@G�t���?�v��@	   TMC_DR_V4      DRIF�o+��(?��'p����I�ܜӽxȩ��t�=��-2���=j�"Pф>b�9tn��;��nl`�>;�A�_��=�Y��Vb���X��|��0|�ہݽ	z�ǹ���:�O��> ���G��s�Ӈ�@=��i��r�tR|@~;������>/�`�$�@�uӒ��=��+g1^(�"�����> =>� �=�������;R>��&cC?�(���&<`����3�<|h
^��8?JV��{H?4���l'?if��>   ,El�>��v��|?   |$b=|h
^��8�\x[�=&��<�F\#�if���   H>�ɾ$̖f��v�   (����Dݔ%6?JV��{H?4���l'?H=S�"�>   ,El�>��v��|?   |$b=�o+��(?��nl`�>��:�O��>������>"�����>R>��&cC?`����3�<�
�-��=]h�0�>�O����=)�d-Aۍ>ŕLWEi�=����y�>��N~=i�=uQH�t�>���?�A@SОѢ
@b"���,@���q�?�v��@	   TMC_DR_20      DRIF�7��8?̀˽��ė3��$ҽے��[O�=z~���=��vr>�T9wW�;��nl`�>��j���=�Y��Vb� ���|��0|�ہݽ?Z�e�ǹ�����v�>zd�p���2l�{�@=�Ka�إ��n)���};������>94U� �@�uӒ��=�g%D^(�_m����>1�a�� �=P�^���;R>��&cC?\�5L��&<��c�3�<����47?JV��{H?0<9��{'?if��>   zpl�>��v��|?   �]b=����47�\x[�=&��#�d#�if���   �;�ɾ$̖f��v�   ����!�w��5?JV��{H?0<9��{'?H=S�"�>   zpl�>��v��|?   �]b=�7��8?��nl`�>����v�>������>_m����>R>��&cC?��c�3�<�
�-��=]h�0�>�O����=)�d-Aۍ>ƕLWEi�=����y�>��N~=i�=uQH�t�>�X�T�<@@%�X ]H	@��2�+@T�?پ�?�s}��@   TMC_DP_DIP1   	   CSRCSBENDE*d�DI?vEt$�T�!�Ӿн��PLq)�=H NU���=f�<�\>~&��X�;���Ƽ��>�9Cfl�=:#%jb���RP�}�Q��X]���j��sm�����>[�ӄ\��Te��rH=�N�d�k�e��ᘅ;m������>���@&����v���=ޕW��;�|�Z
��>������=�Z`G��;��o�&cC?�&�5�&<t|a��(�<~��i��5?�$�޷~? ���݊'?����>   Vڙ�>�?��|?   :�=~��i��5�<�vW_�������l#������   �x�ɾ�y���v�   ���}�<�,�3?�$�޷~? ���݊'?��4��"�>   Vڙ�>�?��|?   :�=E*d�DI?���Ƽ��>sm�����>m������>|�Z
��>��o�&cC?t|a��(�<�
��$��=�.�4>�>b�8ϭ�=�N��ۍ>���\Ei�=<.�y�>�~��=i�=�Ȑt�>�S6�f=@���'"�@���;*@X���0�?�s}��@   TMC_DR_SIDE_C      CSRDRIFT�,lZ?��_W&��ճ�νs :��=��k��=��ʌ�>�/!Tu��;���Ƽ��>�G���E�=:#%jb��B��P�}��J��}T���SC������1�����>�tW�p��\H�աG=��\�Z=(�(�;m������>�$1�:&��D����=��S�%�;�Ӌg��>hL]2���=�^	�D��;�����aC?Ik�g�&<�q8�(�</����V4?�$�޷~?o#_�ә'?����>   (���>�X���|?   �=/����V4�<�vW_����7-u#������   �p�ɾ8�P���v�   \����I�٩�2?�$�޷~?o#_�ә'?��4��"�>   (���>�X���|?   �=�,lZ?���Ƽ��>�1�����>m������>Ӌg��>�����aC?�q8�(�<�
��$��=,��>�>i-��=���܍>���\Ei�=|�J�y�>B��=i�=��jt�>��?87}:@{��L��@(�V��)@��\Y�?�V��M!@   TMC_DR_SIDE      DRIF]��G��>C��VcԽ�Z�b�����{1!6�=��t��Z��7����e�t�%P�����Ƽ��>�>�^^�=:#%jb�+P�-r�}��J��}T���{}������)htw�>�Ϗ-���~��D�D=�� �=������;m������>6Z�H&��D����=�[��u�;�I�n��>U������=����H��;�����aC?U�|`�&<��&��(�<ȵy�/�)?�$�޷~?�(����'?����>   *���>�X���|?   P)�=$�Zm�'�<�vW_���po^��#������   le�ɾ8�P���v�   ����ȵy�/�)?�$�޷~?�(����'?��4��"�>   *���>�X���|?   P)�=]��G��>���Ƽ��>��)htw�>m������>I�n��>�����aC?��&��(�<�
��$��=*��>�>g-��=���܍>���\Ei�=|�J�y�>B��=i�=��jt�>��s�W5)@s��g3��?�O��[$@N���P�?E��M"@   TMC_DP_DIP2   	   CSRCSBEND�=ڨ;�>@Ո�ˈҽY4�H�L��m���1�=�U�br����6�Q������G»&�!�`�>9���B��=R\�Vb���~�m�{����piݽz G���A τBD�>�MF+���Ui�
0z@=6b|`걣=��8�|};@ojլ��>�G�Wa���֫)d��=�/H���3��,7{�ܹ>SiP"���=K��г�;�)���aC?���[�&<��0\%$�<��WI�'?�o�{H?��Ԭ�(?s���>   �� �>�&��|?   �H=�*_�$��e{�=&��HY�֮#�s����   $pxɾ�[����v�   `�����WI�'?�o�{H?��Ԭ�(?ñ���"�>   �� �>�&��|?   �H=�=ڨ;�>&�!�`�>A τBD�>@ojլ��>�,7{�ܹ>�)���aC?��0\%$�<J]!���=V���X8�>��)���=��~5�܍>##(aEi�=�GB�y�>���=i�=!��mt�>���ٖ�%@���,3�?3���j�#@���$���?E���"@   TMC_DR_CENT_C      CSRDRIFT_:�w�g�>��(���ѽ�L�L��������Ă=�	 �����l�l�E ��]��gŻ&�!�`�>��3��Z�=R\�Vb�2�Z�o�{����piݽ�����6�D�.�>�B��S����<��N@=���V�
�=�_��.};@ojլ��>@3�y`���֫)d��= ;�l��3���=I�ܹ>8P=伷�=m��&ѳ�;�)���aC?&��DE�&<�%$�<��)}�&?�o�{H?]��
(?s���>   Z� �>�&��|?   Be=��$�;$��e{�=&�ғ$���#�s����   �nxɾ�[����v�   0�����)}�&?�o�{H?]��
(?ñ���"�>   Z� �>�&��|?   Be=_:�w�g�>&�!�`�>6�D�.�>@ojլ��>��=I�ܹ>�)���aC?�%$�<J]!���=V���X8�>��)���=��~5�܍>##(aEi�=�GB�y�>���=i�=!��mt�>2!.e7+$@ �����?����"k#@�!k�q�?E���"@   TMC_BPM      MONI_:�w�g�>��(���ѽ�L�L��������Ă=�	 �����l�l�E ��]��gŻ&�!�`�>��3��Z�=R\�Vb�2�Z�o�{����piݽ�����6�D�.�>�B��S����<��N@=���V�
�=�_��.};@ojլ��>@3�y`���֫)d��= ;�l��3���=I�ܹ>8P=伷�=m��&ѳ�;�)���aC?&��DE�&<�%$�<��)}�&?�o�{H?]��
(?s���>   Z� �>�&��|?   Be=��$�;$��e{�=&�ғ$���#�s����   �nxɾ�[����v�   0�����)}�&?�o�{H?]��
(?ñ���"�>   Z� �>�&��|?   Be=_:�w�g�>&�!�`�>6�D�.�>@ojլ��>��=I�ܹ>�)���aC?�%$�<J]!���=V���X8�>��)���=��~5�܍>##(aEi�=�GB�y�>���=i�=!��mt�>2!.e7+$@ �����?����"k#@�!k�q�?E���#@   TMC_DR_CENT      DRIF�1L����>Vd� �LϽ�(&�u2��NGn�y�=��V�]n�v��|ޫ~�˻&�!�`�>t�)��=R\�Vb��#�?s�{����piݽ�?g�����M��>��[�f���40�&��?=G2DC���=��7L�|;@ojլ��>��9�^���֫)d��=Q�)蝍3�H_�ܹ>��1h·�=��ѳ�;�)���aC?��m��&<O��}$$�< ���q$?�o�{H?����(?s���>   �
!�>�&��|?   ��=�lP�j�"��e{�=&��)�]7�#�s����   (lxɾ�[����v�   ��� ���q$?�o�{H?����(?ñ���"�>   �
!�>�&��|?   ��=�1L����>&�!�`�>��M��>@ojլ��>H_�ܹ>�)���aC?O��}$$�<H]!���=T���X8�>��)���=��~5�܍>##(aEi�=�GB�y�>���=i�=!��mt�>+��l;!@o8h��#�?�}fs�#@��|�\K�?�!��$@   TMC_DP_DIP3   	   CSRCSBEND=����=�>����K�˽�غ�ǝ���ӝ�`|=EO�����������S��z nϻ~uG�`w�>�'��܈=Ā�wxCb�Q����y��cQ�?�=`M�~;���!'�6��>�� WԀ���2\9=�I���n�=���~X�v;[�y����>��o������ű���=���f�*������ӹ>�2�z���=�Ctj���;�d��aC?Z��g�&<�ҹ���<��U�t"?�mULd?m�h�'(?M=^I
��>   Tڊ�>��ʞ�|?   �o~=ǚ�	�t!��mULd�����w�#�M=^I
���   ��bɾ�)�"��v�   D0����U�t"?S#�Z@?m�h�'(?T�]��"�>   Tڊ�>��ʞ�|?   �o~==����=�>~uG�`w�>�!'�6��>[�y����>�����ӹ>�d��aC?�ҹ���<鞏2��=��"��>��"����=<���܍>S��dEi�=M���y�>~�F�=i�=��Fpt�>p��1tD@(^.\�n�?=�b��"@,go����?�!��%@   TMC_DR_SIDE_C      CSRDRIFTy?����>R�*�ǽ>��~Q��޳���w=���H1ϔ���k�����*dDe�һ~uG�`w�>k(�<��=Ā�wxCb���#X��y��cQ�?�=>��L	;��kZ�k��>�3�: S_�-VV��8=��8� �=��&�Bv;[�y����>K�MR�����ű���="Y��'�*�P,U�ӹ>�j;����=:[-���;�d��aC?��*<�&<kQn��<��V� ?�mULd?~���6(?M=^I
��>   x��>��ʞ�|?   ��~=	[�{���mULd�p��q��#�M=^I
���   8�bɾ�)�"��v�   T1����V� ?S#�Z@?~���6(?T�]��"�>   x��>��ʞ�|?   ��~=y?����>~uG�`w�>kZ�k��>[�y����>P,U�ӹ>�d��aC?kQn��<鞏2��=��"��>��"����=<���܍>S��dEi�=M���y�>~�F�=i�=��Fpt�>C�ǩP�@;#��$��?Z|�� �"@{���{�?���0�+@   TMC_DR_SIDE      DRIF�<�|m.�>|4C�~Lt��%�*���N�@���L�]��XL���/���=��.JZ�~uG�`w�>b��C�q=Ā�wxCb�<�����y��cQ�?�=�ֵ&;��xͿ{���>=�[d�=o6=��h����=��ٖ��s;[�y����>z�H�d����ű���=:_:dp�*��f-^�ӹ>y�g����=�D�����;�d��aC?v�P:�&<v�M��<�ZS���?�mULd?C�����(?M=^I
��>   j��>��ʞ�|?   \X�=��;9���mULd�����4�#�M=^I
���   ��bɾ�)�"��v�   8���ZS���?S#�Z@?C�����(?T�]��"�>   j��>��ʞ�|?   \X�=�<�|m.�>~uG�`w�>xͿ{���>[�y����>�f-^�ӹ>�d��aC?v�M��<螏2��=��"��>��"����=;���܍>S��dEi�=M���y�>~�F�=i�=��Fpt�>k�蘫@v!��cM�?a`��ݏ$@���EZӿ�#�2�,@   TMC_DP_DIP4   	   CSRCSBENDCs���d�>�� �(ܘ=6����t���~C�
Q`��A'�>�� �C6�1�=R�7R��Ӿ��`�>�6��z�i=�[�O�Vb��,��|�"��vpiݽЅ���ǹ�I�*v��>�������=� L��9=7�C���=v+؅iv;�)v����>��sT(뼨Hߝ펅=�{�~(����O��>%�>@/��=�f�^��;JL���aC?.I3o.�&<=�f�0�<�p�M(?���{H?A%�B��(?�[�X���>   �a��>��3�|?   b~=�=�˹�*5�=&�[�@s$��[�X����   ��ɾE�H��v�   <f ��p�M(?���{H?A%�B��(?�i���"�>   �a��>��3�|?   b~=Cs���d�>Ӿ��`�>I�*v��>�)v����>���O��>JL���aC?=�f�0�<�W�I��=��9��>J�����=C7�܍>w1%fEi�=Oz��y�>����=i�=[
qt�>c�u�@�,Z�������BD8%@�Jܥ��ֿ�#�2�-@   TMC_DR_20_C      CSRDRIFT�ʔb5=�>�%n-DЫ=�NBM�r����?/p|i��6�O0ާ�X�M��=Oɉ�_�Ӿ��`�>��6�`=�[�O�Vb���S��|�"��vpiݽ>�ǹ�L�*��H�>�F�{٨=BF~�.�8=�sb�W�=�Q)�v;�)v����>R���P(뼨Hߝ펅=܃�=�~(�Ͼn���>%��4��=Ћ��_��;JL���aC?.<+��&<��p�0�<Az5r��?���{H? �َ|�(?�[�X���>   ���>��3�|?   ķ=v�Ժ�	�*5�=&�F�Tα$��[�X����   �ɾE�H��v�   �g �Az5r��?���{H? �َ|�(?�i���"�>   ���>��3�|?   ķ=�ʔb5=�>Ӿ��`�>L�*��H�>�)v����>Ͼn���>JL���aC?��p�0�<�W�I��=��9��>J�����=C7�܍>v1%fEi�=Nz��y�>����=i�=Z
qt�>P7˫�	@x8P8��ӿ٫L��%@���U%ڿ�#�2�-@	   TMC_WA_OU      WATCH�ʔb5=�>�%n-DЫ=�NBM�r����?/p|i��6�O0ާ�X�M��=Oɉ�_�Ӿ��`�>��6�`=�[�O�Vb���S��|�"��vpiݽ>�ǹ�L�*��H�>�F�{٨=BF~�.�8=�sb�W�=�Q)�v;�)v����>R���P(뼨Hߝ펅=܃�=�~(�Ͼn���>%��4��=Ћ��_��;JL���aC?.<+��&<��p�0�<Az5r��?���{H? �َ|�(?�[�X���>   ���>��3�|?   ķ=v�Ժ�	�*5�=&�F�Tα$��[�X����   �ɾE�H��v�   �g �Az5r��?���{H? �َ|�(?�i���"�>   ���>��3�|?   ķ=�ʔb5=�>Ӿ��`�>L�*��H�>�)v����>Ͼn���>JL���aC?��p�0�<�W�I��=��9��>J�����=C7�܍>v1%fEi�=Nz��y�>����=i�=Z
qt�>P7˫�	@x8P8��ӿ٫L��%@���U%ڿ