SDDS1
!# little-endian
&description text="sigma matrix--input: xls_linac_tmc_hxr_bc1.track.ele  lattice: xls_linac_tmc_hxr.2.lte", contents="sigma matrix", &end
&parameter name=Step, description="Simulation step", type=long, &end
&parameter name=SVNVersion, description="SVN version number", type=string, fixed_value=unknown, &end
&column name=s, units=m, description=Distance, type=double,  &end
&column name=ElementName, description="Element name", format_string=%10s, type=string,  &end
&column name=ElementOccurence, description="Occurence of element", format_string=%6ld, type=long,  &end
&column name=ElementType, description="Element-type name", format_string=%10s, type=string,  &end
&column name=s1, symbol="$gs$r$b1$n", units=m, description="sqrt(<x*x>)", type=double,  &end
&column name=s12, symbol="$gs$r$b12$n", units=m, description="<x*xp'>", type=double,  &end
&column name=s13, symbol="$gs$r$b13$n", units="m$a2$n", description="<x*y>", type=double,  &end
&column name=s14, symbol="$gs$r$b14$n", units=m, description="<x*y'>", type=double,  &end
&column name=s15, symbol="$gs$r$b15$n", units="m$a2$n", description="<x*s>", type=double,  &end
&column name=s16, symbol="$gs$r$b16$n", units=m, description="<x*delta>", type=double,  &end
&column name=s17, symbol="$gs$r$b17$n", units="m*s", description="<x*t>", type=double,  &end
&column name=s2, symbol="$gs$r$b2$n", description="sqrt(<x'*x'>)", type=double,  &end
&column name=s23, symbol="$gs$r$b23$n", units=m, description="<x'*y>", type=double,  &end
&column name=s24, symbol="$gs$r$b24$n", description="<x'*y'>", type=double,  &end
&column name=s25, symbol="$gs$r$b25$n", units=m, description="<x'*s>", type=double,  &end
&column name=s26, symbol="$gs$r$b26$n", description="<x'*delta>", type=double,  &end
&column name=s27, symbol="$gs$r$b27$n", units=s, description="<x'*t>", type=double,  &end
&column name=s3, symbol="$gs$r$b3$n", units=m, description="sqrt(<y*y>)", type=double,  &end
&column name=s34, symbol="$gs$r$b34$n", units=m, description="<y*y'>", type=double,  &end
&column name=s35, symbol="$gs$r$b35$n", units="m$a2$n", description="<y*s>", type=double,  &end
&column name=s36, symbol="$gs$r$b36$n", units=m, description="<y*delta>", type=double,  &end
&column name=s37, symbol="$gs$r$b37$n", units="m*s", description="<y*t>", type=double,  &end
&column name=s4, symbol="$gs$r$b4$n", description="sqrt(<y'*y')>", type=double,  &end
&column name=s45, symbol="$gs$r$b45$n", units=m, description="<y'*s>", type=double,  &end
&column name=s46, symbol="$gs$r$b46$n", description="<y'*delta>", type=double,  &end
&column name=s47, symbol="$gs$r$b47$n", units=s, description="<y'*t>", type=double,  &end
&column name=s5, symbol="$gs$r$b5$n", units=m, description="sqrt(<s*s>)", type=double,  &end
&column name=s56, symbol="$gs$r$b56$n", units=m, description="<s*delta>", type=double,  &end
&column name=s57, symbol="$gs$r$b57$n", units="m*s", description="<s*t>", type=double,  &end
&column name=s6, symbol="$gs$r$b6$n", description="sqrt(<delta*delta>)", type=double,  &end
&column name=s67, symbol="$gs$r$b67$n", units=s, description="<delta*t>", type=double,  &end
&column name=s7, symbol="$gs$r$b7$n", description="sqrt(<t*t>)", type=double,  &end
&column name=ma1, symbol="max$sb$ex$sb$e", units=m, description="maximum absolute value of x", type=double,  &end
&column name=ma2, symbol="max$sb$ex'$sb$e", description="maximum absolute value of x'", type=double,  &end
&column name=ma3, symbol="max$sb$ey$sb$e", units=m, description="maximum absolute value of y", type=double,  &end
&column name=ma4, symbol="max$sb$ey'$sb$e", description="maximum absolute value of y'", type=double,  &end
&column name=ma5, symbol="max$sb$e$gD$rs$sb$e", units=m, description="maximum absolute deviation of s", type=double,  &end
&column name=ma6, symbol="max$sb$e$gd$r$sb$e", description="maximum absolute value of delta", type=double,  &end
&column name=ma7, symbol="max$sb$e$gD$rt$sb$e", units=s, description="maximum absolute deviation of t", type=double,  &end
&column name=minimum1, symbol="x$bmin$n", units=m, type=double,  &end
&column name=minimum2, symbol="x'$bmin$n", units=m, type=double,  &end
&column name=minimum3, symbol="y$bmin$n", units=m, type=double,  &end
&column name=minimum4, symbol="y'$bmin$n", units=m, type=double,  &end
&column name=minimum5, symbol="$gD$rs$bmin$n", units=m, type=double,  &end
&column name=minimum6, symbol="$gd$r$bmin$n", units=m, type=double,  &end
&column name=minimum7, symbol="t$bmin$n", units=s, type=double,  &end
&column name=maximum1, symbol="x$bmax$n", units=m, type=double,  &end
&column name=maximum2, symbol="x'$bmax$n", units=m, type=double,  &end
&column name=maximum3, symbol="y$bmax$n", units=m, type=double,  &end
&column name=maximum4, symbol="y'$bmax$n", units=m, type=double,  &end
&column name=maximum5, symbol="$gD$rs$bmax$n", units=m, type=double,  &end
&column name=maximum6, symbol="$gd$r$bmax$n", units=m, type=double,  &end
&column name=maximum7, symbol="t$bmax$n", units=s, type=double,  &end
&column name=Sx, symbol="$gs$r$bx$n", units=m, description=sqrt(<(x-<x>)^2>), type=double,  &end
&column name=Sxp, symbol="$gs$r$bx'$n", description=sqrt(<(x'-<x'>)^2>), type=double,  &end
&column name=Sy, symbol="$gs$r$by$n", units=m, description=sqrt(<(y-<y>)^2>), type=double,  &end
&column name=Syp, symbol="$gs$r$by'$n", description=sqrt(<(y'-<y'>)^2>), type=double,  &end
&column name=Ss, symbol="$gs$r$bs$n", units=m, description=sqrt(<(s-<s>)^2>), type=double,  &end
&column name=Sdelta, symbol="$gs$bd$n$r", description=sqrt(<(delta-<delta>)^2>), type=double,  &end
&column name=St, symbol="$gs$r$bt$n", units=s, description=sqrt(<(t-<t>)^2>), type=double,  &end
&column name=ex, symbol="$ge$r$bx$n", units=m, description="geometric horizontal emittance", type=double,  &end
&column name=enx, symbol="$ge$r$bx,n$n", units=m, description="normalized horizontal emittance", type=double,  &end
&column name=ecx, symbol="$ge$r$bx,c$n", units=m, description="geometric horizontal emittance less dispersive contributions", type=double,  &end
&column name=ecnx, symbol="$ge$r$bx,cn$n", units=m, description="normalized horizontal emittance less dispersive contributions", type=double,  &end
&column name=ey, symbol="$ge$r$by$n", units=m, description="geometric vertical emittance", type=double,  &end
&column name=eny, symbol="$ge$r$by,n$n", units=m, description="normalized vertical emittance", type=double,  &end
&column name=ecy, symbol="$ge$r$by,c$n", units=m, description="geometric vertical emittance less dispersive contributions", type=double,  &end
&column name=ecny, symbol="$ge$r$by,cn$n", units=m, description="normalized vertical emittance less dispersive contributions", type=double,  &end
&column name=betaxBeam, symbol="$gb$r$bx,beam$n", units=m, description="betax for the beam, excluding dispersive contributions", type=double,  &end
&column name=alphaxBeam, symbol="$ga$r$bx,beam$n", description="alphax for the beam, excluding dispersive contributions", type=double,  &end
&column name=betayBeam, symbol="$gb$r$by,beam$n", units=m, description="betay for the beam, excluding dispersive contributions", type=double,  &end
&column name=alphayBeam, symbol="$ga$r$by,beam$n", description="alphay for the beam, excluding dispersive contributions", type=double,  &end
&data mode=binary, &end
.                 _BEG_      MARK��Ln� ?�C;����=�G�l�/ٽq�2|d�=/�#�K>D�мh�U>��B�oE<~K6[�a�>���Wj���!ȵ([������2L�=�����OB>LA#�2<j��Г ?�������ܢ˘������Z���i�qD���y��!�>�u�DZ�=�"&Zz�=hl괁�;[]<l��4?�����?�>�ܗ�4ط<Ĉ~�Ă?���&�=����tr=>��U�a6?�eX��?��E�[M(?l��k9�?  �9dyF?�B'�? ��kZ�=�jM�}5��eX�����E�[M(���2�sT� ��F�D��k���� ��O2���>��U�a6?n�� |�?t���*-(?l��k9�?  �9dyF?�B'�? ��kZ�=��Ln� ?~K6[�a�>j��Г ?�y��!�>[]<l��4?Ĉ~�Ă?����tr=c�OdU�=�<�3���>�&����=�㝑���>���"�=bG�뽂>q��"�=hg��佂>K�J�

@��a��
��E?~M��@�K��V�?           BNCH      CHARGE��Ln� ?�C;����=�G�l�/ٽq�2|d�=/�#�K>D�мh�U>��B�oE<~K6[�a�>���Wj���!ȵ([������2L�=�����OB>LA#�2<j��Г ?�������ܢ˘������Z���i�qD���y��!�>�u�DZ�=�"&Zz�=hl괁�;[]<l��4?�����?�>�ܗ�4ط<Ĉ~�Ă?���&�=����tr=>��U�a6?�eX��?��E�[M(?l��k9�?  �9dyF?�B'�? ��kZ�=�jM�}5��eX�����E�[M(���2�sT� ��F�D��k���� ��O2���>��U�a6?n�� |�?t���*-(?l��k9�?  �9dyF?�B'�? ��kZ�=��Ln� ?~K6[�a�>j��Г ?�y��!�>[]<l��4?Ĉ~�Ă?����tr=c�OdU�=�<�3���>�&����=�㝑���>���"�=bG�뽂>q��"�=hg��佂>K�J�

@��a��
��E?~M��@�K��V�?        
   BC1_IN_CNT      CENTER��Ln� ?�C;����=�G�l�/ٽq�2|d�=/�#�K>D�мh�U>��B�oE<~K6[�a�>���Wj���!ȵ([������2L�=�����OB>LA#�2<j��Г ?�������ܢ˘������Z���i�qD���y��!�>�u�DZ�=�"&Zz�=il괁�;[]<l��4?�����?�>�ܗ�4ط<Ĉ~�Ă?���&�=����tr=�A�|�b6?+m����?�X�^M(?��$� �?  �9dyF?�B'�? ��kZ�=I��ܺ|5�+m������X�^M(���+�T� ��F�D��k���� ��O2����A�|�b6?�JBQ�?u�~w'-(?��$� �?  �9dyF?�B'�? ��kZ�=��Ln� ?~K6[�a�>j��Г ?�y��!�>[]<l��4?Ĉ~�Ă?����tr=c�OdU�=�<�3���>�&����=�㝑���>���"�=bG�뽂>q��"�=hg��佂>K�J�

@��a��
��E?~M��@�K��V�?V(�2�8�?	   BC1_DR_V1      DRIFܫ
�?ļa�� >n-}���ٽOE�lb�=m�Ǜz>Z/��Z>F���OJ<~K6[�a�>��a�y���!ȵ([��p���2L�=�����OB>Q��j�2<�r�XP��>6�n����՗a��40I���Z�z���y��!�>��P�HZ�=�"&Zz�=V=ƞ!�;x�_��4?D趫�?�>?�u^�׷<Ĉ~�Ă?UC���=���sr=/";k�8?+m����?�ʴ�l$?��$� �?  \ dyF?�B'�?  ���= ��Φ8�+m������ʴ�l$���+�T� ��Y�D��k����  1R흂�/";k�8?�JBQ�?C���"?��$� �?  \ dyF?�B'�?  ���=ܫ
�?~K6[�a�>�r�XP��>�y��!�>x�_��4?Ĉ~�Ă?���sr=f�OdU�=�<�3���>�&����=�㝑���>���"�=bG�뽂>q��"�=hg��佂>���Q@�+��y����K��_@�RK���?��C(&��?   BC1_DR_QD_ED      DRIF��2��Z?���2_ >X�/C\�ٽ��̝%�=����>���~�Z>��y��J<~K6[�a�>R��H���!ȵ([���[��2L�=�����OB>�S�%�2<*����>��	��>�y���ݑi�`�d��ef�y��!�>"�HZ�=�"&Zz�='�]�;�V�^��4?:���?�>aҁt�׷<Ĉ~�Ă?g~�u��=̞ͼ�sr=b�7)��8?+m����?���O$?��$� �?  \dyF?�B'�? �^�=m����8�+m��������O$���+�T� �&[�D��k����  ��睂�b�7)��8?�JBQ�?*;=OnN"?��$� �?  \dyF?�B'�? �^�=��2��Z?~K6[�a�>*����>�y��!�>�V�^��4?Ĉ~�Ă?̞ͼ�sr=e�OdU�=�<�3���>�&����=�㝑���>���"�=bG�뽂>q��"�=hg��佂>WA�g�@~m�{�����ܛ���@#�z=�?���<���?
   BC1_QD_V01      QUAD�r��?��u�&�=�&�	�ٽ�2�)t��=��{�!E>�.�חJ[>���q:K<b���!�>�rM�T��@#�q����P�v�W�=��x/�G<>Xř�-�+<�4�����>����ZO�́{��)���u�p������L���u����>o@O��=�,|b��=�K�2��;�g�]��4?秩�?�>�[���׷<Ĉ~�Ă?
Pʎ��=7���sr=�N˩9?Vu�j�2?$]�H@�#?���'S ? ��dyF?�B'�? ����=&H��9��R$��$]�H@�#�z�/�_�� ��\�D��k���� ��❂��N˩9?Vu�j�2?�2���"?���'S ? ��dyF?�B'�? ����=�r��?b���!�>�4�����>�u����>�g�]��4?Ĉ~�Ă?7���sr=g�Z��=VG�bR��>�ֵh0��=�+pX5��>M=�!�=��ŷ@��>��0�!�=�)Nw:��>�O3��n@����@����R^�P@��ot��?���<���?   BC1_COR      KICKER�r��?��u�&�=�&�	�ٽ�2�)t��=��{�!E>�.�חJ[>���q:K<b���!�>�rM�T��@#�q����P�v�W�=��x/�G<>Xř�-�+<�4�����>����ZO�́{��)���u�p������L���u����>o@O��=�,|b��=�K�2��;�g�]��4?秩�?�>�[���׷<Ĉ~�Ă?
Pʎ��=7���sr=�N˩9?Vu�j�2?$]�H@�#?���'S ? ��dyF?�B'�? ����=&H��9��R$��$]�H@�#�z�/�_�� ��\�D��k���� ��❂��N˩9?Vu�j�2?�2���"?���'S ? ��dyF?�B'�? ����=�r��?b���!�>�4�����>�u����>�g�]��4?Ĉ~�Ă?7���sr=g�Z��=VG�bR��>�ֵh0��=�+pX5��>M=�!�=��ŷ@��>��0�!�=�)Nw:��>�O3��n@����@����R^�P@��ot��?���<���?   BC1_BPM      MONI�r��?��u�&�=�&�	�ٽ�2�)t��=��{�!E>�.�חJ[>���q:K<b���!�>�rM�T��@#�q����P�v�W�=��x/�G<>Xř�-�+<�4�����>����ZO�́{��)���u�p������L���u����>o@O��=�,|b��=�K�2��;�g�]��4?秩�?�>�[���׷<Ĉ~�Ă?
Pʎ��=7���sr=�N˩9?Vu�j�2?$]�H@�#?���'S ? ��dyF?�B'�? ����=&H��9��R$��$]�H@�#�z�/�_�� ��\�D��k���� ��❂��N˩9?Vu�j�2?�2���"?���'S ? ��dyF?�B'�? ����=�r��?b���!�>�4�����>�u����>�g�]��4?Ĉ~�Ă?7���sr=g�Z��=VG�bR��>�ֵh0��=�+pX5��>M=�!�=��ŷ@��>��0�!�=�)Nw:��>�O3��n@����@����R^�P@��ot��?Bz9Q�$�?
   BC1_QD_V01      QUADp����?�7!�/@�=�׺�tڽvf��r�W�L:f�j�>a���7�[>�͚C%AK<cP.�@�>D��}s����������B�"��=3~���3>����#<�L똆�>W�k�1ཛ�����oZ�eTe�ĈV[1o��(���>����nʧ=�&"����=��Z��;�;�O�\��4?u���?�>^۟q�׷<Ĉ~�Ă?N)����=)e��sr=�-d�@79?˩�ށ�?�^_��#?�@��Z� ? �5dyF?�B'�?  ) �=�-d�@79�כ#E���^_��#��XD��\� �\^�D��k����  ��ݝ��\g�~T-9?˩�ށ�?��D�!?�@��Z� ? �5dyF?�B'�?  ) �=p����?cP.�@�>�L똆�>�(���>�O�\��4?Ĉ~�Ă?)e��sr=����	�=���"��>&l�b���=,'�W��>���j!�=K\�ĭ��>$��d!�=�̇���>��vg{�@�LY�������@-�L�b��?�	�F���?   BC1_DR_QD_ED      DRIF,J���?7w3/�=��0�Dڽ�����vh��.�<�>�<�6�[>��#)�vK<cP.�@�>ڻ��=���������X� ��=3~���3>��O��#<J#�ōP�>����{�޽2�/*&���%�����:f������(���>Moʧ=�&"����=k��;�;��W\��4?p�%��?�>�]y��׷<Ĉ~�Ă?���b��=/y�S�sr=ƀ�%�L9?˩�ށ�?�)���{#?�@��Z� ?  �dyF?�B'�?  5[��=ƀ�%�L9�כ#E���)���{#��XD��\�  <`�D��k����  T,؝���J�FO39?˩�ށ�?��Pj�!?�@��Z� ?  �dyF?�B'�?  5[��=,J���?cP.�@�>J#�ōP�>�(���>��W\��4?Ĉ~�Ă?/y�S�sr=����	�=���"��>'l�b���=-'�W��>���j!�=L\�ĭ��>&��d!�=�̇���>6%�y @�E������aI8�@��_��!�?Ʊ�4j@	   BC1_DR_V2      DRIF�	��?)5���	>.��j���,%�Q�˽���w�>^M,IK�l>#�r��0\<cP.�@�>ց/x н��������%���=3~���3>,��Oם#<�����?�|v��>6_�g�=	feذ�>���Q<�(���>}4G�ʧ=�&"����=㏪�8�;��o��4?B��A�?�>P��Է<Ĉ~�Ă?���7�=�G#�pr=��9�C?˩�ށ�?\�AD�*J?�@��Z� ?  �ScyF?�B'�?  ����=���#u�C�כ#E��HH��%�F��XD��\�  gd�D��k����  }䚂���9�C?˩�ށ�?\�AD�*J?�@��Z� ?  �ScyF?�B'�?  ����=�	��?cP.�@�>�����?�(���>��o��4?Ĉ~�Ă?�G#�pr=����	�=���"��>$l�b���=*'�W��>���j!�=H\�ĭ��>!��d!�=��̇���>?��	�<@�p��������ꜘ/@�~s�����S��@   BC1_DR_QD_ED      DRIFV��H;�?�4��/�	>�/��D����m{�̽��c��>��Y�J�l>���n�K\<cP.�@�>v̥��н��������|���=3~���3>ؾ0�ѝ#<4
��b?'d3�C�>�ȹ<�E�=T G�+>[�>鹊<�(���>�M��ʧ=�&"����=z�6�8�;v�� ��4?��@�?�>1�ӷ<Ĉ~�Ă?+X�1�=Z ���pr=�e���C?˩�ށ�? ����WJ?�@��Z� ?  �RcyF?�B'�?  ����=�e���C�כ#E��+����F��XD��\�  Gf�D��k����  *�ޚ�����'��C?˩�ށ�? ����WJ?�@��Z� ?  �RcyF?�B'�?  ����=V��H;�?cP.�@�>4
��b?�(���>v�� ��4?Ĉ~�Ă?Z ���pr=����	�=���"��>(l�b���=.'�W��>���j!�=K\�ĭ��>$��d!�=�̇���>x��l�<@���������0@�%��c���_0��@
   BC1_QD_V02      QUADy�}�
?9�� ��.>#��J	��i�p7��=�N'@* >dm���l>C�sGu�\<+���??4�K�f���t-Z�=���ݫ>#C��Y>|?��ӒI<��e��
?o�l�}��o� ��S�="�88>nzfX�<J�0/��>n���hv����pE�罞9�(�ֻ�Ei ��4? ��@�?�>6���ӷ<Ĉ~�Ă?���+�=����pr=�/?<K�C?¤̼�U1?���P�RJ?��w�$?  �OcyF?�B'�?  K��=�/?<K�C�¤̼�U1���C�F���w�$�  �e�D��k����  �oٚ����CSF�C?d�a)1?���P�RJ?�� J='$?  �OcyF?�B'�?  K��=y�}�
?+���??��e��
?J�0/��>�Ei ��4?Ĉ~�Ă?����pr=2*���=(7���r�>�������=�)�sg�>W�}�5�= d,�f��>�(�T/�=
�Q�_��>�HL�=@S`ft]+�	���p0@Z�F�z�@�_0��@   BC1_COR      KICKERy�}�
?9�� ��.>#��J	��i�p7��=�N'@* >dm���l>C�sGu�\<+���??4�K�f���t-Z�=���ݫ>#C��Y>|?��ӒI<��e��
?o�l�}��o� ��S�="�88>nzfX�<J�0/��>n���hv����pE�罞9�(�ֻ�Ei ��4? ��@�?�>6���ӷ<Ĉ~�Ă?���+�=����pr=�/?<K�C?¤̼�U1?���P�RJ?��w�$?  �OcyF?�B'�?  K��=�/?<K�C�¤̼�U1���C�F���w�$�  �e�D��k����  �oٚ����CSF�C?d�a)1?���P�RJ?�� J='$?  �OcyF?�B'�?  K��=y�}�
?+���??��e��
?J�0/��>�Ei ��4?Ĉ~�Ă?����pr=2*���=(7���r�>�������=�)�sg�>W�}�5�= d,�f��>�(�T/�=
�Q�_��>�HL�=@S`ft]+�	���p0@Z�F�z�@�_0��@   BC1_BPM      MONIy�}�
?9�� ��.>#��J	��i�p7��=�N'@* >dm���l>C�sGu�\<+���??4�K�f���t-Z�=���ݫ>#C��Y>|?��ӒI<��e��
?o�l�}��o� ��S�="�88>nzfX�<J�0/��>n���hv����pE�罞9�(�ֻ�Ei ��4? ��@�?�>6���ӷ<Ĉ~�Ă?���+�=����pr=�/?<K�C?¤̼�U1?���P�RJ?��w�$?  �OcyF?�B'�?  K��=�/?<K�C�¤̼�U1���C�F���w�$�  �e�D��k����  �oٚ����CSF�C?d�a)1?���P�RJ?�� J='$?  �OcyF?�B'�?  K��=y�}�
?+���??��e��
?J�0/��>�Ei ��4?Ĉ~�Ă?����pr=2*���=(7���r�>�������=�)�sg�>W�}�5�= d,�f��>�(�T/�=
�Q�_��>�HL�=@S`ft]+�	���p0@Z�F�z�@���x�@
   BC1_QD_V02      QUAD��Cï?3Z<<>Q��,�R��0��l�=�i3,�o >$ �ϧm>�LUR]<�ME�?�lH�%�����5�=k)�!>�}6��g>t�NW<G��n�?/��j�#������=M!#��>���ƮP<=]��U�?͋]3B���	�RR����-����c�9���4?9x�8�?�>�|� �ӷ<Ĉ~�Ă?��a�%�=����pr=�����|D?Q�'���??qV/��I?���u��<?  �@cyF?�B'�?  U���=�����|D�Q�'���?�<�&S�]F����u��<�  >c�D��k����  �KԚ��[��0�zD?�к͠�??qV/��I?HJ�tž:?  �@cyF?�B'�?  U���=��Cï?�ME�?G��n�?=]��U�?c�9���4?Ĉ~�Ă?����pr=���F��=3Eo*��>)ҶU���=2|�Z��>�T��*�=�^�Ƃ>���l�*�=׮7{~Ƃ>�Kxs�/?@�W�n9�r��a�/@�s:�m!@�x��@   BC1_DR_QD_ED      DRIF��H?�u�D4=>E>�st���ҙ#���=������ >��.�K�n>F�$��O^<�ME�?��*�v�����5�=�'�V�!>�}6��g>�E��NW<R<�b?�O�O#�(ۃN�=͈��-t>�LZ?��<=]��U�?�j��F���	�RR����d�����飬4?G��&�?�>���ӷ<Ĉ~�Ă?� �u�=zJyN�pr=�1S)E?Q�'���?? ���KPI?���u��<?  �$cyF?�B'�?  ���=�1S)E�Q�'���?����c�E����u��<�  �_�D��k����  ��Κ��=\��l&E?�к͠�?? ���KPI?HJ�tž:?  �$cyF?�B'�?  ���=��H?�ME�?R<�b?=]��U�?�飬4?Ĉ~�Ă?zJyN�pr=����F��=�Ao*��>�ζU���=�|�Z��>�T��*�=�^�Ƃ>j��l�*�=��7{~Ƃ>į"ӭ@@X�)�x�9�Z��v�-@��Lj�!@�5w�@	   BC1_DR_V3      DRIF.@=DB?&? ��qA>��$�����؝�> �=�w��@$>��-XEr>tTͭTb<�ME�?�tb(4r�����5�=ְ��!>�}6��g>j�0��NW<n�-?�@�� �4a�qpo�=T�����>M�˟T<=]��U�?� �a���	�RR���`��s����42���4?|�h��?�>mL���ӷ<Ĉ~�Ă?�6�G��=I���pr=�=�I?Q�'���??q}����E?���u��<?  ebyF?�B'�?  S?��=�=�I�Q�'���?�Fܒ��uB����u��<�  �K�D��k����  ������/[ZxPI?�к͠�??q}����E?HJ�tž:?  ebyF?�B'�?  S?��=.@=DB?�ME�?n�-?=]��U�?�42���4?Ĉ~�Ă?I���pr=/���F��=GDo*��>DѶU���=F|�Z��>�T��*�=�^�Ƃ>j��l�*�=��7{~Ƃ>oyϟ(�G@�)��?��Fﱯ%@1����@��ԕ>@   BC1_DR_QD_ED      DRIF����?��с�A>�s�S���|�	�Z��=Ϋ���$>��t ��r>����b<�ME�?G��F������5�=����!>�}6��g>��4�NW<.�ՙE?�}y3 �Ŧ����=����'>�.<�D�<=]��U�?˜Owf���	�RR��PlH�q�����
z��4?�u��?�>2w��ӷ<Ĉ~�Ă?��(��=ɾ�0�pr=�uTV��I?Q�'���?? �'E?���u��<?  BcbyF?�B'�?  �Y��=�uTV��I�Q�'���?��D�`�A����u��<�  gH�D��k����  �D��������I?�к͠�?? �'E?HJ�tž:?  BcbyF?�B'�?  �Y��=����?�ME�?.�ՙE?=]��U�?��
z��4?Ĉ~�Ă?ɾ�0�pr=f���F��=oBo*��>y϶U���=k|�Z��>�T��*�=�^�Ƃ>j��l�*�=��7{~Ƃ>D�qt!I@^׏-�?��T�x$@(B"+�7@��cXg@
   BC1_QD_V03      QUAD30��?E:6~I=>�J������۪8�=���F%>�a�1s>Tb�G��b<�ubyD?(j-����jh�=�v\ ^�>(��=W�b>��,�6iR<^��b3�
?L��d�r�V�z�=G�����>�*ʅvC<zek �
�>`���G0����E|��׋��J�ᯁm��4?�����?�>�~h�ӷ<Ĉ~�Ă?(� �=����pr=�6+]J?��G
�9?�=�u�D?��}f�7?  eMbyF?�B'�?  �Џ�=�6+]J���G
�9��e��qgA���}f�7�  3F�D��k����  � �����~>|RJ?�t��8?�=�u�D?~�K��6?  eMbyF?�B'�?  �Џ�=30��?�ubyD?^��b3�
?zek �
�>ᯁm��4?Ĉ~�Ă?����pr=� ���=t^�Q t�>b�kI���=Y�foAh�>�'r
:&�=�������>��Cr3&�=�� )���>S|�p�nJ@��۹V�9�S���z#@G��b@��cXg@   BC1_COR      KICKER30��?E:6~I=>�J������۪8�=���F%>�a�1s>Tb�G��b<�ubyD?(j-����jh�=�v\ ^�>(��=W�b>��,�6iR<^��b3�
?L��d�r�V�z�=G�����>�*ʅvC<zek �
�>`���G0����E|��׋��J�ᯁm��4?�����?�>�~h�ӷ<Ĉ~�Ă?(� �=����pr=�6+]J?��G
�9?�=�u�D?��}f�7?  eMbyF?�B'�?  �Џ�=�6+]J���G
�9��e��qgA���}f�7�  3F�D��k����  � �����~>|RJ?�t��8?�=�u�D?~�K��6?  eMbyF?�B'�?  �Џ�=30��?�ubyD?^��b3�
?zek �
�>ᯁm��4?Ĉ~�Ă?����pr=� ���=t^�Q t�>b�kI���=Y�foAh�>�'r
:&�=�������>��Cr3&�=�� )���>S|�p�nJ@��۹V�9�S���z#@G��b@��cXg@   BC1_BPM      MONI30��?E:6~I=>�J������۪8�=���F%>�a�1s>Tb�G��b<�ubyD?(j-����jh�=�v\ ^�>(��=W�b>��,�6iR<^��b3�
?L��d�r�V�z�=G�����>�*ʅvC<zek �
�>`���G0����E|��׋��J�ᯁm��4?�����?�>�~h�ӷ<Ĉ~�Ă?(� �=����pr=�6+]J?��G
�9?�=�u�D?��}f�7?  eMbyF?�B'�?  �Џ�=�6+]J���G
�9��e��qgA���}f�7�  3F�D��k����  � �����~>|RJ?�t��8?�=�u�D?~�K��6?  eMbyF?�B'�?  �Џ�=30��?�ubyD?^��b3�
?zek �
�>ᯁm��4?Ĉ~�Ă?����pr=� ���=t^�Q t�>b�kI���=Y�foAh�>�'r
:&�=�������>��Cr3&�=�� )���>S|�p�nJ@��۹V�9�S���z#@G��b@�?�	�@
   BC1_QD_V03      QUAD�HG?o�)���5>�`08�]��D�w�=q�!�%>�v�G�s>m`&kKc<��j�4?�ImZ�Q/���=�Y�$N>�oĠ�%[>�m����J<M�ēh
?�8m��F����T%�=���r>��v_��<dt�S�>�n�j�૽n�**������ƍs���,f��4?��X��?�>���p�ӷ<Ĉ~�Ă?�w�=�>��pr=Ɏ &�J?~r+I2?ha�l�"D?���}2?  �?byF?�B'�?  \O��=Ɏ &�J�~r+I2�l	h�A����}2�  F�D��k����  ������Q)�v��J?�U�!2?ha�l�"D?�����1?  �?byF?�B'�?  \O��=�HG?��j�4?M�ēh
?dt�S�>�,f��4?Ĉ~�Ă?�>��pr=ۙ��:��=��g�Gx�>�'F���=8Hx`el�>�	�E"�=�P٥���>��j?"�=)1O扽�>tI�ۅQK@��TŰ^3��+n�"@A��@ڑ�9��@   BC1_DR_QD_ED      DRIF�k��?��D� 6>��d$��)=�p��=ʂ���%>������s>�q�e�c<��j�4?*$�C��Q/���=����M>�oĠ�%[>����J<O.���	?�&F��qD*����=Ł ī,>?HJ1�<dt�S�>L����૽n�**�����$�qn�����`��4?��~��?�>=�}�ӷ<Ĉ~�Ă?�^��='���pr=k���/K?~r+I2?���F�C?���}2?  G5byF?�B'�?  Sy��=k���/K�~r+I2��/O��@����}2�  
G�D��k����  ������C rP#K?�U�!2?���F�C?�����1?  G5byF?�B'�?  Sy��=�k��?��j�4?O.���	?dt�S�>���`��4?Ĉ~�Ă?'���pr=h���:��="�g�Gx�>~'F���=�Gx`el�>�	�E"�=�P٥���>��j?"�="1O扽�>��1��%L@�/O��3��N�E�!@���7�@�[�@	   BC1_DR_V4      DRIF���u�� ?uP
�G�7>?�rт���5�w#�="��u'>��M�r)u>ӭi8��d<��j�4?ŭ|��A�Q/���=��(bL>�oĠ�%[>M��~�J<���/?K�%���i~��t�=����>Pǒ��u <dt�S�><�u��૽n�**�����1�gV��ЋH��4?O_�s�?�>��tłӷ<Ĉ~�Ă?�2Ġ=�Mwqpr=�@���M?~r+I2?g�����A?���}2?  �byF?�B'�?  �i�=�@���M�~r+I2���?"�=����}2�  hK�D��k����  ����<~#l�L?�U�!2?g�����A?�����1?  �byF?�B'�?  �i�=���u�� ?��j�4?���/?dt�S�>ЋH��4?Ĉ~�Ă?�Mwqpr=ۙ��:��=��g�Gx�>�'F���=7Hx`el�>�	�E"�=�P٥���>�j?"�=1O扽�>$�R��P@=?�!
5����U��@��2+@�UD&�@   BC1_DR_QD_ED      DRIFD��F-8!?$��7>��B)�2���	ˏH�= ����'>�2U2Jsu>��6��4e<��j�4? f�!��Q/���=�&L>�oĠ�%[>Ռ��v�J<w��~�?쭽�	��wԷT�(�=�k߃�>�&�j42 <dt�S�>�3�D᫽n�**�����KQ��j��B��4?q]n�?�>����{ӷ<Ĉ~�Ă?)HZ轠=�u�lpr=t^'�gM?~r+I2?�+�Z�A?���}2?  ��ayF?�B'�?  �,c�=t^'�gM�~r+I2����p=����}2�  VL�D��k����  �rz���.U
�VM?�U�!2?�+�Z�A?�����1?  ��ayF?�B'�?  �,c�=D��F-8!?��j�4?w��~�?dt�S�>j��B��4?Ĉ~�Ă?�u�lpr=ۙ��:��=��g�Gx�>�'F���=8Hx`el�>�	�E"�=�P٥���>��j?"�=)1O扽�>�a��*�P@��iU5���5�b@3��]@&�����@
   BC1_QD_V04      QUAD�oI�:^!?[�3�9W >��9Q����5�
:���=��9k��'>�t,�e�u>�u��Wbe<hZ��Xf�>��,l�̽����;8�=�;̳��=���L��A>����}1<���E�P?h+�Ha������=�U	��l>���:� <:�"��>��Y�Z���� ���H᧤�ֻ/�?��4?s�j�?�>��YKuӷ<Ĉ~�Ă? �V���=`��gpr=ܥ��W�M?V���ߞ?�F���3A?d���'?  �ayF?�B'�?  	�]�=ܥ��W�M�>���D98��q<�d���'�  N�D��k����  �Ru�����{���M?V���ߞ?�F���3A?�����&?  �ayF?�B'�?  	�]�=�oI�:^!?hZ��Xf�>���E�P?:�"��>/�?��4?Ĉ~�Ă?`��gpr=���2E�=�����>�C��9�=j��ݙՂ>��n&�=Y�y�E��>p)B��=1-t�>��>m�Q�zP@�̇Ȝ����D}@�O:�)@&�����@   BC1_COR      KICKER�oI�:^!?[�3�9W >��9Q����5�
:���=��9k��'>�t,�e�u>�u��Wbe<hZ��Xf�>��,l�̽����;8�=�;̳��=���L��A>����}1<���E�P?h+�Ha������=�U	��l>���:� <:�"��>��Y�Z���� ���H᧤�ֻ/�?��4?s�j�?�>��YKuӷ<Ĉ~�Ă? �V���=`��gpr=ܥ��W�M?V���ߞ?�F���3A?d���'?  �ayF?�B'�?  	�]�=ܥ��W�M�>���D98��q<�d���'�  N�D��k����  �Ru�����{���M?V���ߞ?�F���3A?�����&?  �ayF?�B'�?  	�]�=�oI�:^!?hZ��Xf�>���E�P?:�"��>/�?��4?Ĉ~�Ă?`��gpr=���2E�=�����>�C��9�=j��ݙՂ>��n&�=Y�y�E��>p)B��=1-t�>��>m�Q�zP@�̇Ȝ����D}@�O:�)@&�����@   BC1_BPM      MONI�oI�:^!?[�3�9W >��9Q����5�
:���=��9k��'>�t,�e�u>�u��Wbe<hZ��Xf�>��,l�̽����;8�=�;̳��=���L��A>����}1<���E�P?h+�Ha������=�U	��l>���:� <:�"��>��Y�Z���� ���H᧤�ֻ/�?��4?s�j�?�>��YKuӷ<Ĉ~�Ă? �V���=`��gpr=ܥ��W�M?V���ߞ?�F���3A?d���'?  �ayF?�B'�?  	�]�=ܥ��W�M�>���D98��q<�d���'�  N�D��k����  �Ru�����{���M?V���ߞ?�F���3A?�����&?  �ayF?�B'�?  	�]�=�oI�:^!?hZ��Xf�>���E�P?:�"��>/�?��4?Ĉ~�Ă?`��gpr=���2E�=�����>�C��9�=j��ݙՂ>��n&�=Y�y�E��>p)B��=1-t�>��>m�Q�zP@�̇Ȝ����D}@�O:�)@Oc��@
   BC1_QD_V04      QUAD��\�^!?5��U�9�W";۹l����/���=���'>�rZ��u>{�%uae<\��'��>Z��[�=H�5A雨�^�����cJq|�C�m��2��iڍ�!?��Շ&��A��5���=L9ЀL`>��=qg��;A�Ő��>�zXs=����[�=��@�;�s?��4?��Ij�?�>���nӷ<Ĉ~�Ă?^ԯ��=�s�bpr=��O���M?�C��}?�"��?A?���J�?  ��ayF?�B'�?  �;X�=��O���M��ʩ����~rQ�<�4�kS�  3O�D��k����  �1p���N���E�M?�C��}?�"��?A?���J�?  ��ayF?�B'�?  �;X�=��\�^!?\��'��>�iڍ�!?A�Ő��>�s?��4?Ĉ~�Ă?�s�bpr=o�$9A$�=x}�-ȃ>P1 ��=G����>��Yd��=��޳�>G����=��~׳�>��3�iwO@�a���@Μ���@s紐��?;`�P1@   BC1_DR_QD_ED      DRIF��e�K!?��$���_$��+�N!��=���D�'>����u>�!�z�Ge<\��'��>�:l��=H�5A雨�Jz����cJq|�C�F6�g�2���H��	?$���-7���^�m��=��N�e>��`���;A�Ő��>J8�5Xs=����[�=��˽@�;	R�>��4?2�Xi�?�>J���gӷ<Ĉ~�Ă?M�Vӫ�=��ѯ\pr=�%ۮJ�M?�C��}?�AVa�@?���J�?  n�ayF?�B'�?  �mR�=�%ۮJ�M��ʩ����!)��;�4�kS�  _N�D��k����  �j���/E��uoM?�C��}?�AVa�@?���J�?  n�ayF?�B'�?  �mR�=��e�K!?\��'��>��H��	?A�Ő��>	R�>��4?Ĉ~�Ă?��ѯ\pr=G�$9A$�=�w}�-ȃ>)1 ��=����>��Yd��=��޳�>F����=��~׳�>�؋��0O@`h��@
�J���@�
ݲ�h�?cBS�@	   BC1_DR_V5      DRIFj�\��� ?��xl�VE\ V����l�x2�=���T'> �;�u>vI���d<\��'��>�� ��=H�5A雨�t$u����cJq|�C�yP����2��R��?��`d���i�=�j�@�~>zG<�� <A�Ő��>+�S�Xs=����[�=GG�A�@�;���:��4?��d�?�>j�,QGӷ<Ĉ~�Ă?&�I��=N1�Cpr=���:��L?�C��}?��'��@?���J�?  F�ayF?�B'�?  �7�=���:��L��ʩ���G	�S3�:�4�kS�  |J�D��k����  k	Q����"�z�L?�C��}?��'��@?���J�?  F�ayF?�B'�?  �7�=j�\��� ?\��'��>�R��?A�Ő��>���:��4?Ĉ~�Ă?N1�Cpr=G�$9A$�=�w}�-ȃ>)1 ��=����>��Yd��=��޳�>G����=��~׳�>�B�w�M@�و�`@�w��w�@?)��?���) @	   BC1_DR_20      DRIF�+O�.	 ?�K�<�� �������BY-���=&B�&>Y�Q���s>�uM�M�c<\��'��>f���;�=H�5A雨���:���cJq|�C�W(���2��~<ܗ�?mF��?�⽢u�g�Z�=��Es�>>��d^ <A�Ő��>H�7�Xs=����[�=����g@�;��0��4?j�Y�?�>�����ҷ<Ĉ~�Ă?���D�=�V��pr=�H��MK?�C��}?F��7�>?���J�?  ��ayF?�B'�?  ���
�=�H��MK��ʩ���8��*�8�4�kS�  �@�D��k����  2������,�1K?�C��}?F��7�>?���J�?  ��ayF?�B'�?  ���
�=�+O�.	 ?\��'��>�~<ܗ�?A�Ő��>��0��4?Ĉ~�Ă?�V��pr=,�$9A$�=�w}�-ȃ>1 ��=����>��Yd��=��޳�>G����=��~׳�>�� ��J@�I�[o�@� ��ւ@�g.>��?��SeL!@   BC1_DP_DIP1   	   CSRCSBEND�4�k%?��aJ�|r>+^b�{*�MxX���=��W5d��
�M���  ,N�塼t� �tD?w����5ǽ� (��7��o��hs)���E�>��׾�j���TǼ��)��?���8I��"`�8��=����W�>��p���	<7.L�'�>�iq�S����C4�=Z]��ֻ��U�=�4?�=�fj�>�5�1,�<窃}�Â?�E���=#��je�r= ��D>Q?�����V?�8Q�K=?��<X5�?  �'�ZG?��Z���?  VV�Ԅ= ��D>Q������V����%v7��7U�]j�  �	�E���������  bt;��ʒ6��XO?z�ukW'U?�8Q�K=?��<X5�?  �'�ZG?��Z���?  VV�Ԅ=�4�k%?t� �tD?��)��?7.L�'�>��U�=�4?窃}�Â?#��je�r=$��l�s>D��;@?g��c�	>mUE�j˚>�U���=å�~���>Rp{��=� �✴�>mԽ`�1@|)�5�� @X�����@����E�?jk����!@   BC1_DR_SIDE_C      CSRDRIFT[�7_�4?-K�m�>��)�E*�����=�h�)�y�N�d���ƾ֎0t�t� �tD?�W�mRSɽ� (��7���1�t�)��pOX�H�׾��IߢTǼ鉆M�s?r�Q�{޽pu}�=��=��P��>e�//�<7.L�'�>����U����jX4�=ܑ]�ֻ�;��L�4?��{��i�>m�&$,�<�+ghÂ?sP�Y�=���M�r=�5g��V?�����V?0�/ɮ�;?��<X5�?  i�#\G?��>_�?  �/ք=�5g��V������V�@{�ý�6��7U�]j�  �ΎE���EN1���  ��6:���#���T?z�ukW'U?0�/ɮ�;?��<X5�?  i�#\G?��>_�?  �/ք=[�7_�4?t� �tD?鉆M�s?7.L�'�>�;��L�4?�+ghÂ?���M�r=*��l�s>�Ё�8@?TIY�3
>����f�>�U���=oנ���>o��{��=h䮃���>Φ��9�0@(�7� @D�6�2�@"9"%}��?������'@   BC1_DR_SIDE      DRIF�C~6Ua?�����õ>7u5��GFLk��=�h�n�����:+�3P��%����t� �tD?��%׽� (��7���<�J*��pOX�H�׾�IL�~SǼD�}:u?�5��=����a�=�Ƅ���>�5G�fa <7.L�'�>d��g����jX4�=ﴜ��ֻV����4?p�
��j�>�&�/�+�<�+ghÂ?�]�c�=ꇿ�h�r= m�E�s?�����V?�����0?��<X5�?  U�1jG?��>_�?  c�&�= m�E�s������V�F8g�D.��7U�]j�  [���E���EN1���  �:�-���]��Pr?z�ukW'U?�����0?��<X5�?  U�1jG?��>_�?  c�&�=�C~6Ua?t� �tD?D�}:u?7.L�'�>V����4?�+ghÂ?ꇿ�h�r=*��l�s>�Ё�8@?��c�3
>ռ��f�>�U���=oנ���>o��{��=h䮃���>��$��3@V�+����?�sF��@����aϿ�s�ʜ(@   BC1_DP_DIP2   	   CSRCSBENDb����`b?4���>n[da?��O���=�vŇ������*������.�ּcSyw �>��BB����1��ը���W�lz�4�-�tF��Z�qo#"�8$�39�?^Qv�t��=ޘ�WȰ=�r�� >m��Rp��;V- ���>8i�K���It�Mgw�=��N�>Ļ�`~/�_&?0�Z9�>�;�/ɛ<�	�:���?���I�<�����c=�0���t?�̘��?����b0?�0GY�(?  FD6�8?dc�?  �v=�0���t�9������`�R��.�T�_����  8:s�6�q��C䶒�  �Jt�+�{V�Bs?�̘��?����b0?�0GY�(?  FD6�8?dc�?  �v=b����`b?cSyw �>8$�39�?V- ���>�`~/�_&?�	�:���?�����c=�*��`>�&�%C�>��@R��=j��"j�>o�͘�=���M���>��_g��=�v`����>9 ��V'@���<4� @����B�@��W��<ҿK ���O)@   BC1_DR_CENT_C      CSRDRIFT���`ab?ܮ0��>X-���𽔙�>���=-[@yl������.�����h!��ּcSyw �>��W�T���1��ը�����jz�Ir�csF���#"�(a�xB?E�`;�=-�#㇖�=�ĊW$>\@�K��;V- ���>���P������2_�=̓^(�>Ļ�<��_&?���9�>���q�ț<,i�ߵ��?��vi2I�<zx�L�c=�	����t?�̘��?}!� �0?�0GY�(?  ,#;�8?�́��?  :0�v=�	����t�9�����n ��`/�T�_����  4�k�6��]`�M���  >R�It�#���,s?�̘��?}!� �0?�0GY�(?  ,#;�8?�́��?  :0�v=���`ab?cSyw �>(a�xB?V- ���>�<��_&?,i�ߵ��?zx�L�c=�*��`>0nN2C�>�q���d�=�Q��`�>o�͘�=$-�Ҕ��>���g��=bl�6���>+pZL_$@�%�;3�?]pe�6}@�ӎ�.׿K ���O)@   BC1_BPM      MONI���`ab?ܮ0��>X-���𽔙�>���=-[@yl������.�����h!��ּcSyw �>��W�T���1��ը�����jz�Ir�csF���#"�(a�xB?E�`;�=-�#㇖�=�ĊW$>\@�K��;V- ���>���P������2_�=̓^(�>Ļ�<��_&?���9�>���q�ț<,i�ߵ��?��vi2I�<zx�L�c=�	����t?�̘��?}!� �0?�0GY�(?  ,#;�8?�́��?  :0�v=�	����t�9�����n ��`/�T�_����  4�k�6��]`�M���  >R�It�#���,s?�̘��?}!� �0?�0GY�(?  ,#;�8?�́��?  :0�v=���`ab?cSyw �>(a�xB?V- ���>�<��_&?,i�ߵ��?zx�L�c=�*��`>0nN2C�>�q���d�=�Q��`�>o�͘�=$-�Ҕ��>���g��=bl�6���>+pZL_$@�%�;3�?]pe�6}@�ӎ�.׿~3�+1*@   BC1_DR_CENT      DRIFT�\�ab?�ݞ�oE>��fOA�`�[��=�\��Q����wL������ܷ!��ּcSyw �>���kh����1��ը�Ss�Ujz�Ir�csF��L�n�""��=#s��?�8�.K��=w�Bg��=3P�-�>W;[&0�;V- ���>�A3R������2_�=c*�<�>Ļ2����_&?��9�>��Tț<,i�ߵ��?DZ�?�H�<-�� ��c=��%�t?�̘��?������/?�0GY�(?  �;�8?�́��?  ��Kv=��%�t�9������AM1�/�T�_����  0(k�6��]`�M���  ��aIt�wKּSs?�̘��?������/?�0GY�(?  �;�8?�́��?  ��Kv=T�\�ab?cSyw �>�=#s��?V- ���>2����_&?,i�ߵ��?-�� ��c=�*��`>0nN2C�>/����d�=�c��`�>o�͘�=$-�Ҕ��>���g��=bl�6���>����!@&
���?Ҝ墒�@�e.e!ܿ��	%�*@   BC1_DP_DIP3   	   CSRCSBENDφpݚWa?ٜ������Bga콿��!�h�=�}���j�?��|�J��N�*�y���5�!	D?���k�K�=q�{n���c�@E�N>��qͫq�>S�r���<Vq�z?��b��=�D{��]|����{� >��Sx�	��C�K�=�>zJ��F�H=���o�����5��3�;Ҋ��a��>�ݒ��>���D@<Bw�B]��?�u�fYK�<��d�m�4=B�.qs?*����V?��׭�?0?��s�?  p�A�?��f ��?  ��7�G=B�.qs������T���׭�?0��%�q5��  ��WD
�)�t���  Ӏ8F�Nl�sV�q?*����V?;�,�ɨ/?��s�?  p�A�?��f ��?  ��7�G=φpݚWa?�5�!	D?Vq�z?C�K�=�>Ҋ��a��>Bw�B]��?��d�m�4=�V;�wJ>�BZ�>5�����=�~�(웅>d$R�F�=$�q���># �_@�=��9���>q����%@���=̉@s�; *@�$W:��ݿ6�&��i+@   BC1_DR_SIDE_C      CSRDRIFT/N�"�_?�$�Vճ�nt�?*�uYvŶ��=�-uQ�ih�����,��*9�v네��5�!	D?EVx�v�=q�{n����hP��N>���r�>Ttx)��<X"�� ?��q�=�=Z[��)�{�`6�p5>,|J��P��C�K�=�>�fJ(�H=�z4��ב���}3��;�Nv��>!�ȥ��>Dy �2@<��T/��?���K�<֢�R5�4=�t�ez�q?*����V?b~���l0?��s�?  ����?kny���?  p��G=�t�ez�q������T�b~���l0��%�q5��  ��B)
����w����  �V�F�>�M�Jip?*����V?ӑ� �(0?��s�?  ����?kny���?  p��G=/N�"�_?�5�!	D?X"�� ?C�K�=�>�Nv��>��T/��?֢�R5�4=-V;�wJ>ZDP��Y�>�f�P-��=U��%�u�>d$R�F�=G�"2ȵ�>WcTb@�=%�z����>�qaJ�h#@�����@�,�/]@��
I� �c�E�0@   BC1_DR_SIDE      DRIF�'hU� ?��j�1�t�l~�F�Zн�fh�,�=um �+)�		9�6��Ծ���e��5�!	D?�Lx�>4�=q�{n����AV �N>���r�>m^.��<�\�RR?�e�����=5x���t�M�e�q�>@�-ʴ�C�K�=�>�A\�F=�z4��ב��� �X��;cfD
��>�3���>,�jx�@<��T/��?��+��<�<��v#�4=�6^T�<?*����V?�":b��5?��s�?   ���?kny���?  0�Y`H=H�7G7������T�l�Lՠ{3��%�q5��  @�0	����w����   j	+E��6^T�<?*����V?�":b��5?��s�?   ���?kny���?  0�Y`H=�'hU� ?�5�!	D?�\�RR?C�K�=�>cfD
��>��T/��?��v#�4=j'V;�wJ>�LP��Y�>r$@P-��=�-%�u�>f$R�F�=I�"2ȵ�>YcTb@�=(�z����>��w�r��?�<�f�?s�X&&@RSƉ�v�r~�4i1@   BC1_DP_DIP4   	   CSRCSBEND�mȓ�y�>?���/�=\ �i��ӽ���xp=�4����ս	�N��]�F�iEF��i�`��>��L|BӽI���w����b���̴=�5�
q=>�٠x�m�;פ]1f?�5(1�=���;�=�B�Y�>�q���;� x7���>W�A�� ���ꇱ���B�NxZ�pF"7��>���:*4�>}�θ#dD<��:��?&A2��S�<�2k�u7=����//?Z�CxӘ?��!ܠ8?:���F	?  `a�?,����?  �j7�J=����//�Z�CxӘ���<�� 4� ZɎ)�  �0�D
��W��c��  ��KF�r PS�--?��?��!ܠ8?:���F	?  `a�?,����?  �j7�J=�mȓ�y�>i�`��>פ]1f?� x7���>pF"7��>��:��?�2k�u7=y�v#��="ȗk���>���@��=��2P�>E����=]�"ʙ��>v�֛��=?WJ ���>�WF����?Ƙ>��ڴ�w��y�'@���kg��?K�6]1@   BC1_DR_20_C      CSRDRIFTdc븞?�>���b�=!�񐛣ٽZ�B�������
nԽ�X��[��S`��4�i�`��>��pb�ԽI���w���oH;��ʴ=c�b*^I=>M�O��k�;�8��};?]�Bl6�=����=F�/}>bJ�����;� x7���>�0� �ۨJ-������IvZ��:�c��>1"#�5�>����:bD<FM�ħ�?Z*'/S�<���<�r7=SXv�)�1?Z�CxӘ?��s)��9?:���F	?  �)��?���d�?  P0��J=SXv�)�1�Z�CxӘ�j6^e4� ZɎ)�  ���C
���$�`��  0~�F�R�2�60?��?��s)��9?:���F	?  �)��?���d�?  P0��J=dc븞?�>i�`��>�8��};?� x7���>�:�c��>FM�ħ�?���<�r7=y�v#��=��#�H��>6���%��=���"P�>E����=M��N��>u8����=k�) H��>+.��ڻ�?<'��x�ҿ�g�R>)@���m��?K�6]1@	   BC1_WA_OU      WATCHdc븞?�>���b�=!�񐛣ٽZ�B�������
nԽ�X��[��S`��4�i�`��>��pb�ԽI���w���oH;��ʴ=c�b*^I=>M�O��k�;�8��};?]�Bl6�=����=F�/}>bJ�����;� x7���>�0� �ۨJ-������IvZ��:�c��>1"#�5�>����:bD<FM�ħ�?Z*'/S�<���<�r7=SXv�)�1?Z�CxӘ?��s)��9?:���F	?  �)��?���d�?  P0��J=SXv�)�1�Z�CxӘ�j6^e4� ZɎ)�  ���C
���$�`��  0~�F�R�2�60?��?��s)��9?:���F	?  �)��?���d�?  P0��J=dc븞?�>i�`��>�8��};?� x7���>�:�c��>FM�ħ�?���<�r7=y�v#��=��#�H��>6���%��=���"P�>E����=M��N��>u8����=k�) H��>+.��ڻ�?<'��x�ҿ�g�R>)@���m��