
%

check_longi = 0;
check_steering = 1;

%

%B = load("Outputs/nominal_Nov12/Beam.dat");
B = load("Outputs/nominal_Nov13/Beam.dat");

%

E = B(:,1); % GeV
X = B(:,2); % um
Y = B(:,3); % um
Z = B(:,4); % um
XP = B(:,5); % urad
YP = B(:,6); % urad

if (check_longi)
  
  figure('visible','off');

  subplot(2,2, 1);
  pkg load statistics;
  hold on;
  [NN, CC] = hist3 ([Z E], [200 200]);
  imagesc (CC{1}, CC{2}, NN');
  axis xy;
  colorbar;
  ylabel("E [GeV]");
  xlabel("Z [um]");

  subplot(2,2, 2);
  hist(E);
  xlabel("E [GeV]");

  subplot(2,2, 3);
  hist(Z);
  xlabel("Z [um]");

  subplot(2,2, 4);
  addpath('functions');
  [E_sp_sl, Z_sl] = Sliced_E_spread(B,100);
  [I_sl, Z_sl] = Sliced_I(B,100);
  ax = plotyy(Z_sl, I_sl, Z_sl, E_sp_sl*100);
  ylabel (ax(1), 'Sliced current [kA]');
  ylabel (ax(2), 'Sliced energy spread [%]');
  xlabel("Z [um]");
  ylim(ax(1), [0 10]);
  ylim(ax(2), [0 0.05]);

  print('plot.png', '-dpng', '-S1300,900');
endif

if ( check_steering )

  pkg load statistics
  
  figure('visible','off');
  hold on;
  
  subplot(2,2, 1);
  hold on;
  [NN, CC] = hist3 ([Z X], [200 300]);
  imagesc (CC{1}, CC{2}, NN');
  axis xy;
  colorbar;
  ylabel("X [um]");
  xlabel("Z [um]");
  ylim([mean(X)-30 mean(X)+30]);
  
  subplot(2,2, 2);
  hold on;
  [NN, CC] = hist3 ([Z Y], [200 300]);
  imagesc (CC{1}, CC{2}, NN');
  axis xy;
  colorbar;
  ylabel("Y [um]");
  xlabel("Z [um]");
  ylim([mean(Y)-30 mean(Y)+30]);
  
  subplot(2,2, 3);
  hold on;
  [NN, CC] = hist3 ([Z XP], [200 300]);
  imagesc (CC{1}, CC{2}, NN');
  axis xy;
  colorbar;
  ylabel("XP [urad]");
  xlabel("Z [um]");
  ylim([mean(XP)-10 mean(XP)+10]);
  
  subplot(2,2, 4);
  hold on;
  [NN, CC] = hist3 ([Z YP], [200 300]);
  imagesc (CC{1}, CC{2}, NN');
  axis xy;
  colorbar;
  ylabel("YP [urad]");
  xlabel("Z [um]");
  ylim([mean(YP)-10 mean(YP)+10]);

  print('plot.png', '-dpng', '-S1300,900');

endif

system('feh plot.png');
pause();
system('rm plot.png');

