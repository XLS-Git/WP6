
PD=$(pwd)

for opt in incoh_1 incoh_2 incoh_3
do
  cd ${PD}/Outputs/${opt}
  tar -zcvf RandomBeams.tar.gz RandomBeams/
done

cd $PD
