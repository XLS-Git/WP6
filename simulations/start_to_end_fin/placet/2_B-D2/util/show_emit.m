
% output beam format: %E %x %y %s %xp %yp %ex %ey

A = load('Outputs/nominal_Nov13/Emittance.dat');

E  = A(:,1); % GeV
X  = A(:,2); % um
Y  = A(:,3);
Z  = A(:,4);
XP = A(:,5); % urad
YP = A(:,6);
EX = A(:,7)*0.1; % mm.mrad (um)
EY = A(:,8)*0.1;

printf("INFO:: Final emittance X: %.2f mm.mrad\n",EX(end));
printf("INFO:: Final emittance Y: %.2f mm.mrad\n",EY(end));

