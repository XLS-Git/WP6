
function TA = tan_a(A,R1,R2)
  TA = tan((A - R1).*pi./(R2-R1) - pi/2);
endfunction

