
% sliced energy spread function
function E_sp_sl_w = Sliced_weighted_E_spread_eff (B,ns)
  E = B(:,1);
  Z = B(:,4);
  n_tot = length(E);
  zmean = mean(Z);
  zspread = std(Z);
  zmin = zmean - 1.5*zspread;
  zmax = zmean + 1.5*zspread;
  if (zmin<min(Z)) zmin = min(Z); endif
  if (zmax>max(Z)) zmax = max(Z); endif
  Zmins = linspace(zmin,zmax,ns+1)(1:end-1);
  Zmaxs = linspace(zmin,zmax,ns+1)(2:end);
  E_sp_sl_w = [];
  for i=1:ns
    M = Z>=Zmins(i) & Z<Zmaxs(i);
    E_i = E(M);
    n_i = length(E_i);
    if (length(E_i)==0) 
      e_mean = 0;
    else 
      e_mean = mean(E_i); 
    endif

    if (e_mean==0 || n_tot==0)
      e_spread = 0;
    else 
      e_spread = std(E_i) / e_mean;
      %e_spread_w = e_spread * power(ns*n_i/n_tot,2);
      e_spread_w = e_spread * ns*n_i/n_tot;
      E_sp_sl_w = [ E_sp_sl_w e_spread_w ];
    endif
  endfor
endfunction

