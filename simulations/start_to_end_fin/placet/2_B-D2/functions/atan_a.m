
function AA = atan_a(A,R1,R2)
  AA = R1 + (R2-R1)./pi.*(atan(A)+pi/2);
endfunction

