
% merit function
function merit_beam = FoM_beam_Nov13 (B)

  printf("OPT_INFO::  . . . FoM_beam_Nov13 . . . \n");

  E = B(:,1);
  Z = B(:,4);
  X = B(:,2);
  Y = B(:,3);
  XP = B(:,5);
  YP = B(:,6);

  n_sl = 100; % Number of slices

  %% Current and bunch length

  [I_sl, Z_sl] = Sliced_I (B,n_sl);

  I_plateau = mean(sort(I_sl)(end-floor(n_sl/3):end)); % 5 kA targeted

  if (I_plateau < 5)
    merit_I = (5-I_plateau)*5; % e.g 3 kA gives merit of 10
  else
    merit_I = 0;
  endif

  Z_length = range(Z_sl); % 5 um targeted (lower priority given required I. Just in case of discontinuity)

  if (Z_length > 5)
    merit_Z = (Z_length-5)*0.5; % e.g 6--20 um gives merit of 0.5--7.5
  else
    merit_Z = 0;
  endif

  %% Energy and energy spread

  E_mean = mean(E);
  M_E = E > E_mean-0.01 & E < E_mean+0.01;
  E_mean = mean(E(M_E));
  if (E_mean<5.50) 
    merit_E = (5.5 - E_mean)*30; % e.g 5.0--5.4 GeV gives merit of 15--3
  else 
    merit_E = (E_mean - 5.5)*20; % e.g 5.6--6.0 GeV gives merit of 2--10
  endif

  E_sp_sl_w = Sliced_weighted_E_spread_eff(B,n_sl);
  E_sp_sl_w_mean = mean(E_sp_sl_w); % targetd 0.01%
  merit_E_sp = E_sp_sl_w_mean * 1e4; % e.g 0.1% gives merit of 10

  %% Transverse offsets and beam sizes

  X_off_sl_w  = Sliced_weighted_offset_eff(B,2,n_sl);
  Y_off_sl_w  = Sliced_weighted_offset_eff(B,3,n_sl);
  XP_off_sl_w = Sliced_weighted_offset_eff(B,5,n_sl);
  YP_off_sl_w = Sliced_weighted_offset_eff(B,6,n_sl);

  X_off_sl_w_mean  = mean(abs(X_off_sl_w));  % 0 um targeted
  Y_off_sl_w_mean  = mean(abs(Y_off_sl_w));
  XP_off_sl_w_mean = mean(abs(XP_off_sl_w)); % 0 urad targeted
  YP_off_sl_w_mean = mean(abs(YP_off_sl_w));

  merit_x_off = X_off_sl_w_mean*0.5; % e.g 20 um gives merit of 10
  merit_y_off = Y_off_sl_w_mean*0.5;
  merit_xp_off = XP_off_sl_w_mean*0.5; % e.g 20 urad gives merit of 10
  merit_yp_off = YP_off_sl_w_mean*0.5;

  X_size = std(X); % 5 um targeted (low priority)
  Y_size = std(Y);
  merit_X_size = X_size*0.15; % e.g 5--20 um gives merit of 0.75--3
  merit_Y_size = Y_size*0.15;
  merit_XY = hypot(merit_X_size, merit_Y_size);

  %% Backups

  %Z_mean = mean(Z);
  %[pv, ks] = ks_test(Z,'unif',min(Z),max(Z));
  %kurt = kurtosis(Z);

  %% final merit

  merit_off = hypot( merit_x_off, merit_y_off, merit_xp_off, merit_yp_off );
  merit_beam = hypot( merit_I, merit_Z, merit_E, merit_E_sp, merit_off, merit_XY );

  %% Printing message

  printf("OPT_INFO:: \n");
  printf("OPT_INFO:: I_plateau        = %.3f kA  (merit = %.2f)\n", I_plateau,merit_I);
  printf("OPT_INFO:: Z_length         = %.3f um  (merit = %.2f)\n", Z_length,merit_Z);
  printf("OPT_INFO:: E_mean           = %.3f GeV  (merit = %.2f)\n", E_mean,merit_E);
  printf("OPT_INFO:: E_sp_sl_w_mean   = %.3f %%  (merit = %.2f)\n", E_sp_sl_w_mean*1e2,merit_E_sp);
  printf("OPT_INFO:: X_off_sl_w_mean    = %.3f um  (merit = %.2f)\n", X_off_sl_w_mean,merit_x_off);
  printf("OPT_INFO:: Y_off_sl_w_mean    = %.3f um  (merit = %.2f)\n", Y_off_sl_w_mean,merit_y_off);
  printf("OPT_INFO:: XP_off_sl_w_mean   = %.3f urad  (merit = %.2f)\n", XP_off_sl_w_mean,merit_xp_off);
  printf("OPT_INFO:: YP_off_sl_w_mean   = %.3f urad  (merit = %.2f)\n", YP_off_sl_w_mean,merit_yp_off);
  printf("OPT_INFO:: X_size           = %.3f um  (merit = %.2f)\n", X_size,merit_X_size);
  printf("OPT_INFO:: Y_size           = %.3f um  (merit = %.2f)\n", Y_size,merit_Y_size);

endfunction

