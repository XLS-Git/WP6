
function [pval, ks] = ks_test (x, dist, varargin)
    
      if (! isvector (x))
        error ("ks_test: X must be a vector");
      endif
    
      n = length (x);
      s = sort (x);
      try
        f = str2func (sprintf ("%scdf", dist));
      catch
        try
          f = str2func (sprintf ("%s_cdf", dist));
        catch
          error ("ks_test: no %scdf or %s_cdf function found",
                 dist, dist);
        end_try_catch
      end_try_catch
    
      alt = "!=";
    
      args{1} = s;
      nvargs = numel (varargin);
      if (nvargs > 0)
        if (ischar (varargin{end}))
          alt = varargin{end};
          args(2:nvargs) = varargin(1:end-1);
        else
          args(2:nvargs+1) = varargin;
        endif
      endif
    
      z = reshape (feval (f, args{:}), 1, n);
    
      if (strcmp (alt, "!=") || strcmp (alt, "<>"))
        ks   = sqrt (n) * max (max ([abs(z - (0:(n-1))/n); abs(z - (1:n)/n)]));
        #pval = 1 - ks_cdf (ks); ## error
	pval = 0;
      elseif (strcmp (alt, ">"))
        ks   = sqrt (n) * max (max ([z - (0:(n-1))/n; z - (1:n)/n]));
        pval = exp (- 2 * ks^2);
      elseif (strcmp (alt, "<"))
        ks   = - sqrt (n) * min (min ([z - (0:(n-1))/n; z - (1:n)/n]));
        pval = exp (- 2 * ks^2);
      else
        error ("ks_test: alternative %s not recognized", alt);
      endif
    
      if (nargout == 0)
        printf ("pval: %g\n", pval);
      endif
    
endfunction
