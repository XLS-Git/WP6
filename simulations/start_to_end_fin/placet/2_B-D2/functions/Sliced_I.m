
% sliced current
function [I_sl, Z_sl] = Sliced_I (B,ns)
  %ns = 200;
  Z = B(:,4);
  zmean = mean(Z);
  zspread = std(Z);
  zmin = zmean - 1.5*zspread;
  zmax = zmean + 1.5*zspread;
  if (zmin<min(Z)) zmin = min(Z); endif
  if (zmax>max(Z)) zmax = max(Z); endif
  Zmins = linspace(zmin,zmax,ns+1)(1:end-1);
  Zmaxs = linspace(zmin,zmax,ns+1)(2:end);
  I_sl = [];
  Z_sl = [];
  for i=1:ns
    M = Z>=Zmins(i) & Z<Zmaxs(i);
    Z_i = Z(M);
    n_i = length(Z_i);
    i_sl = 75e-12 * n_i/1e5 / ((Zmaxs(i) - Zmins(i))*1e-3/2.998e8);
    I_sl = [ I_sl i_sl ];

    z_c = (Zmins(i) + Zmaxs(i))/2.0;
    Z_sl = [ Z_sl z_c ];
  endfor
endfunction

