
% sliced weighted x, y, xp, yp offset function
function X_off_sl_w = Sliced_weighted_offset_eff (B, ic, ns)
  X = B(:,ic);
  Z = B(:,4);
  n_tot = length(X);
  zmean = mean(Z);
  zspread = std(Z);
  zmin = zmean - 1.5*zspread;
  zmax = zmean + 1.5*zspread;
  if (zmin<min(Z)) zmin = min(Z); endif
  if (zmax>max(Z)) zmax = max(Z); endif
  Zmins = linspace(zmin,zmax,ns+1)(1:end-1);
  Zmaxs = linspace(zmin,zmax,ns+1)(2:end);
  X_off_sl_w = [];
  for i=1:ns
    M = Z>=Zmins(i) & Z<Zmaxs(i);
    X_i = X(M);
    n_i = length(X_i);
    if (length(X_i)==0 || n_tot==0) 
      x_mean = 0;
    else 
      x_mean = mean(X_i); 
      %x_mean_w = x_mean * power(ns*n_i/n_tot,2);
      x_mean_w = x_mean * ns*n_i/n_tot;
      X_off_sl_w = [ X_off_sl_w x_mean_w ];
    endif
  endfor
endfunction

