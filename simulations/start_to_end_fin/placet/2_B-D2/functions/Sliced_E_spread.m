
% sliced energy spread function
function [E_sp_sl, Z_sl] = Sliced_E_spread (B,ns)
  E = B(:,1);
  Z = B(:,4);
  zmean = mean(Z);
  zspread = std(Z);
  zmin = zmean - 1.5*zspread;
  zmax = zmean + 1.5*zspread;
  if (zmin<min(Z)) zmin = min(Z); endif
  if (zmax>max(Z)) zmax = max(Z); endif
  Zmins = linspace(zmin,zmax,ns+1)(1:end-1);
  Zmaxs = linspace(zmin,zmax,ns+1)(2:end);
  E_sp_sl = [];
  Z_sl = [];
  for i=1:ns
    M = Z>=Zmins(i) & Z<Zmaxs(i);
    E_i = E(M);
    if (length(E_i)==0) e_mean = 0;
    else e_mean = mean(E_i); endif
    if (e_mean==0)
      e_spread = 0;
    else 
      e_spread = std(E_i) / e_mean;
    endif
    E_sp_sl = [ E_sp_sl e_spread ];

    z_center = (Zmins(i)+Zmaxs(i))/2.0;
    Z_sl = [ Z_sl z_center ];
  endfor
endfunction
