
% Input: 1st C-Band section output @ 120 MeV, in share/, [E X Y Z XP YP]
Parameters.input_file = "cband_out_4ps_0.18mm_100k_120MeV_4_laser_heater.dat";

Parameters.beamline_name = "xls_linac_hxr";

% lattice from 120 MeV, in share/
Parameters.lattice_file = "xls_lattice_c_inj_bc1_ln1_bc2_ln2_ln3_tmc_hxr_maxe.tcl";

% 75 pC bunch for all schemes
Parameters.bunch_charge = 75e-12;

% BC1
Parameters.ang1 = 0.06117379; % rad
Parameters.e01 = 0.264709+0.5e-3; % GeV

% BC2
Parameters.ang2 = 0.02132792;
Parameters.e02 = 1.044124+1e-3;

% TMC
Parameters.angt = 0.00261799;
Parameters.e0t = 5.493365;

% LN0
Parameters.grd0c = 15e-3; % GV/m
Parameters.phs0c = 30.2; % deg
Parameters.grd0k = 25.53e-3; % = 7.8e-3/(305.56*1e-3)
Parameters.phs0k = 192.0;

% LN1
Parameters.grd1 = 65e-3;
Parameters.phs1 = 35.76;

% LN2
Parameters.grd2 = 65e-3;
Parameters.phs2 = 10.82;

% LN3
Parameters.grd3 = 65e-3;
Parameters.phs3 = 10.82;

% Final matching quadrupole strengths
Parameters.FMQS = [ 0.013*Parameters.e0t*-22.613  0.013*Parameters.e0t*-22.613  0.013*Parameters.e0t*30  0.013*Parameters.e0t*30  0.013*Parameters.e0t*-10.5  0.013*Parameters.e0t*-10.5 ...
                    0.013*Parameters.e0t*15 * [1 1 -1 -1 1] ];

save("-text","config.dat","Parameters");
