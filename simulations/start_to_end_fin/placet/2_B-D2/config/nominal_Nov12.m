
% Input: 1st C-Band section output @ 120 MeV, in share/, [E X Y Z XP YP]
Parameters.input_file = "cband_out_4ps_0.18mm_100k_120MeV_4_laser_heater.dat";

Parameters.beamline_name = "xls_linac_hxr";

% lattice from 120 MeV, in share/
Parameters.lattice_file = "xls_lattice_c_inj_bc1_ln1_bc2_ln2_ln3_tmc_hxr_maxe.tcl";

% 75 pC bunch for all schemes
Parameters.bunch_charge = 75e-12;

% BC1
Parameters.ang1 = 0.060239678757756; % rad
Parameters.e01 = 0.2761115283183905; % GeV

% BC2
Parameters.ang2 = 0.0216087621768588;
Parameters.e02 = 1.06889569755204;

% TMC
Parameters.angt = 0.00261799;
Parameters.e0t = 5.493365;

% LN0
Parameters.grd0c = 0.01591868019812148; % GV/m
Parameters.phs0c = 29.54745533771792; % deg
Parameters.grd0k = 0.02524518898933211;
Parameters.phs0k = 190.5402431846163;

% LN1
Parameters.grd1 = 0.06500033184435987;
Parameters.phs1 = 35.12395985105615;

% LN2
Parameters.grd2 = 0.06500034492181604;
Parameters.phs2 = 11.11617734739295;

% LN3
Parameters.grd3 = 0.06500033251051278;
Parameters.phs3 = 10.75842250165791;

% Final matching quadrupole strengths
Parameters.FMQS = [ -2.199805434448066 -2.030282067906714 2.323331181728761 2.748456219538546 -1.035850330382864 -0.001710268767030065 0.5386403116932543 3.076477432817095 -0.6833778994390762 -2.157324915787616 2.041428478140485 ];

save("-text","config.dat","Parameters");
