
% Input: 1st C-Band section output @ 120 MeV, in share/, [E X Y Z XP YP]
Parameters.input_file = "cband_out_4ps_0.18mm_100k_120MeV_4_laser_heater.dat";

Parameters.beamline_name = "xls_linac_hxr";

% lattice from 120 MeV, in share/
Parameters.lattice_file = "xls_lattice_c_inj_bc1_ln1_bc2_ln2_ln3_tmc_hxr_maxe.tcl";

% 75 pC bunch for all schemes
Parameters.bunch_charge = 75e-12;

% BC1
Parameters.ang1 = 0.06303798657885221; % rad
Parameters.e01 = 0.273643907169348; % GeV

% BC2
Parameters.ang2 = 0.0202001311084575;
Parameters.e02 = 1.121312600296389;

% TMC
Parameters.angt = 0.00261799;
Parameters.e0t = 5.493365;

% LN0
Parameters.grd0c = 0.01582730005638876; % GV/m
Parameters.phs0c = 29.99129955769009; % deg
Parameters.grd0k = 0.02504954968141703;
Parameters.phs0k = 199.3306673792626;

% LN1
Parameters.grd1 = 0.06500021392534151;
Parameters.phs1 = 34.81990313423306;

% LN2
Parameters.grd2 = 0.0650004193788772;
Parameters.phs2 = 11.24138627050971;

% LN3
Parameters.grd3 = 0.06500042893706537;
Parameters.phs3 = 11.13544443190072;

% Final matching quadrupole strengths
Parameters.FMQS = [ -1.379173665272238 -1.922103622755995 1.846059089085289 2.673235334477802 -1.084233145048299 -0.6173075975980202 1.073717640406879 1.531738808591204 -1.316823291138193 -0.9296739582835474 1.153981896976739 ];

save("-text","config.dat","Parameters");
