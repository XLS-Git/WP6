
% Input: 1st C-Band section output @ 120 MeV, in share/, [E X Y Z XP YP]
Parameters.input_file = "cband_out_4ps_0.18mm_100k_120MeV_4_laser_heater.dat";

Parameters.beamline_name = "xls_linac_hxr";

% lattice from 120 MeV, in share/
Parameters.lattice_file = "xls_lattice_c_inj_bc1_ln1_bc2_ln2_ln3_tmc_hxr_maxe.tcl";

% 75 pC bunch for all schemes
Parameters.bunch_charge = 75e-12;

% BC1
Parameters.ang1 = 0.0870134; % rad
Parameters.e01 = 0.33522556; % GeV

% BC2
Parameters.ang2 = 0.0288147;
Parameters.e02 = 1.19333104;

% TMC
Parameters.angt = 0.002620;
Parameters.e0t = 5.50;

% LN0
Parameters.grd0c = 0.021801; % GV/m
Parameters.phs0c = 28.940521; % deg
Parameters.grd0k = 0.0363006;
Parameters.phs0k = 235.36545;

% LN1
Parameters.grd1 = 0.064511;
Parameters.phs1 = 41.343923;

% LN2
Parameters.grd2 = 0.0645011;
Parameters.phs2 = 18.35318912;

% LN3
Parameters.grd3 = 0.064497784;
Parameters.phs3 = 19.7216321;

% Final matching quadrupole strengths
Parameters.FMQS = [ -1.179115 -1.329514 1.305924 3.246051 -1.001765 -1.233714 3.413867 0.431191 -1.217875 -1.643872 3.009140 ];

save("-text","config.dat","Parameters");

