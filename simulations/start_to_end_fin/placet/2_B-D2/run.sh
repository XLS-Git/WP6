#!/bin/bash

WD=$(pwd)

Options=(
  #nominal_nolh_avni
  #nominal_avni
  #nominal_Nov12
  nominal_Nov13
  ##
  #optimise_lat_Nov13
  #optimise_quad_Nov13
  #optimise_Nov13
  ##
  #incoh_1 ## cg: 2%, grd: 0.10%, phs: 0.05 deg 
  #incoh_2 ## cg: 2%, grd: 0.04%, phs: 0.05 deg
  #incoh_3 ## cg: 2%, grd: 0.04%, phs: 0.05 deg, dt_injection: 75 fs
)

for option in ${Options[*]}
do

  cd $WD

  mkdir -pv $WD/Outputs/${option}
  rm 	-rf $WD/Outputs/${option}/*
  cd	$WD/Outputs/${option}
  
  # optimise
  if [[ $option == optimise* ]];then

#     octave $WD/scr/${option}.m $WD $option 2>&1|grep -v 'Entering CSR bend'|tee log
    octave $WD/scr/${option}.m $WD $option >/dev/tty

  # track
  else
    octave $WD/config/${option}.m >/dev/tty
#     placet $WD/scr/track.tcl $WD $option 2>&1|grep -v 'Entering CSR bend'|tee log.track
    placet $WD/scr/track.tcl $WD $option >/dev/tty
  fi

  cd $WD

done

