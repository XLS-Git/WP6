## constants
set clight 299792458.0
set pi [expr acos(-1.)]
set Z0 [expr 120*$pi]
set eC 1.6021766e-19
set mc2 0.510998910e-3

# load related octave and tcl scripts

source $share_dir/structure_parameters.tcl

Octave {
    scriptdir="$share_dir/";
    source([scriptdir,"generate_bunch_apr_18.m"]);
    source([scriptdir,"tools_apr_18.m"]);
    source([scriptdir,"wake_init.m"]);
}

source $share_dir/wake_init.tcl
source $share_dir/tools.tcl
source $share_dir/make_beam_apr_18.tcl


#########################################################################################
# change font color
#----------------------------------------------------------------------------------------
proc color {foreground text} {
    return [exec tput setaf $foreground] $text[exec tput sgr0]
}
