Girder
SetReferenceEnergy  $e0
Drift      -name    "LN0_WA_IN2" -length 0 
Drift      -name    "LN0_DR_V1" -length 0.200000001479 
Drift      -name    "LN0_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN0_QD_V01"  -length 0.04  -strength [expr 0.04*$e0*-9.34662563905] -e0 $e0
Dipole     -name    "LN0_COR" -length 0 
Bpm        -name    "LN0_BPM" -length 0 
Quadrupole -name    "LN0_QD_V01"  -length 0.04  -strength [expr 0.04*$e0*-9.34662563905] -e0 $e0
Drift      -name    "LN0_DR_QD_ED" -length 0.0425 
Drift      -name    "LN0_DR_V2" -length 0.357305438201 
Drift      -name    "LN0_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN0_QD_V02"  -length 0.04  -strength [expr 0.04*$e0*10.2329605971] -e0 $e0
Dipole     -name    "LN0_COR" -length 0 
Bpm        -name    "LN0_BPM" -length 0 
Quadrupole -name    "LN0_QD_V02"  -length 0.04  -strength [expr 0.04*$e0*10.2329605971] -e0 $e0
Drift      -name    "LN0_DR_QD_ED" -length 0.0425 
Drift      -name    "LN0_DR_V3" -length 0.997243110848 
Drift      -name    "LN0_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN0_QD_V03"  -length 0.04  -strength [expr 0.04*$e0*1.29803854766] -e0 $e0
Dipole     -name    "LN0_COR" -length 0 
Bpm        -name    "LN0_BPM" -length 0 
Quadrupole -name    "LN0_QD_V03"  -length 0.04  -strength [expr 0.04*$e0*1.29803854766] -e0 $e0
Drift      -name    "LN0_DR_QD_ED" -length 0.0425 
Drift      -name    "LN0_DR_V4" -length 1.42143952541 
Drift      -name    "LN0_DR_20" -length 0.2 
Sbend      -name    "LN0_DP_DIP1" -length 0.1  -angle -0.08726646  -E1 0 -E2 -0.08726646 -hgap 0 -fint 0.5 -tilt 0 -e0 $e0 -six_dim 1 
Drift      -name    "LN0_DR_20_C" -length 0.2 
Sbend      -name    "LN0_DP_DIP2" -length 0.1  -angle 0.08726646  -E1 0.08726646 -E2 0 -hgap 0 -fint 0.5 -tilt 0 -e0 $e0 -six_dim 1 
Drift      -name    "LN0_DR_20_C" -length 0.2 
Bpm        -name    "LN0_BPM_LH" -length 0 
Drift      -name    "LN0_DR_20" -length 0.2 
Drift      -name    "LN0_UN_UND" -length 0.24 
Drift      -name    "LN0_DR_20" -length 0.2 
Bpm        -name    "LN0_BPM_LH" -length 0 
Drift      -name    "LN0_DR_VS" -length 0.1 
Drift      -name    "LN0_DR_10" -length 0.1 
Sbend      -name    "LN0_DP_DIP3" -length 0.1  -angle 0.08726646  -E1 0 -E2 0.08726646 -hgap 0 -fint 0.5 -tilt 0 -e0 $e0 -six_dim 1 
Drift      -name    "LN0_DR_20_C" -length 0.2 
Sbend      -name    "LN0_DP_DIP4" -length 0.1  -angle -0.08726646  -E1 -0.08726646 -E2 0 -hgap 0 -fint 0.5 -tilt 0 -e0 $e0 -six_dim 1 
Drift      -name    "LN0_DR_20_C" -length 0.2 
Drift      -name    "LN0_DR_VS" -length 0.1 
Drift      -name    "LN0_DR_V5" -length 0.663943311863 
Drift      -name    "LN0_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN0_QD_V04"  -length 0.04  -strength [expr 0.04*$e0*4.08755073927] -e0 $e0
Dipole     -name    "LN0_COR" -length 0 
Bpm        -name    "LN0_BPM" -length 0 
Quadrupole -name    "LN0_QD_V04"  -length 0.04  -strength [expr 0.04*$e0*4.08755073927] -e0 $e0
Drift      -name    "LN0_DR_QD_ED" -length 0.0425 
Drift      -name    "LN0_DR_V6" -length 0.259892892724 
Drift      -name    "LN0_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN0_QD_V05"  -length 0.04  -strength [expr 0.04*$e0*6.41862575683] -e0 $e0
Dipole     -name    "LN0_COR" -length 0 
Bpm        -name    "LN0_BPM" -length 0 
Quadrupole -name    "LN0_QD_V05"  -length 0.04  -strength [expr 0.04*$e0*6.41862575683] -e0 $e0
Drift      -name    "LN0_DR_QD_ED" -length 0.0425 
Drift      -name    "LN0_DR_V7" -length 1.41975054217 
Drift      -name    "LN0_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN0_QD_V06"  -length 0.04  -strength [expr 0.04*$e0*-7.02375048761] -e0 $e0
Dipole     -name    "LN0_COR" -length 0 
Bpm        -name    "LN0_BPM" -length 0 
Quadrupole -name    "LN0_QD_V06"  -length 0.04  -strength [expr 0.04*$e0*-7.02375048761] -e0 $e0
Drift      -name    "LN0_DR_QD_ED" -length 0.0425 
Drift      -name    "LN0_DR_V8" -length 0.200000760223 
Drift      -name    "LN0_DR_VS" -length 0.1 
Drift      -name    "LN0_DR_BL" -length 0.05 
Drift      -name    "LN0_DR_XCA_ED_DI" -length 0.1001043 
CrabCavity -name    "LN0_XCA0_DI" -length 0.9997914  -voltage $LN0_XCA_VLT -phase $LN0_XCA_PHS -frequency 11.9942
#puts $LN0_XCA_PHS
#Octave {
#  name = "LN0_XCA0_DI";
#  i_name = 1;
#  nn = placet_get_name_number_list("$beamlinename", name)(i_name);
#  vlt = placet_element_get_attribute("$beamlinename", nn, "voltage");
#  phs = placet_element_get_attribute("$beamlinename", nn, "phase");
#  disp("INFO:: Resetting in lattice ...");
#  Tcl_SetVar("LN0_XCA_VLT", vlt);
#  Tcl_SetVar("LN0_XCA_PHS", phs);
#}
#puts $LN0_XCA_PHS
set e0      [expr $e0 + $LN0_XCA_VLT*sin(3.14159265359*$LN0_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN0_DR_XCA_ED_DI" -length 0.1001043 
Drift      -name    "LN0_DR_BL" -length 0.05 
Drift      -name    "LN0_TDS_DR" -length 0.25 
Drift      -name    "LN0_TDS_DR" -length 0.25 
Drift      -name    "LN0_TDS_DR" -length 0.25 
Drift      -name    "LN0_TDS_DR" -length 0.25 
Drift      -name    "LN0_TDS_DR" -length 0.25 
Drift      -name    "LN0_TDS_DR" -length 0.25 
Drift      -name    "LN0_TDS_DR" -length 0.25 
Drift      -name    "LN0_TDS_DR" -length 0.25 
Sbend      -name    "LN0_DP_TDS" -length 0.1  -angle 0  -E1 0 -E2 0 -hgap 0 -fint 0.5 -tilt 0 -e0 $e0 -six_dim 1 
Drift      -name    "LN0_DR_20" -length 0.2 
Drift      -name    "LN0_DR_VS" -length 0.1 
Drift      -name    "LN0_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN0_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*7] -e0 $e0
Dipole     -name    "LN0_COR" -length 0 
Bpm        -name    "LN0_BPM" -length 0 
Quadrupole -name    "LN0_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*7] -e0 $e0
Drift      -name    "LN0_DR_QD_ED" -length 0.0425 
Drift      -name    "LN0_DR_SV" -length 0.1 
Drift      -name    "LN0_DR_BL" -length 0.05 
Drift      -name    "LN0_DR_CCA_ED" -length 0.050198 
Cavity     -name    "LN0_CCA0" -length 1.899604  -gradient $LN0_CCA_GRD -phase $LN0_CCA_PHS -frequency 5.9971
#puts "DEBUG 1 ..."
#puts $LN0_CCA_GRD
#Octave {
#  name = "LN0_CCA0";
#  i_name = 1;
#  nn = placet_get_name_number_list("$beamlinename", name)(i_name);
#  grd = placet_element_get_attribute("$beamlinename", nn, "gradient");
#  phs = placet_element_get_attribute("$beamlinename", nn, "phase");
#  disp("INFO:: Resetting in lattice ...");
#  Tcl_SetVar("LN0_CCA_GRD", grd);
#  Tcl_SetVar("LN0_CCA_PHS", phs);
#}
#puts $LN0_CCA_GRD
#puts "DEBUG 2 ..."
set e0      [expr $e0 + 1.899604*$LN0_CCA_GRD*cos(3.14159265359*$LN0_CCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN0_DR_CCA_ED" -length 0.050198 
Drift      -name    "LN0_DR_BL" -length 0.05 
Drift      -name    "LN0_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN0_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-7] -e0 $e0
Dipole     -name    "LN0_COR" -length 0 
Bpm        -name    "LN0_BPM" -length 0 
Quadrupole -name    "LN0_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-7] -e0 $e0
Drift      -name    "LN0_DR_QD_ED" -length 0.0425 
Drift      -name    "LN0_DR_VS" -length 0.1 
Drift      -name    "LN0_DR_BL" -length 0.05 
Drift      -name    "LN0_DR_CCA_ED" -length 0.050198 
Cavity     -name    "LN0_CCA0" -length 1.899604  -gradient $LN0_CCA_GRD -phase $LN0_CCA_PHS -frequency 5.9971
set e0      [expr $e0 + 1.899604*$LN0_CCA_GRD*cos(3.14159265359*$LN0_CCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN0_DR_CCA_ED" -length 0.050198 
Drift      -name    "LN0_DR_BL" -length 0.05 
Drift      -name    "LN0_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN0_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*7] -e0 $e0
Dipole     -name    "LN0_COR" -length 0 
Bpm        -name    "LN0_BPM" -length 0 
Quadrupole -name    "LN0_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*7] -e0 $e0
Drift      -name    "LN0_DR_QD_ED" -length 0.0425 
Drift      -name    "LN0_DR_VS" -length 0.1 
Drift      -name    "LN0_DR_BL" -length 0.05 
Drift      -name    "LN0_DR_CCA_ED" -length 0.050198 
Cavity     -name    "LN0_CCA0" -length 1.899604  -gradient $LN0_CCA_GRD -phase $LN0_CCA_PHS -frequency 5.9971
set e0      [expr $e0 + 1.899604*$LN0_CCA_GRD*cos(3.14159265359*$LN0_CCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN0_DR_CCA_ED" -length 0.050198 
Drift      -name    "LN0_DR_BL" -length 0.05 
Drift      -name    "LN0_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN0_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-7] -e0 $e0
Dipole     -name    "LN0_COR" -length 0 
Bpm        -name    "LN0_BPM" -length 0 
Quadrupole -name    "LN0_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-7] -e0 $e0
Drift      -name    "LN0_DR_QD_ED" -length 0.0425 
Drift      -name    "LN0_DR_VS" -length 0.1 
Drift      -name    "LN0_DR_BL" -length 0.05 
Drift      -name    "LN0_DR_CCA_ED" -length 0.050198 
Cavity     -name    "LN0_CCA0" -length 1.899604  -gradient $LN0_CCA_GRD -phase $LN0_CCA_PHS -frequency 5.9971
set e0      [expr $e0 + 1.899604*$LN0_CCA_GRD*cos(3.14159265359*$LN0_CCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN0_DR_CCA_ED" -length 0.050198 
Drift      -name    "LN0_DR_BL" -length 0.05 
Drift      -name    "LN0_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN0_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*7] -e0 $e0
Dipole     -name    "LN0_COR" -length 0 
Bpm        -name    "LN0_BPM" -length 0 
Quadrupole -name    "LN0_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*7] -e0 $e0
Drift      -name    "LN0_DR_QD_ED" -length 0.0425 
Drift      -name    "LN0_DR_VS" -length 0.1 
Drift      -name    "LN0_DR_BL" -length 0.05 
Drift      -name    "LN0_DR_CCA_ED" -length 0.050198 
Cavity     -name    "LN0_CCA0" -length 1.899604  -gradient $LN0_CCA_GRD -phase $LN0_CCA_PHS -frequency 5.9971
set e0      [expr $e0 + 1.899604*$LN0_CCA_GRD*cos(3.14159265359*$LN0_CCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN0_DR_CCA_ED" -length 0.050198 
Drift      -name    "LN0_DR_BL" -length 0.05 
Drift      -name    "LN0_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN0_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-7] -e0 $e0
Dipole     -name    "LN0_COR" -length 0 
Bpm        -name    "LN0_BPM" -length 0 
Quadrupole -name    "LN0_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-7] -e0 $e0
Drift      -name    "LN0_DR_QD_ED" -length 0.0425 
Drift      -name    "LN0_DR_VS" -length 0.1 
Drift      -name    "LN0_DR_BL" -length 0.05 
Drift      -name    "LN0_DR_CCA_ED" -length 0.050198 
Cavity     -name    "LN0_CCA0" -length 1.899604  -gradient $LN0_CCA_GRD -phase $LN0_CCA_PHS -frequency 5.9971
set e0      [expr $e0 + 1.899604*$LN0_CCA_GRD*cos(3.14159265359*$LN0_CCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN0_DR_CCA_ED" -length 0.050198 
Drift      -name    "LN0_DR_BL" -length 0.05 
Drift      -name    "LN0_WA_LNZ_IN2" -length 0 
Drift      -name    "LN0_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN0_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*7] -e0 $e0
Dipole     -name    "LN0_COR" -length 0 
Bpm        -name    "LN0_BPM" -length 0 
Quadrupole -name    "LN0_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*7] -e0 $e0
Drift      -name    "LN0_DR_QD_ED" -length 0.0425 
Drift      -name    "LN0_DR_VS" -length 0.1 
Drift      -name    "LN0_DR_LNZ_ED2" -length 0.625 
Drift      -name    "LN0_DR_BL" -length 0.05 
Drift      -name    "LN0_DR_KCA_ED" -length 0.0222541 
Cavity     -name    "LN0_KCA0" -length 0.3054918  -gradient $LN0_KCA_GRD -phase $LN0_KCA_PHS -frequency 35.9826
set e0      [expr $e0 + 0.3054918*$LN0_KCA_GRD*cos(3.14159265359*$LN0_KCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN0_DR_KCA_ED" -length 0.0222541 
Drift      -name    "LN0_DR_BL" -length 0.05 
Drift      -name    "LN0_DR_KCA_ED" -length 0.0222541 
Drift      -name    "LN0_DR_KCA0" -length 0.3054918 
Drift      -name    "LN0_DR_KCA_ED" -length 0.0222541 
Drift      -name    "LN0_DR_BL" -length 0.05 
Drift      -name    "LN0_DR_LNZ_ED2" -length 0.625 
Drift      -name    "LN0_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN0_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-7] -e0 $e0
Dipole     -name    "LN0_COR" -length 0 
Bpm        -name    "LN0_BPM" -length 0 
Quadrupole -name    "LN0_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-7] -e0 $e0
Drift      -name    "LN0_DR_QD_ED" -length 0.0425 
Drift      -name    "LN0_DR_DR" -length 0.1 
Drift      -name    "LN0_DR_BL" -length 0.05 
Drift      -name    "LN0_DR_CCA_ED" -length 0.050198 
Drift      -name    "LN0_DR_CCA0H" -length 0.349802 
Drift      -name    "LN0_DR_A" -length 0.2 
Drift      -name    "LN0_DR_VS" -length 0.1 
Drift      -name    "LN0_DR_A" -length 0.2 
Drift      -name    "LN0_DR_CT" -length 0.1 
Drift      -name    "LN0_WA_OU2" -length 0 
Drift      -name    "BC1_DR_V1" -length 0.538206671373 
Drift      -name    "BC1_DR_QD_ED" -length 0.0425 
Quadrupole -name    "BC1_QD_V01"  -length 0.04  -strength [expr 0.04*$e0*1.83741033642] -e0 $e0
Dipole     -name    "BC1_COR" -length 0 
Bpm        -name    "BC1_BPM" -length 0 
Quadrupole -name    "BC1_QD_V01"  -length 0.04  -strength [expr 0.04*$e0*1.83741033642] -e0 $e0
Drift      -name    "BC1_DR_QD_ED" -length 0.0425 
Drift      -name    "BC1_DR_V2" -length 5.90034025961 
Drift      -name    "BC1_DR_QD_ED" -length 0.0425 
Quadrupole -name    "BC1_QD_V02"  -length 0.04  -strength [expr 0.04*$e0*-9.14633718755] -e0 $e0
Dipole     -name    "BC1_COR" -length 0 
Bpm        -name    "BC1_BPM" -length 0 
Quadrupole -name    "BC1_QD_V02"  -length 0.04  -strength [expr 0.04*$e0*-9.14633718755] -e0 $e0
Drift      -name    "BC1_DR_QD_ED" -length 0.0425 
Drift      -name    "BC1_DR_V3" -length 0.249612802881 
Drift      -name    "BC1_DR_QD_ED" -length 0.0425 
Quadrupole -name    "BC1_QD_V03"  -length 0.04  -strength [expr 0.04*$e0*3.21483971534] -e0 $e0
Dipole     -name    "BC1_COR" -length 0 
Bpm        -name    "BC1_BPM" -length 0 
Quadrupole -name    "BC1_QD_V03"  -length 0.04  -strength [expr 0.04*$e0*3.21483971534] -e0 $e0
Drift      -name    "BC1_DR_QD_ED" -length 0.0425 
Drift      -name    "BC1_DR_V4" -length 0.200000016045 
Drift      -name    "BC1_DR_QD_ED" -length 0.0425 
Quadrupole -name    "BC1_QD_V04"  -length 0.04  -strength [expr 0.04*$e0*5.28363690536] -e0 $e0
Dipole     -name    "BC1_COR" -length 0 
Bpm        -name    "BC1_BPM" -length 0 
Quadrupole -name    "BC1_QD_V04"  -length 0.04  -strength [expr 0.04*$e0*5.28363690536] -e0 $e0
Drift      -name    "BC1_DR_QD_ED" -length 0.0425 
Drift      -name    "BC1_DR_V5" -length 0.200203972325 
Drift      -name    "BC1_DR_20" -length 0.5 
Sbend      -name    "BC1_DP_DIP1" -length 0.50031199  -angle -0.06117379  -E1 0 -E2 -0.06117379 -hgap 0 -fint 0.5 -tilt 0 -e0 $e0 -six_dim 1 
Drift      -name    "BC1_DR_SIDE_C" -length 0.5 
Drift      -name    "BC1_DR_SIDE" -length 2.75609062 
Sbend      -name    "BC1_DP_DIP2" -length 0.50031199  -angle 0.06117379  -E1 0.06117379 -E2 0 -hgap 0 -fint 0.5 -tilt 0 -e0 $e0 -six_dim 1 
Drift      -name    "BC1_DR_CENT" -length 0.35 
Bpm        -name    "BC1_BPM" -length 0 
Drift      -name    "BC1_DR_CENT" -length 0.35 
Sbend      -name    "BC1_DP_DIP3" -length 0.50031199  -angle 0.06117379  -E1 0 -E2 0.06117379 -hgap 0 -fint 0.5 -tilt 0 -e0 $e0 -six_dim 1 
Drift      -name    "BC1_DR_SIDE_C" -length 0.5 
Drift      -name    "BC1_DR_SIDE" -length 2.75609062 
Sbend      -name    "BC1_DP_DIP4" -length 0.50031199  -angle -0.06117379  -E1 -0.06117379 -E2 0 -hgap 0 -fint 0.5 -tilt 0 -e0 $e0 -six_dim 1 
Drift      -name    "BC1_DR_20_C" -length 0.5 
Drift      -name    "BC1_WA_OU2" -length 0 

Drift      -name    "BC1_DR_CT" -length 0.1 
Drift      -name    "BC1_DR_DI_V1" -length 0.291167237663 
Drift      -name    "BC1_DR_QD_ED" -length 0.0425 
Quadrupole -name    "BC1_QD_DI_V01"  -length 0.04  -strength [expr 0.04*$e0*-8.54917382445] -e0 $e0
Dipole     -name    "BC1_COR_DI" -length 0 
Bpm        -name    "BC1_BPM_DI" -length 0 
Quadrupole -name    "BC1_QD_DI_V01"  -length 0.04  -strength [expr 0.04*$e0*-8.54917382445] -e0 $e0
Drift      -name    "BC1_DR_QD_ED" -length 0.0425 
Drift      -name    "BC1_DR_DI_V2" -length 0.470002000765 
Drift      -name    "BC1_DR_QD_ED" -length 0.0425 
Quadrupole -name    "BC1_QD_DI_V02"  -length 0.04  -strength [expr 0.04*$e0*13.2887945573] -e0 $e0
Dipole     -name    "BC1_COR_DI" -length 0 
Bpm        -name    "BC1_BPM_DI" -length 0 
Quadrupole -name    "BC1_QD_DI_V02"  -length 0.04  -strength [expr 0.04*$e0*13.2887945573] -e0 $e0
Drift      -name    "BC1_DR_QD_ED" -length 0.0425 
Drift      -name    "BC1_DR_DI_V3" -length 1.19908280027 
Drift      -name    "BC1_DR_QD_ED" -length 0.0425 
Quadrupole -name    "BC1_QD_DI_V03"  -length 0.04  -strength [expr 0.04*$e0*-5.03687825988] -e0 $e0
Dipole     -name    "BC1_COR_DI" -length 0 
Bpm        -name    "BC1_BPM_DI" -length 0 
Quadrupole -name    "BC1_QD_DI_V03"  -length 0.04  -strength [expr 0.04*$e0*-5.03687825988] -e0 $e0
Drift      -name    "BC1_DR_QD_ED" -length 0.0425 
Drift      -name    "BC1_DR_DI_V5" -length 0.2 
Drift      -name    "BC1_DR_DI" -length 0.25 
Drift      -name    "BC1_DR_VS" -length 0.1 
Drift      -name    "BC1_DR_BL" -length 0.05 
Drift      -name    "BC1_DR_XCA_ED_DI" -length 0.1001043 
CrabCavity -name    "BC1_XCA0_DI" -length 0.9997914  -voltage $BC1_XCA_VLT -phase $BC1_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + $BC1_XCA_VLT*sin(3.14159265359*$BC1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "BC1_DR_XCA_ED_DI" -length 0.1001043 
Drift      -name    "BC1_DR_BL" -length 0.05 
Drift      -name    "BC1_DR_VS" -length 0.1 
Drift      -name    "BC1_DR_DI" -length 0.25 
Drift      -name    "BC1_DR_DI" -length 0.25 
Drift      -name    "BC1_DR_DI_QD_ED" -length 0.0425 
Quadrupole -name    "BC1_QD_DI_FH"  -length 0.04  -strength [expr 0.04*$e0*8] -e0 $e0
Dipole     -name    "BC1_COR_DI" -length 0 
Bpm        -name    "BC1_BPM_DI" -length 0 
Quadrupole -name    "BC1_QD_DI_FH"  -length 0.04  -strength [expr 0.04*$e0*8] -e0 $e0
Drift      -name    "BC1_DR_DI_QD_ED" -length 0.0425 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_DI_VS" -length 0.1 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_DI_QD_ED" -length 0.0425 
Quadrupole -name    "BC1_QD_DI_DH"  -length 0.04  -strength [expr 0.04*$e0*-8] -e0 $e0
Dipole     -name    "BC1_COR_DI" -length 0 
Bpm        -name    "BC1_BPM_DI" -length 0 
Quadrupole -name    "BC1_QD_DI_DH"  -length 0.04  -strength [expr 0.04*$e0*-8] -e0 $e0
Drift      -name    "BC1_DR_DI_QD_ED" -length 0.0425 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_DI_VS" -length 0.1 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_DI_QD_ED" -length 0.0425 
Quadrupole -name    "BC1_QD_DI_FH"  -length 0.04  -strength [expr 0.04*$e0*8] -e0 $e0
Dipole     -name    "BC1_COR_DI" -length 0 
Bpm        -name    "BC1_BPM_DI" -length 0 
Quadrupole -name    "BC1_QD_DI_FH"  -length 0.04  -strength [expr 0.04*$e0*8] -e0 $e0
Drift      -name    "BC1_DR_DI_QD_ED" -length 0.0425 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_DI_VS" -length 0.1 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_DI_QD_ED" -length 0.0425 
Quadrupole -name    "BC1_QD_DI_DH"  -length 0.04  -strength [expr 0.04*$e0*-8] -e0 $e0
Dipole     -name    "BC1_COR_DI" -length 0 
Bpm        -name    "BC1_BPM_DI" -length 0 
Quadrupole -name    "BC1_QD_DI_DH"  -length 0.04  -strength [expr 0.04*$e0*-8] -e0 $e0
Drift      -name    "BC1_DR_DI_QD_ED" -length 0.0425 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_DI_VS" -length 0.1 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_FODO" -length 0.15 
Drift      -name    "BC1_DR_DI_QD_ED" -length 0.0425 
Quadrupole -name    "BC1_QD_DI_FH"  -length 0.04  -strength [expr 0.04*$e0*8] -e0 $e0
Dipole     -name    "BC1_COR_DI" -length 0 
Bpm        -name    "BC1_BPM_DI" -length 0 
Quadrupole -name    "BC1_QD_DI_FH"  -length 0.04  -strength [expr 0.04*$e0*8] -e0 $e0
Drift      -name    "BC1_DR_DI_QD_ED" -length 0.0425 
Drift      -name    "BC1_DR_DI" -length 0.25 
Sbend      -name    "BC1_DP_DI" -length 0.25  -angle 0  -E1 0 -E2 0 -hgap 0 -fint 0.5 -tilt 0 -e0 $e0 -six_dim 1 
Drift      -name    "BC1_DR_DI" -length 0.25 
Drift      -name    "BC1_DR_DI" -length 0.25 
Drift      -name    "BC1_DR_DI" -length 0.25 
Drift      -name    "BC1_WA_OU_DI2" -length 0 

Drift      -name    "LN1_DR_V1" -length 0.20052997193 
Drift      -name    "LN1_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN1_QD_V01"  -length 0.04  -strength [expr 0.04*$e0*5.28922214629] -e0 $e0
Dipole     -name    "LN1_COR" -length 0 
Bpm        -name    "LN1_BPM" -length 0 
Quadrupole -name    "LN1_QD_V01"  -length 0.04  -strength [expr 0.04*$e0*5.28922214629] -e0 $e0
Drift      -name    "LN1_DR_QD_ED" -length 0.0425 
Drift      -name    "LN1_DR_V2" -length 0.219250433995 
Drift      -name    "LN1_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN1_QD_V02"  -length 0.04  -strength [expr 0.04*$e0*-10.8616577789] -e0 $e0
Dipole     -name    "LN1_COR" -length 0 
Bpm        -name    "LN1_BPM" -length 0 
Quadrupole -name    "LN1_QD_V02"  -length 0.04  -strength [expr 0.04*$e0*-10.8616577789] -e0 $e0
Drift      -name    "LN1_DR_QD_ED" -length 0.0425 
Drift      -name    "LN1_DR_V3" -length 1.91461777963 
Drift      -name    "LN1_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN1_QD_V03"  -length 0.04  -strength [expr 0.04*$e0*1.11737511691] -e0 $e0
Dipole     -name    "LN1_COR" -length 0 
Bpm        -name    "LN1_BPM" -length 0 
Quadrupole -name    "LN1_QD_V03"  -length 0.04  -strength [expr 0.04*$e0*1.11737511691] -e0 $e0
Drift      -name    "LN1_DR_QD_ED" -length 0.0425 
Drift      -name    "LN1_DR_V4" -length 0.2 
Drift      -name    "LN1_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN1_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*6.6] -e0 $e0
Dipole     -name    "LN1_COR" -length 0 
Bpm        -name    "LN1_BPM" -length 0 
Quadrupole -name    "LN1_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*6.6] -e0 $e0
Drift      -name    "LN1_DR_QD_ED" -length 0.0425 
Drift      -name    "LN1_DR_SV" -length 0.1 
Drift      -name    "LN1_DR_BL" -length 0.05 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_BL" -length 0.05 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_BL" -length 0.05 
Drift      -name    "LN1_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN1_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-6.6] -e0 $e0
Dipole     -name    "LN1_COR" -length 0 
Bpm        -name    "LN1_BPM" -length 0 
Quadrupole -name    "LN1_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-6.6] -e0 $e0
Drift      -name    "LN1_DR_QD_ED" -length 0.0425 
Drift      -name    "LN1_DR_VS" -length 0.1 
Drift      -name    "LN1_DR_BL" -length 0.05 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_BL" -length 0.05 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_BL" -length 0.05 
Drift      -name    "LN1_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN1_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*6.6] -e0 $e0
Dipole     -name    "LN1_COR" -length 0 
Bpm        -name    "LN1_BPM" -length 0 
Quadrupole -name    "LN1_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*6.6] -e0 $e0
Drift      -name    "LN1_DR_QD_ED" -length 0.0425 
Drift      -name    "LN1_DR_SV" -length 0.1 
Drift      -name    "LN1_DR_BL" -length 0.05 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_BL" -length 0.05 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_BL" -length 0.05 
Drift      -name    "LN1_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN1_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-6.6] -e0 $e0
Dipole     -name    "LN1_COR" -length 0 
Bpm        -name    "LN1_BPM" -length 0 
Quadrupole -name    "LN1_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-6.6] -e0 $e0
Drift      -name    "LN1_DR_QD_ED" -length 0.0425 
Drift      -name    "LN1_DR_VS" -length 0.1 
Drift      -name    "LN1_DR_BL" -length 0.05 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_BL" -length 0.05 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_BL" -length 0.05 
Drift      -name    "LN1_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN1_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*6.6] -e0 $e0
Dipole     -name    "LN1_COR" -length 0 
Bpm        -name    "LN1_BPM" -length 0 
Quadrupole -name    "LN1_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*6.6] -e0 $e0
Drift      -name    "LN1_DR_QD_ED" -length 0.0425 
Drift      -name    "LN1_DR_SV" -length 0.1 
Drift      -name    "LN1_DR_BL" -length 0.05 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_BL" -length 0.05 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_BL" -length 0.05 
Drift      -name    "LN1_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN1_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-6.6] -e0 $e0
Dipole     -name    "LN1_COR" -length 0 
Bpm        -name    "LN1_BPM" -length 0 
Quadrupole -name    "LN1_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-6.6] -e0 $e0
Drift      -name    "LN1_DR_QD_ED" -length 0.0425 
Drift      -name    "LN1_DR_VS" -length 0.1 
Drift      -name    "LN1_DR_BL" -length 0.05 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_BL" -length 0.05 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_BL" -length 0.05 
Drift      -name    "LN1_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN1_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*6.6] -e0 $e0
Dipole     -name    "LN1_COR" -length 0 
Bpm        -name    "LN1_BPM" -length 0 
Quadrupole -name    "LN1_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*6.6] -e0 $e0
Drift      -name    "LN1_DR_QD_ED" -length 0.0425 
Drift      -name    "LN1_DR_SV" -length 0.1 
Drift      -name    "LN1_DR_BL" -length 0.05 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_BL" -length 0.05 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_BL" -length 0.05 
Drift      -name    "LN1_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN1_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-6.6] -e0 $e0
Dipole     -name    "LN1_COR" -length 0 
Bpm        -name    "LN1_BPM" -length 0 
Quadrupole -name    "LN1_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-6.6] -e0 $e0
Drift      -name    "LN1_DR_QD_ED" -length 0.0425 
Drift      -name    "LN1_DR_VS" -length 0.1 
Drift      -name    "LN1_DR_BL" -length 0.05 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_BL" -length 0.05 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN1_XCA0" -length 0.9164755  -gradient $LN1_XCA_GRD -phase $LN1_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN1_XCA_GRD*cos(3.14159265359*$LN1_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_BL" -length 0.05 
Drift      -name    "LN1_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN1_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*6.6] -e0 $e0
Dipole     -name    "LN1_COR" -length 0 
Bpm        -name    "LN1_BPM" -length 0 
Quadrupole -name    "LN1_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*6.6] -e0 $e0
Drift      -name    "LN1_DR_QD_ED" -length 0.0425 
Drift      -name    "LN1_DR_DR" -length 0.1 
Drift      -name    "LN1_DR_BL" -length 0.05 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_XCA_" -length 0.9164755 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_BL" -length 0.05 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_XCA_" -length 0.9164755 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_BL" -length 0.05 
Drift      -name    "LN1_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN1_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-6.6] -e0 $e0
Dipole     -name    "LN1_COR" -length 0 
Bpm        -name    "LN1_BPM" -length 0 
Quadrupole -name    "LN1_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-6.6] -e0 $e0
Drift      -name    "LN1_DR_QD_ED" -length 0.0425 
Drift      -name    "LN1_DR_DR" -length 0.1 
Drift      -name    "LN1_DR_BL" -length 0.05 
Drift      -name    "LN1_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN1_DR_XCA_A" -length 0.8164755 
Drift      -name    "LN1_DR_VS" -length 0.1 
Drift      -name    "LN1_WA_OU2" -length 0 


Drift      -name    "BC2_DR_V1" -length 0.471866672144 
Drift      -name    "BC2_DR_QD_ED" -length 0.0425 
Quadrupole -name    "BC2_QD_V01"  -length 0.04  -strength [expr 0.04*$e0*0.386038089258] -e0 $e0
Dipole     -name    "BC2_COR" -length 0 
Bpm        -name    "BC2_BPM" -length 0 
Quadrupole -name    "BC2_QD_V01"  -length 0.04  -strength [expr 0.04*$e0*0.386038089258] -e0 $e0
Drift      -name    "BC2_DR_QD_ED" -length 0.0425 
Drift      -name    "BC2_DR_V2" -length 5.87242259478 
Drift      -name    "BC2_DR_QD_ED" -length 0.0425 
Quadrupole -name    "BC2_QD_V02"  -length 0.04  -strength [expr 0.04*$e0*-11.6230895973] -e0 $e0
Dipole     -name    "BC2_COR" -length 0 
Bpm        -name    "BC2_BPM" -length 0 
Quadrupole -name    "BC2_QD_V02"  -length 0.04  -strength [expr 0.04*$e0*-11.6230895973] -e0 $e0
Drift      -name    "BC2_DR_QD_ED" -length 0.0425 
Drift      -name    "BC2_DR_V3" -length 0.200000001011 
Drift      -name    "BC2_DR_QD_ED" -length 0.0425 
Quadrupole -name    "BC2_QD_V03"  -length 0.04  -strength [expr 0.04*$e0*10.7721699578] -e0 $e0
Dipole     -name    "BC2_COR" -length 0 
Bpm        -name    "BC2_BPM" -length 0 
Quadrupole -name    "BC2_QD_V03"  -length 0.04  -strength [expr 0.04*$e0*10.7721699578] -e0 $e0
Drift      -name    "BC2_DR_QD_ED" -length 0.0425 
Drift      -name    "BC2_DR_V4" -length 0.2 
Drift      -name    "BC2_DR_20" -length 0.5 
Sbend      -name    "BC2_DP_DIP1" -length 0.60004549  -angle -0.02132792  -E1 0 -E2 -0.02132792 -hgap 0 -fint 0.5 -tilt 0 -e0 $e0 -six_dim 1 
Drift      -name    "BC2_DR_SIDE_C" -length 0.5 
Drift      -name    "BC2_DR_SIDE" -length 3.20084169 
Sbend      -name    "BC2_DP_DIP2" -length 0.60004549  -angle 0.02132792  -E1 0.02132792 -E2 0 -hgap 0 -fint 0.5 -tilt 0 -e0 $e0 -six_dim 1 
Drift      -name    "BC2_DR_CENT_C" -length 0.25 
Bpm        -name    "BC2_BPM" -length 0 
Drift      -name    "BC2_DR_CENT" -length 0.25 
Sbend      -name    "BC2_DP_DIP3" -length 0.60004549  -angle 0.02132792  -E1 0 -E2 0.02132792 -hgap 0 -fint 0.5 -tilt 0 -e0 $e0 -six_dim 1 
Drift      -name    "BC2_DR_SIDE_C" -length 0.5 
Drift      -name    "BC2_DR_SIDE" -length 3.20084169 
Sbend      -name    "BC2_DP_DIP4" -length 0.60004549  -angle -0.02132792  -E1 -0.02132792 -E2 0 -hgap 0 -fint 0.5 -tilt 0 -e0 $e0 -six_dim 1 
Drift      -name    "BC2_DR_20_C" -length 0.5 
Drift      -name    "BC2_WA_OU2" -length 0 


Drift      -name    "LN2_DR_CT" -length 0.1 
Drift      -name    "LN2_DR_V1" -length 2.29482966931 
Drift      -name    "LN2_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN2_QD_V01"  -length 0.04  -strength [expr 0.04*$e0*13.7853413124] -e0 $e0
Dipole     -name    "LN2_COR" -length 0 
Bpm        -name    "LN2_BPM" -length 0 
Quadrupole -name    "LN2_QD_V01"  -length 0.04  -strength [expr 0.04*$e0*13.7853413124] -e0 $e0
Drift      -name    "LN2_DR_QD_ED" -length 0.0425 
Drift      -name    "LN2_DR_V2" -length 0.509089064773 
Drift      -name    "LN2_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN2_QD_V02"  -length 0.04  -strength [expr 0.04*$e0*-13.1356278969] -e0 $e0
Dipole     -name    "LN2_COR" -length 0 
Bpm        -name    "LN2_BPM" -length 0 
Quadrupole -name    "LN2_QD_V02"  -length 0.04  -strength [expr 0.04*$e0*-13.1356278969] -e0 $e0
Drift      -name    "LN2_DR_QD_ED" -length 0.0425 
Drift      -name    "LN2_DR_V3" -length 1.49046353232 
Drift      -name    "LN2_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN2_QD_V03"  -length 0.04  -strength [expr 0.04*$e0*9.17947179427] -e0 $e0
Dipole     -name    "LN2_COR" -length 0 
Bpm        -name    "LN2_BPM" -length 0 
Quadrupole -name    "LN2_QD_V03"  -length 0.04  -strength [expr 0.04*$e0*9.17947179427] -e0 $e0
Drift      -name    "LN2_DR_QD_ED" -length 0.0425 
Drift      -name    "LN2_DR_V4" -length 2.99993932723 
Drift      -name    "LN2_DR_VS" -length 0.1 
Drift      -name    "LN2_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN2_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*6.7] -e0 $e0
Dipole     -name    "LN2_COR" -length 0 
Bpm        -name    "LN2_BPM" -length 0 
Quadrupole -name    "LN2_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*6.7] -e0 $e0
Drift      -name    "LN2_DR_QD_ED" -length 0.0425 
Drift      -name    "LN2_DR_SV" -length 0.1 
Drift      -name    "LN2_DR_BL" -length 0.05 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_BL" -length 0.05 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_BL" -length 0.05 
Drift      -name    "LN2_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN2_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-6.7] -e0 $e0
Dipole     -name    "LN2_COR" -length 0 
Bpm        -name    "LN2_BPM" -length 0 
Quadrupole -name    "LN2_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-6.7] -e0 $e0
Drift      -name    "LN2_DR_QD_ED" -length 0.0425 
Drift      -name    "LN2_DR_VS" -length 0.1 
Drift      -name    "LN2_DR_BL" -length 0.05 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_BL" -length 0.05 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_BL" -length 0.05 
Drift      -name    "LN2_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN2_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*6.7] -e0 $e0
Dipole     -name    "LN2_COR" -length 0 
Bpm        -name    "LN2_BPM" -length 0 
Quadrupole -name    "LN2_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*6.7] -e0 $e0
Drift      -name    "LN2_DR_QD_ED" -length 0.0425 
Drift      -name    "LN2_DR_SV" -length 0.1 
Drift      -name    "LN2_DR_BL" -length 0.05 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_BL" -length 0.05 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_BL" -length 0.05 
Drift      -name    "LN2_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN2_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-6.7] -e0 $e0
Dipole     -name    "LN2_COR" -length 0 
Bpm        -name    "LN2_BPM" -length 0 
Quadrupole -name    "LN2_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-6.7] -e0 $e0
Drift      -name    "LN2_DR_QD_ED" -length 0.0425 
Drift      -name    "LN2_DR_VS" -length 0.1 
Drift      -name    "LN2_DR_BL" -length 0.05 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_BL" -length 0.05 
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN2_XCA0" -length 0.9164755  -gradient $LN2_XCA_GRD -phase $LN2_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN2_XCA_GRD*cos(3.14159265359*$LN2_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN2_DR_BL" -length 0.05 
Drift      -name    "LN2_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN2_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*6.7] -e0 $e0
Dipole     -name    "LN2_COR" -length 0 
Bpm        -name    "LN2_BPM" -length 0 
Quadrupole -name    "LN2_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*6.7] -e0 $e0
Drift      -name    "LN2_DR_QD_ED" -length 0.0425 
Drift      -name    "LN2_WA_OU2" -length 0 
Drift      -name    "LN2_DR_DI_V1" -length 1.16105229033 
Drift      -name    "LN2_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN2_QD_DI_V01"  -length 0.04  -strength [expr 0.04*$e0*-2.65234226681] -e0 $e0
Dipole     -name    "LN2_COR_DI" -length 0 
Bpm        -name    "LN2_BPM_DI" -length 0 
Quadrupole -name    "LN2_QD_DI_V01"  -length 0.04  -strength [expr 0.04*$e0*-2.65234226681] -e0 $e0
Drift      -name    "LN2_DR_QD_ED" -length 0.0425 
Drift      -name    "LN2_DR_DI_V2" -length 0.827998010248 
Drift      -name    "LN2_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN2_QD_DI_V02"  -length 0.04  -strength [expr 0.04*$e0*10.1741860066] -e0 $e0
Dipole     -name    "LN2_COR_DI" -length 0 
Bpm        -name    "LN2_BPM_DI" -length 0 
Quadrupole -name    "LN2_QD_DI_V02"  -length 0.04  -strength [expr 0.04*$e0*10.1741860066] -e0 $e0
Drift      -name    "LN2_DR_QD_ED" -length 0.0425 
Drift      -name    "LN2_DR_DI_V3" -length 0.200153917065 
Drift      -name    "LN2_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN2_QD_DI_V03"  -length 0.04  -strength [expr 0.04*$e0*-12.4920089825] -e0 $e0
Dipole     -name    "LN2_COR_DI" -length 0 
Bpm        -name    "LN2_BPM_DI" -length 0 
Quadrupole -name    "LN2_QD_DI_V03"  -length 0.04  -strength [expr 0.04*$e0*-12.4920089825] -e0 $e0
Drift      -name    "LN2_DR_QD_ED" -length 0.0425 
Drift      -name    "LN2_DR_DI_V5" -length 0.2 
Drift      -name    "LN2_DR_VS" -length 0.1 
Drift      -name    "LN2_DR_DI" -length 0.25 
Drift      -name    "LN2_DR_VS_DI" -length 0.1 
Drift      -name    "LN2_DR_BL_DI" -length 0.05 
Drift      -name    "LN2_DR_XCA_ED_DI" -length 0.1001043 
CrabCavity -name    "LN2_XCA0_DI" -length 0.9997914  -voltage $LN2_XCA_VLT_DI -phase $LN2_XCA_PHS_DI -frequency 11.9942
set e0      [expr $e0 + $LN2_XCA_VLT_DI*sin(3.14159265359*$LN2_XCA_PHS_DI/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_XCA_ED_DI" -length 0.1001043 
Drift      -name    "LN2_DR_BL_DI" -length 0.05 
Drift      -name    "LN2_DR_DI" -length 0.25 
Drift      -name    "LN2_DR_DI" -length 0.25 
Drift      -name    "LN2_DR_DI_QD_ED" -length 0.0425 
Quadrupole -name    "LN2_QD_DI_FH"  -length 0.04  -strength [expr 0.04*$e0*8] -e0 $e0
Dipole     -name    "LN2_COR_DI" -length 0 
Bpm        -name    "LN2_BPM_DI" -length 0 
Quadrupole -name    "LN2_QD_DI_FH"  -length 0.04  -strength [expr 0.04*$e0*8] -e0 $e0
Drift      -name    "LN2_DR_DI_QD_ED" -length 0.0425 
Drift      -name    "LN2_DR_FODO" -length 0.15 
Drift      -name    "LN2_DR_FODO" -length 0.15 
Drift      -name    "LN2_DR_FODO" -length 0.15 
Drift      -name    "LN2_DR_FODO" -length 0.15 
Drift      -name    "LN2_DR_DI_VS" -length 0.1 
Drift      -name    "LN2_DR_FODO" -length 0.15 
Drift      -name    "LN2_DR_FODO" -length 0.15 
Drift      -name    "LN2_DR_FODO" -length 0.15 
Drift      -name    "LN2_DR_FODO" -length 0.15 
Drift      -name    "LN2_DR_DI_QD_ED" -length 0.0425 
Quadrupole -name    "LN2_QD_DI_DH"  -length 0.04  -strength [expr 0.04*$e0*-8] -e0 $e0
Dipole     -name    "LN2_COR_DI" -length 0 
Bpm        -name    "LN2_BPM_DI" -length 0 
Quadrupole -name    "LN2_QD_DI_DH"  -length 0.04  -strength [expr 0.04*$e0*-8] -e0 $e0
Drift      -name    "LN2_DR_DI_QD_ED" -length 0.0425 
Drift      -name    "LN2_DR_FODO" -length 0.15 
Drift      -name    "LN2_DR_FODO" -length 0.15 
Drift      -name    "LN2_DR_FODO" -length 0.15 
Drift      -name    "LN2_DR_FODO" -length 0.15 
Drift      -name    "LN2_DR_DI_VS" -length 0.1 
Drift      -name    "LN2_DR_FODO" -length 0.15 
Drift      -name    "LN2_DR_FODO" -length 0.15 
Drift      -name    "LN2_DR_FODO" -length 0.15 
Drift      -name    "LN2_DR_FODO" -length 0.15 
Drift      -name    "LN2_DR_DI_QD_ED" -length 0.0425 
Quadrupole -name    "LN2_QD_DI_FH"  -length 0.04  -strength [expr 0.04*$e0*8] -e0 $e0
Dipole     -name    "LN2_COR_DI" -length 0 
Bpm        -name    "LN2_BPM_DI" -length 0 
Quadrupole -name    "LN2_QD_DI_FH"  -length 0.04  -strength [expr 0.04*$e0*8] -e0 $e0
Drift      -name    "LN2_DR_DI_QD_ED" -length 0.0425 
Drift      -name    "LN2_DR_FODO" -length 0.15 
Drift      -name    "LN2_DR_FODO" -length 0.15 
Drift      -name    "LN2_DR_FODO" -length 0.15 
Drift      -name    "LN2_DR_FODO" -length 0.15 
Drift      -name    "LN2_DR_DI_VS" -length 0.1 
Drift      -name    "LN2_DR_FODO" -length 0.15 
Drift      -name    "LN2_DR_FODO" -length 0.15 
Drift      -name    "LN2_DR_FODO" -length 0.15 
Drift      -name    "LN2_DR_FODO" -length 0.15 
Drift      -name    "LN2_DR_DI_QD_ED" -length 0.0425 
Quadrupole -name    "LN2_QD_DI_DH"  -length 0.04  -strength [expr 0.04*$e0*-8] -e0 $e0
Dipole     -name    "LN2_COR_DI" -length 0 
Bpm        -name    "LN2_BPM_DI" -length 0 
Quadrupole -name    "LN2_QD_DI_DH"  -length 0.04  -strength [expr 0.04*$e0*-8] -e0 $e0
Drift      -name    "LN2_DR_DI_QD_ED" -length 0.0425 
Drift      -name    "LN2_DR_FODO" -length 0.15 
Drift      -name    "LN2_DR_FODO" -length 0.15 
Drift      -name    "LN2_DR_FODO" -length 0.15 
Drift      -name    "LN2_DR_FODO" -length 0.15 
Drift      -name    "LN2_DR_DI_VS" -length 0.1 
Drift      -name    "LN2_DR_FODO" -length 0.15 
Drift      -name    "LN2_DR_FODO" -length 0.15 
Drift      -name    "LN2_DR_FODO" -length 0.15 
Drift      -name    "LN2_DR_FODO" -length 0.15 
Drift      -name    "LN2_DR_DI_QD_ED" -length 0.0425 
Quadrupole -name    "LN2_QD_DI_FH"  -length 0.04  -strength [expr 0.04*$e0*8] -e0 $e0
Dipole     -name    "LN2_COR_DI" -length 0 
Bpm        -name    "LN2_BPM_DI" -length 0 
Quadrupole -name    "LN2_QD_DI_FH"  -length 0.04  -strength [expr 0.04*$e0*8] -e0 $e0
Drift      -name    "LN2_DR_DI_QD_ED" -length 0.0425 
Drift      -name    "LN2_DR_FODO" -length 0.15 
Drift      -name    "LN2_DR_FODO" -length 0.15 
Drift      -name    "LN2_DR_FODO" -length 0.15 
Drift      -name    "LN2_DR_FODO" -length 0.15 
Drift      -name    "LN2_DR_DI_VS" -length 0.1 
Sbend      -name    "LN2_DP_DI" -length 0.15  -angle 0  -E1 0 -E2 0 -hgap 0 -fint 0.5 -tilt 0 -e0 $e0 -six_dim 1 
Drift      -name    "LN2_DR_FODO" -length 0.15 
Drift      -name    "LN2_DR_FODO" -length 0.15 
Drift      -name    "LN2_DR_FODO" -length 0.15 
Drift      -name    "LN2_DR_DI_QD_ED" -length 0.0425 
Quadrupole -name    "LN2_QD_DI_DH"  -length 0.04  -strength [expr 0.04*$e0*-8] -e0 $e0
Dipole     -name    "LN2_COR_DI" -length 0 
Bpm        -name    "LN2_BPM_DI" -length 0 
Quadrupole -name    "LN2_QD_DI_DH"  -length 0.04  -strength [expr 0.04*$e0*-8] -e0 $e0
Drift      -name    "LN2_DR_DI_QD_ED" -length 0.0425 
Drift      -name    "LN2_DR_DI" -length 0.25 
Drift      -name    "LN2_WA_OU_DI2" -length 0 
Drift      -name    "LN2_DR_BP_V1" -length 2.86923311323 
Drift      -name    "LN2_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN2_QD_BP_V01"  -length 0.04  -strength [expr 0.04*$e0*-0.0801035217721] -e0 $e0
Dipole     -name    "LN2_COR" -length 0 
Bpm        -name    "LN2_BPM" -length 0 
Quadrupole -name    "LN2_QD_BP_V01"  -length 0.04  -strength [expr 0.04*$e0*-0.0801035217721] -e0 $e0
Drift      -name    "LN2_DR_QD_ED" -length 0.0425 
Drift      -name    "LN2_DR_BP_V2" -length 2.99999993996 
Drift      -name    "LN2_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN2_QD_BP_V02"  -length 0.04  -strength [expr 0.04*$e0*10.0934216134] -e0 $e0
Dipole     -name    "LN2_COR" -length 0 
Bpm        -name    "LN2_BPM" -length 0 
Quadrupole -name    "LN2_QD_BP_V02"  -length 0.04  -strength [expr 0.04*$e0*10.0934216134] -e0 $e0
Drift      -name    "LN2_DR_QD_ED" -length 0.0425 
Drift      -name    "LN2_DR_BP_V3" -length 0.448126539627 
Drift      -name    "LN2_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN2_QD_BP_V03"  -length 0.04  -strength [expr 0.04*$e0*-9.62118628892] -e0 $e0
Dipole     -name    "LN2_COR" -length 0 
Bpm        -name    "LN2_BPM" -length 0 
Quadrupole -name    "LN2_QD_BP_V03"  -length 0.04  -strength [expr 0.04*$e0*-9.62118628892] -e0 $e0
Drift      -name    "LN2_DR_QD_ED" -length 0.0425 
Drift      -name    "LN2_DR_BP_V4" -length 0.201493610796 
Drift      -name    "LN2_DR_BP_0" -length 0.2 
Drift      -name    "LN2_DR_BP_BL" -length 0.05 
Drift      -name    "LN2_DR_SCA_ED" -length 0.08325105 
CrabCavity -name    "LN2_SCA0" -length 0.5334979  -voltage $LN2_SCA_VLT -phase $LN2_SCA_PHS -frequency 2.997
set e0      [expr $e0 + $LN2_SCA_VLT*sin(3.14159265359*$LN2_SCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN2_DR_SCA_ED" -length 0.08325105 
Drift      -name    "LN2_DR_BP_BL" -length 0.05 
Drift      -name    "LN2_DR_BP_0" -length 0.2 
Drift      -name    "LN2_DR_BP" -length 0.4 
Drift      -name    "LN2_DR_BP" -length 0.4 
Drift      -name    "LN2_DR_BP_VS" -length 0.1 
Drift      -name    "LN2_DR_BP_DR" -length 0.1 
Drift      -name    "LN2_DP_SEPM" -length 0.5 
Drift      -name    "LN2_DR_BP" -length 0.4 
Drift      -name    "LN2_DR_BP" -length 0.4 
Drift      -name    "LN2_DR_BP" -length 0.4 
Drift      -name    "LN2_DR_BP" -length 0.4 
Drift      -name    "LN2_DR_BP" -length 0.4 
Drift      -name    "LN2_DR_BP_VS" -length 0.1 
Drift      -name    "LN2_DR_BP" -length 0.4 
Drift      -name    "LN2_BP_WA_OU2" -length 0 

Drift      -name    "LN3_DR_CT" -length 0.1 
Drift      -name    "LN3_DR_V1" -length 0.100074881712 
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN3_QD_V01"  -length 0.04  -strength [expr 0.04*$e0*-5.89934227435] -e0 $e0
Dipole     -name    "LN3_COR" -length 0 
Bpm        -name    "LN3_BPM" -length 0 
Quadrupole -name    "LN3_QD_V01"  -length 0.04  -strength [expr 0.04*$e0*-5.89934227435] -e0 $e0
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Drift      -name    "LN3_DR_V2" -length 0.105217431443 
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN3_QD_V02"  -length 0.04  -strength [expr 0.04*$e0*10.4965029886] -e0 $e0
Dipole     -name    "LN3_COR" -length 0 
Bpm        -name    "LN3_BPM" -length 0 
Quadrupole -name    "LN3_QD_V02"  -length 0.04  -strength [expr 0.04*$e0*10.4965029886] -e0 $e0
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Drift      -name    "LN3_DR_V3" -length 0.932257578801 
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN3_QD_V03"  -length 0.04  -strength [expr 0.04*$e0*-4.73938830712] -e0 $e0
Dipole     -name    "LN3_COR" -length 0 
Bpm        -name    "LN3_BPM" -length 0 
Quadrupole -name    "LN3_QD_V03"  -length 0.04  -strength [expr 0.04*$e0*-4.73938830712] -e0 $e0
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Drift      -name    "LN3_DR_V4" -length 0.121300495532 
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN3_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*4.2] -e0 $e0
Dipole     -name    "LN3_COR" -length 0 
Bpm        -name    "LN3_BPM" -length 0 
Quadrupole -name    "LN3_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*4.2] -e0 $e0
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Drift      -name    "LN3_DR_SV" -length 0.1 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN3_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-4.2] -e0 $e0
Dipole     -name    "LN3_COR" -length 0 
Bpm        -name    "LN3_BPM" -length 0 
Quadrupole -name    "LN3_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-4.2] -e0 $e0
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Drift      -name    "LN3_DR_VS" -length 0.1 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN3_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*4.2] -e0 $e0
Dipole     -name    "LN3_COR" -length 0 
Bpm        -name    "LN3_BPM" -length 0 
Quadrupole -name    "LN3_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*4.2] -e0 $e0
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Drift      -name    "LN3_DR_SV" -length 0.1 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN3_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-4.2] -e0 $e0
Dipole     -name    "LN3_COR" -length 0 
Bpm        -name    "LN3_BPM" -length 0 
Quadrupole -name    "LN3_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-4.2] -e0 $e0
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Drift      -name    "LN3_DR_VS" -length 0.1 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN3_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*4.2] -e0 $e0
Dipole     -name    "LN3_COR" -length 0 
Bpm        -name    "LN3_BPM" -length 0 
Quadrupole -name    "LN3_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*4.2] -e0 $e0
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Drift      -name    "LN3_DR_SV" -length 0.1 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN3_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-4.2] -e0 $e0
Dipole     -name    "LN3_COR" -length 0 
Bpm        -name    "LN3_BPM" -length 0 
Quadrupole -name    "LN3_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-4.2] -e0 $e0
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Drift      -name    "LN3_DR_VS" -length 0.1 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN3_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*4.2] -e0 $e0
Dipole     -name    "LN3_COR" -length 0 
Bpm        -name    "LN3_BPM" -length 0 
Quadrupole -name    "LN3_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*4.2] -e0 $e0
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Drift      -name    "LN3_DR_SV" -length 0.1 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN3_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-4.2] -e0 $e0
Dipole     -name    "LN3_COR" -length 0 
Bpm        -name    "LN3_BPM" -length 0 
Quadrupole -name    "LN3_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-4.2] -e0 $e0
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Drift      -name    "LN3_DR_VS" -length 0.1 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN3_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*4.2] -e0 $e0
Dipole     -name    "LN3_COR" -length 0 
Bpm        -name    "LN3_BPM" -length 0 
Quadrupole -name    "LN3_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*4.2] -e0 $e0
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Drift      -name    "LN3_DR_SV" -length 0.1 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN3_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-4.2] -e0 $e0
Dipole     -name    "LN3_COR" -length 0 
Bpm        -name    "LN3_BPM" -length 0 
Quadrupole -name    "LN3_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-4.2] -e0 $e0
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Drift      -name    "LN3_DR_VS" -length 0.1 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN3_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*4.2] -e0 $e0
Dipole     -name    "LN3_COR" -length 0 
Bpm        -name    "LN3_BPM" -length 0 
Quadrupole -name    "LN3_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*4.2] -e0 $e0
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Drift      -name    "LN3_DR_SV" -length 0.1 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN3_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-4.2] -e0 $e0
Dipole     -name    "LN3_COR" -length 0 
Bpm        -name    "LN3_BPM" -length 0 
Quadrupole -name    "LN3_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-4.2] -e0 $e0
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Drift      -name    "LN3_DR_VS" -length 0.1 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN3_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*4.2] -e0 $e0
Dipole     -name    "LN3_COR" -length 0 
Bpm        -name    "LN3_BPM" -length 0 
Quadrupole -name    "LN3_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*4.2] -e0 $e0
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Drift      -name    "LN3_DR_SV" -length 0.1 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN3_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-4.2] -e0 $e0
Dipole     -name    "LN3_COR" -length 0 
Bpm        -name    "LN3_BPM" -length 0 
Quadrupole -name    "LN3_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-4.2] -e0 $e0
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Drift      -name    "LN3_DR_VS" -length 0.1 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN3_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*4.2] -e0 $e0
Dipole     -name    "LN3_COR" -length 0 
Bpm        -name    "LN3_BPM" -length 0 
Quadrupole -name    "LN3_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*4.2] -e0 $e0
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Drift      -name    "LN3_DR_SV" -length 0.1 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN3_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-4.2] -e0 $e0
Dipole     -name    "LN3_COR" -length 0 
Bpm        -name    "LN3_BPM" -length 0 
Quadrupole -name    "LN3_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-4.2] -e0 $e0
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Drift      -name    "LN3_DR_VS" -length 0.1 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN3_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*4.2] -e0 $e0
Dipole     -name    "LN3_COR" -length 0 
Bpm        -name    "LN3_BPM" -length 0 
Quadrupole -name    "LN3_QD_FH"  -length 0.04  -strength [expr 0.04*$e0*4.2] -e0 $e0
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Drift      -name    "LN3_DR_SV" -length 0.1 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Cavity     -name    "LN3_XCA0" -length 0.9164755  -gradient $LN3_XCA_GRD -phase $LN3_XCA_PHS -frequency 11.9942
set e0      [expr $e0 + 0.9164755*$LN3_XCA_GRD*cos(3.14159265359*$LN3_XCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN3_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-4.2] -e0 $e0
Dipole     -name    "LN3_COR" -length 0 
Bpm        -name    "LN3_BPM" -length 0 
Quadrupole -name    "LN3_QD_DH"  -length 0.04  -strength [expr 0.04*$e0*-4.2] -e0 $e0
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Drift      -name    "LN3_DR_DR" -length 0.1 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_XCA_" -length 0.9164755 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_BL" -length 0.05 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_XCA_" -length 0.9164755 
Drift      -name    "LN3_DR_XCA_ED" -length 0.05676225 
Drift      -name    "LN3_DR_CT" -length 0.1 
Drift      -name    "LN3_WA_OU2" -length 0 
Drift      -name    "LN3_DR_BP_V1" -length 0.793830967135 
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN3_QD_BP_V01"  -length 0.04  -strength [expr 0.04*$e0*-9.13727992969] -e0 $e0
Dipole     -name    "LN3_COR" -length 0 
Bpm        -name    "LN3_BPM" -length 0 
Quadrupole -name    "LN3_QD_BP_V01"  -length 0.04  -strength [expr 0.04*$e0*-9.13727992969] -e0 $e0
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Drift      -name    "LN3_DR_BP_V2" -length 1.74059280144 
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN3_QD_BP_V02"  -length 0.04  -strength [expr 0.04*$e0*10.3954931562] -e0 $e0
Dipole     -name    "LN3_COR" -length 0 
Bpm        -name    "LN3_BPM" -length 0 
Quadrupole -name    "LN3_QD_BP_V02"  -length 0.04  -strength [expr 0.04*$e0*10.3954931562] -e0 $e0
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Drift      -name    "LN3_DR_BP_V3" -length 0.956274357832 
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Quadrupole -name    "LN3_QD_BP_V03"  -length 0.04  -strength [expr 0.04*$e0*-9.17246989919] -e0 $e0
Dipole     -name    "LN3_COR" -length 0 
Bpm        -name    "LN3_BPM" -length 0 
Quadrupole -name    "LN3_QD_BP_V03"  -length 0.04  -strength [expr 0.04*$e0*-9.17246989919] -e0 $e0
Drift      -name    "LN3_DR_QD_ED" -length 0.0425 
Drift      -name    "LN3_DR_BP_V4" -length 0.545326758693 
Drift      -name    "LN3_DR_BP_0" -length 0.2 
Drift      -name    "LN3_DR_BP_BL" -length 0.05 
Drift      -name    "LN3_DR_SCA_ED" -length 0.08325105 
CrabCavity -name    "LN3_SCA0" -length 0.5334979  -voltage $LN3_SCA_VLT -phase $LN3_SCA_PHS -frequency 2.997
set e0      [expr $e0 + $LN3_SCA_VLT*sin(3.14159265359*$LN3_SCA_PHS/180.) ]
SetReferenceEnergy  $e0
Drift      -name    "LN3_DR_SCA_ED" -length 0.08325105 
Drift      -name    "LN3_DR_BP_BL" -length 0.05 
Drift      -name    "LN3_DR_BP_0" -length 0.2 
Drift      -name    "LN3_DR_BP" -length 0.4 
Drift      -name    "LN3_DR_BP_VS" -length 0.1 
Drift      -name    "LN3_DR_BP" -length 0.4 
Drift      -name    "LN3_DP_SEPM" -length 0.45 
Drift      -name    "LN3_DR_BP" -length 0.4 
Drift      -name    "LN3_DR_BP" -length 0.4 
Drift      -name    "LN3_DR_BP" -length 0.4 
Drift      -name    "LN3_DR_BP" -length 0.4 
Drift      -name    "LN3_DR_BP" -length 0.4 
Drift      -name    "LN3_DR_BP_VS" -length 0.1 
Drift      -name    "LN3_DR_BP" -length 0.4 
Drift      -name    "LN3_BP_WA_OU2_" -length 0 


Drift      -name    "TMC_DR_V1" -length 0.422827856306 
Drift      -name    "TMC_DR_QD_ED" -length 0.0425 
Quadrupole -name    "TMC_QD_V01"  -length 0.04  -strength [expr 0.04*$e0*3.76403858348] -e0 $e0
Dipole     -name    "TMC_COR" -length 0 
Bpm        -name    "TMC_BPM" -length 0 
Quadrupole -name    "TMC_QD_V01"  -length 0.04  -strength [expr 0.04*$e0*3.76403858348] -e0 $e0
Drift      -name    "TMC_DR_QD_ED" -length 0.0425 
Drift      -name    "TMC_DR_V2" -length 7.44284457751 
Drift      -name    "TMC_DR_QD_ED" -length 0.0425 
Quadrupole -name    "TMC_QD_V02"  -length 0.04  -strength [expr 0.04*$e0*-8.67075502217] -e0 $e0
Dipole     -name    "TMC_COR" -length 0 
Bpm        -name    "TMC_BPM" -length 0 
Quadrupole -name    "TMC_QD_V02"  -length 0.04  -strength [expr 0.04*$e0*-8.67075502217] -e0 $e0
Drift      -name    "TMC_DR_QD_ED" -length 0.0425 
Drift      -name    "TMC_DR_V3" -length 0.200973572259 
Drift      -name    "TMC_DR_QD_ED" -length 0.0425 
Quadrupole -name    "TMC_QD_V03"  -length 0.04  -strength [expr 0.04*$e0*8.38785495838] -e0 $e0
Dipole     -name    "TMC_COR" -length 0 
Bpm        -name    "TMC_BPM" -length 0 
Quadrupole -name    "TMC_QD_V03"  -length 0.04  -strength [expr 0.04*$e0*8.38785495838] -e0 $e0
Drift      -name    "TMC_DR_QD_ED" -length 0.0425 
Drift      -name    "TMC_DR_V4" -length 0.2 
Drift      -name    "TMC_DR_20" -length 0.5 
Sbend      -name    "TMC_DP_DIP1" -length 0.50000057  -angle -0.00261799  -E1 0 -E2 -0.00261799 -hgap 0 -fint 0.5 -tilt 0 -e0 $e0 -six_dim 1 
Drift      -name    "TMC_DR_SIDE_C" -length 0.5 
Drift      -name    "TMC_DR_SIDE" -length 3.00001199 
Sbend      -name    "TMC_DP_DIP2" -length 0.50000057  -angle 0.00261799  -E1 0.00261799 -E2 0 -hgap 0 -fint 0.5 -tilt 0 -e0 $e0 -six_dim 1 
Drift      -name    "TMC_DR_CENT_C" -length 0.25 
Bpm        -name    "TMC_BPM" -length 0 
Drift      -name    "TMC_DR_CENT" -length 0.5 
Sbend      -name    "TMC_DP_DIP3" -length 0.50000057  -angle 0.00261799  -E1 0 -E2 0.00261799 -hgap 0 -fint 0.5 -tilt 0 -e0 $e0 -six_dim 1 
Drift      -name    "TMC_DR_SIDE_C" -length 0.5 
Drift      -name    "TMC_DR_SIDE" -length 3.00001199 
Sbend      -name    "TMC_DP_DIP4" -length 0.50000057  -angle -0.00261799  -E1 -0.00261799 -E2 0 -hgap 0 -fint 0.5 -tilt 0 -e0 $e0 -six_dim 1 
Drift      -name    "TMC_DR_20_C" -length 0.5 
Drift      -name    "TMC_WA_OU2" -length 0 

Drift      -name    "HXR_DR_V1" -length 0.213073506554 
Drift      -name    "HXR_DR_QD_ED" -length 0.052 
Quadrupole -name    "HXR_QD_V01"  -length 0.013  -strength [expr 0.013*$e0*-22.6129547228] -e0 $e0
Dipole     -name    "HXR_COR" -length 0 
Bpm        -name    "HXR_BPM" -length 0 
Quadrupole -name    "HXR_QD_V01"  -length 0.013  -strength [expr 0.013*$e0*-22.6129547228] -e0 $e0
Drift      -name    "HXR_DR_QD_ED" -length 0.052 
Drift      -name    "HXR_DR_V2" -length 0.289828383759 
Drift      -name    "HXR_DR_QD_ED" -length 0.052 
Quadrupole -name    "HXR_QD_V02"  -length 0.013  -strength [expr 0.013*$e0*29.9999998914] -e0 $e0
Dipole     -name    "HXR_COR" -length 0 
Bpm        -name    "HXR_BPM" -length 0 
Quadrupole -name    "HXR_QD_V02"  -length 0.013  -strength [expr 0.013*$e0*29.9999998914] -e0 $e0
Drift      -name    "HXR_DR_QD_ED" -length 0.052 
Drift      -name    "HXR_DR_V3" -length 1.93335695355 
Drift      -name    "HXR_DR_QD_ED" -length 0.052 
Quadrupole -name    "HXR_QD_V03"  -length 0.013  -strength [expr 0.013*$e0*-10.4996243742] -e0 $e0
Dipole     -name    "HXR_COR" -length 0 
Bpm        -name    "HXR_BPM" -length 0 
Quadrupole -name    "HXR_QD_V03"  -length 0.013  -strength [expr 0.013*$e0*-10.4996243742] -e0 $e0
Drift      -name    "HXR_DR_QD_ED" -length 0.052 
Drift      -name    "HXR_DR_V4" -length 2.7421195193 
Drift      -name    "HXR_DR_CT" -length 0.1 
Drift      -name    "HXR_DR_QD_ED" -length 0.052 
Quadrupole -name    "HXR_QD_FH"  -length 0.013  -strength [expr 0.013*$e0*15] -e0 $e0
Dipole     -name    "HXR_COR" -length 0 
Bpm        -name    "HXR_BPM" -length 0 
Quadrupole -name    "HXR_QD_FH"  -length 0.013  -strength [expr 0.013*$e0*15] -e0 $e0
Drift      -name    "HXR_DR_QD_ED" -length 0.052 
Drift      -name    "HXR_DR_WIG_ED" -length 0.195 
Drift     -name    "HXR_WIG_0" -length 1.768 
Drift      -name    "HXR_DR_WIG_ED" -length 0.195 
Drift      -name    "HXR_DR_QD_ED" -length 0.052 
Quadrupole -name    "HXR_QD_DH"  -length 0.013  -strength [expr 0.013*$e0*-15] -e0 $e0
Dipole     -name    "HXR_COR" -length 0 
Bpm        -name    "HXR_BPM" -length 0 
Quadrupole -name    "HXR_QD_DH"  -length 0.013  -strength [expr 0.013*$e0*-15] -e0 $e0
Drift      -name    "HXR_DR_QD_ED" -length 0.052 
Drift      -name    "HXR_DR_WIG_ED" -length 0.195 
Drift     -name    "HXR_WIG_0" -length 1.768 
Drift      -name    "HXR_DR_WIG_ED" -length 0.195 
Drift      -name    "HXR_DR_QD_ED" -length 0.052 
Quadrupole -name    "HXR_QD_FH"  -length 0.013  -strength [expr 0.013*$e0*15] -e0 $e0
Drift      -name    "HXR_WA_OU2" -length 0 
