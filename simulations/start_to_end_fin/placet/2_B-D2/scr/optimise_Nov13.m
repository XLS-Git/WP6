
global gdp;

gdp.work_dir = argv(){1}; % work dir

gdp.option = argv(){2}; % option

%%

addpath ([gdp.work_dir '/functions']);

%% ------------------
%% Initial parameters
%% ------------------

% TMC
gdp.angt = 0.00261799;
gdp.e0t = 5.493365;

% Free parameters

%gdp.PL = [ 0.06117379 0.264709+0.5e-3 0.02132792 1.044124+1e-3 15e-3 30.2 25.53e-3 192.0 65e-3 35.76 65e-3 10.82 65e-3 10.82 ];
%gdp.FMQS = [ 0.013*gdp.e0t*-22.613  0.013*gdp.e0t*-22.613  0.013*gdp.e0t*30  0.013*gdp.e0t*30  0.013*gdp.e0t*-10.5  0.013*gdp.e0t*-10.5  0.013*gdp.e0t*15 * [1 1 -1 -1 1] ];
%gdp.P0 = [ gdp.PL gdp.FMQS ];

gdp.P0 = [ 0.06127023974906644 0.2681424957246049 0.02095616680176684 1.050358367634364 0.01539896669389282 30.34511685385529 0.02632474382797911 191.5963025991374 0.06499991363236593 35.74592567945059 0.06499977811381205 10.7912977223912 0.06499992918731679 10.65054403373979 -1.730128781140586 -1.775223508252196 2.135124635277936 2.105383352283597 -0.7426818332013414 -0.942276387904287 1.09416677602626 1.269598139106802 -1.390053896991196 -1.345334764343554 1.334965852447859 ];

% Constraints

gdp.R1 		= gdp.P0 * (1.0 - 0.20);
gdp.R2 		= gdp.P0 * (1.0 + 0.20); 
gdp.R2(9:2:13)  = 65.1e-3; % GV/m
gdp.R1(15:end)  = -3.5; % GeV/m
gdp.R2(15:end)  =  3.5; % GeV/m


%% ---------
%% functions
%% ---------

% create config.dat
function create_config (P)
  global gdp;
  i = 1;
  ang1 	= P(i++);
  e01  	= P(i++);
  ang2 	= P(i++);
  e02  	= P(i++);
  grd0c	= P(i++);
  phs0c	= P(i++);
  grd0k	= P(i++);
  phs0k	= P(i++);
  grd1 	= P(i++);
  phs1 	= P(i++);
  grd2 	= P(i++);
  phs2 	= P(i++);
  grd3 	= P(i++);
  phs3 	= P(i++);
  MQS 	= P(i:end);

  % Input: 1st C-Band section output @ 120 MeV, in share/, [E X Y Z XP YP]
  Parameters.input_file = "cband_out_4ps_0.18mm_100k_120MeV_4_laser_heater.dat";
  
  Parameters.beamline_name = "xls_linac_hxr";
  
  % lattice from 120 MeV, in share/
  Parameters.lattice_file = "xls_lattice_c_inj_bc1_ln1_bc2_ln2_ln3_tmc_hxr_maxe.tcl";
  
  % 75 pC bunch for all schemes
  Parameters.bunch_charge = 75e-12;
  
  % BC1
  Parameters.ang1 = ang1;
  Parameters.e01 = e01;
  
  % BC2
  Parameters.ang2 = ang2;
  Parameters.e02 = e02;
  
  % TMC
  Parameters.angt = gdp.angt;
  Parameters.e0t = gdp.e0t;
  
  % LN0
  Parameters.grd0c = grd0c;
  Parameters.phs0c = phs0c;
  Parameters.grd0k = grd0k;
  Parameters.phs0k = phs0k;
  
  % LN1
  Parameters.grd1 = grd1;
  Parameters.phs1 = phs1;
  
  % LN2
  Parameters.grd2 = grd2;
  Parameters.phs2 = phs2;
  
  % LN3
  Parameters.grd3 = grd3;
  Parameters.phs3 = phs3;
  
  % Final matching quadrupole strengths
  Parameters.FMQS = MQS;
  
  save("-text","config.dat","Parameters");
endfunction

% track function
function merit = track (P)
  global gdp;
  create_config (P);
  cmd = ['placet ' gdp.work_dir '/scr/track.tcl ' gdp.work_dir ' ' gdp.option];
  system(cmd);
  try
    load([gdp.work_dir '/Outputs/' gdp.option '/Twiss.dat']);
    d_beta_x  = beta_x1 - beta_x0;
    d_beta_y  = beta_y1 - beta_y0;
    d_alpha_x = alpha_x1 - alpha_x0;
    d_alpha_y = alpha_y1 - alpha_y0;
    merit_beta_x = abs(d_beta_x)*1000;
    merit_beta_y = abs(d_beta_y)*1000;
    merit_alpha_x = abs(d_alpha_x)*1000;
    merit_alpha_y = abs(d_alpha_y)*1000;
    printf("OPT_INFO:: -------------------------------------- \n");
    printf("OPT_INFO:: beta_x1 = %.3f m (targeted: 8.27 m)  (merit = %.2f)\n", beta_x1, merit_beta_x);
    printf("OPT_INFO:: beta_y1 = %.3f m (targeted: 3.28 m)  (merit = %.2f)\n", beta_y1, merit_beta_y);
    printf("OPT_INFO:: alpha_x1 = %.4f (targeted: 0.00)  (merit = %.2f)\n", alpha_x1, merit_alpha_x);
    printf("OPT_INFO:: alpha_y1 = %.4f (targeted: 0.00)  (merit = %.2f)\n", alpha_y1, merit_alpha_y);
    merit_twiss = hypot(merit_beta_x, merit_beta_y, merit_alpha_x, merit_alpha_y)

    B = load([gdp.work_dir '/Outputs/' gdp.option '/Beam.dat']);
    merit_beam = FoM_beam_Nov13(B)

    merit = hypot(merit_twiss, merit_beam)
  catch
    merit = 1e6 + sum(P)
    printf("OPT_INFO:: -------------------------------------- \n");
    printf("OPT_INFO:: Beam lost. \n"); 
  end_try_catch
endfunction

% optimise function
function merit = optimise (tan_P)
  global gdp;
  P = atan_a(tan_P,gdp.R1,gdp.R2);
  merit = track(P);
endfunction


%% ------------
%% Optimisation
%% ------------

P0 = gdp.P0;

tan_P0 = tan_a(P0,gdp.R1,gdp.R2);

[tan_OP, merit] = fminsearch("optimise", tan_P0);

OP = atan_a (tan_OP,gdp.R1,gdp.R2);

disp("OPT_INFO:: optimisation finished!");
save ("-text", [gdp.work_dir '/Outputs/' gdp.option '/Results.dat'], "OP","merit");

