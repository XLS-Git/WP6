
global gdp;

gdp.work_dir = argv(){1}; % work dir

gdp.option = argv(){2}; % option

%%

addpath ([gdp.work_dir '/functions']);

%% ------------------
%% Initial parameters
%% ------------------

% result of optimise_lat_Nov13
gdp.PL = [ 0.06303798657885221 0.273643907169348 0.0202001311084575 1.121312600296389 0.01582730005638876 29.99129955769009 0.02504954968141703 199.3306673792626 0.06500021392534151 34.81990313423306 0.0650004193788772 11.24138627050971 0.06500042893706537 11.13544443190072 ];

% BC1
gdp.ang1 = gdp.PL(1); % rad
gdp.e01 = gdp.PL(2); % GeV

% BC2
gdp.ang2 = gdp.PL(3);
gdp.e02 = gdp.PL(4);

% TMC
gdp.angt = 0.00261799;
gdp.e0t = 5.493365;

% LN0
gdp.grd0c = gdp.PL(5); % GV/m
gdp.phs0c = gdp.PL(6); % deg
gdp.grd0k = gdp.PL(7);
gdp.phs0k = gdp.PL(8);

% LN1
gdp.grd1 = gdp.PL(9);
gdp.phs1 = gdp.PL(10);

% LN2
gdp.grd2 = gdp.PL(11);
gdp.phs2 = gdp.PL(12);

% LN3
gdp.grd3 = gdp.PL(13);
gdp.phs3 = gdp.PL(14);

% Final matching quadrupole strengths (iterated)
gdp.FMQS = [ 0.013*gdp.e0t*-22.613  0.013*gdp.e0t*-22.613  0.013*gdp.e0t*30  0.013*gdp.e0t*30  0.013*gdp.e0t*-10.5  0.013*gdp.e0t*-10.5  0.013*gdp.e0t*15 * [1 1 -1 -1 1] ];

gdp.P0 = [ gdp.FMQS ];

%

gdp.R1 = -3.5*ones(1,length(gdp.P0)); % GeV/m
gdp.R2 =  3.5*ones(1,length(gdp.P0)); % GeV/m


%% ---------
%% functions
%% ---------

% create config.dat
function create_config (P)
  global gdp;
  i = 1;
  ang1 	= P(i++);
  e01  	= P(i++);
  ang2 	= P(i++);
  e02  	= P(i++);
  grd0c	= P(i++);
  phs0c	= P(i++);
  grd0k	= P(i++);
  phs0k	= P(i++);
  grd1 	= P(i++);
  phs1 	= P(i++);
  grd2 	= P(i++);
  phs2 	= P(i++);
  grd3 	= P(i++);
  phs3 	= P(i++);
  MQS 	= P(i:end);

  % Input: 1st C-Band section output @ 120 MeV, in share/, [E X Y Z XP YP]
  Parameters.input_file = "cband_out_4ps_0.18mm_100k_120MeV_4_laser_heater.dat";
  
  Parameters.beamline_name = "xls_linac_hxr";
  
  % lattice from 120 MeV, in share/
  Parameters.lattice_file = "xls_lattice_c_inj_bc1_ln1_bc2_ln2_ln3_tmc_hxr_maxe.tcl";
  
  % 75 pC bunch for all schemes
  Parameters.bunch_charge = 75e-12;
  
  % BC1
  Parameters.ang1 = ang1;
  Parameters.e01 = e01;
  
  % BC2
  Parameters.ang2 = ang2;
  Parameters.e02 = e02;
  
  % TMC
  Parameters.angt = gdp.angt;
  Parameters.e0t = gdp.e0t;
  
  % LN0
  Parameters.grd0c = grd0c;
  Parameters.phs0c = phs0c;
  Parameters.grd0k = grd0k;
  Parameters.phs0k = phs0k;
  
  % LN1
  Parameters.grd1 = grd1;
  Parameters.phs1 = phs1;
  
  % LN2
  Parameters.grd2 = grd2;
  Parameters.phs2 = phs2;
  
  % LN3
  Parameters.grd3 = grd3;
  Parameters.phs3 = phs3;
  
  % Final matching quadrupole strengths
  Parameters.FMQS = MQS;
  
  save("-text","config.dat","Parameters");
endfunction

% track function
function merit = track (P)
  global gdp;
  create_config ([gdp.PL P]);
  cmd = ['placet ' gdp.work_dir '/scr/track.tcl ' gdp.work_dir ' ' gdp.option];
  system(cmd);
  try
    load([gdp.work_dir '/Outputs/' gdp.option '/Twiss.dat']);

    d_beta_x  = beta_x1 - beta_x0;
    d_beta_y  = beta_y1 - beta_y0;
    d_alpha_x = alpha_x1 - alpha_x0;
    d_alpha_y = alpha_y1 - alpha_y0;
    merit_beta_x = abs(d_beta_x)*1000;
    merit_beta_y = abs(d_beta_y)*1000;
    merit_alpha_x = abs(d_alpha_x)*1000;
    merit_alpha_y = abs(d_alpha_y)*1000;
    printf("OPT_INFO:: -------------------------------------- \n");
    printf("OPT_INFO:: beta_x1 = %.3f m (targeted: 8.27 m)  (merit = %.2f)\n", beta_x1, merit_beta_x);
    printf("OPT_INFO:: beta_y1 = %.3f m (targeted: 3.28 m)  (merit = %.2f)\n", beta_y1, merit_beta_y);
    printf("OPT_INFO:: alpha_x1 = %.4f (targeted: 0.00)  (merit = %.2f)\n", alpha_x1, merit_alpha_x);
    printf("OPT_INFO:: alpha_y1 = %.4f (targeted: 0.00)  (merit = %.2f)\n", alpha_y1, merit_alpha_y);
    merit_twiss = hypot(merit_beta_x, merit_beta_y, merit_alpha_x, merit_alpha_y)

    B = load([gdp.work_dir '/Outputs/' gdp.option '/Beam.dat']);
    merit_beam = FoM_beam_Nov13(B)

    merit = hypot(merit_twiss, merit_beam)
  catch
    merit = 1e6 + sum(P)
    printf("OPT_INFO:: -------------------------------------- \n");
    printf("OPT_INFO:: Beam lost. \n"); 
  end_try_catch
endfunction

% optimise function
function merit = optimise (tan_P)
  global gdp;
  P = atan_a(tan_P,gdp.R1,gdp.R2);
  merit = track(P);
endfunction


%% ------------
%% Optimisation
%% ------------

P0 = gdp.P0;

tan_P0 = tan_a(P0,gdp.R1,gdp.R2);

[tan_OP, merit] = fminsearch("optimise", tan_P0);

OP = atan_a (tan_OP,gdp.R1,gdp.R2);

disp("OPT_INFO:: optimisation finished!");
save ("-text", [gdp.work_dir '/Outputs/' gdp.option '/Results.dat'], "OP","merit");

