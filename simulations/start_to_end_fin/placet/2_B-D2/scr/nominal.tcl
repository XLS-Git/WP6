
Octave {

      addpath ("${work_dir}/functions");

      beta_x0 = 8.27; # m
      beta_y0 = 3.28; # m
      alpha_x0 = 0;
      alpha_y0 = 0;

      printf("INFO:: Twiss targeted: %f %f %f %f \n", beta_x0, beta_y0,alpha_x0, alpha_y0);

      emit_file_format = "%E %x %y %s %xp %yp %ex %ey";

      [E, B] = placet_test_no_correction("$beamlinename", ["beam" num2str(0)], "None", emit_file_format);

      if(length(B)!=0)

        twiss = placet_get_twiss_matrix(B);

        beta_x1 = twiss(1,1);
        beta_y1 = twiss(3,3);
        alpha_x1 = twiss(1,2);
        alpha_y1 = twiss(3,4);

        printf("INFO:: Twiss obtained: %f %f %f %f \n", beta_x1, beta_y1, alpha_x1, alpha_y1);

        if (strncmp("${option}","nominal",7))
          merit_beam = FoM_beam_Nov13(B)
	endif

        save ("-text", ["${out_dir}/Twiss.dat"], "beta_x0","beta_y0","alpha_x0","alpha_y0","beta_x1","beta_y1","alpha_x1","alpha_y1");

        save ("-ascii", ["${out_dir}/Emittance.dat"], "E");
        save ("-ascii", ["${out_dir}/Beam.dat"], "B");
      else
        disp("INFO:: beam lost. No output.");
        system ("rm -f ${out_dir}/Twiss.dat");
        system ("rm -f ${out_dir}/Emittance.dat");
        system ("rm -f ${out_dir}/Beam.dat");
      endif

}

