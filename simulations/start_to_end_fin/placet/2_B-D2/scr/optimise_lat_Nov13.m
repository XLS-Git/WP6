
global gdp;

gdp.work_dir = argv(){1}; % work dir

gdp.option = argv(){2}; % option

%%

addpath ([gdp.work_dir '/functions']);

%% ------------------
%% Initial parameters
%% ------------------

% TMC
gdp.angt = 0.00261799;
gdp.e0t = 5.493365;


% Final matching quadrupole strengths (not useful in this optimisation)
gdp.FMQS = [ 0.013*gdp.e0t*-22.613  0.013*gdp.e0t*-22.613  0.013*gdp.e0t*30  0.013*gdp.e0t*30  0.013*gdp.e0t*-10.5  0.013*gdp.e0t*-10.5  0.013*gdp.e0t*15 * [1 1 -1 -1 1] ];

% Free parameter list
%gdp.P0 = [ 0.060239678757756 0.2761115283183905 0.0216087621768588 1.06889569755204 0.01591868019812148 29.54745533771792 0.02524518898933211 190.5402431846163 0.06500033184435987 35.12395985105615 0.06500034492181604 11.11617734739295 0.06500033251051278 10.75842250165791 ];
%gdp.P0 = [ 0.06070608738102631 0.2806105897339135 0.02146107918091197 1.111830183108845 0.01640778740392512 29.07363335046684 0.0249107303676181 192.0917760659372 0.0650004681118318 34.51188897819004 0.06500053423603114 11.16707380481647 0.06500049866300525 10.93767557069311 ];
gdp.P0 = [ 0.06303798657885221 0.273643907169348 0.0202001311084575 1.121312600296389 0.01582730005638876 29.99129955769009 0.02504954968141703 199.3306673792626 0.06500021392534151 34.81990313423306 0.0650004193788772 11.24138627050971 0.06500042893706537 11.13544443190072 ];

gdp.R1 		= gdp.P0 * (1.0 - 0.30);
gdp.R2 		= gdp.P0 * (1.0 + 0.30); 
gdp.R2(9:2:13)  = 65.1e-3; % GV/m


%% ---------
%% functions
%% ---------

% create config.dat
function create_config (P)
  global gdp;
  i = 1;
  ang1 	= P(i++);
  e01  	= P(i++);
  ang2 	= P(i++);
  e02  	= P(i++);
  grd0c	= P(i++);
  phs0c	= P(i++);
  grd0k	= P(i++);
  phs0k	= P(i++);
  grd1 	= P(i++);
  phs1 	= P(i++);
  grd2 	= P(i++);
  phs2 	= P(i++);
  grd3 	= P(i++);
  phs3 	= P(i++);
  MQS 	= gdp.FMQS;

  % Input: 1st C-Band section output @ 120 MeV, in share/, [E X Y Z XP YP]
  Parameters.input_file = "cband_out_4ps_0.18mm_100k_120MeV_4_laser_heater.dat";
  
  Parameters.beamline_name = "xls_linac_hxr";
  
  % lattice from 120 MeV, in share/
  Parameters.lattice_file = "xls_lattice_c_inj_bc1_ln1_bc2_ln2_ln3_tmc_hxr_maxe.tcl";
  
  % 75 pC bunch for all schemes
  Parameters.bunch_charge = 75e-12;
  
  % BC1
  Parameters.ang1 = ang1;
  Parameters.e01  = e01;
  
  % BC2
  Parameters.ang2 = ang2;
  Parameters.e02  = e02;
  
  % TMC
  Parameters.angt = gdp.angt;
  Parameters.e0t  = gdp.e0t;
  
  % LN0
  Parameters.grd0c = grd0c;
  Parameters.phs0c = phs0c;
  Parameters.grd0k = grd0k;
  Parameters.phs0k = phs0k;
  
  % LN1
  Parameters.grd1 = grd1;
  Parameters.phs1 = phs1;
  
  % LN2
  Parameters.grd2 = grd2;
  Parameters.phs2 = phs2;
  
  % LN3
  Parameters.grd3 = grd3;
  Parameters.phs3 = phs3;
  
  % Final matching quadrupole strengths
  Parameters.FMQS = MQS;
  
  save("-text","config.dat","Parameters");
endfunction

% track function
function merit = track (P)
  global gdp;
  create_config (P);
  cmd = ['placet ' gdp.work_dir '/scr/track.tcl ' gdp.work_dir ' ' gdp.option];
  system(cmd);
  try

    Beam = load([gdp.work_dir '/Outputs/' gdp.option '/Watch_12.dat']); % TMC exit

    if (isnan(Beam(1,1)))
      merit = 1e6 + sum(P)
      printf("OPT_INFO:: -------------------------------------- \n");
      printf("OPT_INFO:: Beam lost. \n"); 
    else
      merit = FoM_beam_Nov13(Beam)
    endif
  catch
    merit = 1e6 + sum(P)
    printf("OPT_INFO:: -------------------------------------- \n");
    printf("OPT_INFO:: Beam lost. \n"); 
  end_try_catch
endfunction

% optimise function
function merit = optimise (tan_P)
  global gdp;
  P = atan_a(tan_P,gdp.R1,gdp.R2);
  merit = track(P);
endfunction


%% ------------
%% Optimisation
%% ------------

P0 = gdp.P0;

tan_P0 = tan_a(P0,gdp.R1,gdp.R2);

[tan_OP, merit] = fminsearch("optimise", tan_P0);

OP = atan_a (tan_OP,gdp.R1,gdp.R2);

disp("OPT_INFO:: optimisation finished!");
save ("-text", [gdp.work_dir '/Outputs/' gdp.option '/Results.dat'], "OP","merit");

