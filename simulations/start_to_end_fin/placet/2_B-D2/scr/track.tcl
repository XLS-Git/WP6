
## Arguments

if {$argc > 1} {
  set work_dir [lindex $argv 0]
  set option [lindex $argv 1]
} else {
  set work_dir "../.."
}

set share_dir 	"$work_dir/share"
set scr_dir 	"$work_dir/scr"
set out_dir 	"."

## Load files

source $share_dir/load_files.tcl

## Parameters

Octave {
  load("config.dat");
  Var_Names = fieldnames(Parameters);
  disp("INFO:: creating config file ...");
  for i = 1:length(Var_Names)
    var_name = Var_Names{i};
    var_val = Parameters.(var_name);
    printf("  %s:\t%s\n", num2str(var_name), num2str(var_val));
    Tcl_SetVar(["p_" var_name],var_val);
  endfor
}

set beam0_indist 	$p_input_file

set beamlinename 	$p_beamline_name
set latfile 		$p_lattice_file
set charge 		[expr ${p_bunch_charge}/1.6e-19]

## Set beam

# parameters defined in share/tools.tcl:proc bunch_parameters
array set BeamDefine	[bunch_parameters $share_dir/$beam0_indist] 

array set BeamDefine	"name beam0 filename $beam0_indist n_slice 201 charge $charge"

set BeamDefine(n_macro) [expr int($BeamDefine(npart)/$BeamDefine(n_slice))]

lassign "$BeamDefine(beta_x) $BeamDefine(beta_y) $BeamDefine(alpha_x) $BeamDefine(alpha_y)" btx bty alx aly

# Initial reference energy
set e0 $BeamDefine(energy)

puts "INFO:: beam parameters:"

parray BeamDefine ;# show array

## Some variables used in lattice file

set LN0_XCA_VLT 0
set LN0_XCA_PHS 0

set LN0_CCA_GRD $p_grd0c
set LN0_CCA_PHS $p_phs0c

set LN0_KCA_GRD $p_grd0k
set LN0_KCA_PHS $p_phs0k

set BC1_XCA_VLT 0
set BC1_XCA_PHS 0

set LN1_XCA_GRD $p_grd1
set LN1_XCA_PHS $p_phs1

set LN2_XCA_GRD $p_grd2
set LN2_XCA_PHS $p_phs2

set LN2_XCA_VLT_DI 0
set LN2_XCA_PHS_DI 0

set LN2_SCA_VLT 0
set LN2_SCA_PHS 0

set LN3_XCA_GRD $p_grd3
set LN3_XCA_PHS $p_phs3

set LN3_SCA_VLT 0
set LN3_SCA_PHS 0

set LN4_XCA_GRD 0
set LN4_XCA_PHS 0

set b0_in "beam0.in"
set b0_ou "beam0.ou"

## Create Beamline

Girder

source $share_dir/$latfile

BeamlineSet -name $beamlinename

#if { $option == "tab" } {
#  source $scr_dir/tab.tcl
#  exit
#}

## BBA parameters

#if { $option == "bba" } {
#  set sigma 	100.0
#  set bpmres 	5.0
#  set dphase1 	10
#  set dphase2 	10
#  set dcharge 	0.9
#  set beta0 	1
#  set beta1 	1
#  set beta2 	1
#  set wgt1 	-1
#  set wgt2 	-1
#  set nbins 	1
#  set noverlap 	0.0
#  set nm 	100
#  set machine 	all
#}

if { $option == "nominal" } {
  set CSF { 100 } ;# nominal
} elseif { $option == "bba" } {
  set CSF [list 100 [expr int($dcharge*100)] ] ;# bba
} elseif { $option == "cgt" } {
  # bunch charge tolerance (+/- 4%)
  set CSF { 96 96 98 100 102 104 } ;# charge tolerance
} elseif { $option == "incoh_1" || $option == "incoh_2" || $option == "incoh_3" } {
  set n_rand 200;
  Octave {
    randn('seed',1);
    mkdir ${out_dir}/RandomBeams
    ## save charge nominal
    charge_pC = 1.0*${charge}*1.6e-19/1e-12; # pC
    save ("-ascii", "${out_dir}/RandomBeams/ChargePC0.dat", "charge_pC");
  }
  set CSF { 100 };
  for {set i 1} {$i <= $n_rand} {incr i} {
    set csf_rnd 100;
    Octave {
      csf_rnd = normrnd (100, 2); # charge error, 2% rms
      ## save charge jitter
      charge_pC = csf_rnd/100.0*${charge}*1.6e-19/1e-12; # pC
      save ("-ascii", "${out_dir}/RandomBeams/ChargePC${i}.dat", "charge_pC");
      Tcl_SetVar("csf_rnd", csf_rnd);
    }
    lappend CSF $csf_rnd;
  }
} else {
  set CSF { 100 } ;# others
}

set ibeam 0
foreach csf $CSF {
  puts "Create beam $ibeam: charge SF = $csf%"
  set BeamDefine(name) "beam$ibeam" ;# beam name
  set BeamDefine(charge) [expr double($csf)/100.0*$charge] ;# different bunch charge
  set BeamDefine(filename) $beam0_indist ;# correct filename
  parray BeamDefine ;# show array in a better way
  make_particle_beam_read BeamDefine $share_dir/$BeamDefine(filename)
  incr ibeam
}

global iwatch
set iwatch 0
proc save_watch {} {
 global iwatch
 incr iwatch
 set filename "Watch_${iwatch}.dat"
 BeamDump -file ${filename}
 }

Octave {
    global beaminxdx = 1;
    global Beams = {};
}

proc octave_save {} {
    Octave {
      Beams{beaminxdx++} = placet_get_beam();
    }
} 

## Apply wakes for all structures

Octave {

    ALLCAVITs = placet_get_number_list("$beamlinename", "cavity");
    placet_element_set_attribute("$beamlinename", ALLCAVITs, "six_dim", true);
    
    LN0XDCs = placet_get_name_number_list("$beamlinename", "LN0_XCA0_DI");
    placet_element_set_attribute("$beamlinename", LN0XDCs, "short_range_wake",  "Xdband_SR_W");
    placet_element_set_attribute("$beamlinename", LN0XDCs, "lambda", $xdband_str(lambda));
    
    LN0CCs = placet_get_name_number_list("$beamlinename", "LN0_CCA0");
    placet_element_set_attribute("$beamlinename", LN0CCs, "short_range_wake",  "Cband_SR_W");
    placet_element_set_attribute("$beamlinename", LN0CCs, "lambda", $cband_str(lambda));
     
    LN0KCs = placet_get_name_number_list("$beamlinename", "LN0_KCA0");
    placet_element_set_attribute("$beamlinename", LN0KCs, "short_range_wake",  "Kband_SR_W");
    placet_element_set_attribute("$beamlinename", LN0KCs, "lambda", $kband_str(lambda));
   
    BC1XCs = placet_get_name_number_list("$beamlinename", "BC1_XCA0");
    placet_element_set_attribute("$beamlinename", BC1XCs, "short_range_wake",  "Xdband_SR_W");
    placet_element_set_attribute("$beamlinename", BC1XCs, "lambda", $xdband_str(lambda));
    
    LN1XCs = placet_get_name_number_list("$beamlinename", "LN1_XCA0");
    placet_element_set_attribute("$beamlinename", LN1XCs, "short_range_wake",  "Xband_SR_W");
    placet_element_set_attribute("$beamlinename", LN1XCs, "lambda", $xband_str(lambda));
    
    LN2XCs = placet_get_name_number_list("$beamlinename", "LN2_XCA0");
    placet_element_set_attribute("$beamlinename", LN2XCs, "short_range_wake",  "Xband_SR_W");
    placet_element_set_attribute("$beamlinename", LN2XCs, "lambda", $xband_str(lambda));
    
    LN2XDCs = placet_get_name_number_list("$beamlinename", "LN2_XCA0_DI");
    placet_element_set_attribute("$beamlinename", LN2XDCs, "short_range_wake",  "Xdband_SR_W");
    placet_element_set_attribute("$beamlinename", LN2XDCs, "lambda", $xdband_str(lambda));    
    
    LN2SCs = placet_get_name_number_list("$beamlinename", "LN2_SCA0");
    placet_element_set_attribute("$beamlinename", LN2SCs, "short_range_wake",  "Sdband_SR_W");
    placet_element_set_attribute("$beamlinename", LN2SCs, "lambda", $sdband_str(lambda));
    
    LN3XCs = placet_get_name_number_list("$beamlinename", "LN3_XCA0");
    placet_element_set_attribute("$beamlinename", LN3XCs, "short_range_wake",  "Xband_SR_W");
    placet_element_set_attribute("$beamlinename", LN3XCs, "lambda", $xband_str(lambda));
    
    LN3SCs = placet_get_name_number_list("$beamlinename", "LN3_SCA0");
    placet_element_set_attribute("$beamlinename", LN3SCs, "short_range_wake",  "Sdband_SR_W");
    placet_element_set_attribute("$beamlinename", LN3SCs, "lambda", $sdband_str(lambda));
}

## Tune bending dipoles

Octave {

  ang1 	= ${p_ang1};
  e01 	= ${p_e01};
  ang2 	= ${p_ang2};
  e02 	= ${p_e02};
  angt 	= ${p_angt};
  e0t 	= ${p_e0t};

  printf("DEBUG:: Tuning BC1 ... Tune parameters: %f %f \n",ang1,e01);

  BC1Ds = placet_get_name_number_list("$beamlinename", "BC1_DP_DIP*");
  BC1D1 = placet_get_name_number_list("$beamlinename", "BC1_DP_DIP1");
  BC1D2 = placet_get_name_number_list("$beamlinename", "BC1_DP_DIP2");
  BC1D3 = placet_get_name_number_list("$beamlinename", "BC1_DP_DIP3");
  BC1D4 = placet_get_name_number_list("$beamlinename", "BC1_DP_DIP4");
  placet_element_set_attribute("$beamlinename", BC1D1, "angle", -ang1); # rad
  placet_element_set_attribute("$beamlinename", BC1D2, "angle", ang1);
  placet_element_set_attribute("$beamlinename", BC1D3, "angle", ang1);
  placet_element_set_attribute("$beamlinename", BC1D4, "angle", -ang1);
  placet_element_set_attribute("$beamlinename", BC1D1, "E2", -ang1);
  placet_element_set_attribute("$beamlinename", BC1D2, "E1", ang1);
  placet_element_set_attribute("$beamlinename", BC1D3, "E2", ang1);
  placet_element_set_attribute("$beamlinename", BC1D4, "E1", -ang1);
  placet_element_set_attribute("$beamlinename", BC1Ds, "e0", e01); # GeV

  printf("DEBUG:: Tuning BC2 ... Tune parameters: %f %f \n",ang2,e02);

  BC2Ds = placet_get_name_number_list("$beamlinename", "BC2_DP_DIP*");
  BC2D1 = placet_get_name_number_list("$beamlinename", "BC2_DP_DIP1");
  BC2D2 = placet_get_name_number_list("$beamlinename", "BC2_DP_DIP2");
  BC2D3 = placet_get_name_number_list("$beamlinename", "BC2_DP_DIP3");
  BC2D4 = placet_get_name_number_list("$beamlinename", "BC2_DP_DIP4");
  placet_element_set_attribute("$beamlinename", BC2D1, "angle", -ang2); # rad
  placet_element_set_attribute("$beamlinename", BC2D2, "angle", ang2);
  placet_element_set_attribute("$beamlinename", BC2D3, "angle", ang2);
  placet_element_set_attribute("$beamlinename", BC2D4, "angle", -ang2);
  placet_element_set_attribute("$beamlinename", BC2D1, "E2", -ang2);
  placet_element_set_attribute("$beamlinename", BC2D2, "E1", ang2);
  placet_element_set_attribute("$beamlinename", BC2D3, "E2", ang2);
  placet_element_set_attribute("$beamlinename", BC2D4, "E1", -ang2);
  placet_element_set_attribute("$beamlinename", BC2Ds, "e0", e02); # GeV

  printf("DEBUG:: Tuning TMC ... Tune parameters: %f %f \n",angt,e0t);

  TMCDs = placet_get_name_number_list("$beamlinename", "TMC_DP_DIP*");
  TMCD1 = placet_get_name_number_list("$beamlinename", "TMC_DP_DIP1");
  TMCD2 = placet_get_name_number_list("$beamlinename", "TMC_DP_DIP2");
  TMCD3 = placet_get_name_number_list("$beamlinename", "TMC_DP_DIP3");
  TMCD4 = placet_get_name_number_list("$beamlinename", "TMC_DP_DIP4");
  placet_element_set_attribute("$beamlinename", TMCD1, "angle", -angt); # rad
  placet_element_set_attribute("$beamlinename", TMCD2, "angle", angt);
  placet_element_set_attribute("$beamlinename", TMCD3, "angle", angt);
  placet_element_set_attribute("$beamlinename", TMCD4, "angle", -angt);
  placet_element_set_attribute("$beamlinename", TMCD1, "E2", -angt);
  placet_element_set_attribute("$beamlinename", TMCD2, "E1", angt);
  placet_element_set_attribute("$beamlinename", TMCD3, "E2", angt);
  placet_element_set_attribute("$beamlinename", TMCD4, "E1", -angt);
  placet_element_set_attribute("$beamlinename", TMCDs, "e0", e0t); # GeV
}

    
## Allows 6d tracking in bunch compression

Octave {

  SIs = placet_get_number_list("$beamlinename", "sbend");
  placet_element_set_attribute("$beamlinename", SIs, "six_dim", true);
  placet_element_set_attribute("$beamlinename", SIs, "csr", true);
  placet_element_set_attribute("$beamlinename", SIs, "csr_charge", 75e-12);
  placet_element_set_attribute("$beamlinename", SIs, "csr_nbins", 50);
  placet_element_set_attribute("$beamlinename", SIs, "csr_nsectors", 10);
  placet_element_set_attribute("$beamlinename", SIs, "csr_filterorder", 1);
  placet_element_set_attribute("$beamlinename", SIs, "csr_nhalffilter", 2);

  QDs = placet_get_number_list("$beamlinename", "quadrupole");
  placet_element_set_attribute("$beamlinename", QDs, "six_dim", true);
}

## Tune final matching quadrupoles

Octave {
  QSs = ${p_FMQS}{1};
  printf("DEBUG:: Tuning final matching quadrupole strengths ... \n");
  printf("DEBUG:: Strengths: %s \n", sprintf("%.2f ",QSs{:}));
  MQs = placet_get_name_number_list("$beamlinename", "HXR_QD_*");
  NQ = length(MQs);
  if (NQ != length(QSs))
    disp("ERROR:: wrong number of matching quadrupole parameters!");
  endif
  for i = 1:NQ
    placet_element_set_attribute("$beamlinename", MQs (i), "strength",  QSs{i});
  endfor
}

## Apply apertures

Octave {
  #II = placet_get_name_number_list("$beamlinename", "*");
  #placet_element_set_attribute("$beamlinename", II, "aperture_x", 0.5); # m
  #placet_element_set_attribute("$beamlinename", II, "aperture_y", 0.5); # m
  #placet_element_set_attribute("$beamlinename", II, "aperture_shape", "circular");
}

## Save beam at watch points
    
if { $option != "incoh_1" && $option != "incoh_2" && $option != "incoh_3" } {
  Octave {
    WAList = placet_get_name_number_list("$beamlinename", "*_WA_*");
    placet_element_set_attribute("$beamlinename", WAList, "tclcall_exit", "save_watch");
  }
}





# if { $option != "incoh_1" && $option != "incoh_2" && $option != "incoh_3" } {
#   Octave {
#    watches = placet_get_name_number_list("$beamlinename", "*_WA_*");
#    placet_element_set_attribute("$beamlinename", watches, "tclcall_exit", "octave_save");
# # 
# #     [~,nb]=size(Beams);
# #     Tcl_SetVar("nbnch", nb);
# #     for i=1:nb
# #         B=Beams{i};
# #         fnm=strcat("watch", num2str(i), ".dat");
# #         save("-text", "-ascii",fnm,"B");
# #      endfor
#   }
# #    fnames={'inj_in.dat', 'ln0_lnz_in.dat', 'ln0_ou.dat', 'bc1_ou.dat', 'bc1_dia_mtch_ou.dat', 'ln1_ou.dat', 'bc2_ou.dat', 'ln2_ou.dat', 'ln2_dia_ou.dat', 'ln2_bp_ou.dat', 'ln3_ou.dat', 'ln3_bp_ou.dat', 'tmc_ou.dat', 'hxr_ou.dat'};
# }


##---------

if { $option == "lwt" } {
  set band "xband"
  source $scr_dir/lwt.tcl
  set band "cband"
  source $scr_dir/lwt.tcl
  exit
} elseif { $option == "incoh_1" || $option == "incoh_2" || $option == "incoh_3" } {
  source ${scr_dir}/${option}.tcl
} else {
  source ${scr_dir}/nominal.tcl
}

puts $iwatch
set fnames {"inj_in_p.sdds"  "ln0_lnz_in_p.sdds"  "ln0_ou_p.sdds"  "bc1_ou_p.sdds"  "bc1_dia_mtch_ou_p.sdds"  "ln1_ou_p.sdds"  "bc2_ou_p.sdds"  "ln2_ou_p.sdds"  "ln2_dia_ou_p.sdds"  "ln2_bp_ou_p.sdds"  "ln3_ou_p.sdds"  "ln3_bp_ou_p.sdds"  "tmc_ou_p.sdds"  "hxr_ou_p.sdds"};
for {set i 0} { $i < $iwatch} {incr i} {
    set k [expr $i+1]
    set filename "Watch_${k}.dat"
    set filename2 [lindex $fnames $i]
    exec placet2elegant.tcl $filename $filename2 75.0 
}

exit ;# END

