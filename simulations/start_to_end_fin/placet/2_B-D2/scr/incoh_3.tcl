
Octave {
  
  emit_file_format = "%s %ex %sex %x %sx %ey %sey %Env %sy %n";

  #BeamFormat = "e/GeV  x/um  y/um  z/um  xp/urad  yp/urad";

  #mkdir ${out_dir}/RandomBeams

  ## Get nominal numbers

  LN0Cs = placet_get_name_number_list("$beamlinename", "LN0_CCA0");
  GRD0_LN0Cs = placet_element_get_attribute("$beamlinename", LN0Cs, "gradient");
  PHS0_LN0Cs = placet_element_get_attribute("$beamlinename", LN0Cs, "phase");

  LN1Cs = placet_get_name_number_list("$beamlinename", "LN1_XCA0");
  GRD0_LN1Cs = placet_element_get_attribute("$beamlinename", LN1Cs, "gradient");
  PHS0_LN1Cs = placet_element_get_attribute("$beamlinename", LN1Cs, "phase");

  LN2Cs = placet_get_name_number_list("$beamlinename", "LN2_XCA0");
  GRD0_LN2Cs = placet_element_get_attribute("$beamlinename", LN2Cs, "gradient");
  PHS0_LN2Cs = placet_element_get_attribute("$beamlinename", LN2Cs, "phase");

  LN3Cs = placet_get_name_number_list("$beamlinename", "LN3_XCA0");
  GRD0_LN3Cs = placet_element_get_attribute("$beamlinename", LN3Cs, "gradient");
  PHS0_LN3Cs = placet_element_get_attribute("$beamlinename", LN3Cs, "phase");

  for i = 0 : str2num("$n_rand")

    ## C- and X- band errors
    grdj = 0.04; # %   rms
    phsj = 0.05; # deg rms
    if (i == 0)
      SF_GRD_LN0 = ones( 6,1);# for LN0 6 C-band, 2 per group
      SF_GRD_LN1 = ones(16,1);# for LN1 16 X-band, 4 per group
      SF_GRD_LN2 = ones( 8,1);# for LN2 8 X-band, 4 per group
      SF_GRD_LN3 = ones(68,1);# for LN3 68 X-band, 4 per group

      SF_PHS_LN0 = zeros( 6,1);# for LN0 6 C-band, 2 per group
      SF_PHS_LN1 = zeros(16,1);# for LN1 16 X-band, 4 per group
      SF_PHS_LN2 = zeros( 8,1);# for LN2 8 X-band, 4 per group
      SF_PHS_LN3 = zeros(68,1);# for LN3 68 X-band, 4 per group
    else
      SF_GRD_LN0 = repmat(normrnd(1,grdj/100.0,1, 6/2),2,1)(:) ;# for LN0 6 C-band, 2 per group
      SF_GRD_LN1 = repmat(normrnd(1,grdj/100.0,1,16/4),4,1)(:) ;# for LN1 16 X-band, 4 per group
      SF_GRD_LN2 = repmat(normrnd(1,grdj/100.0,1, 8/4),4,1)(:) ;# for LN2 8 X-band, 4 per group
      SF_GRD_LN3 = repmat(normrnd(1,grdj/100.0,1,68/4),4,1)(:) ;# for LN3 68 X-band, 4 per group

      SF_PHS_LN0 = repmat(normrnd(0,phsj,1, 6/2),2,1)(:) ;# for LN0 6 C-band, 2 per group
      SF_PHS_LN1 = repmat(normrnd(0,phsj,1,16/4),4,1)(:) ;# for LN1 16 X-band, 4 per group
      SF_PHS_LN2 = repmat(normrnd(0,phsj,1, 8/4),4,1)(:) ;# for LN2 8 X-band, 4 per group
      SF_PHS_LN3 = repmat(normrnd(0,phsj,1,68/4),4,1)(:) ;# for LN3 68 X-band, 4 per group
    endif
        
    ## Set gradient and phase jitters

    GRD_LN0Cs = GRD0_LN0Cs .* SF_GRD_LN0;
    PHS_LN0Cs = PHS0_LN0Cs + SF_PHS_LN0;
    placet_element_set_attribute("$beamlinename", LN0Cs, "gradient", GRD_LN0Cs);
    placet_element_set_attribute("$beamlinename", LN0Cs, "phase", PHS_LN0Cs);
      
    GRD_LN1Cs = GRD0_LN1Cs .* SF_GRD_LN1;
    PHS_LN1Cs = PHS0_LN1Cs + SF_PHS_LN1;
    placet_element_set_attribute("$beamlinename", LN1Cs, "gradient", GRD_LN1Cs);
    placet_element_set_attribute("$beamlinename", LN1Cs, "phase", PHS_LN1Cs);
      
    GRD_LN2Cs = GRD0_LN2Cs .* SF_GRD_LN2;
    PHS_LN2Cs = PHS0_LN2Cs + SF_PHS_LN2;
    placet_element_set_attribute("$beamlinename", LN2Cs, "gradient", GRD_LN2Cs);
    placet_element_set_attribute("$beamlinename", LN2Cs, "phase", PHS_LN2Cs);
      
    GRD_LN3Cs = GRD0_LN3Cs .* SF_GRD_LN3;
    PHS_LN3Cs = PHS0_LN3Cs + SF_PHS_LN3;
    placet_element_set_attribute("$beamlinename", LN3Cs, "gradient", GRD_LN3Cs);
    placet_element_set_attribute("$beamlinename", LN3Cs, "phase", PHS_LN3Cs);

    ## Injector laser gun timing jitter

    tmj = 75; # fs rms
    if (i == 0)
      dt_fs = 0;
    else
      dt_fs = normrnd(0,tmj); # fs
    endif
    B0 = placet_get_beam(["beam" num2str(i)]);
    B = B0;
    B(:,4) += dt_fs * 0.299792458; # um
    placet_set_beam(["beam" num2str(i)], B);

    save ("-ascii", ["${out_dir}/RandomBeams/TimingFS" num2str(i) ".dat"], "dt_fs");

    ## Tracking

    [E, B] = placet_test_no_correction("$beamlinename", ["beam" num2str(i)], "None", emit_file_format);

    ## Save beam

    save ("-ascii", ["${out_dir}/RandomBeams/Beam" num2str(i) ".dat"], "B");

    ## Save gradients and phases

    Gradients = [ GRD_LN0Cs; GRD_LN1Cs; GRD_LN2Cs; GRD_LN3Cs ];
    Phases    = [ PHS_LN0Cs; PHS_LN1Cs; PHS_LN2Cs; PHS_LN3Cs ];

    save ("-ascii", ["${out_dir}/RandomBeams/Gradients" num2str(i) ".dat"], "Gradients");
    save ("-ascii", ["${out_dir}/RandomBeams/Phases" num2str(i) ".dat"], "Phases");

  endfor
  
}


exit;
