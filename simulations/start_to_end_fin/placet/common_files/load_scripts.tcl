## constants
set clight 299792458.0
set pi [expr acos(-1.)]
set Z0 [expr 120*$pi]
set eC 1.6021766e-19
set mc2 0.510998910e-3

# #  some file names

set script_dir0 "../../common_files/"





# load related octave and tcl scripts

source $script_dir0/structure_parameters.tcl

Octave {
    scriptdir="$script_dir0/";
    source([scriptdir,"generate_bunch_apr_18.m"]);
    source([scriptdir,"tools_nov_21.m"]);
    source([scriptdir,"wake_init.m"]);
}

source $script_dir0/wake_init.tcl
source $script_dir0/tools.tcl
source $script_dir0/make_beam_apr_18.tcl


#########################################################################################
# change font color
#----------------------------------------------------------------------------------------
proc color {foreground text} {
    return [exec tput setaf $foreground] $text[exec tput sgr0]
}
