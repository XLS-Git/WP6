1;
% file contains some functions used for manuplation of bunch file ...
% 

%----------------------------------------
%  smoothing 1d vector 3rd order
function smoothout=my_smooth(vector) 
  n=length(vector);
  tmp=[];
  for k=1:5
    tmp(1)=vector(1);
    a1=vector(1);
    a2=vector(2);
    a3=vector(3);
    tmp(2)=(a1+a2+a3)/3;
    
    for i=3:n-2
      sum=0;
      for j=1:5
	sum=sum+vector(i+j-3);
      end;
      tmp(i)=sum/5;
    end;
    a1=vector(n-2);
    a2=vector(n-1);
    a3=vector(n);
    tmp(n-1)=(a1+a2+a3)/3;
    tmp(n)=vector(n);
    vector=tmp;
   end;
  smoothout=vector;
end

%----------------------------------------
%  returns the histogram data for longitudinal coordinat
function hist=beam_histogram(beam,nslice) 
  B=load(beam);
  z=sort(B(:,4));
  minz=z(1);
  maxz=z(end);
  [count zl]=hist(z(2:end-1),nslice-2);
  zl=[minz zl maxz];
  count=[1 count 1];
  count=count./sum(count);
  count=my_smooth(count);
  hist=[zl' count'];   
end



%----------------------------------------
% calculates the variance of a vector
function suu=var1d(u) 
    um=mean(u);
    suu=sum((u-um).^2)/length(u);
endfunction


%----------------------------------------
% calculates the variance of two vector
function suv=var2d(u,v) 
    lu=length(u);
    lv=length(v);

    if (lu != lv)
        suv=0.0;
        return;
    end;

    um=mean(u);
    vm=mean(v);
    suv=sum((u-um).*(v-vm))/lu;
    
end


%----------------------------------------
% calculates the variance of two vector
function suv=mysigma(u,v) 
    lu=length(u);
    lv=length(v);

    if (lu != lv)
        suv=0.0;
        return;
    end;
   suv=sum((u).*(v))/lu;
end


%----------------------------------------
% calculates the unprojected emittance of two vector
function emt=eps(u,v) 
    lu=length(u);
    lv=length(v);
    if (lu != lv)
        eps=0.0;
        return;
    end;
    ruu=var1d(u);
    rvv=var1d(v);
    ruv=var2d(u,v);
    emt=sqrt(ruu*rvv-ruv*ruv);
endfunction





%------------------------------------------------------------------------
%  this function calculates the transverse twiss parameters
function twissout=twisscalcfile(filein) 
   m0c2=0.00051099893;
   B=load(filein);
   [zs,id]=sort(B(:,4));
   B=B(id,:);
   e=B(:,1);
   x=B(:,2);
   y=B(:,3);
   z=B(:,4);
   xp=B(:,5);
   yp=B(:,6);

   e0=mean(e);
   g0=e0/m0c2+1;
   z0=mean(z);
   x0=mean(x);
   y0=mean(y);
   xp0=mean(xp);
   yp0=mean(yp);
   
   sxx=var1d(x);
   sxpxp=var1d(xp);
   sxxp=var2d(x,xp);
   epsx=sqrt(sxx*sxpxp-sxxp*sxxp);
   bx=sxx/epsx;
   ax=-sxxp/epsx;

   syy  =var1d(y);
   sypyp=var1d(yp);
   syyp =var2d(y,yp);
   epsy =sqrt(syy*sypyp-syyp*syyp);
   by   =syy/epsy;
   ay   =-syyp/epsy;

%     outpar=[x0,xp0,ax,bx,epsx,y0,yp0,ay,by,epsy,g0];
 
    pars=struct('x0', x0, 'xp0', xp0, 'alpha_x', ax, 'beta_x', bx, 'emit_x', epsx, 'y0', y0, 'yp0', yp0, 'alpha_y', ay, 'beta_y', by, 'emit_y', epsy);
   
   twissout=pars;
   
end


%------------------------------------------------------------------------
%  this function calculates the transverse twiss parameters
function [x0,xp0,ax,bx,epsx,y0,yp0,ay,by,epsy,g0]=twisscalcbeam(beam) 
   m0c2=0.00051099893;

   B=beam; 
   [zs,id]=sort(B(:,4));
   B=B(id,:);
   e=B(:,1);
   x=B(:,2);
   y=B(:,3);
   z=B(:,4);
   xp=B(:,5);
   yp=B(:,6);

   e0=mean(e);
   g0=e0/m0c2+1;
   z0=mean(z);
   x0=mean(x);
   y0=mean(y);
   xp0=mean(xp);
   yp0=mean(yp);
   
   sxx=var1d(x);
   sxpxp=var1d(xp);
   sxxp=var2d(x,xp);
   epsx=sqrt(sxx*sxpxp-sxxp*sxxp);
   bx=sxx/epsx;
   ax=-sxxp/epsx;

   syy  =var1d(y);
   sypyp=var1d(yp);
   syyp =var2d(y,yp);
   epsy =sqrt(syy*sypyp-syyp*syyp);
   by   =syy/epsy;
   ay   =-syyp/epsy;

   
end


%------------------------------------------------------------------------
%  this function calculates the transverse twiss parameters
function calcout=calc_beam_pars(filein) 

    B= filein;
    
    m0c2=0.51099893;
   
    [zs,id]=sort(B(:,4));
    B=B(id,:);
    w=B(:,1)*1e3;  # MeV
    x=B(:,2)*1e-3;  # mm
    y=B(:,3)*1e-3;  # mm
    z=B(:,4)*1e-3;  # mm
    xp=B(:,5)*1e-3; # mrad
    yp=B(:,6)*1e-3; # mrad
    dw= (w-mean(w))/mean(w) ;# relative
    
    w0=mean(w);
    g0=w0/m0c2+1;
    z0=mean(z);
    x0=mean(x);
    y0=mean(y);
    xp0=mean(xp);
    yp0=mean(yp);
    dw0=mean(dw);

    sig_xx  =mysigma(x-x0,x-x0);
    sig_xpxp=mysigma(xp-xp0,xp-xp0);
    sig_xxp =mysigma(x-x0,xp-xp0);
    emt_x =sqrt( sig_xx * sig_xpxp - sig_xxp * sig_xxp );
    
    sig_yy  =mysigma(y-y0,y-y0);
    sig_ypyp=mysigma(yp-yp0,yp-yp0);
    sig_yyp =mysigma(y-y0,yp-yp0);
    emt_y =sqrt( sig_yy * sig_ypyp - sig_yyp * sig_yyp );
    
    sig_zz=mysigma(z-z0,z-z0);
    sig_ww=mysigma(w-w0,w-w0);
    
%      sig_zw=mysigma(z-z0,w-w0)
%      emt_z =sqrt( sig_zz * sig_ww - sig_zw * sig_zw )*1000

    sig_dwdw=mysigma(dw-dw0,dw-dw0);
    sig_xdw =mysigma(x-x0,dw-dw0);
    sig_xpdw=mysigma(xp-xp0,dw-dw0);
    sig_ydw =mysigma(y-y0,dw-dw0);
    sig_ypdw=mysigma(yp-yp0,dw-dw0); 
   
    Dx =sig_xdw/sig_dwdw;
    Dxp=sig_xpdw/sig_dwdw;

    Dy=sig_ydw/sig_dwdw;
    Dyp=sig_ypdw/sig_dwdw;
   
    ebetx =  sig_xx   - sig_xdw*sig_xdw/sig_dwdw;
    egamx =  sig_xpxp - sig_xpdw*sig_xpdw/sig_dwdw;
    ealfx = -sig_xxp  + sig_xdw*sig_xpdw/sig_dwdw;

    ebety =  sig_yy   - sig_ydw*sig_ydw/sig_dwdw;
    egamy =  sig_ypyp - sig_ypdw*sig_ypdw/sig_dwdw;
    ealfy = -sig_yyp  + sig_ydw*sig_ypdw/sig_dwdw;
    
    emt_cx = sqrt(ebetx*egamx-ealfx*ealfx);
    emt_cy = sqrt(ebety*egamy-ealfy*ealfy);
    
    sx=sqrt(sig_xx)*1e3;  #um
    sy=sqrt(sig_yy)*1e3;  #um
    sz=sqrt(sig_zz)*1e3;  #um
    sw =sqrt(sig_ww);     # MeV
    dE=sw*100/w0;         # relative         
    x0=x0*1e3;            #um
    y0=y0*1e3;            #um
    z0=z0*1e3;            #um
    w0=w0*1e-3;           #GeV
    emt_xn=emt_x*g0;      #mm.mrad
    emt_yn=emt_y*g0;      #mm.mrad
    emt_xcn=emt_cx*g0;    #mm.mrad
    emt_ycn=emt_cy*g0;    #mm.mrad
   
    calcout=[w0 x0 y0 z0  sw sx sy sz dE emt_xn emt_yn emt_xcn emt_ycn];

endfunction


%------------------------------------------------------------------------
%  this function calculates the transverse twiss parameters
function shiftbeam(filein,dt,fileout) 
   cc=299792458;
   dz=dt*cc*1e6;
   B=load(filein);
   B(:,4)=B(:,4)+dz;
   printf('%s beam shifted by %f um',filein, dz)
   save("-text", "-ascii",fileout,"B");
%     save -text -ascii 'fileout' B;
end


