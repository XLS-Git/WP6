Girder
SetReferenceEnergy   1.238601e-01
Drift      -name "LN0_DR_V5"        -length  6.639433e-01 -e0  1.237690e-01
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  1.237690e-01
Quadrupole -name "LN0_QD_V04"       -length  4.000000e-02 -e0  1.237690e-01 -strength  1.779631e-02
Dipole     -name "LN0_COR"          -length  0.000000e+00 -e0  1.237690e-01
Bpm        -name "LN0_BPM"          -length  0.000000e+00 -e0  1.237690e-01
Quadrupole -name "LN0_QD_V04"       -length  4.000000e-02 -e0  1.237690e-01 -strength  1.779631e-02
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  1.237690e-01
Drift      -name "LN0_DR_V6"        -length  2.598929e-01 -e0  1.237690e-01
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  1.237690e-01
Quadrupole -name "LN0_QD_V05"       -length  4.000000e-02 -e0  1.237690e-01 -strength  3.441588e-02
Dipole     -name "LN0_COR"          -length  0.000000e+00 -e0  1.237690e-01
Bpm        -name "LN0_BPM"          -length  0.000000e+00 -e0  1.237690e-01
Quadrupole -name "LN0_QD_V05"       -length  4.000000e-02 -e0  1.237690e-01 -strength  3.441588e-02
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  1.237690e-01
Drift      -name "LN0_DR_V7"        -length  1.419751e+00 -e0  1.237690e-01
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  1.237690e-01
Quadrupole -name "LN0_QD_V06"       -length  4.000000e-02 -e0  1.237690e-01 -strength -3.626929e-02
Dipole     -name "LN0_COR"          -length  0.000000e+00 -e0  1.237690e-01
Bpm        -name "LN0_BPM"          -length  0.000000e+00 -e0  1.237690e-01
Quadrupole -name "LN0_QD_V06"       -length  4.000000e-02 -e0  1.237690e-01 -strength -3.626929e-02
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  1.237690e-01
Drift      -name "LN0_DR_V8"        -length  2.000008e-01 -e0  1.237690e-01
Drift      -name "LN0_DR_VS"        -length  1.000000e-01 -e0  1.237690e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  1.237690e-01
Drift      -name "LN0_DR_XCA_ED_DI" -length  1.001043e-01 -e0  1.237690e-01
CrabCavity -name "LN0_XCA0_DI"      -length  9.997914e-01 -voltage  0.000000e+00 -phase  9.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.237690e-01
Drift      -name "LN0_DR_XCA_ED_DI" -length  1.001043e-01 -e0  1.237285e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  1.237285e-01
Drift      -name "LN0_TDS_DR"       -length  2.500000e-01 -e0  1.237285e-01
Drift      -name "LN0_TDS_DR"       -length  2.500000e-01 -e0  1.237285e-01
Drift      -name "LN0_TDS_DR"       -length  2.500000e-01 -e0  1.237285e-01
Drift      -name "LN0_TDS_DR"       -length  2.500000e-01 -e0  1.237285e-01
Drift      -name "LN0_TDS_DR"       -length  2.500000e-01 -e0  1.237285e-01
Drift      -name "LN0_TDS_DR"       -length  2.500000e-01 -e0  1.237285e-01
Drift      -name "LN0_TDS_DR"       -length  2.500000e-01 -e0  1.237285e-01
Drift      -name "LN0_TDS_DR"       -length  2.500000e-01 -e0  1.237285e-01
Sbend      -name "LN0_DP_TDS"       -length  1.000000e-01 -e0  1.237285e-01 -angle  0.000000e+00 -E1  0.000000e+00 -E2  0.000000e+00 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  0.000000e+00 -six_dim 1
Drift      -name "LN0_DR_20"        -length  2.000000e-01 -e0  1.237285e-01
Drift      -name "LN0_DR_VS"        -length  1.000000e-01 -e0  1.237285e-01
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  1.237285e-01
Quadrupole -name "LN0_QD_FH"        -length  4.000000e-02 -e0  1.237285e-01 -strength  3.464399e-02
Dipole     -name "LN0_COR"          -length  0.000000e+00 -e0  1.237285e-01
Bpm        -name "LN0_BPM"          -length  0.000000e+00 -e0  1.237285e-01
Quadrupole -name "LN0_QD_FH"        -length  4.000000e-02 -e0  1.237285e-01 -strength  3.464399e-02
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  1.237285e-01
Drift      -name "LN0_DR_SV"        -length  1.000000e-01 -e0  1.237285e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  1.237285e-01
Drift      -name "LN0_DR_CCA_ED"    -length  5.019800e-02 -e0  1.237285e-01
Cavity     -name "LN0_CCA0"         -length  1.899604e+00 -gradient  1.500000e-02 -phase  2.600000e+01 -frequency  5.997100e+00
SetReferenceEnergy   1.492816e-01
Drift      -name "LN0_DR_CCA_ED"    -length  5.019800e-02 -e0  1.492816e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  1.492816e-01
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  1.492816e-01
Quadrupole -name "LN0_QD_DH"        -length  4.000000e-02 -e0  1.492816e-01 -strength -4.179885e-02
Dipole     -name "LN0_COR"          -length  0.000000e+00 -e0  1.492816e-01
Bpm        -name "LN0_BPM"          -length  0.000000e+00 -e0  1.492816e-01
Quadrupole -name "LN0_QD_DH"        -length  4.000000e-02 -e0  1.492816e-01 -strength -4.179885e-02
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  1.492816e-01
Drift      -name "LN0_DR_VS"        -length  1.000000e-01 -e0  1.492816e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  1.492816e-01
Drift      -name "LN0_DR_CCA_ED"    -length  5.019800e-02 -e0  1.492816e-01
Cavity     -name "LN0_CCA0"         -length  1.899604e+00 -gradient  1.500000e-02 -phase  2.600000e+01 -frequency  5.997100e+00
SetReferenceEnergy   1.748347e-01
Drift      -name "LN0_DR_CCA_ED"    -length  5.019800e-02 -e0  1.748347e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  1.748347e-01
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  1.748347e-01
Quadrupole -name "LN0_QD_FH"        -length  4.000000e-02 -e0  1.748347e-01 -strength  4.895371e-02
Dipole     -name "LN0_COR"          -length  0.000000e+00 -e0  1.748347e-01
Bpm        -name "LN0_BPM"          -length  0.000000e+00 -e0  1.748347e-01
Quadrupole -name "LN0_QD_FH"        -length  4.000000e-02 -e0  1.748347e-01 -strength  4.895371e-02
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  1.748347e-01
Drift      -name "LN0_DR_VS"        -length  1.000000e-01 -e0  1.748347e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  1.748347e-01
Drift      -name "LN0_DR_CCA_ED"    -length  5.019800e-02 -e0  1.748347e-01
Cavity     -name "LN0_CCA0"         -length  1.899604e+00 -gradient  1.500000e-02 -phase  2.600000e+01 -frequency  5.997100e+00
SetReferenceEnergy   2.003877e-01
Drift      -name "LN0_DR_CCA_ED"    -length  5.019800e-02 -e0  2.003877e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  2.003877e-01
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  2.003877e-01
Quadrupole -name "LN0_QD_DH"        -length  4.000000e-02 -e0  2.003877e-01 -strength -5.610857e-02
Dipole     -name "LN0_COR"          -length  0.000000e+00 -e0  2.003877e-01
Bpm        -name "LN0_BPM"          -length  0.000000e+00 -e0  2.003877e-01
Quadrupole -name "LN0_QD_DH"        -length  4.000000e-02 -e0  2.003877e-01 -strength -5.610857e-02
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  2.003877e-01
Drift      -name "LN0_DR_VS"        -length  1.000000e-01 -e0  2.003877e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  2.003877e-01
Drift      -name "LN0_DR_CCA_ED"    -length  5.019800e-02 -e0  2.003877e-01
Cavity     -name "LN0_CCA0"         -length  1.899604e+00 -gradient  1.500000e-02 -phase  2.600000e+01 -frequency  5.997100e+00
SetReferenceEnergy   2.259408e-01
Drift      -name "LN0_DR_CCA_ED"    -length  5.019800e-02 -e0  2.259408e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  2.259408e-01
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  2.259408e-01
Quadrupole -name "LN0_QD_FH"        -length  4.000000e-02 -e0  2.259408e-01 -strength  6.326343e-02
Dipole     -name "LN0_COR"          -length  0.000000e+00 -e0  2.259408e-01
Bpm        -name "LN0_BPM"          -length  0.000000e+00 -e0  2.259408e-01
Quadrupole -name "LN0_QD_FH"        -length  4.000000e-02 -e0  2.259408e-01 -strength  6.326343e-02
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  2.259408e-01
Drift      -name "LN0_DR_VS"        -length  1.000000e-01 -e0  2.259408e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  2.259408e-01
Drift      -name "LN0_DR_CCA_ED"    -length  5.019800e-02 -e0  2.259408e-01
Cavity     -name "LN0_CCA0"         -length  1.899604e+00 -gradient  1.500000e-02 -phase  2.600000e+01 -frequency  5.997100e+00
SetReferenceEnergy   2.514939e-01
Drift      -name "LN0_DR_CCA_ED"    -length  5.019800e-02 -e0  2.514939e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  2.514939e-01
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  2.514939e-01
Quadrupole -name "LN0_QD_DH"        -length  4.000000e-02 -e0  2.514939e-01 -strength -7.041830e-02
Dipole     -name "LN0_COR"          -length  0.000000e+00 -e0  2.514939e-01
Bpm        -name "LN0_BPM"          -length  0.000000e+00 -e0  2.514939e-01
Quadrupole -name "LN0_QD_DH"        -length  4.000000e-02 -e0  2.514939e-01 -strength -7.041830e-02
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  2.514939e-01
Drift      -name "LN0_DR_VS"        -length  1.000000e-01 -e0  2.514939e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  2.514939e-01
Drift      -name "LN0_DR_CCA_ED"    -length  5.019800e-02 -e0  2.514939e-01
Cavity     -name "LN0_CCA0"         -length  1.899604e+00 -gradient  1.500000e-02 -phase  2.600000e+01 -frequency  5.997100e+00
SetReferenceEnergy   2.770470e-01
Drift      -name "LN0_DR_CCA_ED"    -length  5.019800e-02 -e0  2.770470e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  2.770470e-01
Drift      -name "LN0_WA_LNZ_IN2"   -length  0.000000e+00 -e0  2.770470e-01
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  2.770470e-01
Quadrupole -name "LN0_QD_FH"        -length  4.000000e-02 -e0  2.770470e-01 -strength  7.757317e-02
Dipole     -name "LN0_COR"          -length  0.000000e+00 -e0  2.770470e-01
Bpm        -name "LN0_BPM"          -length  0.000000e+00 -e0  2.770470e-01
Quadrupole -name "LN0_QD_FH"        -length  4.000000e-02 -e0  2.770470e-01 -strength  7.757317e-02
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  2.770470e-01
Drift      -name "LN0_DR_VS"        -length  1.000000e-01 -e0  2.770470e-01
Drift      -name "LN0_DR_LNZ_ED2"   -length  6.250000e-01 -e0  2.770470e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  2.770470e-01
Drift      -name "LN0_DR_KCA_ED"    -length  2.225410e-02 -e0  2.770470e-01
Cavity     -name "LN0_KCA0"         -length  3.054918e-01 -gradient  2.453617e-02 -phase  1.920000e+02 -frequency  3.598260e+01
SetReferenceEnergy   2.698730e-01
Drift      -name "LN0_DR_KCA_ED"    -length  2.225410e-02 -e0  2.698730e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  2.698730e-01
Drift      -name "LN0_DR_KCA_ED"    -length  2.225410e-02 -e0  2.698730e-01
Drift      -name "LN0_DR_KCA0"      -length  3.054918e-01 -e0  2.698730e-01
Drift      -name "LN0_DR_KCA_ED"    -length  2.225410e-02 -e0  2.698730e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  2.698730e-01
Drift      -name "LN0_DR_LNZ_ED2"   -length  6.250000e-01 -e0  2.698730e-01
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  2.698730e-01
Quadrupole -name "LN0_QD_DH"        -length  4.000000e-02 -e0  2.698730e-01 -strength -7.556445e-02
Dipole     -name "LN0_COR"          -length  0.000000e+00 -e0  2.698730e-01
Bpm        -name "LN0_BPM"          -length  0.000000e+00 -e0  2.698730e-01
Quadrupole -name "LN0_QD_DH"        -length  4.000000e-02 -e0  2.698730e-01 -strength -7.556445e-02
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  2.698730e-01
Drift      -name "LN0_DR_DR"        -length  1.000000e-01 -e0  2.698730e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  2.698730e-01
Drift      -name "LN0_DR_CCA_ED"    -length  5.019800e-02 -e0  2.698730e-01
Drift      -name "LN0_DR_CCA0H"     -length  3.498020e-01 -e0  2.698730e-01
Drift      -name "LN0_DR_A"         -length  2.000000e-01 -e0  2.698730e-01
Drift      -name "LN0_DR_VS"        -length  1.000000e-01 -e0  2.698730e-01
Drift      -name "LN0_DR_A"         -length  2.000000e-01 -e0  2.698730e-01
Drift      -name "LN0_DR_CT"        -length  1.000000e-01 -e0  2.698730e-01
Drift      -name "LN0_WA_OU2"       -length  0.000000e+00 -e0  2.698730e-01
Drift      -name "BC1_DR_V1"        -length  5.382067e-01 -e0  2.698730e-01
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.698730e-01
Quadrupole -name "BC1_QD_V01"       -length  4.000000e-02 -e0  2.698730e-01 -strength  2.054140e-02
Dipole     -name "BC1_COR"          -length  0.000000e+00 -e0  2.698730e-01
Bpm        -name "BC1_BPM"          -length  0.000000e+00 -e0  2.698730e-01
Quadrupole -name "BC1_QD_V01"       -length  4.000000e-02 -e0  2.698730e-01 -strength  2.054140e-02
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.698730e-01
Drift      -name "BC1_DR_V2"        -length  5.900340e+00 -e0  2.698730e-01
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.698730e-01
Quadrupole -name "BC1_QD_V02"       -length  4.000000e-02 -e0  2.698730e-01 -strength -9.874167e-02
Dipole     -name "BC1_COR"          -length  0.000000e+00 -e0  2.698730e-01
Bpm        -name "BC1_BPM"          -length  0.000000e+00 -e0  2.698730e-01
Quadrupole -name "BC1_QD_V02"       -length  4.000000e-02 -e0  2.698730e-01 -strength -9.874167e-02
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.698730e-01
Drift      -name "BC1_DR_V3"        -length  2.496128e-01 -e0  2.698730e-01
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.698730e-01
Quadrupole -name "BC1_QD_V03"       -length  4.000000e-02 -e0  2.698730e-01 -strength  3.530922e-02
Dipole     -name "BC1_COR"          -length  0.000000e+00 -e0  2.698730e-01
Bpm        -name "BC1_BPM"          -length  0.000000e+00 -e0  2.698730e-01
Quadrupole -name "BC1_QD_V03"       -length  4.000000e-02 -e0  2.698730e-01 -strength  3.530922e-02
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.698730e-01
Drift      -name "BC1_DR_V4"        -length  2.000000e-01 -e0  2.698730e-01
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.698730e-01
Quadrupole -name "BC1_QD_V04"       -length  4.000000e-02 -e0  2.698730e-01 -strength  5.701369e-02
Dipole     -name "BC1_COR"          -length  0.000000e+00 -e0  2.698730e-01
Bpm        -name "BC1_BPM"          -length  0.000000e+00 -e0  2.698730e-01
Quadrupole -name "BC1_QD_V04"       -length  4.000000e-02 -e0  2.698730e-01 -strength  5.701369e-02
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.698730e-01
Drift      -name "BC1_DR_V5"        -length  2.002040e-01 -e0  2.698730e-01
Drift      -name "BC1_DR_20"        -length  5.000000e-01 -e0  2.698730e-01
Sbend      -name "BC1_DP_DIP1"      -length  4.002981e-01 -e0  2.698729e-01 -angle -6.684611e-02 -E1  0.000000e+00 -E2 -6.684611e-02 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  0.000000e+00  -six_dim 1 -csr 1 
Drift      -name "BC1_DR_SIDE_C"    -length  5.000000e-01 -e0  2.698720e-01
Drift      -name "BC1_DR_SIDE"      -length  2.757275e+00 -e0  2.698720e-01
Sbend      -name "BC1_DP_DIP2"      -length  4.002981e-01 -e0  2.698716e-01 -angle  6.684611e-02 -E1  6.684611e-02 -E2  0.000000e+00 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  0.000000e+00  -six_dim 1 -csr 1 
Drift      -name "BC1_DR_CENT"      -length  3.500000e-01 -e0  2.698716e-01
Bpm        -name "BC1_BPM"          -length  0.000000e+00 -e0  2.698716e-01
Drift      -name "BC1_DR_CENT"      -length  3.500000e-01 -e0  2.698716e-01
Sbend      -name "BC1_DP_DIP3"      -length  4.002981e-01 -e0  2.698622e-01 -angle  6.684611e-02 -E1  0.000000e+00 -E2  6.684611e-02 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  0.000000e+00  -six_dim 1 -csr 1 
Drift      -name "BC1_DR_SIDE_C"    -length  5.000000e-01 -e0  2.698361e-01
Drift      -name "BC1_DR_SIDE"      -length  2.757275e+00 -e0  2.698361e-01
Sbend      -name "BC1_DP_DIP4"      -length  4.002981e-01 -e0  2.698120e-01 -angle -6.684611e-02 -E1 -6.684611e-02 -E2  0.000000e+00 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  0.000000e+00  -six_dim 1 -csr 1 
Drift      -name "BC1_DR_20_C"      -length  5.000000e-01 -e0  2.697895e-01
Drift      -name "BC1_WA_OU2"       -length  0.000000e+00 -e0  2.697895e-01
Drift      -name "BC1_DR_CT"        -length  1.000000e-01 -e0  2.697895e-01
Drift      -name "BC1_DR_DI_V1"     -length  2.911672e-01 -e0  2.697895e-01
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.697895e-01
Quadrupole -name "BC1_QD_DI_V01"    -length  4.000000e-02 -e0  2.697895e-01 -strength -9.254272e-02
Dipole     -name "BC1_COR_DI"       -length  0.000000e+00 -e0  2.697895e-01
Bpm        -name "BC1_BPM_DI"       -length  0.000000e+00 -e0  2.697895e-01
Quadrupole -name "BC1_QD_DI_V01"    -length  4.000000e-02 -e0  2.697895e-01 -strength -9.254272e-02
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.697895e-01
Drift      -name "BC1_DR_DI_V2"     -length  4.700020e-01 -e0  2.697895e-01
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.697895e-01
Quadrupole -name "BC1_QD_DI_V02"    -length  4.000000e-02 -e0  2.697895e-01 -strength  1.451457e-01
Dipole     -name "BC1_COR_DI"       -length  0.000000e+00 -e0  2.697895e-01
Bpm        -name "BC1_BPM_DI"       -length  0.000000e+00 -e0  2.697895e-01
Quadrupole -name "BC1_QD_DI_V02"    -length  4.000000e-02 -e0  2.697895e-01 -strength  1.451457e-01
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.697895e-01
Drift      -name "BC1_DR_DI_V3"     -length  1.199083e+00 -e0  2.697895e-01
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.697895e-01
Quadrupole -name "BC1_QD_DI_V03"    -length  4.000000e-02 -e0  2.697895e-01 -strength -5.534839e-02
Dipole     -name "BC1_COR_DI"       -length  0.000000e+00 -e0  2.697895e-01
Bpm        -name "BC1_BPM_DI"       -length  0.000000e+00 -e0  2.697895e-01
Quadrupole -name "BC1_QD_DI_V03"    -length  4.000000e-02 -e0  2.697895e-01 -strength -5.534839e-02
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.697895e-01
Drift      -name "BC1_DR_DI_V5"     -length  2.000000e-01 -e0  2.697895e-01
Drift      -name "BC1_DR_DI"        -length  2.500000e-01 -e0  2.697895e-01
Drift      -name "BC1_DR_VS"        -length  1.000000e-01 -e0  2.697895e-01
Drift      -name "BC1_DR_BL"        -length  5.000000e-02 -e0  2.697895e-01
Drift      -name "BC1_DR_XCA_ED_DI" -length  1.001043e-01 -e0  2.697895e-01
CrabCavity -name "BC1_XCA0_DI"      -length  9.997914e-01 -voltage  0.000000e+00 -phase  9.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.697895e-01
Drift      -name "BC1_DR_XCA_ED_DI" -length  1.001043e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_BL"        -length  5.000000e-02 -e0  2.697215e-01
Drift      -name "BC1_DR_VS"        -length  1.000000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_DI"        -length  2.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_DI"        -length  2.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_DI_QD_ED"  -length  4.250000e-02 -e0  2.697215e-01
Quadrupole -name "BC1_QD_DI_FH"     -length  4.000000e-02 -e0  2.697215e-01 -strength  8.631089e-02
Dipole     -name "BC1_COR_DI"       -length  0.000000e+00 -e0  2.697215e-01
Bpm        -name "BC1_BPM_DI"       -length  0.000000e+00 -e0  2.697215e-01
Quadrupole -name "BC1_QD_DI_FH"     -length  4.000000e-02 -e0  2.697215e-01 -strength  8.631089e-02
Drift      -name "BC1_DR_DI_QD_ED"  -length  4.250000e-02 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_DI_VS"     -length  1.000000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_DI_QD_ED"  -length  4.250000e-02 -e0  2.697215e-01
Quadrupole -name "BC1_QD_DI_DH"     -length  4.000000e-02 -e0  2.697215e-01 -strength -8.631089e-02
Dipole     -name "BC1_COR_DI"       -length  0.000000e+00 -e0  2.697215e-01
Bpm        -name "BC1_BPM_DI"       -length  0.000000e+00 -e0  2.697215e-01
Quadrupole -name "BC1_QD_DI_DH"     -length  4.000000e-02 -e0  2.697215e-01 -strength -8.631089e-02
Drift      -name "BC1_DR_DI_QD_ED"  -length  4.250000e-02 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_DI_VS"     -length  1.000000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_DI_QD_ED"  -length  4.250000e-02 -e0  2.697215e-01
Quadrupole -name "BC1_QD_DI_FH"     -length  4.000000e-02 -e0  2.697215e-01 -strength  8.631089e-02
Dipole     -name "BC1_COR_DI"       -length  0.000000e+00 -e0  2.697215e-01
Bpm        -name "BC1_BPM_DI"       -length  0.000000e+00 -e0  2.697215e-01
Quadrupole -name "BC1_QD_DI_FH"     -length  4.000000e-02 -e0  2.697215e-01 -strength  8.631089e-02
Drift      -name "BC1_DR_DI_QD_ED"  -length  4.250000e-02 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_DI_VS"     -length  1.000000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_DI_QD_ED"  -length  4.250000e-02 -e0  2.697215e-01
Quadrupole -name "BC1_QD_DI_DH"     -length  4.000000e-02 -e0  2.697215e-01 -strength -8.631089e-02
Dipole     -name "BC1_COR_DI"       -length  0.000000e+00 -e0  2.697215e-01
Bpm        -name "BC1_BPM_DI"       -length  0.000000e+00 -e0  2.697215e-01
Quadrupole -name "BC1_QD_DI_DH"     -length  4.000000e-02 -e0  2.697215e-01 -strength -8.631089e-02
Drift      -name "BC1_DR_DI_QD_ED"  -length  4.250000e-02 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_DI_VS"     -length  1.000000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_DI_QD_ED"  -length  4.250000e-02 -e0  2.697215e-01
Quadrupole -name "BC1_QD_DI_FH"     -length  4.000000e-02 -e0  2.697215e-01 -strength  8.631089e-02
Dipole     -name "BC1_COR_DI"       -length  0.000000e+00 -e0  2.697215e-01
Bpm        -name "BC1_BPM_DI"       -length  0.000000e+00 -e0  2.697215e-01
Quadrupole -name "BC1_QD_DI_FH"     -length  4.000000e-02 -e0  2.697215e-01 -strength  8.631089e-02
Drift      -name "BC1_DR_DI_QD_ED"  -length  4.250000e-02 -e0  2.697215e-01
Drift      -name "BC1_DR_DI"        -length  2.500000e-01 -e0  2.697215e-01
Sbend      -name "BC1_DP_DI"        -length  2.500000e-01 -e0  2.697215e-01 -angle  0.000000e+00 -E1  0.000000e+00 -E2  0.000000e+00 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  0.000000e+00 -six_dim 1
Drift      -name "BC1_DR_DI"        -length  2.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_DI"        -length  2.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_DI"        -length  2.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_WA_OU_DI2"    -length  0.000000e+00 -e0  2.697215e-01
Drift      -name "LN1_DR_V1"        -length  2.005300e-01 -e0  2.697215e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  2.697215e-01
Quadrupole -name "LN1_QD_V01"       -length  4.000000e-02 -e0  2.697215e-01 -strength  7.032831e-02
Dipole     -name "LN1_COR"          -length  0.000000e+00 -e0  2.697215e-01
Bpm        -name "LN1_BPM"          -length  0.000000e+00 -e0  2.697215e-01
Quadrupole -name "LN1_QD_V01"       -length  4.000000e-02 -e0  2.697215e-01 -strength  7.032831e-02
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  2.697215e-01
Drift      -name "LN1_DR_V2"        -length  2.192504e-01 -e0  2.697215e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  2.697215e-01
Quadrupole -name "LN1_QD_V02"       -length  4.000000e-02 -e0  2.697215e-01 -strength -1.256105e-01
Dipole     -name "LN1_COR"          -length  0.000000e+00 -e0  2.697215e-01
Bpm        -name "LN1_BPM"          -length  0.000000e+00 -e0  2.697215e-01
Quadrupole -name "LN1_QD_V02"       -length  4.000000e-02 -e0  2.697215e-01 -strength -1.256105e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  2.697215e-01
Drift      -name "LN1_DR_V3"        -length  1.914618e+00 -e0  2.697215e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  2.697215e-01
Quadrupole -name "LN1_QD_V03"       -length  4.000000e-02 -e0  2.697215e-01 -strength  1.347152e-02
Dipole     -name "LN1_COR"          -length  0.000000e+00 -e0  2.697215e-01
Bpm        -name "LN1_BPM"          -length  0.000000e+00 -e0  2.697215e-01
Quadrupole -name "LN1_QD_V03"       -length  4.000000e-02 -e0  2.697215e-01 -strength  1.347152e-02
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  2.697215e-01
Drift      -name "LN1_DR_V4"        -length  2.000000e-01 -e0  2.697215e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  2.697215e-01
Quadrupole -name "LN1_QD_FH"        -length  4.000000e-02 -e0  2.697215e-01 -strength  7.120648e-02
Dipole     -name "LN1_COR"          -length  0.000000e+00 -e0  2.697215e-01
Bpm        -name "LN1_BPM"          -length  0.000000e+00 -e0  2.697215e-01
Quadrupole -name "LN1_QD_FH"        -length  4.000000e-02 -e0  2.697215e-01 -strength  7.120648e-02
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  2.697215e-01
Drift      -name "LN1_DR_SV"        -length  1.000000e-01 -e0  2.697215e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  2.697215e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  2.697215e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  3.500000e+01 -frequency  1.199420e+01
SetReferenceEnergy   3.184374e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  3.184374e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  3.184374e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  3.184374e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  3.500000e+01 -frequency  1.199420e+01
SetReferenceEnergy   3.671532e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  3.671532e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  3.671532e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  3.671532e-01
Quadrupole -name "LN1_QD_DH"        -length  4.000000e-02 -e0  3.671532e-01 -strength -9.692845e-02
Dipole     -name "LN1_COR"          -length  0.000000e+00 -e0  3.671532e-01
Bpm        -name "LN1_BPM"          -length  0.000000e+00 -e0  3.671532e-01
Quadrupole -name "LN1_QD_DH"        -length  4.000000e-02 -e0  3.671532e-01 -strength -9.692845e-02
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  3.671532e-01
Drift      -name "LN1_DR_VS"        -length  1.000000e-01 -e0  3.671532e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  3.671532e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  3.671532e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  3.500000e+01 -frequency  1.199420e+01
SetReferenceEnergy   4.158691e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  4.158691e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  4.158691e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  4.158691e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  3.500000e+01 -frequency  1.199420e+01
SetReferenceEnergy   4.645849e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  4.645849e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  4.645849e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  4.645849e-01
Quadrupole -name "LN1_QD_FH"        -length  4.000000e-02 -e0  4.645849e-01 -strength  1.226504e-01
Dipole     -name "LN1_COR"          -length  0.000000e+00 -e0  4.645849e-01
Bpm        -name "LN1_BPM"          -length  0.000000e+00 -e0  4.645849e-01
Quadrupole -name "LN1_QD_FH"        -length  4.000000e-02 -e0  4.645849e-01 -strength  1.226504e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  4.645849e-01
Drift      -name "LN1_DR_SV"        -length  1.000000e-01 -e0  4.645849e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  4.645849e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  4.645849e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  3.500000e+01 -frequency  1.199420e+01
SetReferenceEnergy   5.133008e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  5.133008e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  5.133008e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  5.133008e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  3.500000e+01 -frequency  1.199420e+01
SetReferenceEnergy   5.620166e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  5.620166e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  5.620166e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  5.620166e-01
Quadrupole -name "LN1_QD_DH"        -length  4.000000e-02 -e0  5.620166e-01 -strength -1.483724e-01
Dipole     -name "LN1_COR"          -length  0.000000e+00 -e0  5.620166e-01
Bpm        -name "LN1_BPM"          -length  0.000000e+00 -e0  5.620166e-01
Quadrupole -name "LN1_QD_DH"        -length  4.000000e-02 -e0  5.620166e-01 -strength -1.483724e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  5.620166e-01
Drift      -name "LN1_DR_VS"        -length  1.000000e-01 -e0  5.620166e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  5.620166e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  5.620166e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  3.500000e+01 -frequency  1.199420e+01
SetReferenceEnergy   6.107324e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  6.107324e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  6.107324e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  6.107324e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  3.500000e+01 -frequency  1.199420e+01
SetReferenceEnergy   6.594483e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  6.594483e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  6.594483e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  6.594483e-01
Quadrupole -name "LN1_QD_FH"        -length  4.000000e-02 -e0  6.594483e-01 -strength  1.740943e-01
Dipole     -name "LN1_COR"          -length  0.000000e+00 -e0  6.594483e-01
Bpm        -name "LN1_BPM"          -length  0.000000e+00 -e0  6.594483e-01
Quadrupole -name "LN1_QD_FH"        -length  4.000000e-02 -e0  6.594483e-01 -strength  1.740943e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  6.594483e-01
Drift      -name "LN1_DR_SV"        -length  1.000000e-01 -e0  6.594483e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  6.594483e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  6.594483e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  3.500000e+01 -frequency  1.199420e+01
SetReferenceEnergy   7.081641e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  7.081641e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  7.081641e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  7.081641e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  3.500000e+01 -frequency  1.199420e+01
SetReferenceEnergy   7.568799e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  7.568799e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  7.568799e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  7.568799e-01
Quadrupole -name "LN1_QD_DH"        -length  4.000000e-02 -e0  7.568799e-01 -strength -1.998163e-01
Dipole     -name "LN1_COR"          -length  0.000000e+00 -e0  7.568799e-01
Bpm        -name "LN1_BPM"          -length  0.000000e+00 -e0  7.568799e-01
Quadrupole -name "LN1_QD_DH"        -length  4.000000e-02 -e0  7.568799e-01 -strength -1.998163e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  7.568799e-01
Drift      -name "LN1_DR_VS"        -length  1.000000e-01 -e0  7.568799e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  7.568799e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  7.568799e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  3.500000e+01 -frequency  1.199420e+01
SetReferenceEnergy   8.055958e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  8.055958e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  8.055958e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  8.055958e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  3.500000e+01 -frequency  1.199420e+01
SetReferenceEnergy   8.543116e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  8.543116e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  8.543116e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  8.543116e-01
Quadrupole -name "LN1_QD_FH"        -length  4.000000e-02 -e0  8.543116e-01 -strength  2.255383e-01
Dipole     -name "LN1_COR"          -length  0.000000e+00 -e0  8.543116e-01
Bpm        -name "LN1_BPM"          -length  0.000000e+00 -e0  8.543116e-01
Quadrupole -name "LN1_QD_FH"        -length  4.000000e-02 -e0  8.543116e-01 -strength  2.255383e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  8.543116e-01
Drift      -name "LN1_DR_SV"        -length  1.000000e-01 -e0  8.543116e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  8.543116e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  8.543116e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  3.500000e+01 -frequency  1.199420e+01
SetReferenceEnergy   9.030274e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  9.030274e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  9.030274e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  9.030274e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  3.500000e+01 -frequency  1.199420e+01
SetReferenceEnergy   9.517433e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  9.517433e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  9.517433e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  9.517433e-01
Quadrupole -name "LN1_QD_DH"        -length  4.000000e-02 -e0  9.517433e-01 -strength -2.512602e-01
Dipole     -name "LN1_COR"          -length  0.000000e+00 -e0  9.517433e-01
Bpm        -name "LN1_BPM"          -length  0.000000e+00 -e0  9.517433e-01
Quadrupole -name "LN1_QD_DH"        -length  4.000000e-02 -e0  9.517433e-01 -strength -2.512602e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  9.517433e-01
Drift      -name "LN1_DR_VS"        -length  1.000000e-01 -e0  9.517433e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  9.517433e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  9.517433e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  3.500000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.000459e+00
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  1.000459e+00
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  1.000459e+00
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  1.000459e+00
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  3.500000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.049175e+00
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  1.049175e+00
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  1.049175e+00
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  1.049175e+00
Quadrupole -name "LN1_QD_FH"        -length  4.000000e-02 -e0  1.049175e+00 -strength  2.769822e-01
Dipole     -name "LN1_COR"          -length  0.000000e+00 -e0  1.049175e+00
Bpm        -name "LN1_BPM"          -length  0.000000e+00 -e0  1.049175e+00
Quadrupole -name "LN1_QD_FH"        -length  4.000000e-02 -e0  1.049175e+00 -strength  2.769822e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  1.049175e+00
Drift      -name "LN1_DR_DR"        -length  1.000000e-01 -e0  1.049175e+00
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  1.049175e+00
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  1.049175e+00
Drift      -name "LN1_DR_XCA_"      -length  9.164755e-01 -e0  1.049175e+00
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  1.049175e+00
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  1.049175e+00
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  1.049175e+00
Drift      -name "LN1_DR_XCA_"      -length  9.164755e-01 -e0  1.049175e+00
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  1.049175e+00
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  1.049175e+00
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  1.049175e+00
Quadrupole -name "LN1_QD_DH"        -length  4.000000e-02 -e0  1.049175e+00 -strength -2.769822e-01
Dipole     -name "LN1_COR"          -length  0.000000e+00 -e0  1.049175e+00
Bpm        -name "LN1_BPM"          -length  0.000000e+00 -e0  1.049175e+00
Quadrupole -name "LN1_QD_DH"        -length  4.000000e-02 -e0  1.049175e+00 -strength -2.769822e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  1.049175e+00
Drift      -name "LN1_DR_DR"        -length  1.000000e-01 -e0  1.049175e+00
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  1.049175e+00
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  1.049175e+00
Drift      -name "LN1_DR_XCA_A"     -length  8.164755e-01 -e0  1.049175e+00
Drift      -name "LN1_DR_VS"        -length  1.000000e-01 -e0  1.049175e+00
Drift      -name "LN1_WA_OU2"       -length  0.000000e+00 -e0  1.049175e+00
Drift      -name "BC2_DR_V1"        -length  4.718667e-01 -e0  1.049175e+00
Drift      -name "BC2_DR_QD_ED"     -length  4.250000e-02 -e0  1.049175e+00
Quadrupole -name "BC2_QD_V01"       -length  4.000000e-02 -e0  1.049175e+00 -strength  1.584651e-02
Dipole     -name "BC2_COR"          -length  0.000000e+00 -e0  1.049175e+00
Bpm        -name "BC2_BPM"          -length  0.000000e+00 -e0  1.049175e+00
Quadrupole -name "BC2_QD_V01"       -length  4.000000e-02 -e0  1.049175e+00 -strength  1.584651e-02
Drift      -name "BC2_DR_QD_ED"     -length  4.250000e-02 -e0  1.049175e+00
Drift      -name "BC2_DR_V2"        -length  5.872423e+00 -e0  1.049175e+00
Drift      -name "BC2_DR_QD_ED"     -length  4.250000e-02 -e0  1.049175e+00
Quadrupole -name "BC2_QD_V02"       -length  4.000000e-02 -e0  1.049175e+00 -strength -4.874093e-01
Dipole     -name "BC2_COR"          -length  0.000000e+00 -e0  1.049175e+00
Bpm        -name "BC2_BPM"          -length  0.000000e+00 -e0  1.049175e+00
Quadrupole -name "BC2_QD_V02"       -length  4.000000e-02 -e0  1.049175e+00 -strength -4.874093e-01
Drift      -name "BC2_DR_QD_ED"     -length  4.250000e-02 -e0  1.049175e+00
Drift      -name "BC2_DR_V3"        -length  2.000000e-01 -e0  1.049175e+00
Drift      -name "BC2_DR_QD_ED"     -length  4.250000e-02 -e0  1.049175e+00
Quadrupole -name "BC2_QD_V03"       -length  4.000000e-02 -e0  1.049175e+00 -strength  4.519432e-01
Dipole     -name "BC2_COR"          -length  0.000000e+00 -e0  1.049175e+00
Bpm        -name "BC2_BPM"          -length  0.000000e+00 -e0  1.049175e+00
Quadrupole -name "BC2_QD_V03"       -length  4.000000e-02 -e0  1.049175e+00 -strength  4.519432e-01
Drift      -name "BC2_DR_QD_ED"     -length  4.250000e-02 -e0  1.049175e+00
Drift      -name "BC2_DR_V4"        -length  2.000000e-01 -e0  1.049175e+00
Drift      -name "BC2_DR_20"        -length  5.000000e-01 -e0  1.049175e+00
Sbend      -name "BC2_DP_DIP1"      -length  6.000576e-01 -e0  1.049169e+00 -angle -2.399828e-02 -E1  0.000000e+00 -E2 -2.399828e-02 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  0.000000e+00  -six_dim 1 -csr 1 
Drift      -name "BC2_DR_SIDE_C"    -length  5.000000e-01 -e0  1.049155e+00
Drift      -name "BC2_DR_SIDE"      -length  3.201066e+00 -e0  1.049155e+00
Sbend      -name "BC2_DP_DIP2"      -length  6.000576e-01 -e0  1.049137e+00 -angle  2.399828e-02 -E1  2.399828e-02 -E2  0.000000e+00 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  0.000000e+00  -six_dim 1 -csr 1 
Drift      -name "BC2_DR_CENT_C"    -length  2.500000e-01 -e0  1.049119e+00
Bpm        -name "BC2_BPM"          -length  0.000000e+00 -e0  1.049119e+00
Drift      -name "BC2_DR_CENT"      -length  2.500000e-01 -e0  1.049119e+00
Sbend      -name "BC2_DP_DIP3"      -length  6.000576e-01 -e0  1.048885e+00 -angle  2.399828e-02 -E1  0.000000e+00 -E2  2.399828e-02 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  0.000000e+00  -six_dim 1 -csr 1 
Drift      -name "BC2_DR_SIDE_C"    -length  5.000000e-01 -e0  1.048477e+00
Drift      -name "BC2_DR_SIDE"      -length  3.201066e+00 -e0  1.048477e+00
Sbend      -name "BC2_DP_DIP4"      -length  6.000576e-01 -e0  1.047822e+00 -angle -2.399828e-02 -E1 -2.399828e-02 -E2  0.000000e+00 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  0.000000e+00  -six_dim 1 -csr 1 
Drift      -name "BC2_DR_20_C"      -length  5.000000e-01 -e0  1.047497e+00
Drift      -name "BC2_WA_OU2"       -length  0.000000e+00 -e0  1.047497e+00
Drift      -name "LN2_DR_CT"        -length  1.000000e-01 -e0  1.047497e+00
Drift      -name "LN2_DR_V1"        -length  2.294830e+00 -e0  1.047497e+00
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.047497e+00
Quadrupole -name "LN2_QD_V01"       -length  4.000000e-02 -e0  1.047497e+00 -strength  5.716482e-01
Dipole     -name "LN2_COR"          -length  0.000000e+00 -e0  1.047497e+00
Bpm        -name "LN2_BPM"          -length  0.000000e+00 -e0  1.047497e+00
Quadrupole -name "LN2_QD_V01"       -length  4.000000e-02 -e0  1.047497e+00 -strength  5.716482e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.047497e+00
Drift      -name "LN2_DR_V2"        -length  5.090891e-01 -e0  1.047497e+00
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.047497e+00
Quadrupole -name "LN2_QD_V02"       -length  4.000000e-02 -e0  1.047497e+00 -strength -5.482096e-01
Dipole     -name "LN2_COR"          -length  0.000000e+00 -e0  1.047497e+00
Bpm        -name "LN2_BPM"          -length  0.000000e+00 -e0  1.047497e+00
Quadrupole -name "LN2_QD_V02"       -length  4.000000e-02 -e0  1.047497e+00 -strength -5.482096e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.047497e+00
Drift      -name "LN2_DR_V3"        -length  1.490464e+00 -e0  1.047497e+00
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.047497e+00
Quadrupole -name "LN2_QD_V03"       -length  4.000000e-02 -e0  1.047497e+00 -strength  3.762188e-01
Dipole     -name "LN2_COR"          -length  0.000000e+00 -e0  1.047497e+00
Bpm        -name "LN2_BPM"          -length  0.000000e+00 -e0  1.047497e+00
Quadrupole -name "LN2_QD_V03"       -length  4.000000e-02 -e0  1.047497e+00 -strength  3.762188e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.047497e+00
Drift      -name "LN2_DR_V4"        -length  2.999939e+00 -e0  1.047497e+00
Drift      -name "LN2_DR_VS"        -length  1.000000e-01 -e0  1.047497e+00
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.047497e+00
Quadrupole -name "LN2_QD_FH"        -length  4.000000e-02 -e0  1.047497e+00 -strength  2.807292e-01
Dipole     -name "LN2_COR"          -length  0.000000e+00 -e0  1.047497e+00
Bpm        -name "LN2_BPM"          -length  0.000000e+00 -e0  1.047497e+00
Quadrupole -name "LN2_QD_FH"        -length  4.000000e-02 -e0  1.047497e+00 -strength  2.807292e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.047497e+00
Drift      -name "LN2_DR_SV"        -length  1.000000e-01 -e0  1.047497e+00
Drift      -name "LN2_DR_BL"        -length  5.000000e-02 -e0  1.047497e+00
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  1.047497e+00
Cavity     -name "LN2_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.106066e+00
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  1.106066e+00
Drift      -name "LN2_DR_BL"        -length  5.000000e-02 -e0  1.106066e+00
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  1.106066e+00
Cavity     -name "LN2_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.164635e+00
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  1.164635e+00
Drift      -name "LN2_DR_BL"        -length  5.000000e-02 -e0  1.164635e+00
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.164635e+00
Quadrupole -name "LN2_QD_DH"        -length  4.000000e-02 -e0  1.164635e+00 -strength -3.121222e-01
Dipole     -name "LN2_COR"          -length  0.000000e+00 -e0  1.164635e+00
Bpm        -name "LN2_BPM"          -length  0.000000e+00 -e0  1.164635e+00
Quadrupole -name "LN2_QD_DH"        -length  4.000000e-02 -e0  1.164635e+00 -strength -3.121222e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.164635e+00
Drift      -name "LN2_DR_VS"        -length  1.000000e-01 -e0  1.164635e+00
Drift      -name "LN2_DR_BL"        -length  5.000000e-02 -e0  1.164635e+00
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  1.164635e+00
Cavity     -name "LN2_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.223204e+00
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  1.223204e+00
Drift      -name "LN2_DR_BL"        -length  5.000000e-02 -e0  1.223204e+00
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  1.223204e+00
Cavity     -name "LN2_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.281773e+00
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  1.281773e+00
Drift      -name "LN2_DR_BL"        -length  5.000000e-02 -e0  1.281773e+00
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.281773e+00
Quadrupole -name "LN2_QD_FH"        -length  4.000000e-02 -e0  1.281773e+00 -strength  3.435153e-01
Dipole     -name "LN2_COR"          -length  0.000000e+00 -e0  1.281773e+00
Bpm        -name "LN2_BPM"          -length  0.000000e+00 -e0  1.281773e+00
Quadrupole -name "LN2_QD_FH"        -length  4.000000e-02 -e0  1.281773e+00 -strength  3.435153e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.281773e+00
Drift      -name "LN2_DR_SV"        -length  1.000000e-01 -e0  1.281773e+00
Drift      -name "LN2_DR_BL"        -length  5.000000e-02 -e0  1.281773e+00
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  1.281773e+00
Cavity     -name "LN2_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.340342e+00
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  1.340342e+00
Drift      -name "LN2_DR_BL"        -length  5.000000e-02 -e0  1.340342e+00
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  1.340342e+00
Cavity     -name "LN2_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.398912e+00
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  1.398912e+00
Drift      -name "LN2_DR_BL"        -length  5.000000e-02 -e0  1.398912e+00
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.398912e+00
Quadrupole -name "LN2_QD_DH"        -length  4.000000e-02 -e0  1.398912e+00 -strength -3.749083e-01
Dipole     -name "LN2_COR"          -length  0.000000e+00 -e0  1.398912e+00
Bpm        -name "LN2_BPM"          -length  0.000000e+00 -e0  1.398912e+00
Quadrupole -name "LN2_QD_DH"        -length  4.000000e-02 -e0  1.398912e+00 -strength -3.749083e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.398912e+00
Drift      -name "LN2_DR_VS"        -length  1.000000e-01 -e0  1.398912e+00
Drift      -name "LN2_DR_BL"        -length  5.000000e-02 -e0  1.398912e+00
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  1.398912e+00
Cavity     -name "LN2_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.457481e+00
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  1.457481e+00
Drift      -name "LN2_DR_BL"        -length  5.000000e-02 -e0  1.457481e+00
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  1.457481e+00
Cavity     -name "LN2_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.516050e+00
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  1.516050e+00
Drift      -name "LN2_DR_BL"        -length  5.000000e-02 -e0  1.516050e+00
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.516050e+00
Quadrupole -name "LN2_QD_FH"        -length  4.000000e-02 -e0  1.516050e+00 -strength  4.063013e-01
Dipole     -name "LN2_COR"          -length  0.000000e+00 -e0  1.516050e+00
Bpm        -name "LN2_BPM"          -length  0.000000e+00 -e0  1.516050e+00
Quadrupole -name "LN2_QD_FH"        -length  4.000000e-02 -e0  1.516050e+00 -strength  4.063013e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.516050e+00
Drift      -name "LN2_WA_OU2"       -length  0.000000e+00 -e0  1.516050e+00
Drift      -name "LN2_DR_DI_V1"     -length  1.161052e+00 -e0  1.516050e+00
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.516050e+00
Quadrupole -name "LN2_QD_DI_V01"    -length  4.000000e-02 -e0  1.516050e+00 -strength -1.332768e-01
Dipole     -name "LN2_COR_DI"       -length  0.000000e+00 -e0  1.516050e+00
Bpm        -name "LN2_BPM_DI"       -length  0.000000e+00 -e0  1.516050e+00
Quadrupole -name "LN2_QD_DI_V01"    -length  4.000000e-02 -e0  1.516050e+00 -strength -1.332768e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.516050e+00
Drift      -name "LN2_DR_DI_V2"     -length  8.279980e-01 -e0  1.516050e+00
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.516050e+00
Quadrupole -name "LN2_QD_DI_V02"    -length  4.000000e-02 -e0  1.516050e+00 -strength  6.190436e-01
Dipole     -name "LN2_COR_DI"       -length  0.000000e+00 -e0  1.516050e+00
Bpm        -name "LN2_BPM_DI"       -length  0.000000e+00 -e0  1.516050e+00
Quadrupole -name "LN2_QD_DI_V02"    -length  4.000000e-02 -e0  1.516050e+00 -strength  6.190436e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.516050e+00
Drift      -name "LN2_DR_DI_V3"     -length  2.001539e-01 -e0  1.516050e+00
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.516050e+00
Quadrupole -name "LN2_QD_DI_V03"    -length  4.000000e-02 -e0  1.516050e+00 -strength -7.680084e-01
Dipole     -name "LN2_COR_DI"       -length  0.000000e+00 -e0  1.516050e+00
Bpm        -name "LN2_BPM_DI"       -length  0.000000e+00 -e0  1.516050e+00
Quadrupole -name "LN2_QD_DI_V03"    -length  4.000000e-02 -e0  1.516050e+00 -strength -7.680084e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.516050e+00
Drift      -name "LN2_DR_DI_V5"     -length  2.000000e-01 -e0  1.516050e+00
Drift      -name "LN2_DR_VS"        -length  1.000000e-01 -e0  1.516050e+00
Drift      -name "LN2_DR_DI"        -length  2.500000e-01 -e0  1.516050e+00
Drift      -name "LN2_DR_VS_DI"     -length  1.000000e-01 -e0  1.516050e+00
Drift      -name "LN2_DR_BL_DI"     -length  5.000000e-02 -e0  1.516050e+00
Drift      -name "LN2_DR_XCA_ED_DI" -length  1.001043e-01 -e0  1.516050e+00
CrabCavity -name "LN2_XCA0_DI"      -length  9.997914e-01 -voltage  0.000000e+00 -phase  9.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.516050e+00
Drift      -name "LN2_DR_XCA_ED_DI" -length  1.001043e-01 -e0  1.515969e+00
Drift      -name "LN2_DR_BL_DI"     -length  5.000000e-02 -e0  1.515969e+00
Drift      -name "LN2_DR_DI"        -length  2.500000e-01 -e0  1.515969e+00
Drift      -name "LN2_DR_DI"        -length  2.500000e-01 -e0  1.515969e+00
Drift      -name "LN2_DR_DI_QD_ED"  -length  4.250000e-02 -e0  1.515969e+00
Quadrupole -name "LN2_QD_DI_FH"     -length  4.000000e-02 -e0  1.515969e+00 -strength  4.851100e-01
Dipole     -name "LN2_COR_DI"       -length  0.000000e+00 -e0  1.515969e+00
Bpm        -name "LN2_BPM_DI"       -length  0.000000e+00 -e0  1.515969e+00
Quadrupole -name "LN2_QD_DI_FH"     -length  4.000000e-02 -e0  1.515969e+00 -strength  4.851100e-01
Drift      -name "LN2_DR_DI_QD_ED"  -length  4.250000e-02 -e0  1.515969e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.515969e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.515969e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.515969e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.515969e+00
Drift      -name "LN2_DR_DI_VS"     -length  1.000000e-01 -e0  1.515969e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.515969e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.515969e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.515969e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.515969e+00
Drift      -name "LN2_DR_DI_QD_ED"  -length  4.250000e-02 -e0  1.515969e+00
Quadrupole -name "LN2_QD_DI_DH"     -length  4.000000e-02 -e0  1.515969e+00 -strength -4.851100e-01
Dipole     -name "LN2_COR_DI"       -length  0.000000e+00 -e0  1.515969e+00
Bpm        -name "LN2_BPM_DI"       -length  0.000000e+00 -e0  1.515969e+00
Quadrupole -name "LN2_QD_DI_DH"     -length  4.000000e-02 -e0  1.515969e+00 -strength -4.851100e-01
Drift      -name "LN2_DR_DI_QD_ED"  -length  4.250000e-02 -e0  1.515969e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.515969e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.515969e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.515969e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.515969e+00
Drift      -name "LN2_DR_DI_VS"     -length  1.000000e-01 -e0  1.515969e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.515969e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.515969e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.515969e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.515969e+00
Drift      -name "LN2_DR_DI_QD_ED"  -length  4.250000e-02 -e0  1.515969e+00
Quadrupole -name "LN2_QD_DI_FH"     -length  4.000000e-02 -e0  1.515969e+00 -strength  4.851100e-01
Dipole     -name "LN2_COR_DI"       -length  0.000000e+00 -e0  1.515969e+00
Bpm        -name "LN2_BPM_DI"       -length  0.000000e+00 -e0  1.515969e+00
Quadrupole -name "LN2_QD_DI_FH"     -length  4.000000e-02 -e0  1.515969e+00 -strength  4.851100e-01
Drift      -name "LN2_DR_DI_QD_ED"  -length  4.250000e-02 -e0  1.515969e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.515969e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.515969e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.515969e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.515969e+00
Drift      -name "LN2_DR_DI_VS"     -length  1.000000e-01 -e0  1.515969e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.515969e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.515969e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.515969e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.515969e+00
Drift      -name "LN2_DR_DI_QD_ED"  -length  4.250000e-02 -e0  1.515969e+00
Quadrupole -name "LN2_QD_DI_DH"     -length  4.000000e-02 -e0  1.515969e+00 -strength -4.851100e-01
Dipole     -name "LN2_COR_DI"       -length  0.000000e+00 -e0  1.515969e+00
Bpm        -name "LN2_BPM_DI"       -length  0.000000e+00 -e0  1.515969e+00
Quadrupole -name "LN2_QD_DI_DH"     -length  4.000000e-02 -e0  1.515969e+00 -strength -4.851100e-01
Drift      -name "LN2_DR_DI_QD_ED"  -length  4.250000e-02 -e0  1.515969e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.515969e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.515969e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.515969e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.515969e+00
Drift      -name "LN2_DR_DI_VS"     -length  1.000000e-01 -e0  1.515969e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.515969e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.515969e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.515969e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.515969e+00
Drift      -name "LN2_DR_DI_QD_ED"  -length  4.250000e-02 -e0  1.515969e+00
Quadrupole -name "LN2_QD_DI_FH"     -length  4.000000e-02 -e0  1.515969e+00 -strength  4.851100e-01
Dipole     -name "LN2_COR_DI"       -length  0.000000e+00 -e0  1.515969e+00
Bpm        -name "LN2_BPM_DI"       -length  0.000000e+00 -e0  1.515969e+00
Quadrupole -name "LN2_QD_DI_FH"     -length  4.000000e-02 -e0  1.515969e+00 -strength  4.851100e-01
Drift      -name "LN2_DR_DI_QD_ED"  -length  4.250000e-02 -e0  1.515969e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.515969e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.515969e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.515969e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.515969e+00
Drift      -name "LN2_DR_DI_VS"     -length  1.000000e-01 -e0  1.515969e+00
Sbend      -name "LN2_DP_DI"        -length  1.500000e-01 -e0  1.515969e+00 -angle  0.000000e+00 -E1  0.000000e+00 -E2  0.000000e+00 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  0.000000e+00 -six_dim 1
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.515969e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.515969e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.515969e+00
Drift      -name "LN2_DR_DI_QD_ED"  -length  4.250000e-02 -e0  1.515969e+00
Quadrupole -name "LN2_QD_DI_DH"     -length  4.000000e-02 -e0  1.515969e+00 -strength -4.851100e-01
Dipole     -name "LN2_COR_DI"       -length  0.000000e+00 -e0  1.515969e+00
Bpm        -name "LN2_BPM_DI"       -length  0.000000e+00 -e0  1.515969e+00
Quadrupole -name "LN2_QD_DI_DH"     -length  4.000000e-02 -e0  1.515969e+00 -strength -4.851100e-01
Drift      -name "LN2_DR_DI_QD_ED"  -length  4.250000e-02 -e0  1.515969e+00
Drift      -name "LN2_DR_DI"        -length  2.500000e-01 -e0  1.515969e+00
Drift      -name "LN2_WA_OU_DI2"    -length  0.000000e+00 -e0  1.515969e+00
Drift      -name "LN2_DR_BP_V1"     -length  3.714219e+00 -e0  1.515969e+00
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.515969e+00
Quadrupole -name "LN2_QD_BP_V01"    -length  4.000000e-02 -e0  1.515969e+00 -strength -7.497352e-02
Dipole     -name "LN2_COR"          -length  0.000000e+00 -e0  1.515969e+00
Bpm        -name "LN2_BPM"          -length  0.000000e+00 -e0  1.515969e+00
Quadrupole -name "LN2_QD_BP_V01"    -length  4.000000e-02 -e0  1.515969e+00 -strength -7.497352e-02
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.515969e+00
Drift      -name "LN2_DR_BP_V2"     -length  3.528767e+00 -e0  1.515969e+00
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.515969e+00
Quadrupole -name "LN2_QD_BP_V02"    -length  4.000000e-02 -e0  1.515969e+00 -strength  8.390164e-01
Dipole     -name "LN2_COR"          -length  0.000000e+00 -e0  1.515969e+00
Bpm        -name "LN2_BPM"          -length  0.000000e+00 -e0  1.515969e+00
Quadrupole -name "LN2_QD_BP_V02"    -length  4.000000e-02 -e0  1.515969e+00 -strength  8.390164e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.515969e+00
Drift      -name "LN2_DR_BP_V3"     -length  2.000269e-01 -e0  1.515969e+00
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.515969e+00
Quadrupole -name "LN2_QD_BP_V03"    -length  4.000000e-02 -e0  1.515969e+00 -strength -8.184421e-01
Dipole     -name "LN2_COR"          -length  0.000000e+00 -e0  1.515969e+00
Bpm        -name "LN2_BPM"          -length  0.000000e+00 -e0  1.515969e+00
Quadrupole -name "LN2_QD_BP_V03"    -length  4.000000e-02 -e0  1.515969e+00 -strength -8.184421e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.515969e+00
Drift      -name "LN2_DR_BP_V4"     -length  2.645902e-01 -e0  1.515969e+00
Drift      -name "LN2_DR_BP_0"      -length  2.000000e-01 -e0  1.515969e+00
Drift      -name "LN2_DR_BP_BL"     -length  5.000000e-02 -e0  1.515969e+00
Drift      -name "LN2_DR_SCA_ED"    -length  8.325105e-02 -e0  1.515969e+00
CrabCavity -name "LN2_SCA0"         -length  5.334979e-01 -voltage  0.000000e+00 -phase  9.000000e+01 -frequency  2.997000e+00
SetReferenceEnergy   1.515969e+00
Drift      -name "LN2_DR_SCA_ED"    -length  8.325105e-02 -e0  1.515964e+00
Drift      -name "LN2_DR_BP_BL"     -length  5.000000e-02 -e0  1.515964e+00
Drift      -name "LN2_DR_BP_0"      -length  2.000000e-01 -e0  1.515964e+00
Drift      -name "LN2_DR_BP"        -length  4.000000e-01 -e0  1.515964e+00
Drift      -name "LN2_DR_BP"        -length  4.000000e-01 -e0  1.515964e+00
Drift      -name "LN2_DR_BP_VS"     -length  1.000000e-01 -e0  1.515964e+00
Drift      -name "LN2_DR_BP_DR"     -length  1.000000e-01 -e0  1.515964e+00
Drift      -name "LN2_DP_SEPM"      -length  2.500000e-01 -e0  1.515964e+00
Drift      -name "LN2_DR_BP"        -length  4.000000e-01 -e0  1.515964e+00
Drift      -name "LN2_DR_BP"        -length  4.000000e-01 -e0  1.515964e+00
Drift      -name "LN2_DR_BP"        -length  4.000000e-01 -e0  1.515964e+00
Drift      -name "LN2_DR_BP"        -length  4.000000e-01 -e0  1.515964e+00
Drift      -name "LN2_DR_BP"        -length  4.000000e-01 -e0  1.515964e+00
Drift      -name "LN2_DR_BP_VS"     -length  1.000000e-01 -e0  1.515964e+00
Drift      -name "LN2_DR_BP"        -length  4.000000e-01 -e0  1.515964e+00
Drift      -name "LN2_BP_WA_OU2_"   -length  0.000000e+00 -e0  1.515964e+00
Drift      -name "LN3_DR_V1"        -length  5.000000e-02 -e0  1.515964e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.515964e+00
Quadrupole -name "LN3_QD_V01"       -length  4.000000e-02 -e0  1.515964e+00 -strength  4.510225e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  1.515964e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  1.515964e+00
Quadrupole -name "LN3_QD_V01"       -length  4.000000e-02 -e0  1.515964e+00 -strength  4.510225e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.515964e+00
Drift      -name "LN3_DR_V2"        -length  5.342817e-01 -e0  1.515964e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.515964e+00
Quadrupole -name "LN3_QD_V02"       -length  4.000000e-02 -e0  1.515964e+00 -strength  5.309185e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  1.515964e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  1.515964e+00
Quadrupole -name "LN3_QD_V02"       -length  4.000000e-02 -e0  1.515964e+00 -strength  5.309185e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.515964e+00
Drift      -name "LN3_DR_V3"        -length  9.374694e-02 -e0  1.515964e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.515964e+00
Quadrupole -name "LN3_QD_V03"       -length  4.000000e-02 -e0  1.515964e+00 -strength -8.921770e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  1.515964e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  1.515964e+00
Quadrupole -name "LN3_QD_V03"       -length  4.000000e-02 -e0  1.515964e+00 -strength -8.921770e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.515964e+00
Drift      -name "LN3_DR_V4"        -length  1.580023e+00 -e0  1.515964e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.515964e+00
Quadrupole -name "LN3_QD_V04"       -length  4.000000e-02 -e0  1.515964e+00 -strength -2.014404e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  1.515964e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  1.515964e+00
Quadrupole -name "LN3_QD_V04"       -length  4.000000e-02 -e0  1.515964e+00 -strength -2.014404e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.515964e+00
Drift      -name "LN3_DR_V5"        -length  1.237385e+00 -e0  1.515964e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.515964e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  1.515964e+00 -strength  2.546819e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  1.515964e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  1.515964e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  1.515964e+00 -strength  2.546819e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.515964e+00
Drift      -name "LN3_DR_SV"        -length  1.000000e-01 -e0  1.515964e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.515964e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.515964e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.574533e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.574533e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.574533e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.574533e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.633102e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.633102e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.633102e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.633102e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.691671e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.691671e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.691671e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.691671e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.750240e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.750240e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.750240e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.750240e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  1.750240e+00 -strength -2.940404e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  1.750240e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  1.750240e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  1.750240e+00 -strength -2.940404e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.750240e+00
Drift      -name "LN3_DR_VS"        -length  1.000000e-01 -e0  1.750240e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.750240e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.750240e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.808809e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.808809e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.808809e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.808809e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.867379e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.867379e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.867379e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.867379e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.925948e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.925948e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.925948e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.925948e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.984517e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.984517e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.984517e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.984517e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  1.984517e+00 -strength  3.333988e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  1.984517e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  1.984517e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  1.984517e+00 -strength  3.333988e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.984517e+00
Drift      -name "LN3_DR_SV"        -length  1.000000e-01 -e0  1.984517e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.984517e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.984517e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.043086e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.043086e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.043086e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.043086e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.101655e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.101655e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.101655e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.101655e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.160224e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.160224e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.160224e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.160224e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.218793e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.218793e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.218793e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  2.218793e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  2.218793e+00 -strength -3.727573e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  2.218793e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  2.218793e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  2.218793e+00 -strength -3.727573e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  2.218793e+00
Drift      -name "LN3_DR_VS"        -length  1.000000e-01 -e0  2.218793e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.218793e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.218793e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.277362e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.277362e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.277362e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.277362e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.335931e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.335931e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.335931e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.335931e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.394501e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.394501e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.394501e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.394501e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.453070e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.453070e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.453070e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  2.453070e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  2.453070e+00 -strength  4.121157e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  2.453070e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  2.453070e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  2.453070e+00 -strength  4.121157e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  2.453070e+00
Drift      -name "LN3_DR_SV"        -length  1.000000e-01 -e0  2.453070e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.453070e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.453070e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.511639e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.511639e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.511639e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.511639e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.570208e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.570208e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.570208e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.570208e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.628777e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.628777e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.628777e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.628777e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.687346e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.687346e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.687346e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  2.687346e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  2.687346e+00 -strength -4.514742e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  2.687346e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  2.687346e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  2.687346e+00 -strength -4.514742e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  2.687346e+00
Drift      -name "LN3_DR_VS"        -length  1.000000e-01 -e0  2.687346e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.687346e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.687346e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.745915e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.745915e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.745915e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.745915e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.804484e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.804484e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.804484e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.804484e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.863054e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.863054e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.863054e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.863054e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.921623e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.921623e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.921623e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  2.921623e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  2.921623e+00 -strength  4.908326e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  2.921623e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  2.921623e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  2.921623e+00 -strength  4.908326e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  2.921623e+00
Drift      -name "LN3_DR_SV"        -length  1.000000e-01 -e0  2.921623e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.921623e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.921623e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.980192e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.980192e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.980192e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.980192e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   3.038761e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  3.038761e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  3.038761e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  3.038761e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   3.097330e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  3.097330e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  3.097330e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  3.097330e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   3.155899e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  3.155899e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  3.155899e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  3.155899e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  3.155899e+00 -strength -5.301911e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  3.155899e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  3.155899e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  3.155899e+00 -strength -5.301911e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  3.155899e+00
Drift      -name "LN3_DR_VS"        -length  1.000000e-01 -e0  3.155899e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  3.155899e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  3.155899e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   3.214468e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  3.214468e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  3.214468e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  3.214468e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   3.273037e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  3.273037e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  3.273037e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  3.273037e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   3.331606e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  3.331606e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  3.331606e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  3.331606e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   3.390176e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  3.390176e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  3.390176e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  3.390176e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  3.390176e+00 -strength  5.695495e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  3.390176e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  3.390176e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  3.390176e+00 -strength  5.695495e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  3.390176e+00
Drift      -name "LN3_DR_SV"        -length  1.000000e-01 -e0  3.390176e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  3.390176e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  3.390176e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   3.448745e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  3.448745e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  3.448745e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  3.448745e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   3.507314e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  3.507314e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  3.507314e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  3.507314e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   3.565883e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  3.565883e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  3.565883e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  3.565883e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   3.624452e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  3.624452e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  3.624452e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  3.624452e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  3.624452e+00 -strength -6.089079e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  3.624452e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  3.624452e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  3.624452e+00 -strength -6.089079e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  3.624452e+00
Drift      -name "LN3_DR_VS"        -length  1.000000e-01 -e0  3.624452e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  3.624452e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  3.624452e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   3.683021e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  3.683021e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  3.683021e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  3.683021e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   3.741590e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  3.741590e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  3.741590e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  3.741590e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   3.800159e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  3.800159e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  3.800159e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  3.800159e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   3.858729e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  3.858729e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  3.858729e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  3.858729e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  3.858729e+00 -strength  6.482664e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  3.858729e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  3.858729e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  3.858729e+00 -strength  6.482664e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  3.858729e+00
Drift      -name "LN3_DR_SV"        -length  1.000000e-01 -e0  3.858729e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  3.858729e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  3.858729e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   3.917298e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  3.917298e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  3.917298e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  3.917298e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   3.975867e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  3.975867e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  3.975867e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  3.975867e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   4.034436e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  4.034436e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  4.034436e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  4.034436e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   4.093005e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  4.093005e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  4.093005e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  4.093005e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  4.093005e+00 -strength -6.876248e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  4.093005e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  4.093005e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  4.093005e+00 -strength -6.876248e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  4.093005e+00
Drift      -name "LN3_DR_VS"        -length  1.000000e-01 -e0  4.093005e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  4.093005e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  4.093005e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   4.151574e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  4.151574e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  4.151574e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  4.151574e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   4.210143e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  4.210143e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  4.210143e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  4.210143e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   4.268712e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  4.268712e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  4.268712e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  4.268712e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   4.327281e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  4.327281e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  4.327281e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  4.327281e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  4.327281e+00 -strength  7.269833e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  4.327281e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  4.327281e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  4.327281e+00 -strength  7.269833e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  4.327281e+00
Drift      -name "LN3_DR_SV"        -length  1.000000e-01 -e0  4.327281e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  4.327281e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  4.327281e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   4.385851e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  4.385851e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  4.385851e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  4.385851e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   4.444420e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  4.444420e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  4.444420e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  4.444420e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   4.502989e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  4.502989e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  4.502989e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  4.502989e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   4.561558e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  4.561558e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  4.561558e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  4.561558e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  4.561558e+00 -strength -7.663417e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  4.561558e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  4.561558e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  4.561558e+00 -strength -7.663417e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  4.561558e+00
Drift      -name "LN3_DR_VS"        -length  1.000000e-01 -e0  4.561558e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  4.561558e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  4.561558e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   4.620127e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  4.620127e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  4.620127e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  4.620127e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   4.678696e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  4.678696e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  4.678696e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  4.678696e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   4.737265e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  4.737265e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  4.737265e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  4.737265e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   4.795834e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  4.795834e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  4.795834e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  4.795834e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  4.795834e+00 -strength  8.057002e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  4.795834e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  4.795834e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  4.795834e+00 -strength  8.057002e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  4.795834e+00
Drift      -name "LN3_DR_SV"        -length  1.000000e-01 -e0  4.795834e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  4.795834e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  4.795834e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   4.854403e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  4.854403e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  4.854403e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  4.854403e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   4.912973e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  4.912973e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  4.912973e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  4.912973e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   4.971542e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  4.971542e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  4.971542e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  4.971542e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   5.030111e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  5.030111e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  5.030111e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  5.030111e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  5.030111e+00 -strength -8.450586e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  5.030111e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  5.030111e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  5.030111e+00 -strength -8.450586e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  5.030111e+00
Drift      -name "LN3_DR_VS"        -length  1.000000e-01 -e0  5.030111e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  5.030111e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  5.030111e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   5.088680e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  5.088680e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  5.088680e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  5.088680e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   5.147249e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  5.147249e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  5.147249e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  5.147249e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   5.205818e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  5.205818e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  5.205818e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  5.205818e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   5.264387e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  5.264387e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  5.264387e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  5.264387e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  5.264387e+00 -strength  8.844171e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  5.264387e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  5.264387e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  5.264387e+00 -strength  8.844171e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  5.264387e+00
Drift      -name "LN3_DR_SV"        -length  1.000000e-01 -e0  5.264387e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  5.264387e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  5.264387e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   5.322956e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  5.322956e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  5.322956e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  5.322956e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   5.381526e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  5.381526e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  5.381526e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  5.381526e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   5.440095e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  5.440095e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  5.440095e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  5.440095e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   5.498664e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  5.498664e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  5.498664e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  5.498664e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  5.498664e+00 -strength -9.237755e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  5.498664e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  5.498664e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  5.498664e+00 -strength -9.237755e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  5.498664e+00
Drift      -name "LN3_DR_DR"        -length  1.000000e-01 -e0  5.498664e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  5.498664e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  5.498664e+00
Drift      -name "LN3_DR_XCA_"      -length  9.164755e-01 -e0  5.498664e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  5.498664e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  5.498664e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  5.498664e+00
Drift      -name "LN3_DR_XCA_"      -length  9.164755e-01 -e0  5.498664e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  5.498664e+00
Drift      -name "LN3_DR_CT"        -length  1.000000e-01 -e0  5.498664e+00
Drift      -name "LN3_WA_OU2"       -length  0.000000e+00 -e0  5.498664e+00
Drift      -name "LN3_DR_BP_V1"     -length  1.926276e+00 -e0  5.498664e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  5.498664e+00
Quadrupole -name "LN3_QD_BP_V01"    -length  4.000000e-02 -e0  5.498664e+00 -strength -1.726063e+00
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  5.498664e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  5.498664e+00
Quadrupole -name "LN3_QD_BP_V01"    -length  4.000000e-02 -e0  5.498664e+00 -strength -1.726063e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  5.498664e+00
Drift      -name "LN3_DR_BP_V2"     -length  2.421223e+00 -e0  5.498664e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  5.498664e+00
Quadrupole -name "LN3_QD_BP_V02"    -length  4.000000e-02 -e0  5.498664e+00 -strength  1.890359e+00
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  5.498664e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  5.498664e+00
Quadrupole -name "LN3_QD_BP_V02"    -length  4.000000e-02 -e0  5.498664e+00 -strength  1.890359e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  5.498664e+00
Drift      -name "LN3_DR_BP_V3"     -length  1.222036e+00 -e0  5.498664e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  5.498664e+00
Quadrupole -name "LN3_QD_BP_V03"    -length  4.000000e-02 -e0  5.498664e+00 -strength -1.801987e+00
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  5.498664e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  5.498664e+00
Quadrupole -name "LN3_QD_BP_V03"    -length  4.000000e-02 -e0  5.498664e+00 -strength -1.801987e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  5.498664e+00
Drift      -name "LN3_DR_BP_V4"     -length  2.000000e-01 -e0  5.498664e+00
Drift      -name "LN3_DR_BP_0"      -length  2.000000e-01 -e0  5.498664e+00
Drift      -name "LN3_DR_BP_BL"     -length  5.000000e-02 -e0  5.498664e+00
Drift      -name "LN3_DR_SCA_ED"    -length  8.325105e-02 -e0  5.498664e+00
CrabCavity -name "LN3_SCA0"         -length  5.334979e-01 -voltage  0.000000e+00 -phase  9.000000e+01 -frequency  2.997000e+00
SetReferenceEnergy   5.498664e+00
Drift      -name "LN3_DR_SCA_ED"    -length  8.325105e-02 -e0  5.498659e+00
Drift      -name "LN3_DR_BP_BL"     -length  5.000000e-02 -e0  5.498659e+00
Drift      -name "LN3_DR_BP_0"      -length  2.000000e-01 -e0  5.498659e+00
Drift      -name "LN3_DR_BP"        -length  4.000000e-01 -e0  5.498659e+00
Drift      -name "LN3_DR_BP_VS"     -length  1.000000e-01 -e0  5.498659e+00
Drift      -name "LN3_DR_BP"        -length  4.000000e-01 -e0  5.498659e+00
Drift      -name "LN3_BP_WA_OU2"    -length  0.000000e+00 -e0  5.498659e+00
Drift      -name "HBP_CNT0"         -length  0.000000e+00 -e0  5.498659e+00
Sbend      -name "HBP_DP_SEPM1"     -length  3.202601e-01 -e0  5.498020e+00 -angle  3.490658e-02 -E1  0.000000e+00 -E2  0.000000e+00 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  3.141593e+00  -six_dim 1 -csr 1 
Sbend      -name "HBP_DP_SEPM1"     -length  3.202601e-01 -e0  5.497171e+00 -angle  3.490658e-02 -E1  0.000000e+00 -E2  0.000000e+00 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  3.141593e+00  -six_dim 1 -csr 1 
Drift      -name "HBP_DR_BP_C"      -length  3.000000e-01 -e0  5.496834e+00
Drift      -name "HBP_DR_BP"        -length  3.000000e-01 -e0  5.496834e+00
Drift      -name "HBP_DR_BP"        -length  3.000000e-01 -e0  5.496834e+00
Drift      -name "HBP_DR_BP"        -length  3.000000e-01 -e0  5.496834e+00
Drift      -name "HBP_DR_V1"        -length  2.629146e-01 -e0  5.496834e+00
Drift      -name "HBP_CNT0"         -length  0.000000e+00 -e0  5.496834e+00
Drift      -name "HBP_DR_QD_ED"     -length  4.250000e-02 -e0  5.496834e+00
Quadrupole -name "HBP_QD_V01"       -length  4.000000e-02 -e0  5.496834e+00 -strength  1.570006e+00
Dipole     -name "HBP_COR"          -length  0.000000e+00 -e0  5.496834e+00
Bpm        -name "HBP_BPM"          -length  0.000000e+00 -e0  5.496834e+00
Quadrupole -name "HBP_QD_V01"       -length  4.000000e-02 -e0  5.496834e+00 -strength  1.570006e+00
Drift      -name "HBP_DR_QD_ED"     -length  4.250000e-02 -e0  5.496834e+00
Drift      -name "HBP_DR_V2"        -length  2.075000e-01 -e0  5.496834e+00
Drift      -name "HBP_DR_V2"        -length  2.075000e-01 -e0  5.496834e+00
Drift      -name "HBP_CNT0"         -length  0.000000e+00 -e0  5.496834e+00
Drift      -name "HBP_DR_QD_ED"     -length  4.250000e-02 -e0  5.496834e+00
Quadrupole -name "HBP_QD_V01"       -length  4.000000e-02 -e0  5.496834e+00 -strength  1.570006e+00
Dipole     -name "HBP_COR"          -length  0.000000e+00 -e0  5.496834e+00
Bpm        -name "HBP_BPM"          -length  0.000000e+00 -e0  5.496834e+00
Quadrupole -name "HBP_QD_V01"       -length  4.000000e-02 -e0  5.496834e+00 -strength  1.570006e+00
Drift      -name "HBP_DR_QD_ED"     -length  4.250000e-02 -e0  5.496834e+00
Drift      -name "HBP_DR_V1"        -length  2.629146e-01 -e0  5.496834e+00
Drift      -name "HBP_DR_BP"        -length  3.000000e-01 -e0  5.496834e+00
Drift      -name "HBP_DR_BP"        -length  3.000000e-01 -e0  5.496834e+00
Drift      -name "HBP_DR_BP"        -length  3.000000e-01 -e0  5.496834e+00
Drift      -name "HBP_DR_BP"        -length  3.000000e-01 -e0  5.496834e+00
Drift      -name "HBP_CNT0"         -length  0.000000e+00 -e0  5.496834e+00
Sbend      -name "HBP_DP_SEPM1"     -length  3.202601e-01 -e0  5.496203e+00 -angle  3.490658e-02 -E1  0.000000e+00 -E2  0.000000e+00 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  3.141593e+00  -six_dim 1 -csr 1 
Sbend      -name "HBP_DP_SEPM1"     -length  3.202601e-01 -e0  5.495375e+00 -angle  3.490658e-02 -E1  0.000000e+00 -E2  0.000000e+00 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  3.141593e+00  -six_dim 1 -csr 1 
Drift      -name "HBP_WA_OU1A"      -length  0.000000e+00 -e0  5.495375e+00
Drift      -name "HBP_DR_BP_C"      -length  3.000000e-01 -e0  5.495046e+00
Drift      -name "HBP_DR_V3"        -length  1.194347e-02 -e0  5.495046e+00
Drift      -name "HBP_DR_QD_ED"     -length  4.250000e-02 -e0  5.495046e+00
Quadrupole -name "HBP_QD_V02"       -length  4.000000e-02 -e0  5.495046e+00 -strength -1.361955e+00
Dipole     -name "HBP_COR"          -length  0.000000e+00 -e0  5.495046e+00
Bpm        -name "HBP_BPM"          -length  0.000000e+00 -e0  5.495046e+00
Quadrupole -name "HBP_QD_V02"       -length  4.000000e-02 -e0  5.495046e+00 -strength -1.361955e+00
Drift      -name "HBP_DR_QD_ED"     -length  4.250000e-02 -e0  5.495046e+00
Drift      -name "HBP_DR_V4"        -length  1.379807e+00 -e0  5.495046e+00
Drift      -name "HBP_DR_QD_ED"     -length  4.250000e-02 -e0  5.495046e+00
Quadrupole -name "HBP_QD_V03"       -length  4.000000e-02 -e0  5.495046e+00 -strength  1.527320e+00
Dipole     -name "HBP_COR"          -length  0.000000e+00 -e0  5.495046e+00
Bpm        -name "HBP_BPM"          -length  0.000000e+00 -e0  5.495046e+00
Quadrupole -name "HBP_QD_V03"       -length  4.000000e-02 -e0  5.495046e+00 -strength  1.527320e+00
Drift      -name "HBP_DR_QD_ED"     -length  4.250000e-02 -e0  5.495046e+00
Drift      -name "HBP_DR_V5"        -length  4.422050e+00 -e0  5.495046e+00
Drift      -name "HBP_DR_QD_ED"     -length  4.250000e-02 -e0  5.495046e+00
Quadrupole -name "HBP_QD_V04"       -length  4.000000e-02 -e0  5.495046e+00 -strength -7.255047e-01
Dipole     -name "HBP_COR"          -length  0.000000e+00 -e0  5.495046e+00
Bpm        -name "HBP_BPM"          -length  0.000000e+00 -e0  5.495046e+00
Quadrupole -name "HBP_QD_V04"       -length  4.000000e-02 -e0  5.495046e+00 -strength -7.255047e-01
Drift      -name "HBP_DR_QD_ED"     -length  4.250000e-02 -e0  5.495046e+00
Drift      -name "HBP_DR_V5"        -length  4.422050e+00 -e0  5.495046e+00
Drift      -name "HBP_DR_QD_ED"     -length  4.250000e-02 -e0  5.495046e+00
Quadrupole -name "HBP_QD_V03"       -length  4.000000e-02 -e0  5.495046e+00 -strength  1.527320e+00
Dipole     -name "HBP_COR"          -length  0.000000e+00 -e0  5.495046e+00
Bpm        -name "HBP_BPM"          -length  0.000000e+00 -e0  5.495046e+00
Quadrupole -name "HBP_QD_V03"       -length  4.000000e-02 -e0  5.495046e+00 -strength  1.527320e+00
Drift      -name "HBP_DR_QD_ED"     -length  4.250000e-02 -e0  5.495046e+00
Drift      -name "HBP_DR_V4"        -length  1.379807e+00 -e0  5.495046e+00
Drift      -name "HBP_DR_QD_ED"     -length  4.250000e-02 -e0  5.495046e+00
Quadrupole -name "HBP_QD_V02"       -length  4.000000e-02 -e0  5.495046e+00 -strength -1.361955e+00
Dipole     -name "HBP_COR"          -length  0.000000e+00 -e0  5.495046e+00
Bpm        -name "HBP_BPM"          -length  0.000000e+00 -e0  5.495046e+00
Quadrupole -name "HBP_QD_V02"       -length  4.000000e-02 -e0  5.495046e+00 -strength -1.361955e+00
Drift      -name "HBP_DR_QD_ED"     -length  4.250000e-02 -e0  5.495046e+00
Drift      -name "HBP_DR_V3"        -length  1.194347e-02 -e0  5.495046e+00
Drift      -name "HBP_DR_BP"        -length  3.000000e-01 -e0  5.495046e+00
Drift      -name "HBP_WA_OU2A"      -length  0.000000e+00 -e0  5.495046e+00
Sbend      -name "HBP_DP_SEPM2"     -length  3.202601e-01 -e0  5.494273e+00 -angle  3.490658e-02 -E1  0.000000e+00 -E2  0.000000e+00 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  0.000000e+00  -six_dim 1 -csr 1 
Sbend      -name "HBP_DP_SEPM2"     -length  3.202601e-01 -e0  5.493535e+00 -angle  3.490658e-02 -E1  0.000000e+00 -E2  0.000000e+00 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  0.000000e+00  -six_dim 1 -csr 1 
Drift      -name "HBP_DR_BP_C"      -length  3.000000e-01 -e0  5.493231e+00
Drift      -name "HBP_DR_BP"        -length  3.000000e-01 -e0  5.493231e+00
Drift      -name "HBP_DR_BP"        -length  3.000000e-01 -e0  5.493231e+00
Drift      -name "HBP_DR_BP"        -length  3.000000e-01 -e0  5.493231e+00
Drift      -name "HBP_DR_V1"        -length  2.629146e-01 -e0  5.493231e+00
Drift      -name "HBP_CNT0"         -length  0.000000e+00 -e0  5.493231e+00
Drift      -name "HBP_DR_QD_ED"     -length  4.250000e-02 -e0  5.493231e+00
Quadrupole -name "HBP_QD_V01"       -length  4.000000e-02 -e0  5.493231e+00 -strength  1.568977e+00
Dipole     -name "HBP_COR"          -length  0.000000e+00 -e0  5.493231e+00
Bpm        -name "HBP_BPM"          -length  0.000000e+00 -e0  5.493231e+00
Quadrupole -name "HBP_QD_V01"       -length  4.000000e-02 -e0  5.493231e+00 -strength  1.568977e+00
Drift      -name "HBP_DR_QD_ED"     -length  4.250000e-02 -e0  5.493231e+00
Drift      -name "HBP_DR_V2"        -length  2.075000e-01 -e0  5.493231e+00
Drift      -name "HBP_DR_V2"        -length  2.075000e-01 -e0  5.493231e+00
Drift      -name "HBP_CNT0"         -length  0.000000e+00 -e0  5.493231e+00
Drift      -name "HBP_DR_QD_ED"     -length  4.250000e-02 -e0  5.493231e+00
Quadrupole -name "HBP_QD_V01"       -length  4.000000e-02 -e0  5.493231e+00 -strength  1.568977e+00
Dipole     -name "HBP_COR"          -length  0.000000e+00 -e0  5.493231e+00
Bpm        -name "HBP_BPM"          -length  0.000000e+00 -e0  5.493231e+00
Quadrupole -name "HBP_QD_V01"       -length  4.000000e-02 -e0  5.493231e+00 -strength  1.568977e+00
Drift      -name "HBP_DR_QD_ED"     -length  4.250000e-02 -e0  5.493231e+00
Drift      -name "HBP_DR_V1"        -length  2.629146e-01 -e0  5.493231e+00
Drift      -name "HBP_DR_BP"        -length  3.000000e-01 -e0  5.493231e+00
Drift      -name "HBP_DR_BP"        -length  3.000000e-01 -e0  5.493231e+00
Drift      -name "HBP_DR_BP"        -length  3.000000e-01 -e0  5.493231e+00
Drift      -name "HBP_DR_BP"        -length  3.000000e-01 -e0  5.493231e+00
Sbend      -name "HBP_DP_SEPM2"     -length  3.202601e-01 -e0  5.492433e+00 -angle  3.490658e-02 -E1  0.000000e+00 -E2  0.000000e+00 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  0.000000e+00  -six_dim 1 -csr 1 
Sbend      -name "HBP_DP_SEPM2"     -length  3.202601e-01 -e0  5.491623e+00 -angle  3.490658e-02 -E1  0.000000e+00 -E2  0.000000e+00 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  0.000000e+00  -six_dim 1 -csr 1 
Drift      -name "HBP_WA_OU3A"      -length  0.000000e+00 -e0  5.491623e+00
Drift      -name "HXR_DR_V1"        -length  1.000000e-01 -e0  5.491623e+00
Drift      -name "HXR_DR_QD_ED"     -length  5.200000e-02 -e0  5.491623e+00
Quadrupole -name "HXR_QD_V01"       -length  1.300000e-02 -e0  5.491623e+00 -strength -2.031103e+00
Dipole     -name "HXR_COR"          -length  0.000000e+00 -e0  5.491623e+00
Bpm        -name "HXR_BPM"          -length  0.000000e+00 -e0  5.491623e+00
Quadrupole -name "HXR_QD_V01"       -length  1.300000e-02 -e0  5.491623e+00 -strength -2.031103e+00
Drift      -name "HXR_DR_QD_ED"     -length  5.200000e-02 -e0  5.491623e+00
Drift      -name "HXR_DR_V2"        -length  1.488602e+00 -e0  5.491623e+00
Drift      -name "HXR_DR_QD_ED"     -length  5.200000e-02 -e0  5.491623e+00
Quadrupole -name "HXR_QD_V02"       -length  1.300000e-02 -e0  5.491623e+00 -strength  1.829163e+00
Dipole     -name "HXR_COR"          -length  0.000000e+00 -e0  5.491623e+00
Bpm        -name "HXR_BPM"          -length  0.000000e+00 -e0  5.491623e+00
Quadrupole -name "HXR_QD_V02"       -length  1.300000e-02 -e0  5.491623e+00 -strength  1.829163e+00
Drift      -name "HXR_DR_QD_ED"     -length  5.200000e-02 -e0  5.491623e+00
Drift      -name "HXR_DR_V3"        -length  2.972012e+00 -e0  5.491623e+00
Drift      -name "HXR_DR_QD_ED"     -length  5.200000e-02 -e0  5.491623e+00
Quadrupole -name "HXR_QD_V03"       -length  1.300000e-02 -e0  5.491623e+00 -strength -1.536891e+00
Dipole     -name "HXR_COR"          -length  0.000000e+00 -e0  5.491623e+00
Bpm        -name "HXR_BPM"          -length  0.000000e+00 -e0  5.491623e+00
Quadrupole -name "HXR_QD_V03"       -length  1.300000e-02 -e0  5.491623e+00 -strength -1.536891e+00
Drift      -name "HXR_DR_QD_ED"     -length  5.200000e-02 -e0  5.491623e+00
Drift      -name "HXR_DR_V4"        -length  1.133579e+00 -e0  5.491623e+00
Drift      -name "HXR_DR_CT"        -length  1.000000e-01 -e0  5.491623e+00
Drift      -name "HXR_WA_M_OU2"     -length  0.000000e+00 -e0  5.491623e+00
Drift      -name "HXR_DR_QD_ED"     -length  5.200000e-02 -e0  5.491623e+00
Quadrupole -name "HXR_QD_FH"        -length  1.300000e-02 -e0  5.491623e+00 -strength  1.070866e+00
Dipole     -name "HXR_COR"          -length  0.000000e+00 -e0  5.491623e+00
Bpm        -name "HXR_BPM"          -length  0.000000e+00 -e0  5.491623e+00
Quadrupole -name "HXR_QD_FH"        -length  1.300000e-02 -e0  5.491623e+00 -strength  1.070866e+00
Drift      -name "HXR_DR_QD_ED"     -length  5.200000e-02 -e0  5.491623e+00
Drift      -name "HXR_DR_WIG_ED"    -length  1.950000e-01 -e0  5.491623e+00
Drift      -name "HXR_WIG_0"        -length  1.768000e+00 -e0  5.491623e+00
Drift      -name "HXR_DR_WIG_ED"    -length  1.950000e-01 -e0  5.491623e+00
Drift      -name "HXR_DR_QD_ED"     -length  5.200000e-02 -e0  5.491623e+00
Quadrupole -name "HXR_QD_DH"        -length  1.300000e-02 -e0  5.491623e+00 -strength -1.070866e+00
Dipole     -name "HXR_COR"          -length  0.000000e+00 -e0  5.491623e+00
Bpm        -name "HXR_BPM"          -length  0.000000e+00 -e0  5.491623e+00
Quadrupole -name "HXR_QD_DH"        -length  1.300000e-02 -e0  5.491623e+00 -strength -1.070866e+00
Drift      -name "HXR_DR_QD_ED"     -length  5.200000e-02 -e0  5.491623e+00
Drift      -name "HXR_DR_WIG_ED"    -length  1.950000e-01 -e0  5.491623e+00
Drift      -name "HXR_WIG_0"        -length  1.768000e+00 -e0  5.491623e+00
Drift      -name "HXR_DR_WIG_ED"    -length  1.950000e-01 -e0  5.491623e+00
Drift      -name "HXR_DR_QD_ED"     -length  5.200000e-02 -e0  5.491623e+00
Quadrupole -name "HXR_QD_FH"        -length  1.300000e-02 -e0  5.491623e+00 -strength  1.070866e+00
Drift      -name "HXR_WA_OU2"       -length  0.000000e+00 -e0  5.491623e+00
