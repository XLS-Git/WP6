set sigma 100.0
set bpmres 5.0
set dphase1 10
set dphase2 10
set dcharge 0.9
set beta0 1
set beta1 1
set beta2 1
set wgt1 -1
set wgt2 -1
set nbins 1
set noverlap 0.0
set nm 100
set machine all




set beamlinename [exec   basename [pwd] ]

set outfolder bba_out 

if {[file exist $outfolder]} {
	exec rm -rf $outfolder
	exec mkdir -p $outfolder
} else {
	exec mkdir -p $outfolder
}

cd   $outfolder

source ../../common_files/load_scripts.tcl

set script_dir "../files"


set beam0_indist [lindex [file split [glob ../files/*out.dat]] end end]
set latfile [lindex [file split [glob ../files/*lattice.tcl]] end end]
puts $beam0_indist
puts $latfile



set charge [expr 75e-12/1.6e-19]

array set BeamDefine  [bunch_parameters $script_dir/$beam0_indist]
array set BeamDefine "name beam0 filename $beam0_indist n_slice 201 charge $charge"
set BeamDefine(n_macro) [expr int($BeamDefine(npart)/$BeamDefine(n_slice))]
lassign "$BeamDefine(beta_x) $BeamDefine(beta_y) $BeamDefine(alpha_x) $BeamDefine(alpha_y)" btx bty alx aly


set e0 $BeamDefine(energy)

puts [array get BeamDefine]

set b0_in beam0.in
set b0_ou beam0.ou



# BeamlineNew
Girder
source $script_dir/$latfile

BeamlineSet -name $beamlinename

make_particle_beam_read BeamDefine $script_dir/$BeamDefine(filename)

## BEAM1 for WFS
array set BeamDefine1  [array get BeamDefine]
array set BeamDefine1 "name beam1 filename $beam0_indist charge [expr $charge*$dcharge]"

set e0 $BeamDefine1(energy)
puts [array get BeamDefine1]

make_particle_beam_read BeamDefine1 $script_dir/$BeamDefine1(filename)

Octave {
    global beaminxdx = 1;
    global Beams = {};
}

proc octave_save {} {
    Octave {
      Beams{beaminxdx++} = placet_get_beam();
    }
} 


Octave {
# apply wakes for all structures
    ALLCAVITs = placet_get_number_list("$beamlinename", "cavity");
    placet_element_set_attribute("$beamlinename", ALLCAVITs, "six_dim", true);
    
    LN0XDCs = placet_get_name_number_list("$beamlinename", "LN0_XCA0_DI");
    placet_element_set_attribute("$beamlinename", LN0XDCs, "short_range_wake",  "Xdband_SR_W");
    placet_element_set_attribute("$beamlinename", LN0XDCs, "lambda", $xdband_str(lambda));
    
    LN0CCs = placet_get_name_number_list("$beamlinename", "LN0_CCA0");
    placet_element_set_attribute("$beamlinename", LN0CCs, "short_range_wake",  "Cband_SR_W");
    placet_element_set_attribute("$beamlinename", LN0CCs, "lambda", $cband_str(lambda));
     
    LN0KCs = placet_get_name_number_list("$beamlinename", "LN0_KCA0");
    placet_element_set_attribute("$beamlinename", LN0KCs, "short_range_wake",  "Kband_SR_W");
    placet_element_set_attribute("$beamlinename", LN0KCs, "lambda", $kband_str(lambda));
   
    BC1XCs = placet_get_name_number_list("$beamlinename", "BC1_XCA0");
    placet_element_set_attribute("$beamlinename", BC1XCs, "short_range_wake",  "Xdband_SR_W");
    placet_element_set_attribute("$beamlinename", BC1XCs, "lambda", $xdband_str(lambda));
    
    LN1XCs = placet_get_name_number_list("$beamlinename", "LN1_XCA0");
    placet_element_set_attribute("$beamlinename", LN1XCs, "short_range_wake",  "Xband_SR_W");
    placet_element_set_attribute("$beamlinename", LN1XCs, "lambda", $xband_str(lambda));
    
    LN2XCs = placet_get_name_number_list("$beamlinename", "LN2_XCA0");
    placet_element_set_attribute("$beamlinename", LN2XCs, "short_range_wake",  "Xband_SR_W");
    placet_element_set_attribute("$beamlinename", LN2XCs, "lambda", $xband_str(lambda));
    
    LN2XDCs = placet_get_name_number_list("$beamlinename", "LN2_XCA0_DI");
    placet_element_set_attribute("$beamlinename", LN2XDCs, "short_range_wake",  "Xdband_SR_W");
    placet_element_set_attribute("$beamlinename", LN2XDCs, "lambda", $xdband_str(lambda));    
    
    LN2SCs = placet_get_name_number_list("$beamlinename", "LN2_SCA0");
    placet_element_set_attribute("$beamlinename", LN2SCs, "short_range_wake",  "Sdband_SR_W");
    placet_element_set_attribute("$beamlinename", LN2SCs, "lambda", $sdband_str(lambda));
    
    LN3XCs = placet_get_name_number_list("$beamlinename", "LN3_XCA0*");
    placet_element_set_attribute("$beamlinename", LN3XCs, "short_range_wake",  "Xband_SR_W");
    placet_element_set_attribute("$beamlinename", LN3XCs, "lambda", $xband_str(lambda));
    
    LN3SCs = placet_get_name_number_list("$beamlinename", "LN3_SCA0");
    placet_element_set_attribute("$beamlinename", LN3SCs, "short_range_wake",  "Sdband_SR_W");
    placet_element_set_attribute("$beamlinename", LN3SCs, "lambda", $sdband_str(lambda));
 
    
#  6d tracking in bunch compression
   SIs = placet_get_number_list("$beamlinename", "sbend");
   placet_element_set_attribute("$beamlinename", SIs, "six_dim", true);
   
   DRs = placet_get_number_list("$beamlinename", "drift");
   placet_element_set_attribute("$beamlinename", DRs, "six_dim", true);
    
   QDs = placet_get_number_list("$beamlinename", "quadrupole");
   placet_element_set_attribute("$beamlinename", QDs, "six_dim", true);

#  CSR in bending magnets
   placet_element_set_attribute("$beamlinename", SIs, "csr", true);
   placet_element_set_attribute("$beamlinename", SIs, "csr_charge", 75e-12);
   placet_element_set_attribute("$beamlinename", SIs, "csr_nbins", 50);
   placet_element_set_attribute("$beamlinename", SIs, "csr_nsectors", 10);
   placet_element_set_attribute("$beamlinename", SIs, "csr_filterorder", 1);
   placet_element_set_attribute("$beamlinename", SIs, "csr_nhalffilter", 2);
}


Octave {
    %%  twiss function along beamline
    [s, beta_x, beta_y, alpha_x, alpha_y, mu_x, mu_y] = placet_evolve_beta_function("$beamlinename", $btx, $alx, $bty, $aly);
    beta_arr= [s  beta_x  beta_y  alpha_x alpha_y mu_x mu_y];
    save -text -ascii $beamlinename.twi beta_arr;
    
}

TestNoCorrection -beam $BeamDefine(name) -survey None -emitt_file $beamlinename.emt


Octave {
    function reduce_energy()
      ACI1 = placet_get_name_number_list("$beamlinename", "LN0_CCA0");
      ACI2 = placet_get_name_number_list("$beamlinename", "LN1_XCA0");
      placet_element_vary_attribute("$beamlinename", ACI1, "phase", +$dphase1);
      placet_element_vary_attribute("$beamlinename", ACI2, "phase", +$dphase2);
    end

    function reset_energy()
      ACI1 = placet_get_name_number_list("$beamlinename", "LN0_CCA0");
      ACI2 = placet_get_name_number_list("$beamlinename", "LN1_XCA0");
      placet_element_vary_attribute("$beamlinename", ACI1, "phase", -$dphase1);
      placet_element_vary_attribute("$beamlinename", ACI2, "phase", -$dphase2);
    end
}


Octave {
    if exist('${script_dir}/R_${beamlinename}_p_${dphase1}_${dphase2}_c_${dcharge}.dat', 'file')
        load '${script_dir}/R_${beamlinename}_p_${dphase1}_${dphase2}_c_${dcharge}.dat';
    else
        disp("iterat")
        Response.Cx = placet_get_number_list("$beamlinename", "dipole");
        Response.Cy = placet_get_number_list("$beamlinename", "dipole");
        Response.Bpms = placet_get_number_list("$beamlinename", "bpm");

        # picks all correctors preceeding the last bpm, and all bpms following the first corrector
        Response.Cx = Response.Cx(Response.Cx < Response.Bpms(end));
        Response.Cy = Response.Cy(Response.Cy < Response.Bpms(end));
        Response.Bpms = Response.Bpms(Response.Bpms > max(Response.Cx(1), Response.Cy(1)));

        # gets the energy at each corrector
        Response.Ex = placet_element_get_attribute("$beamlinename", Response.Cx, "e0");
        Response.Ey = placet_element_get_attribute("$beamlinename", Response.Cy, "e0");
        
        # compute response matrices
        placet_test_no_correction("$beamlinename", "beam0", "Zero");
        Response.B0 = placet_get_bpm_readings("$beamlinename", Response.Bpms);
        Response.R0x = placet_get_response_matrix_attribute ("$beamlinename", "beam0", Response.Bpms, "x", Response.Cx, "strength_x", "Zero");
        Response.R0y = placet_get_response_matrix_attribute ("$beamlinename", "beam0", Response.Bpms, "y", Response.Cy, "strength_y", "Zero");
        reduce_energy();
        placet_test_no_correction("$beamlinename", "beam0", "Zero");
        Response.B1 = placet_get_bpm_readings("$beamlinename", Response.Bpms) - Response.B0;
        Response.R1x = placet_get_response_matrix_attribute ("$beamlinename", "beam0", Response.Bpms, "x", Response.Cx, "strength_x", "Zero") - Response.R0x;
        Response.R1y = placet_get_response_matrix_attribute ("$beamlinename", "beam0", Response.Bpms, "y", Response.Cy, "strength_y", "Zero") - Response.R0y;
        reset_energy();
        placet_test_no_correction("$beamlinename", "beam1", "Zero");
        Response.B2 = placet_get_bpm_readings("$beamlinename", Response.Bpms) - Response.B0;
        Response.R2x = placet_get_response_matrix_attribute ("$beamlinename", "beam1", Response.Bpms, "x", Response.Cx, "strength_x", "Zero") - Response.R0x;
        Response.R2y = placet_get_response_matrix_attribute ("$beamlinename", "beam1", Response.Bpms, "y", Response.Cy, "strength_y", "Zero") - Response.R0y;
        save -text '${script_dir}/R_${beamlinename}_p_${dphase1}_${dphase2}_c_${dcharge}.dat' Response;
    end
}

proc my_survey {} {
    global beamlinename machine sigma bpmres
    
    Octave {
        randn("seed", $machine * 1e4);
        BI = placet_get_number_list("$beamlinename", "bpm");
        CI = placet_get_number_list("$beamlinename", "cavity");
        DI = placet_get_number_list("$beamlinename", "dipole");
        QI = placet_get_number_list("$beamlinename", "quadrupole");
        QI1 = QI(1:2:end)(:);
        QI2 = QI(2:2:end)(:);
        QI1l = placet_element_get_attribute("$beamlinename", QI1, "length")(:);
        QI1x = randn(size(QI1)) * $sigma;
        QI1y = randn(size(QI1)) * $sigma;
        QI1xp = randn(size(QI1)) * $sigma;
        QI1yp = randn(size(QI1)) * $sigma;
        QI1roll = randn(size(QI1)) * 200;
        QI2x = QI1x .+ QI1xp .* QI1l;
        QI2y = QI1y .+ QI1yp .* QI1l;
        placet_element_set_attribute("$beamlinename", QI1, "x", QI1x);
        placet_element_set_attribute("$beamlinename", QI2, "x", QI2x);
        placet_element_set_attribute("$beamlinename", QI1, "y", QI1y);
        placet_element_set_attribute("$beamlinename", QI2, "y", QI2y);
        placet_element_set_attribute("$beamlinename", QI1, "xp", QI1xp);
        placet_element_set_attribute("$beamlinename", QI2, "xp", QI1xp);
        placet_element_set_attribute("$beamlinename", QI1, "yp", QI1yp);
        placet_element_set_attribute("$beamlinename", QI2, "yp", QI1yp);
        placet_element_set_attribute("$beamlinename", QI1, "roll", QI1roll);
        placet_element_set_attribute("$beamlinename", QI2, "roll", QI1roll);
        placet_element_set_attribute("$beamlinename", DI, "strength_x", 0.0);
        placet_element_set_attribute("$beamlinename", DI, "strength_y", 0.0);
        placet_element_set_attribute("$beamlinename", DI, "xp", randn(size(DI)) * $sigma);
        placet_element_set_attribute("$beamlinename", DI, "yp", randn(size(DI)) * $sigma);
        placet_element_set_attribute("$beamlinename", DI, "roll", randn(size(DI)) * 200);
        placet_element_set_attribute("$beamlinename", BI, "resolution", $bpmres);
        placet_element_set_attribute("$beamlinename", BI, "x", randn(size(BI)) * $sigma);
        placet_element_set_attribute("$beamlinename", BI, "y", randn(size(BI)) * $sigma);
        placet_element_set_attribute("$beamlinename", BI, "xp", randn(size(BI)) * $sigma);
        placet_element_set_attribute("$beamlinename", BI, "yp", randn(size(BI)) * $sigma);
        placet_element_set_attribute("$beamlinename", BI, "roll", randn(size(BI)) * 200);
        placet_element_set_attribute("$beamlinename", CI, "x", randn(size(CI)) * $sigma);
        placet_element_set_attribute("$beamlinename", CI, "y", randn(size(CI)) * $sigma);
        placet_element_set_attribute("$beamlinename", CI, "xp", randn(size(CI)) * $sigma);
        placet_element_set_attribute("$beamlinename", CI, "yp", randn(size(CI)) * $sigma);
        BENDs = placet_get_number_list("$beamlinename", "sbend")
        placet_element_set_attribute("$beamlinename", BENDs, "roll", randn(size(BENDs)) * 200);
    }
}




if { $machine == "all" } {
    set machine_start 1
    set machine_end $nm
} {
    set machine_start $machine
    set machine_end $machine
}

if { $wgt1 == -1 } {
    set wgt1 [expr round(sqrt(($bpmres * $bpmres + $sigma * $sigma) / (2 * $bpmres * $bpmres)))]
    puts "Using DFS theoretical weight = $wgt1"
}

if { $wgt2 == -1 } {
    set wgt2 [expr round(sqrt(($bpmres * $bpmres + $sigma * $sigma) / (2 * $bpmres * $bpmres)))]
    puts "Using WFS theoretical weight = $wgt2"
}

Octave {
    EmittX_hist = zeros($nm, 4);
    EmittY_hist = zeros($nm, 4);
}

# Binning
Octave {
    nBpms = length(Response.Bpms);
    Blen = nBpms / ($nbins * (1 - $noverlap) + $noverlap);
    for i=1:$nbins
      Bmin = floor((i - 1) * Blen - (i - 1) * Blen * $noverlap) + 1;
      Bmax = floor((i)     * Blen - (i - 1) * Blen * $noverlap);
      Cxmin = find(Response.Cx < Response.Bpms(Bmin))(end);
      Cxmax = find(Response.Cx < Response.Bpms(Bmax))(end);
      Cymin = find(Response.Cy < Response.Bpms(Bmin))(end);
      Cymax = find(Response.Cy < Response.Bpms(Bmax))(end);
      Bins(i).Bpms = Bmin:Bmax;
      Bins(i).Cx = Cxmin:Cxmax;
      Bins(i).Cy = Cymin:Cymax;
    end
    printf("Each bin contains approx %g bpms.\n", round(Blen));
}

Octave {
    SX = zeros($nm, length(Response.Cx)); % corrector strengths
    SY = zeros($nm, length(Response.Cy));
}

for {set machine $machine_start} {$machine <= $machine_end} {incr machine} {
    
    puts "MACHINE: $machine/$nm"
    
    my_survey
    
    Octave {
	disp("TRACKING...");
	[E,B] = placet_test_no_correction("$beamlinename", "beam0", "None", "%s %E %dE %ex %ey %sz");
	if exist('E0', 'var')
	E0 = E0 .+ E;
	else
	E0 = E;
	end
	EmittX_hist($machine, 1) = E(end,4);
	EmittY_hist($machine, 1) = E(end,5);
	
	disp("1-TO-1 CORRECTION $machine/$nm");
        for bin = 1:$nbins
	  Bin = Bins(bin);
	  nCx = length(Bin.Cx);
  	  nCy = length(Bin.Cy);
   	  R0x = Response.R0x(Bin.Bpms,Bin.Cx);
   	  R0y = Response.R0y(Bin.Bpms,Bin.Cy);
	  for i=1:1
	    B0 = placet_get_bpm_readings("$beamlinename", Response.Bpms);
	    B0 -= Response.B0;
   	    B0 = B0(Bin.Bpms,:);
            Cx = -[ R0x ; $beta0 * eye(nCx) ] \ [ B0(:,1) ; zeros(nCx,1) ];
	    Cy = -[ R0y ; $beta0 * eye(nCy) ] \ [ B0(:,2) ; zeros(nCy,1) ];
	    placet_element_vary_attribute("$beamlinename", Response.Cx(Bin.Cx), "strength_x", Cx);
	    placet_element_vary_attribute("$beamlinename", Response.Cy(Bin.Cy), "strength_y", Cy);
	    [E,B] = placet_test_no_correction("$beamlinename", "beam0", "None", 1, 0, Response.Bpms(Bin.Bpms(end)), "%s %E %dE %ex %ey %sz");
	  end
	end

	if exist('E1', 'var')
	E1 = E1 .+ E;
	else
	E1 = E;
	end
	EmittX_hist($machine, 2) = E(end,4);
	EmittY_hist($machine, 2) = E(end,5);

	disp("DFS CORRECTION $machine/$nm");
        for bin = 1:$nbins
	  Bin = Bins(bin);
	  nCx = length(Bin.Cx);
  	  nCy = length(Bin.Cy);
   	  R0x = Response.R0x(Bin.Bpms,Bin.Cx);
   	  R0y = Response.R0y(Bin.Bpms,Bin.Cy);
   	  R1x = Response.R1x(Bin.Bpms,Bin.Cx);
   	  R1y = Response.R1y(Bin.Bpms,Bin.Cy);
	  for i=1:1
	    B0 = placet_get_bpm_readings("$beamlinename", Response.Bpms);
	    reduce_energy();
	    placet_test_no_correction("$beamlinename", "beam0", "None", 1, 0, Response.Bpms(Bin.Bpms(end)));
	    reset_energy();
	    B1 = placet_get_bpm_readings("$beamlinename", Response.Bpms) - B0;
	    B0 -= Response.B0;
	    B1 -= Response.B1;
            B0 = B0(Bin.Bpms,:);
            B1 = B1(Bin.Bpms,:);
	    Cx = -[ R0x ; $wgt1 * R1x ; $beta1 * eye(nCx) ] \ [ B0(:,1) ; $wgt1 * B1(:,1) ; zeros(nCx,1) ];
	    Cy = -[ R0y ; $wgt1 * R1y ; $beta1 * eye(nCy) ] \ [ B0(:,2) ; $wgt1 * B1(:,2) ; zeros(nCy,1) ];
	    placet_element_vary_attribute("$beamlinename", Response.Cx(Bin.Cx), "strength_x", Cx);
	    placet_element_vary_attribute("$beamlinename", Response.Cy(Bin.Cy), "strength_y", Cy);
	    [E,B] = placet_test_no_correction("$beamlinename", "beam0", "None", 1, 0, Response.Bpms(Bin.Bpms(end)), "%s %E %dE %ex %ey %sz");
	  end
	end

	if exist('E2', 'var')
	E2 = E2 .+ E;
	else
	E2 = E;
	end
	EmittX_hist($machine, 3) = E(end,4);
	EmittY_hist($machine, 3) = E(end,5);

	disp("WFS CORRECTION $machine/$nm");
        for bin = 1:$nbins
	  Bin = Bins(bin);
	  nCx = length(Bin.Cx);
  	  nCy = length(Bin.Cy);
   	  R0x = Response.R0x(Bin.Bpms,Bin.Cx);
   	  R0y = Response.R0y(Bin.Bpms,Bin.Cy);
   	  R1x = Response.R1x(Bin.Bpms,Bin.Cx);
   	  R1y = Response.R1y(Bin.Bpms,Bin.Cy);
   	  R2x = Response.R2x(Bin.Bpms,Bin.Cx);
   	  R2y = Response.R2y(Bin.Bpms,Bin.Cy);
	  for i=1:1
	    B0 = placet_get_bpm_readings("$beamlinename", Response.Bpms);
	    % DFS
	    reduce_energy();
	    placet_test_no_correction("$beamlinename", "beam0", "None", 1, 0, Response.Bpms(Bin.Bpms(end)));
	    reset_energy();
	    B1 = placet_get_bpm_readings("$beamlinename", Response.Bpms) - B0;
            % WFS
	    placet_test_no_correction("$beamlinename", "beam1", "None", 1, 0, Response.Bpms(Bin.Bpms(end)));
	    B2 = placet_get_bpm_readings("$beamlinename", Response.Bpms) - B0;
	    %
	    B0 -= Response.B0;
	    B1 -= Response.B1;
	    B2 -= Response.B2;
            B0 = B0(Bin.Bpms,:);
            B1 = B1(Bin.Bpms,:);
            B2 = B2(Bin.Bpms,:);
	    Cx = -[ R0x ; $wgt1 * R1x ; $wgt2 * R2x ; $beta2 * eye(nCx) ] \ [ B0(:,1) ; $wgt1 * B1(:,1) ; $wgt2 * B2(:,1) ; zeros(nCx,1) ];
	    Cy = -[ R0y ; $wgt1 * R1y ; $wgt2 * R2y ; $beta2 * eye(nCy) ] \ [ B0(:,2) ; $wgt1 * B1(:,2) ; $wgt2 * B2(:,2) ; zeros(nCy,1) ];
	    placet_element_vary_attribute("$beamlinename", Response.Cx(Bin.Cx), "strength_x", Cx);
	    placet_element_vary_attribute("$beamlinename", Response.Cy(Bin.Cy), "strength_y", Cy);
	    [E,B] = placet_test_no_correction("$beamlinename", "beam0", "None", 1, 0, Response.Bpms(Bin.Bpms(end)), "%s %E %dE %ex %ey %sz");
	  end
	end

	if exist('E3', 'var')
	E3 = E3 .+ E;
	else
	E3 = E;
	end
	EmittX_hist($machine, 4) = E(end,4);
	EmittY_hist($machine, 4) = E(end,5);

	SX($machine,:) = placet_element_get_attribute("$beamlinename", Response.Cx, "strength_x");
	SY($machine,:) = placet_element_get_attribute("$beamlinename", Response.Cy, "strength_y");
    }
}

set nm [expr $machine_end - $machine_start + 1]

Octave {
    E0 = E0 / $nm;
    E1 = E1 / $nm;
    E2 = E2 / $nm;
    E3 = E3 / $nm;
    save -text ${beamlinename}_emitt_no_n_${nm}.dat E0
    save -text ${beamlinename}_emitt_simple_b_${beta0}_n_${nm}.dat E1
    save -text ${beamlinename}_emitt_dfs_w_${wgt1}_b_${beta0}_${beta1}_n_${nm}.dat E2
    save -text ${beamlinename}_emitt_wfs_w_${wgt1}_${wgt2}_b_${beta0}_${beta1}_${beta2}_c_${dcharge}_n_${nm}.dat E3
    save -text ${beamlinename}_emittx_hist_w_${wgt1}_${wgt2}_b_${beta0}_${beta1}_${beta2}_c_${dcharge}_n_${nm}.dat EmittX_hist
    save -text ${beamlinename}_emitty_hist_w_${wgt1}_${wgt2}_b_${beta0}_${beta1}_${beta2}_c_${dcharge}_n_${nm}.dat EmittY_hist
    save -text ${beamlinename}_strengthx_w_${wgt1}_${wgt2}_b_${beta0}_${beta1}_${beta2}_c_${dcharge}_n_${nm}.dat SX
    save -text ${beamlinename}_strengthy_w_${wgt1}_${wgt2}_b_${beta0}_${beta1}_${beta2}_c_${dcharge}_n_${nm}.dat SY
}

