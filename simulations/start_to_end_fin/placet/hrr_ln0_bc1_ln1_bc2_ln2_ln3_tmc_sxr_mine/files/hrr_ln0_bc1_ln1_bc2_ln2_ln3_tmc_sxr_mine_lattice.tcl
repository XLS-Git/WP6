Girder
SetReferenceEnergy   1.238601e-01
Drift      -name "LN0_DR_V5"        -length  6.639433e-01 -e0  1.237690e-01
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  1.237690e-01
Quadrupole -name "LN0_QD_V04"       -length  4.000000e-02 -e0  1.237690e-01 -strength  1.779631e-02
Dipole     -name "LN0_COR"          -length  0.000000e+00 -e0  1.237690e-01
Bpm        -name "LN0_BPM"          -length  0.000000e+00 -e0  1.237690e-01
Quadrupole -name "LN0_QD_V04"       -length  4.000000e-02 -e0  1.237690e-01 -strength  1.779631e-02
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  1.237690e-01
Drift      -name "LN0_DR_V6"        -length  2.598929e-01 -e0  1.237690e-01
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  1.237690e-01
Quadrupole -name "LN0_QD_V05"       -length  4.000000e-02 -e0  1.237690e-01 -strength  3.441588e-02
Dipole     -name "LN0_COR"          -length  0.000000e+00 -e0  1.237690e-01
Bpm        -name "LN0_BPM"          -length  0.000000e+00 -e0  1.237690e-01
Quadrupole -name "LN0_QD_V05"       -length  4.000000e-02 -e0  1.237690e-01 -strength  3.441588e-02
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  1.237690e-01
Drift      -name "LN0_DR_V7"        -length  1.419751e+00 -e0  1.237690e-01
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  1.237690e-01
Quadrupole -name "LN0_QD_V06"       -length  4.000000e-02 -e0  1.237690e-01 -strength -3.626929e-02
Dipole     -name "LN0_COR"          -length  0.000000e+00 -e0  1.237690e-01
Bpm        -name "LN0_BPM"          -length  0.000000e+00 -e0  1.237690e-01
Quadrupole -name "LN0_QD_V06"       -length  4.000000e-02 -e0  1.237690e-01 -strength -3.626929e-02
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  1.237690e-01
Drift      -name "LN0_DR_V8"        -length  2.000008e-01 -e0  1.237690e-01
Drift      -name "LN0_DR_VS"        -length  1.000000e-01 -e0  1.237690e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  1.237690e-01
Drift      -name "LN0_DR_XCA_ED_DI" -length  1.001043e-01 -e0  1.237690e-01
CrabCavity -name "LN0_XCA0_DI"      -length  9.997914e-01 -voltage  0.000000e+00 -phase  9.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.237690e-01
Drift      -name "LN0_DR_XCA_ED_DI" -length  1.001043e-01 -e0  1.237285e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  1.237285e-01
Drift      -name "LN0_TDS_DR"       -length  2.500000e-01 -e0  1.237285e-01
Drift      -name "LN0_TDS_DR"       -length  2.500000e-01 -e0  1.237285e-01
Drift      -name "LN0_TDS_DR"       -length  2.500000e-01 -e0  1.237285e-01
Drift      -name "LN0_TDS_DR"       -length  2.500000e-01 -e0  1.237285e-01
Drift      -name "LN0_TDS_DR"       -length  2.500000e-01 -e0  1.237285e-01
Drift      -name "LN0_TDS_DR"       -length  2.500000e-01 -e0  1.237285e-01
Drift      -name "LN0_TDS_DR"       -length  2.500000e-01 -e0  1.237285e-01
Drift      -name "LN0_TDS_DR"       -length  2.500000e-01 -e0  1.237285e-01
Sbend      -name "LN0_DP_TDS"       -length  1.000000e-01 -e0  1.237285e-01 -angle  0.000000e+00 -E1  0.000000e+00 -E2  0.000000e+00 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  0.000000e+00 -six_dim 1
Drift      -name "LN0_DR_20"        -length  2.000000e-01 -e0  1.237285e-01
Drift      -name "LN0_DR_VS"        -length  1.000000e-01 -e0  1.237285e-01
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  1.237285e-01
Quadrupole -name "LN0_QD_FH"        -length  4.000000e-02 -e0  1.237285e-01 -strength  3.464399e-02
Dipole     -name "LN0_COR"          -length  0.000000e+00 -e0  1.237285e-01
Bpm        -name "LN0_BPM"          -length  0.000000e+00 -e0  1.237285e-01
Quadrupole -name "LN0_QD_FH"        -length  4.000000e-02 -e0  1.237285e-01 -strength  3.464399e-02
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  1.237285e-01
Drift      -name "LN0_DR_SV"        -length  1.000000e-01 -e0  1.237285e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  1.237285e-01
Drift      -name "LN0_DR_CCA_ED"    -length  5.019800e-02 -e0  1.237285e-01
Cavity     -name "LN0_CCA0"         -length  1.899604e+00 -gradient  1.500000e-02 -phase  2.600000e+01 -frequency  5.997100e+00
SetReferenceEnergy   1.492816e-01
Drift      -name "LN0_DR_CCA_ED"    -length  5.019800e-02 -e0  1.492816e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  1.492816e-01
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  1.492816e-01
Quadrupole -name "LN0_QD_DH"        -length  4.000000e-02 -e0  1.492816e-01 -strength -4.179885e-02
Dipole     -name "LN0_COR"          -length  0.000000e+00 -e0  1.492816e-01
Bpm        -name "LN0_BPM"          -length  0.000000e+00 -e0  1.492816e-01
Quadrupole -name "LN0_QD_DH"        -length  4.000000e-02 -e0  1.492816e-01 -strength -4.179885e-02
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  1.492816e-01
Drift      -name "LN0_DR_VS"        -length  1.000000e-01 -e0  1.492816e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  1.492816e-01
Drift      -name "LN0_DR_CCA_ED"    -length  5.019800e-02 -e0  1.492816e-01
Cavity     -name "LN0_CCA0"         -length  1.899604e+00 -gradient  1.500000e-02 -phase  2.600000e+01 -frequency  5.997100e+00
SetReferenceEnergy   1.748347e-01
Drift      -name "LN0_DR_CCA_ED"    -length  5.019800e-02 -e0  1.748347e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  1.748347e-01
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  1.748347e-01
Quadrupole -name "LN0_QD_FH"        -length  4.000000e-02 -e0  1.748347e-01 -strength  4.895371e-02
Dipole     -name "LN0_COR"          -length  0.000000e+00 -e0  1.748347e-01
Bpm        -name "LN0_BPM"          -length  0.000000e+00 -e0  1.748347e-01
Quadrupole -name "LN0_QD_FH"        -length  4.000000e-02 -e0  1.748347e-01 -strength  4.895371e-02
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  1.748347e-01
Drift      -name "LN0_DR_VS"        -length  1.000000e-01 -e0  1.748347e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  1.748347e-01
Drift      -name "LN0_DR_CCA_ED"    -length  5.019800e-02 -e0  1.748347e-01
Cavity     -name "LN0_CCA0"         -length  1.899604e+00 -gradient  1.500000e-02 -phase  2.600000e+01 -frequency  5.997100e+00
SetReferenceEnergy   2.003877e-01
Drift      -name "LN0_DR_CCA_ED"    -length  5.019800e-02 -e0  2.003877e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  2.003877e-01
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  2.003877e-01
Quadrupole -name "LN0_QD_DH"        -length  4.000000e-02 -e0  2.003877e-01 -strength -5.610857e-02
Dipole     -name "LN0_COR"          -length  0.000000e+00 -e0  2.003877e-01
Bpm        -name "LN0_BPM"          -length  0.000000e+00 -e0  2.003877e-01
Quadrupole -name "LN0_QD_DH"        -length  4.000000e-02 -e0  2.003877e-01 -strength -5.610857e-02
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  2.003877e-01
Drift      -name "LN0_DR_VS"        -length  1.000000e-01 -e0  2.003877e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  2.003877e-01
Drift      -name "LN0_DR_CCA_ED"    -length  5.019800e-02 -e0  2.003877e-01
Cavity     -name "LN0_CCA0"         -length  1.899604e+00 -gradient  1.500000e-02 -phase  2.600000e+01 -frequency  5.997100e+00
SetReferenceEnergy   2.259408e-01
Drift      -name "LN0_DR_CCA_ED"    -length  5.019800e-02 -e0  2.259408e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  2.259408e-01
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  2.259408e-01
Quadrupole -name "LN0_QD_FH"        -length  4.000000e-02 -e0  2.259408e-01 -strength  6.326343e-02
Dipole     -name "LN0_COR"          -length  0.000000e+00 -e0  2.259408e-01
Bpm        -name "LN0_BPM"          -length  0.000000e+00 -e0  2.259408e-01
Quadrupole -name "LN0_QD_FH"        -length  4.000000e-02 -e0  2.259408e-01 -strength  6.326343e-02
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  2.259408e-01
Drift      -name "LN0_DR_VS"        -length  1.000000e-01 -e0  2.259408e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  2.259408e-01
Drift      -name "LN0_DR_CCA_ED"    -length  5.019800e-02 -e0  2.259408e-01
Cavity     -name "LN0_CCA0"         -length  1.899604e+00 -gradient  1.500000e-02 -phase  2.600000e+01 -frequency  5.997100e+00
SetReferenceEnergy   2.514939e-01
Drift      -name "LN0_DR_CCA_ED"    -length  5.019800e-02 -e0  2.514939e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  2.514939e-01
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  2.514939e-01
Quadrupole -name "LN0_QD_DH"        -length  4.000000e-02 -e0  2.514939e-01 -strength -7.041830e-02
Dipole     -name "LN0_COR"          -length  0.000000e+00 -e0  2.514939e-01
Bpm        -name "LN0_BPM"          -length  0.000000e+00 -e0  2.514939e-01
Quadrupole -name "LN0_QD_DH"        -length  4.000000e-02 -e0  2.514939e-01 -strength -7.041830e-02
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  2.514939e-01
Drift      -name "LN0_DR_VS"        -length  1.000000e-01 -e0  2.514939e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  2.514939e-01
Drift      -name "LN0_DR_CCA_ED"    -length  5.019800e-02 -e0  2.514939e-01
Cavity     -name "LN0_CCA0"         -length  1.899604e+00 -gradient  1.500000e-02 -phase  2.600000e+01 -frequency  5.997100e+00
SetReferenceEnergy   2.770470e-01
Drift      -name "LN0_DR_CCA_ED"    -length  5.019800e-02 -e0  2.770470e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  2.770470e-01
Drift      -name "LN0_WA_LNZ_IN2"   -length  0.000000e+00 -e0  2.770470e-01
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  2.770470e-01
Quadrupole -name "LN0_QD_FH"        -length  4.000000e-02 -e0  2.770470e-01 -strength  7.757317e-02
Dipole     -name "LN0_COR"          -length  0.000000e+00 -e0  2.770470e-01
Bpm        -name "LN0_BPM"          -length  0.000000e+00 -e0  2.770470e-01
Quadrupole -name "LN0_QD_FH"        -length  4.000000e-02 -e0  2.770470e-01 -strength  7.757317e-02
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  2.770470e-01
Drift      -name "LN0_DR_VS"        -length  1.000000e-01 -e0  2.770470e-01
Drift      -name "LN0_DR_LNZ_ED2"   -length  6.250000e-01 -e0  2.770470e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  2.770470e-01
Drift      -name "LN0_DR_KCA_ED"    -length  2.225410e-02 -e0  2.770470e-01
Cavity     -name "LN0_KCA0"         -length  3.054918e-01 -gradient  2.453617e-02 -phase  1.920000e+02 -frequency  3.598260e+01
SetReferenceEnergy   2.698730e-01
Drift      -name "LN0_DR_KCA_ED"    -length  2.225410e-02 -e0  2.698730e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  2.698730e-01
Drift      -name "LN0_DR_KCA_ED"    -length  2.225410e-02 -e0  2.698730e-01
Drift      -name "LN0_DR_KCA0"      -length  3.054918e-01 -e0  2.698730e-01
Drift      -name "LN0_DR_KCA_ED"    -length  2.225410e-02 -e0  2.698730e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  2.698730e-01
Drift      -name "LN0_DR_LNZ_ED2"   -length  6.250000e-01 -e0  2.698730e-01
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  2.698730e-01
Quadrupole -name "LN0_QD_DH"        -length  4.000000e-02 -e0  2.698730e-01 -strength -7.556445e-02
Dipole     -name "LN0_COR"          -length  0.000000e+00 -e0  2.698730e-01
Bpm        -name "LN0_BPM"          -length  0.000000e+00 -e0  2.698730e-01
Quadrupole -name "LN0_QD_DH"        -length  4.000000e-02 -e0  2.698730e-01 -strength -7.556445e-02
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  2.698730e-01
Drift      -name "LN0_DR_DR"        -length  1.000000e-01 -e0  2.698730e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  2.698730e-01
Drift      -name "LN0_DR_CCA_ED"    -length  5.019800e-02 -e0  2.698730e-01
Drift      -name "LN0_DR_CCA0H"     -length  3.498020e-01 -e0  2.698730e-01
Drift      -name "LN0_DR_A"         -length  2.000000e-01 -e0  2.698730e-01
Drift      -name "LN0_DR_VS"        -length  1.000000e-01 -e0  2.698730e-01
Drift      -name "LN0_DR_A"         -length  2.000000e-01 -e0  2.698730e-01
Drift      -name "LN0_DR_CT"        -length  1.000000e-01 -e0  2.698730e-01
Drift      -name "LN0_WA_OU2"       -length  0.000000e+00 -e0  2.698730e-01
Drift      -name "BC1_DR_V1"        -length  5.382067e-01 -e0  2.698730e-01
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.698730e-01
Quadrupole -name "BC1_QD_V01"       -length  4.000000e-02 -e0  2.698730e-01 -strength  2.032735e-02
Dipole     -name "BC1_COR"          -length  0.000000e+00 -e0  2.698730e-01
Bpm        -name "BC1_BPM"          -length  0.000000e+00 -e0  2.698730e-01
Quadrupole -name "BC1_QD_V01"       -length  4.000000e-02 -e0  2.698730e-01 -strength  2.032735e-02
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.698730e-01
Drift      -name "BC1_DR_V2"        -length  5.900340e+00 -e0  2.698730e-01
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.698730e-01
Quadrupole -name "BC1_QD_V02"       -length  4.000000e-02 -e0  2.698730e-01 -strength -9.786305e-02
Dipole     -name "BC1_COR"          -length  0.000000e+00 -e0  2.698730e-01
Bpm        -name "BC1_BPM"          -length  0.000000e+00 -e0  2.698730e-01
Quadrupole -name "BC1_QD_V02"       -length  4.000000e-02 -e0  2.698730e-01 -strength -9.786305e-02
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.698730e-01
Drift      -name "BC1_DR_V3"        -length  2.496128e-01 -e0  2.698730e-01
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.698730e-01
Quadrupole -name "BC1_QD_V03"       -length  4.000000e-02 -e0  2.698730e-01 -strength  3.530938e-02
Dipole     -name "BC1_COR"          -length  0.000000e+00 -e0  2.698730e-01
Bpm        -name "BC1_BPM"          -length  0.000000e+00 -e0  2.698730e-01
Quadrupole -name "BC1_QD_V03"       -length  4.000000e-02 -e0  2.698730e-01 -strength  3.530938e-02
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.698730e-01
Drift      -name "BC1_DR_V4"        -length  2.000000e-01 -e0  2.698730e-01
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.698730e-01
Quadrupole -name "BC1_QD_V04"       -length  4.000000e-02 -e0  2.698730e-01 -strength  5.690781e-02
Dipole     -name "BC1_COR"          -length  0.000000e+00 -e0  2.698730e-01
Bpm        -name "BC1_BPM"          -length  0.000000e+00 -e0  2.698730e-01
Quadrupole -name "BC1_QD_V04"       -length  4.000000e-02 -e0  2.698730e-01 -strength  5.690781e-02
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.698730e-01
Drift      -name "BC1_DR_V5"        -length  2.002040e-01 -e0  2.698730e-01
Drift      -name "BC1_DR_20"        -length  5.000000e-01 -e0  2.698730e-01
Sbend      -name "BC1_DP_DIP1"      -length  4.002981e-01 -e0  2.698729e-01 -angle -6.684611e-02 -E1  0.000000e+00 -E2 -6.684611e-02 -hgap  1.500000e-02 -fint  5.000000e-01 -tilt  0.000000e+00  -six_dim 1 -csr 1 
Drift      -name "BC1_DR_SIDE_C"    -length  3.000000e-01 -e0  2.698724e-01
Drift      -name "BC1_DR_SIDE"      -length  2.957275e+00 -e0  2.698724e-01
Sbend      -name "BC1_DP_DIP2"      -length  4.002981e-01 -e0  2.698720e-01 -angle  6.684611e-02 -E1  6.684611e-02 -E2  0.000000e+00 -hgap  1.500000e-02 -fint  5.000000e-01 -tilt  0.000000e+00  -six_dim 1 -csr 1 
Drift      -name "BC1_DR_CENT_C"    -length  3.500000e-01 -e0  2.698701e-01
Bpm        -name "BC1_BPM"          -length  0.000000e+00 -e0  2.698701e-01
Drift      -name "BC1_DR_CENT"      -length  3.500000e-01 -e0  2.698701e-01
Sbend      -name "BC1_DP_DIP3"      -length  4.002981e-01 -e0  2.698608e-01 -angle  6.684611e-02 -E1  0.000000e+00 -E2  6.684611e-02 -hgap  1.500000e-02 -fint  5.000000e-01 -tilt  0.000000e+00  -six_dim 1 -csr 1 
Drift      -name "BC1_DR_SIDE_C"    -length  3.000000e-01 -e0  2.698418e-01
Drift      -name "BC1_DR_SIDE"      -length  2.957275e+00 -e0  2.698418e-01
Sbend      -name "BC1_DP_DIP4"      -length  4.002981e-01 -e0  2.698179e-01 -angle -6.684611e-02 -E1 -6.684611e-02 -E2  0.000000e+00 -hgap  1.500000e-02 -fint  5.000000e-01 -tilt  0.000000e+00  -six_dim 1 -csr 1 
Drift      -name "BC1_DR_20_C"      -length  3.000000e-01 -e0  2.698013e-01
Drift      -name "BC1_WA_OU2"       -length  0.000000e+00 -e0  2.698013e-01
Drift      -name "BC1_DR_CT"        -length  1.000000e-01 -e0  2.698013e-01
Drift      -name "BC1_DR_DI_V1"     -length  2.911672e-01 -e0  2.698013e-01
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.698013e-01
Quadrupole -name "BC1_QD_DI_V01"    -length  4.000000e-02 -e0  2.698013e-01 -strength -9.231869e-02
Dipole     -name "BC1_COR_DI"       -length  0.000000e+00 -e0  2.698013e-01
Bpm        -name "BC1_BPM_DI"       -length  0.000000e+00 -e0  2.698013e-01
Quadrupole -name "BC1_QD_DI_V01"    -length  4.000000e-02 -e0  2.698013e-01 -strength -9.231869e-02
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.698013e-01
Drift      -name "BC1_DR_DI_V2"     -length  4.700020e-01 -e0  2.698013e-01
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.698013e-01
Quadrupole -name "BC1_QD_DI_V02"    -length  4.000000e-02 -e0  2.698013e-01 -strength  1.468554e-01
Dipole     -name "BC1_COR_DI"       -length  0.000000e+00 -e0  2.698013e-01
Bpm        -name "BC1_BPM_DI"       -length  0.000000e+00 -e0  2.698013e-01
Quadrupole -name "BC1_QD_DI_V02"    -length  4.000000e-02 -e0  2.698013e-01 -strength  1.468554e-01
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.698013e-01
Drift      -name "BC1_DR_DI_V3"     -length  1.199083e+00 -e0  2.698013e-01
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.698013e-01
Quadrupole -name "BC1_QD_DI_V03"    -length  4.000000e-02 -e0  2.698013e-01 -strength -5.605805e-02
Dipole     -name "BC1_COR_DI"       -length  0.000000e+00 -e0  2.698013e-01
Bpm        -name "BC1_BPM_DI"       -length  0.000000e+00 -e0  2.698013e-01
Quadrupole -name "BC1_QD_DI_V03"    -length  4.000000e-02 -e0  2.698013e-01 -strength -5.605805e-02
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.698013e-01
Drift      -name "BC1_DR_DI_V5"     -length  2.000000e-01 -e0  2.698013e-01
Drift      -name "BC1_DR_DI"        -length  2.500000e-01 -e0  2.698013e-01
Drift      -name "BC1_DR_VS"        -length  1.000000e-01 -e0  2.698013e-01
Drift      -name "BC1_DR_BL"        -length  5.000000e-02 -e0  2.698013e-01
Drift      -name "BC1_DR_XCA_ED_DI" -length  1.001043e-01 -e0  2.698013e-01
CrabCavity -name "BC1_XCA0_DI"      -length  9.997914e-01 -voltage  0.000000e+00 -phase  9.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.698013e-01
Drift      -name "BC1_DR_XCA_ED_DI" -length  1.001043e-01 -e0  2.697334e-01
Drift      -name "BC1_DR_BL"        -length  5.000000e-02 -e0  2.697334e-01
Drift      -name "BC1_DR_VS"        -length  1.000000e-01 -e0  2.697334e-01
Drift      -name "BC1_DR_DI"        -length  2.500000e-01 -e0  2.697334e-01
Drift      -name "BC1_DR_DI"        -length  2.500000e-01 -e0  2.697334e-01
Drift      -name "BC1_DR_DI_QD_ED"  -length  4.250000e-02 -e0  2.697334e-01
Quadrupole -name "BC1_QD_DI_FH"     -length  4.000000e-02 -e0  2.697334e-01 -strength  8.631467e-02
Dipole     -name "BC1_COR_DI"       -length  0.000000e+00 -e0  2.697334e-01
Bpm        -name "BC1_BPM_DI"       -length  0.000000e+00 -e0  2.697334e-01
Quadrupole -name "BC1_QD_DI_FH"     -length  4.000000e-02 -e0  2.697334e-01 -strength  8.631467e-02
Drift      -name "BC1_DR_DI_QD_ED"  -length  4.250000e-02 -e0  2.697334e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697334e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697334e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697334e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697334e-01
Drift      -name "BC1_DR_DI_VS"     -length  1.000000e-01 -e0  2.697334e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697334e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697334e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697334e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697334e-01
Drift      -name "BC1_DR_DI_QD_ED"  -length  4.250000e-02 -e0  2.697334e-01
Quadrupole -name "BC1_QD_DI_DH"     -length  4.000000e-02 -e0  2.697334e-01 -strength -8.631467e-02
Dipole     -name "BC1_COR_DI"       -length  0.000000e+00 -e0  2.697334e-01
Bpm        -name "BC1_BPM_DI"       -length  0.000000e+00 -e0  2.697334e-01
Quadrupole -name "BC1_QD_DI_DH"     -length  4.000000e-02 -e0  2.697334e-01 -strength -8.631467e-02
Drift      -name "BC1_DR_DI_QD_ED"  -length  4.250000e-02 -e0  2.697334e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697334e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697334e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697334e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697334e-01
Drift      -name "BC1_DR_DI_VS"     -length  1.000000e-01 -e0  2.697334e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697334e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697334e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697334e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697334e-01
Drift      -name "BC1_DR_DI_QD_ED"  -length  4.250000e-02 -e0  2.697334e-01
Quadrupole -name "BC1_QD_DI_FH"     -length  4.000000e-02 -e0  2.697334e-01 -strength  8.631467e-02
Dipole     -name "BC1_COR_DI"       -length  0.000000e+00 -e0  2.697334e-01
Bpm        -name "BC1_BPM_DI"       -length  0.000000e+00 -e0  2.697334e-01
Quadrupole -name "BC1_QD_DI_FH"     -length  4.000000e-02 -e0  2.697334e-01 -strength  8.631467e-02
Drift      -name "BC1_DR_DI_QD_ED"  -length  4.250000e-02 -e0  2.697334e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697334e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697334e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697334e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697334e-01
Drift      -name "BC1_DR_DI_VS"     -length  1.000000e-01 -e0  2.697334e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697334e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697334e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697334e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697334e-01
Drift      -name "BC1_DR_DI_QD_ED"  -length  4.250000e-02 -e0  2.697334e-01
Quadrupole -name "BC1_QD_DI_DH"     -length  4.000000e-02 -e0  2.697334e-01 -strength -8.631467e-02
Dipole     -name "BC1_COR_DI"       -length  0.000000e+00 -e0  2.697334e-01
Bpm        -name "BC1_BPM_DI"       -length  0.000000e+00 -e0  2.697334e-01
Quadrupole -name "BC1_QD_DI_DH"     -length  4.000000e-02 -e0  2.697334e-01 -strength -8.631467e-02
Drift      -name "BC1_DR_DI_QD_ED"  -length  4.250000e-02 -e0  2.697334e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697334e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697334e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697334e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697334e-01
Drift      -name "BC1_DR_DI_VS"     -length  1.000000e-01 -e0  2.697334e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697334e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697334e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697334e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697334e-01
Drift      -name "BC1_DR_DI_QD_ED"  -length  4.250000e-02 -e0  2.697334e-01
Quadrupole -name "BC1_QD_DI_FH"     -length  4.000000e-02 -e0  2.697334e-01 -strength  8.631467e-02
Dipole     -name "BC1_COR_DI"       -length  0.000000e+00 -e0  2.697334e-01
Bpm        -name "BC1_BPM_DI"       -length  0.000000e+00 -e0  2.697334e-01
Quadrupole -name "BC1_QD_DI_FH"     -length  4.000000e-02 -e0  2.697334e-01 -strength  8.631467e-02
Drift      -name "BC1_DR_DI_QD_ED"  -length  4.250000e-02 -e0  2.697334e-01
Drift      -name "BC1_DR_DI"        -length  2.500000e-01 -e0  2.697334e-01
Sbend      -name "BC1_DP_DI"        -length  2.500000e-01 -e0  2.697334e-01 -angle  0.000000e+00 -E1  0.000000e+00 -E2  0.000000e+00 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  0.000000e+00 -six_dim 1
Drift      -name "BC1_DR_DI"        -length  2.500000e-01 -e0  2.697334e-01
Drift      -name "BC1_DR_DI"        -length  2.500000e-01 -e0  2.697334e-01
Drift      -name "BC1_DR_DI"        -length  2.500000e-01 -e0  2.697334e-01
Drift      -name "BC1_WA_OU_DI2"    -length  0.000000e+00 -e0  2.697334e-01
Drift      -name "LN1_DR_V1"        -length  2.406495e-01 -e0  2.697334e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  2.697334e-01
Quadrupole -name "LN1_QD_V01"       -length  4.000000e-02 -e0  2.697334e-01 -strength  5.687075e-02
Dipole     -name "LN1_COR"          -length  0.000000e+00 -e0  2.697334e-01
Bpm        -name "LN1_BPM"          -length  0.000000e+00 -e0  2.697334e-01
Quadrupole -name "LN1_QD_V01"       -length  4.000000e-02 -e0  2.697334e-01 -strength  5.687075e-02
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  2.697334e-01
Drift      -name "LN1_DR_V2"        -length  2.473804e-01 -e0  2.697334e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  2.697334e-01
Quadrupole -name "LN1_QD_V02"       -length  4.000000e-02 -e0  2.697334e-01 -strength -1.170449e-01
Dipole     -name "LN1_COR"          -length  0.000000e+00 -e0  2.697334e-01
Bpm        -name "LN1_BPM"          -length  0.000000e+00 -e0  2.697334e-01
Quadrupole -name "LN1_QD_V02"       -length  4.000000e-02 -e0  2.697334e-01 -strength -1.170449e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  2.697334e-01
Drift      -name "LN1_DR_V3"        -length  1.851602e+00 -e0  2.697334e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  2.697334e-01
Quadrupole -name "LN1_QD_V03"       -length  4.000000e-02 -e0  2.697334e-01 -strength  1.358433e-02
Dipole     -name "LN1_COR"          -length  0.000000e+00 -e0  2.697334e-01
Bpm        -name "LN1_BPM"          -length  0.000000e+00 -e0  2.697334e-01
Quadrupole -name "LN1_QD_V03"       -length  4.000000e-02 -e0  2.697334e-01 -strength  1.358433e-02
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  2.697334e-01
Drift      -name "LN1_DR_V4"        -length  1.000069e-01 -e0  2.697334e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  2.697334e-01
Quadrupole -name "LN1_QD_FH"        -length  4.000000e-02 -e0  2.697334e-01 -strength  7.120961e-02
Dipole     -name "LN1_COR"          -length  0.000000e+00 -e0  2.697334e-01
Bpm        -name "LN1_BPM"          -length  0.000000e+00 -e0  2.697334e-01
Quadrupole -name "LN1_QD_FH"        -length  4.000000e-02 -e0  2.697334e-01 -strength  7.120961e-02
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  2.697334e-01
Drift      -name "LN1_DR_SV"        -length  1.000000e-01 -e0  2.697334e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  2.697334e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  2.697334e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  3.100000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.932193e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  2.932193e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  2.932193e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  2.932193e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  3.100000e+01 -frequency  1.199420e+01
SetReferenceEnergy   3.167051e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  3.167051e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  3.167051e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  3.167051e-01
Quadrupole -name "LN1_QD_DH"        -length  4.000000e-02 -e0  3.167051e-01 -strength -8.361016e-02
Dipole     -name "LN1_COR"          -length  0.000000e+00 -e0  3.167051e-01
Bpm        -name "LN1_BPM"          -length  0.000000e+00 -e0  3.167051e-01
Quadrupole -name "LN1_QD_DH"        -length  4.000000e-02 -e0  3.167051e-01 -strength -8.361016e-02
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  3.167051e-01
Drift      -name "LN1_DR_VS"        -length  1.000000e-01 -e0  3.167051e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  3.167051e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  3.167051e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  3.100000e+01 -frequency  1.199420e+01
SetReferenceEnergy   3.401910e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  3.401910e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  3.401910e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  3.401910e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  3.100000e+01 -frequency  1.199420e+01
SetReferenceEnergy   3.636769e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  3.636769e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  3.636769e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  3.636769e-01
Quadrupole -name "LN1_QD_FH"        -length  4.000000e-02 -e0  3.636769e-01 -strength  9.601070e-02
Dipole     -name "LN1_COR"          -length  0.000000e+00 -e0  3.636769e-01
Bpm        -name "LN1_BPM"          -length  0.000000e+00 -e0  3.636769e-01
Quadrupole -name "LN1_QD_FH"        -length  4.000000e-02 -e0  3.636769e-01 -strength  9.601070e-02
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  3.636769e-01
Drift      -name "LN1_DR_SV"        -length  1.000000e-01 -e0  3.636769e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  3.636769e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  3.636769e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  3.100000e+01 -frequency  1.199420e+01
SetReferenceEnergy   3.871628e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  3.871628e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  3.871628e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  3.871628e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  3.100000e+01 -frequency  1.199420e+01
SetReferenceEnergy   4.106486e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  4.106486e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  4.106486e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  4.106486e-01
Quadrupole -name "LN1_QD_DH"        -length  4.000000e-02 -e0  4.106486e-01 -strength -1.084112e-01
Dipole     -name "LN1_COR"          -length  0.000000e+00 -e0  4.106486e-01
Bpm        -name "LN1_BPM"          -length  0.000000e+00 -e0  4.106486e-01
Quadrupole -name "LN1_QD_DH"        -length  4.000000e-02 -e0  4.106486e-01 -strength -1.084112e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  4.106486e-01
Drift      -name "LN1_DR_VS"        -length  1.000000e-01 -e0  4.106486e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  4.106486e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  4.106486e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  3.100000e+01 -frequency  1.199420e+01
SetReferenceEnergy   4.341345e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  4.341345e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  4.341345e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  4.341345e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  3.100000e+01 -frequency  1.199420e+01
SetReferenceEnergy   4.576203e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  4.576203e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  4.576203e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  4.576203e-01
Quadrupole -name "LN1_QD_FH"        -length  4.000000e-02 -e0  4.576203e-01 -strength  1.208118e-01
Dipole     -name "LN1_COR"          -length  0.000000e+00 -e0  4.576203e-01
Bpm        -name "LN1_BPM"          -length  0.000000e+00 -e0  4.576203e-01
Quadrupole -name "LN1_QD_FH"        -length  4.000000e-02 -e0  4.576203e-01 -strength  1.208118e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  4.576203e-01
Drift      -name "LN1_DR_SV"        -length  1.000000e-01 -e0  4.576203e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  4.576203e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  4.576203e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  3.100000e+01 -frequency  1.199420e+01
SetReferenceEnergy   4.811062e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  4.811062e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  4.811062e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  4.811062e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  3.100000e+01 -frequency  1.199420e+01
SetReferenceEnergy   5.045920e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  5.045920e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  5.045920e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  5.045920e-01
Quadrupole -name "LN1_QD_DH"        -length  4.000000e-02 -e0  5.045920e-01 -strength -1.332123e-01
Dipole     -name "LN1_COR"          -length  0.000000e+00 -e0  5.045920e-01
Bpm        -name "LN1_BPM"          -length  0.000000e+00 -e0  5.045920e-01
Quadrupole -name "LN1_QD_DH"        -length  4.000000e-02 -e0  5.045920e-01 -strength -1.332123e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  5.045920e-01
Drift      -name "LN1_DR_VS"        -length  1.000000e-01 -e0  5.045920e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  5.045920e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  5.045920e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  3.100000e+01 -frequency  1.199420e+01
SetReferenceEnergy   5.280779e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  5.280779e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  5.280779e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  5.280779e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  3.100000e+01 -frequency  1.199420e+01
SetReferenceEnergy   5.515637e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  5.515637e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  5.515637e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  5.515637e-01
Quadrupole -name "LN1_QD_FH"        -length  4.000000e-02 -e0  5.515637e-01 -strength  1.456128e-01
Dipole     -name "LN1_COR"          -length  0.000000e+00 -e0  5.515637e-01
Bpm        -name "LN1_BPM"          -length  0.000000e+00 -e0  5.515637e-01
Quadrupole -name "LN1_QD_FH"        -length  4.000000e-02 -e0  5.515637e-01 -strength  1.456128e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  5.515637e-01
Drift      -name "LN1_DR_SV"        -length  1.000000e-01 -e0  5.515637e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  5.515637e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  5.515637e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  3.100000e+01 -frequency  1.199420e+01
SetReferenceEnergy   5.750496e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  5.750496e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  5.750496e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  5.750496e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  3.100000e+01 -frequency  1.199420e+01
SetReferenceEnergy   5.985354e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  5.985354e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  5.985354e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  5.985354e-01
Quadrupole -name "LN1_QD_DH"        -length  4.000000e-02 -e0  5.985354e-01 -strength -1.580134e-01
Dipole     -name "LN1_COR"          -length  0.000000e+00 -e0  5.985354e-01
Bpm        -name "LN1_BPM"          -length  0.000000e+00 -e0  5.985354e-01
Quadrupole -name "LN1_QD_DH"        -length  4.000000e-02 -e0  5.985354e-01 -strength -1.580134e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  5.985354e-01
Drift      -name "LN1_DR_VS"        -length  1.000000e-01 -e0  5.985354e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  5.985354e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  5.985354e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  3.100000e+01 -frequency  1.199420e+01
SetReferenceEnergy   6.220213e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  6.220213e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  6.220213e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  6.220213e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  3.100000e+01 -frequency  1.199420e+01
SetReferenceEnergy   6.455071e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  6.455071e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  6.455071e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  6.455071e-01
Quadrupole -name "LN1_QD_FH"        -length  4.000000e-02 -e0  6.455071e-01 -strength  1.704139e-01
Dipole     -name "LN1_COR"          -length  0.000000e+00 -e0  6.455071e-01
Bpm        -name "LN1_BPM"          -length  0.000000e+00 -e0  6.455071e-01
Quadrupole -name "LN1_QD_FH"        -length  4.000000e-02 -e0  6.455071e-01 -strength  1.704139e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  6.455071e-01
Drift      -name "LN1_DR_DR"        -length  1.000000e-01 -e0  6.455071e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  6.455071e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  6.455071e-01
Drift      -name "LN1_DR_XCA_"      -length  9.164755e-01 -e0  6.455071e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  6.455071e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  6.455071e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  6.455071e-01
Drift      -name "LN1_DR_XCA_"      -length  9.164755e-01 -e0  6.455071e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  6.455071e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  6.455071e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  6.455071e-01
Quadrupole -name "LN1_QD_DH"        -length  4.000000e-02 -e0  6.455071e-01 -strength -1.704139e-01
Dipole     -name "LN1_COR"          -length  0.000000e+00 -e0  6.455071e-01
Bpm        -name "LN1_BPM"          -length  0.000000e+00 -e0  6.455071e-01
Quadrupole -name "LN1_QD_DH"        -length  4.000000e-02 -e0  6.455071e-01 -strength -1.704139e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  6.455071e-01
Drift      -name "LN1_DR_DR"        -length  1.000000e-01 -e0  6.455071e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  6.455071e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  6.455071e-01
Drift      -name "LN1_DR_XCA_A"     -length  8.164755e-01 -e0  6.455071e-01
Drift      -name "LN1_DR_VS"        -length  1.000000e-01 -e0  6.455071e-01
Drift      -name "LN1_WA_OU2"       -length  0.000000e+00 -e0  6.455071e-01
Drift      -name "BC2_IN_CNT"       -length  0.000000e+00 -e0  6.455071e-01
Drift      -name "BC2_DR_V1"        -length  4.718667e-01 -e0  6.455071e-01
Drift      -name "BC2_DR_QD_ED"     -length  4.250000e-02 -e0  6.455071e-01
Quadrupole -name "BC2_QD_V01"       -length  4.000000e-02 -e0  6.455071e-01 -strength  9.773863e-03
Dipole     -name "BC2_COR"          -length  0.000000e+00 -e0  6.455071e-01
Bpm        -name "BC2_BPM"          -length  0.000000e+00 -e0  6.455071e-01
Quadrupole -name "BC2_QD_V01"       -length  4.000000e-02 -e0  6.455071e-01 -strength  9.773863e-03
Drift      -name "BC2_DR_QD_ED"     -length  4.250000e-02 -e0  6.455071e-01
Drift      -name "BC2_DR_V2"        -length  5.872423e+00 -e0  6.455071e-01
Drift      -name "BC2_DR_QD_ED"     -length  4.250000e-02 -e0  6.455071e-01
Quadrupole -name "BC2_QD_V02"       -length  4.000000e-02 -e0  6.455071e-01 -strength -3.000722e-01
Dipole     -name "BC2_COR"          -length  0.000000e+00 -e0  6.455071e-01
Bpm        -name "BC2_BPM"          -length  0.000000e+00 -e0  6.455071e-01
Quadrupole -name "BC2_QD_V02"       -length  4.000000e-02 -e0  6.455071e-01 -strength -3.000722e-01
Drift      -name "BC2_DR_QD_ED"     -length  4.250000e-02 -e0  6.455071e-01
Drift      -name "BC2_DR_V3"        -length  2.000000e-01 -e0  6.455071e-01
Drift      -name "BC2_DR_QD_ED"     -length  4.250000e-02 -e0  6.455071e-01
Quadrupole -name "BC2_QD_V03"       -length  4.000000e-02 -e0  6.455071e-01 -strength  2.781983e-01
Dipole     -name "BC2_COR"          -length  0.000000e+00 -e0  6.455071e-01
Bpm        -name "BC2_BPM"          -length  0.000000e+00 -e0  6.455071e-01
Quadrupole -name "BC2_QD_V03"       -length  4.000000e-02 -e0  6.455071e-01 -strength  2.781983e-01
Drift      -name "BC2_DR_QD_ED"     -length  4.250000e-02 -e0  6.455071e-01
Drift      -name "BC2_DR_V4"        -length  2.000000e-01 -e0  6.455071e-01
Drift      -name "BC2_DR_20"        -length  5.000000e-01 -e0  6.455071e-01
Sbend      -name "BC2_DP_DIP1"      -length  6.000476e-01 -e0  6.455023e-01 -angle -2.181662e-02 -E1  0.000000e+00 -E2 -2.181662e-02 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  0.000000e+00  -six_dim 1 -csr 1 
Drift      -name "BC2_DR_SIDE_C"    -length  5.000000e-01 -e0  6.454893e-01
Drift      -name "BC2_DR_SIDE"      -length  3.200881e+00 -e0  6.454893e-01
Sbend      -name "BC2_DP_DIP2"      -length  6.000476e-01 -e0  6.454773e-01 -angle  2.181662e-02 -E1  2.181662e-02 -E2  0.000000e+00 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  0.000000e+00  -six_dim 1 -csr 1 
Drift      -name "BC2_DR_CENT_C"    -length  2.500000e-01 -e0  6.454621e-01
Bpm        -name "BC2_BPM"          -length  0.000000e+00 -e0  6.454621e-01
Drift      -name "BC2_DR_CENT"      -length  2.500000e-01 -e0  6.454621e-01
Sbend      -name "BC2_DP_DIP3"      -length  6.000476e-01 -e0  6.454035e-01 -angle  2.181662e-02 -E1  0.000000e+00 -E2  2.181662e-02 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  0.000000e+00  -six_dim 1 -csr 1 
Drift      -name "BC2_DR_SIDE_C"    -length  5.000000e-01 -e0  6.453216e-01
Drift      -name "BC2_DR_SIDE"      -length  3.200881e+00 -e0  6.453216e-01
Sbend      -name "BC2_DP_DIP4"      -length  6.000476e-01 -e0  6.452295e-01 -angle -2.181662e-02 -E1 -2.181662e-02 -E2  0.000000e+00 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  0.000000e+00  -six_dim 1 -csr 1 
Drift      -name "BC2_IN_CNT"       -length  0.000000e+00 -e0  6.452295e-01
Drift      -name "BC2_DR_20_C"      -length  5.000000e-01 -e0  6.451551e-01
Drift      -name "BC2_IN_CNT"       -length  0.000000e+00 -e0  6.451551e-01
Drift      -name "BC2_WA_OU2"       -length  0.000000e+00 -e0  6.451551e-01
Drift      -name "LN2_DR_CT"        -length  1.000000e-01 -e0  6.451551e-01
Drift      -name "LN2_DR_V1"        -length  2.294830e+00 -e0  6.451551e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  6.451551e-01
Quadrupole -name "LN2_QD_V01"       -length  4.000000e-02 -e0  6.451551e-01 -strength  3.443561e-01
Dipole     -name "LN2_COR"          -length  0.000000e+00 -e0  6.451551e-01
Bpm        -name "LN2_BPM"          -length  0.000000e+00 -e0  6.451551e-01
Quadrupole -name "LN2_QD_V01"       -length  4.000000e-02 -e0  6.451551e-01 -strength  3.443561e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  6.451551e-01
Drift      -name "LN2_DR_V2"        -length  5.090891e-01 -e0  6.451551e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  6.451551e-01
Quadrupole -name "LN2_QD_V02"       -length  4.000000e-02 -e0  6.451551e-01 -strength -3.341057e-01
Dipole     -name "LN2_COR"          -length  0.000000e+00 -e0  6.451551e-01
Bpm        -name "LN2_BPM"          -length  0.000000e+00 -e0  6.451551e-01
Quadrupole -name "LN2_QD_V02"       -length  4.000000e-02 -e0  6.451551e-01 -strength -3.341057e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  6.451551e-01
Drift      -name "LN2_DR_V3"        -length  1.490464e+00 -e0  6.451551e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  6.451551e-01
Quadrupole -name "LN2_QD_V03"       -length  4.000000e-02 -e0  6.451551e-01 -strength  2.274475e-01
Dipole     -name "LN2_COR"          -length  0.000000e+00 -e0  6.451551e-01
Bpm        -name "LN2_BPM"          -length  0.000000e+00 -e0  6.451551e-01
Quadrupole -name "LN2_QD_V03"       -length  4.000000e-02 -e0  6.451551e-01 -strength  2.274475e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  6.451551e-01
Drift      -name "LN2_DR_V4"        -length  2.999939e+00 -e0  6.451551e-01
Drift      -name "LN2_DR_VS"        -length  1.000000e-01 -e0  6.451551e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  6.451551e-01
Quadrupole -name "LN2_QD_FH"        -length  4.000000e-02 -e0  6.451551e-01 -strength  1.729016e-01
Dipole     -name "LN2_COR"          -length  0.000000e+00 -e0  6.451551e-01
Bpm        -name "LN2_BPM"          -length  0.000000e+00 -e0  6.451551e-01
Quadrupole -name "LN2_QD_FH"        -length  4.000000e-02 -e0  6.451551e-01 -strength  1.729016e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  6.451551e-01
Drift      -name "LN2_DR_SV"        -length  1.000000e-01 -e0  6.451551e-01
Drift      -name "LN2_DR_BL"        -length  5.000000e-02 -e0  6.451551e-01
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  6.451551e-01
Cavity     -name "LN2_XCA0"         -length  9.164755e-01 -gradient  2.520000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   6.654557e-01
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  6.654557e-01
Drift      -name "LN2_DR_BL"        -length  5.000000e-02 -e0  6.654557e-01
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  6.654557e-01
Cavity     -name "LN2_XCA0"         -length  9.164755e-01 -gradient  2.520000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   6.857563e-01
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  6.857563e-01
Drift      -name "LN2_DR_BL"        -length  5.000000e-02 -e0  6.857563e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  6.857563e-01
Quadrupole -name "LN2_QD_DH"        -length  4.000000e-02 -e0  6.857563e-01 -strength -1.837827e-01
Dipole     -name "LN2_COR"          -length  0.000000e+00 -e0  6.857563e-01
Bpm        -name "LN2_BPM"          -length  0.000000e+00 -e0  6.857563e-01
Quadrupole -name "LN2_QD_DH"        -length  4.000000e-02 -e0  6.857563e-01 -strength -1.837827e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  6.857563e-01
Drift      -name "LN2_DR_VS"        -length  1.000000e-01 -e0  6.857563e-01
Drift      -name "LN2_DR_BL"        -length  5.000000e-02 -e0  6.857563e-01
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  6.857563e-01
Cavity     -name "LN2_XCA0"         -length  9.164755e-01 -gradient  2.520000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   7.060569e-01
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  7.060569e-01
Drift      -name "LN2_DR_BL"        -length  5.000000e-02 -e0  7.060569e-01
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  7.060569e-01
Cavity     -name "LN2_XCA0"         -length  9.164755e-01 -gradient  2.520000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   7.263575e-01
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  7.263575e-01
Drift      -name "LN2_DR_BL"        -length  5.000000e-02 -e0  7.263575e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  7.263575e-01
Quadrupole -name "LN2_QD_FH"        -length  4.000000e-02 -e0  7.263575e-01 -strength  1.946638e-01
Dipole     -name "LN2_COR"          -length  0.000000e+00 -e0  7.263575e-01
Bpm        -name "LN2_BPM"          -length  0.000000e+00 -e0  7.263575e-01
Quadrupole -name "LN2_QD_FH"        -length  4.000000e-02 -e0  7.263575e-01 -strength  1.946638e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  7.263575e-01
Drift      -name "LN2_DR_SV"        -length  1.000000e-01 -e0  7.263575e-01
Drift      -name "LN2_DR_BL"        -length  5.000000e-02 -e0  7.263575e-01
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  7.263575e-01
Cavity     -name "LN2_XCA0"         -length  9.164755e-01 -gradient  2.520000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   7.466581e-01
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  7.466581e-01
Drift      -name "LN2_DR_BL"        -length  5.000000e-02 -e0  7.466581e-01
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  7.466581e-01
Cavity     -name "LN2_XCA0"         -length  9.164755e-01 -gradient  2.520000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   7.669587e-01
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  7.669587e-01
Drift      -name "LN2_DR_BL"        -length  5.000000e-02 -e0  7.669587e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  7.669587e-01
Quadrupole -name "LN2_QD_DH"        -length  4.000000e-02 -e0  7.669587e-01 -strength -2.055449e-01
Dipole     -name "LN2_COR"          -length  0.000000e+00 -e0  7.669587e-01
Bpm        -name "LN2_BPM"          -length  0.000000e+00 -e0  7.669587e-01
Quadrupole -name "LN2_QD_DH"        -length  4.000000e-02 -e0  7.669587e-01 -strength -2.055449e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  7.669587e-01
Drift      -name "LN2_DR_VS"        -length  1.000000e-01 -e0  7.669587e-01
Drift      -name "LN2_DR_BL"        -length  5.000000e-02 -e0  7.669587e-01
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  7.669587e-01
Cavity     -name "LN2_XCA0"         -length  9.164755e-01 -gradient  2.520000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   7.872592e-01
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  7.872592e-01
Drift      -name "LN2_DR_BL"        -length  5.000000e-02 -e0  7.872592e-01
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  7.872592e-01
Cavity     -name "LN2_XCA0"         -length  9.164755e-01 -gradient  2.520000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   8.075598e-01
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  8.075598e-01
Drift      -name "LN2_DR_BL"        -length  5.000000e-02 -e0  8.075598e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  8.075598e-01
Quadrupole -name "LN2_QD_FH"        -length  4.000000e-02 -e0  8.075598e-01 -strength  2.164260e-01
Dipole     -name "LN2_COR"          -length  0.000000e+00 -e0  8.075598e-01
Bpm        -name "LN2_BPM"          -length  0.000000e+00 -e0  8.075598e-01
Quadrupole -name "LN2_QD_FH"        -length  4.000000e-02 -e0  8.075598e-01 -strength  2.164260e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  8.075598e-01
Drift      -name "LN2_WA_OU2"       -length  0.000000e+00 -e0  8.075598e-01
Drift      -name "LN2_DR_DI_V1"     -length  1.161052e+00 -e0  8.075598e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  8.075598e-01
Quadrupole -name "LN2_QD_DI_V01"    -length  4.000000e-02 -e0  8.075598e-01 -strength -7.960008e-02
Dipole     -name "LN2_COR_DI"       -length  0.000000e+00 -e0  8.075598e-01
Bpm        -name "LN2_BPM_DI"       -length  0.000000e+00 -e0  8.075598e-01
Quadrupole -name "LN2_QD_DI_V01"    -length  4.000000e-02 -e0  8.075598e-01 -strength -7.960008e-02
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  8.075598e-01
Drift      -name "LN2_DR_DI_V2"     -length  8.279980e-01 -e0  8.075598e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  8.075598e-01
Quadrupole -name "LN2_QD_DI_V02"    -length  4.000000e-02 -e0  8.075598e-01 -strength  3.269747e-01
Dipole     -name "LN2_COR_DI"       -length  0.000000e+00 -e0  8.075598e-01
Bpm        -name "LN2_BPM_DI"       -length  0.000000e+00 -e0  8.075598e-01
Quadrupole -name "LN2_QD_DI_V02"    -length  4.000000e-02 -e0  8.075598e-01 -strength  3.269747e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  8.075598e-01
Drift      -name "LN2_DR_DI_V3"     -length  2.001539e-01 -e0  8.075598e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  8.075598e-01
Quadrupole -name "LN2_QD_DI_V03"    -length  4.000000e-02 -e0  8.075598e-01 -strength -4.029062e-01
Dipole     -name "LN2_COR_DI"       -length  0.000000e+00 -e0  8.075598e-01
Bpm        -name "LN2_BPM_DI"       -length  0.000000e+00 -e0  8.075598e-01
Quadrupole -name "LN2_QD_DI_V03"    -length  4.000000e-02 -e0  8.075598e-01 -strength -4.029062e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  8.075598e-01
Drift      -name "LN2_DR_DI_V5"     -length  2.000000e-01 -e0  8.075598e-01
Drift      -name "LN2_DR_VS"        -length  1.000000e-01 -e0  8.075598e-01
Drift      -name "LN2_DR_DI"        -length  2.500000e-01 -e0  8.075598e-01
Drift      -name "LN2_DR_VS_DI"     -length  1.000000e-01 -e0  8.075598e-01
Drift      -name "LN2_DR_BL_DI"     -length  5.000000e-02 -e0  8.075598e-01
Drift      -name "LN2_DR_XCA_ED_DI" -length  1.001043e-01 -e0  8.075598e-01
CrabCavity -name "LN2_XCA0_DI"      -length  9.997914e-01 -voltage  0.000000e+00 -phase  9.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   8.075598e-01
Drift      -name "LN2_DR_XCA_ED_DI" -length  1.001043e-01 -e0  8.074833e-01
Drift      -name "LN2_DR_BL_DI"     -length  5.000000e-02 -e0  8.074833e-01
Drift      -name "LN2_DR_DI"        -length  2.500000e-01 -e0  8.074833e-01
Drift      -name "LN2_DR_DI"        -length  2.500000e-01 -e0  8.074833e-01
Drift      -name "LN2_DR_DI_QD_ED"  -length  4.250000e-02 -e0  8.074833e-01
Quadrupole -name "LN2_QD_DI_FH"     -length  4.000000e-02 -e0  8.074833e-01 -strength  2.583947e-01
Dipole     -name "LN2_COR_DI"       -length  0.000000e+00 -e0  8.074833e-01
Bpm        -name "LN2_BPM_DI"       -length  0.000000e+00 -e0  8.074833e-01
Quadrupole -name "LN2_QD_DI_FH"     -length  4.000000e-02 -e0  8.074833e-01 -strength  2.583947e-01
Drift      -name "LN2_DR_DI_QD_ED"  -length  4.250000e-02 -e0  8.074833e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.074833e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.074833e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.074833e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.074833e-01
Drift      -name "LN2_DR_DI_VS"     -length  1.000000e-01 -e0  8.074833e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.074833e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.074833e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.074833e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.074833e-01
Drift      -name "LN2_DR_DI_QD_ED"  -length  4.250000e-02 -e0  8.074833e-01
Quadrupole -name "LN2_QD_DI_DH"     -length  4.000000e-02 -e0  8.074833e-01 -strength -2.583947e-01
Dipole     -name "LN2_COR_DI"       -length  0.000000e+00 -e0  8.074833e-01
Bpm        -name "LN2_BPM_DI"       -length  0.000000e+00 -e0  8.074833e-01
Quadrupole -name "LN2_QD_DI_DH"     -length  4.000000e-02 -e0  8.074833e-01 -strength -2.583947e-01
Drift      -name "LN2_DR_DI_QD_ED"  -length  4.250000e-02 -e0  8.074833e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.074833e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.074833e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.074833e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.074833e-01
Drift      -name "LN2_DR_DI_VS"     -length  1.000000e-01 -e0  8.074833e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.074833e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.074833e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.074833e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.074833e-01
Drift      -name "LN2_DR_DI_QD_ED"  -length  4.250000e-02 -e0  8.074833e-01
Quadrupole -name "LN2_QD_DI_FH"     -length  4.000000e-02 -e0  8.074833e-01 -strength  2.583947e-01
Dipole     -name "LN2_COR_DI"       -length  0.000000e+00 -e0  8.074833e-01
Bpm        -name "LN2_BPM_DI"       -length  0.000000e+00 -e0  8.074833e-01
Quadrupole -name "LN2_QD_DI_FH"     -length  4.000000e-02 -e0  8.074833e-01 -strength  2.583947e-01
Drift      -name "LN2_DR_DI_QD_ED"  -length  4.250000e-02 -e0  8.074833e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.074833e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.074833e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.074833e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.074833e-01
Drift      -name "LN2_DR_DI_VS"     -length  1.000000e-01 -e0  8.074833e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.074833e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.074833e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.074833e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.074833e-01
Drift      -name "LN2_DR_DI_QD_ED"  -length  4.250000e-02 -e0  8.074833e-01
Quadrupole -name "LN2_QD_DI_DH"     -length  4.000000e-02 -e0  8.074833e-01 -strength -2.583947e-01
Dipole     -name "LN2_COR_DI"       -length  0.000000e+00 -e0  8.074833e-01
Bpm        -name "LN2_BPM_DI"       -length  0.000000e+00 -e0  8.074833e-01
Quadrupole -name "LN2_QD_DI_DH"     -length  4.000000e-02 -e0  8.074833e-01 -strength -2.583947e-01
Drift      -name "LN2_DR_DI_QD_ED"  -length  4.250000e-02 -e0  8.074833e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.074833e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.074833e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.074833e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.074833e-01
Drift      -name "LN2_DR_DI_VS"     -length  1.000000e-01 -e0  8.074833e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.074833e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.074833e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.074833e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.074833e-01
Drift      -name "LN2_DR_DI_QD_ED"  -length  4.250000e-02 -e0  8.074833e-01
Quadrupole -name "LN2_QD_DI_FH"     -length  4.000000e-02 -e0  8.074833e-01 -strength  2.583947e-01
Dipole     -name "LN2_COR_DI"       -length  0.000000e+00 -e0  8.074833e-01
Bpm        -name "LN2_BPM_DI"       -length  0.000000e+00 -e0  8.074833e-01
Quadrupole -name "LN2_QD_DI_FH"     -length  4.000000e-02 -e0  8.074833e-01 -strength  2.583947e-01
Drift      -name "LN2_DR_DI_QD_ED"  -length  4.250000e-02 -e0  8.074833e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.074833e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.074833e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.074833e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.074833e-01
Drift      -name "LN2_DR_DI_VS"     -length  1.000000e-01 -e0  8.074833e-01
Sbend      -name "LN2_DP_DI"        -length  1.500000e-01 -e0  8.074833e-01 -angle  0.000000e+00 -E1  0.000000e+00 -E2  0.000000e+00 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  0.000000e+00 -six_dim 1
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.074833e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.074833e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.074833e-01
Drift      -name "LN2_DR_DI_QD_ED"  -length  4.250000e-02 -e0  8.074833e-01
Quadrupole -name "LN2_QD_DI_DH"     -length  4.000000e-02 -e0  8.074833e-01 -strength -2.583947e-01
Dipole     -name "LN2_COR_DI"       -length  0.000000e+00 -e0  8.074833e-01
Bpm        -name "LN2_BPM_DI"       -length  0.000000e+00 -e0  8.074833e-01
Quadrupole -name "LN2_QD_DI_DH"     -length  4.000000e-02 -e0  8.074833e-01 -strength -2.583947e-01
Drift      -name "LN2_DR_DI_QD_ED"  -length  4.250000e-02 -e0  8.074833e-01
Drift      -name "LN2_DR_DI"        -length  2.500000e-01 -e0  8.074833e-01
Drift      -name "LN2_WA_OU_DI2"    -length  0.000000e+00 -e0  8.074833e-01
Drift      -name "LN2_DR_BP_V1"     -length  3.714219e+00 -e0  8.074833e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  8.074833e-01
Quadrupole -name "LN2_QD_BP_V01"    -length  4.000000e-02 -e0  8.074833e-01 -strength -3.721211e-02
Dipole     -name "LN2_COR"          -length  0.000000e+00 -e0  8.074833e-01
Bpm        -name "LN2_BPM"          -length  0.000000e+00 -e0  8.074833e-01
Quadrupole -name "LN2_QD_BP_V01"    -length  4.000000e-02 -e0  8.074833e-01 -strength -3.721211e-02
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  8.074833e-01
Drift      -name "LN2_DR_BP_V2"     -length  3.528767e+00 -e0  8.074833e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  8.074833e-01
Quadrupole -name "LN2_QD_BP_V02"    -length  4.000000e-02 -e0  8.074833e-01 -strength  4.459563e-01
Dipole     -name "LN2_COR"          -length  0.000000e+00 -e0  8.074833e-01
Bpm        -name "LN2_BPM"          -length  0.000000e+00 -e0  8.074833e-01
Quadrupole -name "LN2_QD_BP_V02"    -length  4.000000e-02 -e0  8.074833e-01 -strength  4.459563e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  8.074833e-01
Drift      -name "LN2_DR_BP_V3"     -length  2.000269e-01 -e0  8.074833e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  8.074833e-01
Quadrupole -name "LN2_QD_BP_V03"    -length  4.000000e-02 -e0  8.074833e-01 -strength -4.351447e-01
Dipole     -name "LN2_COR"          -length  0.000000e+00 -e0  8.074833e-01
Bpm        -name "LN2_BPM"          -length  0.000000e+00 -e0  8.074833e-01
Quadrupole -name "LN2_QD_BP_V03"    -length  4.000000e-02 -e0  8.074833e-01 -strength -4.351447e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  8.074833e-01
Drift      -name "LN2_DR_BP_V4"     -length  2.645902e-01 -e0  8.074833e-01
Drift      -name "LN2_DR_BP_0"      -length  2.000000e-01 -e0  8.074833e-01
Drift      -name "LN2_DR_BP_BL"     -length  5.000000e-02 -e0  8.074833e-01
Drift      -name "LN2_DR_SCA_ED"    -length  8.325105e-02 -e0  8.074833e-01
CrabCavity -name "LN2_SCA0"         -length  5.334979e-01 -voltage  0.000000e+00 -phase  9.000000e+01 -frequency  2.997000e+00
SetReferenceEnergy   8.074833e-01
Drift      -name "LN2_DR_SCA_ED"    -length  8.325105e-02 -e0  8.074786e-01
Drift      -name "LN2_DR_BP_BL"     -length  5.000000e-02 -e0  8.074786e-01
Drift      -name "LN2_DR_BP_0"      -length  2.000000e-01 -e0  8.074786e-01
Drift      -name "LN2_DR_BP"        -length  4.000000e-01 -e0  8.074786e-01
Drift      -name "LN2_DR_BP"        -length  4.000000e-01 -e0  8.074786e-01
Drift      -name "LN2_DR_BP_VS"     -length  1.000000e-01 -e0  8.074786e-01
Drift      -name "LN2_DR_BP_DR"     -length  1.000000e-01 -e0  8.074786e-01
Drift      -name "LN2_DP_SEPM"      -length  2.500000e-01 -e0  8.074786e-01
Drift      -name "LN2_DR_BP"        -length  4.000000e-01 -e0  8.074786e-01
Drift      -name "LN2_DR_BP"        -length  4.000000e-01 -e0  8.074786e-01
Drift      -name "LN2_DR_BP"        -length  4.000000e-01 -e0  8.074786e-01
Drift      -name "LN2_DR_BP"        -length  4.000000e-01 -e0  8.074786e-01
Drift      -name "LN2_DR_BP"        -length  4.000000e-01 -e0  8.074786e-01
Drift      -name "LN2_DR_BP_VS"     -length  1.000000e-01 -e0  8.074786e-01
Drift      -name "LN2_DR_BP"        -length  4.000000e-01 -e0  8.074786e-01
Drift      -name "LN2_BP_WA_OU2_"   -length  0.000000e+00 -e0  8.074786e-01
Drift      -name "LN3_DR_V1"        -length  5.000000e-02 -e0  8.074786e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  8.074786e-01
Quadrupole -name "LN3_QD_V01"       -length  4.000000e-02 -e0  8.074786e-01 -strength  2.245255e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  8.074786e-01
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  8.074786e-01
Quadrupole -name "LN3_QD_V01"       -length  4.000000e-02 -e0  8.074786e-01 -strength  2.245255e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  8.074786e-01
Drift      -name "LN3_DR_V2"        -length  5.342817e-01 -e0  8.074786e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  8.074786e-01
Quadrupole -name "LN3_QD_V02"       -length  4.000000e-02 -e0  8.074786e-01 -strength  3.115148e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  8.074786e-01
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  8.074786e-01
Quadrupole -name "LN3_QD_V02"       -length  4.000000e-02 -e0  8.074786e-01 -strength  3.115148e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  8.074786e-01
Drift      -name "LN3_DR_V3"        -length  9.374694e-02 -e0  8.074786e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  8.074786e-01
Quadrupole -name "LN3_QD_V03"       -length  4.000000e-02 -e0  8.074786e-01 -strength -4.881786e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  8.074786e-01
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  8.074786e-01
Quadrupole -name "LN3_QD_V03"       -length  4.000000e-02 -e0  8.074786e-01 -strength -4.881786e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  8.074786e-01
Drift      -name "LN3_DR_V4"        -length  1.580023e+00 -e0  8.074786e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  8.074786e-01
Quadrupole -name "LN3_QD_V04"       -length  4.000000e-02 -e0  8.074786e-01 -strength -1.069829e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  8.074786e-01
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  8.074786e-01
Quadrupole -name "LN3_QD_V04"       -length  4.000000e-02 -e0  8.074786e-01 -strength -1.069829e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  8.074786e-01
Drift      -name "LN3_DR_V5"        -length  1.237385e+00 -e0  8.074786e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  8.074786e-01
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  8.074786e-01 -strength  1.356564e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  8.074786e-01
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  8.074786e-01
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  8.074786e-01 -strength  1.356564e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  8.074786e-01
Drift      -name "LN3_DR_SV"        -length  1.000000e-01 -e0  8.074786e-01
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  8.074786e-01
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  8.074786e-01
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.520000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   8.277791e-01
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  8.277791e-01
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  8.277791e-01
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  8.277791e-01
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.520000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   8.480797e-01
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  8.480797e-01
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  8.480797e-01
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  8.480797e-01
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.520000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   8.683802e-01
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  8.683802e-01
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  8.683802e-01
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  8.683802e-01
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.520000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   8.886807e-01
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  8.886807e-01
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  8.886807e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  8.886807e-01
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  8.886807e-01 -strength -1.492984e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  8.886807e-01
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  8.886807e-01
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  8.886807e-01 -strength -1.492984e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  8.886807e-01
Drift      -name "LN3_DR_VS"        -length  1.000000e-01 -e0  8.886807e-01
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  8.886807e-01
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  8.886807e-01
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.520000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   9.089813e-01
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  9.089813e-01
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  9.089813e-01
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  9.089813e-01
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.520000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   9.292818e-01
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  9.292818e-01
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  9.292818e-01
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  9.292818e-01
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.520000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   9.495824e-01
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  9.495824e-01
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  9.495824e-01
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  9.495824e-01
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.520000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   9.698829e-01
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  9.698829e-01
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  9.698829e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  9.698829e-01
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  9.698829e-01 -strength  1.629403e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  9.698829e-01
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  9.698829e-01
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  9.698829e-01 -strength  1.629403e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  9.698829e-01
Drift      -name "LN3_DR_SV"        -length  1.000000e-01 -e0  9.698829e-01
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  9.698829e-01
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  9.698829e-01
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.520000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   9.901834e-01
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  9.901834e-01
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  9.901834e-01
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  9.901834e-01
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.520000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.010484e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.010484e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.010484e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.010484e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.520000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.030785e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.030785e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.030785e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.030785e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.520000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.051085e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.051085e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.051085e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.051085e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  1.051085e+00 -strength -1.765823e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  1.051085e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  1.051085e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  1.051085e+00 -strength -1.765823e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.051085e+00
Drift      -name "LN3_DR_VS"        -length  1.000000e-01 -e0  1.051085e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.051085e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.051085e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.520000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.071386e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.071386e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.071386e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.071386e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.520000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.091686e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.091686e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.091686e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.091686e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.520000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.111987e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.111987e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.111987e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.111987e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.520000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.132287e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.132287e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.132287e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.132287e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  1.132287e+00 -strength  1.902242e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  1.132287e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  1.132287e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  1.132287e+00 -strength  1.902242e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.132287e+00
Drift      -name "LN3_DR_SV"        -length  1.000000e-01 -e0  1.132287e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.132287e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.132287e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.520000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.152588e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.152588e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.152588e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.152588e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.520000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.172888e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.172888e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.172888e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.172888e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.520000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.193189e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.193189e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.193189e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.193189e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.520000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.213489e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.213489e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.213489e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.213489e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  1.213489e+00 -strength -2.038662e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  1.213489e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  1.213489e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  1.213489e+00 -strength -2.038662e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.213489e+00
Drift      -name "LN3_DR_VS"        -length  1.000000e-01 -e0  1.213489e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.213489e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.213489e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.520000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.233790e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.233790e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.233790e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.233790e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.520000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.254090e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.254090e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.254090e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.254090e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.520000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.274391e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.274391e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.274391e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.274391e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.520000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.294691e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.294691e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.294691e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.294691e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  1.294691e+00 -strength  2.175082e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  1.294691e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  1.294691e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  1.294691e+00 -strength  2.175082e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.294691e+00
Drift      -name "LN3_DR_SV"        -length  1.000000e-01 -e0  1.294691e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.294691e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.294691e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.520000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.314992e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.314992e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.314992e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.314992e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.520000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.335293e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.335293e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.335293e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.335293e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.520000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.355593e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.355593e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.355593e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.355593e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.520000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.375894e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.375894e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.375894e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.375894e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  1.375894e+00 -strength -2.311501e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  1.375894e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  1.375894e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  1.375894e+00 -strength -2.311501e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.375894e+00
Drift      -name "LN3_DR_VS"        -length  1.000000e-01 -e0  1.375894e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.375894e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.375894e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.520000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.396194e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.396194e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.396194e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.396194e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.520000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.416495e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.416495e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.416495e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.416495e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.520000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.436795e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.436795e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.436795e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.436795e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.520000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.457096e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.457096e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.457096e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.457096e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  1.457096e+00 -strength  2.447921e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  1.457096e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  1.457096e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  1.457096e+00 -strength  2.447921e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.457096e+00
Drift      -name "LN3_DR_SV"        -length  1.000000e-01 -e0  1.457096e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.457096e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.457096e+00
Cavity     -name "LN3_XCA0_2"       -length  9.164755e-01 -gradient  1.260000e-02 -phase  1.520000e+02 -frequency  1.199420e+01
SetReferenceEnergy   1.446809e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.446809e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.446809e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.446809e+00
Cavity     -name "LN3_XCA0_2"       -length  9.164755e-01 -gradient  1.260000e-02 -phase  1.520000e+02 -frequency  1.199420e+01
SetReferenceEnergy   1.436521e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.436521e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.436521e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.436521e+00
Cavity     -name "LN3_XCA0_2"       -length  9.164755e-01 -gradient  1.260000e-02 -phase  1.520000e+02 -frequency  1.199420e+01
SetReferenceEnergy   1.426234e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.426234e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.426234e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.426234e+00
Cavity     -name "LN3_XCA0_2"       -length  9.164755e-01 -gradient  1.260000e-02 -phase  1.520000e+02 -frequency  1.199420e+01
SetReferenceEnergy   1.415947e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.415947e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.415947e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.415947e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  1.415947e+00 -strength -2.378791e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  1.415947e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  1.415947e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  1.415947e+00 -strength -2.378791e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.415947e+00
Drift      -name "LN3_DR_VS"        -length  1.000000e-01 -e0  1.415947e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.415947e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.415947e+00
Cavity     -name "LN3_XCA0_2"       -length  9.164755e-01 -gradient  1.260000e-02 -phase  1.520000e+02 -frequency  1.199420e+01
SetReferenceEnergy   1.405660e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.405660e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.405660e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.405660e+00
Cavity     -name "LN3_XCA0_2"       -length  9.164755e-01 -gradient  1.260000e-02 -phase  1.520000e+02 -frequency  1.199420e+01
SetReferenceEnergy   1.395373e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.395373e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.395373e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.395373e+00
Cavity     -name "LN3_XCA0_2"       -length  9.164755e-01 -gradient  1.260000e-02 -phase  1.520000e+02 -frequency  1.199420e+01
SetReferenceEnergy   1.385085e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.385085e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.385085e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.385085e+00
Cavity     -name "LN3_XCA0_2"       -length  9.164755e-01 -gradient  1.260000e-02 -phase  1.520000e+02 -frequency  1.199420e+01
SetReferenceEnergy   1.374798e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.374798e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.374798e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.374798e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  1.374798e+00 -strength  2.309661e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  1.374798e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  1.374798e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  1.374798e+00 -strength  2.309661e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.374798e+00
Drift      -name "LN3_DR_SV"        -length  1.000000e-01 -e0  1.374798e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.374798e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.374798e+00
Cavity     -name "LN3_XCA0_2"       -length  9.164755e-01 -gradient  1.260000e-02 -phase  1.520000e+02 -frequency  1.199420e+01
SetReferenceEnergy   1.364511e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.364511e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.364511e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.364511e+00
Cavity     -name "LN3_XCA0_2"       -length  9.164755e-01 -gradient  1.260000e-02 -phase  1.520000e+02 -frequency  1.199420e+01
SetReferenceEnergy   1.354224e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.354224e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.354224e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.354224e+00
Cavity     -name "LN3_XCA0_2"       -length  9.164755e-01 -gradient  1.260000e-02 -phase  1.520000e+02 -frequency  1.199420e+01
SetReferenceEnergy   1.343937e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.343937e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.343937e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.343937e+00
Cavity     -name "LN3_XCA0_2"       -length  9.164755e-01 -gradient  1.260000e-02 -phase  1.520000e+02 -frequency  1.199420e+01
SetReferenceEnergy   1.333649e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.333649e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.333649e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.333649e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  1.333649e+00 -strength -2.240531e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  1.333649e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  1.333649e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  1.333649e+00 -strength -2.240531e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.333649e+00
Drift      -name "LN3_DR_VS"        -length  1.000000e-01 -e0  1.333649e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.333649e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.333649e+00
Cavity     -name "LN3_XCA0_2"       -length  9.164755e-01 -gradient  1.260000e-02 -phase  1.520000e+02 -frequency  1.199420e+01
SetReferenceEnergy   1.323362e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.323362e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.323362e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.323362e+00
Cavity     -name "LN3_XCA0_2"       -length  9.164755e-01 -gradient  1.260000e-02 -phase  1.520000e+02 -frequency  1.199420e+01
SetReferenceEnergy   1.313075e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.313075e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.313075e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.313075e+00
Cavity     -name "LN3_XCA0_2"       -length  9.164755e-01 -gradient  1.260000e-02 -phase  1.520000e+02 -frequency  1.199420e+01
SetReferenceEnergy   1.302788e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.302788e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.302788e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.302788e+00
Cavity     -name "LN3_XCA0_2"       -length  9.164755e-01 -gradient  1.260000e-02 -phase  1.520000e+02 -frequency  1.199420e+01
SetReferenceEnergy   1.292501e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.292501e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.292501e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.292501e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  1.292501e+00 -strength  2.171401e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  1.292501e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  1.292501e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  1.292501e+00 -strength  2.171401e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.292501e+00
Drift      -name "LN3_DR_SV"        -length  1.000000e-01 -e0  1.292501e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.292501e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.292501e+00
Cavity     -name "LN3_XCA0_2"       -length  9.164755e-01 -gradient  1.260000e-02 -phase  1.520000e+02 -frequency  1.199420e+01
SetReferenceEnergy   1.282214e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.282214e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.282214e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.282214e+00
Cavity     -name "LN3_XCA0_2"       -length  9.164755e-01 -gradient  1.260000e-02 -phase  1.520000e+02 -frequency  1.199420e+01
SetReferenceEnergy   1.271926e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.271926e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.271926e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.271926e+00
Cavity     -name "LN3_XCA0_2"       -length  9.164755e-01 -gradient  1.260000e-02 -phase  1.520000e+02 -frequency  1.199420e+01
SetReferenceEnergy   1.261639e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.261639e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.261639e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.261639e+00
Cavity     -name "LN3_XCA0_2"       -length  9.164755e-01 -gradient  1.260000e-02 -phase  1.520000e+02 -frequency  1.199420e+01
SetReferenceEnergy   1.251352e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.251352e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.251352e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.251352e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  1.251352e+00 -strength -2.102271e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  1.251352e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  1.251352e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  1.251352e+00 -strength -2.102271e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.251352e+00
Drift      -name "LN3_DR_VS"        -length  1.000000e-01 -e0  1.251352e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.251352e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.251352e+00
Cavity     -name "LN3_XCA0_2"       -length  9.164755e-01 -gradient  1.260000e-02 -phase  1.520000e+02 -frequency  1.199420e+01
SetReferenceEnergy   1.241065e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.241065e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.241065e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.241065e+00
Cavity     -name "LN3_XCA0_2"       -length  9.164755e-01 -gradient  1.260000e-02 -phase  1.520000e+02 -frequency  1.199420e+01
SetReferenceEnergy   1.230778e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.230778e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.230778e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.230778e+00
Cavity     -name "LN3_XCA0_2"       -length  9.164755e-01 -gradient  1.260000e-02 -phase  1.520000e+02 -frequency  1.199420e+01
SetReferenceEnergy   1.220490e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.220490e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.220490e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.220490e+00
Cavity     -name "LN3_XCA0_2"       -length  9.164755e-01 -gradient  1.260000e-02 -phase  1.520000e+02 -frequency  1.199420e+01
SetReferenceEnergy   1.210203e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.210203e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.210203e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.210203e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  1.210203e+00 -strength  2.033141e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  1.210203e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  1.210203e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  1.210203e+00 -strength  2.033141e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.210203e+00
Drift      -name "LN3_DR_SV"        -length  1.000000e-01 -e0  1.210203e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.210203e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.210203e+00
Cavity     -name "LN3_XCA0_2"       -length  9.164755e-01 -gradient  1.260000e-02 -phase  1.520000e+02 -frequency  1.199420e+01
SetReferenceEnergy   1.199916e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.199916e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.199916e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.199916e+00
Cavity     -name "LN3_XCA0_2"       -length  9.164755e-01 -gradient  1.260000e-02 -phase  1.520000e+02 -frequency  1.199420e+01
SetReferenceEnergy   1.189629e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.189629e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.189629e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.189629e+00
Cavity     -name "LN3_XCA0_2"       -length  9.164755e-01 -gradient  1.260000e-02 -phase  1.520000e+02 -frequency  1.199420e+01
SetReferenceEnergy   1.179342e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.179342e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.179342e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.179342e+00
Cavity     -name "LN3_XCA0_2"       -length  9.164755e-01 -gradient  1.260000e-02 -phase  1.520000e+02 -frequency  1.199420e+01
SetReferenceEnergy   1.169054e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.169054e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.169054e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.169054e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  1.169054e+00 -strength -1.964011e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  1.169054e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  1.169054e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  1.169054e+00 -strength -1.964011e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.169054e+00
Drift      -name "LN3_DR_VS"        -length  1.000000e-01 -e0  1.169054e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.169054e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.169054e+00
Cavity     -name "LN3_XCA0_2"       -length  9.164755e-01 -gradient  1.260000e-02 -phase  1.520000e+02 -frequency  1.199420e+01
SetReferenceEnergy   1.158767e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.158767e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.158767e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.158767e+00
Cavity     -name "LN3_XCA0_2"       -length  9.164755e-01 -gradient  1.260000e-02 -phase  1.520000e+02 -frequency  1.199420e+01
SetReferenceEnergy   1.148480e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.148480e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.148480e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.148480e+00
Cavity     -name "LN3_XCA0_2"       -length  9.164755e-01 -gradient  1.260000e-02 -phase  1.520000e+02 -frequency  1.199420e+01
SetReferenceEnergy   1.138193e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.138193e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.138193e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.138193e+00
Cavity     -name "LN3_XCA0_2"       -length  9.164755e-01 -gradient  1.260000e-02 -phase  1.520000e+02 -frequency  1.199420e+01
SetReferenceEnergy   1.127906e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.127906e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.127906e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.127906e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  1.127906e+00 -strength  1.894882e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  1.127906e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  1.127906e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  1.127906e+00 -strength  1.894882e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.127906e+00
Drift      -name "LN3_DR_SV"        -length  1.000000e-01 -e0  1.127906e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.127906e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.127906e+00
Cavity     -name "LN3_XCA0_2"       -length  9.164755e-01 -gradient  1.260000e-02 -phase  1.520000e+02 -frequency  1.199420e+01
SetReferenceEnergy   1.117618e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.117618e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.117618e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.117618e+00
Cavity     -name "LN3_XCA0_2"       -length  9.164755e-01 -gradient  1.260000e-02 -phase  1.520000e+02 -frequency  1.199420e+01
SetReferenceEnergy   1.107331e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.107331e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.107331e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.107331e+00
Cavity     -name "LN3_XCA0_2"       -length  9.164755e-01 -gradient  1.260000e-02 -phase  1.520000e+02 -frequency  1.199420e+01
SetReferenceEnergy   1.097044e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.097044e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.097044e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.097044e+00
Cavity     -name "LN3_XCA0_2"       -length  9.164755e-01 -gradient  1.260000e-02 -phase  1.520000e+02 -frequency  1.199420e+01
SetReferenceEnergy   1.086757e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.086757e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.086757e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.086757e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  1.086757e+00 -strength -1.825752e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  1.086757e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  1.086757e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  1.086757e+00 -strength -1.825752e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.086757e+00
Drift      -name "LN3_DR_DR"        -length  1.000000e-01 -e0  1.086757e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.086757e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.086757e+00
Drift      -name "LN3_DR_XCA_"      -length  9.164755e-01 -e0  1.086757e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.086757e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.086757e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.086757e+00
Drift      -name "LN3_DR_XCA_"      -length  9.164755e-01 -e0  1.086757e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.086757e+00
Drift      -name "LN3_DR_CT"        -length  1.000000e-01 -e0  1.086757e+00
Drift      -name "LN3_WA_OU2"       -length  0.000000e+00 -e0  1.086757e+00
Drift      -name "LN3_DR_BP_V1"     -length  1.988701e+00 -e0  1.086757e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.086757e+00
Quadrupole -name "LN3_QD_BP_V01"    -length  4.000000e-02 -e0  1.086757e+00 -strength -3.496689e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  1.086757e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  1.086757e+00
Quadrupole -name "LN3_QD_BP_V01"    -length  4.000000e-02 -e0  1.086757e+00 -strength -3.496689e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.086757e+00
Drift      -name "LN3_DR_BP_V2"     -length  2.427668e+00 -e0  1.086757e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.086757e+00
Quadrupole -name "LN3_QD_BP_V02"    -length  4.000000e-02 -e0  1.086757e+00 -strength  3.753808e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  1.086757e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  1.086757e+00
Quadrupole -name "LN3_QD_BP_V02"    -length  4.000000e-02 -e0  1.086757e+00 -strength  3.753808e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.086757e+00
Drift      -name "LN3_DR_BP_V3"     -length  1.213336e+00 -e0  1.086757e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.086757e+00
Quadrupole -name "LN3_QD_BP_V03"    -length  4.000000e-02 -e0  1.086757e+00 -strength -3.566999e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  1.086757e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  1.086757e+00
Quadrupole -name "LN3_QD_BP_V03"    -length  4.000000e-02 -e0  1.086757e+00 -strength -3.566999e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.086757e+00
Drift      -name "LN3_DR_BP_V4"     -length  2.000000e-01 -e0  1.086757e+00
Drift      -name "LN3_DR_BP_0"      -length  2.000000e-01 -e0  1.086757e+00
Drift      -name "LN3_DR_BP_BL"     -length  5.000000e-02 -e0  1.086757e+00
Drift      -name "LN3_DR_SCA_ED"    -length  8.325105e-02 -e0  1.086757e+00
CrabCavity -name "LN3_SCA0"         -length  5.334979e-01 -voltage  0.000000e+00 -phase  9.000000e+01 -frequency  2.997000e+00
SetReferenceEnergy   1.086757e+00
Drift      -name "LN3_DR_SCA_ED"    -length  8.325105e-02 -e0  1.086752e+00
Drift      -name "LN3_DR_BP_BL"     -length  5.000000e-02 -e0  1.086752e+00
Drift      -name "LN3_DR_BP_0"      -length  2.000000e-01 -e0  1.086752e+00
Drift      -name "LN3_DR_BP"        -length  4.000000e-01 -e0  1.086752e+00
Drift      -name "LN3_DR_BP_VS"     -length  1.000000e-01 -e0  1.086752e+00
Drift      -name "LN3_DR_BP"        -length  4.000000e-01 -e0  1.086752e+00
Drift      -name "LN3_DP_SEPM"      -length  4.500000e-01 -e0  1.086752e+00
Drift      -name "LN3_DR_BP"        -length  4.000000e-01 -e0  1.086752e+00
Drift      -name "LN3_DR_BP"        -length  4.000000e-01 -e0  1.086752e+00
Drift      -name "LN3_DR_BP"        -length  4.000000e-01 -e0  1.086752e+00
Drift      -name "LN3_DR_BP"        -length  4.000000e-01 -e0  1.086752e+00
Drift      -name "LN3_DR_BP"        -length  4.000000e-01 -e0  1.086752e+00
Drift      -name "LN3_DR_BP_VS"     -length  1.000000e-01 -e0  1.086752e+00
Drift      -name "LN3_DR_BP"        -length  4.000000e-01 -e0  1.086752e+00
Drift      -name "LN3_BP_WA_OU2_"   -length  0.000000e+00 -e0  1.086752e+00
Drift      -name "TMC_DR_V1"        -length  2.004117e-01 -e0  1.086752e+00
Drift      -name "TMC_DR_QD_ED"     -length  4.250000e-02 -e0  1.086752e+00
Quadrupole -name "TMC_QD_V01"       -length  4.000000e-02 -e0  1.086752e+00 -strength  2.226113e-01
Dipole     -name "TMC_COR"          -length  0.000000e+00 -e0  1.086752e+00
Bpm        -name "TMC_BPM"          -length  0.000000e+00 -e0  1.086752e+00
Quadrupole -name "TMC_QD_V01"       -length  4.000000e-02 -e0  1.086752e+00 -strength  2.226113e-01
Drift      -name "TMC_DR_QD_ED"     -length  4.250000e-02 -e0  1.086752e+00
Drift      -name "TMC_DR_V2"        -length  2.866492e+00 -e0  1.086752e+00
Drift      -name "TMC_DR_QD_ED"     -length  4.250000e-02 -e0  1.086752e+00
Quadrupole -name "TMC_QD_V02"       -length  4.000000e-02 -e0  1.086752e+00 -strength -4.281956e-01
Dipole     -name "TMC_COR"          -length  0.000000e+00 -e0  1.086752e+00
Bpm        -name "TMC_BPM"          -length  0.000000e+00 -e0  1.086752e+00
Quadrupole -name "TMC_QD_V02"       -length  4.000000e-02 -e0  1.086752e+00 -strength -4.281956e-01
Drift      -name "TMC_DR_QD_ED"     -length  4.250000e-02 -e0  1.086752e+00
Drift      -name "TMC_DR_V3"        -length  3.885389e-01 -e0  1.086752e+00
Drift      -name "TMC_DR_QD_ED"     -length  4.250000e-02 -e0  1.086752e+00
Quadrupole -name "TMC_QD_V03"       -length  4.000000e-02 -e0  1.086752e+00 -strength  3.411675e-01
Dipole     -name "TMC_COR"          -length  0.000000e+00 -e0  1.086752e+00
Bpm        -name "TMC_BPM"          -length  0.000000e+00 -e0  1.086752e+00
Quadrupole -name "TMC_QD_V03"       -length  4.000000e-02 -e0  1.086752e+00 -strength  3.411675e-01
Drift      -name "TMC_DR_QD_ED"     -length  4.250000e-02 -e0  1.086752e+00
Drift      -name "TMC_DR_V4"        -length  2.000000e-01 -e0  1.086752e+00
Drift      -name "TMC_DR_20"        -length  5.000000e-01 -e0  1.086752e+00
Sbend      -name "TMC_DP_DIP1"      -length  5.000001e-01 -e0  1.086752e+00 -angle -8.726600e-04 -E1  0.000000e+00 -E2 -8.726600e-04 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  0.000000e+00  -six_dim 1 -csr 1 
Drift      -name "TMC_DR_SIDE_C"    -length  5.000000e-01 -e0  1.086752e+00
Drift      -name "TMC_DR_SIDE"      -length  3.000001e+00 -e0  1.086752e+00
Sbend      -name "TMC_DP_DIP2"      -length  5.000001e-01 -e0  1.086752e+00 -angle  8.726600e-04 -E1  8.726600e-04 -E2  0.000000e+00 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  0.000000e+00  -six_dim 1 -csr 1 
Drift      -name "TMC_DR_CENT_C"    -length  2.500000e-01 -e0  1.086752e+00
Bpm        -name "TMC_BPM"          -length  0.000000e+00 -e0  1.086752e+00
Drift      -name "TMC_DR_CENT"      -length  5.000000e-01 -e0  1.086752e+00
Sbend      -name "TMC_DP_DIP3"      -length  5.000001e-01 -e0  1.086752e+00 -angle  8.726600e-04 -E1  0.000000e+00 -E2  8.726600e-04 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  0.000000e+00  -six_dim 1 -csr 1 
Drift      -name "TMC_DR_SIDE_C"    -length  5.000000e-01 -e0  1.086752e+00
Drift      -name "TMC_DR_SIDE"      -length  3.000001e+00 -e0  1.086752e+00
Sbend      -name "TMC_DP_DIP4"      -length  5.000001e-01 -e0  1.086752e+00 -angle -8.726600e-04 -E1 -8.726600e-04 -E2  0.000000e+00 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  0.000000e+00  -six_dim 1 -csr 1 
Drift      -name "TMC_DR_20_C"      -length  5.000000e-01 -e0  1.086752e+00
Drift      -name "TMC_WA_OU2"       -length  0.000000e+00 -e0  1.086752e+00
Drift      -name "HXR_DR_V1"        -length  2.000000e-01 -e0  1.086752e+00
Drift      -name "HXR_DR_QD_ED"     -length  5.200000e-02 -e0  1.086752e+00
Quadrupole -name "HXR_QD_V01"       -length  1.300000e-02 -e0  1.086752e+00 -strength  3.658111e-01
Dipole     -name "HXR_COR"          -length  0.000000e+00 -e0  1.086752e+00
Bpm        -name "HXR_BPM"          -length  0.000000e+00 -e0  1.086752e+00
Quadrupole -name "HXR_QD_V01"       -length  1.300000e-02 -e0  1.086752e+00 -strength  3.658111e-01
Drift      -name "HXR_DR_QD_ED"     -length  5.200000e-02 -e0  1.086752e+00
Drift      -name "HXR_DR_V2"        -length  5.892182e-01 -e0  1.086752e+00
Drift      -name "HXR_DR_QD_ED"     -length  5.200000e-02 -e0  1.086752e+00
Quadrupole -name "HXR_QD_V02"       -length  1.300000e-02 -e0  1.086752e+00 -strength -4.238333e-01
Dipole     -name "HXR_COR"          -length  0.000000e+00 -e0  1.086752e+00
Bpm        -name "HXR_BPM"          -length  0.000000e+00 -e0  1.086752e+00
Quadrupole -name "HXR_QD_V02"       -length  1.300000e-02 -e0  1.086752e+00 -strength -4.238333e-01
Drift      -name "HXR_DR_QD_ED"     -length  5.200000e-02 -e0  1.086752e+00
Drift      -name "HXR_DR_V3"        -length  1.144548e+00 -e0  1.086752e+00
Drift      -name "HXR_DR_QD_ED"     -length  5.200000e-02 -e0  1.086752e+00
Quadrupole -name "HXR_QD_V03"       -length  1.300000e-02 -e0  1.086752e+00 -strength  1.563936e-01
Dipole     -name "HXR_COR"          -length  0.000000e+00 -e0  1.086752e+00
Bpm        -name "HXR_BPM"          -length  0.000000e+00 -e0  1.086752e+00
Quadrupole -name "HXR_QD_V03"       -length  1.300000e-02 -e0  1.086752e+00 -strength  1.563936e-01
Drift      -name "HXR_DR_QD_ED"     -length  5.200000e-02 -e0  1.086752e+00
Drift      -name "HXR_DR_V4"        -length  2.759790e+00 -e0  1.086752e+00
Drift      -name "HXR_DR_CT"        -length  1.000000e-01 -e0  1.086752e+00
Drift      -name "HXR_WA_M_OU2"     -length  0.000000e+00 -e0  1.086752e+00
Drift      -name "HXR_DR_QD_ED"     -length  5.200000e-02 -e0  1.086752e+00
Quadrupole -name "HXR_QD_FH"        -length  1.300000e-02 -e0  1.086752e+00 -strength  2.119167e-01
Dipole     -name "HXR_COR"          -length  0.000000e+00 -e0  1.086752e+00
Bpm        -name "HXR_BPM"          -length  0.000000e+00 -e0  1.086752e+00
Quadrupole -name "HXR_QD_FH"        -length  1.300000e-02 -e0  1.086752e+00 -strength  2.119167e-01
Drift      -name "HXR_DR_QD_ED"     -length  5.200000e-02 -e0  1.086752e+00
Drift      -name "HXR_DR_WIG_ED"    -length  1.950000e-01 -e0  1.086752e+00
Drift      -name "HXR_WIG_0"        -length  1.768000e+00 -e0  1.086752e+00
Drift      -name "HXR_DR_WIG_ED"    -length  1.950000e-01 -e0  1.086752e+00
Drift      -name "HXR_DR_QD_ED"     -length  5.200000e-02 -e0  1.086752e+00
Quadrupole -name "HXR_QD_DH"        -length  1.300000e-02 -e0  1.086752e+00 -strength -2.119167e-01
Dipole     -name "HXR_COR"          -length  0.000000e+00 -e0  1.086752e+00
Bpm        -name "HXR_BPM"          -length  0.000000e+00 -e0  1.086752e+00
Quadrupole -name "HXR_QD_DH"        -length  1.300000e-02 -e0  1.086752e+00 -strength -2.119167e-01
Drift      -name "HXR_DR_QD_ED"     -length  5.200000e-02 -e0  1.086752e+00
Drift      -name "HXR_DR_WIG_ED"    -length  1.950000e-01 -e0  1.086752e+00
Drift      -name "HXR_WIG_0"        -length  1.768000e+00 -e0  1.086752e+00
Drift      -name "HXR_DR_WIG_ED"    -length  1.950000e-01 -e0  1.086752e+00
Drift      -name "HXR_DR_QD_ED"     -length  5.200000e-02 -e0  1.086752e+00
Drift      -name "HXR_WA_OU2"       -length  0.000000e+00 -e0  1.086752e+00
