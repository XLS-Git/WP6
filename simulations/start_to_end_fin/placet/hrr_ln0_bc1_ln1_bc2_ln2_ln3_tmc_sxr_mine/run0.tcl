

set beamlinename [exec   basename [pwd] ]

set outfolder run0_out 

if {[file exist $outfolder]} {
	exec rm -rf $outfolder
	exec mkdir -p $outfolder
} else {
	exec mkdir -p $outfolder
}

cd   $outfolder

source ../../common_files/load_scripts.tcl

set script_dir "../files"


set beam0_indist [lindex [file split [glob ../files/*out.dat]] end end]
set latfile [lindex [file split [glob ../files/*lattice.tcl]] end end]
puts $beam0_indist
puts $latfile





set charge [expr 75e-12/1.6e-19]

array set BeamDefine  [bunch_parameters $script_dir/$beam0_indist]
array set BeamDefine "name beam0 filename $beam0_indist n_slice 101 charge $charge"
set BeamDefine(n_macro) [expr int($BeamDefine(npart)/$BeamDefine(n_slice))]
lassign "$BeamDefine(beta_x) $BeamDefine(beta_y) $BeamDefine(alpha_x) $BeamDefine(alpha_y)" btx bty alx aly



set e0 $BeamDefine(energy)

puts [array get BeamDefine]

set b0_in beam0.in
set b0_ou beam0.ou




Girder
source $script_dir/$latfile
BeamlineSet -name $beamlinename

make_particle_beam_read BeamDefine $script_dir/$BeamDefine(filename)

Octave {
    global beaminxdx = 1;
    global Beams = {};
}

proc octave_save {} {
    Octave {
      Beams{beaminxdx++} = placet_get_beam();
    }
} 


Octave {
# apply wakes for all structures
	ALLCAVITs = placet_get_number_list("$beamlinename", "cavity");
	placet_element_set_attribute("$beamlinename", ALLCAVITs, "six_dim", true);
    
    LN0XDCs = placet_get_name_number_list("$beamlinename", "LN0_XCA0_DI");
    placet_element_set_attribute("$beamlinename", LN0XDCs, "short_range_wake",  "Xdband_SR_W");
    placet_element_set_attribute("$beamlinename", LN0XDCs, "lambda", $xdband_str(lambda));
    
    LN0CCs = placet_get_name_number_list("$beamlinename", "LN0_CCA0");
    placet_element_set_attribute("$beamlinename", LN0CCs, "short_range_wake",  "Cband_SR_W");
    placet_element_set_attribute("$beamlinename", LN0CCs, "lambda", $cband_str(lambda));
     
    LN0KCs = placet_get_name_number_list("$beamlinename", "LN0_KCA0");
    placet_element_set_attribute("$beamlinename", LN0KCs, "short_range_wake",  "Kband_SR_W");
    placet_element_set_attribute("$beamlinename", LN0KCs, "lambda", $kband_str(lambda));
   
    BC1XCs = placet_get_name_number_list("$beamlinename", "BC1_XCA0");
    placet_element_set_attribute("$beamlinename", BC1XCs, "short_range_wake",  "Xdband_SR_W");
    placet_element_set_attribute("$beamlinename", BC1XCs, "lambda", $xdband_str(lambda));
    
    LN1XCs = placet_get_name_number_list("$beamlinename", "LN1_XCA0");
    placet_element_set_attribute("$beamlinename", LN1XCs, "short_range_wake",  "Xband_SR_W");
    placet_element_set_attribute("$beamlinename", LN1XCs, "lambda", $xband_str(lambda));
    
    LN2XCs = placet_get_name_number_list("$beamlinename", "LN2_XCA0");
    placet_element_set_attribute("$beamlinename", LN2XCs, "short_range_wake",  "Xband_SR_W");
    placet_element_set_attribute("$beamlinename", LN2XCs, "lambda", $xband_str(lambda));
    
    LN2XDCs = placet_get_name_number_list("$beamlinename", "LN2_XCA0_DI");
    placet_element_set_attribute("$beamlinename", LN2XDCs, "short_range_wake",  "Xdband_SR_W");
    placet_element_set_attribute("$beamlinename", LN2XDCs, "lambda", $xdband_str(lambda));    
    
    LN2SCs = placet_get_name_number_list("$beamlinename", "LN2_SCA0");
    placet_element_set_attribute("$beamlinename", LN2SCs, "short_range_wake",  "Sdband_SR_W");
    placet_element_set_attribute("$beamlinename", LN2SCs, "lambda", $sdband_str(lambda));
    
    LN3XCs = placet_get_name_number_list("$beamlinename", "LN3_XCA0*");
    placet_element_set_attribute("$beamlinename", LN3XCs, "short_range_wake",  "Xband_SR_W");
    placet_element_set_attribute("$beamlinename", LN3XCs, "lambda", $xband_str(lambda));
    
    LN3SCs = placet_get_name_number_list("$beamlinename", "LN3_SCA0");
    placet_element_set_attribute("$beamlinename", LN3SCs, "short_range_wake",  "Sdband_SR_W");
    placet_element_set_attribute("$beamlinename", LN3SCs, "lambda", $sdband_str(lambda));

    LN3XCs2 = placet_get_name_number_list("$beamlinename", "LN3_XCA0_2");
    placet_element_set_attribute("$beamlinename", LN3XCs2, "phase",  "28+180");

    
#  6d tracking in bunch compression
   SIs = placet_get_number_list("$beamlinename", "sbend");
   placet_element_set_attribute("$beamlinename", SIs, "six_dim", true);
   
   DRFTSs = placet_get_number_list("$beamlinename", "drift");;
   placet_element_set_attribute("$beamlinename", DRFTSs, "six_dim", true);
    

   placet_element_set_attribute("$beamlinename", SIs, "csr", true);
   placet_element_set_attribute("$beamlinename", SIs, "csr_charge", 75e-12);
   placet_element_set_attribute("$beamlinename", SIs, "csr_nbins", 50);
   placet_element_set_attribute("$beamlinename", SIs, "csr_nsectors", 10);
   placet_element_set_attribute("$beamlinename", SIs, "csr_filterorder", 1);
   placet_element_set_attribute("$beamlinename", SIs, "csr_nhalffilter", 2);

   QDs = placet_get_number_list("$beamlinename", "quadrupole");
   placet_element_set_attribute("$beamlinename", QDs, "six_dim", true);

   
   watches = placet_get_name_number_list("$beamlinename", "*_WA_*")

   placet_element_set_attribute("$beamlinename", watches, "tclcall_exit", "octave_save");
}


Octave {

    
   %%  twiss function along beamline
	[s, beta_x, beta_y, alpha_x, alpha_y, mu_x, mu_y] = placet_evolve_beta_function("$beamlinename", $btx, $alx, $bty, $aly);
	beta_arr= [s  beta_x  beta_y  alpha_x alpha_y mu_x mu_y];
	save -text -ascii $beamlinename.twi beta_arr;
    
}

# BeamEnergyPlot -beam $BeamDefine(name) -file beamlinename.ener

TestNoCorrection -beam $BeamDefine(name) -survey None -emitt_file $beamlinename.emt


Octave {
    [~,nb]=size(Beams);
    fnames={ 'ln0_lnz_ina.dat.gz', 'ln0_oua.dat.gz', 'bc1_oua.dat.gz', 'bc1_dia_oua.dat.gz', 'ln1_oua.dat.gz', 'bc2_oua.dat.gz', 'ln2_oua.dat.gz', 'ln2_di_oua.dat.gz', 'ln3_ina.dat.gz', 'ln3_oua.dat.gz', 'tmc_ina.dat.gz', 'tmc_oua.dat.gz', 'hxr_m_oua.dat.gz', 'hxr_oua.dat.gz'};

    pltnams={};
    
    for i=1:nb
        
        B=Beams{i};
        fnm=fnames{i};
        pltnams{i}=fnm;
        emean=mean(B(:,1));
        smean=mean(B(:,4));
        B(:,4)=B(:,4)-smean;
        sigz=std(B(:,4));
        BE=load(strcat ("../eleouts/",fnm));
        emeane=mean(BE(:,1));
        smeane=mean(BE(:,4));
        BE(:,4)=BE(:,4)-smeane;
        sigze=std(BE(:,4));        
        printf("at position %s Emean=%f GeV %f GeV, Sig_z=%f um %f um, S_off=%f um %f um\n",fnm,emean,emeane,sigz,sigze,smean,smeane) 
        save("-text", "-ascii","-z", fnm,"B");
        
        
             
    endfor
    
    pltnams = strcat(pltnams,{' '});
    pltnams=cell2mat(pltnams);
    
    Tcl_SetVar("pltnams", pltnams);
    
}

set fl [lindex $pltnams end]
puts $fl

if {[file exist ../phase_plots]} {
        exec rm -rf ../phase_plots
        exec mkdir -p ../phase_plots
    } else {
        exec mkdir -p ../phase_plots
    }

set term "set terminal png font Arial 14 size 800,600 enhanced"

foreach fl $pltnams {
   set plot_phs  "'< zcat $fl' u 4:1 w p ti 'placet', '< zcat  ../eleouts/$fl ' u 4:1 w p ti 'elegant' "
   set flname  [string replace [file rootname [file tail $fl]] end end ""]
   set plotname $flname.png
   puts $flname
   exec echo "$term \nset output '$plotname' \nset key left \nset title '$flname' font 'Arial, 18'  noenhanced \n \
              set xlabel 'z (um)'  font 'Arial, 18' \nset ylabel 'E (GeV)'  font 'Arial, 18' \n  \
               pl $plot_phs \n unset output " | gnuplot -persist
               
   exec mv $plotname ../phase_plots/
}





exit
