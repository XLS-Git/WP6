Girder
SetReferenceEnergy   6.560098e-02
Drift      -name "LN0_DR_V5"        -length  9.998685e-01 -e0  6.560098e-02
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  6.560098e-02
Quadrupole -name "LN0_QD_V04"       -length  4.000000e-02 -e0  6.560098e-02 -strength -5.668903e-03
Dipole     -name "LN0_COR"          -length  0.000000e+00 -e0  6.560098e-02
Bpm        -name "LN0_BPM"          -length  0.000000e+00 -e0  6.560098e-02
Quadrupole -name "LN0_QD_V04"       -length  4.000000e-02 -e0  6.560098e-02 -strength -5.668903e-03
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  6.560098e-02
Drift      -name "LN0_DR_V6"        -length  1.500000e+00 -e0  6.560098e-02
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  6.560098e-02
Quadrupole -name "LN0_QD_V05"       -length  4.000000e-02 -e0  6.560098e-02 -strength  2.869671e-02
Dipole     -name "LN0_COR"          -length  0.000000e+00 -e0  6.560098e-02
Bpm        -name "LN0_BPM"          -length  0.000000e+00 -e0  6.560098e-02
Quadrupole -name "LN0_QD_V05"       -length  4.000000e-02 -e0  6.560098e-02 -strength  2.869671e-02
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  6.560098e-02
Drift      -name "LN0_DR_V7"        -length  8.061127e-01 -e0  6.560098e-02
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  6.560098e-02
Quadrupole -name "LN0_QD_V06"       -length  4.000000e-02 -e0  6.560098e-02 -strength -2.744692e-02
Dipole     -name "LN0_COR"          -length  0.000000e+00 -e0  6.560098e-02
Bpm        -name "LN0_BPM"          -length  0.000000e+00 -e0  6.560098e-02
Quadrupole -name "LN0_QD_V06"       -length  4.000000e-02 -e0  6.560098e-02 -strength -2.744692e-02
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  6.560098e-02
Drift      -name "LN0_DR_V8"        -length  2.000016e-01 -e0  6.560098e-02
Drift      -name "LN0_DR_VS"        -length  1.000000e-01 -e0  6.560098e-02
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  6.560098e-02
Drift      -name "LN0_DR_XCA_ED_DI" -length  1.001043e-01 -e0  6.560098e-02
CrabCavity -name "LN0_XCA0_DI"      -length  9.997914e-01 -voltage  0.000000e+00 -phase  9.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   6.560098e-02
Drift      -name "LN0_DR_XCA_ED_DI" -length  1.001043e-01 -e0  6.550935e-02
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  6.550935e-02
Drift      -name "LN0_TDS_DR"       -length  2.500000e-01 -e0  6.550935e-02
Drift      -name "LN0_TDS_DR"       -length  2.500000e-01 -e0  6.550935e-02
Drift      -name "LN0_TDS_DR"       -length  2.500000e-01 -e0  6.550935e-02
Drift      -name "LN0_TDS_DR"       -length  2.500000e-01 -e0  6.550935e-02
Drift      -name "LN0_TDS_DR"       -length  2.500000e-01 -e0  6.550935e-02
Drift      -name "LN0_TDS_DR"       -length  2.500000e-01 -e0  6.550935e-02
Drift      -name "LN0_TDS_DR"       -length  2.500000e-01 -e0  6.550935e-02
Drift      -name "LN0_TDS_DR"       -length  2.500000e-01 -e0  6.550935e-02
Sbend      -name "LN0_DP_TDS"       -length  1.000000e-01 -e0  6.550935e-02 -angle  0.000000e+00 -E1  0.000000e+00 -E2  0.000000e+00 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  0.000000e+00 -six_dim 1
Drift      -name "LN0_DR_20"        -length  2.000000e-01 -e0  6.550935e-02
Drift      -name "LN0_DR_VS"        -length  1.000000e-01 -e0  6.550935e-02
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  6.550935e-02
Quadrupole -name "LN0_QD_FH"        -length  4.000000e-02 -e0  6.550935e-02 -strength  1.834262e-02
Dipole     -name "LN0_COR"          -length  0.000000e+00 -e0  6.550935e-02
Bpm        -name "LN0_BPM"          -length  0.000000e+00 -e0  6.550935e-02
Quadrupole -name "LN0_QD_FH"        -length  4.000000e-02 -e0  6.550935e-02 -strength  1.834262e-02
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  6.550935e-02
Drift      -name "LN0_DR_SV"        -length  1.000000e-01 -e0  6.550935e-02
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  6.550935e-02
Drift      -name "LN0_DR_CCA_ED"    -length  5.019800e-02 -e0  6.550935e-02
Cavity     -name "LN0_CCA0"         -length  1.899604e+00 -gradient  3.000000e-02 -phase  2.600000e+01 -frequency  5.997100e+00
SetReferenceEnergy   1.166504e-01
Drift      -name "LN0_DR_CCA_ED"    -length  5.019800e-02 -e0  1.166504e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  1.166504e-01
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  1.166504e-01
Quadrupole -name "LN0_QD_DH"        -length  4.000000e-02 -e0  1.166504e-01 -strength -3.266211e-02
Dipole     -name "LN0_COR"          -length  0.000000e+00 -e0  1.166504e-01
Bpm        -name "LN0_BPM"          -length  0.000000e+00 -e0  1.166504e-01
Quadrupole -name "LN0_QD_DH"        -length  4.000000e-02 -e0  1.166504e-01 -strength -3.266211e-02
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  1.166504e-01
Drift      -name "LN0_DR_VS"        -length  1.000000e-01 -e0  1.166504e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  1.166504e-01
Drift      -name "LN0_DR_CCA_ED"    -length  5.019800e-02 -e0  1.166504e-01
Cavity     -name "LN0_CCA0"         -length  1.899604e+00 -gradient  3.000000e-02 -phase  2.600000e+01 -frequency  5.997100e+00
SetReferenceEnergy   1.677915e-01
Drift      -name "LN0_DR_CCA_ED"    -length  5.019800e-02 -e0  1.677915e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  1.677915e-01
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  1.677915e-01
Quadrupole -name "LN0_QD_FH"        -length  4.000000e-02 -e0  1.677915e-01 -strength  4.698161e-02
Dipole     -name "LN0_COR"          -length  0.000000e+00 -e0  1.677915e-01
Bpm        -name "LN0_BPM"          -length  0.000000e+00 -e0  1.677915e-01
Quadrupole -name "LN0_QD_FH"        -length  4.000000e-02 -e0  1.677915e-01 -strength  4.698161e-02
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  1.677915e-01
Drift      -name "LN0_DR_VS"        -length  1.000000e-01 -e0  1.677915e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  1.677915e-01
Drift      -name "LN0_DR_CCA_ED"    -length  5.019800e-02 -e0  1.677915e-01
Cavity     -name "LN0_CCA0"         -length  1.899604e+00 -gradient  3.000000e-02 -phase  2.600000e+01 -frequency  5.997100e+00
SetReferenceEnergy   2.189326e-01
Drift      -name "LN0_DR_CCA_ED"    -length  5.019800e-02 -e0  2.189326e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  2.189326e-01
Drift      -name "LN0_WA_LNZ_IN2"   -length  0.000000e+00 -e0  2.189326e-01
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  2.189326e-01
Quadrupole -name "LN0_QD_DH"        -length  4.000000e-02 -e0  2.189326e-01 -strength -6.130114e-02
Dipole     -name "LN0_COR"          -length  0.000000e+00 -e0  2.189326e-01
Bpm        -name "LN0_BPM"          -length  0.000000e+00 -e0  2.189326e-01
Quadrupole -name "LN0_QD_DH"        -length  4.000000e-02 -e0  2.189326e-01 -strength -6.130114e-02
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  2.189326e-01
Drift      -name "LN0_DR_VS"        -length  1.000000e-01 -e0  2.189326e-01
Drift      -name "LN0_DR_LNZ_ED2"   -length  6.250000e-01 -e0  2.189326e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  2.189326e-01
Drift      -name "LN0_DR_KCA_ED"    -length  2.225410e-02 -e0  2.189326e-01
Cavity     -name "LN0_KCA0"         -length  3.054918e-01 -gradient  2.257869e-02 -phase  1.930000e+02 -frequency  3.598260e+01
SetReferenceEnergy   2.123646e-01
Drift      -name "LN0_DR_KCA_ED"    -length  2.225410e-02 -e0  2.123646e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  2.123646e-01
Drift      -name "LN0_DR_KCA_ED"    -length  2.225410e-02 -e0  2.123646e-01
Drift      -name "LN0_DR_KCA0"      -length  3.054918e-01 -e0  2.123646e-01
Drift      -name "LN0_DR_KCA_ED"    -length  2.225410e-02 -e0  2.123646e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  2.123646e-01
Drift      -name "LN0_DR_LNZ_ED2"   -length  6.250000e-01 -e0  2.123646e-01
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  2.123646e-01
Quadrupole -name "LN0_QD_FH"        -length  4.000000e-02 -e0  2.123646e-01 -strength  5.946208e-02
Dipole     -name "LN0_COR"          -length  0.000000e+00 -e0  2.123646e-01
Bpm        -name "LN0_BPM"          -length  0.000000e+00 -e0  2.123646e-01
Quadrupole -name "LN0_QD_FH"        -length  4.000000e-02 -e0  2.123646e-01 -strength  5.946208e-02
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  2.123646e-01
Drift      -name "LN0_WA_OU2"       -length  0.000000e+00 -e0  2.123646e-01
Drift      -name "BC1_DR_V1"        -length  6.224317e-01 -e0  2.123646e-01
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.123646e-01
Quadrupole -name "BC1_QD_V01"       -length  4.000000e-02 -e0  2.123646e-01 -strength  1.536247e-02
Dipole     -name "BC1_COR"          -length  0.000000e+00 -e0  2.123646e-01
Bpm        -name "BC1_BPM"          -length  0.000000e+00 -e0  2.123646e-01
Quadrupole -name "BC1_QD_V01"       -length  4.000000e-02 -e0  2.123646e-01 -strength  1.536247e-02
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.123646e-01
Drift      -name "BC1_DR_V2"        -length  6.073603e+00 -e0  2.123646e-01
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.123646e-01
Quadrupole -name "BC1_QD_V02"       -length  4.000000e-02 -e0  2.123646e-01 -strength -7.627914e-02
Dipole     -name "BC1_COR"          -length  0.000000e+00 -e0  2.123646e-01
Bpm        -name "BC1_BPM"          -length  0.000000e+00 -e0  2.123646e-01
Quadrupole -name "BC1_QD_V02"       -length  4.000000e-02 -e0  2.123646e-01 -strength -7.627914e-02
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.123646e-01
Drift      -name "BC1_DR_V3"        -length  2.421820e-01 -e0  2.123646e-01
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.123646e-01
Quadrupole -name "BC1_QD_V03"       -length  4.000000e-02 -e0  2.123646e-01 -strength  2.937504e-02
Dipole     -name "BC1_COR"          -length  0.000000e+00 -e0  2.123646e-01
Bpm        -name "BC1_BPM"          -length  0.000000e+00 -e0  2.123646e-01
Quadrupole -name "BC1_QD_V03"       -length  4.000000e-02 -e0  2.123646e-01 -strength  2.937504e-02
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.123646e-01
Drift      -name "BC1_DR_V4"        -length  2.780184e-01 -e0  2.123646e-01
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.123646e-01
Quadrupole -name "BC1_QD_V04"       -length  4.000000e-02 -e0  2.123646e-01 -strength  4.783275e-02
Dipole     -name "BC1_COR"          -length  0.000000e+00 -e0  2.123646e-01
Bpm        -name "BC1_BPM"          -length  0.000000e+00 -e0  2.123646e-01
Quadrupole -name "BC1_QD_V04"       -length  4.000000e-02 -e0  2.123646e-01 -strength  4.783275e-02
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.123646e-01
Drift      -name "BC1_DR_V5"        -length  2.680051e-01 -e0  2.123646e-01
Drift      -name "BC1_DR_20"        -length  5.000000e-01 -e0  2.123646e-01
Sbend      -name "BC1_DP_DIP1"      -length  4.002253e-01 -e0  2.123646e-01 -angle -5.811946e-02 -E1  0.000000e+00 -E2 -5.811946e-02 -hgap  1.500000e-02 -fint  5.000000e-01 -tilt  0.000000e+00  -six_dim 1 -csr 1 
Drift      -name "BC1_DR_SIDE_C"    -length  3.000000e-01 -e0  2.123646e-01
Drift      -name "BC1_DR_SIDE"      -length  2.955497e+00 -e0  2.123646e-01
Sbend      -name "BC1_DP_DIP2"      -length  4.002253e-01 -e0  2.123646e-01 -angle  5.811946e-02 -E1  5.811946e-02 -E2  0.000000e+00 -hgap  1.500000e-02 -fint  5.000000e-01 -tilt  0.000000e+00  -six_dim 1 -csr 1 
Drift      -name "BC1_DR_CENT_C"    -length  3.500000e-01 -e0  2.123646e-01
Bpm        -name "BC1_BPM"          -length  0.000000e+00 -e0  2.123646e-01
Drift      -name "BC1_DR_CENT"      -length  3.500000e-01 -e0  2.123646e-01
Sbend      -name "BC1_DP_DIP3"      -length  4.002253e-01 -e0  2.123646e-01 -angle  5.811946e-02 -E1  0.000000e+00 -E2  5.811946e-02 -hgap  1.500000e-02 -fint  5.000000e-01 -tilt  0.000000e+00  -six_dim 1 -csr 1 
Drift      -name "BC1_DR_SIDE_C"    -length  3.000000e-01 -e0  2.123646e-01
Drift      -name "BC1_DR_SIDE"      -length  2.955497e+00 -e0  2.123646e-01
Sbend      -name "BC1_DP_DIP4"      -length  4.002253e-01 -e0  2.123646e-01 -angle -5.811946e-02 -E1 -5.811946e-02 -E2  0.000000e+00 -hgap  1.500000e-02 -fint  5.000000e-01 -tilt  0.000000e+00  -six_dim 1 -csr 1 
Drift      -name "BC1_DR_20_C"      -length  3.000000e-01 -e0  2.123646e-01
Drift      -name "BC1_WA_OU2"       -length  0.000000e+00 -e0  2.123646e-01
Drift      -name "BC1_DR_CT"        -length  1.000000e-01 -e0  2.123646e-01
Drift      -name "BC1_DR_DI_V1"     -length  2.000127e-01 -e0  2.123646e-01
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.123646e-01
Quadrupole -name "BC1_QD_DI_V01"    -length  4.000000e-02 -e0  2.123646e-01 -strength -7.952225e-02
Dipole     -name "BC1_COR_DI"       -length  0.000000e+00 -e0  2.123646e-01
Bpm        -name "BC1_BPM_DI"       -length  0.000000e+00 -e0  2.123646e-01
Quadrupole -name "BC1_QD_DI_V01"    -length  4.000000e-02 -e0  2.123646e-01 -strength -7.952225e-02
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.123646e-01
Drift      -name "BC1_DR_DI_V2"     -length  4.782030e-01 -e0  2.123646e-01
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.123646e-01
Quadrupole -name "BC1_QD_DI_V02"    -length  4.000000e-02 -e0  2.123646e-01 -strength  1.032077e-01
Dipole     -name "BC1_COR_DI"       -length  0.000000e+00 -e0  2.123646e-01
Bpm        -name "BC1_BPM_DI"       -length  0.000000e+00 -e0  2.123646e-01
Quadrupole -name "BC1_QD_DI_V02"    -length  4.000000e-02 -e0  2.123646e-01 -strength  1.032077e-01
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.123646e-01
Drift      -name "BC1_DR_DI_V3"     -length  1.459871e+00 -e0  2.123646e-01
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.123646e-01
Quadrupole -name "BC1_QD_DI_V03"    -length  4.000000e-02 -e0  2.123646e-01 -strength -3.500808e-02
Dipole     -name "BC1_COR_DI"       -length  0.000000e+00 -e0  2.123646e-01
Bpm        -name "BC1_BPM_DI"       -length  0.000000e+00 -e0  2.123646e-01
Quadrupole -name "BC1_QD_DI_V03"    -length  4.000000e-02 -e0  2.123646e-01 -strength -3.500808e-02
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.123646e-01
Drift      -name "BC1_DR_DI_V5"     -length  2.000000e-01 -e0  2.123646e-01
Drift      -name "BC1_DR_DI"        -length  2.500000e-01 -e0  2.123646e-01
Drift      -name "BC1_DR_VS"        -length  1.000000e-01 -e0  2.123646e-01
Drift      -name "BC1_DR_BL"        -length  5.000000e-02 -e0  2.123646e-01
Drift      -name "BC1_DR_XCA_ED_DI" -length  1.001043e-01 -e0  2.123646e-01
CrabCavity -name "BC1_XCA0_DI"      -length  9.997914e-01 -voltage  0.000000e+00 -phase  9.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.123646e-01
Drift      -name "BC1_DR_XCA_ED_DI" -length  1.001043e-01 -e0  2.122460e-01
Drift      -name "BC1_DR_BL"        -length  5.000000e-02 -e0  2.122460e-01
Drift      -name "BC1_DR_VS"        -length  1.000000e-01 -e0  2.122460e-01
Drift      -name "BC1_DR_DI"        -length  2.500000e-01 -e0  2.122460e-01
Drift      -name "BC1_DR_DI"        -length  2.500000e-01 -e0  2.122460e-01
Drift      -name "BC1_DR_DI_QD_ED"  -length  4.250000e-02 -e0  2.122460e-01
Quadrupole -name "BC1_QD_DI_FH"     -length  4.000000e-02 -e0  2.122460e-01 -strength  5.093905e-02
Dipole     -name "BC1_COR_DI"       -length  0.000000e+00 -e0  2.122460e-01
Bpm        -name "BC1_BPM_DI"       -length  0.000000e+00 -e0  2.122460e-01
Quadrupole -name "BC1_QD_DI_FH"     -length  4.000000e-02 -e0  2.122460e-01 -strength  5.093905e-02
Drift      -name "BC1_DR_DI_QD_ED"  -length  4.250000e-02 -e0  2.122460e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.122460e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.122460e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.122460e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.122460e-01
Drift      -name "BC1_DR_DI_VS"     -length  1.000000e-01 -e0  2.122460e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.122460e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.122460e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.122460e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.122460e-01
Drift      -name "BC1_DR_DI_QD_ED"  -length  4.250000e-02 -e0  2.122460e-01
Quadrupole -name "BC1_QD_DI_DH"     -length  4.000000e-02 -e0  2.122460e-01 -strength -5.093905e-02
Dipole     -name "BC1_COR_DI"       -length  0.000000e+00 -e0  2.122460e-01
Bpm        -name "BC1_BPM_DI"       -length  0.000000e+00 -e0  2.122460e-01
Quadrupole -name "BC1_QD_DI_DH"     -length  4.000000e-02 -e0  2.122460e-01 -strength -5.093905e-02
Drift      -name "BC1_DR_DI_QD_ED"  -length  4.250000e-02 -e0  2.122460e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.122460e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.122460e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.122460e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.122460e-01
Drift      -name "BC1_DR_DI_VS"     -length  1.000000e-01 -e0  2.122460e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.122460e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.122460e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.122460e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.122460e-01
Drift      -name "BC1_DR_DI_QD_ED"  -length  4.250000e-02 -e0  2.122460e-01
Quadrupole -name "BC1_QD_DI_FH"     -length  4.000000e-02 -e0  2.122460e-01 -strength  5.093905e-02
Dipole     -name "BC1_COR_DI"       -length  0.000000e+00 -e0  2.122460e-01
Bpm        -name "BC1_BPM_DI"       -length  0.000000e+00 -e0  2.122460e-01
Quadrupole -name "BC1_QD_DI_FH"     -length  4.000000e-02 -e0  2.122460e-01 -strength  5.093905e-02
Drift      -name "BC1_DR_DI_QD_ED"  -length  4.250000e-02 -e0  2.122460e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.122460e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.122460e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.122460e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.122460e-01
Drift      -name "BC1_DR_DI_VS"     -length  1.000000e-01 -e0  2.122460e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.122460e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.122460e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.122460e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.122460e-01
Drift      -name "BC1_DR_DI_QD_ED"  -length  4.250000e-02 -e0  2.122460e-01
Quadrupole -name "BC1_QD_DI_DH"     -length  4.000000e-02 -e0  2.122460e-01 -strength -5.093905e-02
Dipole     -name "BC1_COR_DI"       -length  0.000000e+00 -e0  2.122460e-01
Bpm        -name "BC1_BPM_DI"       -length  0.000000e+00 -e0  2.122460e-01
Quadrupole -name "BC1_QD_DI_DH"     -length  4.000000e-02 -e0  2.122460e-01 -strength -5.093905e-02
Drift      -name "BC1_DR_DI_QD_ED"  -length  4.250000e-02 -e0  2.122460e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.122460e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.122460e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.122460e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.122460e-01
Drift      -name "BC1_DR_DI_VS"     -length  1.000000e-01 -e0  2.122460e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.122460e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.122460e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.122460e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.122460e-01
Drift      -name "BC1_DR_DI_QD_ED"  -length  4.250000e-02 -e0  2.122460e-01
Quadrupole -name "BC1_QD_DI_FH"     -length  4.000000e-02 -e0  2.122460e-01 -strength  5.093905e-02
Dipole     -name "BC1_COR_DI"       -length  0.000000e+00 -e0  2.122460e-01
Bpm        -name "BC1_BPM_DI"       -length  0.000000e+00 -e0  2.122460e-01
Quadrupole -name "BC1_QD_DI_FH"     -length  4.000000e-02 -e0  2.122460e-01 -strength  5.093905e-02
Drift      -name "BC1_DR_DI_QD_ED"  -length  4.250000e-02 -e0  2.122460e-01
Drift      -name "BC1_DR_DI"        -length  2.500000e-01 -e0  2.122460e-01
Sbend      -name "BC1_DP_DI"        -length  2.500000e-01 -e0  2.122460e-01 -angle  0.000000e+00 -E1  0.000000e+00 -E2  0.000000e+00 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  0.000000e+00 -six_dim 1
Drift      -name "BC1_DR_DI"        -length  2.500000e-01 -e0  2.122460e-01
Drift      -name "BC1_DR_DI"        -length  2.500000e-01 -e0  2.122460e-01
Drift      -name "BC1_DR_DI"        -length  2.500000e-01 -e0  2.122460e-01
Drift      -name "BC1_WA_OU_DI2"    -length  0.000000e+00 -e0  2.122460e-01
Drift      -name "LN1_DR_V1"        -length  1.780190e-01 -e0  2.122460e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  2.122460e-01
Quadrupole -name "LN1_QD_V01"       -length  4.000000e-02 -e0  2.122460e-01 -strength  2.925993e-02
Dipole     -name "LN1_COR"          -length  0.000000e+00 -e0  2.122460e-01
Bpm        -name "LN1_BPM"          -length  0.000000e+00 -e0  2.122460e-01
Quadrupole -name "LN1_QD_V01"       -length  4.000000e-02 -e0  2.122460e-01 -strength  2.925993e-02
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  2.122460e-01
Drift      -name "LN1_DR_V2"        -length  1.008292e-01 -e0  2.122460e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  2.122460e-01
Quadrupole -name "LN1_QD_V02"       -length  4.000000e-02 -e0  2.122460e-01 -strength -7.440508e-02
Dipole     -name "LN1_COR"          -length  0.000000e+00 -e0  2.122460e-01
Bpm        -name "LN1_BPM"          -length  0.000000e+00 -e0  2.122460e-01
Quadrupole -name "LN1_QD_V02"       -length  4.000000e-02 -e0  2.122460e-01 -strength -7.440508e-02
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  2.122460e-01
Drift      -name "LN1_DR_V3"        -length  2.337738e+00 -e0  2.122460e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  2.122460e-01
Quadrupole -name "LN1_QD_V03"       -length  4.000000e-02 -e0  2.122460e-01 -strength -5.197147e-03
Dipole     -name "LN1_COR"          -length  0.000000e+00 -e0  2.122460e-01
Bpm        -name "LN1_BPM"          -length  0.000000e+00 -e0  2.122460e-01
Quadrupole -name "LN1_QD_V03"       -length  4.000000e-02 -e0  2.122460e-01 -strength -5.197147e-03
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  2.122460e-01
Drift      -name "LN1_DR_V4"        -length  1.026734e-01 -e0  2.122460e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  2.122460e-01
Quadrupole -name "LN1_QD_FH"        -length  4.000000e-02 -e0  2.122460e-01 -strength  5.603295e-02
Dipole     -name "LN1_COR"          -length  0.000000e+00 -e0  2.122460e-01
Bpm        -name "LN1_BPM"          -length  0.000000e+00 -e0  2.122460e-01
Quadrupole -name "LN1_QD_FH"        -length  4.000000e-02 -e0  2.122460e-01 -strength  5.603295e-02
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  2.122460e-01
Drift      -name "LN1_DR_SV"        -length  1.000000e-01 -e0  2.122460e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  2.122460e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  2.122460e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.688203e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  2.688203e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  2.688203e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  2.688203e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   3.253946e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  3.253946e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  3.253946e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  3.253946e-01
Quadrupole -name "LN1_QD_DH"        -length  4.000000e-02 -e0  3.253946e-01 -strength -8.590417e-02
Dipole     -name "LN1_COR"          -length  0.000000e+00 -e0  3.253946e-01
Bpm        -name "LN1_BPM"          -length  0.000000e+00 -e0  3.253946e-01
Quadrupole -name "LN1_QD_DH"        -length  4.000000e-02 -e0  3.253946e-01 -strength -8.590417e-02
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  3.253946e-01
Drift      -name "LN1_DR_VS"        -length  1.000000e-01 -e0  3.253946e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  3.253946e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  3.253946e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   3.819689e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  3.819689e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  3.819689e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  3.819689e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   4.385431e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  4.385431e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  4.385431e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  4.385431e-01
Quadrupole -name "LN1_QD_FH"        -length  4.000000e-02 -e0  4.385431e-01 -strength  1.157754e-01
Dipole     -name "LN1_COR"          -length  0.000000e+00 -e0  4.385431e-01
Bpm        -name "LN1_BPM"          -length  0.000000e+00 -e0  4.385431e-01
Quadrupole -name "LN1_QD_FH"        -length  4.000000e-02 -e0  4.385431e-01 -strength  1.157754e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  4.385431e-01
Drift      -name "LN1_DR_SV"        -length  1.000000e-01 -e0  4.385431e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  4.385431e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  4.385431e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   4.951174e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  4.951174e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  4.951174e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  4.951174e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   5.516917e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  5.516917e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  5.516917e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  5.516917e-01
Quadrupole -name "LN1_QD_DH"        -length  4.000000e-02 -e0  5.516917e-01 -strength -1.456466e-01
Dipole     -name "LN1_COR"          -length  0.000000e+00 -e0  5.516917e-01
Bpm        -name "LN1_BPM"          -length  0.000000e+00 -e0  5.516917e-01
Quadrupole -name "LN1_QD_DH"        -length  4.000000e-02 -e0  5.516917e-01 -strength -1.456466e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  5.516917e-01
Drift      -name "LN1_DR_VS"        -length  1.000000e-01 -e0  5.516917e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  5.516917e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  5.516917e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   6.082659e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  6.082659e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  6.082659e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  6.082659e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   6.648402e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  6.648402e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  6.648402e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  6.648402e-01
Quadrupole -name "LN1_QD_FH"        -length  4.000000e-02 -e0  6.648402e-01 -strength  1.755178e-01
Dipole     -name "LN1_COR"          -length  0.000000e+00 -e0  6.648402e-01
Bpm        -name "LN1_BPM"          -length  0.000000e+00 -e0  6.648402e-01
Quadrupole -name "LN1_QD_FH"        -length  4.000000e-02 -e0  6.648402e-01 -strength  1.755178e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  6.648402e-01
Drift      -name "LN1_WA_OU2"       -length  0.000000e+00 -e0  6.648402e-01
Drift      -name "BC2_IN_CNT"       -length  0.000000e+00 -e0  6.648402e-01
Drift      -name "BC2_DR_V1"        -length  2.158724e-01 -e0  6.648402e-01
Drift      -name "BC2_DR_QD_ED"     -length  4.250000e-02 -e0  6.648402e-01
Quadrupole -name "BC2_QD_V01"       -length  4.000000e-02 -e0  6.648402e-01 -strength  1.149222e-01
Dipole     -name "BC2_COR"          -length  0.000000e+00 -e0  6.648402e-01
Bpm        -name "BC2_BPM"          -length  0.000000e+00 -e0  6.648402e-01
Quadrupole -name "BC2_QD_V01"       -length  4.000000e-02 -e0  6.648402e-01 -strength  1.149222e-01
Drift      -name "BC2_DR_QD_ED"     -length  4.250000e-02 -e0  6.648402e-01
Drift      -name "BC2_DR_V2"        -length  5.269439e+00 -e0  6.648402e-01
Drift      -name "BC2_DR_QD_ED"     -length  4.250000e-02 -e0  6.648402e-01
Quadrupole -name "BC2_QD_V02"       -length  4.000000e-02 -e0  6.648402e-01 -strength -2.846241e-01
Dipole     -name "BC2_COR"          -length  0.000000e+00 -e0  6.648402e-01
Bpm        -name "BC2_BPM"          -length  0.000000e+00 -e0  6.648402e-01
Quadrupole -name "BC2_QD_V02"       -length  4.000000e-02 -e0  6.648402e-01 -strength -2.846241e-01
Drift      -name "BC2_DR_QD_ED"     -length  4.250000e-02 -e0  6.648402e-01
Drift      -name "BC2_DR_V3"        -length  2.226137e-01 -e0  6.648402e-01
Drift      -name "BC2_DR_QD_ED"     -length  4.250000e-02 -e0  6.648402e-01
Quadrupole -name "BC2_QD_V03"       -length  4.000000e-02 -e0  6.648402e-01 -strength  2.933807e-01
Dipole     -name "BC2_COR"          -length  0.000000e+00 -e0  6.648402e-01
Bpm        -name "BC2_BPM"          -length  0.000000e+00 -e0  6.648402e-01
Quadrupole -name "BC2_QD_V03"       -length  4.000000e-02 -e0  6.648402e-01 -strength  2.933807e-01
Drift      -name "BC2_DR_QD_ED"     -length  4.250000e-02 -e0  6.648402e-01
Drift      -name "BC2_DR_V4"        -length  2.000000e-01 -e0  6.648402e-01
Drift      -name "BC2_DR_20"        -length  5.000000e-01 -e0  6.648402e-01
Sbend      -name "BC2_DP_DIP1"      -length  6.000515e-01 -e0  6.648402e-01 -angle -2.268928e-02 -E1  0.000000e+00 -E2 -2.268928e-02 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  0.000000e+00  -six_dim 1 -csr 1 
Drift      -name "BC2_DR_SIDE_C"    -length  5.000000e-01 -e0  6.648402e-01
Drift      -name "BC2_DR_SIDE"      -length  3.200953e+00 -e0  6.648402e-01
Sbend      -name "BC2_DP_DIP2"      -length  6.000515e-01 -e0  6.648402e-01 -angle  2.268928e-02 -E1  2.268928e-02 -E2  0.000000e+00 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  0.000000e+00  -six_dim 1 -csr 1 
Drift      -name "BC2_DR_CENT_C"    -length  2.500000e-01 -e0  6.648402e-01
Bpm        -name "BC2_BPM"          -length  0.000000e+00 -e0  6.648402e-01
Drift      -name "BC2_DR_CENT"      -length  2.500000e-01 -e0  6.648402e-01
Sbend      -name "BC2_DP_DIP3"      -length  6.000515e-01 -e0  6.648402e-01 -angle  2.268928e-02 -E1  0.000000e+00 -E2  2.268928e-02 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  0.000000e+00  -six_dim 1 -csr 1 
Drift      -name "BC2_DR_SIDE_C"    -length  5.000000e-01 -e0  6.648402e-01
Drift      -name "BC2_DR_SIDE"      -length  3.200953e+00 -e0  6.648402e-01
Sbend      -name "BC2_DP_DIP4"      -length  6.000515e-01 -e0  6.648402e-01 -angle -2.268928e-02 -E1 -2.268928e-02 -E2  0.000000e+00 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  0.000000e+00  -six_dim 1 -csr 1 
Drift      -name "BC2_IN_CNT"       -length  0.000000e+00 -e0  6.648402e-01
Drift      -name "BC2_DR_20_C"      -length  5.000000e-01 -e0  6.648402e-01
Drift      -name "BC2_IN_CNT"       -length  0.000000e+00 -e0  6.648402e-01
Drift      -name "BC2_WA_OU2"       -length  0.000000e+00 -e0  6.648402e-01
Drift      -name "LN2_DR_CT"        -length  1.000000e-01 -e0  6.648402e-01
Drift      -name "LN2_DR_V1"        -length  1.141306e+00 -e0  6.648402e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  6.648402e-01
Quadrupole -name "LN2_QD_V01"       -length  4.000000e-02 -e0  6.648402e-01 -strength  3.694484e-01
Dipole     -name "LN2_COR"          -length  0.000000e+00 -e0  6.648402e-01
Bpm        -name "LN2_BPM"          -length  0.000000e+00 -e0  6.648402e-01
Quadrupole -name "LN2_QD_V01"       -length  4.000000e-02 -e0  6.648402e-01 -strength  3.694484e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  6.648402e-01
Drift      -name "LN2_DR_V2"        -length  7.053014e-01 -e0  6.648402e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  6.648402e-01
Quadrupole -name "LN2_QD_V02"       -length  4.000000e-02 -e0  6.648402e-01 -strength -3.694017e-01
Dipole     -name "LN2_COR"          -length  0.000000e+00 -e0  6.648402e-01
Bpm        -name "LN2_BPM"          -length  0.000000e+00 -e0  6.648402e-01
Quadrupole -name "LN2_QD_V02"       -length  4.000000e-02 -e0  6.648402e-01 -strength -3.694017e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  6.648402e-01
Drift      -name "LN2_DR_V3"        -length  6.915788e-01 -e0  6.648402e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  6.648402e-01
Quadrupole -name "LN2_QD_V03"       -length  4.000000e-02 -e0  6.648402e-01 -strength  2.318665e-01
Dipole     -name "LN2_COR"          -length  0.000000e+00 -e0  6.648402e-01
Bpm        -name "LN2_BPM"          -length  0.000000e+00 -e0  6.648402e-01
Quadrupole -name "LN2_QD_V03"       -length  4.000000e-02 -e0  6.648402e-01 -strength  2.318665e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  6.648402e-01
Drift      -name "LN2_DR_V4"        -length  2.491549e+00 -e0  6.648402e-01
Drift      -name "LN2_DR_VS"        -length  1.000000e-01 -e0  6.648402e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  6.648402e-01
Quadrupole -name "LN2_QD_FH"        -length  4.000000e-02 -e0  6.648402e-01 -strength  1.781772e-01
Dipole     -name "LN2_COR"          -length  0.000000e+00 -e0  6.648402e-01
Bpm        -name "LN2_BPM"          -length  0.000000e+00 -e0  6.648402e-01
Quadrupole -name "LN2_QD_FH"        -length  4.000000e-02 -e0  6.648402e-01 -strength  1.781772e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  6.648402e-01
Drift      -name "LN2_DR_SV"        -length  1.000000e-01 -e0  6.648402e-01
Drift      -name "LN2_DR_BL"        -length  5.000000e-02 -e0  6.648402e-01
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  6.648402e-01
Cavity     -name "LN2_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase -3.500000e+01 -frequency  1.199420e+01
SetReferenceEnergy   7.132980e-01
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  7.132980e-01
Drift      -name "LN2_DR_BL"        -length  5.000000e-02 -e0  7.132980e-01
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  7.132980e-01
Cavity     -name "LN2_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase -3.500000e+01 -frequency  1.199420e+01
SetReferenceEnergy   7.620058e-01
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  7.620058e-01
Drift      -name "LN2_DR_BL"        -length  5.000000e-02 -e0  7.620058e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  7.620058e-01
Quadrupole -name "LN2_QD_DH"        -length  4.000000e-02 -e0  7.620058e-01 -strength -2.042176e-01
Dipole     -name "LN2_COR"          -length  0.000000e+00 -e0  7.620058e-01
Bpm        -name "LN2_BPM"          -length  0.000000e+00 -e0  7.620058e-01
Quadrupole -name "LN2_QD_DH"        -length  4.000000e-02 -e0  7.620058e-01 -strength -2.042176e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  7.620058e-01
Drift      -name "LN2_DR_VS"        -length  1.000000e-01 -e0  7.620058e-01
Drift      -name "LN2_DR_BL"        -length  5.000000e-02 -e0  7.620058e-01
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  7.620058e-01
Cavity     -name "LN2_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase -3.500000e+01 -frequency  1.199420e+01
SetReferenceEnergy   8.107136e-01
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  8.107136e-01
Drift      -name "LN2_DR_BL"        -length  5.000000e-02 -e0  8.107136e-01
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  8.107136e-01
Cavity     -name "LN2_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase -3.500000e+01 -frequency  1.199420e+01
SetReferenceEnergy   8.594214e-01
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  8.594214e-01
Drift      -name "LN2_DR_BL"        -length  5.000000e-02 -e0  8.594214e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  8.594214e-01
Quadrupole -name "LN2_QD_FH"        -length  4.000000e-02 -e0  8.594214e-01 -strength  2.303249e-01
Dipole     -name "LN2_COR"          -length  0.000000e+00 -e0  8.594214e-01
Bpm        -name "LN2_BPM"          -length  0.000000e+00 -e0  8.594214e-01
Quadrupole -name "LN2_QD_FH"        -length  4.000000e-02 -e0  8.594214e-01 -strength  2.303249e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  8.594214e-01
Drift      -name "LN2_DR_SV"        -length  1.000000e-01 -e0  8.594214e-01
Drift      -name "LN2_DR_BL"        -length  5.000000e-02 -e0  8.594214e-01
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  8.594214e-01
Cavity     -name "LN2_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase -3.500000e+01 -frequency  1.199420e+01
SetReferenceEnergy   9.081293e-01
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  9.081293e-01
Drift      -name "LN2_DR_BL"        -length  5.000000e-02 -e0  9.081293e-01
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  9.081293e-01
Cavity     -name "LN2_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase -3.500000e+01 -frequency  1.199420e+01
SetReferenceEnergy   9.568371e-01
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  9.568371e-01
Drift      -name "LN2_DR_BL"        -length  5.000000e-02 -e0  9.568371e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  9.568371e-01
Quadrupole -name "LN2_QD_DH"        -length  4.000000e-02 -e0  9.568371e-01 -strength -2.564323e-01
Dipole     -name "LN2_COR"          -length  0.000000e+00 -e0  9.568371e-01
Bpm        -name "LN2_BPM"          -length  0.000000e+00 -e0  9.568371e-01
Quadrupole -name "LN2_QD_DH"        -length  4.000000e-02 -e0  9.568371e-01 -strength -2.564323e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  9.568371e-01
Drift      -name "LN2_DR_VS"        -length  1.000000e-01 -e0  9.568371e-01
Drift      -name "LN2_DR_BL"        -length  5.000000e-02 -e0  9.568371e-01
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  9.568371e-01
Cavity     -name "LN2_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase -3.500000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.005545e+00
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  1.005545e+00
Drift      -name "LN2_DR_BL"        -length  5.000000e-02 -e0  1.005545e+00
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  1.005545e+00
Cavity     -name "LN2_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase -3.500000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.054253e+00
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  1.054253e+00
Drift      -name "LN2_DR_BL"        -length  5.000000e-02 -e0  1.054253e+00
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.054253e+00
Quadrupole -name "LN2_QD_FH"        -length  4.000000e-02 -e0  1.054253e+00 -strength  2.825397e-01
Dipole     -name "LN2_COR"          -length  0.000000e+00 -e0  1.054253e+00
Bpm        -name "LN2_BPM"          -length  0.000000e+00 -e0  1.054253e+00
Quadrupole -name "LN2_QD_FH"        -length  4.000000e-02 -e0  1.054253e+00 -strength  2.825397e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.054253e+00
Drift      -name "LN2_WA_OU2"       -length  0.000000e+00 -e0  1.054253e+00
Drift      -name "LN2_DR_DI_V1"     -length  1.174292e+00 -e0  1.054253e+00
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.054253e+00
Quadrupole -name "LN2_QD_DI_V01"    -length  4.000000e-02 -e0  1.054253e+00 -strength -1.081616e-01
Dipole     -name "LN2_COR_DI"       -length  0.000000e+00 -e0  1.054253e+00
Bpm        -name "LN2_BPM_DI"       -length  0.000000e+00 -e0  1.054253e+00
Quadrupole -name "LN2_QD_DI_V01"    -length  4.000000e-02 -e0  1.054253e+00 -strength -1.081616e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.054253e+00
Drift      -name "LN2_DR_DI_V2"     -length  8.605683e-01 -e0  1.054253e+00
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.054253e+00
Quadrupole -name "LN2_QD_DI_V02"    -length  4.000000e-02 -e0  1.054253e+00 -strength  4.119216e-01
Dipole     -name "LN2_COR_DI"       -length  0.000000e+00 -e0  1.054253e+00
Bpm        -name "LN2_BPM_DI"       -length  0.000000e+00 -e0  1.054253e+00
Quadrupole -name "LN2_QD_DI_V02"    -length  4.000000e-02 -e0  1.054253e+00 -strength  4.119216e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.054253e+00
Drift      -name "LN2_DR_DI_V3"     -length  2.178709e-01 -e0  1.054253e+00
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.054253e+00
Quadrupole -name "LN2_QD_DI_V03"    -length  4.000000e-02 -e0  1.054253e+00 -strength -5.009720e-01
Dipole     -name "LN2_COR_DI"       -length  0.000000e+00 -e0  1.054253e+00
Bpm        -name "LN2_BPM_DI"       -length  0.000000e+00 -e0  1.054253e+00
Quadrupole -name "LN2_QD_DI_V03"    -length  4.000000e-02 -e0  1.054253e+00 -strength -5.009720e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.054253e+00
Drift      -name "LN2_DR_DI_V5"     -length  2.000000e-01 -e0  1.054253e+00
Drift      -name "LN2_DR_VS"        -length  1.000000e-01 -e0  1.054253e+00
Drift      -name "LN2_DR_DI"        -length  2.500000e-01 -e0  1.054253e+00
Drift      -name "LN2_DR_VS_DI"     -length  1.000000e-01 -e0  1.054253e+00
Drift      -name "LN2_DR_BL_DI"     -length  5.000000e-02 -e0  1.054253e+00
Drift      -name "LN2_DR_XCA_ED_DI" -length  1.001043e-01 -e0  1.054253e+00
CrabCavity -name "LN2_XCA0_DI"      -length  9.997914e-01 -voltage  0.000000e+00 -phase  9.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.054253e+00
Drift      -name "LN2_DR_XCA_ED_DI" -length  1.001043e-01 -e0  1.054177e+00
Drift      -name "LN2_DR_BL_DI"     -length  5.000000e-02 -e0  1.054177e+00
Drift      -name "LN2_DR_DI"        -length  2.500000e-01 -e0  1.054177e+00
Drift      -name "LN2_DR_DI"        -length  2.500000e-01 -e0  1.054177e+00
Drift      -name "LN2_DR_DI_QD_ED"  -length  4.250000e-02 -e0  1.054177e+00
Quadrupole -name "LN2_QD_DI_FH"     -length  4.000000e-02 -e0  1.054177e+00 -strength  3.373368e-01
Dipole     -name "LN2_COR_DI"       -length  0.000000e+00 -e0  1.054177e+00
Bpm        -name "LN2_BPM_DI"       -length  0.000000e+00 -e0  1.054177e+00
Quadrupole -name "LN2_QD_DI_FH"     -length  4.000000e-02 -e0  1.054177e+00 -strength  3.373368e-01
Drift      -name "LN2_DR_DI_QD_ED"  -length  4.250000e-02 -e0  1.054177e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.054177e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.054177e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.054177e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.054177e+00
Drift      -name "LN2_DR_DI_VS"     -length  1.000000e-01 -e0  1.054177e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.054177e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.054177e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.054177e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.054177e+00
Drift      -name "LN2_DR_DI_QD_ED"  -length  4.250000e-02 -e0  1.054177e+00
Quadrupole -name "LN2_QD_DI_DH"     -length  4.000000e-02 -e0  1.054177e+00 -strength -3.373368e-01
Dipole     -name "LN2_COR_DI"       -length  0.000000e+00 -e0  1.054177e+00
Bpm        -name "LN2_BPM_DI"       -length  0.000000e+00 -e0  1.054177e+00
Quadrupole -name "LN2_QD_DI_DH"     -length  4.000000e-02 -e0  1.054177e+00 -strength -3.373368e-01
Drift      -name "LN2_DR_DI_QD_ED"  -length  4.250000e-02 -e0  1.054177e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.054177e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.054177e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.054177e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.054177e+00
Drift      -name "LN2_DR_DI_VS"     -length  1.000000e-01 -e0  1.054177e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.054177e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.054177e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.054177e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.054177e+00
Drift      -name "LN2_DR_DI_QD_ED"  -length  4.250000e-02 -e0  1.054177e+00
Quadrupole -name "LN2_QD_DI_FH"     -length  4.000000e-02 -e0  1.054177e+00 -strength  3.373368e-01
Dipole     -name "LN2_COR_DI"       -length  0.000000e+00 -e0  1.054177e+00
Bpm        -name "LN2_BPM_DI"       -length  0.000000e+00 -e0  1.054177e+00
Quadrupole -name "LN2_QD_DI_FH"     -length  4.000000e-02 -e0  1.054177e+00 -strength  3.373368e-01
Drift      -name "LN2_DR_DI_QD_ED"  -length  4.250000e-02 -e0  1.054177e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.054177e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.054177e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.054177e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.054177e+00
Drift      -name "LN2_DR_DI_VS"     -length  1.000000e-01 -e0  1.054177e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.054177e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.054177e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.054177e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.054177e+00
Drift      -name "LN2_DR_DI_QD_ED"  -length  4.250000e-02 -e0  1.054177e+00
Quadrupole -name "LN2_QD_DI_DH"     -length  4.000000e-02 -e0  1.054177e+00 -strength -3.373368e-01
Dipole     -name "LN2_COR_DI"       -length  0.000000e+00 -e0  1.054177e+00
Bpm        -name "LN2_BPM_DI"       -length  0.000000e+00 -e0  1.054177e+00
Quadrupole -name "LN2_QD_DI_DH"     -length  4.000000e-02 -e0  1.054177e+00 -strength -3.373368e-01
Drift      -name "LN2_DR_DI_QD_ED"  -length  4.250000e-02 -e0  1.054177e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.054177e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.054177e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.054177e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.054177e+00
Drift      -name "LN2_DR_DI_VS"     -length  1.000000e-01 -e0  1.054177e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.054177e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.054177e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.054177e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.054177e+00
Drift      -name "LN2_DR_DI_QD_ED"  -length  4.250000e-02 -e0  1.054177e+00
Quadrupole -name "LN2_QD_DI_FH"     -length  4.000000e-02 -e0  1.054177e+00 -strength  3.373368e-01
Dipole     -name "LN2_COR_DI"       -length  0.000000e+00 -e0  1.054177e+00
Bpm        -name "LN2_BPM_DI"       -length  0.000000e+00 -e0  1.054177e+00
Quadrupole -name "LN2_QD_DI_FH"     -length  4.000000e-02 -e0  1.054177e+00 -strength  3.373368e-01
Drift      -name "LN2_DR_DI_QD_ED"  -length  4.250000e-02 -e0  1.054177e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.054177e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.054177e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.054177e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.054177e+00
Drift      -name "LN2_DR_DI_VS"     -length  1.000000e-01 -e0  1.054177e+00
Sbend      -name "LN2_DP_DI"        -length  1.500000e-01 -e0  1.054177e+00 -angle  0.000000e+00 -E1  0.000000e+00 -E2  0.000000e+00 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  0.000000e+00 -six_dim 1
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.054177e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.054177e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.054177e+00
Drift      -name "LN2_DR_DI_QD_ED"  -length  4.250000e-02 -e0  1.054177e+00
Quadrupole -name "LN2_QD_DI_DH"     -length  4.000000e-02 -e0  1.054177e+00 -strength -3.373368e-01
Dipole     -name "LN2_COR_DI"       -length  0.000000e+00 -e0  1.054177e+00
Bpm        -name "LN2_BPM_DI"       -length  0.000000e+00 -e0  1.054177e+00
Quadrupole -name "LN2_QD_DI_DH"     -length  4.000000e-02 -e0  1.054177e+00 -strength -3.373368e-01
Drift      -name "LN2_DR_DI_QD_ED"  -length  4.250000e-02 -e0  1.054177e+00
Drift      -name "LN2_DR_DI"        -length  2.500000e-01 -e0  1.054177e+00
Drift      -name "LN2_WA_OU_DI2"    -length  0.000000e+00 -e0  1.054177e+00
Drift      -name "LN3_DR_V1"        -length  3.165655e+00 -e0  1.054177e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.054177e+00
Quadrupole -name "LN3_QD_V01"       -length  4.000000e-02 -e0  1.054177e+00 -strength  5.308815e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  1.054177e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  1.054177e+00
Quadrupole -name "LN3_QD_V01"       -length  4.000000e-02 -e0  1.054177e+00 -strength  5.308815e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.054177e+00
Drift      -name "LN3_DR_V2"        -length  5.000026e-02 -e0  1.054177e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.054177e+00
Quadrupole -name "LN3_QD_V02"       -length  4.000000e-02 -e0  1.054177e+00 -strength -3.105949e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  1.054177e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  1.054177e+00
Quadrupole -name "LN3_QD_V02"       -length  4.000000e-02 -e0  1.054177e+00 -strength -3.105949e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.054177e+00
Drift      -name "LN3_DR_V3"        -length  5.318559e-01 -e0  1.054177e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.054177e+00
Quadrupole -name "LN3_QD_V03"       -length  4.000000e-02 -e0  1.054177e+00 -strength -2.407692e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  1.054177e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  1.054177e+00
Quadrupole -name "LN3_QD_V03"       -length  4.000000e-02 -e0  1.054177e+00 -strength -2.407692e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.054177e+00
Drift      -name "LN3_DR_V4"        -length  1.380223e+00 -e0  1.054177e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.054177e+00
Quadrupole -name "LN3_QD_V04"       -length  4.000000e-02 -e0  1.054177e+00 -strength -1.038414e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  1.054177e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  1.054177e+00
Quadrupole -name "LN3_QD_V04"       -length  4.000000e-02 -e0  1.054177e+00 -strength -1.038414e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.054177e+00
Drift      -name "LN3_DR_V5"        -length  2.163240e+00 -e0  1.054177e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.054177e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  1.054177e+00 -strength  1.771018e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  1.054177e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  1.054177e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  1.054177e+00 -strength  1.771018e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.054177e+00
Drift      -name "LN3_DR_SV"        -length  1.000000e-01 -e0  1.054177e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.054177e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.054177e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase -3.500000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.102885e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.102885e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.102885e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.102885e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase -3.500000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.151593e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.151593e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.151593e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.151593e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase -3.500000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.200301e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.200301e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.200301e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.200301e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase -3.500000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.249009e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.249009e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.249009e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.249009e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  1.249009e+00 -strength -2.098334e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  1.249009e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  1.249009e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  1.249009e+00 -strength -2.098334e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.249009e+00
Drift      -name "LN3_DR_VS"        -length  1.000000e-01 -e0  1.249009e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.249009e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.249009e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase -3.500000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.297716e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.297716e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.297716e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.297716e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase -3.500000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.346424e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.346424e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.346424e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.346424e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase -3.500000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.395132e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.395132e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.395132e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.395132e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase -3.500000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.443840e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.443840e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.443840e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.443840e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  1.443840e+00 -strength  2.425651e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  1.443840e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  1.443840e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  1.443840e+00 -strength  2.425651e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.443840e+00
Drift      -name "LN3_DR_SV"        -length  1.000000e-01 -e0  1.443840e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.443840e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.443840e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase -3.500000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.492548e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.492548e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.492548e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.492548e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase -3.500000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.541255e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.541255e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.541255e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.541255e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase -3.500000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.589963e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.589963e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.589963e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.589963e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase -3.500000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.638671e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.638671e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.638671e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.638671e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  1.638671e+00 -strength -2.752967e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  1.638671e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  1.638671e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  1.638671e+00 -strength -2.752967e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.638671e+00
Drift      -name "LN3_DR_VS"        -length  1.000000e-01 -e0  1.638671e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.638671e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.638671e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase -3.500000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.687379e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.687379e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.687379e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.687379e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase -3.500000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.736087e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.736087e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.736087e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.736087e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase -3.500000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.784794e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.784794e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.784794e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.784794e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase -3.500000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.833502e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.833502e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.833502e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.833502e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  1.833502e+00 -strength  3.080284e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  1.833502e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  1.833502e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  1.833502e+00 -strength  3.080284e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.833502e+00
Drift      -name "LN3_DR_SV"        -length  1.000000e-01 -e0  1.833502e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.833502e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.833502e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase -3.500000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.882210e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.882210e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.882210e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.882210e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase -3.500000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.930918e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.930918e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.930918e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.930918e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase -3.500000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.979626e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.979626e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.979626e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.979626e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase -3.500000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.028333e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.028333e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.028333e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  2.028333e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  2.028333e+00 -strength -3.407600e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  2.028333e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  2.028333e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  2.028333e+00 -strength -3.407600e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  2.028333e+00
Drift      -name "LN3_DR_DR"        -length  1.000000e-01 -e0  2.028333e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.028333e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.028333e+00
Drift      -name "LN3_DR_XCA_"      -length  9.164755e-01 -e0  2.028333e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.028333e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.028333e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.028333e+00
Drift      -name "LN3_DR_XCA_"      -length  9.164755e-01 -e0  2.028333e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.028333e+00
Drift      -name "LN3_DR_CT"        -length  1.000000e-01 -e0  2.028333e+00
Drift      -name "LN3_WA_OU2"       -length  0.000000e+00 -e0  2.028333e+00
Drift      -name "SXR_DR_V1"        -length  2.000000e-01 -e0  2.028333e+00
Drift      -name "SXR_DR_QD_ED"     -length  5.200000e-02 -e0  2.028333e+00
Quadrupole -name "SXR_QD_V01"       -length  1.300000e-02 -e0  2.028333e+00 -strength  5.534320e-01
Dipole     -name "SXR_COR"          -length  0.000000e+00 -e0  2.028333e+00
Bpm        -name "SXR_BPM"          -length  0.000000e+00 -e0  2.028333e+00
Quadrupole -name "SXR_QD_V01"       -length  1.300000e-02 -e0  2.028333e+00 -strength  5.534320e-01
Drift      -name "SXR_DR_QD_ED"     -length  5.200000e-02 -e0  2.028333e+00
Drift      -name "SXR_DR_V2"        -length  5.892182e-01 -e0  2.028333e+00
Drift      -name "SXR_DR_QD_ED"     -length  5.200000e-02 -e0  2.028333e+00
Quadrupole -name "SXR_QD_V02"       -length  1.300000e-02 -e0  2.028333e+00 -strength -1.625445e-01
Dipole     -name "SXR_COR"          -length  0.000000e+00 -e0  2.028333e+00
Bpm        -name "SXR_BPM"          -length  0.000000e+00 -e0  2.028333e+00
Quadrupole -name "SXR_QD_V02"       -length  1.300000e-02 -e0  2.028333e+00 -strength -1.625445e-01
Drift      -name "SXR_DR_QD_ED"     -length  5.200000e-02 -e0  2.028333e+00
Drift      -name "SXR_DR_V3"        -length  1.144548e+00 -e0  2.028333e+00
Drift      -name "SXR_DR_QD_ED"     -length  5.200000e-02 -e0  2.028333e+00
Quadrupole -name "SXR_QD_V03"       -length  1.300000e-02 -e0  2.028333e+00 -strength -2.886462e-01
Dipole     -name "SXR_COR"          -length  0.000000e+00 -e0  2.028333e+00
Bpm        -name "SXR_BPM"          -length  0.000000e+00 -e0  2.028333e+00
Quadrupole -name "SXR_QD_V03"       -length  1.300000e-02 -e0  2.028333e+00 -strength -2.886462e-01
Drift      -name "SXR_DR_QD_ED"     -length  5.200000e-02 -e0  2.028333e+00
Drift      -name "SXR_DR_V4"        -length  2.759790e+00 -e0  2.028333e+00
Drift      -name "SXR_DR_CT"        -length  1.000000e-01 -e0  2.028333e+00
Drift      -name "SXR_WA_M_OU2"     -length  0.000000e+00 -e0  2.028333e+00
Drift      -name "SXR_DR_QD_ED"     -length  5.200000e-02 -e0  2.028333e+00
Quadrupole -name "SXR_QD_FH"        -length  1.300000e-02 -e0  2.028333e+00 -strength  3.955250e-01
Dipole     -name "SXR_COR"          -length  0.000000e+00 -e0  2.028333e+00
Bpm        -name "SXR_BPM"          -length  0.000000e+00 -e0  2.028333e+00
Quadrupole -name "SXR_QD_FH"        -length  1.300000e-02 -e0  2.028333e+00 -strength  3.955250e-01
Drift      -name "SXR_DR_QD_ED"     -length  5.200000e-02 -e0  2.028333e+00
Drift      -name "SXR_DR_WIG_ED"    -length  1.950000e-01 -e0  2.028333e+00
Drift      -name "SXR_WIG_0"        -length  1.768000e+00 -e0  2.028333e+00
Drift      -name "SXR_DR_WIG_ED"    -length  1.950000e-01 -e0  2.028333e+00
Drift      -name "SXR_DR_QD_ED"     -length  5.200000e-02 -e0  2.028333e+00
Quadrupole -name "SXR_QD_DH"        -length  1.300000e-02 -e0  2.028333e+00 -strength -3.955250e-01
Dipole     -name "SXR_COR"          -length  0.000000e+00 -e0  2.028333e+00
Bpm        -name "SXR_BPM"          -length  0.000000e+00 -e0  2.028333e+00
Quadrupole -name "SXR_QD_DH"        -length  1.300000e-02 -e0  2.028333e+00 -strength -3.955250e-01
Drift      -name "SXR_DR_QD_ED"     -length  5.200000e-02 -e0  2.028333e+00
Drift      -name "SXR_DR_WIG_ED"    -length  1.950000e-01 -e0  2.028333e+00
Drift      -name "SXR_WIG_0"        -length  1.768000e+00 -e0  2.028333e+00
Drift      -name "SXR_DR_WIG_ED"    -length  1.950000e-01 -e0  2.028333e+00
Drift      -name "SXR_DR_QD_ED"     -length  5.200000e-02 -e0  2.028333e+00
Quadrupole -name "SXR_QD_FH"        -length  1.300000e-02 -e0  2.028333e+00 -strength  3.955250e-01
Drift      -name "SXR_WA_OU2"       -length  0.000000e+00 -e0  2.028333e+00
