Girder
SetReferenceEnergy   1.238601e-01
Drift      -name "LN0_DR_V5"        -length  6.639433e-01 -e0  1.237690e-01
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  1.237690e-01
Quadrupole -name "LN0_QD_V04"       -length  4.000000e-02 -e0  1.237690e-01 -strength  1.779631e-02
Dipole     -name "LN0_COR"          -length  0.000000e+00 -e0  1.237690e-01
Bpm        -name "LN0_BPM"          -length  0.000000e+00 -e0  1.237690e-01
Quadrupole -name "LN0_QD_V04"       -length  4.000000e-02 -e0  1.237690e-01 -strength  1.779631e-02
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  1.237690e-01
Drift      -name "LN0_DR_V6"        -length  2.598929e-01 -e0  1.237690e-01
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  1.237690e-01
Quadrupole -name "LN0_QD_V05"       -length  4.000000e-02 -e0  1.237690e-01 -strength  3.441588e-02
Dipole     -name "LN0_COR"          -length  0.000000e+00 -e0  1.237690e-01
Bpm        -name "LN0_BPM"          -length  0.000000e+00 -e0  1.237690e-01
Quadrupole -name "LN0_QD_V05"       -length  4.000000e-02 -e0  1.237690e-01 -strength  3.441588e-02
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  1.237690e-01
Drift      -name "LN0_DR_V7"        -length  1.419751e+00 -e0  1.237690e-01
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  1.237690e-01
Quadrupole -name "LN0_QD_V06"       -length  4.000000e-02 -e0  1.237690e-01 -strength -3.626929e-02
Dipole     -name "LN0_COR"          -length  0.000000e+00 -e0  1.237690e-01
Bpm        -name "LN0_BPM"          -length  0.000000e+00 -e0  1.237690e-01
Quadrupole -name "LN0_QD_V06"       -length  4.000000e-02 -e0  1.237690e-01 -strength -3.626929e-02
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  1.237690e-01
Drift      -name "LN0_DR_V8"        -length  2.000008e-01 -e0  1.237690e-01
Drift      -name "LN0_DR_VS"        -length  1.000000e-01 -e0  1.237690e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  1.237690e-01
Drift      -name "LN0_DR_XCA_ED_DI" -length  1.001043e-01 -e0  1.237690e-01
CrabCavity -name "LN0_XCA0_DI"      -length  9.997914e-01 -voltage  0.000000e+00 -phase  9.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.237690e-01
Drift      -name "LN0_DR_XCA_ED_DI" -length  1.001043e-01 -e0  1.237285e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  1.237285e-01
Drift      -name "LN0_TDS_DR"       -length  2.500000e-01 -e0  1.237285e-01
Drift      -name "LN0_TDS_DR"       -length  2.500000e-01 -e0  1.237285e-01
Drift      -name "LN0_TDS_DR"       -length  2.500000e-01 -e0  1.237285e-01
Drift      -name "LN0_TDS_DR"       -length  2.500000e-01 -e0  1.237285e-01
Drift      -name "LN0_TDS_DR"       -length  2.500000e-01 -e0  1.237285e-01
Drift      -name "LN0_TDS_DR"       -length  2.500000e-01 -e0  1.237285e-01
Drift      -name "LN0_TDS_DR"       -length  2.500000e-01 -e0  1.237285e-01
Drift      -name "LN0_TDS_DR"       -length  2.500000e-01 -e0  1.237285e-01
Sbend      -name "LN0_DP_TDS"       -length  1.000000e-01 -e0  1.237285e-01 -angle  0.000000e+00 -E1  0.000000e+00 -E2  0.000000e+00 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  0.000000e+00 -six_dim 1
Drift      -name "LN0_DR_20"        -length  2.000000e-01 -e0  1.237285e-01
Drift      -name "LN0_DR_VS"        -length  1.000000e-01 -e0  1.237285e-01
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  1.237285e-01
Quadrupole -name "LN0_QD_FH"        -length  4.000000e-02 -e0  1.237285e-01 -strength  3.464399e-02
Dipole     -name "LN0_COR"          -length  0.000000e+00 -e0  1.237285e-01
Bpm        -name "LN0_BPM"          -length  0.000000e+00 -e0  1.237285e-01
Quadrupole -name "LN0_QD_FH"        -length  4.000000e-02 -e0  1.237285e-01 -strength  3.464399e-02
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  1.237285e-01
Drift      -name "LN0_DR_SV"        -length  1.000000e-01 -e0  1.237285e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  1.237285e-01
Drift      -name "LN0_DR_CCA_ED"    -length  5.019800e-02 -e0  1.237285e-01
Cavity     -name "LN0_CCA0"         -length  1.899604e+00 -gradient  1.500000e-02 -phase  2.600000e+01 -frequency  5.997100e+00
SetReferenceEnergy   1.492816e-01
Drift      -name "LN0_DR_CCA_ED"    -length  5.019800e-02 -e0  1.492816e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  1.492816e-01
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  1.492816e-01
Quadrupole -name "LN0_QD_DH"        -length  4.000000e-02 -e0  1.492816e-01 -strength -4.179885e-02
Dipole     -name "LN0_COR"          -length  0.000000e+00 -e0  1.492816e-01
Bpm        -name "LN0_BPM"          -length  0.000000e+00 -e0  1.492816e-01
Quadrupole -name "LN0_QD_DH"        -length  4.000000e-02 -e0  1.492816e-01 -strength -4.179885e-02
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  1.492816e-01
Drift      -name "LN0_DR_VS"        -length  1.000000e-01 -e0  1.492816e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  1.492816e-01
Drift      -name "LN0_DR_CCA_ED"    -length  5.019800e-02 -e0  1.492816e-01
Cavity     -name "LN0_CCA0"         -length  1.899604e+00 -gradient  1.500000e-02 -phase  2.600000e+01 -frequency  5.997100e+00
SetReferenceEnergy   1.748347e-01
Drift      -name "LN0_DR_CCA_ED"    -length  5.019800e-02 -e0  1.748347e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  1.748347e-01
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  1.748347e-01
Quadrupole -name "LN0_QD_FH"        -length  4.000000e-02 -e0  1.748347e-01 -strength  4.895371e-02
Dipole     -name "LN0_COR"          -length  0.000000e+00 -e0  1.748347e-01
Bpm        -name "LN0_BPM"          -length  0.000000e+00 -e0  1.748347e-01
Quadrupole -name "LN0_QD_FH"        -length  4.000000e-02 -e0  1.748347e-01 -strength  4.895371e-02
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  1.748347e-01
Drift      -name "LN0_DR_VS"        -length  1.000000e-01 -e0  1.748347e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  1.748347e-01
Drift      -name "LN0_DR_CCA_ED"    -length  5.019800e-02 -e0  1.748347e-01
Cavity     -name "LN0_CCA0"         -length  1.899604e+00 -gradient  1.500000e-02 -phase  2.600000e+01 -frequency  5.997100e+00
SetReferenceEnergy   2.003877e-01
Drift      -name "LN0_DR_CCA_ED"    -length  5.019800e-02 -e0  2.003877e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  2.003877e-01
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  2.003877e-01
Quadrupole -name "LN0_QD_DH"        -length  4.000000e-02 -e0  2.003877e-01 -strength -5.610857e-02
Dipole     -name "LN0_COR"          -length  0.000000e+00 -e0  2.003877e-01
Bpm        -name "LN0_BPM"          -length  0.000000e+00 -e0  2.003877e-01
Quadrupole -name "LN0_QD_DH"        -length  4.000000e-02 -e0  2.003877e-01 -strength -5.610857e-02
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  2.003877e-01
Drift      -name "LN0_DR_VS"        -length  1.000000e-01 -e0  2.003877e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  2.003877e-01
Drift      -name "LN0_DR_CCA_ED"    -length  5.019800e-02 -e0  2.003877e-01
Cavity     -name "LN0_CCA0"         -length  1.899604e+00 -gradient  1.500000e-02 -phase  2.600000e+01 -frequency  5.997100e+00
SetReferenceEnergy   2.259408e-01
Drift      -name "LN0_DR_CCA_ED"    -length  5.019800e-02 -e0  2.259408e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  2.259408e-01
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  2.259408e-01
Quadrupole -name "LN0_QD_FH"        -length  4.000000e-02 -e0  2.259408e-01 -strength  6.326343e-02
Dipole     -name "LN0_COR"          -length  0.000000e+00 -e0  2.259408e-01
Bpm        -name "LN0_BPM"          -length  0.000000e+00 -e0  2.259408e-01
Quadrupole -name "LN0_QD_FH"        -length  4.000000e-02 -e0  2.259408e-01 -strength  6.326343e-02
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  2.259408e-01
Drift      -name "LN0_DR_VS"        -length  1.000000e-01 -e0  2.259408e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  2.259408e-01
Drift      -name "LN0_DR_CCA_ED"    -length  5.019800e-02 -e0  2.259408e-01
Cavity     -name "LN0_CCA0"         -length  1.899604e+00 -gradient  1.500000e-02 -phase  2.600000e+01 -frequency  5.997100e+00
SetReferenceEnergy   2.514939e-01
Drift      -name "LN0_DR_CCA_ED"    -length  5.019800e-02 -e0  2.514939e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  2.514939e-01
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  2.514939e-01
Quadrupole -name "LN0_QD_DH"        -length  4.000000e-02 -e0  2.514939e-01 -strength -7.041830e-02
Dipole     -name "LN0_COR"          -length  0.000000e+00 -e0  2.514939e-01
Bpm        -name "LN0_BPM"          -length  0.000000e+00 -e0  2.514939e-01
Quadrupole -name "LN0_QD_DH"        -length  4.000000e-02 -e0  2.514939e-01 -strength -7.041830e-02
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  2.514939e-01
Drift      -name "LN0_DR_VS"        -length  1.000000e-01 -e0  2.514939e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  2.514939e-01
Drift      -name "LN0_DR_CCA_ED"    -length  5.019800e-02 -e0  2.514939e-01
Cavity     -name "LN0_CCA0"         -length  1.899604e+00 -gradient  1.500000e-02 -phase  2.600000e+01 -frequency  5.997100e+00
SetReferenceEnergy   2.770470e-01
Drift      -name "LN0_DR_CCA_ED"    -length  5.019800e-02 -e0  2.770470e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  2.770470e-01
Drift      -name "LN0_WA_LNZ_IN2"   -length  0.000000e+00 -e0  2.770470e-01
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  2.770470e-01
Quadrupole -name "LN0_QD_FH"        -length  4.000000e-02 -e0  2.770470e-01 -strength  7.757317e-02
Dipole     -name "LN0_COR"          -length  0.000000e+00 -e0  2.770470e-01
Bpm        -name "LN0_BPM"          -length  0.000000e+00 -e0  2.770470e-01
Quadrupole -name "LN0_QD_FH"        -length  4.000000e-02 -e0  2.770470e-01 -strength  7.757317e-02
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  2.770470e-01
Drift      -name "LN0_DR_VS"        -length  1.000000e-01 -e0  2.770470e-01
Drift      -name "LN0_DR_LNZ_ED2"   -length  6.250000e-01 -e0  2.770470e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  2.770470e-01
Drift      -name "LN0_DR_KCA_ED"    -length  2.225410e-02 -e0  2.770470e-01
Cavity     -name "LN0_KCA0"         -length  3.054918e-01 -gradient  2.453617e-02 -phase  1.920000e+02 -frequency  3.598260e+01
SetReferenceEnergy   2.698730e-01
Drift      -name "LN0_DR_KCA_ED"    -length  2.225410e-02 -e0  2.698730e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  2.698730e-01
Drift      -name "LN0_DR_KCA_ED"    -length  2.225410e-02 -e0  2.698730e-01
Drift      -name "LN0_DR_KCA0"      -length  3.054918e-01 -e0  2.698730e-01
Drift      -name "LN0_DR_KCA_ED"    -length  2.225410e-02 -e0  2.698730e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  2.698730e-01
Drift      -name "LN0_DR_LNZ_ED2"   -length  6.250000e-01 -e0  2.698730e-01
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  2.698730e-01
Quadrupole -name "LN0_QD_DH"        -length  4.000000e-02 -e0  2.698730e-01 -strength -7.556445e-02
Dipole     -name "LN0_COR"          -length  0.000000e+00 -e0  2.698730e-01
Bpm        -name "LN0_BPM"          -length  0.000000e+00 -e0  2.698730e-01
Quadrupole -name "LN0_QD_DH"        -length  4.000000e-02 -e0  2.698730e-01 -strength -7.556445e-02
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  2.698730e-01
Drift      -name "LN0_DR_DR"        -length  1.000000e-01 -e0  2.698730e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  2.698730e-01
Drift      -name "LN0_DR_CCA_ED"    -length  5.019800e-02 -e0  2.698730e-01
Drift      -name "LN0_DR_CCA0H"     -length  3.498020e-01 -e0  2.698730e-01
Drift      -name "LN0_DR_A"         -length  2.000000e-01 -e0  2.698730e-01
Drift      -name "LN0_DR_VS"        -length  1.000000e-01 -e0  2.698730e-01
Drift      -name "LN0_DR_A"         -length  2.000000e-01 -e0  2.698730e-01
Drift      -name "LN0_DR_CT"        -length  1.000000e-01 -e0  2.698730e-01
Drift      -name "LN0_WA_OU2"       -length  0.000000e+00 -e0  2.698730e-01
Drift      -name "BC1_DR_V1"        -length  5.382067e-01 -e0  2.698730e-01
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.698730e-01
Quadrupole -name "BC1_QD_V01"       -length  4.000000e-02 -e0  2.698730e-01 -strength  2.054140e-02
Dipole     -name "BC1_COR"          -length  0.000000e+00 -e0  2.698730e-01
Bpm        -name "BC1_BPM"          -length  0.000000e+00 -e0  2.698730e-01
Quadrupole -name "BC1_QD_V01"       -length  4.000000e-02 -e0  2.698730e-01 -strength  2.054140e-02
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.698730e-01
Drift      -name "BC1_DR_V2"        -length  5.900340e+00 -e0  2.698730e-01
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.698730e-01
Quadrupole -name "BC1_QD_V02"       -length  4.000000e-02 -e0  2.698730e-01 -strength -9.874167e-02
Dipole     -name "BC1_COR"          -length  0.000000e+00 -e0  2.698730e-01
Bpm        -name "BC1_BPM"          -length  0.000000e+00 -e0  2.698730e-01
Quadrupole -name "BC1_QD_V02"       -length  4.000000e-02 -e0  2.698730e-01 -strength -9.874167e-02
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.698730e-01
Drift      -name "BC1_DR_V3"        -length  2.496128e-01 -e0  2.698730e-01
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.698730e-01
Quadrupole -name "BC1_QD_V03"       -length  4.000000e-02 -e0  2.698730e-01 -strength  3.530922e-02
Dipole     -name "BC1_COR"          -length  0.000000e+00 -e0  2.698730e-01
Bpm        -name "BC1_BPM"          -length  0.000000e+00 -e0  2.698730e-01
Quadrupole -name "BC1_QD_V03"       -length  4.000000e-02 -e0  2.698730e-01 -strength  3.530922e-02
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.698730e-01
Drift      -name "BC1_DR_V4"        -length  2.000000e-01 -e0  2.698730e-01
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.698730e-01
Quadrupole -name "BC1_QD_V04"       -length  4.000000e-02 -e0  2.698730e-01 -strength  5.701369e-02
Dipole     -name "BC1_COR"          -length  0.000000e+00 -e0  2.698730e-01
Bpm        -name "BC1_BPM"          -length  0.000000e+00 -e0  2.698730e-01
Quadrupole -name "BC1_QD_V04"       -length  4.000000e-02 -e0  2.698730e-01 -strength  5.701369e-02
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.698730e-01
Drift      -name "BC1_DR_V5"        -length  2.002040e-01 -e0  2.698730e-01
Drift      -name "BC1_DR_20"        -length  5.000000e-01 -e0  2.698730e-01
Sbend      -name "BC1_DP_DIP1"      -length  4.002981e-01 -e0  2.698729e-01 -angle -6.684611e-02 -E1  0.000000e+00 -E2 -6.684611e-02 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  0.000000e+00  -six_dim 1 -csr 1 
Drift      -name "BC1_DR_SIDE_C"    -length  5.000000e-01 -e0  2.698720e-01
Drift      -name "BC1_DR_SIDE"      -length  2.757275e+00 -e0  2.698720e-01
Sbend      -name "BC1_DP_DIP2"      -length  4.002981e-01 -e0  2.698716e-01 -angle  6.684611e-02 -E1  6.684611e-02 -E2  0.000000e+00 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  0.000000e+00  -six_dim 1 -csr 1 
Drift      -name "BC1_DR_CENT"      -length  3.500000e-01 -e0  2.698716e-01
Bpm        -name "BC1_BPM"          -length  0.000000e+00 -e0  2.698716e-01
Drift      -name "BC1_DR_CENT"      -length  3.500000e-01 -e0  2.698716e-01
Sbend      -name "BC1_DP_DIP3"      -length  4.002981e-01 -e0  2.698622e-01 -angle  6.684611e-02 -E1  0.000000e+00 -E2  6.684611e-02 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  0.000000e+00  -six_dim 1 -csr 1 
Drift      -name "BC1_DR_SIDE_C"    -length  5.000000e-01 -e0  2.698361e-01
Drift      -name "BC1_DR_SIDE"      -length  2.757275e+00 -e0  2.698361e-01
Sbend      -name "BC1_DP_DIP4"      -length  4.002981e-01 -e0  2.698120e-01 -angle -6.684611e-02 -E1 -6.684611e-02 -E2  0.000000e+00 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  0.000000e+00  -six_dim 1 -csr 1 
Drift      -name "BC1_DR_20_C"      -length  5.000000e-01 -e0  2.697895e-01
Drift      -name "BC1_WA_OU2"       -length  0.000000e+00 -e0  2.697895e-01
Drift      -name "BC1_DR_CT"        -length  1.000000e-01 -e0  2.697895e-01
Drift      -name "BC1_DR_DI_V1"     -length  2.911672e-01 -e0  2.697895e-01
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.697895e-01
Quadrupole -name "BC1_QD_DI_V01"    -length  4.000000e-02 -e0  2.697895e-01 -strength -9.254272e-02
Dipole     -name "BC1_COR_DI"       -length  0.000000e+00 -e0  2.697895e-01
Bpm        -name "BC1_BPM_DI"       -length  0.000000e+00 -e0  2.697895e-01
Quadrupole -name "BC1_QD_DI_V01"    -length  4.000000e-02 -e0  2.697895e-01 -strength -9.254272e-02
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.697895e-01
Drift      -name "BC1_DR_DI_V2"     -length  4.700020e-01 -e0  2.697895e-01
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.697895e-01
Quadrupole -name "BC1_QD_DI_V02"    -length  4.000000e-02 -e0  2.697895e-01 -strength  1.451457e-01
Dipole     -name "BC1_COR_DI"       -length  0.000000e+00 -e0  2.697895e-01
Bpm        -name "BC1_BPM_DI"       -length  0.000000e+00 -e0  2.697895e-01
Quadrupole -name "BC1_QD_DI_V02"    -length  4.000000e-02 -e0  2.697895e-01 -strength  1.451457e-01
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.697895e-01
Drift      -name "BC1_DR_DI_V3"     -length  1.199083e+00 -e0  2.697895e-01
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.697895e-01
Quadrupole -name "BC1_QD_DI_V03"    -length  4.000000e-02 -e0  2.697895e-01 -strength -5.534839e-02
Dipole     -name "BC1_COR_DI"       -length  0.000000e+00 -e0  2.697895e-01
Bpm        -name "BC1_BPM_DI"       -length  0.000000e+00 -e0  2.697895e-01
Quadrupole -name "BC1_QD_DI_V03"    -length  4.000000e-02 -e0  2.697895e-01 -strength -5.534839e-02
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.697895e-01
Drift      -name "BC1_DR_DI_V5"     -length  2.000000e-01 -e0  2.697895e-01
Drift      -name "BC1_DR_DI"        -length  2.500000e-01 -e0  2.697895e-01
Drift      -name "BC1_DR_VS"        -length  1.000000e-01 -e0  2.697895e-01
Drift      -name "BC1_DR_BL"        -length  5.000000e-02 -e0  2.697895e-01
Drift      -name "BC1_DR_XCA_ED_DI" -length  1.001043e-01 -e0  2.697895e-01
CrabCavity -name "BC1_XCA0_DI"      -length  9.997914e-01 -voltage  0.000000e+00 -phase  9.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.697895e-01
Drift      -name "BC1_DR_XCA_ED_DI" -length  1.001043e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_BL"        -length  5.000000e-02 -e0  2.697215e-01
Drift      -name "BC1_DR_VS"        -length  1.000000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_DI"        -length  2.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_DI"        -length  2.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_DI_QD_ED"  -length  4.250000e-02 -e0  2.697215e-01
Quadrupole -name "BC1_QD_DI_FH"     -length  4.000000e-02 -e0  2.697215e-01 -strength  8.631089e-02
Dipole     -name "BC1_COR_DI"       -length  0.000000e+00 -e0  2.697215e-01
Bpm        -name "BC1_BPM_DI"       -length  0.000000e+00 -e0  2.697215e-01
Quadrupole -name "BC1_QD_DI_FH"     -length  4.000000e-02 -e0  2.697215e-01 -strength  8.631089e-02
Drift      -name "BC1_DR_DI_QD_ED"  -length  4.250000e-02 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_DI_VS"     -length  1.000000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_DI_QD_ED"  -length  4.250000e-02 -e0  2.697215e-01
Quadrupole -name "BC1_QD_DI_DH"     -length  4.000000e-02 -e0  2.697215e-01 -strength -8.631089e-02
Dipole     -name "BC1_COR_DI"       -length  0.000000e+00 -e0  2.697215e-01
Bpm        -name "BC1_BPM_DI"       -length  0.000000e+00 -e0  2.697215e-01
Quadrupole -name "BC1_QD_DI_DH"     -length  4.000000e-02 -e0  2.697215e-01 -strength -8.631089e-02
Drift      -name "BC1_DR_DI_QD_ED"  -length  4.250000e-02 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_DI_VS"     -length  1.000000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_DI_QD_ED"  -length  4.250000e-02 -e0  2.697215e-01
Quadrupole -name "BC1_QD_DI_FH"     -length  4.000000e-02 -e0  2.697215e-01 -strength  8.631089e-02
Dipole     -name "BC1_COR_DI"       -length  0.000000e+00 -e0  2.697215e-01
Bpm        -name "BC1_BPM_DI"       -length  0.000000e+00 -e0  2.697215e-01
Quadrupole -name "BC1_QD_DI_FH"     -length  4.000000e-02 -e0  2.697215e-01 -strength  8.631089e-02
Drift      -name "BC1_DR_DI_QD_ED"  -length  4.250000e-02 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_DI_VS"     -length  1.000000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_DI_QD_ED"  -length  4.250000e-02 -e0  2.697215e-01
Quadrupole -name "BC1_QD_DI_DH"     -length  4.000000e-02 -e0  2.697215e-01 -strength -8.631089e-02
Dipole     -name "BC1_COR_DI"       -length  0.000000e+00 -e0  2.697215e-01
Bpm        -name "BC1_BPM_DI"       -length  0.000000e+00 -e0  2.697215e-01
Quadrupole -name "BC1_QD_DI_DH"     -length  4.000000e-02 -e0  2.697215e-01 -strength -8.631089e-02
Drift      -name "BC1_DR_DI_QD_ED"  -length  4.250000e-02 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_DI_VS"     -length  1.000000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_DI_QD_ED"  -length  4.250000e-02 -e0  2.697215e-01
Quadrupole -name "BC1_QD_DI_FH"     -length  4.000000e-02 -e0  2.697215e-01 -strength  8.631089e-02
Dipole     -name "BC1_COR_DI"       -length  0.000000e+00 -e0  2.697215e-01
Bpm        -name "BC1_BPM_DI"       -length  0.000000e+00 -e0  2.697215e-01
Quadrupole -name "BC1_QD_DI_FH"     -length  4.000000e-02 -e0  2.697215e-01 -strength  8.631089e-02
Drift      -name "BC1_DR_DI_QD_ED"  -length  4.250000e-02 -e0  2.697215e-01
Drift      -name "BC1_DR_DI"        -length  2.500000e-01 -e0  2.697215e-01
Sbend      -name "BC1_DP_DI"        -length  2.500000e-01 -e0  2.697215e-01 -angle  0.000000e+00 -E1  0.000000e+00 -E2  0.000000e+00 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  0.000000e+00 -six_dim 1
Drift      -name "BC1_DR_DI"        -length  2.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_DI"        -length  2.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_DI"        -length  2.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_WA_OU_DI2"    -length  0.000000e+00 -e0  2.697215e-01
Drift      -name "LN1_DR_V1"        -length  2.005300e-01 -e0  2.697215e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  2.697215e-01
Quadrupole -name "LN1_QD_V01"       -length  4.000000e-02 -e0  2.697215e-01 -strength  7.032831e-02
Dipole     -name "LN1_COR"          -length  0.000000e+00 -e0  2.697215e-01
Bpm        -name "LN1_BPM"          -length  0.000000e+00 -e0  2.697215e-01
Quadrupole -name "LN1_QD_V01"       -length  4.000000e-02 -e0  2.697215e-01 -strength  7.032831e-02
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  2.697215e-01
Drift      -name "LN1_DR_V2"        -length  2.192504e-01 -e0  2.697215e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  2.697215e-01
Quadrupole -name "LN1_QD_V02"       -length  4.000000e-02 -e0  2.697215e-01 -strength -1.256105e-01
Dipole     -name "LN1_COR"          -length  0.000000e+00 -e0  2.697215e-01
Bpm        -name "LN1_BPM"          -length  0.000000e+00 -e0  2.697215e-01
Quadrupole -name "LN1_QD_V02"       -length  4.000000e-02 -e0  2.697215e-01 -strength -1.256105e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  2.697215e-01
Drift      -name "LN1_DR_V3"        -length  1.914618e+00 -e0  2.697215e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  2.697215e-01
Quadrupole -name "LN1_QD_V03"       -length  4.000000e-02 -e0  2.697215e-01 -strength  1.347152e-02
Dipole     -name "LN1_COR"          -length  0.000000e+00 -e0  2.697215e-01
Bpm        -name "LN1_BPM"          -length  0.000000e+00 -e0  2.697215e-01
Quadrupole -name "LN1_QD_V03"       -length  4.000000e-02 -e0  2.697215e-01 -strength  1.347152e-02
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  2.697215e-01
Drift      -name "LN1_DR_V4"        -length  2.000000e-01 -e0  2.697215e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  2.697215e-01
Quadrupole -name "LN1_QD_FH"        -length  4.000000e-02 -e0  2.697215e-01 -strength  7.120648e-02
Dipole     -name "LN1_COR"          -length  0.000000e+00 -e0  2.697215e-01
Bpm        -name "LN1_BPM"          -length  0.000000e+00 -e0  2.697215e-01
Quadrupole -name "LN1_QD_FH"        -length  4.000000e-02 -e0  2.697215e-01 -strength  7.120648e-02
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  2.697215e-01
Drift      -name "LN1_DR_SV"        -length  1.000000e-01 -e0  2.697215e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  2.697215e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  2.697215e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  3.500000e+01 -frequency  1.199420e+01
SetReferenceEnergy   3.184374e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  3.184374e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  3.184374e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  3.184374e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  3.500000e+01 -frequency  1.199420e+01
SetReferenceEnergy   3.671532e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  3.671532e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  3.671532e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  3.671532e-01
Quadrupole -name "LN1_QD_DH"        -length  4.000000e-02 -e0  3.671532e-01 -strength -9.692845e-02
Dipole     -name "LN1_COR"          -length  0.000000e+00 -e0  3.671532e-01
Bpm        -name "LN1_BPM"          -length  0.000000e+00 -e0  3.671532e-01
Quadrupole -name "LN1_QD_DH"        -length  4.000000e-02 -e0  3.671532e-01 -strength -9.692845e-02
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  3.671532e-01
Drift      -name "LN1_DR_VS"        -length  1.000000e-01 -e0  3.671532e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  3.671532e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  3.671532e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  3.500000e+01 -frequency  1.199420e+01
SetReferenceEnergy   4.158691e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  4.158691e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  4.158691e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  4.158691e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  3.500000e+01 -frequency  1.199420e+01
SetReferenceEnergy   4.645849e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  4.645849e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  4.645849e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  4.645849e-01
Quadrupole -name "LN1_QD_FH"        -length  4.000000e-02 -e0  4.645849e-01 -strength  1.226504e-01
Dipole     -name "LN1_COR"          -length  0.000000e+00 -e0  4.645849e-01
Bpm        -name "LN1_BPM"          -length  0.000000e+00 -e0  4.645849e-01
Quadrupole -name "LN1_QD_FH"        -length  4.000000e-02 -e0  4.645849e-01 -strength  1.226504e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  4.645849e-01
Drift      -name "LN1_DR_SV"        -length  1.000000e-01 -e0  4.645849e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  4.645849e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  4.645849e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  3.500000e+01 -frequency  1.199420e+01
SetReferenceEnergy   5.133008e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  5.133008e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  5.133008e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  5.133008e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  3.500000e+01 -frequency  1.199420e+01
SetReferenceEnergy   5.620166e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  5.620166e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  5.620166e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  5.620166e-01
Quadrupole -name "LN1_QD_DH"        -length  4.000000e-02 -e0  5.620166e-01 -strength -1.483724e-01
Dipole     -name "LN1_COR"          -length  0.000000e+00 -e0  5.620166e-01
Bpm        -name "LN1_BPM"          -length  0.000000e+00 -e0  5.620166e-01
Quadrupole -name "LN1_QD_DH"        -length  4.000000e-02 -e0  5.620166e-01 -strength -1.483724e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  5.620166e-01
Drift      -name "LN1_DR_VS"        -length  1.000000e-01 -e0  5.620166e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  5.620166e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  5.620166e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  3.500000e+01 -frequency  1.199420e+01
SetReferenceEnergy   6.107324e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  6.107324e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  6.107324e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  6.107324e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  3.500000e+01 -frequency  1.199420e+01
SetReferenceEnergy   6.594483e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  6.594483e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  6.594483e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  6.594483e-01
Quadrupole -name "LN1_QD_FH"        -length  4.000000e-02 -e0  6.594483e-01 -strength  1.740943e-01
Dipole     -name "LN1_COR"          -length  0.000000e+00 -e0  6.594483e-01
Bpm        -name "LN1_BPM"          -length  0.000000e+00 -e0  6.594483e-01
Quadrupole -name "LN1_QD_FH"        -length  4.000000e-02 -e0  6.594483e-01 -strength  1.740943e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  6.594483e-01
Drift      -name "LN1_DR_SV"        -length  1.000000e-01 -e0  6.594483e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  6.594483e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  6.594483e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  3.500000e+01 -frequency  1.199420e+01
SetReferenceEnergy   7.081641e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  7.081641e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  7.081641e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  7.081641e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  3.500000e+01 -frequency  1.199420e+01
SetReferenceEnergy   7.568799e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  7.568799e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  7.568799e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  7.568799e-01
Quadrupole -name "LN1_QD_DH"        -length  4.000000e-02 -e0  7.568799e-01 -strength -1.998163e-01
Dipole     -name "LN1_COR"          -length  0.000000e+00 -e0  7.568799e-01
Bpm        -name "LN1_BPM"          -length  0.000000e+00 -e0  7.568799e-01
Quadrupole -name "LN1_QD_DH"        -length  4.000000e-02 -e0  7.568799e-01 -strength -1.998163e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  7.568799e-01
Drift      -name "LN1_DR_VS"        -length  1.000000e-01 -e0  7.568799e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  7.568799e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  7.568799e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  3.500000e+01 -frequency  1.199420e+01
SetReferenceEnergy   8.055958e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  8.055958e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  8.055958e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  8.055958e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  3.500000e+01 -frequency  1.199420e+01
SetReferenceEnergy   8.543116e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  8.543116e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  8.543116e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  8.543116e-01
Quadrupole -name "LN1_QD_FH"        -length  4.000000e-02 -e0  8.543116e-01 -strength  2.255383e-01
Dipole     -name "LN1_COR"          -length  0.000000e+00 -e0  8.543116e-01
Bpm        -name "LN1_BPM"          -length  0.000000e+00 -e0  8.543116e-01
Quadrupole -name "LN1_QD_FH"        -length  4.000000e-02 -e0  8.543116e-01 -strength  2.255383e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  8.543116e-01
Drift      -name "LN1_DR_SV"        -length  1.000000e-01 -e0  8.543116e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  8.543116e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  8.543116e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  3.500000e+01 -frequency  1.199420e+01
SetReferenceEnergy   9.030274e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  9.030274e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  9.030274e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  9.030274e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  3.500000e+01 -frequency  1.199420e+01
SetReferenceEnergy   9.517433e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  9.517433e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  9.517433e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  9.517433e-01
Quadrupole -name "LN1_QD_DH"        -length  4.000000e-02 -e0  9.517433e-01 -strength -2.512602e-01
Dipole     -name "LN1_COR"          -length  0.000000e+00 -e0  9.517433e-01
Bpm        -name "LN1_BPM"          -length  0.000000e+00 -e0  9.517433e-01
Quadrupole -name "LN1_QD_DH"        -length  4.000000e-02 -e0  9.517433e-01 -strength -2.512602e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  9.517433e-01
Drift      -name "LN1_DR_VS"        -length  1.000000e-01 -e0  9.517433e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  9.517433e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  9.517433e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  3.500000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.000459e+00
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  1.000459e+00
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  1.000459e+00
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  1.000459e+00
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  3.500000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.049175e+00
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  1.049175e+00
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  1.049175e+00
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  1.049175e+00
Quadrupole -name "LN1_QD_FH"        -length  4.000000e-02 -e0  1.049175e+00 -strength  2.769822e-01
Dipole     -name "LN1_COR"          -length  0.000000e+00 -e0  1.049175e+00
Bpm        -name "LN1_BPM"          -length  0.000000e+00 -e0  1.049175e+00
Quadrupole -name "LN1_QD_FH"        -length  4.000000e-02 -e0  1.049175e+00 -strength  2.769822e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  1.049175e+00
Drift      -name "LN1_DR_DR"        -length  1.000000e-01 -e0  1.049175e+00
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  1.049175e+00
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  1.049175e+00
Drift      -name "LN1_DR_XCA_"      -length  9.164755e-01 -e0  1.049175e+00
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  1.049175e+00
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  1.049175e+00
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  1.049175e+00
Drift      -name "LN1_DR_XCA_"      -length  9.164755e-01 -e0  1.049175e+00
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  1.049175e+00
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  1.049175e+00
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  1.049175e+00
Quadrupole -name "LN1_QD_DH"        -length  4.000000e-02 -e0  1.049175e+00 -strength -2.769822e-01
Dipole     -name "LN1_COR"          -length  0.000000e+00 -e0  1.049175e+00
Bpm        -name "LN1_BPM"          -length  0.000000e+00 -e0  1.049175e+00
Quadrupole -name "LN1_QD_DH"        -length  4.000000e-02 -e0  1.049175e+00 -strength -2.769822e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  1.049175e+00
Drift      -name "LN1_DR_DR"        -length  1.000000e-01 -e0  1.049175e+00
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  1.049175e+00
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  1.049175e+00
Drift      -name "LN1_DR_XCA_A"     -length  8.164755e-01 -e0  1.049175e+00
Drift      -name "LN1_DR_VS"        -length  1.000000e-01 -e0  1.049175e+00
Drift      -name "LN1_WA_OU2"       -length  0.000000e+00 -e0  1.049175e+00
Drift      -name "BC2_DR_V1"        -length  4.718667e-01 -e0  1.049175e+00
Drift      -name "BC2_DR_QD_ED"     -length  4.250000e-02 -e0  1.049175e+00
Quadrupole -name "BC2_QD_V01"       -length  4.000000e-02 -e0  1.049175e+00 -strength  1.584651e-02
Dipole     -name "BC2_COR"          -length  0.000000e+00 -e0  1.049175e+00
Bpm        -name "BC2_BPM"          -length  0.000000e+00 -e0  1.049175e+00
Quadrupole -name "BC2_QD_V01"       -length  4.000000e-02 -e0  1.049175e+00 -strength  1.584651e-02
Drift      -name "BC2_DR_QD_ED"     -length  4.250000e-02 -e0  1.049175e+00
Drift      -name "BC2_DR_V2"        -length  5.872423e+00 -e0  1.049175e+00
Drift      -name "BC2_DR_QD_ED"     -length  4.250000e-02 -e0  1.049175e+00
Quadrupole -name "BC2_QD_V02"       -length  4.000000e-02 -e0  1.049175e+00 -strength -4.874093e-01
Dipole     -name "BC2_COR"          -length  0.000000e+00 -e0  1.049175e+00
Bpm        -name "BC2_BPM"          -length  0.000000e+00 -e0  1.049175e+00
Quadrupole -name "BC2_QD_V02"       -length  4.000000e-02 -e0  1.049175e+00 -strength -4.874093e-01
Drift      -name "BC2_DR_QD_ED"     -length  4.250000e-02 -e0  1.049175e+00
Drift      -name "BC2_DR_V3"        -length  2.000000e-01 -e0  1.049175e+00
Drift      -name "BC2_DR_QD_ED"     -length  4.250000e-02 -e0  1.049175e+00
Quadrupole -name "BC2_QD_V03"       -length  4.000000e-02 -e0  1.049175e+00 -strength  4.519432e-01
Dipole     -name "BC2_COR"          -length  0.000000e+00 -e0  1.049175e+00
Bpm        -name "BC2_BPM"          -length  0.000000e+00 -e0  1.049175e+00
Quadrupole -name "BC2_QD_V03"       -length  4.000000e-02 -e0  1.049175e+00 -strength  4.519432e-01
Drift      -name "BC2_DR_QD_ED"     -length  4.250000e-02 -e0  1.049175e+00
Drift      -name "BC2_DR_V4"        -length  2.000000e-01 -e0  1.049175e+00
Drift      -name "BC2_DR_20"        -length  5.000000e-01 -e0  1.049175e+00
Sbend      -name "BC2_DP_DIP1"      -length  6.000576e-01 -e0  1.049169e+00 -angle -2.399828e-02 -E1  0.000000e+00 -E2 -2.399828e-02 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  0.000000e+00  -six_dim 1 -csr 1 
Drift      -name "BC2_DR_SIDE_C"    -length  5.000000e-01 -e0  1.049155e+00
Drift      -name "BC2_DR_SIDE"      -length  3.201066e+00 -e0  1.049155e+00
Sbend      -name "BC2_DP_DIP2"      -length  6.000576e-01 -e0  1.049137e+00 -angle  2.399828e-02 -E1  2.399828e-02 -E2  0.000000e+00 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  0.000000e+00  -six_dim 1 -csr 1 
Drift      -name "BC2_DR_CENT_C"    -length  2.500000e-01 -e0  1.049119e+00
Bpm        -name "BC2_BPM"          -length  0.000000e+00 -e0  1.049119e+00
Drift      -name "BC2_DR_CENT"      -length  2.500000e-01 -e0  1.049119e+00
Sbend      -name "BC2_DP_DIP3"      -length  6.000576e-01 -e0  1.048885e+00 -angle  2.399828e-02 -E1  0.000000e+00 -E2  2.399828e-02 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  0.000000e+00  -six_dim 1 -csr 1 
Drift      -name "BC2_DR_SIDE_C"    -length  5.000000e-01 -e0  1.048477e+00
Drift      -name "BC2_DR_SIDE"      -length  3.201066e+00 -e0  1.048477e+00
Sbend      -name "BC2_DP_DIP4"      -length  6.000576e-01 -e0  1.047822e+00 -angle -2.399828e-02 -E1 -2.399828e-02 -E2  0.000000e+00 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  0.000000e+00  -six_dim 1 -csr 1 
Drift      -name "BC2_DR_20_C"      -length  5.000000e-01 -e0  1.047497e+00
Drift      -name "BC2_WA_OU2"       -length  0.000000e+00 -e0  1.047497e+00
Drift      -name "LN2_DR_CT"        -length  1.000000e-01 -e0  1.047497e+00
Drift      -name "LN2_DR_V1"        -length  2.294830e+00 -e0  1.047497e+00
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.047497e+00
Quadrupole -name "LN2_QD_V01"       -length  4.000000e-02 -e0  1.047497e+00 -strength  5.722700e-01
Dipole     -name "LN2_COR"          -length  0.000000e+00 -e0  1.047497e+00
Bpm        -name "LN2_BPM"          -length  0.000000e+00 -e0  1.047497e+00
Quadrupole -name "LN2_QD_V01"       -length  4.000000e-02 -e0  1.047497e+00 -strength  5.722700e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.047497e+00
Drift      -name "LN2_DR_V2"        -length  5.090891e-01 -e0  1.047497e+00
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.047497e+00
Quadrupole -name "LN2_QD_V02"       -length  4.000000e-02 -e0  1.047497e+00 -strength -5.485761e-01
Dipole     -name "LN2_COR"          -length  0.000000e+00 -e0  1.047497e+00
Bpm        -name "LN2_BPM"          -length  0.000000e+00 -e0  1.047497e+00
Quadrupole -name "LN2_QD_V02"       -length  4.000000e-02 -e0  1.047497e+00 -strength -5.485761e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.047497e+00
Drift      -name "LN2_DR_V3"        -length  1.490464e+00 -e0  1.047497e+00
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.047497e+00
Quadrupole -name "LN2_QD_V03"       -length  4.000000e-02 -e0  1.047497e+00 -strength  3.774752e-01
Dipole     -name "LN2_COR"          -length  0.000000e+00 -e0  1.047497e+00
Bpm        -name "LN2_BPM"          -length  0.000000e+00 -e0  1.047497e+00
Quadrupole -name "LN2_QD_V03"       -length  4.000000e-02 -e0  1.047497e+00 -strength  3.774752e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.047497e+00
Drift      -name "LN2_DR_V4"        -length  2.999939e+00 -e0  1.047497e+00
Drift      -name "LN2_DR_VS"        -length  1.000000e-01 -e0  1.047497e+00
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.047497e+00
Quadrupole -name "LN2_QD_FH"        -length  4.000000e-02 -e0  1.047497e+00 -strength  2.807292e-01
Dipole     -name "LN2_COR"          -length  0.000000e+00 -e0  1.047497e+00
Bpm        -name "LN2_BPM"          -length  0.000000e+00 -e0  1.047497e+00
Quadrupole -name "LN2_QD_FH"        -length  4.000000e-02 -e0  1.047497e+00 -strength  2.807292e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.047497e+00
Drift      -name "LN2_DR_SV"        -length  1.000000e-01 -e0  1.047497e+00
Drift      -name "LN2_DR_BL"        -length  5.000000e-02 -e0  1.047497e+00
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  1.047497e+00
Cavity     -name "LN2_XCA0"         -length  9.164755e-01 -gradient  2.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.069964e+00
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  1.069964e+00
Drift      -name "LN2_DR_BL"        -length  5.000000e-02 -e0  1.069964e+00
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  1.069964e+00
Cavity     -name "LN2_XCA0"         -length  9.164755e-01 -gradient  2.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.092431e+00
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  1.092431e+00
Drift      -name "LN2_DR_BL"        -length  5.000000e-02 -e0  1.092431e+00
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.092431e+00
Quadrupole -name "LN2_QD_DH"        -length  4.000000e-02 -e0  1.092431e+00 -strength -2.927715e-01
Dipole     -name "LN2_COR"          -length  0.000000e+00 -e0  1.092431e+00
Bpm        -name "LN2_BPM"          -length  0.000000e+00 -e0  1.092431e+00
Quadrupole -name "LN2_QD_DH"        -length  4.000000e-02 -e0  1.092431e+00 -strength -2.927715e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.092431e+00
Drift      -name "LN2_DR_VS"        -length  1.000000e-01 -e0  1.092431e+00
Drift      -name "LN2_DR_BL"        -length  5.000000e-02 -e0  1.092431e+00
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  1.092431e+00
Cavity     -name "LN2_XCA0"         -length  9.164755e-01 -gradient  2.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.114898e+00
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  1.114898e+00
Drift      -name "LN2_DR_BL"        -length  5.000000e-02 -e0  1.114898e+00
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  1.114898e+00
Cavity     -name "LN2_XCA0"         -length  9.164755e-01 -gradient  2.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.137365e+00
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  1.137365e+00
Drift      -name "LN2_DR_BL"        -length  5.000000e-02 -e0  1.137365e+00
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.137365e+00
Quadrupole -name "LN2_QD_FH"        -length  4.000000e-02 -e0  1.137365e+00 -strength  3.048138e-01
Dipole     -name "LN2_COR"          -length  0.000000e+00 -e0  1.137365e+00
Bpm        -name "LN2_BPM"          -length  0.000000e+00 -e0  1.137365e+00
Quadrupole -name "LN2_QD_FH"        -length  4.000000e-02 -e0  1.137365e+00 -strength  3.048138e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.137365e+00
Drift      -name "LN2_DR_SV"        -length  1.000000e-01 -e0  1.137365e+00
Drift      -name "LN2_DR_BL"        -length  5.000000e-02 -e0  1.137365e+00
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  1.137365e+00
Cavity     -name "LN2_XCA0"         -length  9.164755e-01 -gradient  2.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.159832e+00
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  1.159832e+00
Drift      -name "LN2_DR_BL"        -length  5.000000e-02 -e0  1.159832e+00
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  1.159832e+00
Cavity     -name "LN2_XCA0"         -length  9.164755e-01 -gradient  2.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.182299e+00
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  1.182299e+00
Drift      -name "LN2_DR_BL"        -length  5.000000e-02 -e0  1.182299e+00
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.182299e+00
Quadrupole -name "LN2_QD_DH"        -length  4.000000e-02 -e0  1.182299e+00 -strength -3.168562e-01
Dipole     -name "LN2_COR"          -length  0.000000e+00 -e0  1.182299e+00
Bpm        -name "LN2_BPM"          -length  0.000000e+00 -e0  1.182299e+00
Quadrupole -name "LN2_QD_DH"        -length  4.000000e-02 -e0  1.182299e+00 -strength -3.168562e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.182299e+00
Drift      -name "LN2_DR_VS"        -length  1.000000e-01 -e0  1.182299e+00
Drift      -name "LN2_DR_BL"        -length  5.000000e-02 -e0  1.182299e+00
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  1.182299e+00
Cavity     -name "LN2_XCA0"         -length  9.164755e-01 -gradient  2.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.204766e+00
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  1.204766e+00
Drift      -name "LN2_DR_BL"        -length  5.000000e-02 -e0  1.204766e+00
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  1.204766e+00
Cavity     -name "LN2_XCA0"         -length  9.164755e-01 -gradient  2.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.227233e+00
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  1.227233e+00
Drift      -name "LN2_DR_BL"        -length  5.000000e-02 -e0  1.227233e+00
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.227233e+00
Quadrupole -name "LN2_QD_FH"        -length  4.000000e-02 -e0  1.227233e+00 -strength  3.288985e-01
Dipole     -name "LN2_COR"          -length  0.000000e+00 -e0  1.227233e+00
Bpm        -name "LN2_BPM"          -length  0.000000e+00 -e0  1.227233e+00
Quadrupole -name "LN2_QD_FH"        -length  4.000000e-02 -e0  1.227233e+00 -strength  3.288985e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.227233e+00
Drift      -name "LN2_WA_OU2"       -length  0.000000e+00 -e0  1.227233e+00
Drift      -name "LN2_DR_DI_V1"     -length  1.161052e+00 -e0  1.227233e+00
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.227233e+00
Quadrupole -name "LN2_QD_DI_V01"    -length  4.000000e-02 -e0  1.227233e+00 -strength -1.091633e-01
Dipole     -name "LN2_COR_DI"       -length  0.000000e+00 -e0  1.227233e+00
Bpm        -name "LN2_BPM_DI"       -length  0.000000e+00 -e0  1.227233e+00
Quadrupole -name "LN2_QD_DI_V01"    -length  4.000000e-02 -e0  1.227233e+00 -strength -1.091633e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.227233e+00
Drift      -name "LN2_DR_DI_V2"     -length  8.279980e-01 -e0  1.227233e+00
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.227233e+00
Quadrupole -name "LN2_QD_DI_V02"    -length  4.000000e-02 -e0  1.227233e+00 -strength  5.017593e-01
Dipole     -name "LN2_COR_DI"       -length  0.000000e+00 -e0  1.227233e+00
Bpm        -name "LN2_BPM_DI"       -length  0.000000e+00 -e0  1.227233e+00
Quadrupole -name "LN2_QD_DI_V02"    -length  4.000000e-02 -e0  1.227233e+00 -strength  5.017593e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.227233e+00
Drift      -name "LN2_DR_DI_V3"     -length  2.001539e-01 -e0  1.227233e+00
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.227233e+00
Quadrupole -name "LN2_QD_DI_V03"    -length  4.000000e-02 -e0  1.227233e+00 -strength -6.216784e-01
Dipole     -name "LN2_COR_DI"       -length  0.000000e+00 -e0  1.227233e+00
Bpm        -name "LN2_BPM_DI"       -length  0.000000e+00 -e0  1.227233e+00
Quadrupole -name "LN2_QD_DI_V03"    -length  4.000000e-02 -e0  1.227233e+00 -strength -6.216784e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.227233e+00
Drift      -name "LN2_DR_DI_V5"     -length  2.000000e-01 -e0  1.227233e+00
Drift      -name "LN2_DR_VS"        -length  1.000000e-01 -e0  1.227233e+00
Drift      -name "LN2_DR_DI"        -length  2.500000e-01 -e0  1.227233e+00
Drift      -name "LN2_DR_VS_DI"     -length  1.000000e-01 -e0  1.227233e+00
Drift      -name "LN2_DR_BL_DI"     -length  5.000000e-02 -e0  1.227233e+00
Drift      -name "LN2_DR_XCA_ED_DI" -length  1.001043e-01 -e0  1.227233e+00
CrabCavity -name "LN2_XCA0_DI"      -length  9.997914e-01 -voltage  0.000000e+00 -phase  9.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.227233e+00
Drift      -name "LN2_DR_XCA_ED_DI" -length  1.001043e-01 -e0  1.227152e+00
Drift      -name "LN2_DR_BL_DI"     -length  5.000000e-02 -e0  1.227152e+00
Drift      -name "LN2_DR_DI"        -length  2.500000e-01 -e0  1.227152e+00
Drift      -name "LN2_DR_DI"        -length  2.500000e-01 -e0  1.227152e+00
Drift      -name "LN2_DR_DI_QD_ED"  -length  4.250000e-02 -e0  1.227152e+00
Quadrupole -name "LN2_QD_DI_FH"     -length  4.000000e-02 -e0  1.227152e+00 -strength  3.926887e-01
Dipole     -name "LN2_COR_DI"       -length  0.000000e+00 -e0  1.227152e+00
Bpm        -name "LN2_BPM_DI"       -length  0.000000e+00 -e0  1.227152e+00
Quadrupole -name "LN2_QD_DI_FH"     -length  4.000000e-02 -e0  1.227152e+00 -strength  3.926887e-01
Drift      -name "LN2_DR_DI_QD_ED"  -length  4.250000e-02 -e0  1.227152e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.227152e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.227152e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.227152e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.227152e+00
Drift      -name "LN2_DR_DI_VS"     -length  1.000000e-01 -e0  1.227152e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.227152e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.227152e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.227152e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.227152e+00
Drift      -name "LN2_DR_DI_QD_ED"  -length  4.250000e-02 -e0  1.227152e+00
Quadrupole -name "LN2_QD_DI_DH"     -length  4.000000e-02 -e0  1.227152e+00 -strength -3.926887e-01
Dipole     -name "LN2_COR_DI"       -length  0.000000e+00 -e0  1.227152e+00
Bpm        -name "LN2_BPM_DI"       -length  0.000000e+00 -e0  1.227152e+00
Quadrupole -name "LN2_QD_DI_DH"     -length  4.000000e-02 -e0  1.227152e+00 -strength -3.926887e-01
Drift      -name "LN2_DR_DI_QD_ED"  -length  4.250000e-02 -e0  1.227152e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.227152e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.227152e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.227152e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.227152e+00
Drift      -name "LN2_DR_DI_VS"     -length  1.000000e-01 -e0  1.227152e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.227152e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.227152e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.227152e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.227152e+00
Drift      -name "LN2_DR_DI_QD_ED"  -length  4.250000e-02 -e0  1.227152e+00
Quadrupole -name "LN2_QD_DI_FH"     -length  4.000000e-02 -e0  1.227152e+00 -strength  3.926887e-01
Dipole     -name "LN2_COR_DI"       -length  0.000000e+00 -e0  1.227152e+00
Bpm        -name "LN2_BPM_DI"       -length  0.000000e+00 -e0  1.227152e+00
Quadrupole -name "LN2_QD_DI_FH"     -length  4.000000e-02 -e0  1.227152e+00 -strength  3.926887e-01
Drift      -name "LN2_DR_DI_QD_ED"  -length  4.250000e-02 -e0  1.227152e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.227152e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.227152e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.227152e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.227152e+00
Drift      -name "LN2_DR_DI_VS"     -length  1.000000e-01 -e0  1.227152e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.227152e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.227152e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.227152e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.227152e+00
Drift      -name "LN2_DR_DI_QD_ED"  -length  4.250000e-02 -e0  1.227152e+00
Quadrupole -name "LN2_QD_DI_DH"     -length  4.000000e-02 -e0  1.227152e+00 -strength -3.926887e-01
Dipole     -name "LN2_COR_DI"       -length  0.000000e+00 -e0  1.227152e+00
Bpm        -name "LN2_BPM_DI"       -length  0.000000e+00 -e0  1.227152e+00
Quadrupole -name "LN2_QD_DI_DH"     -length  4.000000e-02 -e0  1.227152e+00 -strength -3.926887e-01
Drift      -name "LN2_DR_DI_QD_ED"  -length  4.250000e-02 -e0  1.227152e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.227152e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.227152e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.227152e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.227152e+00
Drift      -name "LN2_DR_DI_VS"     -length  1.000000e-01 -e0  1.227152e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.227152e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.227152e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.227152e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.227152e+00
Drift      -name "LN2_DR_DI_QD_ED"  -length  4.250000e-02 -e0  1.227152e+00
Quadrupole -name "LN2_QD_DI_FH"     -length  4.000000e-02 -e0  1.227152e+00 -strength  3.926887e-01
Dipole     -name "LN2_COR_DI"       -length  0.000000e+00 -e0  1.227152e+00
Bpm        -name "LN2_BPM_DI"       -length  0.000000e+00 -e0  1.227152e+00
Quadrupole -name "LN2_QD_DI_FH"     -length  4.000000e-02 -e0  1.227152e+00 -strength  3.926887e-01
Drift      -name "LN2_DR_DI_QD_ED"  -length  4.250000e-02 -e0  1.227152e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.227152e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.227152e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.227152e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.227152e+00
Drift      -name "LN2_DR_DI_VS"     -length  1.000000e-01 -e0  1.227152e+00
Sbend      -name "LN2_DP_DI"        -length  1.500000e-01 -e0  1.227152e+00 -angle  0.000000e+00 -E1  0.000000e+00 -E2  0.000000e+00 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  0.000000e+00 -six_dim 1
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.227152e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.227152e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.227152e+00
Drift      -name "LN2_DR_DI_QD_ED"  -length  4.250000e-02 -e0  1.227152e+00
Quadrupole -name "LN2_QD_DI_DH"     -length  4.000000e-02 -e0  1.227152e+00 -strength -3.926887e-01
Dipole     -name "LN2_COR_DI"       -length  0.000000e+00 -e0  1.227152e+00
Bpm        -name "LN2_BPM_DI"       -length  0.000000e+00 -e0  1.227152e+00
Quadrupole -name "LN2_QD_DI_DH"     -length  4.000000e-02 -e0  1.227152e+00 -strength -3.926887e-01
Drift      -name "LN2_DR_DI_QD_ED"  -length  4.250000e-02 -e0  1.227152e+00
Drift      -name "LN2_DR_DI"        -length  2.500000e-01 -e0  1.227152e+00
Drift      -name "LN2_WA_OU_DI2"    -length  0.000000e+00 -e0  1.227152e+00
Drift      -name "LN2_DR_BP_V1"     -length  3.714219e+00 -e0  1.227152e+00
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.227152e+00
Quadrupole -name "LN2_QD_BP_V01"    -length  4.000000e-02 -e0  1.227152e+00 -strength -6.018088e-02
Dipole     -name "LN2_COR"          -length  0.000000e+00 -e0  1.227152e+00
Bpm        -name "LN2_BPM"          -length  0.000000e+00 -e0  1.227152e+00
Quadrupole -name "LN2_QD_BP_V01"    -length  4.000000e-02 -e0  1.227152e+00 -strength -6.018088e-02
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.227152e+00
Drift      -name "LN2_DR_BP_V2"     -length  3.528767e+00 -e0  1.227152e+00
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.227152e+00
Quadrupole -name "LN2_QD_BP_V02"    -length  4.000000e-02 -e0  1.227152e+00 -strength  6.790603e-01
Dipole     -name "LN2_COR"          -length  0.000000e+00 -e0  1.227152e+00
Bpm        -name "LN2_BPM"          -length  0.000000e+00 -e0  1.227152e+00
Quadrupole -name "LN2_QD_BP_V02"    -length  4.000000e-02 -e0  1.227152e+00 -strength  6.790603e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.227152e+00
Drift      -name "LN2_DR_BP_V3"     -length  2.000269e-01 -e0  1.227152e+00
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.227152e+00
Quadrupole -name "LN2_QD_BP_V03"    -length  4.000000e-02 -e0  1.227152e+00 -strength -6.624949e-01
Dipole     -name "LN2_COR"          -length  0.000000e+00 -e0  1.227152e+00
Bpm        -name "LN2_BPM"          -length  0.000000e+00 -e0  1.227152e+00
Quadrupole -name "LN2_QD_BP_V03"    -length  4.000000e-02 -e0  1.227152e+00 -strength -6.624949e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.227152e+00
Drift      -name "LN2_DR_BP_V4"     -length  2.645902e-01 -e0  1.227152e+00
Drift      -name "LN2_DR_BP_0"      -length  2.000000e-01 -e0  1.227152e+00
Drift      -name "LN2_DR_BP_BL"     -length  5.000000e-02 -e0  1.227152e+00
Drift      -name "LN2_DR_SCA_ED"    -length  8.325105e-02 -e0  1.227152e+00
CrabCavity -name "LN2_SCA0"         -length  5.334979e-01 -voltage  0.000000e+00 -phase  9.000000e+01 -frequency  2.997000e+00
SetReferenceEnergy   1.227152e+00
Drift      -name "LN2_DR_SCA_ED"    -length  8.325105e-02 -e0  1.227147e+00
Drift      -name "LN2_DR_BP_BL"     -length  5.000000e-02 -e0  1.227147e+00
Drift      -name "LN2_DR_BP_0"      -length  2.000000e-01 -e0  1.227147e+00
Drift      -name "LN2_DR_BP"        -length  4.000000e-01 -e0  1.227147e+00
Drift      -name "LN2_DR_BP"        -length  4.000000e-01 -e0  1.227147e+00
Drift      -name "LN2_DR_BP_VS"     -length  1.000000e-01 -e0  1.227147e+00
Drift      -name "LN2_DR_BP_DR"     -length  1.000000e-01 -e0  1.227147e+00
Drift      -name "LN2_DP_SEPM"      -length  2.500000e-01 -e0  1.227147e+00
Drift      -name "LN2_DR_BP"        -length  4.000000e-01 -e0  1.227147e+00
Drift      -name "LN2_DR_BP"        -length  4.000000e-01 -e0  1.227147e+00
Drift      -name "LN2_DR_BP"        -length  4.000000e-01 -e0  1.227147e+00
Drift      -name "LN2_DR_BP"        -length  4.000000e-01 -e0  1.227147e+00
Drift      -name "LN2_DR_BP"        -length  4.000000e-01 -e0  1.227147e+00
Drift      -name "LN2_DR_BP_VS"     -length  1.000000e-01 -e0  1.227147e+00
Drift      -name "LN2_DR_BP"        -length  4.000000e-01 -e0  1.227147e+00
Drift      -name "LN2_BP_WA_OU2_"   -length  0.000000e+00 -e0  1.227147e+00
Drift      -name "LN3_DR_V1"        -length  5.000000e-02 -e0  1.227147e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.227147e+00
Quadrupole -name "LN3_QD_V01"       -length  4.000000e-02 -e0  1.227147e+00 -strength  3.583693e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  1.227147e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  1.227147e+00
Quadrupole -name "LN3_QD_V01"       -length  4.000000e-02 -e0  1.227147e+00 -strength  3.583693e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.227147e+00
Drift      -name "LN3_DR_V2"        -length  5.342817e-01 -e0  1.227147e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.227147e+00
Quadrupole -name "LN3_QD_V02"       -length  4.000000e-02 -e0  1.227147e+00 -strength  4.422435e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  1.227147e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  1.227147e+00
Quadrupole -name "LN3_QD_V02"       -length  4.000000e-02 -e0  1.227147e+00 -strength  4.422435e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.227147e+00
Drift      -name "LN3_DR_V3"        -length  9.374694e-02 -e0  1.227147e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.227147e+00
Quadrupole -name "LN3_QD_V03"       -length  4.000000e-02 -e0  1.227147e+00 -strength -7.263974e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  1.227147e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  1.227147e+00
Quadrupole -name "LN3_QD_V03"       -length  4.000000e-02 -e0  1.227147e+00 -strength -7.263974e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.227147e+00
Drift      -name "LN3_DR_V4"        -length  1.580023e+00 -e0  1.227147e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.227147e+00
Quadrupole -name "LN3_QD_V04"       -length  4.000000e-02 -e0  1.227147e+00 -strength -1.638408e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  1.227147e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  1.227147e+00
Quadrupole -name "LN3_QD_V04"       -length  4.000000e-02 -e0  1.227147e+00 -strength -1.638408e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.227147e+00
Drift      -name "LN3_DR_V5"        -length  1.237385e+00 -e0  1.227147e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.227147e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  1.227147e+00 -strength  2.061607e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  1.227147e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  1.227147e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  1.227147e+00 -strength  2.061607e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.227147e+00
Drift      -name "LN3_DR_SV"        -length  1.000000e-01 -e0  1.227147e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.227147e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.227147e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.249614e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.249614e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.249614e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.249614e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.272081e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.272081e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.272081e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.272081e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.294548e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.294548e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.294548e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.294548e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.317015e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.317015e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.317015e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.317015e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  1.317015e+00 -strength -2.212586e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  1.317015e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  1.317015e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  1.317015e+00 -strength -2.212586e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.317015e+00
Drift      -name "LN3_DR_VS"        -length  1.000000e-01 -e0  1.317015e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.317015e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.317015e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.339482e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.339482e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.339482e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.339482e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.361949e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.361949e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.361949e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.361949e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.384416e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.384416e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.384416e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.384416e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.406883e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.406883e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.406883e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.406883e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  1.406883e+00 -strength  2.363564e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  1.406883e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  1.406883e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  1.406883e+00 -strength  2.363564e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.406883e+00
Drift      -name "LN3_DR_SV"        -length  1.000000e-01 -e0  1.406883e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.406883e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.406883e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.429350e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.429350e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.429350e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.429350e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.451818e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.451818e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.451818e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.451818e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.474285e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.474285e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.474285e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.474285e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.496752e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.496752e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.496752e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.496752e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  1.496752e+00 -strength -2.514543e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  1.496752e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  1.496752e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  1.496752e+00 -strength -2.514543e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.496752e+00
Drift      -name "LN3_DR_VS"        -length  1.000000e-01 -e0  1.496752e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.496752e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.496752e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.519219e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.519219e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.519219e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.519219e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.541686e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.541686e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.541686e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.541686e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.564153e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.564153e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.564153e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.564153e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.586620e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.586620e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.586620e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.586620e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  1.586620e+00 -strength  2.665521e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  1.586620e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  1.586620e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  1.586620e+00 -strength  2.665521e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.586620e+00
Drift      -name "LN3_DR_SV"        -length  1.000000e-01 -e0  1.586620e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.586620e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.586620e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.609087e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.609087e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.609087e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.609087e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.631554e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.631554e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.631554e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.631554e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.654021e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.654021e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.654021e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.654021e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.676488e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.676488e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.676488e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.676488e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  1.676488e+00 -strength -2.816499e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  1.676488e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  1.676488e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  1.676488e+00 -strength -2.816499e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.676488e+00
Drift      -name "LN3_DR_VS"        -length  1.000000e-01 -e0  1.676488e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.676488e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.676488e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.698955e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.698955e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.698955e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.698955e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.721422e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.721422e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.721422e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.721422e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.743889e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.743889e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.743889e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.743889e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.766356e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.766356e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.766356e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.766356e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  1.766356e+00 -strength  2.967478e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  1.766356e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  1.766356e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  1.766356e+00 -strength  2.967478e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.766356e+00
Drift      -name "LN3_DR_SV"        -length  1.000000e-01 -e0  1.766356e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.766356e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.766356e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.788823e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.788823e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.788823e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.788823e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.811290e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.811290e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.811290e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.811290e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.833757e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.833757e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.833757e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.833757e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.856224e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.856224e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.856224e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.856224e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  1.856224e+00 -strength -3.118456e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  1.856224e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  1.856224e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  1.856224e+00 -strength -3.118456e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.856224e+00
Drift      -name "LN3_DR_VS"        -length  1.000000e-01 -e0  1.856224e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.856224e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.856224e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.878691e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.878691e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.878691e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.878691e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.901158e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.901158e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.901158e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.901158e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.923625e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.923625e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.923625e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.923625e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.946092e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.946092e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.946092e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.946092e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  1.946092e+00 -strength  3.269434e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  1.946092e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  1.946092e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  1.946092e+00 -strength  3.269434e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.946092e+00
Drift      -name "LN3_DR_SV"        -length  1.000000e-01 -e0  1.946092e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.946092e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.946092e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.968559e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.968559e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.968559e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.968559e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.991026e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.991026e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.991026e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.991026e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.013493e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.013493e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.013493e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.013493e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.035960e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.035960e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.035960e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  2.035960e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  2.035960e+00 -strength -3.420413e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  2.035960e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  2.035960e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  2.035960e+00 -strength -3.420413e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  2.035960e+00
Drift      -name "LN3_DR_VS"        -length  1.000000e-01 -e0  2.035960e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.035960e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.035960e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.058427e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.058427e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.058427e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.058427e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.080894e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.080894e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.080894e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.080894e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.103361e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.103361e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.103361e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.103361e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.125828e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.125828e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.125828e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  2.125828e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  2.125828e+00 -strength  3.571391e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  2.125828e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  2.125828e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  2.125828e+00 -strength  3.571391e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  2.125828e+00
Drift      -name "LN3_DR_SV"        -length  1.000000e-01 -e0  2.125828e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.125828e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.125828e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.148295e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.148295e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.148295e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.148295e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.170762e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.170762e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.170762e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.170762e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.193229e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.193229e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.193229e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.193229e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.215696e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.215696e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.215696e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  2.215696e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  2.215696e+00 -strength -3.722369e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  2.215696e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  2.215696e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  2.215696e+00 -strength -3.722369e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  2.215696e+00
Drift      -name "LN3_DR_VS"        -length  1.000000e-01 -e0  2.215696e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.215696e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.215696e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.238163e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.238163e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.238163e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.238163e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.260630e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.260630e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.260630e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.260630e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.283097e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.283097e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.283097e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.283097e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.305564e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.305564e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.305564e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  2.305564e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  2.305564e+00 -strength  3.873348e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  2.305564e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  2.305564e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  2.305564e+00 -strength  3.873348e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  2.305564e+00
Drift      -name "LN3_DR_SV"        -length  1.000000e-01 -e0  2.305564e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.305564e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.305564e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.328031e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.328031e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.328031e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.328031e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.350498e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.350498e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.350498e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.350498e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.372965e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.372965e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.372965e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.372965e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.395432e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.395432e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.395432e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  2.395432e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  2.395432e+00 -strength -4.024326e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  2.395432e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  2.395432e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  2.395432e+00 -strength -4.024326e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  2.395432e+00
Drift      -name "LN3_DR_VS"        -length  1.000000e-01 -e0  2.395432e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.395432e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.395432e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.417899e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.417899e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.417899e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.417899e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.440366e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.440366e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.440366e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.440366e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.462833e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.462833e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.462833e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.462833e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.485300e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.485300e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.485300e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  2.485300e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  2.485300e+00 -strength  4.175304e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  2.485300e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  2.485300e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  2.485300e+00 -strength  4.175304e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  2.485300e+00
Drift      -name "LN3_DR_SV"        -length  1.000000e-01 -e0  2.485300e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.485300e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.485300e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.507767e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.507767e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.507767e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.507767e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.530234e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.530234e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.530234e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.530234e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.552701e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.552701e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.552701e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.552701e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.575168e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.575168e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.575168e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  2.575168e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  2.575168e+00 -strength -4.326283e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  2.575168e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  2.575168e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  2.575168e+00 -strength -4.326283e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  2.575168e+00
Drift      -name "LN3_DR_VS"        -length  1.000000e-01 -e0  2.575168e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.575168e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.575168e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.597635e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.597635e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.597635e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.597635e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.620102e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.620102e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.620102e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.620102e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.642569e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.642569e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.642569e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.642569e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.665036e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.665036e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.665036e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  2.665036e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  2.665036e+00 -strength  4.477261e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  2.665036e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  2.665036e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  2.665036e+00 -strength  4.477261e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  2.665036e+00
Drift      -name "LN3_DR_SV"        -length  1.000000e-01 -e0  2.665036e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.665036e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.665036e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.687503e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.687503e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.687503e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.687503e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.709970e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.709970e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.709970e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.709970e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.732437e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.732437e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.732437e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.732437e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  2.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.754904e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.754904e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.754904e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  2.754904e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  2.754904e+00 -strength -4.628240e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  2.754904e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  2.754904e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  2.754904e+00 -strength -4.628240e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  2.754904e+00
Drift      -name "LN3_DR_DR"        -length  1.000000e-01 -e0  2.754904e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.754904e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.754904e+00
Drift      -name "LN3_DR_XCA_"      -length  9.164755e-01 -e0  2.754904e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.754904e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.754904e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.754904e+00
Drift      -name "LN3_DR_XCA_"      -length  9.164755e-01 -e0  2.754904e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.754904e+00
Drift      -name "LN3_DR_CT"        -length  1.000000e-01 -e0  2.754904e+00
Drift      -name "LN3_WA_OU2"       -length  0.000000e+00 -e0  2.754904e+00
Drift      -name "LN3_DR_BP_V1"     -length  1.898529e+00 -e0  2.754904e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  2.754904e+00
Quadrupole -name "LN3_QD_BP_V01"    -length  4.000000e-02 -e0  2.754904e+00 -strength -8.611834e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  2.754904e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  2.754904e+00
Quadrupole -name "LN3_QD_BP_V01"    -length  4.000000e-02 -e0  2.754904e+00 -strength -8.611834e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  2.754904e+00
Drift      -name "LN3_DR_BP_V2"     -length  2.438830e+00 -e0  2.754904e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  2.754904e+00
Quadrupole -name "LN3_QD_BP_V02"    -length  4.000000e-02 -e0  2.754904e+00 -strength  9.468176e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  2.754904e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  2.754904e+00
Quadrupole -name "LN3_QD_BP_V02"    -length  4.000000e-02 -e0  2.754904e+00 -strength  9.468176e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  2.754904e+00
Drift      -name "LN3_DR_BP_V3"     -length  1.216600e+00 -e0  2.754904e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  2.754904e+00
Quadrupole -name "LN3_QD_BP_V03"    -length  4.000000e-02 -e0  2.754904e+00 -strength -9.035970e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  2.754904e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  2.754904e+00
Quadrupole -name "LN3_QD_BP_V03"    -length  4.000000e-02 -e0  2.754904e+00 -strength -9.035970e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  2.754904e+00
Drift      -name "LN3_DR_BP_V4"     -length  2.000413e-01 -e0  2.754904e+00
Drift      -name "LN3_DR_BP_0"      -length  2.000000e-01 -e0  2.754904e+00
Drift      -name "LN3_DR_BP_BL"     -length  5.000000e-02 -e0  2.754904e+00
Drift      -name "LN3_DR_SCA_ED"    -length  8.325105e-02 -e0  2.754904e+00
CrabCavity -name "LN3_SCA0"         -length  5.334979e-01 -voltage  0.000000e+00 -phase  9.000000e+01 -frequency  2.997000e+00
SetReferenceEnergy   2.754904e+00
Drift      -name "LN3_DR_SCA_ED"    -length  8.325105e-02 -e0  2.754900e+00
Drift      -name "LN3_DR_BP_BL"     -length  5.000000e-02 -e0  2.754900e+00
Drift      -name "LN3_DR_BP_0"      -length  2.000000e-01 -e0  2.754900e+00
Drift      -name "LN3_DR_BP"        -length  4.000000e-01 -e0  2.754900e+00
Drift      -name "LN3_DR_BP_VS"     -length  1.000000e-01 -e0  2.754900e+00
Drift      -name "LN3_DR_BP"        -length  4.000000e-01 -e0  2.754900e+00
Drift      -name "LN3_BP_WA_OUA"    -length  0.000000e+00 -e0  2.754900e+00
Drift      -name "HBP_CNT0"         -length  0.000000e+00 -e0  2.754900e+00
Sbend      -name "HBP_DP_SEPM1"     -length  3.202601e-01 -e0  2.754243e+00 -angle  3.490658e-02 -E1  0.000000e+00 -E2  0.000000e+00 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  3.141593e+00  -six_dim 1 -csr 1 
Sbend      -name "HBP_DP_SEPM1"     -length  3.202601e-01 -e0  2.753366e+00 -angle  3.490658e-02 -E1  0.000000e+00 -E2  0.000000e+00 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  3.141593e+00  -six_dim 1 -csr 1 
Drift      -name "HBP_DR_BP_C"      -length  3.000000e-01 -e0  2.753017e+00
Drift      -name "HBP_DR_BP"        -length  3.000000e-01 -e0  2.753017e+00
Drift      -name "HBP_DR_BP"        -length  3.000000e-01 -e0  2.753017e+00
Drift      -name "HBP_DR_BP"        -length  3.000000e-01 -e0  2.753017e+00
Drift      -name "HBP_DR_V1"        -length  2.629146e-01 -e0  2.753017e+00
Drift      -name "HBP_CNT0"         -length  0.000000e+00 -e0  2.753017e+00
Drift      -name "HBP_DR_QD_ED"     -length  4.250000e-02 -e0  2.753017e+00
Quadrupole -name "HBP_QD_V01"       -length  4.000000e-02 -e0  2.753017e+00 -strength  7.863166e-01
Dipole     -name "HBP_COR"          -length  0.000000e+00 -e0  2.753017e+00
Bpm        -name "HBP_BPM"          -length  0.000000e+00 -e0  2.753017e+00
Quadrupole -name "HBP_QD_V01"       -length  4.000000e-02 -e0  2.753017e+00 -strength  7.863166e-01
Drift      -name "HBP_DR_QD_ED"     -length  4.250000e-02 -e0  2.753017e+00
Drift      -name "HBP_DR_V2"        -length  2.075000e-01 -e0  2.753017e+00
Drift      -name "HBP_DR_V2"        -length  2.075000e-01 -e0  2.753017e+00
Drift      -name "HBP_CNT0"         -length  0.000000e+00 -e0  2.753017e+00
Drift      -name "HBP_DR_QD_ED"     -length  4.250000e-02 -e0  2.753017e+00
Quadrupole -name "HBP_QD_V01"       -length  4.000000e-02 -e0  2.753017e+00 -strength  7.863166e-01
Dipole     -name "HBP_COR"          -length  0.000000e+00 -e0  2.753017e+00
Bpm        -name "HBP_BPM"          -length  0.000000e+00 -e0  2.753017e+00
Quadrupole -name "HBP_QD_V01"       -length  4.000000e-02 -e0  2.753017e+00 -strength  7.863166e-01
Drift      -name "HBP_DR_QD_ED"     -length  4.250000e-02 -e0  2.753017e+00
Drift      -name "HBP_DR_V1"        -length  2.629146e-01 -e0  2.753017e+00
Drift      -name "HBP_DR_BP"        -length  3.000000e-01 -e0  2.753017e+00
Drift      -name "HBP_DR_BP"        -length  3.000000e-01 -e0  2.753017e+00
Drift      -name "HBP_DR_BP"        -length  3.000000e-01 -e0  2.753017e+00
Drift      -name "HBP_DR_BP"        -length  3.000000e-01 -e0  2.753017e+00
Drift      -name "HBP_CNT0"         -length  0.000000e+00 -e0  2.753017e+00
Sbend      -name "HBP_DP_SEPM1"     -length  3.202601e-01 -e0  2.752326e+00 -angle  3.490658e-02 -E1  0.000000e+00 -E2  0.000000e+00 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  3.141593e+00  -six_dim 1 -csr 1 
Sbend      -name "HBP_DP_SEPM1"     -length  3.202601e-01 -e0  2.751466e+00 -angle  3.490658e-02 -E1  0.000000e+00 -E2  0.000000e+00 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  3.141593e+00  -six_dim 1 -csr 1 
Drift      -name "HBP_WA_OU1A"      -length  0.000000e+00 -e0  2.751466e+00
Drift      -name "HBP_DR_BP_C"      -length  3.000000e-01 -e0  2.751144e+00
Drift      -name "HBP_DR_V3"        -length  1.194347e-02 -e0  2.751144e+00
Drift      -name "HBP_DR_QD_ED"     -length  4.250000e-02 -e0  2.751144e+00
Quadrupole -name "HBP_QD_V02"       -length  4.000000e-02 -e0  2.751144e+00 -strength -6.817379e-01
Dipole     -name "HBP_COR"          -length  0.000000e+00 -e0  2.751144e+00
Bpm        -name "HBP_BPM"          -length  0.000000e+00 -e0  2.751144e+00
Quadrupole -name "HBP_QD_V02"       -length  4.000000e-02 -e0  2.751144e+00 -strength -6.817379e-01
Drift      -name "HBP_DR_QD_ED"     -length  4.250000e-02 -e0  2.751144e+00
Drift      -name "HBP_DR_V4"        -length  1.379807e+00 -e0  2.751144e+00
Drift      -name "HBP_DR_QD_ED"     -length  4.250000e-02 -e0  2.751144e+00
Quadrupole -name "HBP_QD_V03"       -length  4.000000e-02 -e0  2.751144e+00 -strength  7.549036e-01
Dipole     -name "HBP_COR"          -length  0.000000e+00 -e0  2.751144e+00
Bpm        -name "HBP_BPM"          -length  0.000000e+00 -e0  2.751144e+00
Quadrupole -name "HBP_QD_V03"       -length  4.000000e-02 -e0  2.751144e+00 -strength  7.549036e-01
Drift      -name "HBP_DR_QD_ED"     -length  4.250000e-02 -e0  2.751144e+00
Drift      -name "HBP_DR_V5"        -length  4.422050e+00 -e0  2.751144e+00
Drift      -name "HBP_DR_QD_ED"     -length  4.250000e-02 -e0  2.751144e+00
Quadrupole -name "HBP_QD_V04"       -length  4.000000e-02 -e0  2.751144e+00 -strength -3.601388e-01
Dipole     -name "HBP_COR"          -length  0.000000e+00 -e0  2.751144e+00
Bpm        -name "HBP_BPM"          -length  0.000000e+00 -e0  2.751144e+00
Quadrupole -name "HBP_QD_V04"       -length  4.000000e-02 -e0  2.751144e+00 -strength -3.601388e-01
Drift      -name "HBP_DR_QD_ED"     -length  4.250000e-02 -e0  2.751144e+00
Drift      -name "HBP_DR_V5"        -length  4.422050e+00 -e0  2.751144e+00
Drift      -name "HBP_DR_QD_ED"     -length  4.250000e-02 -e0  2.751144e+00
Quadrupole -name "HBP_QD_V03"       -length  4.000000e-02 -e0  2.751144e+00 -strength  7.549036e-01
Dipole     -name "HBP_COR"          -length  0.000000e+00 -e0  2.751144e+00
Bpm        -name "HBP_BPM"          -length  0.000000e+00 -e0  2.751144e+00
Quadrupole -name "HBP_QD_V03"       -length  4.000000e-02 -e0  2.751144e+00 -strength  7.549036e-01
Drift      -name "HBP_DR_QD_ED"     -length  4.250000e-02 -e0  2.751144e+00
Drift      -name "HBP_DR_V4"        -length  1.379807e+00 -e0  2.751144e+00
Drift      -name "HBP_DR_QD_ED"     -length  4.250000e-02 -e0  2.751144e+00
Quadrupole -name "HBP_QD_V02"       -length  4.000000e-02 -e0  2.751144e+00 -strength -6.817379e-01
Dipole     -name "HBP_COR"          -length  0.000000e+00 -e0  2.751144e+00
Bpm        -name "HBP_BPM"          -length  0.000000e+00 -e0  2.751144e+00
Quadrupole -name "HBP_QD_V02"       -length  4.000000e-02 -e0  2.751144e+00 -strength -6.817379e-01
Drift      -name "HBP_DR_QD_ED"     -length  4.250000e-02 -e0  2.751144e+00
Drift      -name "HBP_DR_V3"        -length  1.194347e-02 -e0  2.751144e+00
Drift      -name "HBP_DR_BP"        -length  3.000000e-01 -e0  2.751144e+00
Drift      -name "HBP_WA_OU2A"      -length  0.000000e+00 -e0  2.751144e+00
Sbend      -name "HBP_DP_SEPM2"     -length  3.202601e-01 -e0  2.750410e+00 -angle  3.490658e-02 -E1  0.000000e+00 -E2  0.000000e+00 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  0.000000e+00  -six_dim 1 -csr 1 
Sbend      -name "HBP_DP_SEPM2"     -length  3.202601e-01 -e0  2.749761e+00 -angle  3.490658e-02 -E1  0.000000e+00 -E2  0.000000e+00 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  0.000000e+00  -six_dim 1 -csr 1 
Drift      -name "HBP_DR_BP_C"      -length  3.000000e-01 -e0  2.749497e+00
Drift      -name "HBP_DR_BP"        -length  3.000000e-01 -e0  2.749497e+00
Drift      -name "HBP_DR_BP"        -length  3.000000e-01 -e0  2.749497e+00
Drift      -name "HBP_DR_BP"        -length  3.000000e-01 -e0  2.749497e+00
Drift      -name "HBP_DR_V1"        -length  2.629146e-01 -e0  2.749497e+00
Drift      -name "HBP_CNT0"         -length  0.000000e+00 -e0  2.749497e+00
Drift      -name "HBP_DR_QD_ED"     -length  4.250000e-02 -e0  2.749497e+00
Quadrupole -name "HBP_QD_V01"       -length  4.000000e-02 -e0  2.749497e+00 -strength  7.853112e-01
Dipole     -name "HBP_COR"          -length  0.000000e+00 -e0  2.749497e+00
Bpm        -name "HBP_BPM"          -length  0.000000e+00 -e0  2.749497e+00
Quadrupole -name "HBP_QD_V01"       -length  4.000000e-02 -e0  2.749497e+00 -strength  7.853112e-01
Drift      -name "HBP_DR_QD_ED"     -length  4.250000e-02 -e0  2.749497e+00
Drift      -name "HBP_DR_V2"        -length  2.075000e-01 -e0  2.749497e+00
Drift      -name "HBP_DR_V2"        -length  2.075000e-01 -e0  2.749497e+00
Drift      -name "HBP_CNT0"         -length  0.000000e+00 -e0  2.749497e+00
Drift      -name "HBP_DR_QD_ED"     -length  4.250000e-02 -e0  2.749497e+00
Quadrupole -name "HBP_QD_V01"       -length  4.000000e-02 -e0  2.749497e+00 -strength  7.853112e-01
Dipole     -name "HBP_COR"          -length  0.000000e+00 -e0  2.749497e+00
Bpm        -name "HBP_BPM"          -length  0.000000e+00 -e0  2.749497e+00
Quadrupole -name "HBP_QD_V01"       -length  4.000000e-02 -e0  2.749497e+00 -strength  7.853112e-01
Drift      -name "HBP_DR_QD_ED"     -length  4.250000e-02 -e0  2.749497e+00
Drift      -name "HBP_DR_V1"        -length  2.629146e-01 -e0  2.749497e+00
Drift      -name "HBP_DR_BP"        -length  3.000000e-01 -e0  2.749497e+00
Drift      -name "HBP_DR_BP"        -length  3.000000e-01 -e0  2.749497e+00
Drift      -name "HBP_DR_BP"        -length  3.000000e-01 -e0  2.749497e+00
Drift      -name "HBP_DR_BP"        -length  3.000000e-01 -e0  2.749497e+00
Sbend      -name "HBP_DP_SEPM2"     -length  3.202601e-01 -e0  2.748740e+00 -angle  3.490658e-02 -E1  0.000000e+00 -E2  0.000000e+00 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  0.000000e+00  -six_dim 1 -csr 1 
Sbend      -name "HBP_DP_SEPM2"     -length  3.202601e-01 -e0  2.747905e+00 -angle  3.490658e-02 -E1  0.000000e+00 -E2  0.000000e+00 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  0.000000e+00  -six_dim 1 -csr 1 
Drift      -name "HBP_WA_OU3A"      -length  0.000000e+00 -e0  2.747905e+00
Drift      -name "HXR_DR_V1"        -length  1.000000e-01 -e0  2.747905e+00
Drift      -name "HXR_DR_QD_ED"     -length  5.200000e-02 -e0  2.747905e+00
Quadrupole -name "HXR_QD_V01"       -length  1.300000e-02 -e0  2.747905e+00 -strength -1.055552e+00
Dipole     -name "HXR_COR"          -length  0.000000e+00 -e0  2.747905e+00
Bpm        -name "HXR_BPM"          -length  0.000000e+00 -e0  2.747905e+00
Quadrupole -name "HXR_QD_V01"       -length  1.300000e-02 -e0  2.747905e+00 -strength -1.055552e+00
Drift      -name "HXR_DR_QD_ED"     -length  5.200000e-02 -e0  2.747905e+00
Drift      -name "HXR_DR_V2"        -length  1.336363e+00 -e0  2.747905e+00
Drift      -name "HXR_DR_QD_ED"     -length  5.200000e-02 -e0  2.747905e+00
Quadrupole -name "HXR_QD_V02"       -length  1.300000e-02 -e0  2.747905e+00 -strength  9.862154e-01
Dipole     -name "HXR_COR"          -length  0.000000e+00 -e0  2.747905e+00
Bpm        -name "HXR_BPM"          -length  0.000000e+00 -e0  2.747905e+00
Quadrupole -name "HXR_QD_V02"       -length  1.300000e-02 -e0  2.747905e+00 -strength  9.862154e-01
Drift      -name "HXR_DR_QD_ED"     -length  5.200000e-02 -e0  2.747905e+00
Drift      -name "HXR_DR_V3"        -length  2.999996e+00 -e0  2.747905e+00
Drift      -name "HXR_DR_QD_ED"     -length  5.200000e-02 -e0  2.747905e+00
Quadrupole -name "HXR_QD_V03"       -length  1.300000e-02 -e0  2.747905e+00 -strength -7.729863e-01
Dipole     -name "HXR_COR"          -length  0.000000e+00 -e0  2.747905e+00
Bpm        -name "HXR_BPM"          -length  0.000000e+00 -e0  2.747905e+00
Quadrupole -name "HXR_QD_V03"       -length  1.300000e-02 -e0  2.747905e+00 -strength -7.729863e-01
Drift      -name "HXR_DR_QD_ED"     -length  5.200000e-02 -e0  2.747905e+00
Drift      -name "HXR_DR_V4"        -length  1.042795e+00 -e0  2.747905e+00
Drift      -name "HXR_DR_CT"        -length  1.000000e-01 -e0  2.747905e+00
Drift      -name "HXR_WA_M_OU2"     -length  0.000000e+00 -e0  2.747905e+00
Drift      -name "HXR_DR_QD_ED"     -length  5.200000e-02 -e0  2.747905e+00
Quadrupole -name "HXR_QD_FH"        -length  1.300000e-02 -e0  2.747905e+00 -strength  5.358415e-01
Dipole     -name "HXR_COR"          -length  0.000000e+00 -e0  2.747905e+00
Bpm        -name "HXR_BPM"          -length  0.000000e+00 -e0  2.747905e+00
Quadrupole -name "HXR_QD_FH"        -length  1.300000e-02 -e0  2.747905e+00 -strength  5.358415e-01
Drift      -name "HXR_DR_QD_ED"     -length  5.200000e-02 -e0  2.747905e+00
Drift      -name "HXR_DR_WIG_ED"    -length  1.950000e-01 -e0  2.747905e+00
Drift      -name "HXR_WIG_0"        -length  1.768000e+00 -e0  2.747905e+00
Drift      -name "HXR_DR_WIG_ED"    -length  1.950000e-01 -e0  2.747905e+00
Drift      -name "HXR_DR_QD_ED"     -length  5.200000e-02 -e0  2.747905e+00
Quadrupole -name "HXR_QD_DH"        -length  1.300000e-02 -e0  2.747905e+00 -strength -5.358415e-01
Dipole     -name "HXR_COR"          -length  0.000000e+00 -e0  2.747905e+00
Bpm        -name "HXR_BPM"          -length  0.000000e+00 -e0  2.747905e+00
Quadrupole -name "HXR_QD_DH"        -length  1.300000e-02 -e0  2.747905e+00 -strength -5.358415e-01
Drift      -name "HXR_DR_QD_ED"     -length  5.200000e-02 -e0  2.747905e+00
Drift      -name "HXR_DR_WIG_ED"    -length  1.950000e-01 -e0  2.747905e+00
Drift      -name "HXR_WIG_0"        -length  1.768000e+00 -e0  2.747905e+00
Drift      -name "HXR_DR_WIG_ED"    -length  1.950000e-01 -e0  2.747905e+00
Drift      -name "HXR_DR_QD_ED"     -length  5.200000e-02 -e0  2.747905e+00
Quadrupole -name "HXR_QD_FH"        -length  1.300000e-02 -e0  2.747905e+00 -strength  5.358415e-01
Drift      -name "HXR_WA_OU2"       -length  0.000000e+00 -e0  2.747905e+00
