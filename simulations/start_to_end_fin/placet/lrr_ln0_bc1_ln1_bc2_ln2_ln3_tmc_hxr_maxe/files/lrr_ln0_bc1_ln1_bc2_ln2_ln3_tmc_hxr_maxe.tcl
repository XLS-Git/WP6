Girder
SetReferenceEnergy  1.247937643977900e-01
Drift      -name "LN0_DR_V1"        -length 0.200000001479 -e0 0.124793764398
Drift      -name "LN0_DR_QD_ED"     -length         0.0425 -e0 0.124793764398
Quadrupole -name "LN0_QD_V01"       -length           0.04 -e0 0.124793764398 -strength -0.0434688066086
Dipole     -name "LN0_COR"          -length              0 -e0 0.124793764398
Bpm        -name "LN0_BPM"          -length              0 -e0 0.124793764398
Quadrupole -name "LN0_QD_V01"       -length           0.04 -e0 0.124793764398 -strength -0.0434688066086
Drift      -name "LN0_DR_QD_ED"     -length         0.0425 -e0 0.124793764398
Drift      -name "LN0_DR_V2"        -length 0.357305438201 -e0 0.124793764398
Drift      -name "LN0_DR_QD_ED"     -length         0.0425 -e0 0.124793764398
Quadrupole -name "LN0_QD_V02"       -length           0.04 -e0 0.124793764398 -strength 0.0572061376265
Dipole     -name "LN0_COR"          -length              0 -e0 0.124793764398
Bpm        -name "LN0_BPM"          -length              0 -e0 0.124793764398
Quadrupole -name "LN0_QD_V02"       -length           0.04 -e0 0.124793764398 -strength 0.0572061376265
Drift      -name "LN0_DR_QD_ED"     -length         0.0425 -e0 0.124793764398
Drift      -name "LN0_DR_V3"        -length 0.997243110848 -e0 0.124793764398
Drift      -name "LN0_DR_QD_ED"     -length         0.0425 -e0 0.124793764398
Quadrupole -name "LN0_QD_V03"       -length           0.04 -e0 0.124793764398 -strength -0.00670479647144
Dipole     -name "LN0_COR"          -length              0 -e0 0.124793764398
Bpm        -name "LN0_BPM"          -length              0 -e0 0.124793764398
Quadrupole -name "LN0_QD_V03"       -length           0.04 -e0 0.124793764398 -strength -0.00670479647144
Drift      -name "LN0_DR_QD_ED"     -length         0.0425 -e0 0.124793764398
Drift      -name "LN0_DR_V4"        -length  1.42143952541 -e0 0.124793764398
Drift      -name "LN0_DR_20"        -length            0.2 -e0 0.124793764398
Sbend      -name "LN0_DP_DIP1"      -length            0.1 -e0 0.124793754963 -angle -0.06981317 -E1 0 -E2 -0.06981317 -hgap 0 -fint 0.5 -tilt 0  -six_dim 1 -csr 1 
Drift      -name "LN0_DR_20_C"      -length            0.2 -e0 0.124793449698
Sbend      -name "LN0_DP_DIP2"      -length            0.1 -e0 0.124793440297 -angle 0.06981317 -E1 0.06981317 -E2 0 -hgap 0 -fint 0.5 -tilt 0  -six_dim 1 -csr 1 
Drift      -name "LN0_DR_20_C"      -length            0.2 -e0 0.124793135743
Bpm        -name "LN0_BPM_LH"       -length              0 -e0 0.124793135743
Drift      -name "LN0_DR_20"        -length            0.2 -e0 0.124793135743
Drift      -name "LN0_UN_UND"       -length           0.24 -e0 0.124793135743
Drift      -name "LN0_DR_20"        -length            0.2 -e0 0.124793135743
Bpm        -name "LN0_BPM_LH"       -length              0 -e0 0.124793135743
Drift      -name "LN0_DR_VS"        -length            0.1 -e0 0.124793135743
Drift      -name "LN0_DR_10"        -length            0.1 -e0 0.124793135743
Sbend      -name "LN0_DP_DIP3"      -length            0.1 -e0 0.124793126275 -angle 0.06981317 -E1 0 -E2 0.06981317 -hgap 0 -fint 0.5 -tilt 0  -six_dim 1 -csr 1 
Drift      -name "LN0_DR_20_C"      -length            0.2 -e0 0.124792818748
Sbend      -name "LN0_DP_DIP4"      -length            0.1 -e0 0.124792809311 -angle -0.06981317 -E1 -0.06981317 -E2 0 -hgap 0 -fint 0.5 -tilt 0  -six_dim 1 -csr 1 
Drift      -name "LN0_DR_20_C"      -length            0.2 -e0 0.124792503836
Drift      -name "LN0_DR_VS"        -length            0.1 -e0 0.124792503836
Drift      -name "WATCH_LH"         -length              0 -e0 0.124792503836
Drift      -name "LN0_DR_V5"        -length 0.663943311863 -e0 0.124792503836
Drift      -name "LN0_DR_QD_ED"     -length         0.0425 -e0 0.124792503836
Quadrupole -name "LN0_QD_V04"       -length           0.04 -e0 0.124792503836 -strength 0.0313485018471
Dipole     -name "LN0_COR"          -length              0 -e0 0.124792503836
Bpm        -name "LN0_BPM"          -length              0 -e0 0.124792503836
Quadrupole -name "LN0_QD_V04"       -length           0.04 -e0 0.124792503836 -strength 0.0313485018471
Drift      -name "LN0_DR_QD_ED"     -length         0.0425 -e0 0.124792503836
Drift      -name "LN0_DR_V6"        -length 0.259892892724 -e0 0.124792503836
Drift      -name "LN0_DR_QD_ED"     -length         0.0425 -e0 0.124792503836
Quadrupole -name "LN0_QD_V05"       -length           0.04 -e0 0.124792503836 -strength 0.0167572282714
Dipole     -name "LN0_COR"          -length              0 -e0 0.124792503836
Bpm        -name "LN0_BPM"          -length              0 -e0 0.124792503836
Quadrupole -name "LN0_QD_V05"       -length           0.04 -e0 0.124792503836 -strength 0.0167572282714
Drift      -name "LN0_DR_QD_ED"     -length         0.0425 -e0 0.124792503836
Drift      -name "LN0_DR_V7"        -length  1.41975054217 -e0 0.124792503836
Drift      -name "LN0_DR_QD_ED"     -length         0.0425 -e0 0.124792503836
Quadrupole -name "LN0_QD_V06"       -length           0.04 -e0 0.124792503836 -strength -0.0320820407709
Dipole     -name "LN0_COR"          -length              0 -e0 0.124792503836
Bpm        -name "LN0_BPM"          -length              0 -e0 0.124792503836
Quadrupole -name "LN0_QD_V06"       -length           0.04 -e0 0.124792503836 -strength -0.0320820407709
Drift      -name "LN0_DR_QD_ED"     -length         0.0425 -e0 0.124792503836
Drift      -name "LN0_DR_V8"        -length 0.200000760223 -e0 0.124792503836
Drift      -name "LN0_DR_VS"        -length            0.1 -e0 0.124792503836
Drift      -name "LN0_DR_BL"        -length           0.05 -e0 0.124792503836
Drift      -name "LN0_DR_XCA_ED_DI" -length      0.1001043 -e0 0.124792503836
CrabCavity -name "LN0_XCA0_DI"      -length      0.9997914 -voltage 0 -phase 90 -frequency 11.9942
SetReferenceEnergy  1.247925038363527e-01
Drift      -name "LN0_DR_XCA_ED_DI" -length      0.1001043 -e0 0.124752025296
Drift      -name "LN0_DR_BL"        -length           0.05 -e0 0.124752025296
Drift      -name "LN0_TDS_DR"       -length           0.25 -e0 0.124752025296
Drift      -name "LN0_TDS_DR"       -length           0.25 -e0 0.124752025296
Drift      -name "LN0_TDS_DR"       -length           0.25 -e0 0.124752025296
Drift      -name "LN0_TDS_DR"       -length           0.25 -e0 0.124752025296
Drift      -name "LN0_TDS_DR"       -length           0.25 -e0 0.124752025296
Drift      -name "LN0_TDS_DR"       -length           0.25 -e0 0.124752025296
Drift      -name "LN0_TDS_DR"       -length           0.25 -e0 0.124752025296
Drift      -name "LN0_TDS_DR"       -length           0.25 -e0 0.124752025296
Sbend      -name "LN0_DP_TDS"       -length            0.1 -e0 0.124752025296 -angle 0 -E1 0 -E2 0 -hgap 0 -fint 0.5 -tilt 0 -six_dim 1
Drift      -name "LN0_DR_20"        -length            0.2 -e0 0.124752025296
Drift      -name "LN0_DR_VS"        -length            0.1 -e0 0.124752025296
Drift      -name "LN0_DR_QD_ED"     -length         0.0425 -e0 0.124752025296
Quadrupole -name "LN0_QD_FH"        -length           0.04 -e0 0.124752025296 -strength 0.034930567083
Dipole     -name "LN0_COR"          -length              0 -e0 0.124752025296
Bpm        -name "LN0_BPM"          -length              0 -e0 0.124752025296
Quadrupole -name "LN0_QD_FH"        -length           0.04 -e0 0.124752025296 -strength 0.034930567083
Drift      -name "LN0_DR_QD_ED"     -length         0.0425 -e0 0.124752025296
Drift      -name "LN0_DR_SV"        -length            0.1 -e0 0.124752025296
Drift      -name "LN0_DR_BL"        -length           0.05 -e0 0.124752025296
Drift      -name "LN0_DR_CCA_ED"    -length       0.050198 -e0 0.124752025296
Cavity     -name "LN0_CCA0"         -length       1.899604 -gradient 0.015 -phase 30 -frequency 5.9971
SetReferenceEnergy  1.493721551803940e-01
Drift      -name "LN0_DR_CCA_ED"    -length       0.050198 -e0  0.14937215518
Drift      -name "LN0_DR_BL"        -length           0.05 -e0  0.14937215518
Drift      -name "LN0_DR_QD_ED"     -length         0.0425 -e0  0.14937215518
Quadrupole -name "LN0_QD_DH"        -length           0.04 -e0  0.14937215518 -strength -0.0418242034505
Dipole     -name "LN0_COR"          -length              0 -e0  0.14937215518
Bpm        -name "LN0_BPM"          -length              0 -e0  0.14937215518
Quadrupole -name "LN0_QD_DH"        -length           0.04 -e0  0.14937215518 -strength -0.0418242034505
Drift      -name "LN0_DR_QD_ED"     -length         0.0425 -e0  0.14937215518
Drift      -name "LN0_DR_VS"        -length            0.1 -e0  0.14937215518
Drift      -name "LN0_DR_BL"        -length           0.05 -e0  0.14937215518
Drift      -name "LN0_DR_CCA_ED"    -length       0.050198 -e0  0.14937215518
Cavity     -name "LN0_CCA0"         -length       1.899604 -gradient 0.015 -phase 30 -frequency 5.9971
SetReferenceEnergy  1.739922890298896e-01
Drift      -name "LN0_DR_CCA_ED"    -length       0.050198 -e0  0.17399228903
Drift      -name "LN0_DR_BL"        -length           0.05 -e0  0.17399228903
Drift      -name "LN0_DR_QD_ED"     -length         0.0425 -e0  0.17399228903
Quadrupole -name "LN0_QD_FH"        -length           0.04 -e0  0.17399228903 -strength 0.0487178409284
Dipole     -name "LN0_COR"          -length              0 -e0  0.17399228903
Bpm        -name "LN0_BPM"          -length              0 -e0  0.17399228903
Quadrupole -name "LN0_QD_FH"        -length           0.04 -e0  0.17399228903 -strength 0.0487178409284
Drift      -name "LN0_DR_QD_ED"     -length         0.0425 -e0  0.17399228903
Drift      -name "LN0_DR_VS"        -length            0.1 -e0  0.17399228903
Drift      -name "LN0_DR_BL"        -length           0.05 -e0  0.17399228903
Drift      -name "LN0_DR_CCA_ED"    -length       0.050198 -e0  0.17399228903
Cavity     -name "LN0_CCA0"         -length       1.899604 -gradient 0.015 -phase 30 -frequency 5.9971
SetReferenceEnergy  1.986124330092847e-01
Drift      -name "LN0_DR_CCA_ED"    -length       0.050198 -e0 0.198612433009
Drift      -name "LN0_DR_BL"        -length           0.05 -e0 0.198612433009
Drift      -name "LN0_DR_QD_ED"     -length         0.0425 -e0 0.198612433009
Quadrupole -name "LN0_QD_DH"        -length           0.04 -e0 0.198612433009 -strength -0.0556114812426
Dipole     -name "LN0_COR"          -length              0 -e0 0.198612433009
Bpm        -name "LN0_BPM"          -length              0 -e0 0.198612433009
Quadrupole -name "LN0_QD_DH"        -length           0.04 -e0 0.198612433009 -strength -0.0556114812426
Drift      -name "LN0_DR_QD_ED"     -length         0.0425 -e0 0.198612433009
Drift      -name "LN0_DR_VS"        -length            0.1 -e0 0.198612433009
Drift      -name "LN0_DR_BL"        -length           0.05 -e0 0.198612433009
Drift      -name "LN0_DR_CCA_ED"    -length       0.050198 -e0 0.198612433009
Cavity     -name "LN0_CCA0"         -length       1.899604 -gradient 0.015 -phase 30 -frequency 5.9971
SetReferenceEnergy  2.232325885178167e-01
Drift      -name "LN0_DR_CCA_ED"    -length       0.050198 -e0 0.223232588518
Drift      -name "LN0_DR_BL"        -length           0.05 -e0 0.223232588518
Drift      -name "LN0_DR_QD_ED"     -length         0.0425 -e0 0.223232588518
Quadrupole -name "LN0_QD_FH"        -length           0.04 -e0 0.223232588518 -strength 0.062505124785
Dipole     -name "LN0_COR"          -length              0 -e0 0.223232588518
Bpm        -name "LN0_BPM"          -length              0 -e0 0.223232588518
Quadrupole -name "LN0_QD_FH"        -length           0.04 -e0 0.223232588518 -strength 0.062505124785
Drift      -name "LN0_DR_QD_ED"     -length         0.0425 -e0 0.223232588518
Drift      -name "LN0_DR_VS"        -length            0.1 -e0 0.223232588518
Drift      -name "LN0_DR_BL"        -length           0.05 -e0 0.223232588518
Drift      -name "LN0_DR_CCA_ED"    -length       0.050198 -e0 0.223232588518
Cavity     -name "LN0_CCA0"         -length       1.899604 -gradient 0.015 -phase 30 -frequency 5.9971
SetReferenceEnergy  2.478527553092857e-01
Drift      -name "LN0_DR_CCA_ED"    -length       0.050198 -e0 0.247852755309
Drift      -name "LN0_DR_BL"        -length           0.05 -e0 0.247852755309
Drift      -name "LN0_DR_QD_ED"     -length         0.0425 -e0 0.247852755309
Quadrupole -name "LN0_QD_DH"        -length           0.04 -e0 0.247852755309 -strength -0.0693987714866
Dipole     -name "LN0_COR"          -length              0 -e0 0.247852755309
Bpm        -name "LN0_BPM"          -length              0 -e0 0.247852755309
Quadrupole -name "LN0_QD_DH"        -length           0.04 -e0 0.247852755309 -strength -0.0693987714866
Drift      -name "LN0_DR_QD_ED"     -length         0.0425 -e0 0.247852755309
Drift      -name "LN0_DR_VS"        -length            0.1 -e0 0.247852755309
Drift      -name "LN0_DR_BL"        -length           0.05 -e0 0.247852755309
Drift      -name "LN0_DR_CCA_ED"    -length       0.050198 -e0 0.247852755309
Cavity     -name "LN0_CCA0"         -length       1.899604 -gradient 0.015 -phase 30 -frequency 5.9971
SetReferenceEnergy  2.724729325831955e-01
Drift      -name "LN0_DR_CCA_ED"    -length       0.050198 -e0 0.272472932583
Drift      -name "LN0_DR_BL"        -length           0.05 -e0 0.272472932583
Drift      -name "LN0_WA_LNZ_IN2"   -length              0 -e0 0.272472932583
Drift      -name "LN0_DR_QD_ED"     -length         0.0425 -e0 0.272472932583
Quadrupole -name "LN0_QD_FH"        -length           0.04 -e0 0.272472932583 -strength 0.0762924211233
Dipole     -name "LN0_COR"          -length              0 -e0 0.272472932583
Bpm        -name "LN0_BPM"          -length              0 -e0 0.272472932583
Quadrupole -name "LN0_QD_FH"        -length           0.04 -e0 0.272472932583 -strength 0.0762924211233
Drift      -name "LN0_DR_QD_ED"     -length         0.0425 -e0 0.272472932583
Drift      -name "LN0_DR_VS"        -length            0.1 -e0 0.272472932583
Drift      -name "LN0_DR_LNZ_ED2"   -length          0.625 -e0 0.272472932583
Drift      -name "LN0_DR_BL"        -length           0.05 -e0 0.272472932583
Drift      -name "LN0_DR_KCA_ED"    -length      0.0222541 -e0 0.272472932583
Cavity     -name "LN0_KCA0"         -length      0.3054918 -gradient 0.0252052591919 -phase 195 -frequency 35.9826
SetReferenceEnergy  2.651953880666264e-01
Drift      -name "LN0_DR_KCA_ED"    -length      0.0222541 -e0 0.265195388067
Drift      -name "LN0_DR_BL"        -length           0.05 -e0 0.265195388067
Drift      -name "LN0_DR_KCA_ED"    -length      0.0222541 -e0 0.265195388067
Drift      -name "LN0_DR_KCA0"      -length      0.3054918 -e0 0.265195388067
Drift      -name "LN0_DR_KCA_ED"    -length      0.0222541 -e0 0.265195388067
Drift      -name "LN0_DR_BL"        -length           0.05 -e0 0.265195388067
Drift      -name "LN0_DR_LNZ_ED2"   -length          0.625 -e0 0.265195388067
Drift      -name "LN0_DR_QD_ED"     -length         0.0425 -e0 0.265195388067
Quadrupole -name "LN0_QD_DH"        -length           0.04 -e0 0.265195388067 -strength -0.0742547086587
Dipole     -name "LN0_COR"          -length              0 -e0 0.265195388067
Bpm        -name "LN0_BPM"          -length              0 -e0 0.265195388067
Quadrupole -name "LN0_QD_DH"        -length           0.04 -e0 0.265195388067 -strength -0.0742547086587
Drift      -name "LN0_DR_QD_ED"     -length         0.0425 -e0 0.265195388067
Drift      -name "LN0_DR_DR"        -length            0.1 -e0 0.265195388067
Drift      -name "LN0_DR_BL"        -length           0.05 -e0 0.265195388067
Drift      -name "LN0_DR_CCA_ED"    -length       0.050198 -e0 0.265195388067
Drift      -name "LN0_DR_CCA0H"     -length       0.349802 -e0 0.265195388067
Drift      -name "LN0_DR_A"         -length            0.2 -e0 0.265195388067
Drift      -name "LN0_DR_VS"        -length            0.1 -e0 0.265195388067
Drift      -name "LN0_DR_A"         -length            0.2 -e0 0.265195388067
Drift      -name "LN0_DR_CT"        -length            0.1 -e0 0.265195388067
Drift      -name "LN0_WA_OU2"       -length              0 -e0 0.265195388067
Drift      -name "BC1_DR_V1"        -length 0.538206671373 -e0 0.265195388067
Drift      -name "BC1_DR_QD_ED"     -length         0.0425 -e0 0.265195388067
Quadrupole -name "BC1_QD_V01"       -length           0.04 -e0 0.265195388067 -strength 0.0192008436089
Dipole     -name "BC1_COR"          -length              0 -e0 0.265195388067
Bpm        -name "BC1_BPM"          -length              0 -e0 0.265195388067
Quadrupole -name "BC1_QD_V01"       -length           0.04 -e0 0.265195388067 -strength 0.0192008436089
Drift      -name "BC1_DR_QD_ED"     -length         0.0425 -e0 0.265195388067
Drift      -name "BC1_DR_V2"        -length  5.90034025961 -e0 0.265195388067
Drift      -name "BC1_DR_QD_ED"     -length         0.0425 -e0 0.265195388067
Quadrupole -name "BC1_QD_V02"       -length           0.04 -e0 0.265195388067 -strength -0.0972809339897
Dipole     -name "BC1_COR"          -length              0 -e0 0.265195388067
Bpm        -name "BC1_BPM"          -length              0 -e0 0.265195388067
Quadrupole -name "BC1_QD_V02"       -length           0.04 -e0 0.265195388067 -strength -0.0972809339897
Drift      -name "BC1_DR_QD_ED"     -length         0.0425 -e0 0.265195388067
Drift      -name "BC1_DR_V3"        -length 0.249612802881 -e0 0.265195388067
Drift      -name "BC1_DR_QD_ED"     -length         0.0425 -e0 0.265195388067
Quadrupole -name "BC1_QD_V03"       -length           0.04 -e0 0.265195388067 -strength 0.034050235969
Dipole     -name "BC1_COR"          -length              0 -e0 0.265195388067
Bpm        -name "BC1_BPM"          -length              0 -e0 0.265195388067
Quadrupole -name "BC1_QD_V03"       -length           0.04 -e0 0.265195388067 -strength 0.034050235969
Drift      -name "BC1_DR_QD_ED"     -length         0.0425 -e0 0.265195388067
Drift      -name "BC1_DR_V4"        -length 0.200000016045 -e0 0.265195388067
Drift      -name "BC1_DR_QD_ED"     -length         0.0425 -e0 0.265195388067
Quadrupole -name "BC1_QD_V04"       -length           0.04 -e0 0.265195388067 -strength 0.0564349269803
Dipole     -name "BC1_COR"          -length              0 -e0 0.265195388067
Bpm        -name "BC1_BPM"          -length              0 -e0 0.265195388067
Quadrupole -name "BC1_QD_V04"       -length           0.04 -e0 0.265195388067 -strength 0.0564349269803
Drift      -name "BC1_DR_QD_ED"     -length         0.0425 -e0 0.265195388067
Drift      -name "BC1_DR_V5"        -length 0.200203972325 -e0 0.265195388067
Drift      -name "BC1_DR_20"        -length            0.5 -e0 0.265195388067
Sbend      -name "BC1_DP_DIP1"      -length     0.50032822 -e0 0.265195262975 -angle -0.06274459 -E1 0 -E2 -0.06274459 -hgap 0.015 -fint 0.5 -tilt 0  -six_dim 1 -csr 1 
Drift      -name "BC1_DR_SIDE_C"    -length            0.3 -e0 0.265194815733
Drift      -name "BC1_DR_SIDE"      -length     2.95640795 -e0 0.265194815733
Sbend      -name "BC1_DP_DIP2"      -length     0.50032822 -e0 0.265194280196 -angle 0.06274459 -E1 0.06274459 -E2 0 -hgap 0.015 -fint 0.5 -tilt 0  -six_dim 1 -csr 1 
Drift      -name "BC1_DR_CENT_C"    -length           0.35 -e0 0.265192544639
Bpm        -name "BC1_BPM"          -length              0 -e0 0.265192544639
Drift      -name "BC1_DR_CENT"      -length           0.35 -e0 0.265192544639
Sbend      -name "BC1_DP_DIP3"      -length     0.50032822 -e0 0.265175808035 -angle 0.06274459 -E1 0 -E2 0.06274459 -hgap 0.015 -fint 0.5 -tilt 0  -six_dim 1 -csr 1 
Drift      -name "BC1_DR_SIDE_C"    -length            0.3 -e0 0.265142841113
Drift      -name "BC1_DR_SIDE"      -length     2.95640795 -e0 0.265142841113
Sbend      -name "BC1_DP_DIP4"      -length     0.50032822 -e0 0.265090676962 -angle -0.06274459 -E1 -0.06274459 -E2 0 -hgap 0.015 -fint 0.5 -tilt 0  -six_dim 1 -csr 1 
Drift      -name "BC1_DR_20_C"      -length            0.3 -e0 0.265065428708
Drift      -name "BC1_WA_OU2"       -length              0 -e0 0.265065428708
Drift      -name "BC1_DR_CT"        -length            0.1 -e0 0.265065428708
Drift      -name "BC1_DR_DI_V1"     -length 0.291167237663 -e0 0.265065428708
Drift      -name "BC1_DR_QD_ED"     -length         0.0425 -e0 0.265065428708
Quadrupole -name "BC1_QD_DI_V01"    -length           0.04 -e0 0.265065428708 -strength -0.0916553543449
Dipole     -name "BC1_COR_DI"       -length              0 -e0 0.265065428708
Bpm        -name "BC1_BPM_DI"       -length              0 -e0 0.265065428708
Quadrupole -name "BC1_QD_DI_V01"    -length           0.04 -e0 0.265065428708 -strength -0.0916553543449
Drift      -name "BC1_DR_QD_ED"     -length         0.0425 -e0 0.265065428708
Drift      -name "BC1_DR_DI_V2"     -length 0.470002000765 -e0 0.265065428708
Drift      -name "BC1_DR_QD_ED"     -length         0.0425 -e0 0.265065428708
Quadrupole -name "BC1_QD_DI_V02"    -length           0.04 -e0 0.265065428708 -strength 0.143288243201
Dipole     -name "BC1_COR_DI"       -length              0 -e0 0.265065428708
Bpm        -name "BC1_BPM_DI"       -length              0 -e0 0.265065428708
Quadrupole -name "BC1_QD_DI_V02"    -length           0.04 -e0 0.265065428708 -strength 0.143288243201
Drift      -name "BC1_DR_QD_ED"     -length         0.0425 -e0 0.265065428708
Drift      -name "BC1_DR_DI_V3"     -length  1.19908280027 -e0 0.265065428708
Drift      -name "BC1_DR_QD_ED"     -length         0.0425 -e0 0.265065428708
Quadrupole -name "BC1_QD_DI_V03"    -length           0.04 -e0 0.265065428708 -strength -0.0533247748135
Dipole     -name "BC1_COR_DI"       -length              0 -e0 0.265065428708
Bpm        -name "BC1_BPM_DI"       -length              0 -e0 0.265065428708
Quadrupole -name "BC1_QD_DI_V03"    -length           0.04 -e0 0.265065428708 -strength -0.0533247748135
Drift      -name "BC1_DR_QD_ED"     -length         0.0425 -e0 0.265065428708
Drift      -name "BC1_DR_DI_V5"     -length            0.2 -e0 0.265065428708
Drift      -name "BC1_DR_DI"        -length           0.25 -e0 0.265065428708
Drift      -name "BC1_DR_VS"        -length            0.1 -e0 0.265065428708
Drift      -name "BC1_DR_BL"        -length           0.05 -e0 0.265065428708
Drift      -name "BC1_DR_XCA_ED_DI" -length      0.1001043 -e0 0.265065428708
CrabCavity -name "BC1_XCA0_DI"      -length      0.9997914 -voltage 0 -phase 90 -frequency 11.9942
SetReferenceEnergy  2.650654287076047e-01
Drift      -name "BC1_DR_XCA_ED_DI" -length      0.1001043 -e0 0.264994470248
Drift      -name "BC1_DR_BL"        -length           0.05 -e0 0.264994470248
Drift      -name "BC1_DR_VS"        -length            0.1 -e0 0.264994470248
Drift      -name "BC1_DR_DI"        -length           0.25 -e0 0.264994470248
Drift      -name "BC1_DR_DI"        -length           0.25 -e0 0.264994470248
Drift      -name "BC1_DR_DI_QD_ED"  -length         0.0425 -e0 0.264994470248
Quadrupole -name "BC1_QD_DI_FH"     -length           0.04 -e0 0.264994470248 -strength 0.0847982304795
Dipole     -name "BC1_COR_DI"       -length              0 -e0 0.264994470248
Bpm        -name "BC1_BPM_DI"       -length              0 -e0 0.264994470248
Quadrupole -name "BC1_QD_DI_FH"     -length           0.04 -e0 0.264994470248 -strength 0.0847982304795
Drift      -name "BC1_DR_DI_QD_ED"  -length         0.0425 -e0 0.264994470248
Drift      -name "BC1_DR_FODO"      -length           0.15 -e0 0.264994470248
Drift      -name "BC1_DR_FODO"      -length           0.15 -e0 0.264994470248
Drift      -name "BC1_DR_FODO"      -length           0.15 -e0 0.264994470248
Drift      -name "BC1_DR_FODO"      -length           0.15 -e0 0.264994470248
Drift      -name "BC1_DR_DI_VS"     -length            0.1 -e0 0.264994470248
Drift      -name "BC1_DR_FODO"      -length           0.15 -e0 0.264994470248
Drift      -name "BC1_DR_FODO"      -length           0.15 -e0 0.264994470248
Drift      -name "BC1_DR_FODO"      -length           0.15 -e0 0.264994470248
Drift      -name "BC1_DR_FODO"      -length           0.15 -e0 0.264994470248
Drift      -name "BC1_DR_DI_QD_ED"  -length         0.0425 -e0 0.264994470248
Quadrupole -name "BC1_QD_DI_DH"     -length           0.04 -e0 0.264994470248 -strength -0.0847982304795
Dipole     -name "BC1_COR_DI"       -length              0 -e0 0.264994470248
Bpm        -name "BC1_BPM_DI"       -length              0 -e0 0.264994470248
Quadrupole -name "BC1_QD_DI_DH"     -length           0.04 -e0 0.264994470248 -strength -0.0847982304795
Drift      -name "BC1_DR_DI_QD_ED"  -length         0.0425 -e0 0.264994470248
Drift      -name "BC1_DR_FODO"      -length           0.15 -e0 0.264994470248
Drift      -name "BC1_DR_FODO"      -length           0.15 -e0 0.264994470248
Drift      -name "BC1_DR_FODO"      -length           0.15 -e0 0.264994470248
Drift      -name "BC1_DR_FODO"      -length           0.15 -e0 0.264994470248
Drift      -name "BC1_DR_DI_VS"     -length            0.1 -e0 0.264994470248
Drift      -name "BC1_DR_FODO"      -length           0.15 -e0 0.264994470248
Drift      -name "BC1_DR_FODO"      -length           0.15 -e0 0.264994470248
Drift      -name "BC1_DR_FODO"      -length           0.15 -e0 0.264994470248
Drift      -name "BC1_DR_FODO"      -length           0.15 -e0 0.264994470248
Drift      -name "BC1_DR_DI_QD_ED"  -length         0.0425 -e0 0.264994470248
Quadrupole -name "BC1_QD_DI_FH"     -length           0.04 -e0 0.264994470248 -strength 0.0847982304795
Dipole     -name "BC1_COR_DI"       -length              0 -e0 0.264994470248
Bpm        -name "BC1_BPM_DI"       -length              0 -e0 0.264994470248
Quadrupole -name "BC1_QD_DI_FH"     -length           0.04 -e0 0.264994470248 -strength 0.0847982304795
Drift      -name "BC1_DR_DI_QD_ED"  -length         0.0425 -e0 0.264994470248
Drift      -name "BC1_DR_FODO"      -length           0.15 -e0 0.264994470248
Drift      -name "BC1_DR_FODO"      -length           0.15 -e0 0.264994470248
Drift      -name "BC1_DR_FODO"      -length           0.15 -e0 0.264994470248
Drift      -name "BC1_DR_FODO"      -length           0.15 -e0 0.264994470248
Drift      -name "BC1_DR_DI_VS"     -length            0.1 -e0 0.264994470248
Drift      -name "BC1_DR_FODO"      -length           0.15 -e0 0.264994470248
Drift      -name "BC1_DR_FODO"      -length           0.15 -e0 0.264994470248
Drift      -name "BC1_DR_FODO"      -length           0.15 -e0 0.264994470248
Drift      -name "BC1_DR_FODO"      -length           0.15 -e0 0.264994470248
Drift      -name "BC1_DR_DI_QD_ED"  -length         0.0425 -e0 0.264994470248
Quadrupole -name "BC1_QD_DI_DH"     -length           0.04 -e0 0.264994470248 -strength -0.0847982304795
Dipole     -name "BC1_COR_DI"       -length              0 -e0 0.264994470248
Bpm        -name "BC1_BPM_DI"       -length              0 -e0 0.264994470248
Quadrupole -name "BC1_QD_DI_DH"     -length           0.04 -e0 0.264994470248 -strength -0.0847982304795
Drift      -name "BC1_DR_DI_QD_ED"  -length         0.0425 -e0 0.264994470248
Drift      -name "BC1_DR_FODO"      -length           0.15 -e0 0.264994470248
Drift      -name "BC1_DR_FODO"      -length           0.15 -e0 0.264994470248
Drift      -name "BC1_DR_FODO"      -length           0.15 -e0 0.264994470248
Drift      -name "BC1_DR_FODO"      -length           0.15 -e0 0.264994470248
Drift      -name "BC1_DR_DI_VS"     -length            0.1 -e0 0.264994470248
Drift      -name "BC1_DR_FODO"      -length           0.15 -e0 0.264994470248
Drift      -name "BC1_DR_FODO"      -length           0.15 -e0 0.264994470248
Drift      -name "BC1_DR_FODO"      -length           0.15 -e0 0.264994470248
Drift      -name "BC1_DR_FODO"      -length           0.15 -e0 0.264994470248
Drift      -name "BC1_DR_DI_QD_ED"  -length         0.0425 -e0 0.264994470248
Quadrupole -name "BC1_QD_DI_FH"     -length           0.04 -e0 0.264994470248 -strength 0.0847982304795
Dipole     -name "BC1_COR_DI"       -length              0 -e0 0.264994470248
Bpm        -name "BC1_BPM_DI"       -length              0 -e0 0.264994470248
Quadrupole -name "BC1_QD_DI_FH"     -length           0.04 -e0 0.264994470248 -strength 0.0847982304795
Drift      -name "BC1_DR_DI_QD_ED"  -length         0.0425 -e0 0.264994470248
Drift      -name "BC1_DR_DI"        -length           0.25 -e0 0.264994470248
Sbend      -name "BC1_DP_DI"        -length           0.25 -e0 0.264994470248 -angle 0 -E1 0 -E2 0 -hgap 0 -fint 0.5 -tilt 0 -six_dim 1
Drift      -name "BC1_DR_DI"        -length           0.25 -e0 0.264994470248
Drift      -name "BC1_DR_DI"        -length           0.25 -e0 0.264994470248
Drift      -name "BC1_DR_DI"        -length           0.25 -e0 0.264994470248
Drift      -name "BC1_WA_OU_DI2"    -length              0 -e0 0.264994470248
Drift      -name "LN1_DR_V1"        -length 0.210843211803 -e0 0.264994470248
Drift      -name "LN1_DR_QD_ED"     -length         0.0425 -e0 0.264994470248
Quadrupole -name "LN1_QD_V01"       -length           0.04 -e0 0.264994470248 -strength 0.0561740333174
Dipole     -name "LN1_COR"          -length              0 -e0 0.264994470248
Bpm        -name "LN1_BPM"          -length              0 -e0 0.264994470248
Quadrupole -name "LN1_QD_V01"       -length           0.04 -e0 0.264994470248 -strength 0.0561740333174
Drift      -name "LN1_DR_QD_ED"     -length         0.0425 -e0 0.264994470248
Drift      -name "LN1_DR_V2"        -length 0.248185807068 -e0 0.264994470248
Drift      -name "LN1_DR_QD_ED"     -length         0.0425 -e0 0.264994470248
Quadrupole -name "LN1_QD_V02"       -length           0.04 -e0 0.264994470248 -strength -0.114490110749
Dipole     -name "LN1_COR"          -length              0 -e0 0.264994470248
Bpm        -name "LN1_BPM"          -length              0 -e0 0.264994470248
Quadrupole -name "LN1_QD_V02"       -length           0.04 -e0 0.264994470248 -strength -0.114490110749
Drift      -name "LN1_DR_QD_ED"     -length         0.0425 -e0 0.264994470248
Drift      -name "LN1_DR_V3"        -length  1.89243764422 -e0 0.264994470248
Drift      -name "LN1_DR_QD_ED"     -length         0.0425 -e0 0.264994470248
Quadrupole -name "LN1_QD_V03"       -length           0.04 -e0 0.264994470248 -strength 0.0130583903574
Dipole     -name "LN1_COR"          -length              0 -e0 0.264994470248
Bpm        -name "LN1_BPM"          -length              0 -e0 0.264994470248
Quadrupole -name "LN1_QD_V03"       -length           0.04 -e0 0.264994470248 -strength 0.0130583903574
Drift      -name "LN1_DR_QD_ED"     -length         0.0425 -e0 0.264994470248
Drift      -name "LN1_DR_V4"        -length 0.130246090585 -e0 0.264994470248
Drift      -name "LN1_DR_QD_ED"     -length         0.0425 -e0 0.264994470248
Quadrupole -name "LN1_QD_FH"        -length           0.04 -e0 0.264994470248 -strength 0.0699585401456
Dipole     -name "LN1_COR"          -length              0 -e0 0.264994470248
Bpm        -name "LN1_BPM"          -length              0 -e0 0.264994470248
Quadrupole -name "LN1_QD_FH"        -length           0.04 -e0 0.264994470248 -strength 0.0699585401456
Drift      -name "LN1_DR_QD_ED"     -length         0.0425 -e0 0.264994470248
Drift      -name "LN1_DR_SV"        -length            0.1 -e0 0.264994470248
Drift      -name "LN1_DR_BL"        -length           0.05 -e0 0.264994470248
Drift      -name "LN1_DR_XCA_ED"    -length     0.05676225 -e0 0.264994470248
Cavity     -name "LN1_XCA0"         -length      0.9164755 -gradient 0.065 -phase 34.6878166718 -frequency 11.9942
SetReferenceEnergy  3.138925686282544e-01
Drift      -name "LN1_DR_XCA_ED"    -length     0.05676225 -e0 0.313892568628
Drift      -name "LN1_DR_BL"        -length           0.05 -e0 0.313892568628
Drift      -name "LN1_DR_XCA_ED"    -length     0.05676225 -e0 0.313892568628
Cavity     -name "LN1_XCA0"         -length      0.9164755 -gradient 0.065 -phase 34.6878166718 -frequency 11.9942
SetReferenceEnergy  3.627906557283072e-01
Drift      -name "LN1_DR_XCA_ED"    -length     0.05676225 -e0 0.362790655728
Drift      -name "LN1_DR_BL"        -length           0.05 -e0 0.362790655728
Drift      -name "LN1_DR_QD_ED"     -length         0.0425 -e0 0.362790655728
Quadrupole -name "LN1_QD_DH"        -length           0.04 -e0 0.362790655728 -strength -0.0957767331123
Dipole     -name "LN1_COR"          -length              0 -e0 0.362790655728
Bpm        -name "LN1_BPM"          -length              0 -e0 0.362790655728
Quadrupole -name "LN1_QD_DH"        -length           0.04 -e0 0.362790655728 -strength -0.0957767331123
Drift      -name "LN1_DR_QD_ED"     -length         0.0425 -e0 0.362790655728
Drift      -name "LN1_DR_VS"        -length            0.1 -e0 0.362790655728
Drift      -name "LN1_DR_BL"        -length           0.05 -e0 0.362790655728
Drift      -name "LN1_DR_XCA_ED"    -length     0.05676225 -e0 0.362790655728
Cavity     -name "LN1_XCA0"         -length      0.9164755 -gradient 0.065 -phase 34.6878166718 -frequency 11.9942
SetReferenceEnergy  4.116887326362105e-01
Drift      -name "LN1_DR_XCA_ED"    -length     0.05676225 -e0 0.411688732636
Drift      -name "LN1_DR_BL"        -length           0.05 -e0 0.411688732636
Drift      -name "LN1_DR_XCA_ED"    -length     0.05676225 -e0 0.411688732636
Cavity     -name "LN1_XCA0"         -length      0.9164755 -gradient 0.065 -phase 34.6878166718 -frequency 11.9942
SetReferenceEnergy  4.605868045714687e-01
Drift      -name "LN1_DR_XCA_ED"    -length     0.05676225 -e0 0.460586804571
Drift      -name "LN1_DR_BL"        -length           0.05 -e0 0.460586804571
Drift      -name "LN1_DR_QD_ED"     -length         0.0425 -e0 0.460586804571
Quadrupole -name "LN1_QD_FH"        -length           0.04 -e0 0.460586804571 -strength 0.121594916407
Dipole     -name "LN1_COR"          -length              0 -e0 0.460586804571
Bpm        -name "LN1_BPM"          -length              0 -e0 0.460586804571
Quadrupole -name "LN1_QD_FH"        -length           0.04 -e0 0.460586804571 -strength 0.121594916407
Drift      -name "LN1_DR_QD_ED"     -length         0.0425 -e0 0.460586804571
Drift      -name "LN1_DR_SV"        -length            0.1 -e0 0.460586804571
Drift      -name "LN1_DR_BL"        -length           0.05 -e0 0.460586804571
Drift      -name "LN1_DR_XCA_ED"    -length     0.05676225 -e0 0.460586804571
Cavity     -name "LN1_XCA0"         -length      0.9164755 -gradient 0.065 -phase 34.6878166718 -frequency 11.9942
SetReferenceEnergy  5.094848715377877e-01
Drift      -name "LN1_DR_XCA_ED"    -length     0.05676225 -e0 0.509484871538
Drift      -name "LN1_DR_BL"        -length           0.05 -e0 0.509484871538
Drift      -name "LN1_DR_XCA_ED"    -length     0.05676225 -e0 0.509484871538
Cavity     -name "LN1_XCA0"         -length      0.9164755 -gradient 0.065 -phase 34.6878166718 -frequency 11.9942
SetReferenceEnergy  5.583829352631651e-01
Drift      -name "LN1_DR_XCA_ED"    -length     0.05676225 -e0 0.558382935263
Drift      -name "LN1_DR_BL"        -length           0.05 -e0 0.558382935263
Drift      -name "LN1_DR_QD_ED"     -length         0.0425 -e0 0.558382935263
Quadrupole -name "LN1_QD_DH"        -length           0.04 -e0 0.558382935263 -strength -0.147413094909
Dipole     -name "LN1_COR"          -length              0 -e0 0.558382935263
Bpm        -name "LN1_BPM"          -length              0 -e0 0.558382935263
Quadrupole -name "LN1_QD_DH"        -length           0.04 -e0 0.558382935263 -strength -0.147413094909
Drift      -name "LN1_DR_QD_ED"     -length         0.0425 -e0 0.558382935263
Drift      -name "LN1_DR_VS"        -length            0.1 -e0 0.558382935263
Drift      -name "LN1_DR_BL"        -length           0.05 -e0 0.558382935263
Drift      -name "LN1_DR_XCA_ED"    -length     0.05676225 -e0 0.558382935263
Cavity     -name "LN1_XCA0"         -length      0.9164755 -gradient 0.065 -phase 34.6878166718 -frequency 11.9942
SetReferenceEnergy  6.072809957916236e-01
Drift      -name "LN1_DR_XCA_ED"    -length     0.05676225 -e0 0.607280995792
Drift      -name "LN1_DR_BL"        -length           0.05 -e0 0.607280995792
Drift      -name "LN1_DR_XCA_ED"    -length     0.05676225 -e0 0.607280995792
Cavity     -name "LN1_XCA0"         -length      0.9164755 -gradient 0.065 -phase 34.6878166718 -frequency 11.9942
SetReferenceEnergy  6.561790547670641e-01
Drift      -name "LN1_DR_XCA_ED"    -length     0.05676225 -e0 0.656179054767
Drift      -name "LN1_DR_BL"        -length           0.05 -e0 0.656179054767
Drift      -name "LN1_DR_QD_ED"     -length         0.0425 -e0 0.656179054767
Quadrupole -name "LN1_QD_FH"        -length           0.04 -e0 0.656179054767 -strength 0.173231270459
Dipole     -name "LN1_COR"          -length              0 -e0 0.656179054767
Bpm        -name "LN1_BPM"          -length              0 -e0 0.656179054767
Quadrupole -name "LN1_QD_FH"        -length           0.04 -e0 0.656179054767 -strength 0.173231270459
Drift      -name "LN1_DR_QD_ED"     -length         0.0425 -e0 0.656179054767
Drift      -name "LN1_DR_SV"        -length            0.1 -e0 0.656179054767
Drift      -name "LN1_DR_BL"        -length           0.05 -e0 0.656179054767
Drift      -name "LN1_DR_XCA_ED"    -length     0.05676225 -e0 0.656179054767
Cavity     -name "LN1_XCA0"         -length      0.9164755 -gradient 0.065 -phase 34.6878166718 -frequency 11.9942
SetReferenceEnergy  7.050771117445136e-01
Drift      -name "LN1_DR_XCA_ED"    -length     0.05676225 -e0 0.705077111745
Drift      -name "LN1_DR_BL"        -length           0.05 -e0 0.705077111745
Drift      -name "LN1_DR_XCA_ED"    -length     0.05676225 -e0 0.705077111745
Cavity     -name "LN1_XCA0"         -length      0.9164755 -gradient 0.065 -phase 34.6878166718 -frequency 11.9942
SetReferenceEnergy  7.539751674562016e-01
Drift      -name "LN1_DR_XCA_ED"    -length     0.05676225 -e0 0.753975167456
Drift      -name "LN1_DR_BL"        -length           0.05 -e0 0.753975167456
Drift      -name "LN1_DR_QD_ED"     -length         0.0425 -e0 0.753975167456
Quadrupole -name "LN1_QD_DH"        -length           0.04 -e0 0.753975167456 -strength -0.199049444208
Dipole     -name "LN1_COR"          -length              0 -e0 0.753975167456
Bpm        -name "LN1_BPM"          -length              0 -e0 0.753975167456
Quadrupole -name "LN1_QD_DH"        -length           0.04 -e0 0.753975167456 -strength -0.199049444208
Drift      -name "LN1_DR_QD_ED"     -length         0.0425 -e0 0.753975167456
Drift      -name "LN1_DR_VS"        -length            0.1 -e0 0.753975167456
Drift      -name "LN1_DR_BL"        -length           0.05 -e0 0.753975167456
Drift      -name "LN1_DR_XCA_ED"    -length     0.05676225 -e0 0.753975167456
Cavity     -name "LN1_XCA0"         -length      0.9164755 -gradient 0.065 -phase 34.6878166718 -frequency 11.9942
SetReferenceEnergy  8.028732214438555e-01
Drift      -name "LN1_DR_XCA_ED"    -length     0.05676225 -e0 0.802873221444
Drift      -name "LN1_DR_BL"        -length           0.05 -e0 0.802873221444
Drift      -name "LN1_DR_XCA_ED"    -length     0.05676225 -e0 0.802873221444
Cavity     -name "LN1_XCA0"         -length      0.9164755 -gradient 0.065 -phase 34.6878166718 -frequency 11.9942
SetReferenceEnergy  8.517712746638989e-01
Drift      -name "LN1_DR_XCA_ED"    -length     0.05676225 -e0 0.851771274664
Drift      -name "LN1_DR_BL"        -length           0.05 -e0 0.851771274664
Drift      -name "LN1_DR_QD_ED"     -length         0.0425 -e0 0.851771274664
Quadrupole -name "LN1_QD_FH"        -length           0.04 -e0 0.851771274664 -strength 0.224867616511
Dipole     -name "LN1_COR"          -length              0 -e0 0.851771274664
Bpm        -name "LN1_BPM"          -length              0 -e0 0.851771274664
Quadrupole -name "LN1_QD_FH"        -length           0.04 -e0 0.851771274664 -strength 0.224867616511
Drift      -name "LN1_DR_QD_ED"     -length         0.0425 -e0 0.851771274664
Drift      -name "LN1_DR_SV"        -length            0.1 -e0 0.851771274664
Drift      -name "LN1_DR_BL"        -length           0.05 -e0 0.851771274664
Drift      -name "LN1_DR_XCA_ED"    -length     0.05676225 -e0 0.851771274664
Cavity     -name "LN1_XCA0"         -length      0.9164755 -gradient 0.065 -phase 34.6878166718 -frequency 11.9942
SetReferenceEnergy  9.006693271390552e-01
Drift      -name "LN1_DR_XCA_ED"    -length     0.05676225 -e0 0.900669327139
Drift      -name "LN1_DR_BL"        -length           0.05 -e0 0.900669327139
Drift      -name "LN1_DR_XCA_ED"    -length     0.05676225 -e0 0.900669327139
Cavity     -name "LN1_XCA0"         -length      0.9164755 -gradient 0.065 -phase 34.6878166718 -frequency 11.9942
SetReferenceEnergy  9.495673790066174e-01
Drift      -name "LN1_DR_XCA_ED"    -length     0.05676225 -e0 0.949567379007
Drift      -name "LN1_DR_BL"        -length           0.05 -e0 0.949567379007
Drift      -name "LN1_DR_QD_ED"     -length         0.0425 -e0 0.949567379007
Quadrupole -name "LN1_QD_DH"        -length           0.04 -e0 0.949567379007 -strength -0.250685788058
Dipole     -name "LN1_COR"          -length              0 -e0 0.949567379007
Bpm        -name "LN1_BPM"          -length              0 -e0 0.949567379007
Quadrupole -name "LN1_QD_DH"        -length           0.04 -e0 0.949567379007 -strength -0.250685788058
Drift      -name "LN1_DR_QD_ED"     -length         0.0425 -e0 0.949567379007
Drift      -name "LN1_DR_VS"        -length            0.1 -e0 0.949567379007
Drift      -name "LN1_DR_BL"        -length           0.05 -e0 0.949567379007
Drift      -name "LN1_DR_XCA_ED"    -length     0.05676225 -e0 0.949567379007
Cavity     -name "LN1_XCA0"         -length      0.9164755 -gradient 0.065 -phase 34.6878166718 -frequency 11.9942
SetReferenceEnergy  9.984654303318296e-01
Drift      -name "LN1_DR_XCA_ED"    -length     0.05676225 -e0 0.998465430332
Drift      -name "LN1_DR_BL"        -length           0.05 -e0 0.998465430332
Drift      -name "LN1_DR_XCA_ED"    -length     0.05676225 -e0 0.998465430332
Cavity     -name "LN1_XCA0"         -length      0.9164755 -gradient 0.065 -phase 34.6878166718 -frequency 11.9942
SetReferenceEnergy  1.047363480841825e+00
Drift      -name "LN1_DR_XCA_ED"    -length     0.05676225 -e0  1.04736348084
Drift      -name "LN1_DR_BL"        -length           0.05 -e0  1.04736348084
Drift      -name "LN1_DR_QD_ED"     -length         0.0425 -e0  1.04736348084
Quadrupole -name "LN1_QD_FH"        -length           0.04 -e0  1.04736348084 -strength 0.276503958942
Dipole     -name "LN1_COR"          -length              0 -e0  1.04736348084
Bpm        -name "LN1_BPM"          -length              0 -e0  1.04736348084
Quadrupole -name "LN1_QD_FH"        -length           0.04 -e0  1.04736348084 -strength 0.276503958942
Drift      -name "LN1_DR_QD_ED"     -length         0.0425 -e0  1.04736348084
Drift      -name "LN1_DR_DR"        -length            0.1 -e0  1.04736348084
Drift      -name "LN1_DR_BL"        -length           0.05 -e0  1.04736348084
Drift      -name "LN1_DR_XCA_ED"    -length     0.05676225 -e0  1.04736348084
Drift      -name "LN1_DR_XCA_"      -length      0.9164755 -e0  1.04736348084
Drift      -name "LN1_DR_XCA_ED"    -length     0.05676225 -e0  1.04736348084
Drift      -name "LN1_DR_BL"        -length           0.05 -e0  1.04736348084
Drift      -name "LN1_DR_XCA_ED"    -length     0.05676225 -e0  1.04736348084
Drift      -name "LN1_DR_XCA_"      -length      0.9164755 -e0  1.04736348084
Drift      -name "LN1_DR_XCA_ED"    -length     0.05676225 -e0  1.04736348084
Drift      -name "LN1_DR_BL"        -length           0.05 -e0  1.04736348084
Drift      -name "LN1_DR_QD_ED"     -length         0.0425 -e0  1.04736348084
Quadrupole -name "LN1_QD_DH"        -length           0.04 -e0  1.04736348084 -strength -0.276503958942
Dipole     -name "LN1_COR"          -length              0 -e0  1.04736348084
Bpm        -name "LN1_BPM"          -length              0 -e0  1.04736348084
Quadrupole -name "LN1_QD_DH"        -length           0.04 -e0  1.04736348084 -strength -0.276503958942
Drift      -name "LN1_DR_QD_ED"     -length         0.0425 -e0  1.04736348084
Drift      -name "LN1_DR_DR"        -length            0.1 -e0  1.04736348084
Drift      -name "LN1_DR_BL"        -length           0.05 -e0  1.04736348084
Drift      -name "LN1_DR_XCA_ED"    -length     0.05676225 -e0  1.04736348084
Drift      -name "LN1_DR_XCA_A"     -length      0.8164755 -e0  1.04736348084
Drift      -name "LN1_DR_VS"        -length            0.1 -e0  1.04736348084
Drift      -name "LN1_WA_OU2"       -length              0 -e0  1.04736348084
Drift      -name "BC2_IN_CNT"       -length              0 -e0  1.04736348084
Drift      -name "BC2_DR_V1"        -length 0.471866672144 -e0  1.04736348084
Drift      -name "BC2_DR_QD_ED"     -length         0.0425 -e0  1.04736348084
Quadrupole -name "BC2_QD_V01"       -length           0.04 -e0  1.04736348084 -strength 0.0143562291008
Dipole     -name "BC2_COR"          -length              0 -e0  1.04736348084
Bpm        -name "BC2_BPM"          -length              0 -e0  1.04736348084
Quadrupole -name "BC2_QD_V01"       -length           0.04 -e0  1.04736348084 -strength 0.0143562291008
Drift      -name "BC2_DR_QD_ED"     -length         0.0425 -e0  1.04736348084
Drift      -name "BC2_DR_V2"        -length  5.87242259478 -e0  1.04736348084
Drift      -name "BC2_DR_QD_ED"     -length         0.0425 -e0  1.04736348084
Quadrupole -name "BC2_QD_V02"       -length           0.04 -e0  1.04736348084 -strength -0.479104266173
Dipole     -name "BC2_COR"          -length              0 -e0  1.04736348084
Bpm        -name "BC2_BPM"          -length              0 -e0  1.04736348084
Quadrupole -name "BC2_QD_V02"       -length           0.04 -e0  1.04736348084 -strength -0.479104266173
Drift      -name "BC2_DR_QD_ED"     -length         0.0425 -e0  1.04736348084
Drift      -name "BC2_DR_V3"        -length 0.200000001011 -e0  1.04736348084
Drift      -name "BC2_DR_QD_ED"     -length         0.0425 -e0  1.04736348084
Quadrupole -name "BC2_QD_V03"       -length           0.04 -e0  1.04736348084 -strength 0.447113384893
Dipole     -name "BC2_COR"          -length              0 -e0  1.04736348084
Bpm        -name "BC2_BPM"          -length              0 -e0  1.04736348084
Quadrupole -name "BC2_QD_V03"       -length           0.04 -e0  1.04736348084 -strength 0.447113384893
Drift      -name "BC2_DR_QD_ED"     -length         0.0425 -e0  1.04736348084
Drift      -name "BC2_DR_V4"        -length            0.2 -e0  1.04736348084
Drift      -name "BC2_DR_20"        -length            0.5 -e0  1.04736348084
Sbend      -name "BC2_DP_DIP1"      -length     0.60004387 -e0  1.04735037701 -angle -0.02094395 -E1 0 -E2 -0.02094395 -hgap 0 -fint 0.5 -tilt 0  -six_dim 1 -csr 1 
Drift      -name "BC2_DR_SIDE_C"    -length            0.5 -e0  1.04732731956
Drift      -name "BC2_DR_SIDE"      -length     3.20081165 -e0  1.04732731956
Sbend      -name "BC2_DP_DIP2"      -length     0.60004387 -e0  1.04729506608 -angle 0.02094395 -E1 0.02094395 -E2 0 -hgap 0 -fint 0.5 -tilt 0  -six_dim 1 -csr 1 
Drift      -name "BC2_DR_CENT_C"    -length           0.25 -e0  1.04726620697
Bpm        -name "BC2_BPM"          -length              0 -e0  1.04726620697
Drift      -name "BC2_DR_CENT"      -length           0.25 -e0  1.04726620697
Sbend      -name "BC2_DP_DIP3"      -length     0.60004387 -e0  1.04698675127 -angle 0.02094395 -E1 0 -E2 0.02094395 -hgap 0 -fint 0.5 -tilt 0  -six_dim 1 -csr 1 
Drift      -name "BC2_DR_SIDE_C"    -length            0.5 -e0  1.04657717974
Drift      -name "BC2_DR_SIDE"      -length     3.20081165 -e0  1.04657717974
Sbend      -name "BC2_DP_DIP4"      -length     0.60004387 -e0  1.04591637958 -angle -0.02094395 -E1 -0.02094395 -E2 0 -hgap 0 -fint 0.5 -tilt 0  -six_dim 1 -csr 1 
Drift      -name "BC2_IN_CNT"       -length              0 -e0  1.04591637958
Drift      -name "BC2_DR_20_C"      -length            0.5 -e0  1.04555776363
Drift      -name "BC2_IN_CNT"       -length              0 -e0  1.04555776363
Drift      -name "BC2_WA_OU2"       -length              0 -e0  1.04555776363

Drift      -name "LN2_DR_CT"        -length            0.1 -e0  1.04555776363
Drift      -name "LN2_DR_V1"        -length  2.29482966931 -e0  1.04555776363
Drift      -name "LN2_DR_QD_ED"     -length         0.0425 -e0  1.04555776363
Quadrupole -name "LN2_QD_V01"       -length           0.04 -e0  1.04555776363 -strength  0.56808418511
Dipole     -name "LN2_COR"          -length              0 -e0  1.04555776363
Bpm        -name "LN2_BPM"          -length              0 -e0  1.04555776363
Quadrupole -name "LN2_QD_V01"       -length           0.04 -e0  1.04555776363 -strength  0.56808418511
Drift      -name "LN2_DR_QD_ED"     -length         0.0425 -e0  1.04555776363
Drift      -name "LN2_DR_V2"        -length 0.509089064773 -e0  1.04555776363
Drift      -name "LN2_DR_QD_ED"     -length         0.0425 -e0  1.04555776363
Quadrupole -name "LN2_QD_V02"       -length           0.04 -e0  1.04555776363 -strength  -0.5446586931
Dipole     -name "LN2_COR"          -length              0 -e0  1.04555776363
Bpm        -name "LN2_BPM"          -length              0 -e0  1.04555776363
Quadrupole -name "LN2_QD_V02"       -length           0.04 -e0  1.04555776363 -strength  -0.5446586931
Drift      -name "LN2_DR_QD_ED"     -length         0.0425 -e0  1.04555776363
Drift      -name "LN2_DR_V3"        -length  1.49046353232 -e0  1.04555776363
Drift      -name "LN2_DR_QD_ED"     -length         0.0425 -e0  1.04555776363
Quadrupole -name "LN2_QD_V03"       -length           0.04 -e0  1.04555776363 -strength 0.372500100816
Dipole     -name "LN2_COR"          -length              0 -e0  1.04555776363
Bpm        -name "LN2_BPM"          -length              0 -e0  1.04555776363
Quadrupole -name "LN2_QD_V03"       -length           0.04 -e0  1.04555776363 -strength 0.372500100816
Drift      -name "LN2_DR_QD_ED"     -length         0.0425 -e0  1.04555776363
Drift      -name "LN2_DR_V4"        -length  2.99993932723 -e0  1.04555776363
Drift      -name "LN2_DR_VS"        -length            0.1 -e0  1.04555776363
Drift      -name "LN2_DR_QD_ED"     -length         0.0425 -e0  1.04555776363
Quadrupole -name "LN2_QD_FH"        -length           0.04 -e0  1.04555776363 -strength 0.280209480652
Dipole     -name "LN2_COR"          -length              0 -e0  1.04555776363
Bpm        -name "LN2_BPM"          -length              0 -e0  1.04555776363
Quadrupole -name "LN2_QD_FH"        -length           0.04 -e0  1.04555776363 -strength 0.280209480652
Drift      -name "LN2_DR_QD_ED"     -length         0.0425 -e0  1.04555776363
Drift      -name "LN2_DR_SV"        -length            0.1 -e0  1.04555776363
Drift      -name "LN2_DR_BL"        -length           0.05 -e0  1.04555776363
Drift      -name "LN2_DR_XCA_ED"    -length     0.05676225 -e0  1.04555776363
Cavity     -name "LN2_XCA0"         -length      0.9164755 -gradient 0.065 -phase 10 -frequency 11.9942
SetReferenceEnergy  1.104126273952895e+00
Drift      -name "LN2_DR_XCA_ED"    -length     0.05676225 -e0  1.10412627395
Drift      -name "LN2_DR_BL"        -length           0.05 -e0  1.10412627395
Drift      -name "LN2_DR_XCA_ED"    -length     0.05676225 -e0  1.10412627395
Cavity     -name "LN2_XCA0"         -length      0.9164755 -gradient 0.065 -phase 10 -frequency 11.9942
SetReferenceEnergy  1.162694781222500e+00
Drift      -name "LN2_DR_XCA_ED"    -length     0.05676225 -e0  1.16269478122
Drift      -name "LN2_DR_BL"        -length           0.05 -e0  1.16269478122
Drift      -name "LN2_DR_QD_ED"     -length         0.0425 -e0  1.16269478122
Quadrupole -name "LN2_QD_DH"        -length           0.04 -e0  1.16269478122 -strength -0.311602201368
Dipole     -name "LN2_COR"          -length              0 -e0  1.16269478122
Bpm        -name "LN2_BPM"          -length              0 -e0  1.16269478122
Quadrupole -name "LN2_QD_DH"        -length           0.04 -e0  1.16269478122 -strength -0.311602201368
Drift      -name "LN2_DR_QD_ED"     -length         0.0425 -e0  1.16269478122
Drift      -name "LN2_DR_VS"        -length            0.1 -e0  1.16269478122
Drift      -name "LN2_DR_BL"        -length           0.05 -e0  1.16269478122
Drift      -name "LN2_DR_XCA_ED"    -length     0.05676225 -e0  1.16269478122
Cavity     -name "LN2_XCA0"         -length      0.9164755 -gradient 0.065 -phase 10 -frequency 11.9942
SetReferenceEnergy  1.221263285268904e+00
Drift      -name "LN2_DR_XCA_ED"    -length     0.05676225 -e0  1.22126328527
Drift      -name "LN2_DR_BL"        -length           0.05 -e0  1.22126328527
Drift      -name "LN2_DR_XCA_ED"    -length     0.05676225 -e0  1.22126328527
Cavity     -name "LN2_XCA0"         -length      0.9164755 -gradient 0.065 -phase 10 -frequency 11.9942
SetReferenceEnergy  1.279831788336266e+00
Drift      -name "LN2_DR_XCA_ED"    -length     0.05676225 -e0  1.27983178834
Drift      -name "LN2_DR_BL"        -length           0.05 -e0  1.27983178834
Drift      -name "LN2_DR_QD_ED"     -length         0.0425 -e0  1.27983178834
Quadrupole -name "LN2_QD_FH"        -length           0.04 -e0  1.27983178834 -strength 0.342994919274
Dipole     -name "LN2_COR"          -length              0 -e0  1.27983178834
Bpm        -name "LN2_BPM"          -length              0 -e0  1.27983178834
Quadrupole -name "LN2_QD_FH"        -length           0.04 -e0  1.27983178834 -strength 0.342994919274
Drift      -name "LN2_DR_QD_ED"     -length         0.0425 -e0  1.27983178834
Drift      -name "LN2_DR_SV"        -length            0.1 -e0  1.27983178834
Drift      -name "LN2_DR_BL"        -length           0.05 -e0  1.27983178834
Drift      -name "LN2_DR_XCA_ED"    -length     0.05676225 -e0  1.27983178834
Cavity     -name "LN2_XCA0"         -length      0.9164755 -gradient 0.065 -phase 10 -frequency 11.9942
SetReferenceEnergy  1.338400289846813e+00
Drift      -name "LN2_DR_XCA_ED"    -length     0.05676225 -e0  1.33840028985
Drift      -name "LN2_DR_BL"        -length           0.05 -e0  1.33840028985
Drift      -name "LN2_DR_XCA_ED"    -length     0.05676225 -e0  1.33840028985
Cavity     -name "LN2_XCA0"         -length      0.9164755 -gradient 0.065 -phase 10 -frequency 11.9942
SetReferenceEnergy  1.396968790267438e+00
Drift      -name "LN2_DR_XCA_ED"    -length     0.05676225 -e0  1.39696879027
Drift      -name "LN2_DR_BL"        -length           0.05 -e0  1.39696879027
Drift      -name "LN2_DR_QD_ED"     -length         0.0425 -e0  1.39696879027
Quadrupole -name "LN2_QD_DH"        -length           0.04 -e0  1.39696879027 -strength -0.374387635792
Dipole     -name "LN2_COR"          -length              0 -e0  1.39696879027
Bpm        -name "LN2_BPM"          -length              0 -e0  1.39696879027
Quadrupole -name "LN2_QD_DH"        -length           0.04 -e0  1.39696879027 -strength -0.374387635792
Drift      -name "LN2_DR_QD_ED"     -length         0.0425 -e0  1.39696879027
Drift      -name "LN2_DR_VS"        -length            0.1 -e0  1.39696879027
Drift      -name "LN2_DR_BL"        -length           0.05 -e0  1.39696879027
Drift      -name "LN2_DR_XCA_ED"    -length     0.05676225 -e0  1.39696879027
Cavity     -name "LN2_XCA0"         -length      0.9164755 -gradient 0.065 -phase 10 -frequency 11.9942
SetReferenceEnergy  1.455537289748286e+00
Drift      -name "LN2_DR_XCA_ED"    -length     0.05676225 -e0  1.45553728975
Drift      -name "LN2_DR_BL"        -length           0.05 -e0  1.45553728975
Drift      -name "LN2_DR_XCA_ED"    -length     0.05676225 -e0  1.45553728975
Cavity     -name "LN2_XCA0"         -length      0.9164755 -gradient 0.065 -phase 10 -frequency 11.9942
SetReferenceEnergy  1.514105787443775e+00
Drift      -name "LN2_DR_XCA_ED"    -length     0.05676225 -e0  1.51410578744
Drift      -name "LN2_DR_BL"        -length           0.05 -e0  1.51410578744
Drift      -name "LN2_DR_QD_ED"     -length         0.0425 -e0  1.51410578744
Quadrupole -name "LN2_QD_FH"        -length           0.04 -e0  1.51410578744 -strength 0.405780351035
Dipole     -name "LN2_COR"          -length              0 -e0  1.51410578744
Bpm        -name "LN2_BPM"          -length              0 -e0  1.51410578744
Quadrupole -name "LN2_QD_FH"        -length           0.04 -e0  1.51410578744 -strength 0.405780351035
Drift      -name "LN2_DR_QD_ED"     -length         0.0425 -e0  1.51410578744
Drift      -name "LN2_WA_OU2"       -length              0 -e0  1.51410578744
Drift      -name "LN2_DR_DI_V1"     -length  1.16105229033 -e0  1.51410578744
Drift      -name "LN2_DR_QD_ED"     -length         0.0425 -e0  1.51410578744
Quadrupole -name "LN2_QD_DI_V01"    -length           0.04 -e0  1.51410578744 -strength -0.115064150403
Dipole     -name "LN2_COR_DI"       -length              0 -e0  1.51410578744
Bpm        -name "LN2_BPM_DI"       -length              0 -e0  1.51410578744
Quadrupole -name "LN2_QD_DI_V01"    -length           0.04 -e0  1.51410578744 -strength -0.115064150403
Drift      -name "LN2_DR_QD_ED"     -length         0.0425 -e0  1.51410578744
Drift      -name "LN2_DR_DI_V2"     -length 0.827998010248 -e0  1.51410578744
Drift      -name "LN2_DR_QD_ED"     -length         0.0425 -e0  1.51410578744
Quadrupole -name "LN2_QD_DI_V02"    -length           0.04 -e0  1.51410578744 -strength 0.597329047825
Dipole     -name "LN2_COR_DI"       -length              0 -e0  1.51410578744
Bpm        -name "LN2_BPM_DI"       -length              0 -e0  1.51410578744
Quadrupole -name "LN2_QD_DI_V02"    -length           0.04 -e0  1.51410578744 -strength 0.597329047825
Drift      -name "LN2_DR_QD_ED"     -length         0.0425 -e0  1.51410578744
Drift      -name "LN2_DR_DI_V3"     -length 0.200153917065 -e0  1.51410578744
Drift      -name "LN2_DR_QD_ED"     -length         0.0425 -e0  1.51410578744
Quadrupole -name "LN2_QD_DI_V03"    -length           0.04 -e0  1.51410578744 -strength -0.757546272558
Dipole     -name "LN2_COR_DI"       -length              0 -e0  1.51410578744
Bpm        -name "LN2_BPM_DI"       -length              0 -e0  1.51410578744
Quadrupole -name "LN2_QD_DI_V03"    -length           0.04 -e0  1.51410578744 -strength -0.757546272558
Drift      -name "LN2_DR_QD_ED"     -length         0.0425 -e0  1.51410578744
Drift      -name "LN2_DR_DI_V5"     -length            0.2 -e0  1.51410578744
Drift      -name "LN2_DR_VS"        -length            0.1 -e0  1.51410578744
Drift      -name "LN2_DR_DI"        -length           0.25 -e0  1.51410578744
Drift      -name "LN2_DR_VS_DI"     -length            0.1 -e0  1.51410578744
Drift      -name "LN2_DR_BL_DI"     -length           0.05 -e0  1.51410578744
Drift      -name "LN2_DR_XCA_ED_DI" -length      0.1001043 -e0  1.51410578744
CrabCavity -name "LN2_XCA0_DI"      -length      0.9997914 -voltage 0 -phase 90 -frequency 11.9942
SetReferenceEnergy  1.514105787443775e+00
Drift      -name "LN2_DR_XCA_ED_DI" -length      0.1001043 -e0   1.5140242821
Drift      -name "LN2_DR_BL_DI"     -length           0.05 -e0   1.5140242821
Drift      -name "LN2_DR_DI"        -length           0.25 -e0   1.5140242821
Drift      -name "LN2_DR_DI"        -length           0.25 -e0   1.5140242821
Drift      -name "LN2_DR_DI_QD_ED"  -length         0.0425 -e0   1.5140242821
Quadrupole -name "LN2_QD_DI_FH"     -length           0.04 -e0   1.5140242821 -strength 0.484487770271
Dipole     -name "LN2_COR_DI"       -length              0 -e0   1.5140242821
Bpm        -name "LN2_BPM_DI"       -length              0 -e0   1.5140242821
Quadrupole -name "LN2_QD_DI_FH"     -length           0.04 -e0   1.5140242821 -strength 0.484487770271
Drift      -name "LN2_DR_DI_QD_ED"  -length         0.0425 -e0   1.5140242821
Drift      -name "LN2_DR_FODO"      -length           0.15 -e0   1.5140242821
Drift      -name "LN2_DR_FODO"      -length           0.15 -e0   1.5140242821
Drift      -name "LN2_DR_FODO"      -length           0.15 -e0   1.5140242821
Drift      -name "LN2_DR_FODO"      -length           0.15 -e0   1.5140242821
Drift      -name "LN2_DR_DI_VS"     -length            0.1 -e0   1.5140242821
Drift      -name "LN2_DR_FODO"      -length           0.15 -e0   1.5140242821
Drift      -name "LN2_DR_FODO"      -length           0.15 -e0   1.5140242821
Drift      -name "LN2_DR_FODO"      -length           0.15 -e0   1.5140242821
Drift      -name "LN2_DR_FODO"      -length           0.15 -e0   1.5140242821
Drift      -name "LN2_DR_DI_QD_ED"  -length         0.0425 -e0   1.5140242821
Quadrupole -name "LN2_QD_DI_DH"     -length           0.04 -e0   1.5140242821 -strength -0.484487770271
Dipole     -name "LN2_COR_DI"       -length              0 -e0   1.5140242821
Bpm        -name "LN2_BPM_DI"       -length              0 -e0   1.5140242821
Quadrupole -name "LN2_QD_DI_DH"     -length           0.04 -e0   1.5140242821 -strength -0.484487770271
Drift      -name "LN2_DR_DI_QD_ED"  -length         0.0425 -e0   1.5140242821
Drift      -name "LN2_DR_FODO"      -length           0.15 -e0   1.5140242821
Drift      -name "LN2_DR_FODO"      -length           0.15 -e0   1.5140242821
Drift      -name "LN2_DR_FODO"      -length           0.15 -e0   1.5140242821
Drift      -name "LN2_DR_FODO"      -length           0.15 -e0   1.5140242821
Drift      -name "LN2_DR_DI_VS"     -length            0.1 -e0   1.5140242821
Drift      -name "LN2_DR_FODO"      -length           0.15 -e0   1.5140242821
Drift      -name "LN2_DR_FODO"      -length           0.15 -e0   1.5140242821
Drift      -name "LN2_DR_FODO"      -length           0.15 -e0   1.5140242821
Drift      -name "LN2_DR_FODO"      -length           0.15 -e0   1.5140242821
Drift      -name "LN2_DR_DI_QD_ED"  -length         0.0425 -e0   1.5140242821
Quadrupole -name "LN2_QD_DI_FH"     -length           0.04 -e0   1.5140242821 -strength 0.484487770271
Dipole     -name "LN2_COR_DI"       -length              0 -e0   1.5140242821
Bpm        -name "LN2_BPM_DI"       -length              0 -e0   1.5140242821
Quadrupole -name "LN2_QD_DI_FH"     -length           0.04 -e0   1.5140242821 -strength 0.484487770271
Drift      -name "LN2_DR_DI_QD_ED"  -length         0.0425 -e0   1.5140242821
Drift      -name "LN2_DR_FODO"      -length           0.15 -e0   1.5140242821
Drift      -name "LN2_DR_FODO"      -length           0.15 -e0   1.5140242821
Drift      -name "LN2_DR_FODO"      -length           0.15 -e0   1.5140242821
Drift      -name "LN2_DR_FODO"      -length           0.15 -e0   1.5140242821
Drift      -name "LN2_DR_DI_VS"     -length            0.1 -e0   1.5140242821
Drift      -name "LN2_DR_FODO"      -length           0.15 -e0   1.5140242821
Drift      -name "LN2_DR_FODO"      -length           0.15 -e0   1.5140242821
Drift      -name "LN2_DR_FODO"      -length           0.15 -e0   1.5140242821
Drift      -name "LN2_DR_FODO"      -length           0.15 -e0   1.5140242821
Drift      -name "LN2_DR_DI_QD_ED"  -length         0.0425 -e0   1.5140242821
Quadrupole -name "LN2_QD_DI_DH"     -length           0.04 -e0   1.5140242821 -strength -0.484487770271
Dipole     -name "LN2_COR_DI"       -length              0 -e0   1.5140242821
Bpm        -name "LN2_BPM_DI"       -length              0 -e0   1.5140242821
Quadrupole -name "LN2_QD_DI_DH"     -length           0.04 -e0   1.5140242821 -strength -0.484487770271
Drift      -name "LN2_DR_DI_QD_ED"  -length         0.0425 -e0   1.5140242821
Drift      -name "LN2_DR_FODO"      -length           0.15 -e0   1.5140242821
Drift      -name "LN2_DR_FODO"      -length           0.15 -e0   1.5140242821
Drift      -name "LN2_DR_FODO"      -length           0.15 -e0   1.5140242821
Drift      -name "LN2_DR_FODO"      -length           0.15 -e0   1.5140242821
Drift      -name "LN2_DR_DI_VS"     -length            0.1 -e0   1.5140242821
Drift      -name "LN2_DR_FODO"      -length           0.15 -e0   1.5140242821
Drift      -name "LN2_DR_FODO"      -length           0.15 -e0   1.5140242821
Drift      -name "LN2_DR_FODO"      -length           0.15 -e0   1.5140242821
Drift      -name "LN2_DR_FODO"      -length           0.15 -e0   1.5140242821
Drift      -name "LN2_DR_DI_QD_ED"  -length         0.0425 -e0   1.5140242821
Quadrupole -name "LN2_QD_DI_FH"     -length           0.04 -e0   1.5140242821 -strength 0.484487770271
Dipole     -name "LN2_COR_DI"       -length              0 -e0   1.5140242821
Bpm        -name "LN2_BPM_DI"       -length              0 -e0   1.5140242821
Quadrupole -name "LN2_QD_DI_FH"     -length           0.04 -e0   1.5140242821 -strength 0.484487770271
Drift      -name "LN2_DR_DI_QD_ED"  -length         0.0425 -e0   1.5140242821
Drift      -name "LN2_DR_FODO"      -length           0.15 -e0   1.5140242821
Drift      -name "LN2_DR_FODO"      -length           0.15 -e0   1.5140242821
Drift      -name "LN2_DR_FODO"      -length           0.15 -e0   1.5140242821
Drift      -name "LN2_DR_FODO"      -length           0.15 -e0   1.5140242821
Drift      -name "LN2_DR_DI_VS"     -length            0.1 -e0   1.5140242821
Sbend      -name "LN2_DP_DI"        -length           0.15 -e0   1.5140242821 -angle 0 -E1 0 -E2 0 -hgap 0 -fint 0.5 -tilt 0 -six_dim 1
Drift      -name "LN2_DR_FODO"      -length           0.15 -e0   1.5140242821
Drift      -name "LN2_DR_FODO"      -length           0.15 -e0   1.5140242821
Drift      -name "LN2_DR_FODO"      -length           0.15 -e0   1.5140242821
Drift      -name "LN2_DR_DI_QD_ED"  -length         0.0425 -e0   1.5140242821
Quadrupole -name "LN2_QD_DI_DH"     -length           0.04 -e0   1.5140242821 -strength -0.484487770271
Dipole     -name "LN2_COR_DI"       -length              0 -e0   1.5140242821
Bpm        -name "LN2_BPM_DI"       -length              0 -e0   1.5140242821
Quadrupole -name "LN2_QD_DI_DH"     -length           0.04 -e0   1.5140242821 -strength -0.484487770271
Drift      -name "LN2_DR_DI_QD_ED"  -length         0.0425 -e0   1.5140242821
Drift      -name "LN2_DR_DI"        -length           0.25 -e0   1.5140242821
Drift      -name "LN2_WA_OU_DI2"    -length              0 -e0   1.5140242821
Drift      -name "LN2_DR_BP_V1"     -length  3.71421928221 -e0   1.5140242821
Drift      -name "LN2_DR_QD_ED"     -length         0.0425 -e0   1.5140242821
Quadrupole -name "LN2_QD_BP_V01"    -length           0.04 -e0   1.5140242821 -strength -0.0698495222833
Dipole     -name "LN2_COR"          -length              0 -e0   1.5140242821
Bpm        -name "LN2_BPM"          -length              0 -e0   1.5140242821
Quadrupole -name "LN2_QD_BP_V01"    -length           0.04 -e0   1.5140242821 -strength -0.0698495222833
Drift      -name "LN2_DR_QD_ED"     -length         0.0425 -e0   1.5140242821
Drift      -name "LN2_DR_BP_V2"     -length  3.52876656637 -e0   1.5140242821
Drift      -name "LN2_DR_QD_ED"     -length         0.0425 -e0   1.5140242821
Quadrupole -name "LN2_QD_BP_V02"    -length           0.04 -e0   1.5140242821 -strength 0.836050237044
Dipole     -name "LN2_COR"          -length              0 -e0   1.5140242821
Bpm        -name "LN2_BPM"          -length              0 -e0   1.5140242821
Quadrupole -name "LN2_QD_BP_V02"    -length           0.04 -e0   1.5140242821 -strength 0.836050237044
Drift      -name "LN2_DR_QD_ED"     -length         0.0425 -e0   1.5140242821
Drift      -name "LN2_DR_BP_V3"     -length 0.200026928049 -e0   1.5140242821
Drift      -name "LN2_DR_QD_ED"     -length         0.0425 -e0   1.5140242821
Quadrupole -name "LN2_QD_BP_V03"    -length           0.04 -e0   1.5140242821 -strength -0.814011396703
Dipole     -name "LN2_COR"          -length              0 -e0   1.5140242821
Bpm        -name "LN2_BPM"          -length              0 -e0   1.5140242821
Quadrupole -name "LN2_QD_BP_V03"    -length           0.04 -e0   1.5140242821 -strength -0.814011396703
Drift      -name "LN2_DR_QD_ED"     -length         0.0425 -e0   1.5140242821
Drift      -name "LN2_DR_BP_V4"     -length 0.264590194376 -e0   1.5140242821
Drift      -name "LN2_DR_BP_0"      -length            0.2 -e0   1.5140242821
Drift      -name "LN2_DR_BP_BL"     -length           0.05 -e0   1.5140242821
Drift      -name "LN2_DR_SCA_ED"    -length     0.08325105 -e0   1.5140242821
CrabCavity -name "LN2_SCA0"         -length      0.5334979 -voltage 0 -phase 90 -frequency 2.997
SetReferenceEnergy  1.514024282096401e+00
Drift      -name "LN2_DR_SCA_ED"    -length     0.08325105 -e0  1.51401933387
Drift      -name "LN2_DR_BP_BL"     -length           0.05 -e0  1.51401933387
Drift      -name "LN2_DR_BP_0"      -length            0.2 -e0  1.51401933387
Drift      -name "LN2_DR_BP"        -length            0.4 -e0  1.51401933387
Drift      -name "LN2_DR_BP"        -length            0.4 -e0  1.51401933387
Drift      -name "LN2_DR_BP_VS"     -length            0.1 -e0  1.51401933387
Drift      -name "LN2_DR_BP_DR"     -length            0.1 -e0  1.51401933387
Drift      -name "LN2_BP_WA_OU2"    -length              0 -e0  1.51401933387
Drift      -name "LN2_DP_SEPM"      -length           0.25 -e0  1.51401933387
Drift      -name "LN2_DR_BP"        -length            0.4 -e0  1.51401933387
Drift      -name "LN2_DR_BP"        -length            0.4 -e0  1.51401933387
Drift      -name "LN2_DR_BP"        -length            0.4 -e0  1.51401933387
Drift      -name "LN2_DR_BP"        -length            0.4 -e0  1.51401933387
Drift      -name "LN2_DR_BP"        -length            0.4 -e0  1.51401933387
Drift      -name "LN2_DR_BP_VS"     -length            0.1 -e0  1.51401933387
Drift      -name "LN2_DR_BP"        -length            0.4 -e0  1.51401933387
## Drift      -name "LN2_BP_WA_OU2"   -length              0 -e0  1.51401933387
Drift      -name "LN3_DR_V1"        -length 0.0500000015106 -e0  1.51401933387
Drift      -name "LN3_DR_QD_ED"     -length         0.0425 -e0  1.51401933387
Quadrupole -name "LN3_QD_V01"       -length           0.04 -e0  1.51401933387 -strength 0.493113422784
Dipole     -name "LN3_COR"          -length              0 -e0  1.51401933387
Bpm        -name "LN3_BPM"          -length              0 -e0  1.51401933387
Quadrupole -name "LN3_QD_V01"       -length           0.04 -e0  1.51401933387 -strength 0.493113422784
Drift      -name "LN3_DR_QD_ED"     -length         0.0425 -e0  1.51401933387
Drift      -name "LN3_DR_V2"        -length 0.534281716737 -e0  1.51401933387
Drift      -name "LN3_DR_QD_ED"     -length         0.0425 -e0  1.51401933387
Quadrupole -name "LN3_QD_V02"       -length           0.04 -e0  1.51401933387 -strength 0.438104614361
Dipole     -name "LN3_COR"          -length              0 -e0  1.51401933387
Bpm        -name "LN3_BPM"          -length              0 -e0  1.51401933387
Quadrupole -name "LN3_QD_V02"       -length           0.04 -e0  1.51401933387 -strength 0.438104614361
Drift      -name "LN3_DR_QD_ED"     -length         0.0425 -e0  1.51401933387
Drift      -name "LN3_DR_V3"        -length 0.093746938839 -e0  1.51401933387
Drift      -name "LN3_DR_QD_ED"     -length         0.0425 -e0  1.51401933387
Quadrupole -name "LN3_QD_V03"       -length           0.04 -e0  1.51401933387 -strength -0.850890354229
Dipole     -name "LN3_COR"          -length              0 -e0  1.51401933387
Bpm        -name "LN3_BPM"          -length              0 -e0  1.51401933387
Quadrupole -name "LN3_QD_V03"       -length           0.04 -e0  1.51401933387 -strength -0.850890354229
Drift      -name "LN3_DR_QD_ED"     -length         0.0425 -e0  1.51401933387
Drift      -name "LN3_DR_V4"        -length  1.58002295167 -e0  1.51401933387
Drift      -name "LN3_DR_QD_ED"     -length         0.0425 -e0  1.51401933387
Quadrupole -name "LN3_QD_V04"       -length           0.04 -e0  1.51401933387 -strength -0.191446684428
Dipole     -name "LN3_COR"          -length              0 -e0  1.51401933387
Bpm        -name "LN3_BPM"          -length              0 -e0  1.51401933387
Quadrupole -name "LN3_QD_V04"       -length           0.04 -e0  1.51401933387 -strength -0.191446684428
Drift      -name "LN3_DR_QD_ED"     -length         0.0425 -e0  1.51401933387
Drift      -name "LN3_DR_V5"        -length  1.23738487567 -e0  1.51401933387
Drift      -name "LN3_DR_QD_ED"     -length         0.0425 -e0  1.51401933387
Quadrupole -name "LN3_QD_FH"        -length           0.04 -e0  1.51401933387 -strength 0.254355248089
Dipole     -name "LN3_COR"          -length              0 -e0  1.51401933387
Bpm        -name "LN3_BPM"          -length              0 -e0  1.51401933387
Quadrupole -name "LN3_QD_FH"        -length           0.04 -e0  1.51401933387 -strength 0.254355248089
Drift      -name "LN3_DR_QD_ED"     -length         0.0425 -e0  1.51401933387
Drift      -name "LN3_DR_SV"        -length            0.1 -e0  1.51401933387
Drift      -name "LN3_DR_BL"        -length           0.05 -e0  1.51401933387
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  1.51401933387
Cavity     -name "LN3_XCA0"         -length      0.9164755 -gradient 0.065 -phase 10 -frequency 11.9942
SetReferenceEnergy  1.572587808411532e+00
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  1.57258780841
Drift      -name "LN3_DR_BL"        -length           0.05 -e0  1.57258780841
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  1.57258780841
Cavity     -name "LN3_XCA0"         -length      0.9164755 -gradient 0.065 -phase 10 -frequency 11.9942
SetReferenceEnergy  1.631156282397370e+00
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0   1.6311562824
Drift      -name "LN3_DR_BL"        -length           0.05 -e0   1.6311562824
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0   1.6311562824
Cavity     -name "LN3_XCA0"         -length      0.9164755 -gradient 0.065 -phase 10 -frequency 11.9942
SetReferenceEnergy  1.689724755947705e+00
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  1.68972475595
Drift      -name "LN3_DR_BL"        -length           0.05 -e0  1.68972475595
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  1.68972475595
Cavity     -name "LN3_XCA0"         -length      0.9164755 -gradient 0.065 -phase 10 -frequency 11.9942
SetReferenceEnergy  1.748293229030172e+00
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  1.74829322903
Drift      -name "LN3_DR_BL"        -length           0.05 -e0  1.74829322903
Drift      -name "LN3_DR_QD_ED"     -length         0.0425 -e0  1.74829322903
Quadrupole -name "LN3_QD_DH"        -length           0.04 -e0  1.74829322903 -strength -0.293713262477
Dipole     -name "LN3_COR"          -length              0 -e0  1.74829322903
Bpm        -name "LN3_BPM"          -length              0 -e0  1.74829322903
Quadrupole -name "LN3_QD_DH"        -length           0.04 -e0  1.74829322903 -strength -0.293713262477
Drift      -name "LN3_DR_QD_ED"     -length         0.0425 -e0  1.74829322903
Drift      -name "LN3_DR_VS"        -length            0.1 -e0  1.74829322903
Drift      -name "LN3_DR_BL"        -length           0.05 -e0  1.74829322903
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  1.74829322903
Cavity     -name "LN3_XCA0"         -length      0.9164755 -gradient 0.065 -phase 10 -frequency 11.9942
SetReferenceEnergy  1.806861701386980e+00
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  1.80686170139
Drift      -name "LN3_DR_BL"        -length           0.05 -e0  1.80686170139
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  1.80686170139
Cavity     -name "LN3_XCA0"         -length      0.9164755 -gradient 0.065 -phase 10 -frequency 11.9942
SetReferenceEnergy  1.865430173716840e+00
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  1.86543017372
Drift      -name "LN3_DR_BL"        -length           0.05 -e0  1.86543017372
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  1.86543017372
Cavity     -name "LN3_XCA0"         -length      0.9164755 -gradient 0.065 -phase 10 -frequency 11.9942
SetReferenceEnergy  1.923998645426417e+00
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  1.92399864543
Drift      -name "LN3_DR_BL"        -length           0.05 -e0  1.92399864543
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  1.92399864543
Cavity     -name "LN3_XCA0"         -length      0.9164755 -gradient 0.065 -phase 10 -frequency 11.9942
SetReferenceEnergy  1.982567117073977e+00
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  1.98256711707
Drift      -name "LN3_DR_BL"        -length           0.05 -e0  1.98256711707
Drift      -name "LN3_DR_QD_ED"     -length         0.0425 -e0  1.98256711707
Quadrupole -name "LN3_QD_FH"        -length           0.04 -e0  1.98256711707 -strength 0.333071275668
Dipole     -name "LN3_COR"          -length              0 -e0  1.98256711707
Bpm        -name "LN3_BPM"          -length              0 -e0  1.98256711707
Quadrupole -name "LN3_QD_FH"        -length           0.04 -e0  1.98256711707 -strength 0.333071275668
Drift      -name "LN3_DR_QD_ED"     -length         0.0425 -e0  1.98256711707
Drift      -name "LN3_DR_SV"        -length            0.1 -e0  1.98256711707
Drift      -name "LN3_DR_BL"        -length           0.05 -e0  1.98256711707
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  1.98256711707
Cavity     -name "LN3_XCA0"         -length      0.9164755 -gradient 0.065 -phase 10 -frequency 11.9942
SetReferenceEnergy  2.041135587619969e+00
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  2.04113558762
Drift      -name "LN3_DR_BL"        -length           0.05 -e0  2.04113558762
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  2.04113558762
Cavity     -name "LN3_XCA0"         -length      0.9164755 -gradient 0.065 -phase 10 -frequency 11.9942
SetReferenceEnergy  2.099704057934897e+00
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  2.09970405793
Drift      -name "LN3_DR_BL"        -length           0.05 -e0  2.09970405793
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  2.09970405793
Cavity     -name "LN3_XCA0"         -length      0.9164755 -gradient 0.065 -phase 10 -frequency 11.9942
SetReferenceEnergy  2.158272528036718e+00
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  2.15827252804
Drift      -name "LN3_DR_BL"        -length           0.05 -e0  2.15827252804
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  2.15827252804
Cavity     -name "LN3_XCA0"         -length      0.9164755 -gradient 0.065 -phase 10 -frequency 11.9942
SetReferenceEnergy  2.216840997715476e+00
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  2.21684099772
Drift      -name "LN3_DR_BL"        -length           0.05 -e0  2.21684099772
Drift      -name "LN3_DR_QD_ED"     -length         0.0425 -e0  2.21684099772
Quadrupole -name "LN3_QD_DH"        -length           0.04 -e0  2.21684099772 -strength -0.372429287616
Dipole     -name "LN3_COR"          -length              0 -e0  2.21684099772
Bpm        -name "LN3_BPM"          -length              0 -e0  2.21684099772
Quadrupole -name "LN3_QD_DH"        -length           0.04 -e0  2.21684099772 -strength -0.372429287616
Drift      -name "LN3_DR_QD_ED"     -length         0.0425 -e0  2.21684099772
Drift      -name "LN3_DR_VS"        -length            0.1 -e0  2.21684099772
Drift      -name "LN3_DR_BL"        -length           0.05 -e0  2.21684099772
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  2.21684099772
Cavity     -name "LN3_XCA0"         -length      0.9164755 -gradient 0.065 -phase 10 -frequency 11.9942
SetReferenceEnergy  2.275409466862888e+00
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  2.27540946686
Drift      -name "LN3_DR_BL"        -length           0.05 -e0  2.27540946686
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  2.27540946686
Cavity     -name "LN3_XCA0"         -length      0.9164755 -gradient 0.065 -phase 10 -frequency 11.9942
SetReferenceEnergy  2.333977935807027e+00
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  2.33397793581
Drift      -name "LN3_DR_BL"        -length           0.05 -e0  2.33397793581
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  2.33397793581
Cavity     -name "LN3_XCA0"         -length      0.9164755 -gradient 0.065 -phase 10 -frequency 11.9942
SetReferenceEnergy  2.392546404513037e+00
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  2.39254640451
Drift      -name "LN3_DR_BL"        -length           0.05 -e0  2.39254640451
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  2.39254640451
Cavity     -name "LN3_XCA0"         -length      0.9164755 -gradient 0.065 -phase 10 -frequency 11.9942
SetReferenceEnergy  2.451114873038927e+00
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  2.45111487304
Drift      -name "LN3_DR_BL"        -length           0.05 -e0  2.45111487304
Drift      -name "LN3_DR_QD_ED"     -length         0.0425 -e0  2.45111487304
Quadrupole -name "LN3_QD_FH"        -length           0.04 -e0  2.45111487304 -strength 0.411787298671
Dipole     -name "LN3_COR"          -length              0 -e0  2.45111487304
Bpm        -name "LN3_BPM"          -length              0 -e0  2.45111487304
Quadrupole -name "LN3_QD_FH"        -length           0.04 -e0  2.45111487304 -strength 0.411787298671
Drift      -name "LN3_DR_QD_ED"     -length         0.0425 -e0  2.45111487304
Drift      -name "LN3_DR_SV"        -length            0.1 -e0  2.45111487304
Drift      -name "LN3_DR_BL"        -length           0.05 -e0  2.45111487304
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  2.45111487304
Cavity     -name "LN3_XCA0"         -length      0.9164755 -gradient 0.065 -phase 10 -frequency 11.9942
SetReferenceEnergy  2.509683341662786e+00
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  2.50968334166
Drift      -name "LN3_DR_BL"        -length           0.05 -e0  2.50968334166
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  2.50968334166
Cavity     -name "LN3_XCA0"         -length      0.9164755 -gradient 0.065 -phase 10 -frequency 11.9942
SetReferenceEnergy  2.568251809943388e+00
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  2.56825180994
Drift      -name "LN3_DR_BL"        -length           0.05 -e0  2.56825180994
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  2.56825180994
Cavity     -name "LN3_XCA0"         -length      0.9164755 -gradient 0.065 -phase 10 -frequency 11.9942
SetReferenceEnergy  2.626820278129123e+00
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  2.62682027813
Drift      -name "LN3_DR_BL"        -length           0.05 -e0  2.62682027813
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  2.62682027813
Cavity     -name "LN3_XCA0"         -length      0.9164755 -gradient 0.065 -phase 10 -frequency 11.9942
SetReferenceEnergy  2.685388746378331e+00
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  2.68538874638
Drift      -name "LN3_DR_BL"        -length           0.05 -e0  2.68538874638
Drift      -name "LN3_DR_QD_ED"     -length         0.0425 -e0  2.68538874638
Quadrupole -name "LN3_QD_DH"        -length           0.04 -e0  2.68538874638 -strength -0.451145309392
Dipole     -name "LN3_COR"          -length              0 -e0  2.68538874638
Bpm        -name "LN3_BPM"          -length              0 -e0  2.68538874638
Quadrupole -name "LN3_QD_DH"        -length           0.04 -e0  2.68538874638 -strength -0.451145309392
Drift      -name "LN3_DR_QD_ED"     -length         0.0425 -e0  2.68538874638
Drift      -name "LN3_DR_VS"        -length            0.1 -e0  2.68538874638
Drift      -name "LN3_DR_BL"        -length           0.05 -e0  2.68538874638
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  2.68538874638
Cavity     -name "LN3_XCA0"         -length      0.9164755 -gradient 0.065 -phase 10 -frequency 11.9942
SetReferenceEnergy  2.743957213873834e+00
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  2.74395721387
Drift      -name "LN3_DR_BL"        -length           0.05 -e0  2.74395721387
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  2.74395721387
Cavity     -name "LN3_XCA0"         -length      0.9164755 -gradient 0.065 -phase 10 -frequency 11.9942
SetReferenceEnergy  2.802525680816479e+00
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  2.80252568082
Drift      -name "LN3_DR_BL"        -length           0.05 -e0  2.80252568082
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  2.80252568082
Cavity     -name "LN3_XCA0"         -length      0.9164755 -gradient 0.065 -phase 10 -frequency 11.9942
SetReferenceEnergy  2.861094147645121e+00
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  2.86109414765
Drift      -name "LN3_DR_BL"        -length           0.05 -e0  2.86109414765
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  2.86109414765
Cavity     -name "LN3_XCA0"         -length      0.9164755 -gradient 0.065 -phase 10 -frequency 11.9942
SetReferenceEnergy  2.919662614394082e+00
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  2.91966261439
Drift      -name "LN3_DR_BL"        -length           0.05 -e0  2.91966261439
Drift      -name "LN3_DR_QD_ED"     -length         0.0425 -e0  2.91966261439
Quadrupole -name "LN3_QD_FH"        -length           0.04 -e0  2.91966261439 -strength 0.490503319218
Dipole     -name "LN3_COR"          -length              0 -e0  2.91966261439
Bpm        -name "LN3_BPM"          -length              0 -e0  2.91966261439
Quadrupole -name "LN3_QD_FH"        -length           0.04 -e0  2.91966261439 -strength 0.490503319218
Drift      -name "LN3_DR_QD_ED"     -length         0.0425 -e0  2.91966261439
Drift      -name "LN3_DR_SV"        -length            0.1 -e0  2.91966261439
Drift      -name "LN3_DR_BL"        -length           0.05 -e0  2.91966261439
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  2.91966261439
Cavity     -name "LN3_XCA0"         -length      0.9164755 -gradient 0.065 -phase 10 -frequency 11.9942
SetReferenceEnergy  2.978231080991749e+00
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  2.97823108099
Drift      -name "LN3_DR_BL"        -length           0.05 -e0  2.97823108099
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  2.97823108099
Cavity     -name "LN3_XCA0"         -length      0.9164755 -gradient 0.065 -phase 10 -frequency 11.9942
SetReferenceEnergy  3.036799547009168e+00
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  3.03679954701
Drift      -name "LN3_DR_BL"        -length           0.05 -e0  3.03679954701
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  3.03679954701
Cavity     -name "LN3_XCA0"         -length      0.9164755 -gradient 0.065 -phase 10 -frequency 11.9942
SetReferenceEnergy  3.095368012631312e+00
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  3.09536801263
Drift      -name "LN3_DR_BL"        -length           0.05 -e0  3.09536801263
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  3.09536801263
Cavity     -name "LN3_XCA0"         -length      0.9164755 -gradient 0.065 -phase 10 -frequency 11.9942
SetReferenceEnergy  3.153936478257821e+00
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  3.15393647826
Drift      -name "LN3_DR_BL"        -length           0.05 -e0  3.15393647826
Drift      -name "LN3_DR_QD_ED"     -length         0.0425 -e0  3.15393647826
Quadrupole -name "LN3_QD_DH"        -length           0.04 -e0  3.15393647826 -strength -0.529861328347
Dipole     -name "LN3_COR"          -length              0 -e0  3.15393647826
Bpm        -name "LN3_BPM"          -length              0 -e0  3.15393647826
Quadrupole -name "LN3_QD_DH"        -length           0.04 -e0  3.15393647826 -strength -0.529861328347
Drift      -name "LN3_DR_QD_ED"     -length         0.0425 -e0  3.15393647826
Drift      -name "LN3_DR_VS"        -length            0.1 -e0  3.15393647826
Drift      -name "LN3_DR_BL"        -length           0.05 -e0  3.15393647826
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  3.15393647826
Cavity     -name "LN3_XCA0"         -length      0.9164755 -gradient 0.065 -phase 10 -frequency 11.9942
SetReferenceEnergy  3.212504943318139e+00
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  3.21250494332
Drift      -name "LN3_DR_BL"        -length           0.05 -e0  3.21250494332
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  3.21250494332
Cavity     -name "LN3_XCA0"         -length      0.9164755 -gradient 0.065 -phase 10 -frequency 11.9942
SetReferenceEnergy  3.271073408216027e+00
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  3.27107340822
Drift      -name "LN3_DR_BL"        -length           0.05 -e0  3.27107340822
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  3.27107340822
Cavity     -name "LN3_XCA0"         -length      0.9164755 -gradient 0.065 -phase 10 -frequency 11.9942
SetReferenceEnergy  3.329641873113320e+00
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  3.32964187311
Drift      -name "LN3_DR_BL"        -length           0.05 -e0  3.32964187311
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  3.32964187311
Cavity     -name "LN3_XCA0"         -length      0.9164755 -gradient 0.065 -phase 10 -frequency 11.9942
SetReferenceEnergy  3.388210338026715e+00
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  3.38821033803
Drift      -name "LN3_DR_BL"        -length           0.05 -e0  3.38821033803
Drift      -name "LN3_DR_QD_ED"     -length         0.0425 -e0  3.38821033803
Quadrupole -name "LN3_QD_FH"        -length           0.04 -e0  3.38821033803 -strength 0.569219336788
Dipole     -name "LN3_COR"          -length              0 -e0  3.38821033803
Bpm        -name "LN3_BPM"          -length              0 -e0  3.38821033803
Quadrupole -name "LN3_QD_FH"        -length           0.04 -e0  3.38821033803 -strength 0.569219336788
Drift      -name "LN3_DR_QD_ED"     -length         0.0425 -e0  3.38821033803
Drift      -name "LN3_DR_SV"        -length            0.1 -e0  3.38821033803
Drift      -name "LN3_DR_BL"        -length           0.05 -e0  3.38821033803
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  3.38821033803
Cavity     -name "LN3_XCA0"         -length      0.9164755 -gradient 0.065 -phase 10 -frequency 11.9942
SetReferenceEnergy  3.446778802858434e+00
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  3.44677880286
Drift      -name "LN3_DR_BL"        -length           0.05 -e0  3.44677880286
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  3.44677880286
Cavity     -name "LN3_XCA0"         -length      0.9164755 -gradient 0.065 -phase 10 -frequency 11.9942
SetReferenceEnergy  3.505347267595017e+00
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0   3.5053472676
Drift      -name "LN3_DR_BL"        -length           0.05 -e0   3.5053472676
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0   3.5053472676
Cavity     -name "LN3_XCA0"         -length      0.9164755 -gradient 0.065 -phase 10 -frequency 11.9942
SetReferenceEnergy  3.563915732091020e+00
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  3.56391573209
Drift      -name "LN3_DR_BL"        -length           0.05 -e0  3.56391573209
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  3.56391573209
Cavity     -name "LN3_XCA0"         -length      0.9164755 -gradient 0.065 -phase 10 -frequency 11.9942
SetReferenceEnergy  3.622484196627749e+00
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  3.62248419663
Drift      -name "LN3_DR_BL"        -length           0.05 -e0  3.62248419663
Drift      -name "LN3_DR_QD_ED"     -length         0.0425 -e0  3.62248419663
Quadrupole -name "LN3_QD_DH"        -length           0.04 -e0  3.62248419663 -strength -0.608577345033
Dipole     -name "LN3_COR"          -length              0 -e0  3.62248419663
Bpm        -name "LN3_BPM"          -length              0 -e0  3.62248419663
Quadrupole -name "LN3_QD_DH"        -length           0.04 -e0  3.62248419663 -strength -0.608577345033
Drift      -name "LN3_DR_QD_ED"     -length         0.0425 -e0  3.62248419663
Drift      -name "LN3_DR_VS"        -length            0.1 -e0  3.62248419663
Drift      -name "LN3_DR_BL"        -length           0.05 -e0  3.62248419663
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  3.62248419663
Cavity     -name "LN3_XCA0"         -length      0.9164755 -gradient 0.065 -phase 10 -frequency 11.9942
SetReferenceEnergy  3.681052660918994e+00
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  3.68105266092
Drift      -name "LN3_DR_BL"        -length           0.05 -e0  3.68105266092
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  3.68105266092
Cavity     -name "LN3_XCA0"         -length      0.9164755 -gradient 0.065 -phase 10 -frequency 11.9942
SetReferenceEnergy  3.739621125182409e+00
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  3.73962112518
Drift      -name "LN3_DR_BL"        -length           0.05 -e0  3.73962112518
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  3.73962112518
Cavity     -name "LN3_XCA0"         -length      0.9164755 -gradient 0.065 -phase 10 -frequency 11.9942
SetReferenceEnergy  3.798189589805388e+00
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  3.79818958981
Drift      -name "LN3_DR_BL"        -length           0.05 -e0  3.79818958981
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  3.79818958981
Cavity     -name "LN3_XCA0"         -length      0.9164755 -gradient 0.065 -phase 10 -frequency 11.9942
SetReferenceEnergy  3.856758054324628e+00
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  3.85675805432
Drift      -name "LN3_DR_BL"        -length           0.05 -e0  3.85675805432
Drift      -name "LN3_DR_QD_ED"     -length         0.0425 -e0  3.85675805432
Quadrupole -name "LN3_QD_FH"        -length           0.04 -e0  3.85675805432 -strength 0.647935353127
Dipole     -name "LN3_COR"          -length              0 -e0  3.85675805432
Bpm        -name "LN3_BPM"          -length              0 -e0  3.85675805432
Quadrupole -name "LN3_QD_FH"        -length           0.04 -e0  3.85675805432 -strength 0.647935353127
Drift      -name "LN3_DR_QD_ED"     -length         0.0425 -e0  3.85675805432
Drift      -name "LN3_DR_SV"        -length            0.1 -e0  3.85675805432
Drift      -name "LN3_DR_BL"        -length           0.05 -e0  3.85675805432
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  3.85675805432
Cavity     -name "LN3_XCA0"         -length      0.9164755 -gradient 0.065 -phase 10 -frequency 11.9942
SetReferenceEnergy  3.915326518796211e+00
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0   3.9153265188
Drift      -name "LN3_DR_BL"        -length           0.05 -e0   3.9153265188
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0   3.9153265188
Cavity     -name "LN3_XCA0"         -length      0.9164755 -gradient 0.065 -phase 10 -frequency 11.9942
SetReferenceEnergy  3.973894983308394e+00
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  3.97389498331
Drift      -name "LN3_DR_BL"        -length           0.05 -e0  3.97389498331
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  3.97389498331
Cavity     -name "LN3_XCA0"         -length      0.9164755 -gradient 0.065 -phase 10 -frequency 11.9942
SetReferenceEnergy  4.032463448235141e+00
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  4.03246344824
Drift      -name "LN3_DR_BL"        -length           0.05 -e0  4.03246344824
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  4.03246344824
Cavity     -name "LN3_XCA0"         -length      0.9164755 -gradient 0.065 -phase 10 -frequency 11.9942
SetReferenceEnergy  4.091031913158217e+00
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  4.09103191316
Drift      -name "LN3_DR_BL"        -length           0.05 -e0  4.09103191316
Drift      -name "LN3_DR_QD_ED"     -length         0.0425 -e0  4.09103191316
Quadrupole -name "LN3_QD_DH"        -length           0.04 -e0  4.09103191316 -strength -0.687293361411
Dipole     -name "LN3_COR"          -length              0 -e0  4.09103191316
Bpm        -name "LN3_BPM"          -length              0 -e0  4.09103191316
Quadrupole -name "LN3_QD_DH"        -length           0.04 -e0  4.09103191316 -strength -0.687293361411
Drift      -name "LN3_DR_QD_ED"     -length         0.0425 -e0  4.09103191316
Drift      -name "LN3_DR_VS"        -length            0.1 -e0  4.09103191316
Drift      -name "LN3_DR_BL"        -length           0.05 -e0  4.09103191316
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  4.09103191316
Cavity     -name "LN3_XCA0"         -length      0.9164755 -gradient 0.065 -phase 10 -frequency 11.9942
SetReferenceEnergy  4.149600377601478e+00
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0   4.1496003776
Drift      -name "LN3_DR_BL"        -length           0.05 -e0   4.1496003776
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0   4.1496003776
Cavity     -name "LN3_XCA0"         -length      0.9164755 -gradient 0.065 -phase 10 -frequency 11.9942
SetReferenceEnergy  4.208168842409282e+00
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  4.20816884241
Drift      -name "LN3_DR_BL"        -length           0.05 -e0  4.20816884241
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  4.20816884241
Cavity     -name "LN3_XCA0"         -length      0.9164755 -gradient 0.065 -phase 10 -frequency 11.9942
SetReferenceEnergy  4.266737307197213e+00
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0   4.2667373072
Drift      -name "LN3_DR_BL"        -length           0.05 -e0   4.2667373072
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0   4.2667373072
Cavity     -name "LN3_XCA0"         -length      0.9164755 -gradient 0.065 -phase 10 -frequency 11.9942
SetReferenceEnergy  4.325305772133871e+00
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  4.32530577213
Drift      -name "LN3_DR_BL"        -length           0.05 -e0  4.32530577213
Drift      -name "LN3_DR_QD_ED"     -length         0.0425 -e0  4.32530577213
Quadrupole -name "LN3_QD_FH"        -length           0.04 -e0  4.32530577213 -strength 0.726651369718
Dipole     -name "LN3_COR"          -length              0 -e0  4.32530577213
Bpm        -name "LN3_BPM"          -length              0 -e0  4.32530577213
Quadrupole -name "LN3_QD_FH"        -length           0.04 -e0  4.32530577213 -strength 0.726651369718
Drift      -name "LN3_DR_QD_ED"     -length         0.0425 -e0  4.32530577213
Drift      -name "LN3_DR_SV"        -length            0.1 -e0  4.32530577213
Drift      -name "LN3_DR_BL"        -length           0.05 -e0  4.32530577213
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  4.32530577213
Cavity     -name "LN3_XCA0"         -length      0.9164755 -gradient 0.065 -phase 10 -frequency 11.9942
SetReferenceEnergy  4.383874237192355e+00
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  4.38387423719
Drift      -name "LN3_DR_BL"        -length           0.05 -e0  4.38387423719
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  4.38387423719
Cavity     -name "LN3_XCA0"         -length      0.9164755 -gradient 0.065 -phase 10 -frequency 11.9942
SetReferenceEnergy  4.442442702203414e+00
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0   4.4424427022
Drift      -name "LN3_DR_BL"        -length           0.05 -e0   4.4424427022
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0   4.4424427022
Cavity     -name "LN3_XCA0"         -length      0.9164755 -gradient 0.065 -phase 10 -frequency 11.9942
SetReferenceEnergy  4.501011167333119e+00
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  4.50101116733
Drift      -name "LN3_DR_BL"        -length           0.05 -e0  4.50101116733
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  4.50101116733
Cavity     -name "LN3_XCA0"         -length      0.9164755 -gradient 0.065 -phase 10 -frequency 11.9942
SetReferenceEnergy  4.559579632469430e+00
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  4.55957963247
Drift      -name "LN3_DR_BL"        -length           0.05 -e0  4.55957963247
Drift      -name "LN3_DR_QD_ED"     -length         0.0425 -e0  4.55957963247
Quadrupole -name "LN3_QD_DH"        -length           0.04 -e0  4.55957963247 -strength -0.766009378255
Dipole     -name "LN3_COR"          -length              0 -e0  4.55957963247
Bpm        -name "LN3_BPM"          -length              0 -e0  4.55957963247
Quadrupole -name "LN3_QD_DH"        -length           0.04 -e0  4.55957963247 -strength -0.766009378255
Drift      -name "LN3_DR_QD_ED"     -length         0.0425 -e0  4.55957963247
Drift      -name "LN3_DR_VS"        -length            0.1 -e0  4.55957963247
Drift      -name "LN3_DR_BL"        -length           0.05 -e0  4.55957963247
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  4.55957963247
Cavity     -name "LN3_XCA0"         -length      0.9164755 -gradient 0.065 -phase 10 -frequency 11.9942
SetReferenceEnergy  4.618148097592750e+00
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  4.61814809759
Drift      -name "LN3_DR_BL"        -length           0.05 -e0  4.61814809759
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  4.61814809759
Cavity     -name "LN3_XCA0"         -length      0.9164755 -gradient 0.065 -phase 10 -frequency 11.9942
SetReferenceEnergy  4.676716562623538e+00
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  4.67671656262
Drift      -name "LN3_DR_BL"        -length           0.05 -e0  4.67671656262
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  4.67671656262
Cavity     -name "LN3_XCA0"         -length      0.9164755 -gradient 0.065 -phase 10 -frequency 11.9942
SetReferenceEnergy  4.735285027810079e+00
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  4.73528502781
Drift      -name "LN3_DR_BL"        -length           0.05 -e0  4.73528502781
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  4.73528502781
Cavity     -name "LN3_XCA0"         -length      0.9164755 -gradient 0.065 -phase 10 -frequency 11.9942
SetReferenceEnergy  4.793853492802009e+00
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0   4.7938534928
Drift      -name "LN3_DR_BL"        -length           0.05 -e0   4.7938534928
Drift      -name "LN3_DR_QD_ED"     -length         0.0425 -e0   4.7938534928
Quadrupole -name "LN3_QD_FH"        -length           0.04 -e0   4.7938534928 -strength 0.805367386791
Dipole     -name "LN3_COR"          -length              0 -e0   4.7938534928
Bpm        -name "LN3_BPM"          -length              0 -e0   4.7938534928
Quadrupole -name "LN3_QD_FH"        -length           0.04 -e0   4.7938534928 -strength 0.805367386791
Drift      -name "LN3_DR_QD_ED"     -length         0.0425 -e0   4.7938534928
Drift      -name "LN3_DR_SV"        -length            0.1 -e0   4.7938534928
Drift      -name "LN3_DR_BL"        -length           0.05 -e0   4.7938534928
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0   4.7938534928
Cavity     -name "LN3_XCA0"         -length      0.9164755 -gradient 0.065 -phase 10 -frequency 11.9942
SetReferenceEnergy  4.852421957977222e+00
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  4.85242195798
Drift      -name "LN3_DR_BL"        -length           0.05 -e0  4.85242195798
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  4.85242195798
Cavity     -name "LN3_XCA0"         -length      0.9164755 -gradient 0.065 -phase 10 -frequency 11.9942
SetReferenceEnergy  4.910990423002207e+00
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0    4.910990423
Drift      -name "LN3_DR_BL"        -length           0.05 -e0    4.910990423
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0    4.910990423
Cavity     -name "LN3_XCA0"         -length      0.9164755 -gradient 0.065 -phase 10 -frequency 11.9942
SetReferenceEnergy  4.969558888064736e+00
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  4.96955888806
Drift      -name "LN3_DR_BL"        -length           0.05 -e0  4.96955888806
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  4.96955888806
Cavity     -name "LN3_XCA0"         -length      0.9164755 -gradient 0.065 -phase 10 -frequency 11.9942
SetReferenceEnergy  5.028127353593020e+00
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  5.02812735359
Drift      -name "LN3_DR_BL"        -length           0.05 -e0  5.02812735359
Drift      -name "LN3_DR_QD_ED"     -length         0.0425 -e0  5.02812735359
Quadrupole -name "LN3_QD_DH"        -length           0.04 -e0  5.02812735359 -strength -0.844725395404
Dipole     -name "LN3_COR"          -length              0 -e0  5.02812735359
Bpm        -name "LN3_BPM"          -length              0 -e0  5.02812735359
Quadrupole -name "LN3_QD_DH"        -length           0.04 -e0  5.02812735359 -strength -0.844725395404
Drift      -name "LN3_DR_QD_ED"     -length         0.0425 -e0  5.02812735359
Drift      -name "LN3_DR_VS"        -length            0.1 -e0  5.02812735359
Drift      -name "LN3_DR_BL"        -length           0.05 -e0  5.02812735359
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  5.02812735359
Cavity     -name "LN3_XCA0"         -length      0.9164755 -gradient 0.065 -phase 10 -frequency 11.9942
SetReferenceEnergy  5.086695819133933e+00
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  5.08669581913
Drift      -name "LN3_DR_BL"        -length           0.05 -e0  5.08669581913
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  5.08669581913
Cavity     -name "LN3_XCA0"         -length      0.9164755 -gradient 0.065 -phase 10 -frequency 11.9942
SetReferenceEnergy  5.145264284935275e+00
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  5.14526428494
Drift      -name "LN3_DR_BL"        -length           0.05 -e0  5.14526428494
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  5.14526428494
Cavity     -name "LN3_XCA0"         -length      0.9164755 -gradient 0.065 -phase 10 -frequency 11.9942
SetReferenceEnergy  5.203832750491312e+00
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  5.20383275049
Drift      -name "LN3_DR_BL"        -length           0.05 -e0  5.20383275049
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  5.20383275049
Cavity     -name "LN3_XCA0"         -length      0.9164755 -gradient 0.065 -phase 10 -frequency 11.9942
SetReferenceEnergy  5.262401215794672e+00
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  5.26240121579
Drift      -name "LN3_DR_BL"        -length           0.05 -e0  5.26240121579
Drift      -name "LN3_DR_QD_ED"     -length         0.0425 -e0  5.26240121579
Quadrupole -name "LN3_QD_FH"        -length           0.04 -e0  5.26240121579 -strength 0.884083404254
Dipole     -name "LN3_COR"          -length              0 -e0  5.26240121579
Bpm        -name "LN3_BPM"          -length              0 -e0  5.26240121579
Quadrupole -name "LN3_QD_FH"        -length           0.04 -e0  5.26240121579 -strength 0.884083404254
Drift      -name "LN3_DR_QD_ED"     -length         0.0425 -e0  5.26240121579
Drift      -name "LN3_DR_SV"        -length            0.1 -e0  5.26240121579
Drift      -name "LN3_DR_BL"        -length           0.05 -e0  5.26240121579
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  5.26240121579
Cavity     -name "LN3_XCA0"         -length      0.9164755 -gradient 0.065 -phase 10 -frequency 11.9942
SetReferenceEnergy  5.320969681355772e+00
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  5.32096968136
Drift      -name "LN3_DR_BL"        -length           0.05 -e0  5.32096968136
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  5.32096968136
Cavity     -name "LN3_XCA0"         -length      0.9164755 -gradient 0.065 -phase 10 -frequency 11.9942
SetReferenceEnergy  5.379538147152460e+00
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  5.37953814715
Drift      -name "LN3_DR_BL"        -length           0.05 -e0  5.37953814715
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  5.37953814715
Cavity     -name "LN3_XCA0"         -length      0.9164755 -gradient 0.065 -phase 10 -frequency 11.9942
SetReferenceEnergy  5.438106613148654e+00
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  5.43810661315
Drift      -name "LN3_DR_BL"        -length           0.05 -e0  5.43810661315
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  5.43810661315
Cavity     -name "LN3_XCA0"         -length      0.9164755 -gradient 0.065 -phase 10 -frequency 11.9942
SetReferenceEnergy  5.496675078882952e+00
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  5.49667507888
Drift      -name "LN3_DR_BL"        -length           0.05 -e0  5.49667507888
Drift      -name "LN3_DR_QD_ED"     -length         0.0425 -e0  5.49667507888
Quadrupole -name "LN3_QD_DH"        -length           0.04 -e0  5.49667507888 -strength -0.923441413252
Dipole     -name "LN3_COR"          -length              0 -e0  5.49667507888
Bpm        -name "LN3_BPM"          -length              0 -e0  5.49667507888
Quadrupole -name "LN3_QD_DH"        -length           0.04 -e0  5.49667507888 -strength -0.923441413252
Drift      -name "LN3_DR_QD_ED"     -length         0.0425 -e0  5.49667507888
Drift      -name "LN3_DR_DR"        -length            0.1 -e0  5.49667507888
Drift      -name "LN3_DR_BL"        -length           0.05 -e0  5.49667507888
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  5.49667507888
Drift      -name "LN3_DR_XCA_"      -length      0.9164755 -e0  5.49667507888
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  5.49667507888
Drift      -name "LN3_DR_BL"        -length           0.05 -e0  5.49667507888
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  5.49667507888
Drift      -name "LN3_DR_XCA_"      -length      0.9164755 -e0  5.49667507888
Drift      -name "LN3_DR_XCA_ED"    -length     0.05676225 -e0  5.49667507888
Drift      -name "LN3_DR_CT"        -length            0.1 -e0  5.49667507888
Drift      -name "LN3_WA_OU2"       -length              0 -e0  5.49667507888
Drift      -name "LN3_DR_BP_V1"     -length  1.98870083041 -e0  5.49667507888
Drift      -name "LN3_DR_QD_ED"     -length         0.0425 -e0  5.49667507888
Quadrupole -name "LN3_QD_BP_V01"    -length           0.04 -e0  5.49667507888 -strength -1.71310835353
Dipole     -name "LN3_COR"          -length              0 -e0  5.49667507888
Bpm        -name "LN3_BPM"          -length              0 -e0  5.49667507888
Quadrupole -name "LN3_QD_BP_V01"    -length           0.04 -e0  5.49667507888 -strength -1.71310835353
Drift      -name "LN3_DR_QD_ED"     -length         0.0425 -e0  5.49667507888
Drift      -name "LN3_DR_BP_V2"     -length  2.42766817127 -e0  5.49667507888
Drift      -name "LN3_DR_QD_ED"     -length         0.0425 -e0  5.49667507888
Quadrupole -name "LN3_QD_BP_V02"    -length           0.04 -e0  5.49667507888 -strength  1.89162126858
Dipole     -name "LN3_COR"          -length              0 -e0  5.49667507888
Bpm        -name "LN3_BPM"          -length              0 -e0  5.49667507888
Quadrupole -name "LN3_QD_BP_V02"    -length           0.04 -e0  5.49667507888 -strength  1.89162126858
Drift      -name "LN3_DR_QD_ED"     -length         0.0425 -e0  5.49667507888
Drift      -name "LN3_DR_BP_V3"     -length  1.21333551627 -e0  5.49667507888
Drift      -name "LN3_DR_QD_ED"     -length         0.0425 -e0  5.49667507888
Quadrupole -name "LN3_QD_BP_V03"    -length           0.04 -e0  5.49667507888 -strength -1.80345073798
Dipole     -name "LN3_COR"          -length              0 -e0  5.49667507888
Bpm        -name "LN3_BPM"          -length              0 -e0  5.49667507888
Quadrupole -name "LN3_QD_BP_V03"    -length           0.04 -e0  5.49667507888 -strength -1.80345073798
Drift      -name "LN3_DR_QD_ED"     -length         0.0425 -e0  5.49667507888
Drift      -name "LN3_DR_BP_V4"     -length 0.200000003084 -e0  5.49667507888
Drift      -name "LN3_DR_BP_0"      -length            0.2 -e0  5.49667507888
Drift      -name "LN3_DR_BP_BL"     -length           0.05 -e0  5.49667507888
Drift      -name "LN3_DR_SCA_ED"    -length     0.08325105 -e0  5.49667507888
CrabCavity -name "LN3_SCA0"         -length      0.5334979 -voltage 0 -phase 90 -frequency 2.997
SetReferenceEnergy  5.496675078882952e+00
Drift      -name "LN3_DR_SCA_ED"    -length     0.08325105 -e0   5.4966701302
Drift      -name "LN3_DR_BP_BL"     -length           0.05 -e0   5.4966701302
Drift      -name "LN3_DR_BP_0"      -length            0.2 -e0   5.4966701302
Drift      -name "LN3_DR_BP"        -length            0.4 -e0   5.4966701302
Drift      -name "LN3_DR_BP_VS"     -length            0.1 -e0   5.4966701302
Drift      -name "LN3_DR_BP"        -length            0.4 -e0   5.4966701302
## Drift      -name "LN3_BP_WA_OU2"    -length              0 -e0   5.4966701302
Drift      -name "LN3_DP_SEPM"      -length           0.45 -e0   5.4966701302
Drift      -name "LN3_DR_BP"        -length            0.4 -e0   5.4966701302
Drift      -name "LN3_DR_BP"        -length            0.4 -e0   5.4966701302
Drift      -name "LN3_DR_BP"        -length            0.4 -e0   5.4966701302
Drift      -name "LN3_DR_BP"        -length            0.4 -e0   5.4966701302
Drift      -name "LN3_DR_BP"        -length            0.4 -e0   5.4966701302
Drift      -name "LN3_DR_BP_VS"     -length            0.1 -e0   5.4966701302
Drift      -name "LN3_DR_BP"        -length            0.4 -e0   5.4966701302
Drift      -name "LN3_BP_WA_OU2_"   -length              0 -e0   5.4966701302
Drift      -name "TMC_DR_V1"        -length 0.200411720325 -e0   5.4966701302
Drift      -name "TMC_DR_QD_ED"     -length         0.0425 -e0   5.4966701302
Quadrupole -name "TMC_QD_V01"       -length           0.04 -e0   5.4966701302 -strength  1.12073237116
Dipole     -name "TMC_COR"          -length              0 -e0   5.4966701302
Bpm        -name "TMC_BPM"          -length              0 -e0   5.4966701302
Quadrupole -name "TMC_QD_V01"       -length           0.04 -e0   5.4966701302 -strength  1.12073237116
Drift      -name "TMC_DR_QD_ED"     -length         0.0425 -e0   5.4966701302
Drift      -name "TMC_DR_V2"        -length   2.8664920379 -e0   5.4966701302
Drift      -name "TMC_DR_QD_ED"     -length         0.0425 -e0   5.4966701302
Quadrupole -name "TMC_QD_V02"       -length           0.04 -e0   5.4966701302 -strength -2.15976817358
Dipole     -name "TMC_COR"          -length              0 -e0   5.4966701302
Bpm        -name "TMC_BPM"          -length              0 -e0   5.4966701302
Quadrupole -name "TMC_QD_V02"       -length           0.04 -e0   5.4966701302 -strength -2.15976817358
Drift      -name "TMC_DR_QD_ED"     -length         0.0425 -e0   5.4966701302
Drift      -name "TMC_DR_V3"        -length 0.388538878731 -e0   5.4966701302
Drift      -name "TMC_DR_QD_ED"     -length         0.0425 -e0   5.4966701302
Quadrupole -name "TMC_QD_V03"       -length           0.04 -e0   5.4966701302 -strength  1.72636551759
Dipole     -name "TMC_COR"          -length              0 -e0   5.4966701302
Bpm        -name "TMC_BPM"          -length              0 -e0   5.4966701302
Quadrupole -name "TMC_QD_V03"       -length           0.04 -e0   5.4966701302 -strength  1.72636551759
Drift      -name "TMC_DR_QD_ED"     -length         0.0425 -e0   5.4966701302
Drift      -name "TMC_DR_V4"        -length            0.2 -e0   5.4966701302
Drift      -name "TMC_DR_20"        -length            0.5 -e0   5.4966701302
Sbend      -name "TMC_DP_DIP1"      -length     0.50000006 -e0  5.49667013028 -angle -0.00087266 -E1 0 -E2 -0.00087266 -hgap 0 -fint 0.5 -tilt 0  -six_dim 1 -csr 1 
Drift      -name "TMC_DR_SIDE_C"    -length            0.5 -e0  5.49666934678
Drift      -name "TMC_DR_SIDE"      -length     3.00000133 -e0  5.49666934678
Sbend      -name "TMC_DP_DIP2"      -length     0.50000006 -e0   5.4966693471 -angle 0.00087266 -E1 0.00087266 -E2 0 -hgap 0 -fint 0.5 -tilt 0  -six_dim 1 -csr 1 
Drift      -name "TMC_DR_CENT_C"    -length           0.25 -e0   5.4966693471
Bpm        -name "TMC_BPM"          -length              0 -e0   5.4966693471
Drift      -name "TMC_DR_CENT"      -length            0.5 -e0   5.4966693471
Sbend      -name "TMC_DP_DIP3"      -length     0.50000006 -e0  5.49666934683 -angle 0.00087266 -E1 0 -E2 0.00087266 -hgap 0 -fint 0.5 -tilt 0  -six_dim 1 -csr 1 
Drift      -name "TMC_DR_SIDE_C"    -length            0.5 -e0  5.49666856622
Drift      -name "TMC_DR_SIDE"      -length     3.00000133 -e0  5.49666856622
Sbend      -name "TMC_DP_DIP4"      -length     0.50000006 -e0  5.49666856599 -angle -0.00087266 -E1 -0.00087266 -E2 0 -hgap 0 -fint 0.5 -tilt 0  -six_dim 1 -csr 1 
Drift      -name "TMC_DR_20_C"      -length            0.5 -e0  5.49666778945
Drift      -name "TMC_WA_OU2"       -length              0 -e0  5.49666778945
Drift      -name "HXR_DR_V1"        -length            0.2 -e0  5.49666778945
Drift      -name "HXR_DR_QD_ED"     -length          0.052 -e0  5.49666778945
Quadrupole -name "HXR_QD_V01"       -length          0.013 -e0  5.49666778945 -strength  1.95319092591
Dipole     -name "HXR_COR"          -length              0 -e0  5.49666778945
Bpm        -name "HXR_BPM"          -length              0 -e0  5.49666778945
Quadrupole -name "HXR_QD_V01"       -length          0.013 -e0  5.49666778945 -strength  1.95319092591
Drift      -name "HXR_DR_QD_ED"     -length          0.052 -e0  5.49666778945
Drift      -name "HXR_DR_V2"        -length 0.589218190145 -e0  5.49666778945
Drift      -name "HXR_DR_QD_ED"     -length          0.052 -e0  5.49666778945
Quadrupole -name "HXR_QD_V02"       -length          0.013 -e0  5.49666778945 -strength -2.14370043789
Dipole     -name "HXR_COR"          -length              0 -e0  5.49666778945
Bpm        -name "HXR_BPM"          -length              0 -e0  5.49666778945
Quadrupole -name "HXR_QD_V02"       -length          0.013 -e0  5.49666778945 -strength -2.14370043789
Drift      -name "HXR_DR_QD_ED"     -length          0.052 -e0  5.49666778945
Drift      -name "HXR_DR_V3"        -length  1.14454782316 -e0  5.49666778945
Drift      -name "HXR_DR_QD_ED"     -length          0.052 -e0  5.49666778945
Quadrupole -name "HXR_QD_V03"       -length          0.013 -e0  5.49666778945 -strength 0.744176186289
Dipole     -name "HXR_COR"          -length              0 -e0  5.49666778945
Bpm        -name "HXR_BPM"          -length              0 -e0  5.49666778945
Quadrupole -name "HXR_QD_V03"       -length          0.013 -e0  5.49666778945 -strength 0.744176186289
Drift      -name "HXR_DR_QD_ED"     -length          0.052 -e0  5.49666778945
Drift      -name "HXR_DR_V4"        -length  2.75978980091 -e0  5.49666778945
Drift      -name "HXR_DR_CT"        -length            0.1 -e0  5.49666778945
Drift      -name "HXR_WA_M_OU2"     -length              0 -e0  5.49666778945
Drift      -name "HXR_DR_QD_ED"     -length          0.052 -e0  5.49666778945
Quadrupole -name "HXR_QD_FH"        -length          0.013 -e0  5.49666778945 -strength  1.07185021894
Dipole     -name "HXR_COR"          -length              0 -e0  5.49666778945
Bpm        -name "HXR_BPM"          -length              0 -e0  5.49666778945
Quadrupole -name "HXR_QD_FH"        -length          0.013 -e0  5.49666778945 -strength  1.07185021894
Drift      -name "HXR_DR_QD_ED"     -length          0.052 -e0  5.49666778945
Drift      -name "HXR_DR_WIG_ED"    -length          0.195 -e0  5.49666778945
Drift      -name "HXR_WIG_0"        -length          1.768 -e0  5.49666778945
Drift      -name "HXR_DR_WIG_ED"    -length          0.195 -e0  5.49666778945
Drift      -name "HXR_DR_QD_ED"     -length          0.052 -e0  5.49666778945
Quadrupole -name "HXR_QD_DH"        -length          0.013 -e0  5.49666778945 -strength -1.07185021894
Dipole     -name "HXR_COR"          -length              0 -e0  5.49666778945
Bpm        -name "HXR_BPM"          -length              0 -e0  5.49666778945
Quadrupole -name "HXR_QD_DH"        -length          0.013 -e0  5.49666778945 -strength -1.07185021894
Drift      -name "HXR_DR_QD_ED"     -length          0.052 -e0  5.49666778945
Drift      -name "HXR_DR_WIG_ED"    -length          0.195 -e0  5.49666778945
Drift      -name "HXR_WIG_0"        -length          1.768 -e0  5.49666778945
Drift      -name "HXR_DR_WIG_ED"    -length          0.195 -e0  5.49666778945
Drift      -name "HXR_DR_QD_ED"     -length          0.052 -e0  5.49666778945
Quadrupole -name "HXR_QD_FH"        -length          0.013 -e0  5.49666778945 -strength  1.07185021894
Drift      -name "HXR_WA_OU2"       -length              0 -e0  5.49666778945
