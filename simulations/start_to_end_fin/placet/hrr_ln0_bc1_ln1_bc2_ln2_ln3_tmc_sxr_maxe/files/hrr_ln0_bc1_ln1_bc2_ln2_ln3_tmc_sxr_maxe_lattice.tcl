Girder
SetReferenceEnergy   1.238601e-01
Drift      -name "LN0_DR_V5"        -length  6.639433e-01 -e0  1.237690e-01
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  1.237690e-01
Quadrupole -name "LN0_QD_V04"       -length  4.000000e-02 -e0  1.237690e-01 -strength  1.779631e-02
Dipole     -name "LN0_COR"          -length  0.000000e+00 -e0  1.237690e-01
Bpm        -name "LN0_BPM"          -length  0.000000e+00 -e0  1.237690e-01
Quadrupole -name "LN0_QD_V04"       -length  4.000000e-02 -e0  1.237690e-01 -strength  1.779631e-02
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  1.237690e-01
Drift      -name "LN0_DR_V6"        -length  2.598929e-01 -e0  1.237690e-01
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  1.237690e-01
Quadrupole -name "LN0_QD_V05"       -length  4.000000e-02 -e0  1.237690e-01 -strength  3.441588e-02
Dipole     -name "LN0_COR"          -length  0.000000e+00 -e0  1.237690e-01
Bpm        -name "LN0_BPM"          -length  0.000000e+00 -e0  1.237690e-01
Quadrupole -name "LN0_QD_V05"       -length  4.000000e-02 -e0  1.237690e-01 -strength  3.441588e-02
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  1.237690e-01
Drift      -name "LN0_DR_V7"        -length  1.419751e+00 -e0  1.237690e-01
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  1.237690e-01
Quadrupole -name "LN0_QD_V06"       -length  4.000000e-02 -e0  1.237690e-01 -strength -3.626929e-02
Dipole     -name "LN0_COR"          -length  0.000000e+00 -e0  1.237690e-01
Bpm        -name "LN0_BPM"          -length  0.000000e+00 -e0  1.237690e-01
Quadrupole -name "LN0_QD_V06"       -length  4.000000e-02 -e0  1.237690e-01 -strength -3.626929e-02
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  1.237690e-01
Drift      -name "LN0_DR_V8"        -length  2.000008e-01 -e0  1.237690e-01
Drift      -name "LN0_DR_VS"        -length  1.000000e-01 -e0  1.237690e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  1.237690e-01
Drift      -name "LN0_DR_XCA_ED_DI" -length  1.001043e-01 -e0  1.237690e-01
CrabCavity -name "LN0_XCA0_DI"      -length  9.997914e-01 -voltage  0.000000e+00 -phase  9.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.237690e-01
Drift      -name "LN0_DR_XCA_ED_DI" -length  1.001043e-01 -e0  1.237285e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  1.237285e-01
Drift      -name "LN0_TDS_DR"       -length  2.500000e-01 -e0  1.237285e-01
Drift      -name "LN0_TDS_DR"       -length  2.500000e-01 -e0  1.237285e-01
Drift      -name "LN0_TDS_DR"       -length  2.500000e-01 -e0  1.237285e-01
Drift      -name "LN0_TDS_DR"       -length  2.500000e-01 -e0  1.237285e-01
Drift      -name "LN0_TDS_DR"       -length  2.500000e-01 -e0  1.237285e-01
Drift      -name "LN0_TDS_DR"       -length  2.500000e-01 -e0  1.237285e-01
Drift      -name "LN0_TDS_DR"       -length  2.500000e-01 -e0  1.237285e-01
Drift      -name "LN0_TDS_DR"       -length  2.500000e-01 -e0  1.237285e-01
Sbend      -name "LN0_DP_TDS"       -length  1.000000e-01 -e0  1.237285e-01 -angle  0.000000e+00 -E1  0.000000e+00 -E2  0.000000e+00 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  0.000000e+00 -six_dim 1
Drift      -name "LN0_DR_20"        -length  2.000000e-01 -e0  1.237285e-01
Drift      -name "LN0_DR_VS"        -length  1.000000e-01 -e0  1.237285e-01
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  1.237285e-01
Quadrupole -name "LN0_QD_FH"        -length  4.000000e-02 -e0  1.237285e-01 -strength  3.464399e-02
Dipole     -name "LN0_COR"          -length  0.000000e+00 -e0  1.237285e-01
Bpm        -name "LN0_BPM"          -length  0.000000e+00 -e0  1.237285e-01
Quadrupole -name "LN0_QD_FH"        -length  4.000000e-02 -e0  1.237285e-01 -strength  3.464399e-02
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  1.237285e-01
Drift      -name "LN0_DR_SV"        -length  1.000000e-01 -e0  1.237285e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  1.237285e-01
Drift      -name "LN0_DR_CCA_ED"    -length  5.019800e-02 -e0  1.237285e-01
Cavity     -name "LN0_CCA0"         -length  1.899604e+00 -gradient  1.500000e-02 -phase  2.600000e+01 -frequency  5.997100e+00
SetReferenceEnergy   1.492816e-01
Drift      -name "LN0_DR_CCA_ED"    -length  5.019800e-02 -e0  1.492816e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  1.492816e-01
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  1.492816e-01
Quadrupole -name "LN0_QD_DH"        -length  4.000000e-02 -e0  1.492816e-01 -strength -4.179885e-02
Dipole     -name "LN0_COR"          -length  0.000000e+00 -e0  1.492816e-01
Bpm        -name "LN0_BPM"          -length  0.000000e+00 -e0  1.492816e-01
Quadrupole -name "LN0_QD_DH"        -length  4.000000e-02 -e0  1.492816e-01 -strength -4.179885e-02
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  1.492816e-01
Drift      -name "LN0_DR_VS"        -length  1.000000e-01 -e0  1.492816e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  1.492816e-01
Drift      -name "LN0_DR_CCA_ED"    -length  5.019800e-02 -e0  1.492816e-01
Cavity     -name "LN0_CCA0"         -length  1.899604e+00 -gradient  1.500000e-02 -phase  2.600000e+01 -frequency  5.997100e+00
SetReferenceEnergy   1.748347e-01
Drift      -name "LN0_DR_CCA_ED"    -length  5.019800e-02 -e0  1.748347e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  1.748347e-01
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  1.748347e-01
Quadrupole -name "LN0_QD_FH"        -length  4.000000e-02 -e0  1.748347e-01 -strength  4.895371e-02
Dipole     -name "LN0_COR"          -length  0.000000e+00 -e0  1.748347e-01
Bpm        -name "LN0_BPM"          -length  0.000000e+00 -e0  1.748347e-01
Quadrupole -name "LN0_QD_FH"        -length  4.000000e-02 -e0  1.748347e-01 -strength  4.895371e-02
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  1.748347e-01
Drift      -name "LN0_DR_VS"        -length  1.000000e-01 -e0  1.748347e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  1.748347e-01
Drift      -name "LN0_DR_CCA_ED"    -length  5.019800e-02 -e0  1.748347e-01
Cavity     -name "LN0_CCA0"         -length  1.899604e+00 -gradient  1.500000e-02 -phase  2.600000e+01 -frequency  5.997100e+00
SetReferenceEnergy   2.003877e-01
Drift      -name "LN0_DR_CCA_ED"    -length  5.019800e-02 -e0  2.003877e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  2.003877e-01
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  2.003877e-01
Quadrupole -name "LN0_QD_DH"        -length  4.000000e-02 -e0  2.003877e-01 -strength -5.610857e-02
Dipole     -name "LN0_COR"          -length  0.000000e+00 -e0  2.003877e-01
Bpm        -name "LN0_BPM"          -length  0.000000e+00 -e0  2.003877e-01
Quadrupole -name "LN0_QD_DH"        -length  4.000000e-02 -e0  2.003877e-01 -strength -5.610857e-02
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  2.003877e-01
Drift      -name "LN0_DR_VS"        -length  1.000000e-01 -e0  2.003877e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  2.003877e-01
Drift      -name "LN0_DR_CCA_ED"    -length  5.019800e-02 -e0  2.003877e-01
Cavity     -name "LN0_CCA0"         -length  1.899604e+00 -gradient  1.500000e-02 -phase  2.600000e+01 -frequency  5.997100e+00
SetReferenceEnergy   2.259408e-01
Drift      -name "LN0_DR_CCA_ED"    -length  5.019800e-02 -e0  2.259408e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  2.259408e-01
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  2.259408e-01
Quadrupole -name "LN0_QD_FH"        -length  4.000000e-02 -e0  2.259408e-01 -strength  6.326343e-02
Dipole     -name "LN0_COR"          -length  0.000000e+00 -e0  2.259408e-01
Bpm        -name "LN0_BPM"          -length  0.000000e+00 -e0  2.259408e-01
Quadrupole -name "LN0_QD_FH"        -length  4.000000e-02 -e0  2.259408e-01 -strength  6.326343e-02
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  2.259408e-01
Drift      -name "LN0_DR_VS"        -length  1.000000e-01 -e0  2.259408e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  2.259408e-01
Drift      -name "LN0_DR_CCA_ED"    -length  5.019800e-02 -e0  2.259408e-01
Cavity     -name "LN0_CCA0"         -length  1.899604e+00 -gradient  1.500000e-02 -phase  2.600000e+01 -frequency  5.997100e+00
SetReferenceEnergy   2.514939e-01
Drift      -name "LN0_DR_CCA_ED"    -length  5.019800e-02 -e0  2.514939e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  2.514939e-01
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  2.514939e-01
Quadrupole -name "LN0_QD_DH"        -length  4.000000e-02 -e0  2.514939e-01 -strength -7.041830e-02
Dipole     -name "LN0_COR"          -length  0.000000e+00 -e0  2.514939e-01
Bpm        -name "LN0_BPM"          -length  0.000000e+00 -e0  2.514939e-01
Quadrupole -name "LN0_QD_DH"        -length  4.000000e-02 -e0  2.514939e-01 -strength -7.041830e-02
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  2.514939e-01
Drift      -name "LN0_DR_VS"        -length  1.000000e-01 -e0  2.514939e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  2.514939e-01
Drift      -name "LN0_DR_CCA_ED"    -length  5.019800e-02 -e0  2.514939e-01
Cavity     -name "LN0_CCA0"         -length  1.899604e+00 -gradient  1.500000e-02 -phase  2.600000e+01 -frequency  5.997100e+00
SetReferenceEnergy   2.770470e-01
Drift      -name "LN0_DR_CCA_ED"    -length  5.019800e-02 -e0  2.770470e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  2.770470e-01
Drift      -name "LN0_WA_LNZ_IN2"   -length  0.000000e+00 -e0  2.770470e-01
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  2.770470e-01
Quadrupole -name "LN0_QD_FH"        -length  4.000000e-02 -e0  2.770470e-01 -strength  7.757317e-02
Dipole     -name "LN0_COR"          -length  0.000000e+00 -e0  2.770470e-01
Bpm        -name "LN0_BPM"          -length  0.000000e+00 -e0  2.770470e-01
Quadrupole -name "LN0_QD_FH"        -length  4.000000e-02 -e0  2.770470e-01 -strength  7.757317e-02
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  2.770470e-01
Drift      -name "LN0_DR_VS"        -length  1.000000e-01 -e0  2.770470e-01
Drift      -name "LN0_DR_LNZ_ED2"   -length  6.250000e-01 -e0  2.770470e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  2.770470e-01
Drift      -name "LN0_DR_KCA_ED"    -length  2.225410e-02 -e0  2.770470e-01
Cavity     -name "LN0_KCA0"         -length  3.054918e-01 -gradient  2.453617e-02 -phase  1.920000e+02 -frequency  3.598260e+01
SetReferenceEnergy   2.698730e-01
Drift      -name "LN0_DR_KCA_ED"    -length  2.225410e-02 -e0  2.698730e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  2.698730e-01
Drift      -name "LN0_DR_KCA_ED"    -length  2.225410e-02 -e0  2.698730e-01
Drift      -name "LN0_DR_KCA0"      -length  3.054918e-01 -e0  2.698730e-01
Drift      -name "LN0_DR_KCA_ED"    -length  2.225410e-02 -e0  2.698730e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  2.698730e-01
Drift      -name "LN0_DR_LNZ_ED2"   -length  6.250000e-01 -e0  2.698730e-01
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  2.698730e-01
Quadrupole -name "LN0_QD_DH"        -length  4.000000e-02 -e0  2.698730e-01 -strength -7.556445e-02
Dipole     -name "LN0_COR"          -length  0.000000e+00 -e0  2.698730e-01
Bpm        -name "LN0_BPM"          -length  0.000000e+00 -e0  2.698730e-01
Quadrupole -name "LN0_QD_DH"        -length  4.000000e-02 -e0  2.698730e-01 -strength -7.556445e-02
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  2.698730e-01
Drift      -name "LN0_DR_DR"        -length  1.000000e-01 -e0  2.698730e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  2.698730e-01
Drift      -name "LN0_DR_CCA_ED"    -length  5.019800e-02 -e0  2.698730e-01
Drift      -name "LN0_DR_CCA0H"     -length  3.498020e-01 -e0  2.698730e-01
Drift      -name "LN0_DR_A"         -length  2.000000e-01 -e0  2.698730e-01
Drift      -name "LN0_DR_VS"        -length  1.000000e-01 -e0  2.698730e-01
Drift      -name "LN0_DR_A"         -length  2.000000e-01 -e0  2.698730e-01
Drift      -name "LN0_DR_CT"        -length  1.000000e-01 -e0  2.698730e-01
Drift      -name "LN0_WA_OU2"       -length  0.000000e+00 -e0  2.698730e-01
Drift      -name "BC1_DR_V1"        -length  5.382067e-01 -e0  2.698730e-01
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.698730e-01
Quadrupole -name "BC1_QD_V01"       -length  4.000000e-02 -e0  2.698730e-01 -strength  2.032735e-02
Dipole     -name "BC1_COR"          -length  0.000000e+00 -e0  2.698730e-01
Bpm        -name "BC1_BPM"          -length  0.000000e+00 -e0  2.698730e-01
Quadrupole -name "BC1_QD_V01"       -length  4.000000e-02 -e0  2.698730e-01 -strength  2.032735e-02
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.698730e-01
Drift      -name "BC1_DR_V2"        -length  5.900340e+00 -e0  2.698730e-01
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.698730e-01
Quadrupole -name "BC1_QD_V02"       -length  4.000000e-02 -e0  2.698730e-01 -strength -9.786305e-02
Dipole     -name "BC1_COR"          -length  0.000000e+00 -e0  2.698730e-01
Bpm        -name "BC1_BPM"          -length  0.000000e+00 -e0  2.698730e-01
Quadrupole -name "BC1_QD_V02"       -length  4.000000e-02 -e0  2.698730e-01 -strength -9.786305e-02
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.698730e-01
Drift      -name "BC1_DR_V3"        -length  2.496128e-01 -e0  2.698730e-01
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.698730e-01
Quadrupole -name "BC1_QD_V03"       -length  4.000000e-02 -e0  2.698730e-01 -strength  3.530938e-02
Dipole     -name "BC1_COR"          -length  0.000000e+00 -e0  2.698730e-01
Bpm        -name "BC1_BPM"          -length  0.000000e+00 -e0  2.698730e-01
Quadrupole -name "BC1_QD_V03"       -length  4.000000e-02 -e0  2.698730e-01 -strength  3.530938e-02
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.698730e-01
Drift      -name "BC1_DR_V4"        -length  2.000000e-01 -e0  2.698730e-01
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.698730e-01
Quadrupole -name "BC1_QD_V04"       -length  4.000000e-02 -e0  2.698730e-01 -strength  5.690781e-02
Dipole     -name "BC1_COR"          -length  0.000000e+00 -e0  2.698730e-01
Bpm        -name "BC1_BPM"          -length  0.000000e+00 -e0  2.698730e-01
Quadrupole -name "BC1_QD_V04"       -length  4.000000e-02 -e0  2.698730e-01 -strength  5.690781e-02
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.698730e-01
Drift      -name "BC1_DR_V5"        -length  2.002040e-01 -e0  2.698730e-01
Drift      -name "BC1_DR_20"        -length  5.000000e-01 -e0  2.698730e-01
Sbend      -name "BC1_DP_DIP1"      -length  4.002981e-01 -e0  2.698729e-01 -angle -6.684611e-02 -E1  0.000000e+00 -E2 -6.684611e-02 -hgap  1.500000e-02 -fint  5.000000e-01 -tilt  0.000000e+00  -six_dim 1 -csr 1 
Drift      -name "BC1_DR_SIDE_C"    -length  3.000000e-01 -e0  2.698724e-01
Drift      -name "BC1_DR_SIDE"      -length  2.957275e+00 -e0  2.698724e-01
Sbend      -name "BC1_DP_DIP2"      -length  4.002981e-01 -e0  2.698720e-01 -angle  6.684611e-02 -E1  6.684611e-02 -E2  0.000000e+00 -hgap  1.500000e-02 -fint  5.000000e-01 -tilt  0.000000e+00  -six_dim 1 -csr 1 
Drift      -name "BC1_DR_CENT_C"    -length  3.500000e-01 -e0  2.698701e-01
Bpm        -name "BC1_BPM"          -length  0.000000e+00 -e0  2.698701e-01
Drift      -name "BC1_DR_CENT"      -length  3.500000e-01 -e0  2.698701e-01
Sbend      -name "BC1_DP_DIP3"      -length  4.002981e-01 -e0  2.698608e-01 -angle  6.684611e-02 -E1  0.000000e+00 -E2  6.684611e-02 -hgap  1.500000e-02 -fint  5.000000e-01 -tilt  0.000000e+00  -six_dim 1 -csr 1 
Drift      -name "BC1_DR_SIDE_C"    -length  3.000000e-01 -e0  2.698418e-01
Drift      -name "BC1_DR_SIDE"      -length  2.957275e+00 -e0  2.698418e-01
Sbend      -name "BC1_DP_DIP4"      -length  4.002981e-01 -e0  2.698179e-01 -angle -6.684611e-02 -E1 -6.684611e-02 -E2  0.000000e+00 -hgap  1.500000e-02 -fint  5.000000e-01 -tilt  0.000000e+00  -six_dim 1 -csr 1 
Drift      -name "BC1_DR_20_C"      -length  3.000000e-01 -e0  2.698013e-01
Drift      -name "BC1_WA_OU2"       -length  0.000000e+00 -e0  2.698013e-01
Drift      -name "BC1_DR_CT"        -length  1.000000e-01 -e0  2.698013e-01
Drift      -name "BC1_DR_DI_V1"     -length  2.911672e-01 -e0  2.698013e-01
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.698013e-01
Quadrupole -name "BC1_QD_DI_V01"    -length  4.000000e-02 -e0  2.698013e-01 -strength -9.231869e-02
Dipole     -name "BC1_COR_DI"       -length  0.000000e+00 -e0  2.698013e-01
Bpm        -name "BC1_BPM_DI"       -length  0.000000e+00 -e0  2.698013e-01
Quadrupole -name "BC1_QD_DI_V01"    -length  4.000000e-02 -e0  2.698013e-01 -strength -9.231869e-02
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.698013e-01
Drift      -name "BC1_DR_DI_V2"     -length  4.700020e-01 -e0  2.698013e-01
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.698013e-01
Quadrupole -name "BC1_QD_DI_V02"    -length  4.000000e-02 -e0  2.698013e-01 -strength  1.468554e-01
Dipole     -name "BC1_COR_DI"       -length  0.000000e+00 -e0  2.698013e-01
Bpm        -name "BC1_BPM_DI"       -length  0.000000e+00 -e0  2.698013e-01
Quadrupole -name "BC1_QD_DI_V02"    -length  4.000000e-02 -e0  2.698013e-01 -strength  1.468554e-01
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.698013e-01
Drift      -name "BC1_DR_DI_V3"     -length  1.199083e+00 -e0  2.698013e-01
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.698013e-01
Quadrupole -name "BC1_QD_DI_V03"    -length  4.000000e-02 -e0  2.698013e-01 -strength -5.605805e-02
Dipole     -name "BC1_COR_DI"       -length  0.000000e+00 -e0  2.698013e-01
Bpm        -name "BC1_BPM_DI"       -length  0.000000e+00 -e0  2.698013e-01
Quadrupole -name "BC1_QD_DI_V03"    -length  4.000000e-02 -e0  2.698013e-01 -strength -5.605805e-02
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.698013e-01
Drift      -name "BC1_DR_DI_V5"     -length  2.000000e-01 -e0  2.698013e-01
Drift      -name "BC1_DR_DI"        -length  2.500000e-01 -e0  2.698013e-01
Drift      -name "BC1_DR_VS"        -length  1.000000e-01 -e0  2.698013e-01
Drift      -name "BC1_DR_BL"        -length  5.000000e-02 -e0  2.698013e-01
Drift      -name "BC1_DR_XCA_ED_DI" -length  1.001043e-01 -e0  2.698013e-01
CrabCavity -name "BC1_XCA0_DI"      -length  9.997914e-01 -voltage  0.000000e+00 -phase  9.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.698013e-01
Drift      -name "BC1_DR_XCA_ED_DI" -length  1.001043e-01 -e0  2.697334e-01
Drift      -name "BC1_DR_BL"        -length  5.000000e-02 -e0  2.697334e-01
Drift      -name "BC1_DR_VS"        -length  1.000000e-01 -e0  2.697334e-01
Drift      -name "BC1_DR_DI"        -length  2.500000e-01 -e0  2.697334e-01
Drift      -name "BC1_DR_DI"        -length  2.500000e-01 -e0  2.697334e-01
Drift      -name "BC1_DR_DI_QD_ED"  -length  4.250000e-02 -e0  2.697334e-01
Quadrupole -name "BC1_QD_DI_FH"     -length  4.000000e-02 -e0  2.697334e-01 -strength  8.631467e-02
Dipole     -name "BC1_COR_DI"       -length  0.000000e+00 -e0  2.697334e-01
Bpm        -name "BC1_BPM_DI"       -length  0.000000e+00 -e0  2.697334e-01
Quadrupole -name "BC1_QD_DI_FH"     -length  4.000000e-02 -e0  2.697334e-01 -strength  8.631467e-02
Drift      -name "BC1_DR_DI_QD_ED"  -length  4.250000e-02 -e0  2.697334e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697334e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697334e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697334e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697334e-01
Drift      -name "BC1_DR_DI_VS"     -length  1.000000e-01 -e0  2.697334e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697334e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697334e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697334e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697334e-01
Drift      -name "BC1_DR_DI_QD_ED"  -length  4.250000e-02 -e0  2.697334e-01
Quadrupole -name "BC1_QD_DI_DH"     -length  4.000000e-02 -e0  2.697334e-01 -strength -8.631467e-02
Dipole     -name "BC1_COR_DI"       -length  0.000000e+00 -e0  2.697334e-01
Bpm        -name "BC1_BPM_DI"       -length  0.000000e+00 -e0  2.697334e-01
Quadrupole -name "BC1_QD_DI_DH"     -length  4.000000e-02 -e0  2.697334e-01 -strength -8.631467e-02
Drift      -name "BC1_DR_DI_QD_ED"  -length  4.250000e-02 -e0  2.697334e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697334e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697334e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697334e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697334e-01
Drift      -name "BC1_DR_DI_VS"     -length  1.000000e-01 -e0  2.697334e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697334e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697334e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697334e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697334e-01
Drift      -name "BC1_DR_DI_QD_ED"  -length  4.250000e-02 -e0  2.697334e-01
Quadrupole -name "BC1_QD_DI_FH"     -length  4.000000e-02 -e0  2.697334e-01 -strength  8.631467e-02
Dipole     -name "BC1_COR_DI"       -length  0.000000e+00 -e0  2.697334e-01
Bpm        -name "BC1_BPM_DI"       -length  0.000000e+00 -e0  2.697334e-01
Quadrupole -name "BC1_QD_DI_FH"     -length  4.000000e-02 -e0  2.697334e-01 -strength  8.631467e-02
Drift      -name "BC1_DR_DI_QD_ED"  -length  4.250000e-02 -e0  2.697334e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697334e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697334e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697334e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697334e-01
Drift      -name "BC1_DR_DI_VS"     -length  1.000000e-01 -e0  2.697334e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697334e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697334e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697334e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697334e-01
Drift      -name "BC1_DR_DI_QD_ED"  -length  4.250000e-02 -e0  2.697334e-01
Quadrupole -name "BC1_QD_DI_DH"     -length  4.000000e-02 -e0  2.697334e-01 -strength -8.631467e-02
Dipole     -name "BC1_COR_DI"       -length  0.000000e+00 -e0  2.697334e-01
Bpm        -name "BC1_BPM_DI"       -length  0.000000e+00 -e0  2.697334e-01
Quadrupole -name "BC1_QD_DI_DH"     -length  4.000000e-02 -e0  2.697334e-01 -strength -8.631467e-02
Drift      -name "BC1_DR_DI_QD_ED"  -length  4.250000e-02 -e0  2.697334e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697334e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697334e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697334e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697334e-01
Drift      -name "BC1_DR_DI_VS"     -length  1.000000e-01 -e0  2.697334e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697334e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697334e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697334e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697334e-01
Drift      -name "BC1_DR_DI_QD_ED"  -length  4.250000e-02 -e0  2.697334e-01
Quadrupole -name "BC1_QD_DI_FH"     -length  4.000000e-02 -e0  2.697334e-01 -strength  8.631467e-02
Dipole     -name "BC1_COR_DI"       -length  0.000000e+00 -e0  2.697334e-01
Bpm        -name "BC1_BPM_DI"       -length  0.000000e+00 -e0  2.697334e-01
Quadrupole -name "BC1_QD_DI_FH"     -length  4.000000e-02 -e0  2.697334e-01 -strength  8.631467e-02
Drift      -name "BC1_DR_DI_QD_ED"  -length  4.250000e-02 -e0  2.697334e-01
Drift      -name "BC1_DR_DI"        -length  2.500000e-01 -e0  2.697334e-01
Sbend      -name "BC1_DP_DI"        -length  2.500000e-01 -e0  2.697334e-01 -angle  0.000000e+00 -E1  0.000000e+00 -E2  0.000000e+00 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  0.000000e+00 -six_dim 1
Drift      -name "BC1_DR_DI"        -length  2.500000e-01 -e0  2.697334e-01
Drift      -name "BC1_DR_DI"        -length  2.500000e-01 -e0  2.697334e-01
Drift      -name "BC1_DR_DI"        -length  2.500000e-01 -e0  2.697334e-01
Drift      -name "BC1_WA_OU_DI2"    -length  0.000000e+00 -e0  2.697334e-01
Drift      -name "LN1_DR_V1"        -length  2.406495e-01 -e0  2.697334e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  2.697334e-01
Quadrupole -name "LN1_QD_V01"       -length  4.000000e-02 -e0  2.697334e-01 -strength  5.687075e-02
Dipole     -name "LN1_COR"          -length  0.000000e+00 -e0  2.697334e-01
Bpm        -name "LN1_BPM"          -length  0.000000e+00 -e0  2.697334e-01
Quadrupole -name "LN1_QD_V01"       -length  4.000000e-02 -e0  2.697334e-01 -strength  5.687075e-02
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  2.697334e-01
Drift      -name "LN1_DR_V2"        -length  2.473804e-01 -e0  2.697334e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  2.697334e-01
Quadrupole -name "LN1_QD_V02"       -length  4.000000e-02 -e0  2.697334e-01 -strength -1.170449e-01
Dipole     -name "LN1_COR"          -length  0.000000e+00 -e0  2.697334e-01
Bpm        -name "LN1_BPM"          -length  0.000000e+00 -e0  2.697334e-01
Quadrupole -name "LN1_QD_V02"       -length  4.000000e-02 -e0  2.697334e-01 -strength -1.170449e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  2.697334e-01
Drift      -name "LN1_DR_V3"        -length  1.851602e+00 -e0  2.697334e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  2.697334e-01
Quadrupole -name "LN1_QD_V03"       -length  4.000000e-02 -e0  2.697334e-01 -strength  1.358433e-02
Dipole     -name "LN1_COR"          -length  0.000000e+00 -e0  2.697334e-01
Bpm        -name "LN1_BPM"          -length  0.000000e+00 -e0  2.697334e-01
Quadrupole -name "LN1_QD_V03"       -length  4.000000e-02 -e0  2.697334e-01 -strength  1.358433e-02
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  2.697334e-01
Drift      -name "LN1_DR_V4"        -length  1.000069e-01 -e0  2.697334e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  2.697334e-01
Quadrupole -name "LN1_QD_FH"        -length  4.000000e-02 -e0  2.697334e-01 -strength  7.120961e-02
Dipole     -name "LN1_COR"          -length  0.000000e+00 -e0  2.697334e-01
Bpm        -name "LN1_BPM"          -length  0.000000e+00 -e0  2.697334e-01
Quadrupole -name "LN1_QD_FH"        -length  4.000000e-02 -e0  2.697334e-01 -strength  7.120961e-02
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  2.697334e-01
Drift      -name "LN1_DR_SV"        -length  1.000000e-01 -e0  2.697334e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  2.697334e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  2.697334e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  3.100000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.932193e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  2.932193e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  2.932193e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  2.932193e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  3.100000e+01 -frequency  1.199420e+01
SetReferenceEnergy   3.167051e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  3.167051e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  3.167051e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  3.167051e-01
Quadrupole -name "LN1_QD_DH"        -length  4.000000e-02 -e0  3.167051e-01 -strength -8.361016e-02
Dipole     -name "LN1_COR"          -length  0.000000e+00 -e0  3.167051e-01
Bpm        -name "LN1_BPM"          -length  0.000000e+00 -e0  3.167051e-01
Quadrupole -name "LN1_QD_DH"        -length  4.000000e-02 -e0  3.167051e-01 -strength -8.361016e-02
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  3.167051e-01
Drift      -name "LN1_DR_VS"        -length  1.000000e-01 -e0  3.167051e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  3.167051e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  3.167051e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  3.100000e+01 -frequency  1.199420e+01
SetReferenceEnergy   3.401910e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  3.401910e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  3.401910e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  3.401910e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  3.100000e+01 -frequency  1.199420e+01
SetReferenceEnergy   3.636769e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  3.636769e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  3.636769e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  3.636769e-01
Quadrupole -name "LN1_QD_FH"        -length  4.000000e-02 -e0  3.636769e-01 -strength  9.601070e-02
Dipole     -name "LN1_COR"          -length  0.000000e+00 -e0  3.636769e-01
Bpm        -name "LN1_BPM"          -length  0.000000e+00 -e0  3.636769e-01
Quadrupole -name "LN1_QD_FH"        -length  4.000000e-02 -e0  3.636769e-01 -strength  9.601070e-02
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  3.636769e-01
Drift      -name "LN1_DR_SV"        -length  1.000000e-01 -e0  3.636769e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  3.636769e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  3.636769e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  3.100000e+01 -frequency  1.199420e+01
SetReferenceEnergy   3.871628e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  3.871628e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  3.871628e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  3.871628e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  3.100000e+01 -frequency  1.199420e+01
SetReferenceEnergy   4.106486e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  4.106486e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  4.106486e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  4.106486e-01
Quadrupole -name "LN1_QD_DH"        -length  4.000000e-02 -e0  4.106486e-01 -strength -1.084112e-01
Dipole     -name "LN1_COR"          -length  0.000000e+00 -e0  4.106486e-01
Bpm        -name "LN1_BPM"          -length  0.000000e+00 -e0  4.106486e-01
Quadrupole -name "LN1_QD_DH"        -length  4.000000e-02 -e0  4.106486e-01 -strength -1.084112e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  4.106486e-01
Drift      -name "LN1_DR_VS"        -length  1.000000e-01 -e0  4.106486e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  4.106486e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  4.106486e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  3.100000e+01 -frequency  1.199420e+01
SetReferenceEnergy   4.341345e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  4.341345e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  4.341345e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  4.341345e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  3.100000e+01 -frequency  1.199420e+01
SetReferenceEnergy   4.576203e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  4.576203e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  4.576203e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  4.576203e-01
Quadrupole -name "LN1_QD_FH"        -length  4.000000e-02 -e0  4.576203e-01 -strength  1.208118e-01
Dipole     -name "LN1_COR"          -length  0.000000e+00 -e0  4.576203e-01
Bpm        -name "LN1_BPM"          -length  0.000000e+00 -e0  4.576203e-01
Quadrupole -name "LN1_QD_FH"        -length  4.000000e-02 -e0  4.576203e-01 -strength  1.208118e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  4.576203e-01
Drift      -name "LN1_DR_SV"        -length  1.000000e-01 -e0  4.576203e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  4.576203e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  4.576203e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  3.100000e+01 -frequency  1.199420e+01
SetReferenceEnergy   4.811062e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  4.811062e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  4.811062e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  4.811062e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  3.100000e+01 -frequency  1.199420e+01
SetReferenceEnergy   5.045920e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  5.045920e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  5.045920e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  5.045920e-01
Quadrupole -name "LN1_QD_DH"        -length  4.000000e-02 -e0  5.045920e-01 -strength -1.332123e-01
Dipole     -name "LN1_COR"          -length  0.000000e+00 -e0  5.045920e-01
Bpm        -name "LN1_BPM"          -length  0.000000e+00 -e0  5.045920e-01
Quadrupole -name "LN1_QD_DH"        -length  4.000000e-02 -e0  5.045920e-01 -strength -1.332123e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  5.045920e-01
Drift      -name "LN1_DR_VS"        -length  1.000000e-01 -e0  5.045920e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  5.045920e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  5.045920e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  3.100000e+01 -frequency  1.199420e+01
SetReferenceEnergy   5.280779e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  5.280779e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  5.280779e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  5.280779e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  3.100000e+01 -frequency  1.199420e+01
SetReferenceEnergy   5.515637e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  5.515637e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  5.515637e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  5.515637e-01
Quadrupole -name "LN1_QD_FH"        -length  4.000000e-02 -e0  5.515637e-01 -strength  1.456128e-01
Dipole     -name "LN1_COR"          -length  0.000000e+00 -e0  5.515637e-01
Bpm        -name "LN1_BPM"          -length  0.000000e+00 -e0  5.515637e-01
Quadrupole -name "LN1_QD_FH"        -length  4.000000e-02 -e0  5.515637e-01 -strength  1.456128e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  5.515637e-01
Drift      -name "LN1_DR_SV"        -length  1.000000e-01 -e0  5.515637e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  5.515637e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  5.515637e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  3.100000e+01 -frequency  1.199420e+01
SetReferenceEnergy   5.750496e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  5.750496e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  5.750496e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  5.750496e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  3.100000e+01 -frequency  1.199420e+01
SetReferenceEnergy   5.985354e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  5.985354e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  5.985354e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  5.985354e-01
Quadrupole -name "LN1_QD_DH"        -length  4.000000e-02 -e0  5.985354e-01 -strength -1.580134e-01
Dipole     -name "LN1_COR"          -length  0.000000e+00 -e0  5.985354e-01
Bpm        -name "LN1_BPM"          -length  0.000000e+00 -e0  5.985354e-01
Quadrupole -name "LN1_QD_DH"        -length  4.000000e-02 -e0  5.985354e-01 -strength -1.580134e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  5.985354e-01
Drift      -name "LN1_DR_VS"        -length  1.000000e-01 -e0  5.985354e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  5.985354e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  5.985354e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  3.100000e+01 -frequency  1.199420e+01
SetReferenceEnergy   6.220213e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  6.220213e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  6.220213e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  6.220213e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  3.100000e+01 -frequency  1.199420e+01
SetReferenceEnergy   6.455071e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  6.455071e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  6.455071e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  6.455071e-01
Quadrupole -name "LN1_QD_FH"        -length  4.000000e-02 -e0  6.455071e-01 -strength  1.704139e-01
Dipole     -name "LN1_COR"          -length  0.000000e+00 -e0  6.455071e-01
Bpm        -name "LN1_BPM"          -length  0.000000e+00 -e0  6.455071e-01
Quadrupole -name "LN1_QD_FH"        -length  4.000000e-02 -e0  6.455071e-01 -strength  1.704139e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  6.455071e-01
Drift      -name "LN1_DR_DR"        -length  1.000000e-01 -e0  6.455071e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  6.455071e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  6.455071e-01
Drift      -name "LN1_DR_XCA_"      -length  9.164755e-01 -e0  6.455071e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  6.455071e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  6.455071e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  6.455071e-01
Drift      -name "LN1_DR_XCA_"      -length  9.164755e-01 -e0  6.455071e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  6.455071e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  6.455071e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  6.455071e-01
Quadrupole -name "LN1_QD_DH"        -length  4.000000e-02 -e0  6.455071e-01 -strength -1.704139e-01
Dipole     -name "LN1_COR"          -length  0.000000e+00 -e0  6.455071e-01
Bpm        -name "LN1_BPM"          -length  0.000000e+00 -e0  6.455071e-01
Quadrupole -name "LN1_QD_DH"        -length  4.000000e-02 -e0  6.455071e-01 -strength -1.704139e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  6.455071e-01
Drift      -name "LN1_DR_DR"        -length  1.000000e-01 -e0  6.455071e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  6.455071e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  6.455071e-01
Drift      -name "LN1_DR_XCA_A"     -length  8.164755e-01 -e0  6.455071e-01
Drift      -name "LN1_DR_VS"        -length  1.000000e-01 -e0  6.455071e-01
Drift      -name "LN1_WA_OU2"       -length  0.000000e+00 -e0  6.455071e-01
Drift      -name "BC2_IN_CNT"       -length  0.000000e+00 -e0  6.455071e-01
Drift      -name "BC2_DR_V1"        -length  4.718667e-01 -e0  6.455071e-01
Drift      -name "BC2_DR_QD_ED"     -length  4.250000e-02 -e0  6.455071e-01
Quadrupole -name "BC2_QD_V01"       -length  4.000000e-02 -e0  6.455071e-01 -strength  9.773863e-03
Dipole     -name "BC2_COR"          -length  0.000000e+00 -e0  6.455071e-01
Bpm        -name "BC2_BPM"          -length  0.000000e+00 -e0  6.455071e-01
Quadrupole -name "BC2_QD_V01"       -length  4.000000e-02 -e0  6.455071e-01 -strength  9.773863e-03
Drift      -name "BC2_DR_QD_ED"     -length  4.250000e-02 -e0  6.455071e-01
Drift      -name "BC2_DR_V2"        -length  5.872423e+00 -e0  6.455071e-01
Drift      -name "BC2_DR_QD_ED"     -length  4.250000e-02 -e0  6.455071e-01
Quadrupole -name "BC2_QD_V02"       -length  4.000000e-02 -e0  6.455071e-01 -strength -3.000722e-01
Dipole     -name "BC2_COR"          -length  0.000000e+00 -e0  6.455071e-01
Bpm        -name "BC2_BPM"          -length  0.000000e+00 -e0  6.455071e-01
Quadrupole -name "BC2_QD_V02"       -length  4.000000e-02 -e0  6.455071e-01 -strength -3.000722e-01
Drift      -name "BC2_DR_QD_ED"     -length  4.250000e-02 -e0  6.455071e-01
Drift      -name "BC2_DR_V3"        -length  2.000000e-01 -e0  6.455071e-01
Drift      -name "BC2_DR_QD_ED"     -length  4.250000e-02 -e0  6.455071e-01
Quadrupole -name "BC2_QD_V03"       -length  4.000000e-02 -e0  6.455071e-01 -strength  2.781983e-01
Dipole     -name "BC2_COR"          -length  0.000000e+00 -e0  6.455071e-01
Bpm        -name "BC2_BPM"          -length  0.000000e+00 -e0  6.455071e-01
Quadrupole -name "BC2_QD_V03"       -length  4.000000e-02 -e0  6.455071e-01 -strength  2.781983e-01
Drift      -name "BC2_DR_QD_ED"     -length  4.250000e-02 -e0  6.455071e-01
Drift      -name "BC2_DR_V4"        -length  2.000000e-01 -e0  6.455071e-01
Drift      -name "BC2_DR_20"        -length  5.000000e-01 -e0  6.455071e-01
Sbend      -name "BC2_DP_DIP1"      -length  6.000476e-01 -e0  6.455023e-01 -angle -2.181662e-02 -E1  0.000000e+00 -E2 -2.181662e-02 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  0.000000e+00  -six_dim 1 -csr 1 
Drift      -name "BC2_DR_SIDE_C"    -length  5.000000e-01 -e0  6.454893e-01
Drift      -name "BC2_DR_SIDE"      -length  3.200881e+00 -e0  6.454893e-01
Sbend      -name "BC2_DP_DIP2"      -length  6.000476e-01 -e0  6.454773e-01 -angle  2.181662e-02 -E1  2.181662e-02 -E2  0.000000e+00 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  0.000000e+00  -six_dim 1 -csr 1 
Drift      -name "BC2_DR_CENT_C"    -length  2.500000e-01 -e0  6.454621e-01
Bpm        -name "BC2_BPM"          -length  0.000000e+00 -e0  6.454621e-01
Drift      -name "BC2_DR_CENT"      -length  2.500000e-01 -e0  6.454621e-01
Sbend      -name "BC2_DP_DIP3"      -length  6.000476e-01 -e0  6.454035e-01 -angle  2.181662e-02 -E1  0.000000e+00 -E2  2.181662e-02 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  0.000000e+00  -six_dim 1 -csr 1 
Drift      -name "BC2_DR_SIDE_C"    -length  5.000000e-01 -e0  6.453216e-01
Drift      -name "BC2_DR_SIDE"      -length  3.200881e+00 -e0  6.453216e-01
Sbend      -name "BC2_DP_DIP4"      -length  6.000476e-01 -e0  6.452295e-01 -angle -2.181662e-02 -E1 -2.181662e-02 -E2  0.000000e+00 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  0.000000e+00  -six_dim 1 -csr 1 
Drift      -name "BC2_IN_CNT"       -length  0.000000e+00 -e0  6.452295e-01
Drift      -name "BC2_DR_20_C"      -length  5.000000e-01 -e0  6.451551e-01
Drift      -name "BC2_IN_CNT"       -length  0.000000e+00 -e0  6.451551e-01
Drift      -name "BC2_WA_OU2"       -length  0.000000e+00 -e0  6.451551e-01
Drift      -name "LN2_DR_CT"        -length  1.000000e-01 -e0  6.451551e-01
Drift      -name "LN2_DR_V1"        -length  2.294830e+00 -e0  6.451551e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  6.451551e-01
Quadrupole -name "LN2_QD_V01"       -length  4.000000e-02 -e0  6.451551e-01 -strength  3.443086e-01
Dipole     -name "LN2_COR"          -length  0.000000e+00 -e0  6.451551e-01
Bpm        -name "LN2_BPM"          -length  0.000000e+00 -e0  6.451551e-01
Quadrupole -name "LN2_QD_V01"       -length  4.000000e-02 -e0  6.451551e-01 -strength  3.443086e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  6.451551e-01
Drift      -name "LN2_DR_V2"        -length  5.090891e-01 -e0  6.451551e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  6.451551e-01
Quadrupole -name "LN2_QD_V02"       -length  4.000000e-02 -e0  6.451551e-01 -strength -3.340840e-01
Dipole     -name "LN2_COR"          -length  0.000000e+00 -e0  6.451551e-01
Bpm        -name "LN2_BPM"          -length  0.000000e+00 -e0  6.451551e-01
Quadrupole -name "LN2_QD_V02"       -length  4.000000e-02 -e0  6.451551e-01 -strength -3.340840e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  6.451551e-01
Drift      -name "LN2_DR_V3"        -length  1.490464e+00 -e0  6.451551e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  6.451551e-01
Quadrupole -name "LN2_QD_V03"       -length  4.000000e-02 -e0  6.451551e-01 -strength  2.273931e-01
Dipole     -name "LN2_COR"          -length  0.000000e+00 -e0  6.451551e-01
Bpm        -name "LN2_BPM"          -length  0.000000e+00 -e0  6.451551e-01
Quadrupole -name "LN2_QD_V03"       -length  4.000000e-02 -e0  6.451551e-01 -strength  2.273931e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  6.451551e-01
Drift      -name "LN2_DR_V4"        -length  2.999939e+00 -e0  6.451551e-01
Drift      -name "LN2_DR_VS"        -length  1.000000e-01 -e0  6.451551e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  6.451551e-01
Quadrupole -name "LN2_QD_FH"        -length  4.000000e-02 -e0  6.451551e-01 -strength  1.729016e-01
Dipole     -name "LN2_COR"          -length  0.000000e+00 -e0  6.451551e-01
Bpm        -name "LN2_BPM"          -length  0.000000e+00 -e0  6.451551e-01
Quadrupole -name "LN2_QD_FH"        -length  4.000000e-02 -e0  6.451551e-01 -strength  1.729016e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  6.451551e-01
Drift      -name "LN2_DR_SV"        -length  1.000000e-01 -e0  6.451551e-01
Drift      -name "LN2_DR_BL"        -length  5.000000e-02 -e0  6.451551e-01
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  6.451551e-01
Cavity     -name "LN2_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   6.693398e-01
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  6.693398e-01
Drift      -name "LN2_DR_BL"        -length  5.000000e-02 -e0  6.693398e-01
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  6.693398e-01
Cavity     -name "LN2_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   6.935246e-01
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  6.935246e-01
Drift      -name "LN2_DR_BL"        -length  5.000000e-02 -e0  6.935246e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  6.935246e-01
Quadrupole -name "LN2_QD_DH"        -length  4.000000e-02 -e0  6.935246e-01 -strength -1.858646e-01
Dipole     -name "LN2_COR"          -length  0.000000e+00 -e0  6.935246e-01
Bpm        -name "LN2_BPM"          -length  0.000000e+00 -e0  6.935246e-01
Quadrupole -name "LN2_QD_DH"        -length  4.000000e-02 -e0  6.935246e-01 -strength -1.858646e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  6.935246e-01
Drift      -name "LN2_DR_VS"        -length  1.000000e-01 -e0  6.935246e-01
Drift      -name "LN2_DR_BL"        -length  5.000000e-02 -e0  6.935246e-01
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  6.935246e-01
Cavity     -name "LN2_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   7.177093e-01
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  7.177093e-01
Drift      -name "LN2_DR_BL"        -length  5.000000e-02 -e0  7.177093e-01
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  7.177093e-01
Cavity     -name "LN2_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   7.418941e-01
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  7.418941e-01
Drift      -name "LN2_DR_BL"        -length  5.000000e-02 -e0  7.418941e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  7.418941e-01
Quadrupole -name "LN2_QD_FH"        -length  4.000000e-02 -e0  7.418941e-01 -strength  1.988276e-01
Dipole     -name "LN2_COR"          -length  0.000000e+00 -e0  7.418941e-01
Bpm        -name "LN2_BPM"          -length  0.000000e+00 -e0  7.418941e-01
Quadrupole -name "LN2_QD_FH"        -length  4.000000e-02 -e0  7.418941e-01 -strength  1.988276e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  7.418941e-01
Drift      -name "LN2_DR_SV"        -length  1.000000e-01 -e0  7.418941e-01
Drift      -name "LN2_DR_BL"        -length  5.000000e-02 -e0  7.418941e-01
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  7.418941e-01
Cavity     -name "LN2_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   7.660788e-01
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  7.660788e-01
Drift      -name "LN2_DR_BL"        -length  5.000000e-02 -e0  7.660788e-01
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  7.660788e-01
Cavity     -name "LN2_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   7.902636e-01
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  7.902636e-01
Drift      -name "LN2_DR_BL"        -length  5.000000e-02 -e0  7.902636e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  7.902636e-01
Quadrupole -name "LN2_QD_DH"        -length  4.000000e-02 -e0  7.902636e-01 -strength -2.117906e-01
Dipole     -name "LN2_COR"          -length  0.000000e+00 -e0  7.902636e-01
Bpm        -name "LN2_BPM"          -length  0.000000e+00 -e0  7.902636e-01
Quadrupole -name "LN2_QD_DH"        -length  4.000000e-02 -e0  7.902636e-01 -strength -2.117906e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  7.902636e-01
Drift      -name "LN2_DR_VS"        -length  1.000000e-01 -e0  7.902636e-01
Drift      -name "LN2_DR_BL"        -length  5.000000e-02 -e0  7.902636e-01
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  7.902636e-01
Cavity     -name "LN2_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   8.144483e-01
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  8.144483e-01
Drift      -name "LN2_DR_BL"        -length  5.000000e-02 -e0  8.144483e-01
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  8.144483e-01
Cavity     -name "LN2_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   8.386331e-01
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  8.386331e-01
Drift      -name "LN2_DR_BL"        -length  5.000000e-02 -e0  8.386331e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  8.386331e-01
Quadrupole -name "LN2_QD_FH"        -length  4.000000e-02 -e0  8.386331e-01 -strength  2.247537e-01
Dipole     -name "LN2_COR"          -length  0.000000e+00 -e0  8.386331e-01
Bpm        -name "LN2_BPM"          -length  0.000000e+00 -e0  8.386331e-01
Quadrupole -name "LN2_QD_FH"        -length  4.000000e-02 -e0  8.386331e-01 -strength  2.247537e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  8.386331e-01
Drift      -name "LN2_WA_OU2"       -length  0.000000e+00 -e0  8.386331e-01
Drift      -name "LN2_DR_DI_V1"     -length  1.161052e+00 -e0  8.386331e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  8.386331e-01
Quadrupole -name "LN2_QD_DI_V01"    -length  4.000000e-02 -e0  8.386331e-01 -strength -8.262686e-02
Dipole     -name "LN2_COR_DI"       -length  0.000000e+00 -e0  8.386331e-01
Bpm        -name "LN2_BPM_DI"       -length  0.000000e+00 -e0  8.386331e-01
Quadrupole -name "LN2_QD_DI_V01"    -length  4.000000e-02 -e0  8.386331e-01 -strength -8.262686e-02
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  8.386331e-01
Drift      -name "LN2_DR_DI_V2"     -length  8.279980e-01 -e0  8.386331e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  8.386331e-01
Quadrupole -name "LN2_QD_DI_V02"    -length  4.000000e-02 -e0  8.386331e-01 -strength  3.396381e-01
Dipole     -name "LN2_COR_DI"       -length  0.000000e+00 -e0  8.386331e-01
Bpm        -name "LN2_BPM_DI"       -length  0.000000e+00 -e0  8.386331e-01
Quadrupole -name "LN2_QD_DI_V02"    -length  4.000000e-02 -e0  8.386331e-01 -strength  3.396381e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  8.386331e-01
Drift      -name "LN2_DR_DI_V3"     -length  2.001539e-01 -e0  8.386331e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  8.386331e-01
Quadrupole -name "LN2_QD_DI_V03"    -length  4.000000e-02 -e0  8.386331e-01 -strength -4.184631e-01
Dipole     -name "LN2_COR_DI"       -length  0.000000e+00 -e0  8.386331e-01
Bpm        -name "LN2_BPM_DI"       -length  0.000000e+00 -e0  8.386331e-01
Quadrupole -name "LN2_QD_DI_V03"    -length  4.000000e-02 -e0  8.386331e-01 -strength -4.184631e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  8.386331e-01
Drift      -name "LN2_DR_DI_V5"     -length  2.000000e-01 -e0  8.386331e-01
Drift      -name "LN2_DR_VS"        -length  1.000000e-01 -e0  8.386331e-01
Drift      -name "LN2_DR_DI"        -length  2.500000e-01 -e0  8.386331e-01
Drift      -name "LN2_DR_VS_DI"     -length  1.000000e-01 -e0  8.386331e-01
Drift      -name "LN2_DR_BL_DI"     -length  5.000000e-02 -e0  8.386331e-01
Drift      -name "LN2_DR_XCA_ED_DI" -length  1.001043e-01 -e0  8.386331e-01
CrabCavity -name "LN2_XCA0_DI"      -length  9.997914e-01 -voltage  0.000000e+00 -phase  9.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   8.386331e-01
Drift      -name "LN2_DR_XCA_ED_DI" -length  1.001043e-01 -e0  8.385566e-01
Drift      -name "LN2_DR_BL_DI"     -length  5.000000e-02 -e0  8.385566e-01
Drift      -name "LN2_DR_DI"        -length  2.500000e-01 -e0  8.385566e-01
Drift      -name "LN2_DR_DI"        -length  2.500000e-01 -e0  8.385566e-01
Drift      -name "LN2_DR_DI_QD_ED"  -length  4.250000e-02 -e0  8.385566e-01
Quadrupole -name "LN2_QD_DI_FH"     -length  4.000000e-02 -e0  8.385566e-01 -strength  2.683381e-01
Dipole     -name "LN2_COR_DI"       -length  0.000000e+00 -e0  8.385566e-01
Bpm        -name "LN2_BPM_DI"       -length  0.000000e+00 -e0  8.385566e-01
Quadrupole -name "LN2_QD_DI_FH"     -length  4.000000e-02 -e0  8.385566e-01 -strength  2.683381e-01
Drift      -name "LN2_DR_DI_QD_ED"  -length  4.250000e-02 -e0  8.385566e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.385566e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.385566e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.385566e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.385566e-01
Drift      -name "LN2_DR_DI_VS"     -length  1.000000e-01 -e0  8.385566e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.385566e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.385566e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.385566e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.385566e-01
Drift      -name "LN2_DR_DI_QD_ED"  -length  4.250000e-02 -e0  8.385566e-01
Quadrupole -name "LN2_QD_DI_DH"     -length  4.000000e-02 -e0  8.385566e-01 -strength -2.683381e-01
Dipole     -name "LN2_COR_DI"       -length  0.000000e+00 -e0  8.385566e-01
Bpm        -name "LN2_BPM_DI"       -length  0.000000e+00 -e0  8.385566e-01
Quadrupole -name "LN2_QD_DI_DH"     -length  4.000000e-02 -e0  8.385566e-01 -strength -2.683381e-01
Drift      -name "LN2_DR_DI_QD_ED"  -length  4.250000e-02 -e0  8.385566e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.385566e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.385566e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.385566e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.385566e-01
Drift      -name "LN2_DR_DI_VS"     -length  1.000000e-01 -e0  8.385566e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.385566e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.385566e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.385566e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.385566e-01
Drift      -name "LN2_DR_DI_QD_ED"  -length  4.250000e-02 -e0  8.385566e-01
Quadrupole -name "LN2_QD_DI_FH"     -length  4.000000e-02 -e0  8.385566e-01 -strength  2.683381e-01
Dipole     -name "LN2_COR_DI"       -length  0.000000e+00 -e0  8.385566e-01
Bpm        -name "LN2_BPM_DI"       -length  0.000000e+00 -e0  8.385566e-01
Quadrupole -name "LN2_QD_DI_FH"     -length  4.000000e-02 -e0  8.385566e-01 -strength  2.683381e-01
Drift      -name "LN2_DR_DI_QD_ED"  -length  4.250000e-02 -e0  8.385566e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.385566e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.385566e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.385566e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.385566e-01
Drift      -name "LN2_DR_DI_VS"     -length  1.000000e-01 -e0  8.385566e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.385566e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.385566e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.385566e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.385566e-01
Drift      -name "LN2_DR_DI_QD_ED"  -length  4.250000e-02 -e0  8.385566e-01
Quadrupole -name "LN2_QD_DI_DH"     -length  4.000000e-02 -e0  8.385566e-01 -strength -2.683381e-01
Dipole     -name "LN2_COR_DI"       -length  0.000000e+00 -e0  8.385566e-01
Bpm        -name "LN2_BPM_DI"       -length  0.000000e+00 -e0  8.385566e-01
Quadrupole -name "LN2_QD_DI_DH"     -length  4.000000e-02 -e0  8.385566e-01 -strength -2.683381e-01
Drift      -name "LN2_DR_DI_QD_ED"  -length  4.250000e-02 -e0  8.385566e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.385566e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.385566e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.385566e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.385566e-01
Drift      -name "LN2_DR_DI_VS"     -length  1.000000e-01 -e0  8.385566e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.385566e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.385566e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.385566e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.385566e-01
Drift      -name "LN2_DR_DI_QD_ED"  -length  4.250000e-02 -e0  8.385566e-01
Quadrupole -name "LN2_QD_DI_FH"     -length  4.000000e-02 -e0  8.385566e-01 -strength  2.683381e-01
Dipole     -name "LN2_COR_DI"       -length  0.000000e+00 -e0  8.385566e-01
Bpm        -name "LN2_BPM_DI"       -length  0.000000e+00 -e0  8.385566e-01
Quadrupole -name "LN2_QD_DI_FH"     -length  4.000000e-02 -e0  8.385566e-01 -strength  2.683381e-01
Drift      -name "LN2_DR_DI_QD_ED"  -length  4.250000e-02 -e0  8.385566e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.385566e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.385566e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.385566e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.385566e-01
Drift      -name "LN2_DR_DI_VS"     -length  1.000000e-01 -e0  8.385566e-01
Sbend      -name "LN2_DP_DI"        -length  1.500000e-01 -e0  8.385566e-01 -angle  0.000000e+00 -E1  0.000000e+00 -E2  0.000000e+00 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  0.000000e+00 -six_dim 1
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.385566e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.385566e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.385566e-01
Drift      -name "LN2_DR_DI_QD_ED"  -length  4.250000e-02 -e0  8.385566e-01
Quadrupole -name "LN2_QD_DI_DH"     -length  4.000000e-02 -e0  8.385566e-01 -strength -2.683381e-01
Dipole     -name "LN2_COR_DI"       -length  0.000000e+00 -e0  8.385566e-01
Bpm        -name "LN2_BPM_DI"       -length  0.000000e+00 -e0  8.385566e-01
Quadrupole -name "LN2_QD_DI_DH"     -length  4.000000e-02 -e0  8.385566e-01 -strength -2.683381e-01
Drift      -name "LN2_DR_DI_QD_ED"  -length  4.250000e-02 -e0  8.385566e-01
Drift      -name "LN2_DR_DI"        -length  2.500000e-01 -e0  8.385566e-01
Drift      -name "LN2_WA_OU_DI2"    -length  0.000000e+00 -e0  8.385566e-01
Drift      -name "LN2_DR_BP_V1"     -length  3.714219e+00 -e0  8.385566e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  8.385566e-01
Quadrupole -name "LN2_QD_BP_V01"    -length  4.000000e-02 -e0  8.385566e-01 -strength -3.872543e-02
Dipole     -name "LN2_COR"          -length  0.000000e+00 -e0  8.385566e-01
Bpm        -name "LN2_BPM"          -length  0.000000e+00 -e0  8.385566e-01
Quadrupole -name "LN2_QD_BP_V01"    -length  4.000000e-02 -e0  8.385566e-01 -strength -3.872543e-02
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  8.385566e-01
Drift      -name "LN2_DR_BP_V2"     -length  3.528767e+00 -e0  8.385566e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  8.385566e-01
Quadrupole -name "LN2_QD_BP_V02"    -length  4.000000e-02 -e0  8.385566e-01 -strength  4.631412e-01
Dipole     -name "LN2_COR"          -length  0.000000e+00 -e0  8.385566e-01
Bpm        -name "LN2_BPM"          -length  0.000000e+00 -e0  8.385566e-01
Quadrupole -name "LN2_QD_BP_V02"    -length  4.000000e-02 -e0  8.385566e-01 -strength  4.631412e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  8.385566e-01
Drift      -name "LN2_DR_BP_V3"     -length  2.000269e-01 -e0  8.385566e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  8.385566e-01
Quadrupole -name "LN2_QD_BP_V03"    -length  4.000000e-02 -e0  8.385566e-01 -strength -4.518976e-01
Dipole     -name "LN2_COR"          -length  0.000000e+00 -e0  8.385566e-01
Bpm        -name "LN2_BPM"          -length  0.000000e+00 -e0  8.385566e-01
Quadrupole -name "LN2_QD_BP_V03"    -length  4.000000e-02 -e0  8.385566e-01 -strength -4.518976e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  8.385566e-01
Drift      -name "LN2_DR_BP_V4"     -length  2.645902e-01 -e0  8.385566e-01
Drift      -name "LN2_DR_BP_0"      -length  2.000000e-01 -e0  8.385566e-01
Drift      -name "LN2_DR_BP_BL"     -length  5.000000e-02 -e0  8.385566e-01
Drift      -name "LN2_DR_SCA_ED"    -length  8.325105e-02 -e0  8.385566e-01
CrabCavity -name "LN2_SCA0"         -length  5.334979e-01 -voltage  0.000000e+00 -phase  9.000000e+01 -frequency  2.997000e+00
SetReferenceEnergy   8.385566e-01
Drift      -name "LN2_DR_SCA_ED"    -length  8.325105e-02 -e0  8.385518e-01
Drift      -name "LN2_DR_BP_BL"     -length  5.000000e-02 -e0  8.385518e-01
Drift      -name "LN2_DR_BP_0"      -length  2.000000e-01 -e0  8.385518e-01
Drift      -name "LN2_DR_BP"        -length  4.000000e-01 -e0  8.385518e-01
Drift      -name "LN2_DR_BP"        -length  4.000000e-01 -e0  8.385518e-01
Drift      -name "LN2_DR_BP_VS"     -length  1.000000e-01 -e0  8.385518e-01
Drift      -name "LN2_DR_BP_DR"     -length  1.000000e-01 -e0  8.385518e-01
Drift      -name "LN2_DP_SEPM"      -length  2.500000e-01 -e0  8.385518e-01
Drift      -name "LN2_DR_BP"        -length  4.000000e-01 -e0  8.385518e-01
Drift      -name "LN2_DR_BP"        -length  4.000000e-01 -e0  8.385518e-01
Drift      -name "LN2_DR_BP"        -length  4.000000e-01 -e0  8.385518e-01
Drift      -name "LN2_DR_BP"        -length  4.000000e-01 -e0  8.385518e-01
Drift      -name "LN2_DR_BP"        -length  4.000000e-01 -e0  8.385518e-01
Drift      -name "LN2_DR_BP_VS"     -length  1.000000e-01 -e0  8.385518e-01
Drift      -name "LN2_DR_BP"        -length  4.000000e-01 -e0  8.385518e-01
Drift      -name "LN2_BP_WA_OU2_"   -length  0.000000e+00 -e0  8.385518e-01
Drift      -name "LN3_DR_V1"        -length  5.000000e-02 -e0  8.385518e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  8.385518e-01
Quadrupole -name "LN3_QD_V01"       -length  4.000000e-02 -e0  8.385518e-01 -strength  2.343165e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  8.385518e-01
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  8.385518e-01
Quadrupole -name "LN3_QD_V01"       -length  4.000000e-02 -e0  8.385518e-01 -strength  2.343165e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  8.385518e-01
Drift      -name "LN3_DR_V2"        -length  5.342817e-01 -e0  8.385518e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  8.385518e-01
Quadrupole -name "LN3_QD_V02"       -length  4.000000e-02 -e0  8.385518e-01 -strength  3.214907e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  8.385518e-01
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  8.385518e-01
Quadrupole -name "LN3_QD_V02"       -length  4.000000e-02 -e0  8.385518e-01 -strength  3.214907e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  8.385518e-01
Drift      -name "LN3_DR_V3"        -length  9.374694e-02 -e0  8.385518e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  8.385518e-01
Quadrupole -name "LN3_QD_V03"       -length  4.000000e-02 -e0  8.385518e-01 -strength -5.062205e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  8.385518e-01
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  8.385518e-01
Quadrupole -name "LN3_QD_V03"       -length  4.000000e-02 -e0  8.385518e-01 -strength -5.062205e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  8.385518e-01
Drift      -name "LN3_DR_V4"        -length  1.580023e+00 -e0  8.385518e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  8.385518e-01
Quadrupole -name "LN3_QD_V04"       -length  4.000000e-02 -e0  8.385518e-01 -strength -1.110308e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  8.385518e-01
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  8.385518e-01
Quadrupole -name "LN3_QD_V04"       -length  4.000000e-02 -e0  8.385518e-01 -strength -1.110308e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  8.385518e-01
Drift      -name "LN3_DR_V5"        -length  1.237385e+00 -e0  8.385518e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  8.385518e-01
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  8.385518e-01 -strength  1.408767e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  8.385518e-01
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  8.385518e-01
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  8.385518e-01 -strength  1.408767e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  8.385518e-01
Drift      -name "LN3_DR_SV"        -length  1.000000e-01 -e0  8.385518e-01
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  8.385518e-01
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  8.385518e-01
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   8.627365e-01
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  8.627365e-01
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  8.627365e-01
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  8.627365e-01
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   8.869212e-01
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  8.869212e-01
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  8.869212e-01
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  8.869212e-01
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   9.111059e-01
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  9.111059e-01
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  9.111059e-01
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  9.111059e-01
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   9.352906e-01
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  9.352906e-01
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  9.352906e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  9.352906e-01
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  9.352906e-01 -strength -1.571288e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  9.352906e-01
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  9.352906e-01
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  9.352906e-01 -strength -1.571288e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  9.352906e-01
Drift      -name "LN3_DR_VS"        -length  1.000000e-01 -e0  9.352906e-01
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  9.352906e-01
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  9.352906e-01
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   9.594753e-01
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  9.594753e-01
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  9.594753e-01
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  9.594753e-01
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   9.836600e-01
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  9.836600e-01
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  9.836600e-01
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  9.836600e-01
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.007845e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.007845e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.007845e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.007845e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.032029e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.032029e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.032029e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.032029e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  1.032029e+00 -strength  1.733809e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  1.032029e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  1.032029e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  1.032029e+00 -strength  1.733809e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.032029e+00
Drift      -name "LN3_DR_SV"        -length  1.000000e-01 -e0  1.032029e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.032029e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.032029e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.056214e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.056214e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.056214e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.056214e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.080399e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.080399e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.080399e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.080399e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.104584e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.104584e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.104584e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.104584e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.128768e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.128768e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.128768e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.128768e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  1.128768e+00 -strength -1.896331e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  1.128768e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  1.128768e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  1.128768e+00 -strength -1.896331e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.128768e+00
Drift      -name "LN3_DR_VS"        -length  1.000000e-01 -e0  1.128768e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.128768e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.128768e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.152953e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.152953e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.152953e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.152953e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.177138e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.177138e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.177138e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.177138e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.201322e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.201322e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.201322e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.201322e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.225507e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.225507e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.225507e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.225507e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  1.225507e+00 -strength  2.058852e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  1.225507e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  1.225507e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  1.225507e+00 -strength  2.058852e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.225507e+00
Drift      -name "LN3_DR_SV"        -length  1.000000e-01 -e0  1.225507e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.225507e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.225507e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.249692e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.249692e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.249692e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.249692e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.273876e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.273876e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.273876e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.273876e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.298061e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.298061e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.298061e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.298061e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.322246e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.322246e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.322246e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.322246e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  1.322246e+00 -strength -2.221373e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  1.322246e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  1.322246e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  1.322246e+00 -strength -2.221373e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.322246e+00
Drift      -name "LN3_DR_VS"        -length  1.000000e-01 -e0  1.322246e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.322246e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.322246e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.346430e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.346430e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.346430e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.346430e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.370615e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.370615e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.370615e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.370615e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.394800e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.394800e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.394800e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.394800e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.418985e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.418985e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.418985e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.418985e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  1.418985e+00 -strength  2.383894e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  1.418985e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  1.418985e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  1.418985e+00 -strength  2.383894e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.418985e+00
Drift      -name "LN3_DR_SV"        -length  1.000000e-01 -e0  1.418985e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.418985e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.418985e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.443169e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.443169e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.443169e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.443169e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.467354e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.467354e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.467354e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.467354e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.491539e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.491539e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.491539e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.491539e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.515723e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.515723e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.515723e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.515723e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  1.515723e+00 -strength -2.546415e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  1.515723e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  1.515723e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  1.515723e+00 -strength -2.546415e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.515723e+00
Drift      -name "LN3_DR_VS"        -length  1.000000e-01 -e0  1.515723e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.515723e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.515723e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.539908e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.539908e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.539908e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.539908e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.564093e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.564093e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.564093e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.564093e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.588277e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.588277e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.588277e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.588277e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.612462e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.612462e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.612462e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.612462e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  1.612462e+00 -strength  2.708936e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  1.612462e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  1.612462e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  1.612462e+00 -strength  2.708936e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.612462e+00
Drift      -name "LN3_DR_SV"        -length  1.000000e-01 -e0  1.612462e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.612462e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.612462e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.636647e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.636647e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.636647e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.636647e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.660831e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.660831e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.660831e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.660831e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.685016e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.685016e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.685016e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.685016e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.709201e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.709201e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.709201e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.709201e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  1.709201e+00 -strength -2.871457e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  1.709201e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  1.709201e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  1.709201e+00 -strength -2.871457e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.709201e+00
Drift      -name "LN3_DR_VS"        -length  1.000000e-01 -e0  1.709201e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.709201e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.709201e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.733386e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.733386e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.733386e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.733386e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.757570e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.757570e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.757570e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.757570e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.781755e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.781755e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.781755e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.781755e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.805940e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.805940e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.805940e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.805940e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  1.805940e+00 -strength  3.033979e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  1.805940e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  1.805940e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  1.805940e+00 -strength  3.033979e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.805940e+00
Drift      -name "LN3_DR_SV"        -length  1.000000e-01 -e0  1.805940e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.805940e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.805940e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.830124e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.830124e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.830124e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.830124e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.854309e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.854309e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.854309e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.854309e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.878494e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.878494e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.878494e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.878494e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.902678e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.902678e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.902678e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.902678e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  1.902678e+00 -strength -3.196500e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  1.902678e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  1.902678e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  1.902678e+00 -strength -3.196500e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.902678e+00
Drift      -name "LN3_DR_VS"        -length  1.000000e-01 -e0  1.902678e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.902678e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.902678e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.926863e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.926863e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.926863e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.926863e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.951048e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.951048e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.951048e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.951048e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.975232e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.975232e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.975232e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.975232e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.999417e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.999417e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.999417e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.999417e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  1.999417e+00 -strength  3.359021e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  1.999417e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  1.999417e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  1.999417e+00 -strength  3.359021e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.999417e+00
Drift      -name "LN3_DR_SV"        -length  1.000000e-01 -e0  1.999417e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.999417e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.999417e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.023602e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.023602e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.023602e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.023602e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.047787e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.047787e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.047787e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.047787e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.071971e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.071971e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.071971e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.071971e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.096156e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.096156e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.096156e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  2.096156e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  2.096156e+00 -strength -3.521542e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  2.096156e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  2.096156e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  2.096156e+00 -strength -3.521542e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  2.096156e+00
Drift      -name "LN3_DR_VS"        -length  1.000000e-01 -e0  2.096156e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.096156e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.096156e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.120341e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.120341e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.120341e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.120341e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.144525e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.144525e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.144525e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.144525e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.168710e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.168710e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.168710e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.168710e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.192895e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.192895e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.192895e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  2.192895e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  2.192895e+00 -strength  3.684063e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  2.192895e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  2.192895e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  2.192895e+00 -strength  3.684063e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  2.192895e+00
Drift      -name "LN3_DR_SV"        -length  1.000000e-01 -e0  2.192895e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.192895e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.192895e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.217079e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.217079e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.217079e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.217079e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.241264e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.241264e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.241264e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.241264e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.265449e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.265449e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.265449e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.265449e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.289633e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.289633e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.289633e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  2.289633e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  2.289633e+00 -strength -3.846584e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  2.289633e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  2.289633e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  2.289633e+00 -strength -3.846584e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  2.289633e+00
Drift      -name "LN3_DR_VS"        -length  1.000000e-01 -e0  2.289633e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.289633e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.289633e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.313818e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.313818e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.313818e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.313818e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.338003e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.338003e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.338003e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.338003e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.362187e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.362187e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.362187e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.362187e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.386372e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.386372e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.386372e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  2.386372e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  2.386372e+00 -strength  4.009105e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  2.386372e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  2.386372e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  2.386372e+00 -strength  4.009105e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  2.386372e+00
Drift      -name "LN3_DR_SV"        -length  1.000000e-01 -e0  2.386372e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.386372e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.386372e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.410557e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.410557e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.410557e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.410557e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.434742e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.434742e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.434742e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.434742e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.458926e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.458926e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.458926e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.458926e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.483111e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.483111e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.483111e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  2.483111e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  2.483111e+00 -strength -4.171626e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  2.483111e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  2.483111e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  2.483111e+00 -strength -4.171626e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  2.483111e+00
Drift      -name "LN3_DR_DR"        -length  1.000000e-01 -e0  2.483111e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.483111e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.483111e+00
Drift      -name "LN3_DR_XCA_"      -length  9.164755e-01 -e0  2.483111e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.483111e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.483111e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.483111e+00
Drift      -name "LN3_DR_XCA_"      -length  9.164755e-01 -e0  2.483111e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.483111e+00
Drift      -name "LN3_DR_CT"        -length  1.000000e-01 -e0  2.483111e+00
Drift      -name "LN3_WA_OU2"       -length  0.000000e+00 -e0  2.483111e+00
Drift      -name "LN3_DR_BP_V1"     -length  1.988701e+00 -e0  2.483111e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  2.483111e+00
Quadrupole -name "LN3_QD_BP_V01"    -length  4.000000e-02 -e0  2.483111e+00 -strength -7.980985e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  2.483111e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  2.483111e+00
Quadrupole -name "LN3_QD_BP_V01"    -length  4.000000e-02 -e0  2.483111e+00 -strength -7.980985e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  2.483111e+00
Drift      -name "LN3_DR_BP_V2"     -length  2.427668e+00 -e0  2.483111e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  2.483111e+00
Quadrupole -name "LN3_QD_BP_V02"    -length  4.000000e-02 -e0  2.483111e+00 -strength  8.575897e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  2.483111e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  2.483111e+00
Quadrupole -name "LN3_QD_BP_V02"    -length  4.000000e-02 -e0  2.483111e+00 -strength  8.575897e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  2.483111e+00
Drift      -name "LN3_DR_BP_V3"     -length  1.213336e+00 -e0  2.483111e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  2.483111e+00
Quadrupole -name "LN3_QD_BP_V03"    -length  4.000000e-02 -e0  2.483111e+00 -strength -8.150376e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  2.483111e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  2.483111e+00
Quadrupole -name "LN3_QD_BP_V03"    -length  4.000000e-02 -e0  2.483111e+00 -strength -8.150376e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  2.483111e+00
Drift      -name "LN3_DR_BP_V4"     -length  2.000000e-01 -e0  2.483111e+00
Drift      -name "LN3_DR_BP_0"      -length  2.000000e-01 -e0  2.483111e+00
Drift      -name "LN3_DR_BP_BL"     -length  5.000000e-02 -e0  2.483111e+00
Drift      -name "LN3_DR_SCA_ED"    -length  8.325105e-02 -e0  2.483111e+00
CrabCavity -name "LN3_SCA0"         -length  5.334979e-01 -voltage  0.000000e+00 -phase  9.000000e+01 -frequency  2.997000e+00
SetReferenceEnergy   2.483111e+00
Drift      -name "LN3_DR_SCA_ED"    -length  8.325105e-02 -e0  2.483106e+00
Drift      -name "LN3_DR_BP_BL"     -length  5.000000e-02 -e0  2.483106e+00
Drift      -name "LN3_DR_BP_0"      -length  2.000000e-01 -e0  2.483106e+00
Drift      -name "LN3_DR_BP"        -length  4.000000e-01 -e0  2.483106e+00
Drift      -name "LN3_DR_BP_VS"     -length  1.000000e-01 -e0  2.483106e+00
Drift      -name "LN3_DR_BP"        -length  4.000000e-01 -e0  2.483106e+00
Drift      -name "LN3_DP_SEPM"      -length  4.500000e-01 -e0  2.483106e+00
Drift      -name "LN3_DR_BP"        -length  4.000000e-01 -e0  2.483106e+00
Drift      -name "LN3_DR_BP"        -length  4.000000e-01 -e0  2.483106e+00
Drift      -name "LN3_DR_BP"        -length  4.000000e-01 -e0  2.483106e+00
Drift      -name "LN3_DR_BP"        -length  4.000000e-01 -e0  2.483106e+00
Drift      -name "LN3_DR_BP"        -length  4.000000e-01 -e0  2.483106e+00
Drift      -name "LN3_DR_BP_VS"     -length  1.000000e-01 -e0  2.483106e+00
Drift      -name "LN3_DR_BP"        -length  4.000000e-01 -e0  2.483106e+00
Drift      -name "LN3_BP_WA_OU2_"   -length  0.000000e+00 -e0  2.483106e+00
Drift      -name "TMC_DR_V1"        -length  2.004117e-01 -e0  2.483106e+00
Drift      -name "TMC_DR_QD_ED"     -length  4.250000e-02 -e0  2.483106e+00
Quadrupole -name "TMC_QD_V01"       -length  4.000000e-02 -e0  2.483106e+00 -strength  5.096705e-01
Dipole     -name "TMC_COR"          -length  0.000000e+00 -e0  2.483106e+00
Bpm        -name "TMC_BPM"          -length  0.000000e+00 -e0  2.483106e+00
Quadrupole -name "TMC_QD_V01"       -length  4.000000e-02 -e0  2.483106e+00 -strength  5.096705e-01
Drift      -name "TMC_DR_QD_ED"     -length  4.250000e-02 -e0  2.483106e+00
Drift      -name "TMC_DR_V2"        -length  2.866492e+00 -e0  2.483106e+00
Drift      -name "TMC_DR_QD_ED"     -length  4.250000e-02 -e0  2.483106e+00
Quadrupole -name "TMC_QD_V02"       -length  4.000000e-02 -e0  2.483106e+00 -strength -9.884738e-01
Dipole     -name "TMC_COR"          -length  0.000000e+00 -e0  2.483106e+00
Bpm        -name "TMC_BPM"          -length  0.000000e+00 -e0  2.483106e+00
Quadrupole -name "TMC_QD_V02"       -length  4.000000e-02 -e0  2.483106e+00 -strength -9.884738e-01
Drift      -name "TMC_DR_QD_ED"     -length  4.250000e-02 -e0  2.483106e+00
Drift      -name "TMC_DR_V3"        -length  3.885389e-01 -e0  2.483106e+00
Drift      -name "TMC_DR_QD_ED"     -length  4.250000e-02 -e0  2.483106e+00
Quadrupole -name "TMC_QD_V03"       -length  4.000000e-02 -e0  2.483106e+00 -strength  7.839836e-01
Dipole     -name "TMC_COR"          -length  0.000000e+00 -e0  2.483106e+00
Bpm        -name "TMC_BPM"          -length  0.000000e+00 -e0  2.483106e+00
Quadrupole -name "TMC_QD_V03"       -length  4.000000e-02 -e0  2.483106e+00 -strength  7.839836e-01
Drift      -name "TMC_DR_QD_ED"     -length  4.250000e-02 -e0  2.483106e+00
Drift      -name "TMC_DR_V4"        -length  2.000000e-01 -e0  2.483106e+00
Drift      -name "TMC_DR_20"        -length  5.000000e-01 -e0  2.483106e+00
Sbend      -name "TMC_DP_DIP1"      -length  5.000001e-01 -e0  2.483106e+00 -angle -8.726600e-04 -E1  0.000000e+00 -E2 -8.726600e-04 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  0.000000e+00  -six_dim 1 -csr 1 
Drift      -name "TMC_DR_SIDE_C"    -length  5.000000e-01 -e0  2.483106e+00
Drift      -name "TMC_DR_SIDE"      -length  3.000001e+00 -e0  2.483106e+00
Sbend      -name "TMC_DP_DIP2"      -length  5.000001e-01 -e0  2.483106e+00 -angle  8.726600e-04 -E1  8.726600e-04 -E2  0.000000e+00 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  0.000000e+00  -six_dim 1 -csr 1 
Drift      -name "TMC_DR_CENT_C"    -length  2.500000e-01 -e0  2.483106e+00
Bpm        -name "TMC_BPM"          -length  0.000000e+00 -e0  2.483106e+00
Drift      -name "TMC_DR_CENT"      -length  5.000000e-01 -e0  2.483106e+00
Sbend      -name "TMC_DP_DIP3"      -length  5.000001e-01 -e0  2.483106e+00 -angle  8.726600e-04 -E1  0.000000e+00 -E2  8.726600e-04 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  0.000000e+00  -six_dim 1 -csr 1 
Drift      -name "TMC_DR_SIDE_C"    -length  5.000000e-01 -e0  2.483106e+00
Drift      -name "TMC_DR_SIDE"      -length  3.000001e+00 -e0  2.483106e+00
Sbend      -name "TMC_DP_DIP4"      -length  5.000001e-01 -e0  2.483106e+00 -angle -8.726600e-04 -E1 -8.726600e-04 -E2  0.000000e+00 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  0.000000e+00  -six_dim 1 -csr 1 
Drift      -name "TMC_DR_20_C"      -length  5.000000e-01 -e0  2.483106e+00
Drift      -name "TMC_WA_OU2"       -length  0.000000e+00 -e0  2.483106e+00
Drift      -name "HXR_DR_V1"        -length  2.000000e-01 -e0  2.483106e+00
Drift      -name "HXR_DR_QD_ED"     -length  5.200000e-02 -e0  2.483106e+00
Quadrupole -name "HXR_QD_V01"       -length  1.300000e-02 -e0  2.483106e+00 -strength  8.822368e-01
Dipole     -name "HXR_COR"          -length  0.000000e+00 -e0  2.483106e+00
Bpm        -name "HXR_BPM"          -length  0.000000e+00 -e0  2.483106e+00
Quadrupole -name "HXR_QD_V01"       -length  1.300000e-02 -e0  2.483106e+00 -strength  8.822368e-01
Drift      -name "HXR_DR_QD_ED"     -length  5.200000e-02 -e0  2.483106e+00
Drift      -name "HXR_DR_V2"        -length  5.892182e-01 -e0  2.483106e+00
Drift      -name "HXR_DR_QD_ED"     -length  5.200000e-02 -e0  2.483106e+00
Quadrupole -name "HXR_QD_V02"       -length  1.300000e-02 -e0  2.483106e+00 -strength -9.684114e-01
Dipole     -name "HXR_COR"          -length  0.000000e+00 -e0  2.483106e+00
Bpm        -name "HXR_BPM"          -length  0.000000e+00 -e0  2.483106e+00
Quadrupole -name "HXR_QD_V02"       -length  1.300000e-02 -e0  2.483106e+00 -strength -9.684114e-01
Drift      -name "HXR_DR_QD_ED"     -length  5.200000e-02 -e0  2.483106e+00
Drift      -name "HXR_DR_V3"        -length  1.144548e+00 -e0  2.483106e+00
Drift      -name "HXR_DR_QD_ED"     -length  5.200000e-02 -e0  2.483106e+00
Quadrupole -name "HXR_QD_V03"       -length  1.300000e-02 -e0  2.483106e+00 -strength  3.111973e-01
Dipole     -name "HXR_COR"          -length  0.000000e+00 -e0  2.483106e+00
Bpm        -name "HXR_BPM"          -length  0.000000e+00 -e0  2.483106e+00
Quadrupole -name "HXR_QD_V03"       -length  1.300000e-02 -e0  2.483106e+00 -strength  3.111973e-01
Drift      -name "HXR_DR_QD_ED"     -length  5.200000e-02 -e0  2.483106e+00
Drift      -name "HXR_DR_V4"        -length  2.759790e+00 -e0  2.483106e+00
Drift      -name "HXR_DR_CT"        -length  1.000000e-01 -e0  2.483106e+00
Drift      -name "HXR_WA_M_OU2"     -length  0.000000e+00 -e0  2.483106e+00
Drift      -name "HXR_DR_QD_ED"     -length  5.200000e-02 -e0  2.483106e+00
Quadrupole -name "HXR_QD_FH"        -length  1.300000e-02 -e0  2.483106e+00 -strength  4.842057e-01
Dipole     -name "HXR_COR"          -length  0.000000e+00 -e0  2.483106e+00
Bpm        -name "HXR_BPM"          -length  0.000000e+00 -e0  2.483106e+00
Quadrupole -name "HXR_QD_FH"        -length  1.300000e-02 -e0  2.483106e+00 -strength  4.842057e-01
Drift      -name "HXR_DR_QD_ED"     -length  5.200000e-02 -e0  2.483106e+00
Drift      -name "HXR_DR_WIG_ED"    -length  1.950000e-01 -e0  2.483106e+00
Drift      -name "HXR_WIG_0"        -length  1.768000e+00 -e0  2.483106e+00
Drift      -name "HXR_DR_WIG_ED"    -length  1.950000e-01 -e0  2.483106e+00
Drift      -name "HXR_DR_QD_ED"     -length  5.200000e-02 -e0  2.483106e+00
Quadrupole -name "HXR_QD_DH"        -length  1.300000e-02 -e0  2.483106e+00 -strength -4.842057e-01
Dipole     -name "HXR_COR"          -length  0.000000e+00 -e0  2.483106e+00
Bpm        -name "HXR_BPM"          -length  0.000000e+00 -e0  2.483106e+00
Quadrupole -name "HXR_QD_DH"        -length  1.300000e-02 -e0  2.483106e+00 -strength -4.842057e-01
Drift      -name "HXR_DR_QD_ED"     -length  5.200000e-02 -e0  2.483106e+00
Drift      -name "HXR_DR_WIG_ED"    -length  1.950000e-01 -e0  2.483106e+00
Drift      -name "HXR_WIG_0"        -length  1.768000e+00 -e0  2.483106e+00
Drift      -name "HXR_DR_WIG_ED"    -length  1.950000e-01 -e0  2.483106e+00
Drift      -name "HXR_DR_QD_ED"     -length  5.200000e-02 -e0  2.483106e+00
Quadrupole -name "HXR_QD_FH"        -length  1.300000e-02 -e0  2.483106e+00 -strength  4.842057e-01
Drift      -name "HXR_WA_OU2"       -length  0.000000e+00 -e0  2.483106e+00
