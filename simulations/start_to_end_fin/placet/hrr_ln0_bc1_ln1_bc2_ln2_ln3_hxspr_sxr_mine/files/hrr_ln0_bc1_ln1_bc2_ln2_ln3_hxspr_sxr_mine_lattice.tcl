Girder
SetReferenceEnergy   1.238601e-01
Drift      -name "LN0_DR_V5"        -length  6.639433e-01 -e0  1.237690e-01
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  1.237690e-01
Quadrupole -name "LN0_QD_V04"       -length  4.000000e-02 -e0  1.237690e-01 -strength  1.779631e-02
Dipole     -name "LN0_COR"          -length  0.000000e+00 -e0  1.237690e-01
Bpm        -name "LN0_BPM"          -length  0.000000e+00 -e0  1.237690e-01
Quadrupole -name "LN0_QD_V04"       -length  4.000000e-02 -e0  1.237690e-01 -strength  1.779631e-02
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  1.237690e-01
Drift      -name "LN0_DR_V6"        -length  2.598929e-01 -e0  1.237690e-01
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  1.237690e-01
Quadrupole -name "LN0_QD_V05"       -length  4.000000e-02 -e0  1.237690e-01 -strength  3.441588e-02
Dipole     -name "LN0_COR"          -length  0.000000e+00 -e0  1.237690e-01
Bpm        -name "LN0_BPM"          -length  0.000000e+00 -e0  1.237690e-01
Quadrupole -name "LN0_QD_V05"       -length  4.000000e-02 -e0  1.237690e-01 -strength  3.441588e-02
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  1.237690e-01
Drift      -name "LN0_DR_V7"        -length  1.419751e+00 -e0  1.237690e-01
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  1.237690e-01
Quadrupole -name "LN0_QD_V06"       -length  4.000000e-02 -e0  1.237690e-01 -strength -3.626929e-02
Dipole     -name "LN0_COR"          -length  0.000000e+00 -e0  1.237690e-01
Bpm        -name "LN0_BPM"          -length  0.000000e+00 -e0  1.237690e-01
Quadrupole -name "LN0_QD_V06"       -length  4.000000e-02 -e0  1.237690e-01 -strength -3.626929e-02
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  1.237690e-01
Drift      -name "LN0_DR_V8"        -length  2.000008e-01 -e0  1.237690e-01
Drift      -name "LN0_DR_VS"        -length  1.000000e-01 -e0  1.237690e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  1.237690e-01
Drift      -name "LN0_DR_XCA_ED_DI" -length  1.001043e-01 -e0  1.237690e-01
CrabCavity -name "LN0_XCA0_DI"      -length  9.997914e-01 -voltage  0.000000e+00 -phase  9.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.237690e-01
Drift      -name "LN0_DR_XCA_ED_DI" -length  1.001043e-01 -e0  1.237285e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  1.237285e-01
Drift      -name "LN0_TDS_DR"       -length  2.500000e-01 -e0  1.237285e-01
Drift      -name "LN0_TDS_DR"       -length  2.500000e-01 -e0  1.237285e-01
Drift      -name "LN0_TDS_DR"       -length  2.500000e-01 -e0  1.237285e-01
Drift      -name "LN0_TDS_DR"       -length  2.500000e-01 -e0  1.237285e-01
Drift      -name "LN0_TDS_DR"       -length  2.500000e-01 -e0  1.237285e-01
Drift      -name "LN0_TDS_DR"       -length  2.500000e-01 -e0  1.237285e-01
Drift      -name "LN0_TDS_DR"       -length  2.500000e-01 -e0  1.237285e-01
Drift      -name "LN0_TDS_DR"       -length  2.500000e-01 -e0  1.237285e-01
Sbend      -name "LN0_DP_TDS"       -length  1.000000e-01 -e0  1.237285e-01 -angle  0.000000e+00 -E1  0.000000e+00 -E2  0.000000e+00 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  0.000000e+00 -six_dim 1
Drift      -name "LN0_DR_20"        -length  2.000000e-01 -e0  1.237285e-01
Drift      -name "LN0_DR_VS"        -length  1.000000e-01 -e0  1.237285e-01
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  1.237285e-01
Quadrupole -name "LN0_QD_FH"        -length  4.000000e-02 -e0  1.237285e-01 -strength  3.464399e-02
Dipole     -name "LN0_COR"          -length  0.000000e+00 -e0  1.237285e-01
Bpm        -name "LN0_BPM"          -length  0.000000e+00 -e0  1.237285e-01
Quadrupole -name "LN0_QD_FH"        -length  4.000000e-02 -e0  1.237285e-01 -strength  3.464399e-02
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  1.237285e-01
Drift      -name "LN0_DR_SV"        -length  1.000000e-01 -e0  1.237285e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  1.237285e-01
Drift      -name "LN0_DR_CCA_ED"    -length  5.019800e-02 -e0  1.237285e-01
Cavity     -name "LN0_CCA0"         -length  1.899604e+00 -gradient  1.500000e-02 -phase  2.600000e+01 -frequency  5.997100e+00
SetReferenceEnergy   1.492816e-01
Drift      -name "LN0_DR_CCA_ED"    -length  5.019800e-02 -e0  1.492816e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  1.492816e-01
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  1.492816e-01
Quadrupole -name "LN0_QD_DH"        -length  4.000000e-02 -e0  1.492816e-01 -strength -4.179885e-02
Dipole     -name "LN0_COR"          -length  0.000000e+00 -e0  1.492816e-01
Bpm        -name "LN0_BPM"          -length  0.000000e+00 -e0  1.492816e-01
Quadrupole -name "LN0_QD_DH"        -length  4.000000e-02 -e0  1.492816e-01 -strength -4.179885e-02
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  1.492816e-01
Drift      -name "LN0_DR_VS"        -length  1.000000e-01 -e0  1.492816e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  1.492816e-01
Drift      -name "LN0_DR_CCA_ED"    -length  5.019800e-02 -e0  1.492816e-01
Cavity     -name "LN0_CCA0"         -length  1.899604e+00 -gradient  1.500000e-02 -phase  2.600000e+01 -frequency  5.997100e+00
SetReferenceEnergy   1.748347e-01
Drift      -name "LN0_DR_CCA_ED"    -length  5.019800e-02 -e0  1.748347e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  1.748347e-01
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  1.748347e-01
Quadrupole -name "LN0_QD_FH"        -length  4.000000e-02 -e0  1.748347e-01 -strength  4.895371e-02
Dipole     -name "LN0_COR"          -length  0.000000e+00 -e0  1.748347e-01
Bpm        -name "LN0_BPM"          -length  0.000000e+00 -e0  1.748347e-01
Quadrupole -name "LN0_QD_FH"        -length  4.000000e-02 -e0  1.748347e-01 -strength  4.895371e-02
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  1.748347e-01
Drift      -name "LN0_DR_VS"        -length  1.000000e-01 -e0  1.748347e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  1.748347e-01
Drift      -name "LN0_DR_CCA_ED"    -length  5.019800e-02 -e0  1.748347e-01
Cavity     -name "LN0_CCA0"         -length  1.899604e+00 -gradient  1.500000e-02 -phase  2.600000e+01 -frequency  5.997100e+00
SetReferenceEnergy   2.003877e-01
Drift      -name "LN0_DR_CCA_ED"    -length  5.019800e-02 -e0  2.003877e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  2.003877e-01
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  2.003877e-01
Quadrupole -name "LN0_QD_DH"        -length  4.000000e-02 -e0  2.003877e-01 -strength -5.610857e-02
Dipole     -name "LN0_COR"          -length  0.000000e+00 -e0  2.003877e-01
Bpm        -name "LN0_BPM"          -length  0.000000e+00 -e0  2.003877e-01
Quadrupole -name "LN0_QD_DH"        -length  4.000000e-02 -e0  2.003877e-01 -strength -5.610857e-02
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  2.003877e-01
Drift      -name "LN0_DR_VS"        -length  1.000000e-01 -e0  2.003877e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  2.003877e-01
Drift      -name "LN0_DR_CCA_ED"    -length  5.019800e-02 -e0  2.003877e-01
Cavity     -name "LN0_CCA0"         -length  1.899604e+00 -gradient  1.500000e-02 -phase  2.600000e+01 -frequency  5.997100e+00
SetReferenceEnergy   2.259408e-01
Drift      -name "LN0_DR_CCA_ED"    -length  5.019800e-02 -e0  2.259408e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  2.259408e-01
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  2.259408e-01
Quadrupole -name "LN0_QD_FH"        -length  4.000000e-02 -e0  2.259408e-01 -strength  6.326343e-02
Dipole     -name "LN0_COR"          -length  0.000000e+00 -e0  2.259408e-01
Bpm        -name "LN0_BPM"          -length  0.000000e+00 -e0  2.259408e-01
Quadrupole -name "LN0_QD_FH"        -length  4.000000e-02 -e0  2.259408e-01 -strength  6.326343e-02
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  2.259408e-01
Drift      -name "LN0_DR_VS"        -length  1.000000e-01 -e0  2.259408e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  2.259408e-01
Drift      -name "LN0_DR_CCA_ED"    -length  5.019800e-02 -e0  2.259408e-01
Cavity     -name "LN0_CCA0"         -length  1.899604e+00 -gradient  1.500000e-02 -phase  2.600000e+01 -frequency  5.997100e+00
SetReferenceEnergy   2.514939e-01
Drift      -name "LN0_DR_CCA_ED"    -length  5.019800e-02 -e0  2.514939e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  2.514939e-01
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  2.514939e-01
Quadrupole -name "LN0_QD_DH"        -length  4.000000e-02 -e0  2.514939e-01 -strength -7.041830e-02
Dipole     -name "LN0_COR"          -length  0.000000e+00 -e0  2.514939e-01
Bpm        -name "LN0_BPM"          -length  0.000000e+00 -e0  2.514939e-01
Quadrupole -name "LN0_QD_DH"        -length  4.000000e-02 -e0  2.514939e-01 -strength -7.041830e-02
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  2.514939e-01
Drift      -name "LN0_DR_VS"        -length  1.000000e-01 -e0  2.514939e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  2.514939e-01
Drift      -name "LN0_DR_CCA_ED"    -length  5.019800e-02 -e0  2.514939e-01
Cavity     -name "LN0_CCA0"         -length  1.899604e+00 -gradient  1.500000e-02 -phase  2.600000e+01 -frequency  5.997100e+00
SetReferenceEnergy   2.770470e-01
Drift      -name "LN0_DR_CCA_ED"    -length  5.019800e-02 -e0  2.770470e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  2.770470e-01
Drift      -name "LN0_WA_LNZ_IN2"   -length  0.000000e+00 -e0  2.770470e-01
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  2.770470e-01
Quadrupole -name "LN0_QD_FH"        -length  4.000000e-02 -e0  2.770470e-01 -strength  7.757317e-02
Dipole     -name "LN0_COR"          -length  0.000000e+00 -e0  2.770470e-01
Bpm        -name "LN0_BPM"          -length  0.000000e+00 -e0  2.770470e-01
Quadrupole -name "LN0_QD_FH"        -length  4.000000e-02 -e0  2.770470e-01 -strength  7.757317e-02
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  2.770470e-01
Drift      -name "LN0_DR_VS"        -length  1.000000e-01 -e0  2.770470e-01
Drift      -name "LN0_DR_LNZ_ED2"   -length  6.250000e-01 -e0  2.770470e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  2.770470e-01
Drift      -name "LN0_DR_KCA_ED"    -length  2.225410e-02 -e0  2.770470e-01
Cavity     -name "LN0_KCA0"         -length  3.054918e-01 -gradient  2.453617e-02 -phase  1.920000e+02 -frequency  3.598260e+01
SetReferenceEnergy   2.698730e-01
Drift      -name "LN0_DR_KCA_ED"    -length  2.225410e-02 -e0  2.698730e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  2.698730e-01
Drift      -name "LN0_DR_KCA_ED"    -length  2.225410e-02 -e0  2.698730e-01
Drift      -name "LN0_DR_KCA0"      -length  3.054918e-01 -e0  2.698730e-01
Drift      -name "LN0_DR_KCA_ED"    -length  2.225410e-02 -e0  2.698730e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  2.698730e-01
Drift      -name "LN0_DR_LNZ_ED2"   -length  6.250000e-01 -e0  2.698730e-01
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  2.698730e-01
Quadrupole -name "LN0_QD_DH"        -length  4.000000e-02 -e0  2.698730e-01 -strength -7.556445e-02
Dipole     -name "LN0_COR"          -length  0.000000e+00 -e0  2.698730e-01
Bpm        -name "LN0_BPM"          -length  0.000000e+00 -e0  2.698730e-01
Quadrupole -name "LN0_QD_DH"        -length  4.000000e-02 -e0  2.698730e-01 -strength -7.556445e-02
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  2.698730e-01
Drift      -name "LN0_DR_DR"        -length  1.000000e-01 -e0  2.698730e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  2.698730e-01
Drift      -name "LN0_DR_CCA_ED"    -length  5.019800e-02 -e0  2.698730e-01
Drift      -name "LN0_DR_CCA0H"     -length  3.498020e-01 -e0  2.698730e-01
Drift      -name "LN0_DR_A"         -length  2.000000e-01 -e0  2.698730e-01
Drift      -name "LN0_DR_VS"        -length  1.000000e-01 -e0  2.698730e-01
Drift      -name "LN0_DR_A"         -length  2.000000e-01 -e0  2.698730e-01
Drift      -name "LN0_DR_CT"        -length  1.000000e-01 -e0  2.698730e-01
Drift      -name "LN0_WA_OU2"       -length  0.000000e+00 -e0  2.698730e-01
Drift      -name "BC1_DR_V1"        -length  5.382067e-01 -e0  2.698730e-01
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.698730e-01
Quadrupole -name "BC1_QD_V01"       -length  4.000000e-02 -e0  2.698730e-01 -strength  2.054140e-02
Dipole     -name "BC1_COR"          -length  0.000000e+00 -e0  2.698730e-01
Bpm        -name "BC1_BPM"          -length  0.000000e+00 -e0  2.698730e-01
Quadrupole -name "BC1_QD_V01"       -length  4.000000e-02 -e0  2.698730e-01 -strength  2.054140e-02
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.698730e-01
Drift      -name "BC1_DR_V2"        -length  5.900340e+00 -e0  2.698730e-01
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.698730e-01
Quadrupole -name "BC1_QD_V02"       -length  4.000000e-02 -e0  2.698730e-01 -strength -9.874167e-02
Dipole     -name "BC1_COR"          -length  0.000000e+00 -e0  2.698730e-01
Bpm        -name "BC1_BPM"          -length  0.000000e+00 -e0  2.698730e-01
Quadrupole -name "BC1_QD_V02"       -length  4.000000e-02 -e0  2.698730e-01 -strength -9.874167e-02
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.698730e-01
Drift      -name "BC1_DR_V3"        -length  2.496128e-01 -e0  2.698730e-01
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.698730e-01
Quadrupole -name "BC1_QD_V03"       -length  4.000000e-02 -e0  2.698730e-01 -strength  3.530922e-02
Dipole     -name "BC1_COR"          -length  0.000000e+00 -e0  2.698730e-01
Bpm        -name "BC1_BPM"          -length  0.000000e+00 -e0  2.698730e-01
Quadrupole -name "BC1_QD_V03"       -length  4.000000e-02 -e0  2.698730e-01 -strength  3.530922e-02
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.698730e-01
Drift      -name "BC1_DR_V4"        -length  2.000000e-01 -e0  2.698730e-01
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.698730e-01
Quadrupole -name "BC1_QD_V04"       -length  4.000000e-02 -e0  2.698730e-01 -strength  5.701369e-02
Dipole     -name "BC1_COR"          -length  0.000000e+00 -e0  2.698730e-01
Bpm        -name "BC1_BPM"          -length  0.000000e+00 -e0  2.698730e-01
Quadrupole -name "BC1_QD_V04"       -length  4.000000e-02 -e0  2.698730e-01 -strength  5.701369e-02
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.698730e-01
Drift      -name "BC1_DR_V5"        -length  2.002040e-01 -e0  2.698730e-01
Drift      -name "BC1_DR_20"        -length  5.000000e-01 -e0  2.698730e-01
Sbend      -name "BC1_DP_DIP1"      -length  4.002981e-01 -e0  2.698729e-01 -angle -6.684611e-02 -E1  0.000000e+00 -E2 -6.684611e-02 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  0.000000e+00  -six_dim 1 -csr 1 
Drift      -name "BC1_DR_SIDE_C"    -length  5.000000e-01 -e0  2.698720e-01
Drift      -name "BC1_DR_SIDE"      -length  2.757275e+00 -e0  2.698720e-01
Sbend      -name "BC1_DP_DIP2"      -length  4.002981e-01 -e0  2.698716e-01 -angle  6.684611e-02 -E1  6.684611e-02 -E2  0.000000e+00 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  0.000000e+00  -six_dim 1 -csr 1 
Drift      -name "BC1_DR_CENT"      -length  3.500000e-01 -e0  2.698716e-01
Bpm        -name "BC1_BPM"          -length  0.000000e+00 -e0  2.698716e-01
Drift      -name "BC1_DR_CENT"      -length  3.500000e-01 -e0  2.698716e-01
Sbend      -name "BC1_DP_DIP3"      -length  4.002981e-01 -e0  2.698622e-01 -angle  6.684611e-02 -E1  0.000000e+00 -E2  6.684611e-02 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  0.000000e+00  -six_dim 1 -csr 1 
Drift      -name "BC1_DR_SIDE_C"    -length  5.000000e-01 -e0  2.698361e-01
Drift      -name "BC1_DR_SIDE"      -length  2.757275e+00 -e0  2.698361e-01
Sbend      -name "BC1_DP_DIP4"      -length  4.002981e-01 -e0  2.698120e-01 -angle -6.684611e-02 -E1 -6.684611e-02 -E2  0.000000e+00 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  0.000000e+00  -six_dim 1 -csr 1 
Drift      -name "BC1_DR_20_C"      -length  5.000000e-01 -e0  2.697895e-01
Drift      -name "BC1_WA_OU2"       -length  0.000000e+00 -e0  2.697895e-01
Drift      -name "BC1_DR_CT"        -length  1.000000e-01 -e0  2.697895e-01
Drift      -name "BC1_DR_DI_V1"     -length  2.911672e-01 -e0  2.697895e-01
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.697895e-01
Quadrupole -name "BC1_QD_DI_V01"    -length  4.000000e-02 -e0  2.697895e-01 -strength -9.254272e-02
Dipole     -name "BC1_COR_DI"       -length  0.000000e+00 -e0  2.697895e-01
Bpm        -name "BC1_BPM_DI"       -length  0.000000e+00 -e0  2.697895e-01
Quadrupole -name "BC1_QD_DI_V01"    -length  4.000000e-02 -e0  2.697895e-01 -strength -9.254272e-02
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.697895e-01
Drift      -name "BC1_DR_DI_V2"     -length  4.700020e-01 -e0  2.697895e-01
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.697895e-01
Quadrupole -name "BC1_QD_DI_V02"    -length  4.000000e-02 -e0  2.697895e-01 -strength  1.451457e-01
Dipole     -name "BC1_COR_DI"       -length  0.000000e+00 -e0  2.697895e-01
Bpm        -name "BC1_BPM_DI"       -length  0.000000e+00 -e0  2.697895e-01
Quadrupole -name "BC1_QD_DI_V02"    -length  4.000000e-02 -e0  2.697895e-01 -strength  1.451457e-01
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.697895e-01
Drift      -name "BC1_DR_DI_V3"     -length  1.199083e+00 -e0  2.697895e-01
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.697895e-01
Quadrupole -name "BC1_QD_DI_V03"    -length  4.000000e-02 -e0  2.697895e-01 -strength -5.534839e-02
Dipole     -name "BC1_COR_DI"       -length  0.000000e+00 -e0  2.697895e-01
Bpm        -name "BC1_BPM_DI"       -length  0.000000e+00 -e0  2.697895e-01
Quadrupole -name "BC1_QD_DI_V03"    -length  4.000000e-02 -e0  2.697895e-01 -strength -5.534839e-02
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.697895e-01
Drift      -name "BC1_DR_DI_V5"     -length  2.000000e-01 -e0  2.697895e-01
Drift      -name "BC1_DR_DI"        -length  2.500000e-01 -e0  2.697895e-01
Drift      -name "BC1_DR_VS"        -length  1.000000e-01 -e0  2.697895e-01
Drift      -name "BC1_DR_BL"        -length  5.000000e-02 -e0  2.697895e-01
Drift      -name "BC1_DR_XCA_ED_DI" -length  1.001043e-01 -e0  2.697895e-01
CrabCavity -name "BC1_XCA0_DI"      -length  9.997914e-01 -voltage  0.000000e+00 -phase  9.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.697895e-01
Drift      -name "BC1_DR_XCA_ED_DI" -length  1.001043e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_BL"        -length  5.000000e-02 -e0  2.697215e-01
Drift      -name "BC1_DR_VS"        -length  1.000000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_DI"        -length  2.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_DI"        -length  2.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_DI_QD_ED"  -length  4.250000e-02 -e0  2.697215e-01
Quadrupole -name "BC1_QD_DI_FH"     -length  4.000000e-02 -e0  2.697215e-01 -strength  8.631089e-02
Dipole     -name "BC1_COR_DI"       -length  0.000000e+00 -e0  2.697215e-01
Bpm        -name "BC1_BPM_DI"       -length  0.000000e+00 -e0  2.697215e-01
Quadrupole -name "BC1_QD_DI_FH"     -length  4.000000e-02 -e0  2.697215e-01 -strength  8.631089e-02
Drift      -name "BC1_DR_DI_QD_ED"  -length  4.250000e-02 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_DI_VS"     -length  1.000000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_DI_QD_ED"  -length  4.250000e-02 -e0  2.697215e-01
Quadrupole -name "BC1_QD_DI_DH"     -length  4.000000e-02 -e0  2.697215e-01 -strength -8.631089e-02
Dipole     -name "BC1_COR_DI"       -length  0.000000e+00 -e0  2.697215e-01
Bpm        -name "BC1_BPM_DI"       -length  0.000000e+00 -e0  2.697215e-01
Quadrupole -name "BC1_QD_DI_DH"     -length  4.000000e-02 -e0  2.697215e-01 -strength -8.631089e-02
Drift      -name "BC1_DR_DI_QD_ED"  -length  4.250000e-02 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_DI_VS"     -length  1.000000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_DI_QD_ED"  -length  4.250000e-02 -e0  2.697215e-01
Quadrupole -name "BC1_QD_DI_FH"     -length  4.000000e-02 -e0  2.697215e-01 -strength  8.631089e-02
Dipole     -name "BC1_COR_DI"       -length  0.000000e+00 -e0  2.697215e-01
Bpm        -name "BC1_BPM_DI"       -length  0.000000e+00 -e0  2.697215e-01
Quadrupole -name "BC1_QD_DI_FH"     -length  4.000000e-02 -e0  2.697215e-01 -strength  8.631089e-02
Drift      -name "BC1_DR_DI_QD_ED"  -length  4.250000e-02 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_DI_VS"     -length  1.000000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_DI_QD_ED"  -length  4.250000e-02 -e0  2.697215e-01
Quadrupole -name "BC1_QD_DI_DH"     -length  4.000000e-02 -e0  2.697215e-01 -strength -8.631089e-02
Dipole     -name "BC1_COR_DI"       -length  0.000000e+00 -e0  2.697215e-01
Bpm        -name "BC1_BPM_DI"       -length  0.000000e+00 -e0  2.697215e-01
Quadrupole -name "BC1_QD_DI_DH"     -length  4.000000e-02 -e0  2.697215e-01 -strength -8.631089e-02
Drift      -name "BC1_DR_DI_QD_ED"  -length  4.250000e-02 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_DI_VS"     -length  1.000000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_DI_QD_ED"  -length  4.250000e-02 -e0  2.697215e-01
Quadrupole -name "BC1_QD_DI_FH"     -length  4.000000e-02 -e0  2.697215e-01 -strength  8.631089e-02
Dipole     -name "BC1_COR_DI"       -length  0.000000e+00 -e0  2.697215e-01
Bpm        -name "BC1_BPM_DI"       -length  0.000000e+00 -e0  2.697215e-01
Quadrupole -name "BC1_QD_DI_FH"     -length  4.000000e-02 -e0  2.697215e-01 -strength  8.631089e-02
Drift      -name "BC1_DR_DI_QD_ED"  -length  4.250000e-02 -e0  2.697215e-01
Drift      -name "BC1_DR_DI"        -length  2.500000e-01 -e0  2.697215e-01
Sbend      -name "BC1_DP_DI"        -length  2.500000e-01 -e0  2.697215e-01 -angle  0.000000e+00 -E1  0.000000e+00 -E2  0.000000e+00 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  0.000000e+00 -six_dim 1
Drift      -name "BC1_DR_DI"        -length  2.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_DI"        -length  2.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_DR_DI"        -length  2.500000e-01 -e0  2.697215e-01
Drift      -name "BC1_WA_OU_DI2"    -length  0.000000e+00 -e0  2.697215e-01
Drift      -name "LN1_DR_V1"        -length  2.005300e-01 -e0  2.697215e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  2.697215e-01
Quadrupole -name "LN1_QD_V01"       -length  4.000000e-02 -e0  2.697215e-01 -strength  6.467030e-02
Dipole     -name "LN1_COR"          -length  0.000000e+00 -e0  2.697215e-01
Bpm        -name "LN1_BPM"          -length  0.000000e+00 -e0  2.697215e-01
Quadrupole -name "LN1_QD_V01"       -length  4.000000e-02 -e0  2.697215e-01 -strength  6.467030e-02
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  2.697215e-01
Drift      -name "LN1_DR_V2"        -length  2.192504e-01 -e0  2.697215e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  2.697215e-01
Quadrupole -name "LN1_QD_V02"       -length  4.000000e-02 -e0  2.697215e-01 -strength -1.223251e-01
Dipole     -name "LN1_COR"          -length  0.000000e+00 -e0  2.697215e-01
Bpm        -name "LN1_BPM"          -length  0.000000e+00 -e0  2.697215e-01
Quadrupole -name "LN1_QD_V02"       -length  4.000000e-02 -e0  2.697215e-01 -strength -1.223251e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  2.697215e-01
Drift      -name "LN1_DR_V3"        -length  1.914618e+00 -e0  2.697215e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  2.697215e-01
Quadrupole -name "LN1_QD_V03"       -length  4.000000e-02 -e0  2.697215e-01 -strength  1.250617e-02
Dipole     -name "LN1_COR"          -length  0.000000e+00 -e0  2.697215e-01
Bpm        -name "LN1_BPM"          -length  0.000000e+00 -e0  2.697215e-01
Quadrupole -name "LN1_QD_V03"       -length  4.000000e-02 -e0  2.697215e-01 -strength  1.250617e-02
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  2.697215e-01
Drift      -name "LN1_DR_V4"        -length  2.000000e-01 -e0  2.697215e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  2.697215e-01
Quadrupole -name "LN1_QD_FH"        -length  4.000000e-02 -e0  2.697215e-01 -strength  7.120648e-02
Dipole     -name "LN1_COR"          -length  0.000000e+00 -e0  2.697215e-01
Bpm        -name "LN1_BPM"          -length  0.000000e+00 -e0  2.697215e-01
Quadrupole -name "LN1_QD_FH"        -length  4.000000e-02 -e0  2.697215e-01 -strength  7.120648e-02
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  2.697215e-01
Drift      -name "LN1_DR_SV"        -length  1.000000e-01 -e0  2.697215e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  2.697215e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  2.697215e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  3.100000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.932074e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  2.932074e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  2.932074e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  2.932074e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  3.100000e+01 -frequency  1.199420e+01
SetReferenceEnergy   3.166933e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  3.166933e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  3.166933e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  3.166933e-01
Quadrupole -name "LN1_QD_DH"        -length  4.000000e-02 -e0  3.166933e-01 -strength -8.360703e-02
Dipole     -name "LN1_COR"          -length  0.000000e+00 -e0  3.166933e-01
Bpm        -name "LN1_BPM"          -length  0.000000e+00 -e0  3.166933e-01
Quadrupole -name "LN1_QD_DH"        -length  4.000000e-02 -e0  3.166933e-01 -strength -8.360703e-02
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  3.166933e-01
Drift      -name "LN1_DR_VS"        -length  1.000000e-01 -e0  3.166933e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  3.166933e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  3.166933e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  3.100000e+01 -frequency  1.199420e+01
SetReferenceEnergy   3.401792e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  3.401792e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  3.401792e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  3.401792e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  3.100000e+01 -frequency  1.199420e+01
SetReferenceEnergy   3.636651e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  3.636651e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  3.636651e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  3.636651e-01
Quadrupole -name "LN1_QD_FH"        -length  4.000000e-02 -e0  3.636651e-01 -strength  9.600758e-02
Dipole     -name "LN1_COR"          -length  0.000000e+00 -e0  3.636651e-01
Bpm        -name "LN1_BPM"          -length  0.000000e+00 -e0  3.636651e-01
Quadrupole -name "LN1_QD_FH"        -length  4.000000e-02 -e0  3.636651e-01 -strength  9.600758e-02
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  3.636651e-01
Drift      -name "LN1_DR_SV"        -length  1.000000e-01 -e0  3.636651e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  3.636651e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  3.636651e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  3.100000e+01 -frequency  1.199420e+01
SetReferenceEnergy   3.871509e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  3.871509e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  3.871509e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  3.871509e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  3.100000e+01 -frequency  1.199420e+01
SetReferenceEnergy   4.106368e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  4.106368e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  4.106368e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  4.106368e-01
Quadrupole -name "LN1_QD_DH"        -length  4.000000e-02 -e0  4.106368e-01 -strength -1.084081e-01
Dipole     -name "LN1_COR"          -length  0.000000e+00 -e0  4.106368e-01
Bpm        -name "LN1_BPM"          -length  0.000000e+00 -e0  4.106368e-01
Quadrupole -name "LN1_QD_DH"        -length  4.000000e-02 -e0  4.106368e-01 -strength -1.084081e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  4.106368e-01
Drift      -name "LN1_DR_VS"        -length  1.000000e-01 -e0  4.106368e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  4.106368e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  4.106368e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  3.100000e+01 -frequency  1.199420e+01
SetReferenceEnergy   4.341226e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  4.341226e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  4.341226e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  4.341226e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  3.100000e+01 -frequency  1.199420e+01
SetReferenceEnergy   4.576085e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  4.576085e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  4.576085e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  4.576085e-01
Quadrupole -name "LN1_QD_FH"        -length  4.000000e-02 -e0  4.576085e-01 -strength  1.208086e-01
Dipole     -name "LN1_COR"          -length  0.000000e+00 -e0  4.576085e-01
Bpm        -name "LN1_BPM"          -length  0.000000e+00 -e0  4.576085e-01
Quadrupole -name "LN1_QD_FH"        -length  4.000000e-02 -e0  4.576085e-01 -strength  1.208086e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  4.576085e-01
Drift      -name "LN1_DR_SV"        -length  1.000000e-01 -e0  4.576085e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  4.576085e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  4.576085e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  3.100000e+01 -frequency  1.199420e+01
SetReferenceEnergy   4.810944e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  4.810944e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  4.810944e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  4.810944e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  3.100000e+01 -frequency  1.199420e+01
SetReferenceEnergy   5.045802e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  5.045802e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  5.045802e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  5.045802e-01
Quadrupole -name "LN1_QD_DH"        -length  4.000000e-02 -e0  5.045802e-01 -strength -1.332092e-01
Dipole     -name "LN1_COR"          -length  0.000000e+00 -e0  5.045802e-01
Bpm        -name "LN1_BPM"          -length  0.000000e+00 -e0  5.045802e-01
Quadrupole -name "LN1_QD_DH"        -length  4.000000e-02 -e0  5.045802e-01 -strength -1.332092e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  5.045802e-01
Drift      -name "LN1_DR_VS"        -length  1.000000e-01 -e0  5.045802e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  5.045802e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  5.045802e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  3.100000e+01 -frequency  1.199420e+01
SetReferenceEnergy   5.280660e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  5.280660e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  5.280660e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  5.280660e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  3.100000e+01 -frequency  1.199420e+01
SetReferenceEnergy   5.515519e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  5.515519e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  5.515519e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  5.515519e-01
Quadrupole -name "LN1_QD_FH"        -length  4.000000e-02 -e0  5.515519e-01 -strength  1.456097e-01
Dipole     -name "LN1_COR"          -length  0.000000e+00 -e0  5.515519e-01
Bpm        -name "LN1_BPM"          -length  0.000000e+00 -e0  5.515519e-01
Quadrupole -name "LN1_QD_FH"        -length  4.000000e-02 -e0  5.515519e-01 -strength  1.456097e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  5.515519e-01
Drift      -name "LN1_DR_SV"        -length  1.000000e-01 -e0  5.515519e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  5.515519e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  5.515519e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  3.100000e+01 -frequency  1.199420e+01
SetReferenceEnergy   5.750377e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  5.750377e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  5.750377e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  5.750377e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  3.100000e+01 -frequency  1.199420e+01
SetReferenceEnergy   5.985236e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  5.985236e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  5.985236e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  5.985236e-01
Quadrupole -name "LN1_QD_DH"        -length  4.000000e-02 -e0  5.985236e-01 -strength -1.580102e-01
Dipole     -name "LN1_COR"          -length  0.000000e+00 -e0  5.985236e-01
Bpm        -name "LN1_BPM"          -length  0.000000e+00 -e0  5.985236e-01
Quadrupole -name "LN1_QD_DH"        -length  4.000000e-02 -e0  5.985236e-01 -strength -1.580102e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  5.985236e-01
Drift      -name "LN1_DR_VS"        -length  1.000000e-01 -e0  5.985236e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  5.985236e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  5.985236e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  3.100000e+01 -frequency  1.199420e+01
SetReferenceEnergy   6.220094e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  6.220094e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  6.220094e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  6.220094e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  3.100000e+01 -frequency  1.199420e+01
SetReferenceEnergy   6.454953e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  6.454953e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  6.454953e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  6.454953e-01
Quadrupole -name "LN1_QD_FH"        -length  4.000000e-02 -e0  6.454953e-01 -strength  1.704107e-01
Dipole     -name "LN1_COR"          -length  0.000000e+00 -e0  6.454953e-01
Bpm        -name "LN1_BPM"          -length  0.000000e+00 -e0  6.454953e-01
Quadrupole -name "LN1_QD_FH"        -length  4.000000e-02 -e0  6.454953e-01 -strength  1.704107e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  6.454953e-01
Drift      -name "LN1_DR_DR"        -length  1.000000e-01 -e0  6.454953e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  6.454953e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  6.454953e-01
Drift      -name "LN1_DR_XCA_"      -length  9.164755e-01 -e0  6.454953e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  6.454953e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  6.454953e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  6.454953e-01
Drift      -name "LN1_DR_XCA_"      -length  9.164755e-01 -e0  6.454953e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  6.454953e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  6.454953e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  6.454953e-01
Quadrupole -name "LN1_QD_DH"        -length  4.000000e-02 -e0  6.454953e-01 -strength -1.704107e-01
Dipole     -name "LN1_COR"          -length  0.000000e+00 -e0  6.454953e-01
Bpm        -name "LN1_BPM"          -length  0.000000e+00 -e0  6.454953e-01
Quadrupole -name "LN1_QD_DH"        -length  4.000000e-02 -e0  6.454953e-01 -strength -1.704107e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  6.454953e-01
Drift      -name "LN1_DR_DR"        -length  1.000000e-01 -e0  6.454953e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  6.454953e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  6.454953e-01
Drift      -name "LN1_DR_XCA_A"     -length  8.164755e-01 -e0  6.454953e-01
Drift      -name "LN1_DR_VS"        -length  1.000000e-01 -e0  6.454953e-01
Drift      -name "LN1_WA_OU2"       -length  0.000000e+00 -e0  6.454953e-01
Drift      -name "BC2_DR_V1"        -length  4.718667e-01 -e0  6.454953e-01
Drift      -name "BC2_DR_QD_ED"     -length  4.250000e-02 -e0  6.454953e-01
Quadrupole -name "BC2_QD_V01"       -length  4.000000e-02 -e0  6.454953e-01 -strength  1.154430e-02
Dipole     -name "BC2_COR"          -length  0.000000e+00 -e0  6.454953e-01
Bpm        -name "BC2_BPM"          -length  0.000000e+00 -e0  6.454953e-01
Quadrupole -name "BC2_QD_V01"       -length  4.000000e-02 -e0  6.454953e-01 -strength  1.154430e-02
Drift      -name "BC2_DR_QD_ED"     -length  4.250000e-02 -e0  6.454953e-01
Drift      -name "BC2_DR_V2"        -length  5.872423e+00 -e0  6.454953e-01
Drift      -name "BC2_DR_QD_ED"     -length  4.250000e-02 -e0  6.454953e-01
Quadrupole -name "BC2_QD_V02"       -length  4.000000e-02 -e0  6.454953e-01 -strength -2.979380e-01
Dipole     -name "BC2_COR"          -length  0.000000e+00 -e0  6.454953e-01
Bpm        -name "BC2_BPM"          -length  0.000000e+00 -e0  6.454953e-01
Quadrupole -name "BC2_QD_V02"       -length  4.000000e-02 -e0  6.454953e-01 -strength -2.979380e-01
Drift      -name "BC2_DR_QD_ED"     -length  4.250000e-02 -e0  6.454953e-01
Drift      -name "BC2_DR_V3"        -length  2.000000e-01 -e0  6.454953e-01
Drift      -name "BC2_DR_QD_ED"     -length  4.250000e-02 -e0  6.454953e-01
Quadrupole -name "BC2_QD_V03"       -length  4.000000e-02 -e0  6.454953e-01 -strength  2.768945e-01
Dipole     -name "BC2_COR"          -length  0.000000e+00 -e0  6.454953e-01
Bpm        -name "BC2_BPM"          -length  0.000000e+00 -e0  6.454953e-01
Quadrupole -name "BC2_QD_V03"       -length  4.000000e-02 -e0  6.454953e-01 -strength  2.768945e-01
Drift      -name "BC2_DR_QD_ED"     -length  4.250000e-02 -e0  6.454953e-01
Drift      -name "BC2_DR_V4"        -length  2.000000e-01 -e0  6.454953e-01
Drift      -name "BC2_DR_20"        -length  5.000000e-01 -e0  6.454953e-01
Sbend      -name "BC2_DP_DIP1"      -length  6.000476e-01 -e0  6.454905e-01 -angle -2.181662e-02 -E1  0.000000e+00 -E2 -2.181662e-02 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  0.000000e+00  -six_dim 1 -csr 1 
Drift      -name "BC2_DR_SIDE_C"    -length  5.000000e-01 -e0  6.454773e-01
Drift      -name "BC2_DR_SIDE"      -length  3.200881e+00 -e0  6.454773e-01
Sbend      -name "BC2_DP_DIP2"      -length  6.000476e-01 -e0  6.454653e-01 -angle  2.181662e-02 -E1  2.181662e-02 -E2  0.000000e+00 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  0.000000e+00  -six_dim 1 -csr 1 
Drift      -name "BC2_DR_CENT_C"    -length  2.500000e-01 -e0  6.454500e-01
Bpm        -name "BC2_BPM"          -length  0.000000e+00 -e0  6.454500e-01
Drift      -name "BC2_DR_CENT"      -length  2.500000e-01 -e0  6.454500e-01
Sbend      -name "BC2_DP_DIP3"      -length  6.000476e-01 -e0  6.453913e-01 -angle  2.181662e-02 -E1  0.000000e+00 -E2  2.181662e-02 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  0.000000e+00  -six_dim 1 -csr 1 
Drift      -name "BC2_DR_SIDE_C"    -length  5.000000e-01 -e0  6.453094e-01
Drift      -name "BC2_DR_SIDE"      -length  3.200881e+00 -e0  6.453094e-01
Sbend      -name "BC2_DP_DIP4"      -length  6.000476e-01 -e0  6.452170e-01 -angle -2.181662e-02 -E1 -2.181662e-02 -E2  0.000000e+00 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  0.000000e+00  -six_dim 1 -csr 1 
Drift      -name "BC2_DR_20_C"      -length  5.000000e-01 -e0  6.451429e-01
Drift      -name "BC2_WA_OU2"       -length  0.000000e+00 -e0  6.451429e-01
Drift      -name "LN2_DR_CT"        -length  1.000000e-01 -e0  6.451429e-01
Drift      -name "LN2_DR_V1"        -length  2.294830e+00 -e0  6.451429e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  6.451429e-01
Quadrupole -name "LN2_QD_V01"       -length  4.000000e-02 -e0  6.451429e-01 -strength  3.450173e-01
Dipole     -name "LN2_COR"          -length  0.000000e+00 -e0  6.451429e-01
Bpm        -name "LN2_BPM"          -length  0.000000e+00 -e0  6.451429e-01
Quadrupole -name "LN2_QD_V01"       -length  4.000000e-02 -e0  6.451429e-01 -strength  3.450173e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  6.451429e-01
Drift      -name "LN2_DR_V2"        -length  5.090891e-01 -e0  6.451429e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  6.451429e-01
Quadrupole -name "LN2_QD_V02"       -length  4.000000e-02 -e0  6.451429e-01 -strength -3.352172e-01
Dipole     -name "LN2_COR"          -length  0.000000e+00 -e0  6.451429e-01
Bpm        -name "LN2_BPM"          -length  0.000000e+00 -e0  6.451429e-01
Quadrupole -name "LN2_QD_V02"       -length  4.000000e-02 -e0  6.451429e-01 -strength -3.352172e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  6.451429e-01
Drift      -name "LN2_DR_V3"        -length  1.490464e+00 -e0  6.451429e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  6.451429e-01
Quadrupole -name "LN2_QD_V03"       -length  4.000000e-02 -e0  6.451429e-01 -strength  2.335484e-01
Dipole     -name "LN2_COR"          -length  0.000000e+00 -e0  6.451429e-01
Bpm        -name "LN2_BPM"          -length  0.000000e+00 -e0  6.451429e-01
Quadrupole -name "LN2_QD_V03"       -length  4.000000e-02 -e0  6.451429e-01 -strength  2.335484e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  6.451429e-01
Drift      -name "LN2_DR_V4"        -length  2.999939e+00 -e0  6.451429e-01
Drift      -name "LN2_DR_VS"        -length  1.000000e-01 -e0  6.451429e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  6.451429e-01
Quadrupole -name "LN2_QD_FH"        -length  4.000000e-02 -e0  6.451429e-01 -strength  1.728983e-01
Dipole     -name "LN2_COR"          -length  0.000000e+00 -e0  6.451429e-01
Bpm        -name "LN2_BPM"          -length  0.000000e+00 -e0  6.451429e-01
Quadrupole -name "LN2_QD_FH"        -length  4.000000e-02 -e0  6.451429e-01 -strength  1.728983e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  6.451429e-01
Drift      -name "LN2_DR_SV"        -length  1.000000e-01 -e0  6.451429e-01
Drift      -name "LN2_DR_BL"        -length  5.000000e-02 -e0  6.451429e-01
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  6.451429e-01
Cavity     -name "LN2_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   6.693276e-01
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  6.693276e-01
Drift      -name "LN2_DR_BL"        -length  5.000000e-02 -e0  6.693276e-01
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  6.693276e-01
Cavity     -name "LN2_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   6.935124e-01
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  6.935124e-01
Drift      -name "LN2_DR_BL"        -length  5.000000e-02 -e0  6.935124e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  6.935124e-01
Quadrupole -name "LN2_QD_DH"        -length  4.000000e-02 -e0  6.935124e-01 -strength -1.858613e-01
Dipole     -name "LN2_COR"          -length  0.000000e+00 -e0  6.935124e-01
Bpm        -name "LN2_BPM"          -length  0.000000e+00 -e0  6.935124e-01
Quadrupole -name "LN2_QD_DH"        -length  4.000000e-02 -e0  6.935124e-01 -strength -1.858613e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  6.935124e-01
Drift      -name "LN2_DR_VS"        -length  1.000000e-01 -e0  6.935124e-01
Drift      -name "LN2_DR_BL"        -length  5.000000e-02 -e0  6.935124e-01
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  6.935124e-01
Cavity     -name "LN2_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   7.176971e-01
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  7.176971e-01
Drift      -name "LN2_DR_BL"        -length  5.000000e-02 -e0  7.176971e-01
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  7.176971e-01
Cavity     -name "LN2_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   7.418819e-01
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  7.418819e-01
Drift      -name "LN2_DR_BL"        -length  5.000000e-02 -e0  7.418819e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  7.418819e-01
Quadrupole -name "LN2_QD_FH"        -length  4.000000e-02 -e0  7.418819e-01 -strength  1.988243e-01
Dipole     -name "LN2_COR"          -length  0.000000e+00 -e0  7.418819e-01
Bpm        -name "LN2_BPM"          -length  0.000000e+00 -e0  7.418819e-01
Quadrupole -name "LN2_QD_FH"        -length  4.000000e-02 -e0  7.418819e-01 -strength  1.988243e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  7.418819e-01
Drift      -name "LN2_DR_SV"        -length  1.000000e-01 -e0  7.418819e-01
Drift      -name "LN2_DR_BL"        -length  5.000000e-02 -e0  7.418819e-01
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  7.418819e-01
Cavity     -name "LN2_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   7.660666e-01
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  7.660666e-01
Drift      -name "LN2_DR_BL"        -length  5.000000e-02 -e0  7.660666e-01
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  7.660666e-01
Cavity     -name "LN2_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   7.902514e-01
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  7.902514e-01
Drift      -name "LN2_DR_BL"        -length  5.000000e-02 -e0  7.902514e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  7.902514e-01
Quadrupole -name "LN2_QD_DH"        -length  4.000000e-02 -e0  7.902514e-01 -strength -2.117874e-01
Dipole     -name "LN2_COR"          -length  0.000000e+00 -e0  7.902514e-01
Bpm        -name "LN2_BPM"          -length  0.000000e+00 -e0  7.902514e-01
Quadrupole -name "LN2_QD_DH"        -length  4.000000e-02 -e0  7.902514e-01 -strength -2.117874e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  7.902514e-01
Drift      -name "LN2_DR_VS"        -length  1.000000e-01 -e0  7.902514e-01
Drift      -name "LN2_DR_BL"        -length  5.000000e-02 -e0  7.902514e-01
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  7.902514e-01
Cavity     -name "LN2_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   8.144361e-01
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  8.144361e-01
Drift      -name "LN2_DR_BL"        -length  5.000000e-02 -e0  8.144361e-01
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  8.144361e-01
Cavity     -name "LN2_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   8.386209e-01
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  8.386209e-01
Drift      -name "LN2_DR_BL"        -length  5.000000e-02 -e0  8.386209e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  8.386209e-01
Quadrupole -name "LN2_QD_FH"        -length  4.000000e-02 -e0  8.386209e-01 -strength  2.247504e-01
Dipole     -name "LN2_COR"          -length  0.000000e+00 -e0  8.386209e-01
Bpm        -name "LN2_BPM"          -length  0.000000e+00 -e0  8.386209e-01
Quadrupole -name "LN2_QD_FH"        -length  4.000000e-02 -e0  8.386209e-01 -strength  2.247504e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  8.386209e-01
Drift      -name "LN2_WA_OU2"       -length  0.000000e+00 -e0  8.386209e-01
Drift      -name "LN2_DR_DI_V1"     -length  1.161052e+00 -e0  8.386209e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  8.386209e-01
Quadrupole -name "LN2_QD_DI_V01"    -length  4.000000e-02 -e0  8.386209e-01 -strength -8.432486e-02
Dipole     -name "LN2_COR_DI"       -length  0.000000e+00 -e0  8.386209e-01
Bpm        -name "LN2_BPM_DI"       -length  0.000000e+00 -e0  8.386209e-01
Quadrupole -name "LN2_QD_DI_V01"    -length  4.000000e-02 -e0  8.386209e-01 -strength -8.432486e-02
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  8.386209e-01
Drift      -name "LN2_DR_DI_V2"     -length  8.279980e-01 -e0  8.386209e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  8.386209e-01
Quadrupole -name "LN2_QD_DI_V02"    -length  4.000000e-02 -e0  8.386209e-01 -strength  3.401024e-01
Dipole     -name "LN2_COR_DI"       -length  0.000000e+00 -e0  8.386209e-01
Bpm        -name "LN2_BPM_DI"       -length  0.000000e+00 -e0  8.386209e-01
Quadrupole -name "LN2_QD_DI_V02"    -length  4.000000e-02 -e0  8.386209e-01 -strength  3.401024e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  8.386209e-01
Drift      -name "LN2_DR_DI_V3"     -length  2.001539e-01 -e0  8.386209e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  8.386209e-01
Quadrupole -name "LN2_QD_DI_V03"    -length  4.000000e-02 -e0  8.386209e-01 -strength -4.178604e-01
Dipole     -name "LN2_COR_DI"       -length  0.000000e+00 -e0  8.386209e-01
Bpm        -name "LN2_BPM_DI"       -length  0.000000e+00 -e0  8.386209e-01
Quadrupole -name "LN2_QD_DI_V03"    -length  4.000000e-02 -e0  8.386209e-01 -strength -4.178604e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  8.386209e-01
Drift      -name "LN2_DR_DI_V5"     -length  2.000000e-01 -e0  8.386209e-01
Drift      -name "LN2_DR_VS"        -length  1.000000e-01 -e0  8.386209e-01
Drift      -name "LN2_DR_DI"        -length  2.500000e-01 -e0  8.386209e-01
Drift      -name "LN2_DR_VS_DI"     -length  1.000000e-01 -e0  8.386209e-01
Drift      -name "LN2_DR_BL_DI"     -length  5.000000e-02 -e0  8.386209e-01
Drift      -name "LN2_DR_XCA_ED_DI" -length  1.001043e-01 -e0  8.386209e-01
CrabCavity -name "LN2_XCA0_DI"      -length  9.997914e-01 -voltage  0.000000e+00 -phase  9.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   8.386209e-01
Drift      -name "LN2_DR_XCA_ED_DI" -length  1.001043e-01 -e0  8.385444e-01
Drift      -name "LN2_DR_BL_DI"     -length  5.000000e-02 -e0  8.385444e-01
Drift      -name "LN2_DR_DI"        -length  2.500000e-01 -e0  8.385444e-01
Drift      -name "LN2_DR_DI"        -length  2.500000e-01 -e0  8.385444e-01
Drift      -name "LN2_DR_DI_QD_ED"  -length  4.250000e-02 -e0  8.385444e-01
Quadrupole -name "LN2_QD_DI_FH"     -length  4.000000e-02 -e0  8.385444e-01 -strength  2.683342e-01
Dipole     -name "LN2_COR_DI"       -length  0.000000e+00 -e0  8.385444e-01
Bpm        -name "LN2_BPM_DI"       -length  0.000000e+00 -e0  8.385444e-01
Quadrupole -name "LN2_QD_DI_FH"     -length  4.000000e-02 -e0  8.385444e-01 -strength  2.683342e-01
Drift      -name "LN2_DR_DI_QD_ED"  -length  4.250000e-02 -e0  8.385444e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.385444e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.385444e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.385444e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.385444e-01
Drift      -name "LN2_DR_DI_VS"     -length  1.000000e-01 -e0  8.385444e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.385444e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.385444e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.385444e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.385444e-01
Drift      -name "LN2_DR_DI_QD_ED"  -length  4.250000e-02 -e0  8.385444e-01
Quadrupole -name "LN2_QD_DI_DH"     -length  4.000000e-02 -e0  8.385444e-01 -strength -2.683342e-01
Dipole     -name "LN2_COR_DI"       -length  0.000000e+00 -e0  8.385444e-01
Bpm        -name "LN2_BPM_DI"       -length  0.000000e+00 -e0  8.385444e-01
Quadrupole -name "LN2_QD_DI_DH"     -length  4.000000e-02 -e0  8.385444e-01 -strength -2.683342e-01
Drift      -name "LN2_DR_DI_QD_ED"  -length  4.250000e-02 -e0  8.385444e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.385444e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.385444e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.385444e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.385444e-01
Drift      -name "LN2_DR_DI_VS"     -length  1.000000e-01 -e0  8.385444e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.385444e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.385444e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.385444e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.385444e-01
Drift      -name "LN2_DR_DI_QD_ED"  -length  4.250000e-02 -e0  8.385444e-01
Quadrupole -name "LN2_QD_DI_FH"     -length  4.000000e-02 -e0  8.385444e-01 -strength  2.683342e-01
Dipole     -name "LN2_COR_DI"       -length  0.000000e+00 -e0  8.385444e-01
Bpm        -name "LN2_BPM_DI"       -length  0.000000e+00 -e0  8.385444e-01
Quadrupole -name "LN2_QD_DI_FH"     -length  4.000000e-02 -e0  8.385444e-01 -strength  2.683342e-01
Drift      -name "LN2_DR_DI_QD_ED"  -length  4.250000e-02 -e0  8.385444e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.385444e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.385444e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.385444e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.385444e-01
Drift      -name "LN2_DR_DI_VS"     -length  1.000000e-01 -e0  8.385444e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.385444e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.385444e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.385444e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.385444e-01
Drift      -name "LN2_DR_DI_QD_ED"  -length  4.250000e-02 -e0  8.385444e-01
Quadrupole -name "LN2_QD_DI_DH"     -length  4.000000e-02 -e0  8.385444e-01 -strength -2.683342e-01
Dipole     -name "LN2_COR_DI"       -length  0.000000e+00 -e0  8.385444e-01
Bpm        -name "LN2_BPM_DI"       -length  0.000000e+00 -e0  8.385444e-01
Quadrupole -name "LN2_QD_DI_DH"     -length  4.000000e-02 -e0  8.385444e-01 -strength -2.683342e-01
Drift      -name "LN2_DR_DI_QD_ED"  -length  4.250000e-02 -e0  8.385444e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.385444e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.385444e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.385444e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.385444e-01
Drift      -name "LN2_DR_DI_VS"     -length  1.000000e-01 -e0  8.385444e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.385444e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.385444e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.385444e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.385444e-01
Drift      -name "LN2_DR_DI_QD_ED"  -length  4.250000e-02 -e0  8.385444e-01
Quadrupole -name "LN2_QD_DI_FH"     -length  4.000000e-02 -e0  8.385444e-01 -strength  2.683342e-01
Dipole     -name "LN2_COR_DI"       -length  0.000000e+00 -e0  8.385444e-01
Bpm        -name "LN2_BPM_DI"       -length  0.000000e+00 -e0  8.385444e-01
Quadrupole -name "LN2_QD_DI_FH"     -length  4.000000e-02 -e0  8.385444e-01 -strength  2.683342e-01
Drift      -name "LN2_DR_DI_QD_ED"  -length  4.250000e-02 -e0  8.385444e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.385444e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.385444e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.385444e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.385444e-01
Drift      -name "LN2_DR_DI_VS"     -length  1.000000e-01 -e0  8.385444e-01
Sbend      -name "LN2_DP_DI"        -length  1.500000e-01 -e0  8.385444e-01 -angle  0.000000e+00 -E1  0.000000e+00 -E2  0.000000e+00 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  0.000000e+00 -six_dim 1
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.385444e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.385444e-01
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  8.385444e-01
Drift      -name "LN2_DR_DI_QD_ED"  -length  4.250000e-02 -e0  8.385444e-01
Quadrupole -name "LN2_QD_DI_DH"     -length  4.000000e-02 -e0  8.385444e-01 -strength -2.683342e-01
Dipole     -name "LN2_COR_DI"       -length  0.000000e+00 -e0  8.385444e-01
Bpm        -name "LN2_BPM_DI"       -length  0.000000e+00 -e0  8.385444e-01
Quadrupole -name "LN2_QD_DI_DH"     -length  4.000000e-02 -e0  8.385444e-01 -strength -2.683342e-01
Drift      -name "LN2_DR_DI_QD_ED"  -length  4.250000e-02 -e0  8.385444e-01
Drift      -name "LN2_DR_DI"        -length  2.500000e-01 -e0  8.385444e-01
Drift      -name "LN2_WA_OU_DI2"    -length  0.000000e+00 -e0  8.385444e-01
Drift      -name "LN2_DR_BP_V1"     -length  3.714219e+00 -e0  8.385444e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  8.385444e-01
Quadrupole -name "LN2_QD_BP_V01"    -length  4.000000e-02 -e0  8.385444e-01 -strength -3.905101e-02
Dipole     -name "LN2_COR"          -length  0.000000e+00 -e0  8.385444e-01
Bpm        -name "LN2_BPM"          -length  0.000000e+00 -e0  8.385444e-01
Quadrupole -name "LN2_QD_BP_V01"    -length  4.000000e-02 -e0  8.385444e-01 -strength -3.905101e-02
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  8.385444e-01
Drift      -name "LN2_DR_BP_V2"     -length  3.528767e+00 -e0  8.385444e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  8.385444e-01
Quadrupole -name "LN2_QD_BP_V02"    -length  4.000000e-02 -e0  8.385444e-01 -strength  4.632623e-01
Dipole     -name "LN2_COR"          -length  0.000000e+00 -e0  8.385444e-01
Bpm        -name "LN2_BPM"          -length  0.000000e+00 -e0  8.385444e-01
Quadrupole -name "LN2_QD_BP_V02"    -length  4.000000e-02 -e0  8.385444e-01 -strength  4.632623e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  8.385444e-01
Drift      -name "LN2_DR_BP_V3"     -length  2.000269e-01 -e0  8.385444e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  8.385444e-01
Quadrupole -name "LN2_QD_BP_V03"    -length  4.000000e-02 -e0  8.385444e-01 -strength -4.519704e-01
Dipole     -name "LN2_COR"          -length  0.000000e+00 -e0  8.385444e-01
Bpm        -name "LN2_BPM"          -length  0.000000e+00 -e0  8.385444e-01
Quadrupole -name "LN2_QD_BP_V03"    -length  4.000000e-02 -e0  8.385444e-01 -strength -4.519704e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  8.385444e-01
Drift      -name "LN2_DR_BP_V4"     -length  2.645902e-01 -e0  8.385444e-01
Drift      -name "LN2_DR_BP_0"      -length  2.000000e-01 -e0  8.385444e-01
Drift      -name "LN2_DR_BP_BL"     -length  5.000000e-02 -e0  8.385444e-01
Drift      -name "LN2_DR_SCA_ED"    -length  8.325105e-02 -e0  8.385444e-01
CrabCavity -name "LN2_SCA0"         -length  5.334979e-01 -voltage  0.000000e+00 -phase  9.000000e+01 -frequency  2.997000e+00
SetReferenceEnergy   8.385444e-01
Drift      -name "LN2_DR_SCA_ED"    -length  8.325105e-02 -e0  8.385396e-01
Drift      -name "LN2_DR_BP_BL"     -length  5.000000e-02 -e0  8.385396e-01
Drift      -name "LN2_DR_BP_0"      -length  2.000000e-01 -e0  8.385396e-01
Drift      -name "LN2_DR_BP"        -length  4.000000e-01 -e0  8.385396e-01
Drift      -name "LN2_DR_BP"        -length  4.000000e-01 -e0  8.385396e-01
Drift      -name "LN2_DR_BP_VS"     -length  1.000000e-01 -e0  8.385396e-01
Drift      -name "LN2_DR_BP_DR"     -length  1.000000e-01 -e0  8.385396e-01
Drift      -name "LN2_DP_SEPM"      -length  2.500000e-01 -e0  8.385396e-01
Drift      -name "LN2_DR_BP"        -length  4.000000e-01 -e0  8.385396e-01
Drift      -name "LN2_DR_BP"        -length  4.000000e-01 -e0  8.385396e-01
Drift      -name "LN2_DR_BP"        -length  4.000000e-01 -e0  8.385396e-01
Drift      -name "LN2_DR_BP"        -length  4.000000e-01 -e0  8.385396e-01
Drift      -name "LN2_DR_BP"        -length  4.000000e-01 -e0  8.385396e-01
Drift      -name "LN2_DR_BP_VS"     -length  1.000000e-01 -e0  8.385396e-01
Drift      -name "LN2_DR_BP"        -length  4.000000e-01 -e0  8.385396e-01
Drift      -name "LN2_BP_WA_OU2_"   -length  0.000000e+00 -e0  8.385396e-01
Drift      -name "LN3_DR_V1"        -length  5.000000e-02 -e0  8.385396e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  8.385396e-01
Quadrupole -name "LN3_QD_V01"       -length  4.000000e-02 -e0  8.385396e-01 -strength  2.362173e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  8.385396e-01
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  8.385396e-01
Quadrupole -name "LN3_QD_V01"       -length  4.000000e-02 -e0  8.385396e-01 -strength  2.362173e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  8.385396e-01
Drift      -name "LN3_DR_V2"        -length  5.342817e-01 -e0  8.385396e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  8.385396e-01
Quadrupole -name "LN3_QD_V02"       -length  4.000000e-02 -e0  8.385396e-01 -strength  3.181431e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  8.385396e-01
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  8.385396e-01
Quadrupole -name "LN3_QD_V02"       -length  4.000000e-02 -e0  8.385396e-01 -strength  3.181431e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  8.385396e-01
Drift      -name "LN3_DR_V3"        -length  9.374694e-02 -e0  8.385396e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  8.385396e-01
Quadrupole -name "LN3_QD_V03"       -length  4.000000e-02 -e0  8.385396e-01 -strength -5.045661e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  8.385396e-01
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  8.385396e-01
Quadrupole -name "LN3_QD_V03"       -length  4.000000e-02 -e0  8.385396e-01 -strength -5.045661e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  8.385396e-01
Drift      -name "LN3_DR_V4"        -length  1.580023e+00 -e0  8.385396e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  8.385396e-01
Quadrupole -name "LN3_QD_V04"       -length  4.000000e-02 -e0  8.385396e-01 -strength -1.113059e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  8.385396e-01
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  8.385396e-01
Quadrupole -name "LN3_QD_V04"       -length  4.000000e-02 -e0  8.385396e-01 -strength -1.113059e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  8.385396e-01
Drift      -name "LN3_DR_V5"        -length  1.237385e+00 -e0  8.385396e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  8.385396e-01
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  8.385396e-01 -strength  1.408747e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  8.385396e-01
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  8.385396e-01
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  8.385396e-01 -strength  1.408747e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  8.385396e-01
Drift      -name "LN3_DR_SV"        -length  1.000000e-01 -e0  8.385396e-01
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  8.385396e-01
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  8.385396e-01
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   8.627243e-01
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  8.627243e-01
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  8.627243e-01
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  8.627243e-01
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   8.869090e-01
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  8.869090e-01
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  8.869090e-01
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  8.869090e-01
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   9.110937e-01
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  9.110937e-01
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  9.110937e-01
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  9.110937e-01
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   9.352784e-01
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  9.352784e-01
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  9.352784e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  9.352784e-01
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  9.352784e-01 -strength -1.571268e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  9.352784e-01
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  9.352784e-01
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  9.352784e-01 -strength -1.571268e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  9.352784e-01
Drift      -name "LN3_DR_VS"        -length  1.000000e-01 -e0  9.352784e-01
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  9.352784e-01
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  9.352784e-01
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   9.594631e-01
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  9.594631e-01
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  9.594631e-01
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  9.594631e-01
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   9.836478e-01
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  9.836478e-01
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  9.836478e-01
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  9.836478e-01
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.007833e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.007833e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.007833e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.007833e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.032017e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.032017e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.032017e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.032017e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  1.032017e+00 -strength  1.733789e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  1.032017e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  1.032017e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  1.032017e+00 -strength  1.733789e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.032017e+00
Drift      -name "LN3_DR_SV"        -length  1.000000e-01 -e0  1.032017e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.032017e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.032017e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.056202e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.056202e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.056202e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.056202e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.080387e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.080387e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.080387e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.080387e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.104571e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.104571e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.104571e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.104571e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.128756e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.128756e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.128756e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.128756e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  1.128756e+00 -strength -1.896310e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  1.128756e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  1.128756e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  1.128756e+00 -strength -1.896310e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.128756e+00
Drift      -name "LN3_DR_VS"        -length  1.000000e-01 -e0  1.128756e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.128756e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.128756e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.152941e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.152941e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.152941e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.152941e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.177125e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.177125e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.177125e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.177125e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.201310e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.201310e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.201310e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.201310e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.225495e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.225495e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.225495e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.225495e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  1.225495e+00 -strength  2.058831e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  1.225495e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  1.225495e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  1.225495e+00 -strength  2.058831e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.225495e+00
Drift      -name "LN3_DR_SV"        -length  1.000000e-01 -e0  1.225495e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.225495e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.225495e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.249679e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.249679e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.249679e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.249679e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.273864e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.273864e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.273864e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.273864e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.298049e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.298049e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.298049e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.298049e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.322234e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.322234e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.322234e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.322234e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  1.322234e+00 -strength -2.221352e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  1.322234e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  1.322234e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  1.322234e+00 -strength -2.221352e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.322234e+00
Drift      -name "LN3_DR_VS"        -length  1.000000e-01 -e0  1.322234e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.322234e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.322234e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.346418e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.346418e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.346418e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.346418e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.370603e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.370603e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.370603e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.370603e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.394788e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.394788e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.394788e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.394788e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.418972e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.418972e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.418972e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.418972e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  1.418972e+00 -strength  2.383874e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  1.418972e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  1.418972e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  1.418972e+00 -strength  2.383874e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.418972e+00
Drift      -name "LN3_DR_SV"        -length  1.000000e-01 -e0  1.418972e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.418972e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.418972e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.443157e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.443157e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.443157e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.443157e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.467342e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.467342e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.467342e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.467342e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.491526e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.491526e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.491526e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.491526e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.515711e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.515711e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.515711e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.515711e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  1.515711e+00 -strength -2.546395e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  1.515711e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  1.515711e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  1.515711e+00 -strength -2.546395e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.515711e+00
Drift      -name "LN3_DR_VS"        -length  1.000000e-01 -e0  1.515711e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.515711e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.515711e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.539896e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.539896e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.539896e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.539896e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.564080e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.564080e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.564080e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.564080e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.588265e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.588265e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.588265e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.588265e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  3.000000e-02 -phase  2.800000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.612450e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.612450e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.612450e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.612450e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  1.612450e+00 -strength  2.708916e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  1.612450e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  1.612450e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  1.612450e+00 -strength  2.708916e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.612450e+00
Drift      -name "LN3_DR_SV"        -length  1.000000e-01 -e0  1.612450e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.612450e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.612450e+00
Cavity     -name "LN3_XCA0_2"       -length  9.164755e-01 -gradient  1.500000e-02 -phase  1.520000e+02 -frequency  1.199420e+01
SetReferenceEnergy   1.600221e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.600221e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.600221e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.600221e+00
Cavity     -name "LN3_XCA0_2"       -length  9.164755e-01 -gradient  1.500000e-02 -phase  1.520000e+02 -frequency  1.199420e+01
SetReferenceEnergy   1.587991e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.587991e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.587991e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.587991e+00
Cavity     -name "LN3_XCA0_2"       -length  9.164755e-01 -gradient  1.500000e-02 -phase  1.520000e+02 -frequency  1.199420e+01
SetReferenceEnergy   1.575762e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.575762e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.575762e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.575762e+00
Cavity     -name "LN3_XCA0_2"       -length  9.164755e-01 -gradient  1.500000e-02 -phase  1.520000e+02 -frequency  1.199420e+01
SetReferenceEnergy   1.563533e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.563533e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.563533e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.563533e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  1.563533e+00 -strength -2.626735e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  1.563533e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  1.563533e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  1.563533e+00 -strength -2.626735e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.563533e+00
Drift      -name "LN3_DR_VS"        -length  1.000000e-01 -e0  1.563533e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.563533e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.563533e+00
Cavity     -name "LN3_XCA0_2"       -length  9.164755e-01 -gradient  1.500000e-02 -phase  1.520000e+02 -frequency  1.199420e+01
SetReferenceEnergy   1.551304e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.551304e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.551304e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.551304e+00
Cavity     -name "LN3_XCA0_2"       -length  9.164755e-01 -gradient  1.500000e-02 -phase  1.520000e+02 -frequency  1.199420e+01
SetReferenceEnergy   1.539074e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.539074e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.539074e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.539074e+00
Cavity     -name "LN3_XCA0_2"       -length  9.164755e-01 -gradient  1.500000e-02 -phase  1.520000e+02 -frequency  1.199420e+01
SetReferenceEnergy   1.526845e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.526845e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.526845e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.526845e+00
Cavity     -name "LN3_XCA0_2"       -length  9.164755e-01 -gradient  1.500000e-02 -phase  1.520000e+02 -frequency  1.199420e+01
SetReferenceEnergy   1.514616e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.514616e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.514616e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.514616e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  1.514616e+00 -strength  2.544555e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  1.514616e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  1.514616e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  1.514616e+00 -strength  2.544555e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.514616e+00
Drift      -name "LN3_DR_SV"        -length  1.000000e-01 -e0  1.514616e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.514616e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.514616e+00
Cavity     -name "LN3_XCA0_2"       -length  9.164755e-01 -gradient  1.500000e-02 -phase  1.520000e+02 -frequency  1.199420e+01
SetReferenceEnergy   1.502387e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.502387e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.502387e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.502387e+00
Cavity     -name "LN3_XCA0_2"       -length  9.164755e-01 -gradient  1.500000e-02 -phase  1.520000e+02 -frequency  1.199420e+01
SetReferenceEnergy   1.490157e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.490157e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.490157e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.490157e+00
Cavity     -name "LN3_XCA0_2"       -length  9.164755e-01 -gradient  1.500000e-02 -phase  1.520000e+02 -frequency  1.199420e+01
SetReferenceEnergy   1.477928e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.477928e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.477928e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.477928e+00
Cavity     -name "LN3_XCA0_2"       -length  9.164755e-01 -gradient  1.500000e-02 -phase  1.520000e+02 -frequency  1.199420e+01
SetReferenceEnergy   1.465699e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.465699e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.465699e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.465699e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  1.465699e+00 -strength -2.462374e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  1.465699e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  1.465699e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  1.465699e+00 -strength -2.462374e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.465699e+00
Drift      -name "LN3_DR_VS"        -length  1.000000e-01 -e0  1.465699e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.465699e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.465699e+00
Cavity     -name "LN3_XCA0_2"       -length  9.164755e-01 -gradient  1.500000e-02 -phase  1.520000e+02 -frequency  1.199420e+01
SetReferenceEnergy   1.453470e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.453470e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.453470e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.453470e+00
Cavity     -name "LN3_XCA0_2"       -length  9.164755e-01 -gradient  1.500000e-02 -phase  1.520000e+02 -frequency  1.199420e+01
SetReferenceEnergy   1.441240e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.441240e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.441240e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.441240e+00
Cavity     -name "LN3_XCA0_2"       -length  9.164755e-01 -gradient  1.500000e-02 -phase  1.520000e+02 -frequency  1.199420e+01
SetReferenceEnergy   1.429011e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.429011e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.429011e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.429011e+00
Cavity     -name "LN3_XCA0_2"       -length  9.164755e-01 -gradient  1.500000e-02 -phase  1.520000e+02 -frequency  1.199420e+01
SetReferenceEnergy   1.416782e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.416782e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.416782e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.416782e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  1.416782e+00 -strength  2.380193e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  1.416782e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  1.416782e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  1.416782e+00 -strength  2.380193e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.416782e+00
Drift      -name "LN3_DR_SV"        -length  1.000000e-01 -e0  1.416782e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.416782e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.416782e+00
Cavity     -name "LN3_XCA0_2"       -length  9.164755e-01 -gradient  1.500000e-02 -phase  1.520000e+02 -frequency  1.199420e+01
SetReferenceEnergy   1.404552e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.404552e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.404552e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.404552e+00
Cavity     -name "LN3_XCA0_2"       -length  9.164755e-01 -gradient  1.500000e-02 -phase  1.520000e+02 -frequency  1.199420e+01
SetReferenceEnergy   1.392323e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.392323e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.392323e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.392323e+00
Cavity     -name "LN3_XCA0_2"       -length  9.164755e-01 -gradient  1.500000e-02 -phase  1.520000e+02 -frequency  1.199420e+01
SetReferenceEnergy   1.380094e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.380094e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.380094e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.380094e+00
Cavity     -name "LN3_XCA0_2"       -length  9.164755e-01 -gradient  1.500000e-02 -phase  1.520000e+02 -frequency  1.199420e+01
SetReferenceEnergy   1.367865e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.367865e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.367865e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.367865e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  1.367865e+00 -strength -2.298013e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  1.367865e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  1.367865e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  1.367865e+00 -strength -2.298013e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.367865e+00
Drift      -name "LN3_DR_VS"        -length  1.000000e-01 -e0  1.367865e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.367865e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.367865e+00
Cavity     -name "LN3_XCA0_2"       -length  9.164755e-01 -gradient  1.500000e-02 -phase  1.520000e+02 -frequency  1.199420e+01
SetReferenceEnergy   1.355635e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.355635e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.355635e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.355635e+00
Cavity     -name "LN3_XCA0_2"       -length  9.164755e-01 -gradient  1.500000e-02 -phase  1.520000e+02 -frequency  1.199420e+01
SetReferenceEnergy   1.343406e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.343406e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.343406e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.343406e+00
Cavity     -name "LN3_XCA0_2"       -length  9.164755e-01 -gradient  1.500000e-02 -phase  1.520000e+02 -frequency  1.199420e+01
SetReferenceEnergy   1.331177e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.331177e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.331177e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.331177e+00
Cavity     -name "LN3_XCA0_2"       -length  9.164755e-01 -gradient  1.500000e-02 -phase  1.520000e+02 -frequency  1.199420e+01
SetReferenceEnergy   1.318948e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.318948e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.318948e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.318948e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  1.318948e+00 -strength  2.215832e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  1.318948e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  1.318948e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  1.318948e+00 -strength  2.215832e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.318948e+00
Drift      -name "LN3_DR_SV"        -length  1.000000e-01 -e0  1.318948e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.318948e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.318948e+00
Cavity     -name "LN3_XCA0_2"       -length  9.164755e-01 -gradient  1.500000e-02 -phase  1.520000e+02 -frequency  1.199420e+01
SetReferenceEnergy   1.306718e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.306718e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.306718e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.306718e+00
Cavity     -name "LN3_XCA0_2"       -length  9.164755e-01 -gradient  1.500000e-02 -phase  1.520000e+02 -frequency  1.199420e+01
SetReferenceEnergy   1.294489e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.294489e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.294489e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.294489e+00
Cavity     -name "LN3_XCA0_2"       -length  9.164755e-01 -gradient  1.500000e-02 -phase  1.520000e+02 -frequency  1.199420e+01
SetReferenceEnergy   1.282260e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.282260e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.282260e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.282260e+00
Cavity     -name "LN3_XCA0_2"       -length  9.164755e-01 -gradient  1.500000e-02 -phase  1.520000e+02 -frequency  1.199420e+01
SetReferenceEnergy   1.270031e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.270031e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.270031e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.270031e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  1.270031e+00 -strength -2.133651e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  1.270031e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  1.270031e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  1.270031e+00 -strength -2.133651e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.270031e+00
Drift      -name "LN3_DR_VS"        -length  1.000000e-01 -e0  1.270031e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.270031e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.270031e+00
Cavity     -name "LN3_XCA0_2"       -length  9.164755e-01 -gradient  1.500000e-02 -phase  1.520000e+02 -frequency  1.199420e+01
SetReferenceEnergy   1.257801e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.257801e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.257801e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.257801e+00
Cavity     -name "LN3_XCA0_2"       -length  9.164755e-01 -gradient  1.500000e-02 -phase  1.520000e+02 -frequency  1.199420e+01
SetReferenceEnergy   1.245572e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.245572e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.245572e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.245572e+00
Cavity     -name "LN3_XCA0_2"       -length  9.164755e-01 -gradient  1.500000e-02 -phase  1.520000e+02 -frequency  1.199420e+01
SetReferenceEnergy   1.233343e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.233343e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.233343e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.233343e+00
Cavity     -name "LN3_XCA0_2"       -length  9.164755e-01 -gradient  1.500000e-02 -phase  1.520000e+02 -frequency  1.199420e+01
SetReferenceEnergy   1.221114e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.221114e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.221114e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.221114e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  1.221114e+00 -strength  2.051471e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  1.221114e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  1.221114e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  1.221114e+00 -strength  2.051471e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.221114e+00
Drift      -name "LN3_DR_SV"        -length  1.000000e-01 -e0  1.221114e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.221114e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.221114e+00
Cavity     -name "LN3_XCA0_2"       -length  9.164755e-01 -gradient  1.500000e-02 -phase  1.520000e+02 -frequency  1.199420e+01
SetReferenceEnergy   1.208884e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.208884e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.208884e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.208884e+00
Cavity     -name "LN3_XCA0_2"       -length  9.164755e-01 -gradient  1.500000e-02 -phase  1.520000e+02 -frequency  1.199420e+01
SetReferenceEnergy   1.196655e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.196655e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.196655e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.196655e+00
Cavity     -name "LN3_XCA0_2"       -length  9.164755e-01 -gradient  1.500000e-02 -phase  1.520000e+02 -frequency  1.199420e+01
SetReferenceEnergy   1.184426e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.184426e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.184426e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.184426e+00
Cavity     -name "LN3_XCA0_2"       -length  9.164755e-01 -gradient  1.500000e-02 -phase  1.520000e+02 -frequency  1.199420e+01
SetReferenceEnergy   1.172197e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.172197e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.172197e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.172197e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  1.172197e+00 -strength -1.969290e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  1.172197e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  1.172197e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  1.172197e+00 -strength -1.969290e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.172197e+00
Drift      -name "LN3_DR_DR"        -length  1.000000e-01 -e0  1.172197e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.172197e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.172197e+00
Drift      -name "LN3_DR_XCA_"      -length  9.164755e-01 -e0  1.172197e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.172197e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.172197e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.172197e+00
Drift      -name "LN3_DR_XCA_"      -length  9.164755e-01 -e0  1.172197e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.172197e+00
Drift      -name "LN3_DR_CT"        -length  1.000000e-01 -e0  1.172197e+00
Drift      -name "LN3_WA_OU2"       -length  0.000000e+00 -e0  1.172197e+00
Drift      -name "LN3_DR_BP_V1"     -length  1.921855e+00 -e0  1.172197e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.172197e+00
Quadrupole -name "LN3_QD_BP_V01"    -length  4.000000e-02 -e0  1.172197e+00 -strength -3.668159e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  1.172197e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  1.172197e+00
Quadrupole -name "LN3_QD_BP_V01"    -length  4.000000e-02 -e0  1.172197e+00 -strength -3.668159e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.172197e+00
Drift      -name "LN3_DR_BP_V2"     -length  2.450553e+00 -e0  1.172197e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.172197e+00
Quadrupole -name "LN3_QD_BP_V02"    -length  4.000000e-02 -e0  1.172197e+00 -strength  4.035037e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  1.172197e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  1.172197e+00
Quadrupole -name "LN3_QD_BP_V02"    -length  4.000000e-02 -e0  1.172197e+00 -strength  4.035037e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.172197e+00
Drift      -name "LN3_DR_BP_V3"     -length  1.207583e+00 -e0  1.172197e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.172197e+00
Quadrupole -name "LN3_QD_BP_V03"    -length  4.000000e-02 -e0  1.172197e+00 -strength -3.851829e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  1.172197e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  1.172197e+00
Quadrupole -name "LN3_QD_BP_V03"    -length  4.000000e-02 -e0  1.172197e+00 -strength -3.851829e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.172197e+00
Drift      -name "LN3_DR_BP_V4"     -length  2.000000e-01 -e0  1.172197e+00
Drift      -name "LN3_DR_BP_0"      -length  2.000000e-01 -e0  1.172197e+00
Drift      -name "LN3_DR_BP_BL"     -length  5.000000e-02 -e0  1.172197e+00
Drift      -name "LN3_DR_SCA_ED"    -length  8.325105e-02 -e0  1.172197e+00
CrabCavity -name "LN3_SCA0"         -length  5.334979e-01 -voltage  0.000000e+00 -phase  9.000000e+01 -frequency  2.997000e+00
SetReferenceEnergy   1.172197e+00
Drift      -name "LN3_DR_SCA_ED"    -length  8.325105e-02 -e0  1.172192e+00
Drift      -name "LN3_DR_BP_BL"     -length  5.000000e-02 -e0  1.172192e+00
Drift      -name "LN3_DR_BP_0"      -length  2.000000e-01 -e0  1.172192e+00
Drift      -name "LN3_DR_BP"        -length  4.000000e-01 -e0  1.172192e+00
Drift      -name "LN3_DR_BP_VS"     -length  1.000000e-01 -e0  1.172192e+00
Drift      -name "LN3_DR_BP"        -length  4.000000e-01 -e0  1.172192e+00
Drift      -name "LN3_BP_WA_OU2"    -length  0.000000e+00 -e0  1.172192e+00
Drift      -name "HBP_CNT0"         -length  0.000000e+00 -e0  1.172192e+00
Sbend      -name "HBP_DP_SEPM1"     -length  3.202601e-01 -e0  1.172085e+00 -angle  3.490658e-02 -E1  0.000000e+00 -E2  0.000000e+00 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  3.141593e+00  -six_dim 1 -csr 1 
Sbend      -name "HBP_DP_SEPM1"     -length  3.202601e-01 -e0  1.171923e+00 -angle  3.490658e-02 -E1  0.000000e+00 -E2  0.000000e+00 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  3.141593e+00  -six_dim 1 -csr 1 
Drift      -name "HBP_DR_BP_C"      -length  3.000000e-01 -e0  1.171842e+00
Drift      -name "HBP_DR_BP"        -length  3.000000e-01 -e0  1.171842e+00
Drift      -name "HBP_DR_BP"        -length  3.000000e-01 -e0  1.171842e+00
Drift      -name "HBP_DR_BP"        -length  3.000000e-01 -e0  1.171842e+00
Drift      -name "HBP_DR_V1"        -length  2.629146e-01 -e0  1.171842e+00
Drift      -name "HBP_CNT0"         -length  0.000000e+00 -e0  1.171842e+00
Drift      -name "HBP_DR_QD_ED"     -length  4.250000e-02 -e0  1.171842e+00
Quadrupole -name "HBP_QD_V01"       -length  4.000000e-02 -e0  1.171842e+00 -strength  3.347015e-01
Dipole     -name "HBP_COR"          -length  0.000000e+00 -e0  1.171842e+00
Bpm        -name "HBP_BPM"          -length  0.000000e+00 -e0  1.171842e+00
Quadrupole -name "HBP_QD_V01"       -length  4.000000e-02 -e0  1.171842e+00 -strength  3.347015e-01
Drift      -name "HBP_DR_QD_ED"     -length  4.250000e-02 -e0  1.171842e+00
Drift      -name "HBP_DR_V2"        -length  2.075000e-01 -e0  1.171842e+00
Drift      -name "HBP_DR_V2"        -length  2.075000e-01 -e0  1.171842e+00
Drift      -name "HBP_CNT0"         -length  0.000000e+00 -e0  1.171842e+00
Drift      -name "HBP_DR_QD_ED"     -length  4.250000e-02 -e0  1.171842e+00
Quadrupole -name "HBP_QD_V01"       -length  4.000000e-02 -e0  1.171842e+00 -strength  3.347015e-01
Dipole     -name "HBP_COR"          -length  0.000000e+00 -e0  1.171842e+00
Bpm        -name "HBP_BPM"          -length  0.000000e+00 -e0  1.171842e+00
Quadrupole -name "HBP_QD_V01"       -length  4.000000e-02 -e0  1.171842e+00 -strength  3.347015e-01
Drift      -name "HBP_DR_QD_ED"     -length  4.250000e-02 -e0  1.171842e+00
Drift      -name "HBP_DR_V1"        -length  2.629146e-01 -e0  1.171842e+00
Drift      -name "HBP_DR_BP"        -length  3.000000e-01 -e0  1.171842e+00
Drift      -name "HBP_DR_BP"        -length  3.000000e-01 -e0  1.171842e+00
Drift      -name "HBP_DR_BP"        -length  3.000000e-01 -e0  1.171842e+00
Drift      -name "HBP_DR_BP"        -length  3.000000e-01 -e0  1.171842e+00
Drift      -name "HBP_CNT0"         -length  0.000000e+00 -e0  1.171842e+00
Sbend      -name "HBP_DP_SEPM1"     -length  3.202601e-01 -e0  1.171737e+00 -angle  3.490658e-02 -E1  0.000000e+00 -E2  0.000000e+00 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  3.141593e+00  -six_dim 1 -csr 1 
Sbend      -name "HBP_DP_SEPM1"     -length  3.202601e-01 -e0  1.171580e+00 -angle  3.490658e-02 -E1  0.000000e+00 -E2  0.000000e+00 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  3.141593e+00  -six_dim 1 -csr 1 
Drift      -name "HBP_WA_OU1A"      -length  0.000000e+00 -e0  1.171580e+00
Drift      -name "HBP_DR_BP_C"      -length  3.000000e-01 -e0  1.171504e+00
Drift      -name "HBP_DR_V3"        -length  1.194347e-02 -e0  1.171504e+00
Drift      -name "HBP_DR_QD_ED"     -length  4.250000e-02 -e0  1.171504e+00
Quadrupole -name "HBP_QD_V02"       -length  4.000000e-02 -e0  1.171504e+00 -strength -2.900394e-01
Dipole     -name "HBP_COR"          -length  0.000000e+00 -e0  1.171504e+00
Bpm        -name "HBP_BPM"          -length  0.000000e+00 -e0  1.171504e+00
Quadrupole -name "HBP_QD_V02"       -length  4.000000e-02 -e0  1.171504e+00 -strength -2.900394e-01
Drift      -name "HBP_DR_QD_ED"     -length  4.250000e-02 -e0  1.171504e+00
Drift      -name "HBP_DR_V4"        -length  1.379807e+00 -e0  1.171504e+00
Drift      -name "HBP_DR_QD_ED"     -length  4.250000e-02 -e0  1.171504e+00
Quadrupole -name "HBP_QD_V03"       -length  4.000000e-02 -e0  1.171504e+00 -strength  3.261788e-01
Dipole     -name "HBP_COR"          -length  0.000000e+00 -e0  1.171504e+00
Bpm        -name "HBP_BPM"          -length  0.000000e+00 -e0  1.171504e+00
Quadrupole -name "HBP_QD_V03"       -length  4.000000e-02 -e0  1.171504e+00 -strength  3.261788e-01
Drift      -name "HBP_DR_QD_ED"     -length  4.250000e-02 -e0  1.171504e+00
Drift      -name "HBP_DR_V5"        -length  4.422050e+00 -e0  1.171504e+00
Drift      -name "HBP_DR_QD_ED"     -length  4.250000e-02 -e0  1.171504e+00
Quadrupole -name "HBP_QD_V04"       -length  4.000000e-02 -e0  1.171504e+00 -strength -1.549631e-01
Dipole     -name "HBP_COR"          -length  0.000000e+00 -e0  1.171504e+00
Bpm        -name "HBP_BPM"          -length  0.000000e+00 -e0  1.171504e+00
Quadrupole -name "HBP_QD_V04"       -length  4.000000e-02 -e0  1.171504e+00 -strength -1.549631e-01
Drift      -name "HBP_DR_QD_ED"     -length  4.250000e-02 -e0  1.171504e+00
Drift      -name "HBP_DR_V5"        -length  4.422050e+00 -e0  1.171504e+00
Drift      -name "HBP_DR_QD_ED"     -length  4.250000e-02 -e0  1.171504e+00
Quadrupole -name "HBP_QD_V03"       -length  4.000000e-02 -e0  1.171504e+00 -strength  3.261788e-01
Dipole     -name "HBP_COR"          -length  0.000000e+00 -e0  1.171504e+00
Bpm        -name "HBP_BPM"          -length  0.000000e+00 -e0  1.171504e+00
Quadrupole -name "HBP_QD_V03"       -length  4.000000e-02 -e0  1.171504e+00 -strength  3.261788e-01
Drift      -name "HBP_DR_QD_ED"     -length  4.250000e-02 -e0  1.171504e+00
Drift      -name "HBP_DR_V4"        -length  1.379807e+00 -e0  1.171504e+00
Drift      -name "HBP_DR_QD_ED"     -length  4.250000e-02 -e0  1.171504e+00
Quadrupole -name "HBP_QD_V02"       -length  4.000000e-02 -e0  1.171504e+00 -strength -2.900394e-01
Dipole     -name "HBP_COR"          -length  0.000000e+00 -e0  1.171504e+00
Bpm        -name "HBP_BPM"          -length  0.000000e+00 -e0  1.171504e+00
Quadrupole -name "HBP_QD_V02"       -length  4.000000e-02 -e0  1.171504e+00 -strength -2.900394e-01
Drift      -name "HBP_DR_QD_ED"     -length  4.250000e-02 -e0  1.171504e+00
Drift      -name "HBP_DR_V3"        -length  1.194347e-02 -e0  1.171504e+00
Drift      -name "HBP_DR_BP"        -length  3.000000e-01 -e0  1.171504e+00
Drift      -name "HBP_WA_OU2A"      -length  0.000000e+00 -e0  1.171504e+00
Sbend      -name "HBP_DP_SEPM2"     -length  3.202601e-01 -e0  1.171349e+00 -angle  3.490658e-02 -E1  0.000000e+00 -E2  0.000000e+00 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  0.000000e+00  -six_dim 1 -csr 1 
Sbend      -name "HBP_DP_SEPM2"     -length  3.202601e-01 -e0  1.171186e+00 -angle  3.490658e-02 -E1  0.000000e+00 -E2  0.000000e+00 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  0.000000e+00  -six_dim 1 -csr 1 
Drift      -name "HBP_DR_BP_C"      -length  3.000000e-01 -e0  1.171105e+00
Drift      -name "HBP_DR_BP"        -length  3.000000e-01 -e0  1.171105e+00
Drift      -name "HBP_DR_BP"        -length  3.000000e-01 -e0  1.171105e+00
Drift      -name "HBP_DR_BP"        -length  3.000000e-01 -e0  1.171105e+00
Drift      -name "HBP_DR_V1"        -length  2.629146e-01 -e0  1.171105e+00
Drift      -name "HBP_CNT0"         -length  0.000000e+00 -e0  1.171105e+00
Drift      -name "HBP_DR_QD_ED"     -length  4.250000e-02 -e0  1.171105e+00
Quadrupole -name "HBP_QD_V01"       -length  4.000000e-02 -e0  1.171105e+00 -strength  3.344911e-01
Dipole     -name "HBP_COR"          -length  0.000000e+00 -e0  1.171105e+00
Bpm        -name "HBP_BPM"          -length  0.000000e+00 -e0  1.171105e+00
Quadrupole -name "HBP_QD_V01"       -length  4.000000e-02 -e0  1.171105e+00 -strength  3.344911e-01
Drift      -name "HBP_DR_QD_ED"     -length  4.250000e-02 -e0  1.171105e+00
Drift      -name "HBP_DR_V2"        -length  2.075000e-01 -e0  1.171105e+00
Drift      -name "HBP_DR_V2"        -length  2.075000e-01 -e0  1.171105e+00
Drift      -name "HBP_CNT0"         -length  0.000000e+00 -e0  1.171105e+00
Drift      -name "HBP_DR_QD_ED"     -length  4.250000e-02 -e0  1.171105e+00
Quadrupole -name "HBP_QD_V01"       -length  4.000000e-02 -e0  1.171105e+00 -strength  3.344911e-01
Dipole     -name "HBP_COR"          -length  0.000000e+00 -e0  1.171105e+00
Bpm        -name "HBP_BPM"          -length  0.000000e+00 -e0  1.171105e+00
Quadrupole -name "HBP_QD_V01"       -length  4.000000e-02 -e0  1.171105e+00 -strength  3.344911e-01
Drift      -name "HBP_DR_QD_ED"     -length  4.250000e-02 -e0  1.171105e+00
Drift      -name "HBP_DR_V1"        -length  2.629146e-01 -e0  1.171105e+00
Drift      -name "HBP_DR_BP"        -length  3.000000e-01 -e0  1.171105e+00
Drift      -name "HBP_DR_BP"        -length  3.000000e-01 -e0  1.171105e+00
Drift      -name "HBP_DR_BP"        -length  3.000000e-01 -e0  1.171105e+00
Drift      -name "HBP_DR_BP"        -length  3.000000e-01 -e0  1.171105e+00
Sbend      -name "HBP_DP_SEPM2"     -length  3.202601e-01 -e0  1.170944e+00 -angle  3.490658e-02 -E1  0.000000e+00 -E2  0.000000e+00 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  0.000000e+00  -six_dim 1 -csr 1 
Sbend      -name "HBP_DP_SEPM2"     -length  3.202601e-01 -e0  1.170797e+00 -angle  3.490658e-02 -E1  0.000000e+00 -E2  0.000000e+00 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  0.000000e+00  -six_dim 1 -csr 1 
Drift      -name "HBP_WA_OU3A"      -length  0.000000e+00 -e0  1.170797e+00
Drift      -name "HXR_DR_V1"        -length  1.000038e-01 -e0  1.170797e+00
Drift      -name "HXR_DR_QD_ED"     -length  5.200000e-02 -e0  1.170797e+00
Quadrupole -name "HXR_QD_V01"       -length  1.300000e-02 -e0  1.170797e+00 -strength -4.364514e-01
Dipole     -name "HXR_COR"          -length  0.000000e+00 -e0  1.170797e+00
Bpm        -name "HXR_BPM"          -length  0.000000e+00 -e0  1.170797e+00
Quadrupole -name "HXR_QD_V01"       -length  1.300000e-02 -e0  1.170797e+00 -strength -4.364514e-01
Drift      -name "HXR_DR_QD_ED"     -length  5.200000e-02 -e0  1.170797e+00
Drift      -name "HXR_DR_V2"        -length  1.397334e+00 -e0  1.170797e+00
Drift      -name "HXR_DR_QD_ED"     -length  5.200000e-02 -e0  1.170797e+00
Quadrupole -name "HXR_QD_V02"       -length  1.300000e-02 -e0  1.170797e+00 -strength  4.081886e-01
Dipole     -name "HXR_COR"          -length  0.000000e+00 -e0  1.170797e+00
Bpm        -name "HXR_BPM"          -length  0.000000e+00 -e0  1.170797e+00
Quadrupole -name "HXR_QD_V02"       -length  1.300000e-02 -e0  1.170797e+00 -strength  4.081886e-01
Drift      -name "HXR_DR_QD_ED"     -length  5.200000e-02 -e0  1.170797e+00
Drift      -name "HXR_DR_V3"        -length  2.772292e+00 -e0  1.170797e+00
Drift      -name "HXR_DR_QD_ED"     -length  5.200000e-02 -e0  1.170797e+00
Quadrupole -name "HXR_QD_V03"       -length  1.300000e-02 -e0  1.170797e+00 -strength -3.300614e-01
Dipole     -name "HXR_COR"          -length  0.000000e+00 -e0  1.170797e+00
Bpm        -name "HXR_BPM"          -length  0.000000e+00 -e0  1.170797e+00
Quadrupole -name "HXR_QD_V03"       -length  1.300000e-02 -e0  1.170797e+00 -strength -3.300614e-01
Drift      -name "HXR_DR_QD_ED"     -length  5.200000e-02 -e0  1.170797e+00
Drift      -name "HXR_DR_V4"        -length  1.007280e+00 -e0  1.170797e+00
Drift      -name "HXR_DR_CT"        -length  1.000000e-01 -e0  1.170797e+00
Drift      -name "HXR_WA_M_OU2"     -length  0.000000e+00 -e0  1.170797e+00
Drift      -name "HXR_DR_QD_ED"     -length  5.200000e-02 -e0  1.170797e+00
Quadrupole -name "HXR_QD_FH"        -length  1.300000e-02 -e0  1.170797e+00 -strength  2.283054e-01
Dipole     -name "HXR_COR"          -length  0.000000e+00 -e0  1.170797e+00
Bpm        -name "HXR_BPM"          -length  0.000000e+00 -e0  1.170797e+00
Quadrupole -name "HXR_QD_FH"        -length  1.300000e-02 -e0  1.170797e+00 -strength  2.283054e-01
Drift      -name "HXR_DR_QD_ED"     -length  5.200000e-02 -e0  1.170797e+00
Drift      -name "HXR_DR_WIG_ED"    -length  1.950000e-01 -e0  1.170797e+00
Drift      -name "HXR_WIG_0"        -length  1.768000e+00 -e0  1.170797e+00
Drift      -name "HXR_DR_WIG_ED"    -length  1.950000e-01 -e0  1.170797e+00
Drift      -name "HXR_DR_QD_ED"     -length  5.200000e-02 -e0  1.170797e+00
Quadrupole -name "HXR_QD_DH"        -length  1.300000e-02 -e0  1.170797e+00 -strength -2.283054e-01
Dipole     -name "HXR_COR"          -length  0.000000e+00 -e0  1.170797e+00
Bpm        -name "HXR_BPM"          -length  0.000000e+00 -e0  1.170797e+00
Quadrupole -name "HXR_QD_DH"        -length  1.300000e-02 -e0  1.170797e+00 -strength -2.283054e-01
Drift      -name "HXR_DR_QD_ED"     -length  5.200000e-02 -e0  1.170797e+00
Drift      -name "HXR_DR_WIG_ED"    -length  1.950000e-01 -e0  1.170797e+00
Drift      -name "HXR_WIG_0"        -length  1.768000e+00 -e0  1.170797e+00
Drift      -name "HXR_DR_WIG_ED"    -length  1.950000e-01 -e0  1.170797e+00
Drift      -name "HXR_DR_QD_ED"     -length  5.200000e-02 -e0  1.170797e+00
Quadrupole -name "HXR_QD_FH"        -length  1.300000e-02 -e0  1.170797e+00 -strength  2.283054e-01
Drift      -name "HXR_WA_OU2"       -length  0.000000e+00 -e0  1.170797e+00
