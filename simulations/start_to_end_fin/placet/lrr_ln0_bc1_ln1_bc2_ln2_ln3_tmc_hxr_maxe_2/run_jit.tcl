

set beamlinename [exec   basename [pwd] ]

set outfolder jitter_out 

if {[file exist $outfolder]} {
	exec rm -rf $outfolder
	exec mkdir -p $outfolder
} else {
	exec mkdir -p $outfolder
}

cd   $outfolder

source ../../common_files/load_scripts.tcl

set script_dir "../files"


set beam0_indist [lindex [file split [glob ../files/*out.dat]] end end]
set latfile [lindex [file split [glob ../files/*lattice.tcl]] end end]
puts $beam0_indist
puts $latfile

set charge_pc 75e-12
set charge0 [expr $charge_pc/1.6e-19]

array set BeamDefine00  [bunch_parameters $script_dir/$beam0_indist]
array set BeamDefine00 "name beam0 n_slice 201 charge $charge0"
set BeamDefine00(n_macro) [expr int($BeamDefine00(npart)/$BeamDefine00(n_slice))]
puts [array get BeamDefine00]
exec cp $script_dir/$beam0_indist beam00_in.dat



set jit_res {}
set nmachine 501

for {set i 0} {$i<$nmachine} {incr i} {
    
    puts "\nMACHINE: $i/[expr $nmachine-1]\n"

    
    if {$i==0} {
        set charge_err     [expr 0.0] ;# 2.5%
        set bnch_time_err  [expr 0.0] ;# fs
        set ln0c_gr_err    [expr 0.0] ;# 0.05%
        set ln0c_ph_err    [expr 0.0] ;# deg
        set ln0k_gr_err    [expr 0.0] ;# 0.05%
        set ln0k_ph_err    [expr 0.0] ;# deg
        set ln1x_gr_err    [expr 0.0] ;# 0.05%
        set ln1x_ph_err    [expr 0.0] ;# deg
        set ln2x_gr_err    [expr 0.0] ;# 0.05%
        set ln2x_ph_err    [expr 0.0] ;# deg
        set ln3x_gr_err    [expr 0.0] ;# 0.05%
        set ln3x_ph_err    [expr 0.0] ;# deg        
    } else {
        set charge_err     [expr 1.0/100.]  ;# 2.5%
        set bnch_time_err  [expr 30.0]      ;# fs
        set ln0c_gr_err    [expr 0.02/100.] ;# 0.02%
        set ln0c_ph_err    [expr 0.04]      ;# deg
        set ln0k_gr_err    [expr 0.02/100.] ;# 0.05%
        set ln0k_ph_err    [expr 0.16]      ;# deg
        set ln1x_gr_err    [expr 0.02/100.] ;# 0.05%
        set ln1x_ph_err    [expr 0.08]      ;# deg
        set ln2x_gr_err    [expr 0.02/100.] ;# 0.05%
        set ln2x_ph_err    [expr 0.08]      ;# deg
        set ln3x_gr_err    [expr 0.02/100.] ;# 0.05%
        set ln3x_ph_err    [expr 0.08]      ;# deg        
    }
    
    set ch_err [expr   (1.0+[random]*$charge_err)]
    set tm_err [expr   [random]*$bnch_time_err*1e-15]
    set beam_name beam$i
    set beamin_save_name $beam_name\_in.dat
    set beamou_save_name $beam_name\_ou.dat
    set beamlinename   lrr_hxr1_maxe$i
    
    Octave { shiftbeam('beam00_in.dat',$tm_err,'$beamin_save_name') }
       
    array set BeamDefine_${i} [array get BeamDefine00]
    set BeamDefine_${i}(charge) [expr $charge0*$ch_err]
    set charge_pc_i             [expr $charge_pc*$ch_err]
    
    set BeamDefine_${i}(filename) $beamin_save_name
    set BeamDefine_${i}(name) $beam_name
#     puts "\n "
#     puts [array get BeamDefine_${i}]    
    
    
# #     beamline definition
    BeamlineNew
    Girder 
    source $script_dir/$latfile
    BeamlineSet -name $beamlinename
# #     define beam
    make_particle_beam_read BeamDefine_${i} $beamin_save_name

    
    Octave {
        # apply wakes for all structures
        LN0XDCs = placet_get_name_number_list("$beamlinename", "LN0_XCA0_DI");
        placet_element_set_attribute("$beamlinename", LN0XDCs, "short_range_wake",  "Xdband_SR_W");
        placet_element_set_attribute("$beamlinename", LN0XDCs, "lambda", $xdband_str(lambda));
        
        LN0CCs = placet_get_name_number_list("$beamlinename", "LN0_CCA0");
        placet_element_set_attribute("$beamlinename", LN0CCs, "short_range_wake",  "Cband_SR_W");
        placet_element_set_attribute("$beamlinename", LN0CCs, "lambda", $cband_str(lambda));
        
        LN0KCs = placet_get_name_number_list("$beamlinename", "LN0_KCA0");
        placet_element_set_attribute("$beamlinename", LN0KCs, "short_range_wake",  "Kband_SR_W");
        placet_element_set_attribute("$beamlinename", LN0KCs, "lambda", $kband_str(lambda));
    
        BC1XCs = placet_get_name_number_list("$beamlinename", "BC1_XCA0");
        placet_element_set_attribute("$beamlinename", BC1XCs, "short_range_wake",  "Xdband_SR_W");
        placet_element_set_attribute("$beamlinename", BC1XCs, "lambda", $xdband_str(lambda));
        
        LN1XCs = placet_get_name_number_list("$beamlinename", "LN1_XCA0");
        placet_element_set_attribute("$beamlinename", LN1XCs, "short_range_wake",  "Xband_SR_W");
        placet_element_set_attribute("$beamlinename", LN1XCs, "lambda", $xband_str(lambda));
        
        LN2XCs = placet_get_name_number_list("$beamlinename", "LN2_XCA0");
        placet_element_set_attribute("$beamlinename", LN2XCs, "short_range_wake",  "Xband_SR_W");
        placet_element_set_attribute("$beamlinename", LN2XCs, "lambda", $xband_str(lambda));
        
        LN2XDCs = placet_get_name_number_list("$beamlinename", "LN2_XCA0_DI");
        placet_element_set_attribute("$beamlinename", LN2XDCs, "short_range_wake",  "Xdband_SR_W");
        placet_element_set_attribute("$beamlinename", LN2XDCs, "lambda", $xdband_str(lambda));    
        
        LN2SCs = placet_get_name_number_list("$beamlinename", "LN2_SCA0");
        placet_element_set_attribute("$beamlinename", LN2SCs, "short_range_wake",  "Sdband_SR_W");
        placet_element_set_attribute("$beamlinename", LN2SCs, "lambda", $sdband_str(lambda));
        
        LN3XCs = placet_get_name_number_list("$beamlinename", "LN3_XCA0");
        placet_element_set_attribute("$beamlinename", LN3XCs, "short_range_wake",  "Xband_SR_W");
        placet_element_set_attribute("$beamlinename", LN3XCs, "lambda", $xband_str(lambda));
        
        LN3SCs = placet_get_name_number_list("$beamlinename", "LN3_SCA0");
        placet_element_set_attribute("$beamlinename", LN3SCs, "short_range_wake",  "Sdband_SR_W");
        placet_element_set_attribute("$beamlinename", LN3SCs, "lambda", $sdband_str(lambda));
        
        
        #  6d tracking in bunch compression
        ALLCAVITs = placet_get_number_list("$beamlinename", "cavity");
        placet_element_set_attribute("$beamlinename", ALLCAVITs, "six_dim", true);
        
        SIs = placet_get_number_list("$beamlinename", "sbend");
        placet_element_set_attribute("$beamlinename", SIs, "six_dim", true);
        
        DRFTSs = placet_get_number_list("$beamlinename", "drift");
        placet_element_set_attribute("$beamlinename", DRFTSs, "six_dim", true);
        
        QDs = placet_get_number_list("$beamlinename", "quadrupole");
        placet_element_set_attribute("$beamlinename", QDs, "six_dim", true);    

        placet_element_set_attribute("$beamlinename", SIs, "csr", true);
        placet_element_set_attribute("$beamlinename", SIs, "csr_charge", $charge_pc_i);
        placet_element_set_attribute("$beamlinename", SIs, "csr_nbins", 50);
        placet_element_set_attribute("$beamlinename", SIs, "csr_nsectors", 10);
        placet_element_set_attribute("$beamlinename", SIs, "csr_filterorder", 1);
        placet_element_set_attribute("$beamlinename", SIs, "csr_nhalffilter", 2);
    }
    

    Octave {
        # group structures according to module
        # LN0 C-Band
        LN0CCsi = LN0CCs(1:2:end)(:);
        LN0CCsj = LN0CCs(2:2:end)(:);
        
        LN0CCsi_ph0= placet_element_get_attribute("$beamlinename", LN0CCsi, "phase");
        LN0CCsi_gr0= placet_element_get_attribute("$beamlinename", LN0CCsi, "gradient");
        
        LN0CCsi_ph = randn(size(LN0CCsi_ph0)) *$ln0c_ph_err +  LN0CCsi_ph0;
        placet_element_set_attribute("$beamlinename", LN0CCsi, "phase", LN0CCsi_ph);
        placet_element_set_attribute("$beamlinename", LN0CCsj, "phase", LN0CCsi_ph);
                
        LN0CCsi_gr= (randn(size(LN0CCsi_gr0))*$ln0c_gr_err + 1).* LN0CCsi_gr0;
        placet_element_set_attribute("$beamlinename", LN0CCsi, "gradient", LN0CCsi_gr);
        placet_element_set_attribute("$beamlinename", LN0CCsj, "gradient", LN0CCsi_gr);    
        
        # LN0 K-Band          
        LN0KCs_ph0= placet_element_get_attribute("$beamlinename", LN0KCs, "phase");
        LN0KCs_gr0= placet_element_get_attribute("$beamlinename", LN0KCs, "gradient");
      
        LN0KCs_ph = randn(size(LN0KCs_ph0))*$ln0k_ph_err +  LN0KCs_ph0;
        placet_element_set_attribute("$beamlinename", LN0KCs, "phase", LN0KCs_ph);
        
        LN0KCs_gr= (randn(size(LN0KCs_gr0))*$ln0k_gr_err + 1).* LN0KCs_gr0;
        placet_element_set_attribute("$beamlinename", LN0KCs, "gradient", LN0KCs_gr);
        
        # LN1 X-Band  
        LN1XCsi = LN1XCs(1:4:end)(:);
        LN1XCsj = LN1XCs(2:4:end)(:);
        LN1XCsk = LN1XCs(3:4:end)(:);
        LN1XCsl = LN1XCs(4:4:end)(:);
        LN1XCsi_ph0= placet_element_get_attribute("$beamlinename", LN1XCsi, "phase");
        LN1XCsi_gr0= placet_element_get_attribute("$beamlinename", LN1XCsi, "gradient");
        
        LN1XCsi_ph = randn(size(LN1XCsi_ph0)) *$ln1x_ph_err +  LN1XCsi_ph0;
        placet_element_set_attribute("$beamlinename", LN1XCsi, "phase", LN1XCsi_ph);
        placet_element_set_attribute("$beamlinename", LN1XCsj, "phase", LN1XCsi_ph);
        placet_element_set_attribute("$beamlinename", LN1XCsk, "phase", LN1XCsi_ph);
        placet_element_set_attribute("$beamlinename", LN1XCsl, "phase", LN1XCsi_ph);

        LN1XCsi_gr= (randn(size(LN1XCsi_gr0))*$ln1x_gr_err + 1).* LN1XCsi_gr0;
        placet_element_set_attribute("$beamlinename", LN1XCsi, "gradient", LN1XCsi_gr);
        placet_element_set_attribute("$beamlinename", LN1XCsj, "gradient", LN1XCsi_gr);
        placet_element_set_attribute("$beamlinename", LN1XCsk, "gradient", LN1XCsi_gr);
        placet_element_set_attribute("$beamlinename", LN1XCsl, "gradient", LN1XCsi_gr);
        
        # LN2 X-Band  
        LN2XCsi = LN2XCs(1:4:end)(:);
        LN2XCsj = LN2XCs(2:4:end)(:);
        LN2XCsk = LN2XCs(3:4:end)(:);
        LN2XCsl = LN2XCs(4:4:end)(:);
        LN2XCsi_ph0= placet_element_get_attribute("$beamlinename", LN2XCsi, "phase");
        LN2XCsi_gr0= placet_element_get_attribute("$beamlinename", LN2XCsi, "gradient");
        
        LN2XCsi_ph = randn(size(LN2XCsi_ph0)) *$ln2x_ph_err +  LN2XCsi_ph0;
        placet_element_set_attribute("$beamlinename", LN2XCsi, "phase", LN2XCsi_ph);
        placet_element_set_attribute("$beamlinename", LN2XCsj, "phase", LN2XCsi_ph);
        placet_element_set_attribute("$beamlinename", LN2XCsk, "phase", LN2XCsi_ph);
        placet_element_set_attribute("$beamlinename", LN2XCsl, "phase", LN2XCsi_ph);

        LN2XCsi_gr= (randn(size(LN2XCsi_gr0))*$ln2x_gr_err + 1).* LN2XCsi_gr0;
        placet_element_set_attribute("$beamlinename", LN2XCsi, "gradient", LN2XCsi_gr);
        placet_element_set_attribute("$beamlinename", LN2XCsj, "gradient", LN2XCsi_gr);
        placet_element_set_attribute("$beamlinename", LN2XCsk, "gradient", LN2XCsi_gr);
        placet_element_set_attribute("$beamlinename", LN2XCsl, "gradient", LN2XCsi_gr);      

        # LN3 X-Band  
        LN3XCsi = LN3XCs(1:4:end)(:);
        LN3XCsj = LN3XCs(2:4:end)(:);
        LN3XCsk = LN3XCs(3:4:end)(:);
        LN3XCsl = LN3XCs(4:4:end)(:);
        LN3XCsi_ph0= placet_element_get_attribute("$beamlinename", LN3XCsi, "phase");
        LN3XCsi_gr0= placet_element_get_attribute("$beamlinename", LN3XCsi, "gradient");
        
        LN3XCsi_ph = randn(size(LN3XCsi_ph0)) *$ln3x_ph_err +  LN3XCsi_ph0;
        placet_element_set_attribute("$beamlinename", LN3XCsi, "phase", LN3XCsi_ph);
        placet_element_set_attribute("$beamlinename", LN3XCsj, "phase", LN3XCsi_ph);
        placet_element_set_attribute("$beamlinename", LN3XCsk, "phase", LN3XCsi_ph);
        placet_element_set_attribute("$beamlinename", LN3XCsl, "phase", LN3XCsi_ph);

        LN3XCsi_gr= (randn(size(LN3XCsi_gr0))*$ln3x_gr_err + 1).* LN3XCsi_gr0;
        placet_element_set_attribute("$beamlinename", LN3XCsi, "gradient", LN3XCsi_gr);
        placet_element_set_attribute("$beamlinename", LN3XCsj, "gradient", LN3XCsi_gr);
        placet_element_set_attribute("$beamlinename", LN3XCsk, "gradient", LN3XCsi_gr);
        placet_element_set_attribute("$beamlinename", LN3XCsl, "gradient", LN3XCsi_gr);          
        

        LN_PHs= placet_element_get_attribute("$beamlinename", ALLCAVITs, "phase");
        LN_GRs= placet_element_get_attribute("$beamlinename", ALLCAVITs, "gradient");
        
#         A=[LN_PHs LN_GRs]        
        ln_ph_av=mean(LN_PHs);
        ln_gr_av=mean(LN_GRs);
        Tcl_SetVar("ln_ph_av", ln_ph_av);
        Tcl_SetVar("ln_gr_av", ln_gr_av);
    }
    
    Octave {
         
        [emitt0,B01] = placet_test_no_correction("$beamlinename", "$beam_name", "None", "%s %E %dE %ex %ey %sz");
        
        save('-text', '-ascii','$beamou_save_name','B01');
        
        beamparsf=calc_beam_pars(B01);
        Tcl_SetVar("beamparsf", beamparsf);
    }
    exec gzip $beamou_save_name
    exec rm $beamin_save_name
    set tmp "$i $ch_err $tm_err $ln_ph_av $ln_gr_av"
    foreach a [lindex [lindex $beamparsf 0] 0] {
        lappend tmp $a
    }
    lappend jit_res "$tmp"
    BeamlineDelete -name $beamlinename
}

set fo [open "jitter_results.dat" w]
puts $fo "# iter charge tmime phase gradient mean_E x0 y0 z0  sw sx sy sz dE emt_xn emt_yn emt_xcn emt_ycn"

foreach aa $jit_res {
    set k  [lindex $aa 0]
    set ch [lindex $aa 1]
    set tm [lindex $aa 2]
    set ph [lindex $aa 3]
    set gr [lindex $aa 4] 
    set w0 [lindex $aa 5]
    set x0 [lindex $aa 6]
    set y0 [lindex $aa 7]
    set z0 [lindex $aa 8] 
    set sw [lindex $aa 9]
    set sx [lindex $aa 10]
    set sy [lindex $aa 11]
    set sz [lindex $aa 12] 
    set dE [lindex $aa 13]
    set exn [lindex $aa 14]
    set eyn [lindex $aa 15]
    set excn [lindex $aa 16]
    set eycn [lindex $aa 17] 
    
    puts $fo [format "%3d %13.6e %13.6e %13.6e %13.6e %13.6e %13.6e %13.6e %13.6e %13.6e %13.6e %13.6e %13.6e %13.6e %13.6e %13.6e %13.6e %13.6e"  $k $ch $tm $ph $gr $w0 $x0 $y0 $z0  $sw $sx $sy $sz $dE $exn $eyn $excn $eycn]  
    
}
close $fo
exit

