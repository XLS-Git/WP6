
#!/bin/sh  
# \
exec tclsh "$0" "$@"
# set names [gets stdin]

set infile      [lindex $argv 0]
set outfile      [lindex $argv 1]

if {[file exist tmp.0]} {
	exec rm -rf tmp.0
} 

if {[file exist tmp.1]} {
	exec rm -rf tmp.1
}

puts $infile

cd $infile

set dfs [glob *_dfs_*]
set wfs [glob *_wfs_*]
set smp [glob *_simple_*]
puts $dfs
puts $wfs
puts $smp

set foname [file rootname $outfile]
# 
set pltfile  $foname\_plt.tcl

puts "$outfile $pltfile"

set fdfs [open $dfs]
set fwfs [open $wfs]
set fsmp [open $smp]

set emax -1e10;
set emin  1e10
set smax -1e10

while { [gets $fdfs ldfs]>=0 && [gets $fwfs lwfs]>=0 && [gets $fsmp lsmp]>=0 } {
    
    if {[lindex $ldfs 0] != "#" } {
        set s [lindex $ldfs 0]
        set el "[lindex $lsmp 3] [lindex $ldfs 3] [lindex $lwfs 3]"
#         puts $el
        set max -1e-10
        set min  1e-10
        foreach k $el { 
            if { $k > $max } { set max $k}
            if { $k < $min } { set mmin $k}
        }
        
        if { $max > $emax } { set emax $max}
        if { $min < $emin } { set emin $min}
        if { $s > $smax } { set smax $s}
    }
}
    set smax [expr round($smax*1.05)]
    set emax [expr round(10*$emax*1.2)/10.]
    set emin [expr round(10*$emin*0.5)/10.]
    puts "$smax $emin $emax"
    



set fl [open $pltfile w]


puts $fl "

set term pdfcairo size 18cm,10cm enhanced font 'Times,20'
set output 'emt_tmp.pdf' 
set multiplot
set lmargin at screen 0.17

# set y2label '{/Symbol s}_{z} ({/Symbol m}m)' offset -1,0
set y2label '{/Symbol s}_{z} (mm)' offset -2,0
set xlabel 'Distance (m)' offset 0,0.8
set ylabel ' {/Symbol e}_{y} (mm.mrad)' offset 2,0
set key font 'Times,16' at 110,0.3
set xrange \[0:275\]
set ytics nomirror
set y2tics
set tics out
set autoscale  y
set yrange \[0.12:0.32\]
set ytics 0.12,0.02,0.32
# set format y '%.2f'
set autoscale y2
set grid lt 0 
pl '$smp' u (\$1*1e0):(\$6*1e-3) w l lw 2 lc 'violet' axes x1y2 ti '{/Symbol s}_{z}', \
   '$smp' u (\$1*1e0):(\$5*1e-1) w l lw 3 lc 'red'    ti 'One-One', \
   '$dfs' u (\$1*1e0):(\$5*1e-1) w l lw 3 lc 'green'   ti 'DFS', \
   '$wfs' u (\$1*1e0):(\$5*1e-1) w l lw 3 lc 'blue'  ti 'WFS'
###

unset ytics
set ytics autofreq
# set yrange \[0:6\]
set rmargin at screen 0.8205
unset grid

set key font 'Times,16' at 110,3.6
set ytics offset  -8, 0 scale 0.1
set autoscale  y
unset y2tics 
unset y2label
set ylabel ' E (GeV)' offset -6, 0 textcolor rgb 'black'
pl '$smp' u (\$1*1e0):(\$2*1e0) w l lw 3 dt 4 lc 'black'    ti 'E', \

unset multiplot
unset output

# 
# # pause -1
"
close $fl

exec gnuplot $pltfile

exec pdfcrop emt_tmp.pdf emt_$foname.pdf
# exec pdfcrop sigma_tmp.pdf sigma_$foname.pdf
# exec pdfcrop beta_tmp.pdf beta_$foname.pdf
# exec pdfcrop r56_tmp.pdf r56_$foname.pdf

exec pdftoppm -png -rx 300 -ry 300 emt_$foname.pdf > emt_$foname.png
# exec pdftoppm -png -rx 300 -ry 300  sigma_$foname.pdf > sigma_$foname.png
# exec pdftoppm -png -rx 300 -ry 300  beta_$foname.pdf > beta_$foname.png
# exec pdftoppm -png -rx 300 -ry 300  r56_$foname.pdf > r56_$foname.png

exec mv emt_$foname.pdf ../
exec mv emt_$foname.png ../
exec rm emt_tmp.pdf 
# 






# 
# set term pdfcairo size 18cm,7cm color enhanced font 'Times,18' rounded dashed
# set output "emt_s.pdf"
# set multiplot layout 1, 2 
# # set tmargin 2
# set grid lt 0
# set xlabel "time(ms)"
# set ylabel "Position (cm)"
# pl 'signalT.dat' u ($1):2 w l lw 2 ti 'full scan', 'signalT.dat' u ($1):3 w l lw 2 ti 'partial scan'
# set xlabel "frequency(kHz)"
# set ylabel "amplitude (#)"
# set xrange [0:130]
# set ytics 0.1
# pl 'signalF.dat' u 1:2 w l lw 2 ti 'full scan','signalF.dat' u 1:3 w l lw 2 ti 'partial scan'
# unset multiplot


# 
# set term pdfcairo size 18cm,7cm color enhanced font 'Times,18' rounded dashed
# set output "bfld.pdf"
# # set multiplot layout 1, 2 
# # set tmargin 2
# set xrange [-200:400]
# set xlabel "length(mm)"
# set ylabel "B_y (G)"
# pl 'bfld.txt' u ($1):($2*1e4) w l lw 2 noti
# # unset output
