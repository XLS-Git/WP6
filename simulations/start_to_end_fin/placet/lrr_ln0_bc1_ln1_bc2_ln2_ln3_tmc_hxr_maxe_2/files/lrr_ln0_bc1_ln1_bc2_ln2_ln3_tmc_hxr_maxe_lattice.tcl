Girder
SetReferenceEnergy   1.238601e-01
Drift      -name "LN0_DR_V5"        -length  6.639433e-01 -e0  1.238601e-01
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  1.238601e-01
Quadrupole -name "LN0_QD_V04"       -length  4.000000e-02 -e0  1.238601e-01 -strength  1.780941e-02
Dipole     -name "LN0_COR"          -length  0.000000e+00 -e0  1.238601e-01
Bpm        -name "LN0_BPM"          -length  0.000000e+00 -e0  1.238601e-01
Quadrupole -name "LN0_QD_V04"       -length  4.000000e-02 -e0  1.238601e-01 -strength  1.780941e-02
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  1.238601e-01
Drift      -name "LN0_DR_V6"        -length  2.598929e-01 -e0  1.238601e-01
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  1.238601e-01
Quadrupole -name "LN0_QD_V05"       -length  4.000000e-02 -e0  1.238601e-01 -strength  3.444120e-02
Dipole     -name "LN0_COR"          -length  0.000000e+00 -e0  1.238601e-01
Bpm        -name "LN0_BPM"          -length  0.000000e+00 -e0  1.238601e-01
Quadrupole -name "LN0_QD_V05"       -length  4.000000e-02 -e0  1.238601e-01 -strength  3.444120e-02
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  1.238601e-01
Drift      -name "LN0_DR_V7"        -length  1.419751e+00 -e0  1.238601e-01
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  1.238601e-01
Quadrupole -name "LN0_QD_V06"       -length  4.000000e-02 -e0  1.238601e-01 -strength -3.629598e-02
Dipole     -name "LN0_COR"          -length  0.000000e+00 -e0  1.238601e-01
Bpm        -name "LN0_BPM"          -length  0.000000e+00 -e0  1.238601e-01
Quadrupole -name "LN0_QD_V06"       -length  4.000000e-02 -e0  1.238601e-01 -strength -3.629598e-02
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  1.238601e-01
Drift      -name "LN0_DR_V8"        -length  2.000008e-01 -e0  1.238601e-01
Drift      -name "LN0_DR_VS"        -length  1.000000e-01 -e0  1.238601e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  1.238601e-01
Drift      -name "LN0_DR_XCA_ED_DI" -length  1.001043e-01 -e0  1.238601e-01
CrabCavity -name "LN0_XCA0_DI"      -length  9.997914e-01 -voltage  0.000000e+00 -phase  9.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.238601e-01
Drift      -name "LN0_DR_XCA_ED_DI" -length  1.001043e-01 -e0  1.237285e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  1.237285e-01
Drift      -name "LN0_TDS_DR"       -length  2.500000e-01 -e0  1.237285e-01
Drift      -name "LN0_TDS_DR"       -length  2.500000e-01 -e0  1.237285e-01
Drift      -name "LN0_TDS_DR"       -length  2.500000e-01 -e0  1.237285e-01
Drift      -name "LN0_TDS_DR"       -length  2.500000e-01 -e0  1.237285e-01
Drift      -name "LN0_TDS_DR"       -length  2.500000e-01 -e0  1.237285e-01
Drift      -name "LN0_TDS_DR"       -length  2.500000e-01 -e0  1.237285e-01
Drift      -name "LN0_TDS_DR"       -length  2.500000e-01 -e0  1.237285e-01
Drift      -name "LN0_TDS_DR"       -length  2.500000e-01 -e0  1.237285e-01
Sbend      -name "LN0_DP_TDS"       -length  1.000000e-01 -e0  1.237285e-01 -angle  0.000000e+00 -E1  0.000000e+00 -E2  0.000000e+00 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  0.000000e+00 -six_dim 1
Drift      -name "LN0_DR_20"        -length  2.000000e-01 -e0  1.237285e-01
Drift      -name "LN0_DR_VS"        -length  1.000000e-01 -e0  1.237285e-01
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  1.237285e-01
Quadrupole -name "LN0_QD_FH"        -length  4.000000e-02 -e0  1.237285e-01 -strength  3.464398e-02
Dipole     -name "LN0_COR"          -length  0.000000e+00 -e0  1.237285e-01
Bpm        -name "LN0_BPM"          -length  0.000000e+00 -e0  1.237285e-01
Quadrupole -name "LN0_QD_FH"        -length  4.000000e-02 -e0  1.237285e-01 -strength  3.464398e-02
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  1.237285e-01
Drift      -name "LN0_DR_SV"        -length  1.000000e-01 -e0  1.237285e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  1.237285e-01
Drift      -name "LN0_DR_CCA_ED"    -length  5.019800e-02 -e0  1.237285e-01
Cavity     -name "LN0_CCA0"         -length  1.899604e+00 -gradient  1.500000e-02 -phase  2.600000e+01 -frequency  5.997100e+00
SetReferenceEnergy   1.492816e-01
Drift      -name "LN0_DR_CCA_ED"    -length  5.019800e-02 -e0  1.492816e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  1.492816e-01
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  1.492816e-01
Quadrupole -name "LN0_QD_DH"        -length  4.000000e-02 -e0  1.492816e-01 -strength -4.179884e-02
Dipole     -name "LN0_COR"          -length  0.000000e+00 -e0  1.492816e-01
Bpm        -name "LN0_BPM"          -length  0.000000e+00 -e0  1.492816e-01
Quadrupole -name "LN0_QD_DH"        -length  4.000000e-02 -e0  1.492816e-01 -strength -4.179884e-02
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  1.492816e-01
Drift      -name "LN0_DR_VS"        -length  1.000000e-01 -e0  1.492816e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  1.492816e-01
Drift      -name "LN0_DR_CCA_ED"    -length  5.019800e-02 -e0  1.492816e-01
Cavity     -name "LN0_CCA0"         -length  1.899604e+00 -gradient  1.500000e-02 -phase  2.600000e+01 -frequency  5.997100e+00
SetReferenceEnergy   1.748346e-01
Drift      -name "LN0_DR_CCA_ED"    -length  5.019800e-02 -e0  1.748346e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  1.748346e-01
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  1.748346e-01
Quadrupole -name "LN0_QD_FH"        -length  4.000000e-02 -e0  1.748346e-01 -strength  4.895370e-02
Dipole     -name "LN0_COR"          -length  0.000000e+00 -e0  1.748346e-01
Bpm        -name "LN0_BPM"          -length  0.000000e+00 -e0  1.748346e-01
Quadrupole -name "LN0_QD_FH"        -length  4.000000e-02 -e0  1.748346e-01 -strength  4.895370e-02
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  1.748346e-01
Drift      -name "LN0_DR_VS"        -length  1.000000e-01 -e0  1.748346e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  1.748346e-01
Drift      -name "LN0_DR_CCA_ED"    -length  5.019800e-02 -e0  1.748346e-01
Cavity     -name "LN0_CCA0"         -length  1.899604e+00 -gradient  1.500000e-02 -phase  2.600000e+01 -frequency  5.997100e+00
SetReferenceEnergy   2.003877e-01
Drift      -name "LN0_DR_CCA_ED"    -length  5.019800e-02 -e0  2.003877e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  2.003877e-01
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  2.003877e-01
Quadrupole -name "LN0_QD_DH"        -length  4.000000e-02 -e0  2.003877e-01 -strength -5.610856e-02
Dipole     -name "LN0_COR"          -length  0.000000e+00 -e0  2.003877e-01
Bpm        -name "LN0_BPM"          -length  0.000000e+00 -e0  2.003877e-01
Quadrupole -name "LN0_QD_DH"        -length  4.000000e-02 -e0  2.003877e-01 -strength -5.610856e-02
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  2.003877e-01
Drift      -name "LN0_DR_VS"        -length  1.000000e-01 -e0  2.003877e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  2.003877e-01
Drift      -name "LN0_DR_CCA_ED"    -length  5.019800e-02 -e0  2.003877e-01
Cavity     -name "LN0_CCA0"         -length  1.899604e+00 -gradient  1.500000e-02 -phase  2.600000e+01 -frequency  5.997100e+00
SetReferenceEnergy   2.259408e-01
Drift      -name "LN0_DR_CCA_ED"    -length  5.019800e-02 -e0  2.259408e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  2.259408e-01
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  2.259408e-01
Quadrupole -name "LN0_QD_FH"        -length  4.000000e-02 -e0  2.259408e-01 -strength  6.326343e-02
Dipole     -name "LN0_COR"          -length  0.000000e+00 -e0  2.259408e-01
Bpm        -name "LN0_BPM"          -length  0.000000e+00 -e0  2.259408e-01
Quadrupole -name "LN0_QD_FH"        -length  4.000000e-02 -e0  2.259408e-01 -strength  6.326343e-02
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  2.259408e-01
Drift      -name "LN0_DR_VS"        -length  1.000000e-01 -e0  2.259408e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  2.259408e-01
Drift      -name "LN0_DR_CCA_ED"    -length  5.019800e-02 -e0  2.259408e-01
Cavity     -name "LN0_CCA0"         -length  1.899604e+00 -gradient  1.500000e-02 -phase  2.600000e+01 -frequency  5.997100e+00
SetReferenceEnergy   2.514939e-01
Drift      -name "LN0_DR_CCA_ED"    -length  5.019800e-02 -e0  2.514939e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  2.514939e-01
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  2.514939e-01
Quadrupole -name "LN0_QD_DH"        -length  4.000000e-02 -e0  2.514939e-01 -strength -7.041830e-02
Dipole     -name "LN0_COR"          -length  0.000000e+00 -e0  2.514939e-01
Bpm        -name "LN0_BPM"          -length  0.000000e+00 -e0  2.514939e-01
Quadrupole -name "LN0_QD_DH"        -length  4.000000e-02 -e0  2.514939e-01 -strength -7.041830e-02
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  2.514939e-01
Drift      -name "LN0_DR_VS"        -length  1.000000e-01 -e0  2.514939e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  2.514939e-01
Drift      -name "LN0_DR_CCA_ED"    -length  5.019800e-02 -e0  2.514939e-01
Cavity     -name "LN0_CCA0"         -length  1.899604e+00 -gradient  1.500000e-02 -phase  2.600000e+01 -frequency  5.997100e+00
SetReferenceEnergy   2.770470e-01
Drift      -name "LN0_DR_CCA_ED"    -length  5.019800e-02 -e0  2.770470e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  2.770470e-01
Drift      -name "LN0_WA_LNZ_IN2"   -length  0.000000e+00 -e0  2.770470e-01
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  2.770470e-01
Quadrupole -name "LN0_QD_FH"        -length  4.000000e-02 -e0  2.770470e-01 -strength  7.757317e-02
Dipole     -name "LN0_COR"          -length  0.000000e+00 -e0  2.770470e-01
Bpm        -name "LN0_BPM"          -length  0.000000e+00 -e0  2.770470e-01
Quadrupole -name "LN0_QD_FH"        -length  4.000000e-02 -e0  2.770470e-01 -strength  7.757317e-02
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  2.770470e-01
Drift      -name "LN0_DR_VS"        -length  1.000000e-01 -e0  2.770470e-01
Drift      -name "LN0_DR_LNZ_ED2"   -length  6.250000e-01 -e0  2.770470e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  2.770470e-01
Drift      -name "LN0_DR_KCA_ED"    -length  2.225410e-02 -e0  2.770470e-01
Cavity     -name "LN0_KCA0"         -length  3.054918e-01 -gradient  2.453617e-02 -phase  1.920000e+02 -frequency  3.598260e+01
SetReferenceEnergy   2.698730e-01
Drift      -name "LN0_DR_KCA_ED"    -length  2.225410e-02 -e0  2.698730e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  2.698730e-01
Drift      -name "LN0_DR_KCA_ED"    -length  2.225410e-02 -e0  2.698730e-01
Drift      -name "LN0_DR_KCA0"      -length  3.054918e-01 -e0  2.698730e-01
Drift      -name "LN0_DR_KCA_ED"    -length  2.225410e-02 -e0  2.698730e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  2.698730e-01
Drift      -name "LN0_DR_LNZ_ED2"   -length  6.250000e-01 -e0  2.698730e-01
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  2.698730e-01
Quadrupole -name "LN0_QD_DH"        -length  4.000000e-02 -e0  2.698730e-01 -strength -7.556445e-02
Dipole     -name "LN0_COR"          -length  0.000000e+00 -e0  2.698730e-01
Bpm        -name "LN0_BPM"          -length  0.000000e+00 -e0  2.698730e-01
Quadrupole -name "LN0_QD_DH"        -length  4.000000e-02 -e0  2.698730e-01 -strength -7.556445e-02
Drift      -name "LN0_DR_QD_ED"     -length  4.250000e-02 -e0  2.698730e-01
Drift      -name "LN0_DR_DR"        -length  1.000000e-01 -e0  2.698730e-01
Drift      -name "LN0_DR_BL"        -length  5.000000e-02 -e0  2.698730e-01
Drift      -name "LN0_DR_CCA_ED"    -length  5.019800e-02 -e0  2.698730e-01
Drift      -name "LN0_DR_CCA0H"     -length  3.498020e-01 -e0  2.698730e-01
Drift      -name "LN0_DR_A"         -length  2.000000e-01 -e0  2.698730e-01
Drift      -name "LN0_DR_VS"        -length  1.000000e-01 -e0  2.698730e-01
Drift      -name "LN0_DR_A"         -length  2.000000e-01 -e0  2.698730e-01
Drift      -name "LN0_DR_CT"        -length  1.000000e-01 -e0  2.698730e-01
Drift      -name "LN0_WA_OU2"       -length  0.000000e+00 -e0  2.698730e-01
Drift      -name "BC1_DR_V1"        -length  5.382067e-01 -e0  2.698730e-01
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.698730e-01
Quadrupole -name "BC1_QD_V01"       -length  4.000000e-02 -e0  2.698730e-01 -strength  2.066892e-02
Dipole     -name "BC1_COR"          -length  0.000000e+00 -e0  2.698730e-01
Bpm        -name "BC1_BPM"          -length  0.000000e+00 -e0  2.698730e-01
Quadrupole -name "BC1_QD_V01"       -length  4.000000e-02 -e0  2.698730e-01 -strength  2.066892e-02
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.698730e-01
Drift      -name "BC1_DR_V2"        -length  5.900340e+00 -e0  2.698730e-01
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.698730e-01
Quadrupole -name "BC1_QD_V02"       -length  4.000000e-02 -e0  2.698730e-01 -strength -9.787245e-02
Dipole     -name "BC1_COR"          -length  0.000000e+00 -e0  2.698730e-01
Bpm        -name "BC1_BPM"          -length  0.000000e+00 -e0  2.698730e-01
Quadrupole -name "BC1_QD_V02"       -length  4.000000e-02 -e0  2.698730e-01 -strength -9.787245e-02
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.698730e-01
Drift      -name "BC1_DR_V3"        -length  2.496128e-01 -e0  2.698730e-01
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.698730e-01
Quadrupole -name "BC1_QD_V03"       -length  4.000000e-02 -e0  2.698730e-01 -strength  3.507942e-02
Dipole     -name "BC1_COR"          -length  0.000000e+00 -e0  2.698730e-01
Bpm        -name "BC1_BPM"          -length  0.000000e+00 -e0  2.698730e-01
Quadrupole -name "BC1_QD_V03"       -length  4.000000e-02 -e0  2.698730e-01 -strength  3.507942e-02
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.698730e-01
Drift      -name "BC1_DR_V4"        -length  2.000000e-01 -e0  2.698730e-01
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.698730e-01
Quadrupole -name "BC1_QD_V04"       -length  4.000000e-02 -e0  2.698730e-01 -strength  5.710846e-02
Dipole     -name "BC1_COR"          -length  0.000000e+00 -e0  2.698730e-01
Bpm        -name "BC1_BPM"          -length  0.000000e+00 -e0  2.698730e-01
Quadrupole -name "BC1_QD_V04"       -length  4.000000e-02 -e0  2.698730e-01 -strength  5.710846e-02
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.698730e-01
Drift      -name "BC1_DR_V5"        -length  2.002040e-01 -e0  2.698730e-01
Drift      -name "BC1_DR_20"        -length  5.000000e-01 -e0  2.698730e-01
Sbend      -name "BC1_DP_DIP1"      -length  4.002981e-01 -e0  2.698730e-01 -angle -6.684611e-02 -E1  0.000000e+00 -E2 -6.684611e-02 -hgap  1.500000e-02 -fint  5.000000e-01 -tilt  0.000000e+00  -six_dim 1 -csr 1 
Drift      -name "BC1_DR_SIDE_C"    -length  3.000000e-01 -e0  2.698730e-01
Drift      -name "BC1_DR_SIDE"      -length  2.957275e+00 -e0  2.698730e-01
Sbend      -name "BC1_DP_DIP2"      -length  4.002981e-01 -e0  2.698730e-01 -angle  6.684611e-02 -E1  6.684611e-02 -E2  0.000000e+00 -hgap  1.500000e-02 -fint  5.000000e-01 -tilt  0.000000e+00  -six_dim 1 -csr 1 
Drift      -name "BC1_DR_CENT_C"    -length  3.500000e-01 -e0  2.698730e-01
Bpm        -name "BC1_BPM"          -length  0.000000e+00 -e0  2.698730e-01
Drift      -name "BC1_DR_CENT"      -length  3.500000e-01 -e0  2.698730e-01
Sbend      -name "BC1_DP_DIP3"      -length  4.002981e-01 -e0  2.698730e-01 -angle  6.684611e-02 -E1  0.000000e+00 -E2  6.684611e-02 -hgap  1.500000e-02 -fint  5.000000e-01 -tilt  0.000000e+00  -six_dim 1 -csr 1 
Drift      -name "BC1_DR_SIDE_C"    -length  3.000000e-01 -e0  2.698730e-01
Drift      -name "BC1_DR_SIDE"      -length  2.957275e+00 -e0  2.698730e-01
Sbend      -name "BC1_DP_DIP4"      -length  4.002981e-01 -e0  2.698730e-01 -angle -6.684611e-02 -E1 -6.684611e-02 -E2  0.000000e+00 -hgap  1.500000e-02 -fint  5.000000e-01 -tilt  0.000000e+00  -six_dim 1 -csr 1 
Drift      -name "BC1_DR_20_C"      -length  3.000000e-01 -e0  2.698730e-01
Drift      -name "BC1_WA_OU2"       -length  0.000000e+00 -e0  2.698730e-01
Drift      -name "BC1_DR_CT"        -length  1.000000e-01 -e0  2.698730e-01
Drift      -name "BC1_DR_DI_V1"     -length  2.911672e-01 -e0  2.698730e-01
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.698730e-01
Quadrupole -name "BC1_QD_DI_V01"    -length  4.000000e-02 -e0  2.698730e-01 -strength -9.243377e-02
Dipole     -name "BC1_COR_DI"       -length  0.000000e+00 -e0  2.698730e-01
Bpm        -name "BC1_BPM_DI"       -length  0.000000e+00 -e0  2.698730e-01
Quadrupole -name "BC1_QD_DI_V01"    -length  4.000000e-02 -e0  2.698730e-01 -strength -9.243377e-02
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.698730e-01
Drift      -name "BC1_DR_DI_V2"     -length  4.700020e-01 -e0  2.698730e-01
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.698730e-01
Quadrupole -name "BC1_QD_DI_V02"    -length  4.000000e-02 -e0  2.698730e-01 -strength  1.468527e-01
Dipole     -name "BC1_COR_DI"       -length  0.000000e+00 -e0  2.698730e-01
Bpm        -name "BC1_BPM_DI"       -length  0.000000e+00 -e0  2.698730e-01
Quadrupole -name "BC1_QD_DI_V02"    -length  4.000000e-02 -e0  2.698730e-01 -strength  1.468527e-01
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.698730e-01
Drift      -name "BC1_DR_DI_V3"     -length  1.199083e+00 -e0  2.698730e-01
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.698730e-01
Quadrupole -name "BC1_QD_DI_V03"    -length  4.000000e-02 -e0  2.698730e-01 -strength -5.594611e-02
Dipole     -name "BC1_COR_DI"       -length  0.000000e+00 -e0  2.698730e-01
Bpm        -name "BC1_BPM_DI"       -length  0.000000e+00 -e0  2.698730e-01
Quadrupole -name "BC1_QD_DI_V03"    -length  4.000000e-02 -e0  2.698730e-01 -strength -5.594611e-02
Drift      -name "BC1_DR_QD_ED"     -length  4.250000e-02 -e0  2.698730e-01
Drift      -name "BC1_DR_DI_V5"     -length  2.000000e-01 -e0  2.698730e-01
Drift      -name "BC1_DR_DI"        -length  2.500000e-01 -e0  2.698730e-01
Drift      -name "BC1_DR_VS"        -length  1.000000e-01 -e0  2.698730e-01
Drift      -name "BC1_DR_BL"        -length  5.000000e-02 -e0  2.698730e-01
Drift      -name "BC1_DR_XCA_ED_DI" -length  1.001043e-01 -e0  2.698730e-01
CrabCavity -name "BC1_XCA0_DI"      -length  9.997914e-01 -voltage  0.000000e+00 -phase  9.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.698730e-01
Drift      -name "BC1_DR_XCA_ED_DI" -length  1.001043e-01 -e0  2.697333e-01
Drift      -name "BC1_DR_BL"        -length  5.000000e-02 -e0  2.697333e-01
Drift      -name "BC1_DR_VS"        -length  1.000000e-01 -e0  2.697333e-01
Drift      -name "BC1_DR_DI"        -length  2.500000e-01 -e0  2.697333e-01
Drift      -name "BC1_DR_DI"        -length  2.500000e-01 -e0  2.697333e-01
Drift      -name "BC1_DR_DI_QD_ED"  -length  4.250000e-02 -e0  2.697333e-01
Quadrupole -name "BC1_QD_DI_FH"     -length  4.000000e-02 -e0  2.697333e-01 -strength  8.631465e-02
Dipole     -name "BC1_COR_DI"       -length  0.000000e+00 -e0  2.697333e-01
Bpm        -name "BC1_BPM_DI"       -length  0.000000e+00 -e0  2.697333e-01
Quadrupole -name "BC1_QD_DI_FH"     -length  4.000000e-02 -e0  2.697333e-01 -strength  8.631465e-02
Drift      -name "BC1_DR_DI_QD_ED"  -length  4.250000e-02 -e0  2.697333e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697333e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697333e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697333e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697333e-01
Drift      -name "BC1_DR_DI_VS"     -length  1.000000e-01 -e0  2.697333e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697333e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697333e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697333e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697333e-01
Drift      -name "BC1_DR_DI_QD_ED"  -length  4.250000e-02 -e0  2.697333e-01
Quadrupole -name "BC1_QD_DI_DH"     -length  4.000000e-02 -e0  2.697333e-01 -strength -8.631465e-02
Dipole     -name "BC1_COR_DI"       -length  0.000000e+00 -e0  2.697333e-01
Bpm        -name "BC1_BPM_DI"       -length  0.000000e+00 -e0  2.697333e-01
Quadrupole -name "BC1_QD_DI_DH"     -length  4.000000e-02 -e0  2.697333e-01 -strength -8.631465e-02
Drift      -name "BC1_DR_DI_QD_ED"  -length  4.250000e-02 -e0  2.697333e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697333e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697333e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697333e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697333e-01
Drift      -name "BC1_DR_DI_VS"     -length  1.000000e-01 -e0  2.697333e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697333e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697333e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697333e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697333e-01
Drift      -name "BC1_DR_DI_QD_ED"  -length  4.250000e-02 -e0  2.697333e-01
Quadrupole -name "BC1_QD_DI_FH"     -length  4.000000e-02 -e0  2.697333e-01 -strength  8.631465e-02
Dipole     -name "BC1_COR_DI"       -length  0.000000e+00 -e0  2.697333e-01
Bpm        -name "BC1_BPM_DI"       -length  0.000000e+00 -e0  2.697333e-01
Quadrupole -name "BC1_QD_DI_FH"     -length  4.000000e-02 -e0  2.697333e-01 -strength  8.631465e-02
Drift      -name "BC1_DR_DI_QD_ED"  -length  4.250000e-02 -e0  2.697333e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697333e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697333e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697333e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697333e-01
Drift      -name "BC1_DR_DI_VS"     -length  1.000000e-01 -e0  2.697333e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697333e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697333e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697333e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697333e-01
Drift      -name "BC1_DR_DI_QD_ED"  -length  4.250000e-02 -e0  2.697333e-01
Quadrupole -name "BC1_QD_DI_DH"     -length  4.000000e-02 -e0  2.697333e-01 -strength -8.631465e-02
Dipole     -name "BC1_COR_DI"       -length  0.000000e+00 -e0  2.697333e-01
Bpm        -name "BC1_BPM_DI"       -length  0.000000e+00 -e0  2.697333e-01
Quadrupole -name "BC1_QD_DI_DH"     -length  4.000000e-02 -e0  2.697333e-01 -strength -8.631465e-02
Drift      -name "BC1_DR_DI_QD_ED"  -length  4.250000e-02 -e0  2.697333e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697333e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697333e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697333e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697333e-01
Drift      -name "BC1_DR_DI_VS"     -length  1.000000e-01 -e0  2.697333e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697333e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697333e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697333e-01
Drift      -name "BC1_DR_FODO"      -length  1.500000e-01 -e0  2.697333e-01
Drift      -name "BC1_DR_DI_QD_ED"  -length  4.250000e-02 -e0  2.697333e-01
Quadrupole -name "BC1_QD_DI_FH"     -length  4.000000e-02 -e0  2.697333e-01 -strength  8.631465e-02
Dipole     -name "BC1_COR_DI"       -length  0.000000e+00 -e0  2.697333e-01
Bpm        -name "BC1_BPM_DI"       -length  0.000000e+00 -e0  2.697333e-01
Quadrupole -name "BC1_QD_DI_FH"     -length  4.000000e-02 -e0  2.697333e-01 -strength  8.631465e-02
Drift      -name "BC1_DR_DI_QD_ED"  -length  4.250000e-02 -e0  2.697333e-01
Drift      -name "BC1_DR_DI"        -length  2.500000e-01 -e0  2.697333e-01
Sbend      -name "BC1_DP_DI"        -length  2.500000e-01 -e0  2.697333e-01 -angle  0.000000e+00 -E1  0.000000e+00 -E2  0.000000e+00 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  0.000000e+00 -six_dim 1
Drift      -name "BC1_DR_DI"        -length  2.500000e-01 -e0  2.697333e-01
Drift      -name "BC1_DR_DI"        -length  2.500000e-01 -e0  2.697333e-01
Drift      -name "BC1_DR_DI"        -length  2.500000e-01 -e0  2.697333e-01
Drift      -name "BC1_WA_OU_DI2"    -length  0.000000e+00 -e0  2.697333e-01
Drift      -name "LN1_DR_V1"        -length  2.486777e-01 -e0  2.697333e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  2.697333e-01
Quadrupole -name "LN1_QD_V01"       -length  4.000000e-02 -e0  2.697333e-01 -strength  5.654639e-02
Dipole     -name "LN1_COR"          -length  0.000000e+00 -e0  2.697333e-01
Bpm        -name "LN1_BPM"          -length  0.000000e+00 -e0  2.697333e-01
Quadrupole -name "LN1_QD_V01"       -length  4.000000e-02 -e0  2.697333e-01 -strength  5.654639e-02
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  2.697333e-01
Drift      -name "LN1_DR_V2"        -length  2.254630e-01 -e0  2.697333e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  2.697333e-01
Quadrupole -name "LN1_QD_V02"       -length  4.000000e-02 -e0  2.697333e-01 -strength -1.181779e-01
Dipole     -name "LN1_COR"          -length  0.000000e+00 -e0  2.697333e-01
Bpm        -name "LN1_BPM"          -length  0.000000e+00 -e0  2.697333e-01
Quadrupole -name "LN1_QD_V02"       -length  4.000000e-02 -e0  2.697333e-01 -strength -1.181779e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  2.697333e-01
Drift      -name "LN1_DR_V3"        -length  1.800859e+00 -e0  2.697333e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  2.697333e-01
Quadrupole -name "LN1_QD_V03"       -length  4.000000e-02 -e0  2.697333e-01 -strength  1.310469e-02
Dipole     -name "LN1_COR"          -length  0.000000e+00 -e0  2.697333e-01
Bpm        -name "LN1_BPM"          -length  0.000000e+00 -e0  2.697333e-01
Quadrupole -name "LN1_QD_V03"       -length  4.000000e-02 -e0  2.697333e-01 -strength  1.310469e-02
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  2.697333e-01
Drift      -name "LN1_DR_V4"        -length  1.507694e-01 -e0  2.697333e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  2.697333e-01
Quadrupole -name "LN1_QD_FH"        -length  4.000000e-02 -e0  2.697333e-01 -strength  7.120959e-02
Dipole     -name "LN1_COR"          -length  0.000000e+00 -e0  2.697333e-01
Bpm        -name "LN1_BPM"          -length  0.000000e+00 -e0  2.697333e-01
Quadrupole -name "LN1_QD_FH"        -length  4.000000e-02 -e0  2.697333e-01 -strength  7.120959e-02
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  2.697333e-01
Drift      -name "LN1_DR_SV"        -length  1.000000e-01 -e0  2.697333e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  2.697333e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  2.697333e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  3.500000e+01 -frequency  1.199420e+01
SetReferenceEnergy   3.184492e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  3.184492e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  3.184492e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  3.184492e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  3.500000e+01 -frequency  1.199420e+01
SetReferenceEnergy   3.671650e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  3.671650e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  3.671650e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  3.671650e-01
Quadrupole -name "LN1_QD_DH"        -length  4.000000e-02 -e0  3.671650e-01 -strength -9.693156e-02
Dipole     -name "LN1_COR"          -length  0.000000e+00 -e0  3.671650e-01
Bpm        -name "LN1_BPM"          -length  0.000000e+00 -e0  3.671650e-01
Quadrupole -name "LN1_QD_DH"        -length  4.000000e-02 -e0  3.671650e-01 -strength -9.693156e-02
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  3.671650e-01
Drift      -name "LN1_DR_VS"        -length  1.000000e-01 -e0  3.671650e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  3.671650e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  3.671650e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  3.500000e+01 -frequency  1.199420e+01
SetReferenceEnergy   4.158809e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  4.158809e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  4.158809e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  4.158809e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  3.500000e+01 -frequency  1.199420e+01
SetReferenceEnergy   4.645967e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  4.645967e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  4.645967e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  4.645967e-01
Quadrupole -name "LN1_QD_FH"        -length  4.000000e-02 -e0  4.645967e-01 -strength  1.226535e-01
Dipole     -name "LN1_COR"          -length  0.000000e+00 -e0  4.645967e-01
Bpm        -name "LN1_BPM"          -length  0.000000e+00 -e0  4.645967e-01
Quadrupole -name "LN1_QD_FH"        -length  4.000000e-02 -e0  4.645967e-01 -strength  1.226535e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  4.645967e-01
Drift      -name "LN1_DR_SV"        -length  1.000000e-01 -e0  4.645967e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  4.645967e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  4.645967e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  3.500000e+01 -frequency  1.199420e+01
SetReferenceEnergy   5.133125e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  5.133125e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  5.133125e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  5.133125e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  3.500000e+01 -frequency  1.199420e+01
SetReferenceEnergy   5.620284e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  5.620284e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  5.620284e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  5.620284e-01
Quadrupole -name "LN1_QD_DH"        -length  4.000000e-02 -e0  5.620284e-01 -strength -1.483755e-01
Dipole     -name "LN1_COR"          -length  0.000000e+00 -e0  5.620284e-01
Bpm        -name "LN1_BPM"          -length  0.000000e+00 -e0  5.620284e-01
Quadrupole -name "LN1_QD_DH"        -length  4.000000e-02 -e0  5.620284e-01 -strength -1.483755e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  5.620284e-01
Drift      -name "LN1_DR_VS"        -length  1.000000e-01 -e0  5.620284e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  5.620284e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  5.620284e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  3.500000e+01 -frequency  1.199420e+01
SetReferenceEnergy   6.107442e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  6.107442e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  6.107442e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  6.107442e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  3.500000e+01 -frequency  1.199420e+01
SetReferenceEnergy   6.594601e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  6.594601e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  6.594601e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  6.594601e-01
Quadrupole -name "LN1_QD_FH"        -length  4.000000e-02 -e0  6.594601e-01 -strength  1.740975e-01
Dipole     -name "LN1_COR"          -length  0.000000e+00 -e0  6.594601e-01
Bpm        -name "LN1_BPM"          -length  0.000000e+00 -e0  6.594601e-01
Quadrupole -name "LN1_QD_FH"        -length  4.000000e-02 -e0  6.594601e-01 -strength  1.740975e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  6.594601e-01
Drift      -name "LN1_DR_SV"        -length  1.000000e-01 -e0  6.594601e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  6.594601e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  6.594601e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  3.500000e+01 -frequency  1.199420e+01
SetReferenceEnergy   7.081759e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  7.081759e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  7.081759e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  7.081759e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  3.500000e+01 -frequency  1.199420e+01
SetReferenceEnergy   7.568917e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  7.568917e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  7.568917e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  7.568917e-01
Quadrupole -name "LN1_QD_DH"        -length  4.000000e-02 -e0  7.568917e-01 -strength -1.998194e-01
Dipole     -name "LN1_COR"          -length  0.000000e+00 -e0  7.568917e-01
Bpm        -name "LN1_BPM"          -length  0.000000e+00 -e0  7.568917e-01
Quadrupole -name "LN1_QD_DH"        -length  4.000000e-02 -e0  7.568917e-01 -strength -1.998194e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  7.568917e-01
Drift      -name "LN1_DR_VS"        -length  1.000000e-01 -e0  7.568917e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  7.568917e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  7.568917e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  3.500000e+01 -frequency  1.199420e+01
SetReferenceEnergy   8.056076e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  8.056076e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  8.056076e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  8.056076e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  3.500000e+01 -frequency  1.199420e+01
SetReferenceEnergy   8.543234e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  8.543234e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  8.543234e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  8.543234e-01
Quadrupole -name "LN1_QD_FH"        -length  4.000000e-02 -e0  8.543234e-01 -strength  2.255414e-01
Dipole     -name "LN1_COR"          -length  0.000000e+00 -e0  8.543234e-01
Bpm        -name "LN1_BPM"          -length  0.000000e+00 -e0  8.543234e-01
Quadrupole -name "LN1_QD_FH"        -length  4.000000e-02 -e0  8.543234e-01 -strength  2.255414e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  8.543234e-01
Drift      -name "LN1_DR_SV"        -length  1.000000e-01 -e0  8.543234e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  8.543234e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  8.543234e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  3.500000e+01 -frequency  1.199420e+01
SetReferenceEnergy   9.030392e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  9.030392e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  9.030392e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  9.030392e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  3.500000e+01 -frequency  1.199420e+01
SetReferenceEnergy   9.517550e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  9.517550e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  9.517550e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  9.517550e-01
Quadrupole -name "LN1_QD_DH"        -length  4.000000e-02 -e0  9.517550e-01 -strength -2.512633e-01
Dipole     -name "LN1_COR"          -length  0.000000e+00 -e0  9.517550e-01
Bpm        -name "LN1_BPM"          -length  0.000000e+00 -e0  9.517550e-01
Quadrupole -name "LN1_QD_DH"        -length  4.000000e-02 -e0  9.517550e-01 -strength -2.512633e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  9.517550e-01
Drift      -name "LN1_DR_VS"        -length  1.000000e-01 -e0  9.517550e-01
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  9.517550e-01
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  9.517550e-01
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  3.500000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.000471e+00
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  1.000471e+00
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  1.000471e+00
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  1.000471e+00
Cavity     -name "LN1_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  3.500000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.049187e+00
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  1.049187e+00
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  1.049187e+00
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  1.049187e+00
Quadrupole -name "LN1_QD_FH"        -length  4.000000e-02 -e0  1.049187e+00 -strength  2.769853e-01
Dipole     -name "LN1_COR"          -length  0.000000e+00 -e0  1.049187e+00
Bpm        -name "LN1_BPM"          -length  0.000000e+00 -e0  1.049187e+00
Quadrupole -name "LN1_QD_FH"        -length  4.000000e-02 -e0  1.049187e+00 -strength  2.769853e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  1.049187e+00
Drift      -name "LN1_DR_DR"        -length  1.000000e-01 -e0  1.049187e+00
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  1.049187e+00
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  1.049187e+00
Drift      -name "LN1_DR_XCA_"      -length  9.164755e-01 -e0  1.049187e+00
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  1.049187e+00
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  1.049187e+00
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  1.049187e+00
Drift      -name "LN1_DR_XCA_"      -length  9.164755e-01 -e0  1.049187e+00
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  1.049187e+00
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  1.049187e+00
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  1.049187e+00
Quadrupole -name "LN1_QD_DH"        -length  4.000000e-02 -e0  1.049187e+00 -strength -2.769853e-01
Dipole     -name "LN1_COR"          -length  0.000000e+00 -e0  1.049187e+00
Bpm        -name "LN1_BPM"          -length  0.000000e+00 -e0  1.049187e+00
Quadrupole -name "LN1_QD_DH"        -length  4.000000e-02 -e0  1.049187e+00 -strength -2.769853e-01
Drift      -name "LN1_DR_QD_ED"     -length  4.250000e-02 -e0  1.049187e+00
Drift      -name "LN1_DR_DR"        -length  1.000000e-01 -e0  1.049187e+00
Drift      -name "LN1_DR_BL"        -length  5.000000e-02 -e0  1.049187e+00
Drift      -name "LN1_DR_XCA_ED"    -length  5.676225e-02 -e0  1.049187e+00
Drift      -name "LN1_DR_XCA_A"     -length  8.164755e-01 -e0  1.049187e+00
Drift      -name "LN1_DR_VS"        -length  1.000000e-01 -e0  1.049187e+00
Drift      -name "LN1_WA_OU2"       -length  0.000000e+00 -e0  1.049187e+00
Drift      -name "BC2_IN_CNT"       -length  0.000000e+00 -e0  1.049187e+00
Drift      -name "BC2_DR_V1"        -length  4.718667e-01 -e0  1.049187e+00
Drift      -name "BC2_DR_QD_ED"     -length  4.250000e-02 -e0  1.049187e+00
Quadrupole -name "BC2_QD_V01"       -length  4.000000e-02 -e0  1.049187e+00 -strength  1.611938e-02
Dipole     -name "BC2_COR"          -length  0.000000e+00 -e0  1.049187e+00
Bpm        -name "BC2_BPM"          -length  0.000000e+00 -e0  1.049187e+00
Quadrupole -name "BC2_QD_V01"       -length  4.000000e-02 -e0  1.049187e+00 -strength  1.611938e-02
Drift      -name "BC2_DR_QD_ED"     -length  4.250000e-02 -e0  1.049187e+00
Drift      -name "BC2_DR_V2"        -length  5.872423e+00 -e0  1.049187e+00
Drift      -name "BC2_DR_QD_ED"     -length  4.250000e-02 -e0  1.049187e+00
Quadrupole -name "BC2_QD_V02"       -length  4.000000e-02 -e0  1.049187e+00 -strength -4.878376e-01
Dipole     -name "BC2_COR"          -length  0.000000e+00 -e0  1.049187e+00
Bpm        -name "BC2_BPM"          -length  0.000000e+00 -e0  1.049187e+00
Quadrupole -name "BC2_QD_V02"       -length  4.000000e-02 -e0  1.049187e+00 -strength -4.878376e-01
Drift      -name "BC2_DR_QD_ED"     -length  4.250000e-02 -e0  1.049187e+00
Drift      -name "BC2_DR_V3"        -length  2.000000e-01 -e0  1.049187e+00
Drift      -name "BC2_DR_QD_ED"     -length  4.250000e-02 -e0  1.049187e+00
Quadrupole -name "BC2_QD_V03"       -length  4.000000e-02 -e0  1.049187e+00 -strength  4.521647e-01
Dipole     -name "BC2_COR"          -length  0.000000e+00 -e0  1.049187e+00
Bpm        -name "BC2_BPM"          -length  0.000000e+00 -e0  1.049187e+00
Quadrupole -name "BC2_QD_V03"       -length  4.000000e-02 -e0  1.049187e+00 -strength  4.521647e-01
Drift      -name "BC2_DR_QD_ED"     -length  4.250000e-02 -e0  1.049187e+00
Drift      -name "BC2_DR_V4"        -length  2.000000e-01 -e0  1.049187e+00
Drift      -name "BC2_DR_20"        -length  5.000000e-01 -e0  1.049187e+00
Sbend      -name "BC2_DP_DIP1"      -length  6.000576e-01 -e0  1.049187e+00 -angle -2.399828e-02 -E1  0.000000e+00 -E2 -2.399828e-02 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  0.000000e+00  -six_dim 1 -csr 1 
Drift      -name "BC2_DR_SIDE_C"    -length  5.000000e-01 -e0  1.049187e+00
Drift      -name "BC2_DR_SIDE"      -length  3.201066e+00 -e0  1.049187e+00
Sbend      -name "BC2_DP_DIP2"      -length  6.000576e-01 -e0  1.049187e+00 -angle  2.399828e-02 -E1  2.399828e-02 -E2  0.000000e+00 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  0.000000e+00  -six_dim 1 -csr 1 
Drift      -name "BC2_DR_CENT_C"    -length  2.500000e-01 -e0  1.049187e+00
Bpm        -name "BC2_BPM"          -length  0.000000e+00 -e0  1.049187e+00
Drift      -name "BC2_DR_CENT"      -length  2.500000e-01 -e0  1.049187e+00
Sbend      -name "BC2_DP_DIP3"      -length  6.000576e-01 -e0  1.049187e+00 -angle  2.399828e-02 -E1  0.000000e+00 -E2  2.399828e-02 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  0.000000e+00  -six_dim 1 -csr 1 
Drift      -name "BC2_DR_SIDE_C"    -length  5.000000e-01 -e0  1.049187e+00
Drift      -name "BC2_DR_SIDE"      -length  3.201066e+00 -e0  1.049187e+00
Sbend      -name "BC2_DP_DIP4"      -length  6.000576e-01 -e0  1.049187e+00 -angle -2.399828e-02 -E1 -2.399828e-02 -E2  0.000000e+00 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  0.000000e+00  -six_dim 1 -csr 1 
Drift      -name "BC2_IN_CNT"       -length  0.000000e+00 -e0  1.049187e+00
Drift      -name "BC2_DR_20_C"      -length  5.000000e-01 -e0  1.049187e+00
Drift      -name "BC2_IN_CNT"       -length  0.000000e+00 -e0  1.049187e+00
Drift      -name "BC2_WA_OU2"       -length  0.000000e+00 -e0  1.049187e+00
Drift      -name "LN2_DR_CT"        -length  1.000000e-01 -e0  1.049187e+00
Drift      -name "LN2_DR_V1"        -length  2.294830e+00 -e0  1.049187e+00
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.049187e+00
Quadrupole -name "LN2_QD_V01"       -length  4.000000e-02 -e0  1.049187e+00 -strength  5.721563e-01
Dipole     -name "LN2_COR"          -length  0.000000e+00 -e0  1.049187e+00
Bpm        -name "LN2_BPM"          -length  0.000000e+00 -e0  1.049187e+00
Quadrupole -name "LN2_QD_V01"       -length  4.000000e-02 -e0  1.049187e+00 -strength  5.721563e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.049187e+00
Drift      -name "LN2_DR_V2"        -length  5.090891e-01 -e0  1.049187e+00
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.049187e+00
Quadrupole -name "LN2_QD_V02"       -length  4.000000e-02 -e0  1.049187e+00 -strength -5.487509e-01
Dipole     -name "LN2_COR"          -length  0.000000e+00 -e0  1.049187e+00
Bpm        -name "LN2_BPM"          -length  0.000000e+00 -e0  1.049187e+00
Quadrupole -name "LN2_QD_V02"       -length  4.000000e-02 -e0  1.049187e+00 -strength -5.487509e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.049187e+00
Drift      -name "LN2_DR_V3"        -length  1.490464e+00 -e0  1.049187e+00
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.049187e+00
Quadrupole -name "LN2_QD_V03"       -length  4.000000e-02 -e0  1.049187e+00 -strength  3.740811e-01
Dipole     -name "LN2_COR"          -length  0.000000e+00 -e0  1.049187e+00
Bpm        -name "LN2_BPM"          -length  0.000000e+00 -e0  1.049187e+00
Quadrupole -name "LN2_QD_V03"       -length  4.000000e-02 -e0  1.049187e+00 -strength  3.740811e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.049187e+00
Drift      -name "LN2_DR_V4"        -length  2.999939e+00 -e0  1.049187e+00
Drift      -name "LN2_DR_VS"        -length  1.000000e-01 -e0  1.049187e+00
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.049187e+00
Quadrupole -name "LN2_QD_FH"        -length  4.000000e-02 -e0  1.049187e+00 -strength  2.811820e-01
Dipole     -name "LN2_COR"          -length  0.000000e+00 -e0  1.049187e+00
Bpm        -name "LN2_BPM"          -length  0.000000e+00 -e0  1.049187e+00
Quadrupole -name "LN2_QD_FH"        -length  4.000000e-02 -e0  1.049187e+00 -strength  2.811820e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.049187e+00
Drift      -name "LN2_DR_SV"        -length  1.000000e-01 -e0  1.049187e+00
Drift      -name "LN2_DR_BL"        -length  5.000000e-02 -e0  1.049187e+00
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  1.049187e+00
Cavity     -name "LN2_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.106058e+00
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  1.106058e+00
Drift      -name "LN2_DR_BL"        -length  5.000000e-02 -e0  1.106058e+00
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  1.106058e+00
Cavity     -name "LN2_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.164627e+00
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  1.164627e+00
Drift      -name "LN2_DR_BL"        -length  5.000000e-02 -e0  1.164627e+00
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.164627e+00
Quadrupole -name "LN2_QD_DH"        -length  4.000000e-02 -e0  1.164627e+00 -strength -3.121202e-01
Dipole     -name "LN2_COR"          -length  0.000000e+00 -e0  1.164627e+00
Bpm        -name "LN2_BPM"          -length  0.000000e+00 -e0  1.164627e+00
Quadrupole -name "LN2_QD_DH"        -length  4.000000e-02 -e0  1.164627e+00 -strength -3.121202e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.164627e+00
Drift      -name "LN2_DR_VS"        -length  1.000000e-01 -e0  1.164627e+00
Drift      -name "LN2_DR_BL"        -length  5.000000e-02 -e0  1.164627e+00
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  1.164627e+00
Cavity     -name "LN2_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.223197e+00
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  1.223197e+00
Drift      -name "LN2_DR_BL"        -length  5.000000e-02 -e0  1.223197e+00
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  1.223197e+00
Cavity     -name "LN2_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.281766e+00
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  1.281766e+00
Drift      -name "LN2_DR_BL"        -length  5.000000e-02 -e0  1.281766e+00
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.281766e+00
Quadrupole -name "LN2_QD_FH"        -length  4.000000e-02 -e0  1.281766e+00 -strength  3.435132e-01
Dipole     -name "LN2_COR"          -length  0.000000e+00 -e0  1.281766e+00
Bpm        -name "LN2_BPM"          -length  0.000000e+00 -e0  1.281766e+00
Quadrupole -name "LN2_QD_FH"        -length  4.000000e-02 -e0  1.281766e+00 -strength  3.435132e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.281766e+00
Drift      -name "LN2_DR_SV"        -length  1.000000e-01 -e0  1.281766e+00
Drift      -name "LN2_DR_BL"        -length  5.000000e-02 -e0  1.281766e+00
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  1.281766e+00
Cavity     -name "LN2_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.340335e+00
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  1.340335e+00
Drift      -name "LN2_DR_BL"        -length  5.000000e-02 -e0  1.340335e+00
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  1.340335e+00
Cavity     -name "LN2_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.398904e+00
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  1.398904e+00
Drift      -name "LN2_DR_BL"        -length  5.000000e-02 -e0  1.398904e+00
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.398904e+00
Quadrupole -name "LN2_QD_DH"        -length  4.000000e-02 -e0  1.398904e+00 -strength -3.749063e-01
Dipole     -name "LN2_COR"          -length  0.000000e+00 -e0  1.398904e+00
Bpm        -name "LN2_BPM"          -length  0.000000e+00 -e0  1.398904e+00
Quadrupole -name "LN2_QD_DH"        -length  4.000000e-02 -e0  1.398904e+00 -strength -3.749063e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.398904e+00
Drift      -name "LN2_DR_VS"        -length  1.000000e-01 -e0  1.398904e+00
Drift      -name "LN2_DR_BL"        -length  5.000000e-02 -e0  1.398904e+00
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  1.398904e+00
Cavity     -name "LN2_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.457473e+00
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  1.457473e+00
Drift      -name "LN2_DR_BL"        -length  5.000000e-02 -e0  1.457473e+00
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  1.457473e+00
Cavity     -name "LN2_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.516042e+00
Drift      -name "LN2_DR_XCA_ED"    -length  5.676225e-02 -e0  1.516042e+00
Drift      -name "LN2_DR_BL"        -length  5.000000e-02 -e0  1.516042e+00
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.516042e+00
Quadrupole -name "LN2_QD_FH"        -length  4.000000e-02 -e0  1.516042e+00 -strength  4.062993e-01
Dipole     -name "LN2_COR"          -length  0.000000e+00 -e0  1.516042e+00
Bpm        -name "LN2_BPM"          -length  0.000000e+00 -e0  1.516042e+00
Quadrupole -name "LN2_QD_FH"        -length  4.000000e-02 -e0  1.516042e+00 -strength  4.062993e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.516042e+00
Drift      -name "LN2_WA_OU2"       -length  0.000000e+00 -e0  1.516042e+00
Drift      -name "LN2_DR_DI_V1"     -length  1.161052e+00 -e0  1.516042e+00
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.516042e+00
Quadrupole -name "LN2_QD_DI_V01"    -length  4.000000e-02 -e0  1.516042e+00 -strength -1.219687e-01
Dipole     -name "LN2_COR_DI"       -length  0.000000e+00 -e0  1.516042e+00
Bpm        -name "LN2_BPM_DI"       -length  0.000000e+00 -e0  1.516042e+00
Quadrupole -name "LN2_QD_DI_V01"    -length  4.000000e-02 -e0  1.516042e+00 -strength -1.219687e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.516042e+00
Drift      -name "LN2_DR_DI_V2"     -length  8.279980e-01 -e0  1.516042e+00
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.516042e+00
Quadrupole -name "LN2_QD_DI_V02"    -length  4.000000e-02 -e0  1.516042e+00 -strength  6.140712e-01
Dipole     -name "LN2_COR_DI"       -length  0.000000e+00 -e0  1.516042e+00
Bpm        -name "LN2_BPM_DI"       -length  0.000000e+00 -e0  1.516042e+00
Quadrupole -name "LN2_QD_DI_V02"    -length  4.000000e-02 -e0  1.516042e+00 -strength  6.140712e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.516042e+00
Drift      -name "LN2_DR_DI_V3"     -length  2.001539e-01 -e0  1.516042e+00
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.516042e+00
Quadrupole -name "LN2_QD_DI_V03"    -length  4.000000e-02 -e0  1.516042e+00 -strength -7.719898e-01
Dipole     -name "LN2_COR_DI"       -length  0.000000e+00 -e0  1.516042e+00
Bpm        -name "LN2_BPM_DI"       -length  0.000000e+00 -e0  1.516042e+00
Quadrupole -name "LN2_QD_DI_V03"    -length  4.000000e-02 -e0  1.516042e+00 -strength -7.719898e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.516042e+00
Drift      -name "LN2_DR_DI_V5"     -length  2.000000e-01 -e0  1.516042e+00
Drift      -name "LN2_DR_VS"        -length  1.000000e-01 -e0  1.516042e+00
Drift      -name "LN2_DR_DI"        -length  2.500000e-01 -e0  1.516042e+00
Drift      -name "LN2_DR_VS_DI"     -length  1.000000e-01 -e0  1.516042e+00
Drift      -name "LN2_DR_BL_DI"     -length  5.000000e-02 -e0  1.516042e+00
Drift      -name "LN2_DR_XCA_ED_DI" -length  1.001043e-01 -e0  1.516042e+00
CrabCavity -name "LN2_XCA0_DI"      -length  9.997914e-01 -voltage  0.000000e+00 -phase  9.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.516042e+00
Drift      -name "LN2_DR_XCA_ED_DI" -length  1.001043e-01 -e0  1.515961e+00
Drift      -name "LN2_DR_BL_DI"     -length  5.000000e-02 -e0  1.515961e+00
Drift      -name "LN2_DR_DI"        -length  2.500000e-01 -e0  1.515961e+00
Drift      -name "LN2_DR_DI"        -length  2.500000e-01 -e0  1.515961e+00
Drift      -name "LN2_DR_DI_QD_ED"  -length  4.250000e-02 -e0  1.515961e+00
Quadrupole -name "LN2_QD_DI_FH"     -length  4.000000e-02 -e0  1.515961e+00 -strength  4.851076e-01
Dipole     -name "LN2_COR_DI"       -length  0.000000e+00 -e0  1.515961e+00
Bpm        -name "LN2_BPM_DI"       -length  0.000000e+00 -e0  1.515961e+00
Quadrupole -name "LN2_QD_DI_FH"     -length  4.000000e-02 -e0  1.515961e+00 -strength  4.851076e-01
Drift      -name "LN2_DR_DI_QD_ED"  -length  4.250000e-02 -e0  1.515961e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.515961e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.515961e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.515961e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.515961e+00
Drift      -name "LN2_DR_DI_VS"     -length  1.000000e-01 -e0  1.515961e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.515961e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.515961e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.515961e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.515961e+00
Drift      -name "LN2_DR_DI_QD_ED"  -length  4.250000e-02 -e0  1.515961e+00
Quadrupole -name "LN2_QD_DI_DH"     -length  4.000000e-02 -e0  1.515961e+00 -strength -4.851076e-01
Dipole     -name "LN2_COR_DI"       -length  0.000000e+00 -e0  1.515961e+00
Bpm        -name "LN2_BPM_DI"       -length  0.000000e+00 -e0  1.515961e+00
Quadrupole -name "LN2_QD_DI_DH"     -length  4.000000e-02 -e0  1.515961e+00 -strength -4.851076e-01
Drift      -name "LN2_DR_DI_QD_ED"  -length  4.250000e-02 -e0  1.515961e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.515961e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.515961e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.515961e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.515961e+00
Drift      -name "LN2_DR_DI_VS"     -length  1.000000e-01 -e0  1.515961e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.515961e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.515961e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.515961e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.515961e+00
Drift      -name "LN2_DR_DI_QD_ED"  -length  4.250000e-02 -e0  1.515961e+00
Quadrupole -name "LN2_QD_DI_FH"     -length  4.000000e-02 -e0  1.515961e+00 -strength  4.851076e-01
Dipole     -name "LN2_COR_DI"       -length  0.000000e+00 -e0  1.515961e+00
Bpm        -name "LN2_BPM_DI"       -length  0.000000e+00 -e0  1.515961e+00
Quadrupole -name "LN2_QD_DI_FH"     -length  4.000000e-02 -e0  1.515961e+00 -strength  4.851076e-01
Drift      -name "LN2_DR_DI_QD_ED"  -length  4.250000e-02 -e0  1.515961e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.515961e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.515961e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.515961e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.515961e+00
Drift      -name "LN2_DR_DI_VS"     -length  1.000000e-01 -e0  1.515961e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.515961e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.515961e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.515961e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.515961e+00
Drift      -name "LN2_DR_DI_QD_ED"  -length  4.250000e-02 -e0  1.515961e+00
Quadrupole -name "LN2_QD_DI_DH"     -length  4.000000e-02 -e0  1.515961e+00 -strength -4.851076e-01
Dipole     -name "LN2_COR_DI"       -length  0.000000e+00 -e0  1.515961e+00
Bpm        -name "LN2_BPM_DI"       -length  0.000000e+00 -e0  1.515961e+00
Quadrupole -name "LN2_QD_DI_DH"     -length  4.000000e-02 -e0  1.515961e+00 -strength -4.851076e-01
Drift      -name "LN2_DR_DI_QD_ED"  -length  4.250000e-02 -e0  1.515961e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.515961e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.515961e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.515961e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.515961e+00
Drift      -name "LN2_DR_DI_VS"     -length  1.000000e-01 -e0  1.515961e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.515961e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.515961e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.515961e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.515961e+00
Drift      -name "LN2_DR_DI_QD_ED"  -length  4.250000e-02 -e0  1.515961e+00
Quadrupole -name "LN2_QD_DI_FH"     -length  4.000000e-02 -e0  1.515961e+00 -strength  4.851076e-01
Dipole     -name "LN2_COR_DI"       -length  0.000000e+00 -e0  1.515961e+00
Bpm        -name "LN2_BPM_DI"       -length  0.000000e+00 -e0  1.515961e+00
Quadrupole -name "LN2_QD_DI_FH"     -length  4.000000e-02 -e0  1.515961e+00 -strength  4.851076e-01
Drift      -name "LN2_DR_DI_QD_ED"  -length  4.250000e-02 -e0  1.515961e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.515961e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.515961e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.515961e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.515961e+00
Drift      -name "LN2_DR_DI_VS"     -length  1.000000e-01 -e0  1.515961e+00
Sbend      -name "LN2_DP_DI"        -length  1.500000e-01 -e0  1.515961e+00 -angle  0.000000e+00 -E1  0.000000e+00 -E2  0.000000e+00 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  0.000000e+00 -six_dim 1
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.515961e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.515961e+00
Drift      -name "LN2_DR_FODO"      -length  1.500000e-01 -e0  1.515961e+00
Drift      -name "LN2_DR_DI_QD_ED"  -length  4.250000e-02 -e0  1.515961e+00
Quadrupole -name "LN2_QD_DI_DH"     -length  4.000000e-02 -e0  1.515961e+00 -strength -4.851076e-01
Dipole     -name "LN2_COR_DI"       -length  0.000000e+00 -e0  1.515961e+00
Bpm        -name "LN2_BPM_DI"       -length  0.000000e+00 -e0  1.515961e+00
Quadrupole -name "LN2_QD_DI_DH"     -length  4.000000e-02 -e0  1.515961e+00 -strength -4.851076e-01
Drift      -name "LN2_DR_DI_QD_ED"  -length  4.250000e-02 -e0  1.515961e+00
Drift      -name "LN2_DR_DI"        -length  2.500000e-01 -e0  1.515961e+00
Drift      -name "LN2_WA_OU_DI2"    -length  0.000000e+00 -e0  1.515961e+00
Drift      -name "LN2_DR_BP_V1"     -length  3.714219e+00 -e0  1.515961e+00
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.515961e+00
Quadrupole -name "LN2_QD_BP_V01"    -length  4.000000e-02 -e0  1.515961e+00 -strength -7.602803e-02
Dipole     -name "LN2_COR"          -length  0.000000e+00 -e0  1.515961e+00
Bpm        -name "LN2_BPM"          -length  0.000000e+00 -e0  1.515961e+00
Quadrupole -name "LN2_QD_BP_V01"    -length  4.000000e-02 -e0  1.515961e+00 -strength -7.602803e-02
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.515961e+00
Drift      -name "LN2_DR_BP_V2"     -length  3.528767e+00 -e0  1.515961e+00
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.515961e+00
Quadrupole -name "LN2_QD_BP_V02"    -length  4.000000e-02 -e0  1.515961e+00 -strength  8.393583e-01
Dipole     -name "LN2_COR"          -length  0.000000e+00 -e0  1.515961e+00
Bpm        -name "LN2_BPM"          -length  0.000000e+00 -e0  1.515961e+00
Quadrupole -name "LN2_QD_BP_V02"    -length  4.000000e-02 -e0  1.515961e+00 -strength  8.393583e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.515961e+00
Drift      -name "LN2_DR_BP_V3"     -length  2.000269e-01 -e0  1.515961e+00
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.515961e+00
Quadrupole -name "LN2_QD_BP_V03"    -length  4.000000e-02 -e0  1.515961e+00 -strength -8.189029e-01
Dipole     -name "LN2_COR"          -length  0.000000e+00 -e0  1.515961e+00
Bpm        -name "LN2_BPM"          -length  0.000000e+00 -e0  1.515961e+00
Quadrupole -name "LN2_QD_BP_V03"    -length  4.000000e-02 -e0  1.515961e+00 -strength -8.189029e-01
Drift      -name "LN2_DR_QD_ED"     -length  4.250000e-02 -e0  1.515961e+00
Drift      -name "LN2_DR_BP_V4"     -length  2.645902e-01 -e0  1.515961e+00
Drift      -name "LN2_DR_BP_0"      -length  2.000000e-01 -e0  1.515961e+00
Drift      -name "LN2_DR_BP_BL"     -length  5.000000e-02 -e0  1.515961e+00
Drift      -name "LN2_DR_SCA_ED"    -length  8.325105e-02 -e0  1.515961e+00
CrabCavity -name "LN2_SCA0"         -length  5.334979e-01 -voltage  0.000000e+00 -phase  9.000000e+01 -frequency  2.997000e+00
SetReferenceEnergy   1.515961e+00
Drift      -name "LN2_DR_SCA_ED"    -length  8.325105e-02 -e0  1.515956e+00
Drift      -name "LN2_DR_BP_BL"     -length  5.000000e-02 -e0  1.515956e+00
Drift      -name "LN2_DR_BP_0"      -length  2.000000e-01 -e0  1.515956e+00
Drift      -name "LN2_DR_BP"        -length  4.000000e-01 -e0  1.515956e+00
Drift      -name "LN2_DR_BP"        -length  4.000000e-01 -e0  1.515956e+00
Drift      -name "LN2_DR_BP_VS"     -length  1.000000e-01 -e0  1.515956e+00
Drift      -name "LN2_DR_BP_DR"     -length  1.000000e-01 -e0  1.515956e+00
Drift      -name "LN2_DP_SEPM"      -length  2.500000e-01 -e0  1.515956e+00
Drift      -name "LN2_DR_BP"        -length  4.000000e-01 -e0  1.515956e+00
Drift      -name "LN2_DR_BP"        -length  4.000000e-01 -e0  1.515956e+00
Drift      -name "LN2_DR_BP"        -length  4.000000e-01 -e0  1.515956e+00
Drift      -name "LN2_DR_BP"        -length  4.000000e-01 -e0  1.515956e+00
Drift      -name "LN2_DR_BP"        -length  4.000000e-01 -e0  1.515956e+00
Drift      -name "LN2_DR_BP_VS"     -length  1.000000e-01 -e0  1.515956e+00
Drift      -name "LN2_DR_BP"        -length  4.000000e-01 -e0  1.515956e+00
Drift      -name "LN2_BP_WA_OU2_"   -length  0.000000e+00 -e0  1.515956e+00
Drift      -name "LN3_DR_V1"        -length  5.000000e-02 -e0  1.515956e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.515956e+00
Quadrupole -name "LN3_QD_V01"       -length  4.000000e-02 -e0  1.515956e+00 -strength  4.419709e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  1.515956e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  1.515956e+00
Quadrupole -name "LN3_QD_V01"       -length  4.000000e-02 -e0  1.515956e+00 -strength  4.419709e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.515956e+00
Drift      -name "LN3_DR_V2"        -length  5.342817e-01 -e0  1.515956e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.515956e+00
Quadrupole -name "LN3_QD_V02"       -length  4.000000e-02 -e0  1.515956e+00 -strength  5.489216e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  1.515956e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  1.515956e+00
Quadrupole -name "LN3_QD_V02"       -length  4.000000e-02 -e0  1.515956e+00 -strength  5.489216e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.515956e+00
Drift      -name "LN3_DR_V3"        -length  9.374694e-02 -e0  1.515956e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.515956e+00
Quadrupole -name "LN3_QD_V03"       -length  4.000000e-02 -e0  1.515956e+00 -strength -9.011340e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  1.515956e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  1.515956e+00
Quadrupole -name "LN3_QD_V03"       -length  4.000000e-02 -e0  1.515956e+00 -strength -9.011340e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.515956e+00
Drift      -name "LN3_DR_V4"        -length  1.580023e+00 -e0  1.515956e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.515956e+00
Quadrupole -name "LN3_QD_V04"       -length  4.000000e-02 -e0  1.515956e+00 -strength -2.016234e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  1.515956e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  1.515956e+00
Quadrupole -name "LN3_QD_V04"       -length  4.000000e-02 -e0  1.515956e+00 -strength -2.016234e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.515956e+00
Drift      -name "LN3_DR_V5"        -length  1.237385e+00 -e0  1.515956e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.515956e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  1.515956e+00 -strength  2.546807e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  1.515956e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  1.515956e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  1.515956e+00 -strength  2.546807e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.515956e+00
Drift      -name "LN3_DR_SV"        -length  1.000000e-01 -e0  1.515956e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.515956e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.515956e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.574526e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.574526e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.574526e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.574526e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.633095e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.633095e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.633095e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.633095e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.691664e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.691664e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.691664e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.691664e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.750233e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.750233e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.750233e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.750233e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  1.750233e+00 -strength -2.940391e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  1.750233e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  1.750233e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  1.750233e+00 -strength -2.940391e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.750233e+00
Drift      -name "LN3_DR_VS"        -length  1.000000e-01 -e0  1.750233e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.750233e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.750233e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.808802e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.808802e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.808802e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.808802e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.867371e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.867371e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.867371e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.867371e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.925940e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.925940e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.925940e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.925940e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   1.984509e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.984509e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.984509e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.984509e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  1.984509e+00 -strength  3.333976e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  1.984509e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  1.984509e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  1.984509e+00 -strength  3.333976e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  1.984509e+00
Drift      -name "LN3_DR_SV"        -length  1.000000e-01 -e0  1.984509e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  1.984509e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  1.984509e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.043078e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.043078e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.043078e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.043078e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.101648e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.101648e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.101648e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.101648e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.160217e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.160217e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.160217e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.160217e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.218786e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.218786e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.218786e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  2.218786e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  2.218786e+00 -strength -3.727560e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  2.218786e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  2.218786e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  2.218786e+00 -strength -3.727560e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  2.218786e+00
Drift      -name "LN3_DR_VS"        -length  1.000000e-01 -e0  2.218786e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.218786e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.218786e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.277355e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.277355e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.277355e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.277355e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.335924e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.335924e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.335924e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.335924e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.394493e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.394493e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.394493e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.394493e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.453062e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.453062e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.453062e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  2.453062e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  2.453062e+00 -strength  4.121145e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  2.453062e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  2.453062e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  2.453062e+00 -strength  4.121145e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  2.453062e+00
Drift      -name "LN3_DR_SV"        -length  1.000000e-01 -e0  2.453062e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.453062e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.453062e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.511631e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.511631e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.511631e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.511631e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.570200e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.570200e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.570200e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.570200e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.628770e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.628770e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.628770e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.628770e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.687339e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.687339e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.687339e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  2.687339e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  2.687339e+00 -strength -4.514729e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  2.687339e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  2.687339e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  2.687339e+00 -strength -4.514729e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  2.687339e+00
Drift      -name "LN3_DR_VS"        -length  1.000000e-01 -e0  2.687339e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.687339e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.687339e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.745908e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.745908e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.745908e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.745908e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.804477e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.804477e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.804477e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.804477e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.863046e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.863046e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.863046e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.863046e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.921615e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.921615e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.921615e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  2.921615e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  2.921615e+00 -strength  4.908314e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  2.921615e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  2.921615e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  2.921615e+00 -strength  4.908314e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  2.921615e+00
Drift      -name "LN3_DR_SV"        -length  1.000000e-01 -e0  2.921615e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.921615e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.921615e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   2.980184e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.980184e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  2.980184e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  2.980184e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   3.038753e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  3.038753e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  3.038753e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  3.038753e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   3.097323e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  3.097323e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  3.097323e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  3.097323e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   3.155892e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  3.155892e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  3.155892e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  3.155892e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  3.155892e+00 -strength -5.301898e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  3.155892e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  3.155892e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  3.155892e+00 -strength -5.301898e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  3.155892e+00
Drift      -name "LN3_DR_VS"        -length  1.000000e-01 -e0  3.155892e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  3.155892e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  3.155892e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   3.214461e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  3.214461e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  3.214461e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  3.214461e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   3.273030e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  3.273030e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  3.273030e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  3.273030e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   3.331599e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  3.331599e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  3.331599e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  3.331599e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   3.390168e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  3.390168e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  3.390168e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  3.390168e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  3.390168e+00 -strength  5.695482e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  3.390168e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  3.390168e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  3.390168e+00 -strength  5.695482e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  3.390168e+00
Drift      -name "LN3_DR_SV"        -length  1.000000e-01 -e0  3.390168e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  3.390168e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  3.390168e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   3.448737e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  3.448737e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  3.448737e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  3.448737e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   3.507306e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  3.507306e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  3.507306e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  3.507306e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   3.565875e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  3.565875e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  3.565875e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  3.565875e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   3.624445e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  3.624445e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  3.624445e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  3.624445e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  3.624445e+00 -strength -6.089067e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  3.624445e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  3.624445e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  3.624445e+00 -strength -6.089067e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  3.624445e+00
Drift      -name "LN3_DR_VS"        -length  1.000000e-01 -e0  3.624445e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  3.624445e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  3.624445e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   3.683014e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  3.683014e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  3.683014e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  3.683014e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   3.741583e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  3.741583e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  3.741583e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  3.741583e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   3.800152e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  3.800152e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  3.800152e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  3.800152e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   3.858721e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  3.858721e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  3.858721e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  3.858721e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  3.858721e+00 -strength  6.482651e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  3.858721e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  3.858721e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  3.858721e+00 -strength  6.482651e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  3.858721e+00
Drift      -name "LN3_DR_SV"        -length  1.000000e-01 -e0  3.858721e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  3.858721e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  3.858721e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   3.917290e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  3.917290e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  3.917290e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  3.917290e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   3.975859e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  3.975859e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  3.975859e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  3.975859e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   4.034428e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  4.034428e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  4.034428e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  4.034428e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   4.092997e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  4.092997e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  4.092997e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  4.092997e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  4.092997e+00 -strength -6.876236e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  4.092997e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  4.092997e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  4.092997e+00 -strength -6.876236e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  4.092997e+00
Drift      -name "LN3_DR_VS"        -length  1.000000e-01 -e0  4.092997e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  4.092997e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  4.092997e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   4.151567e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  4.151567e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  4.151567e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  4.151567e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   4.210136e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  4.210136e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  4.210136e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  4.210136e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   4.268705e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  4.268705e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  4.268705e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  4.268705e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   4.327274e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  4.327274e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  4.327274e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  4.327274e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  4.327274e+00 -strength  7.269820e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  4.327274e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  4.327274e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  4.327274e+00 -strength  7.269820e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  4.327274e+00
Drift      -name "LN3_DR_SV"        -length  1.000000e-01 -e0  4.327274e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  4.327274e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  4.327274e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   4.385843e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  4.385843e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  4.385843e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  4.385843e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   4.444412e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  4.444412e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  4.444412e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  4.444412e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   4.502981e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  4.502981e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  4.502981e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  4.502981e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   4.561550e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  4.561550e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  4.561550e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  4.561550e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  4.561550e+00 -strength -7.663405e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  4.561550e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  4.561550e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  4.561550e+00 -strength -7.663405e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  4.561550e+00
Drift      -name "LN3_DR_VS"        -length  1.000000e-01 -e0  4.561550e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  4.561550e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  4.561550e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   4.620120e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  4.620120e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  4.620120e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  4.620120e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   4.678689e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  4.678689e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  4.678689e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  4.678689e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   4.737258e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  4.737258e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  4.737258e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  4.737258e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   4.795827e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  4.795827e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  4.795827e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  4.795827e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  4.795827e+00 -strength  8.056989e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  4.795827e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  4.795827e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  4.795827e+00 -strength  8.056989e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  4.795827e+00
Drift      -name "LN3_DR_SV"        -length  1.000000e-01 -e0  4.795827e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  4.795827e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  4.795827e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   4.854396e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  4.854396e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  4.854396e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  4.854396e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   4.912965e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  4.912965e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  4.912965e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  4.912965e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   4.971534e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  4.971534e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  4.971534e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  4.971534e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   5.030103e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  5.030103e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  5.030103e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  5.030103e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  5.030103e+00 -strength -8.450574e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  5.030103e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  5.030103e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  5.030103e+00 -strength -8.450574e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  5.030103e+00
Drift      -name "LN3_DR_VS"        -length  1.000000e-01 -e0  5.030103e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  5.030103e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  5.030103e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   5.088672e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  5.088672e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  5.088672e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  5.088672e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   5.147242e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  5.147242e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  5.147242e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  5.147242e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   5.205811e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  5.205811e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  5.205811e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  5.205811e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   5.264380e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  5.264380e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  5.264380e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  5.264380e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  5.264380e+00 -strength  8.844158e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  5.264380e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  5.264380e+00
Quadrupole -name "LN3_QD_FH"        -length  4.000000e-02 -e0  5.264380e+00 -strength  8.844158e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  5.264380e+00
Drift      -name "LN3_DR_SV"        -length  1.000000e-01 -e0  5.264380e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  5.264380e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  5.264380e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   5.322949e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  5.322949e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  5.322949e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  5.322949e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   5.381518e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  5.381518e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  5.381518e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  5.381518e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   5.440087e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  5.440087e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  5.440087e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  5.440087e+00
Cavity     -name "LN3_XCA0"         -length  9.164755e-01 -gradient  6.500000e-02 -phase  1.000000e+01 -frequency  1.199420e+01
SetReferenceEnergy   5.498656e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  5.498656e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  5.498656e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  5.498656e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  5.498656e+00 -strength -9.237743e-01
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  5.498656e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  5.498656e+00
Quadrupole -name "LN3_QD_DH"        -length  4.000000e-02 -e0  5.498656e+00 -strength -9.237743e-01
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  5.498656e+00
Drift      -name "LN3_DR_DR"        -length  1.000000e-01 -e0  5.498656e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  5.498656e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  5.498656e+00
Drift      -name "LN3_DR_XCA_"      -length  9.164755e-01 -e0  5.498656e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  5.498656e+00
Drift      -name "LN3_DR_BL"        -length  5.000000e-02 -e0  5.498656e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  5.498656e+00
Drift      -name "LN3_DR_XCA_"      -length  9.164755e-01 -e0  5.498656e+00
Drift      -name "LN3_DR_XCA_ED"    -length  5.676225e-02 -e0  5.498656e+00
Drift      -name "LN3_DR_CT"        -length  1.000000e-01 -e0  5.498656e+00
Drift      -name "LN3_WA_OU2"       -length  0.000000e+00 -e0  5.498656e+00
Drift      -name "LN3_DR_BP_V1"     -length  1.988701e+00 -e0  5.498656e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  5.498656e+00
Quadrupole -name "LN3_QD_BP_V01"    -length  4.000000e-02 -e0  5.498656e+00 -strength -1.757912e+00
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  5.498656e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  5.498656e+00
Quadrupole -name "LN3_QD_BP_V01"    -length  4.000000e-02 -e0  5.498656e+00 -strength -1.757912e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  5.498656e+00
Drift      -name "LN3_DR_BP_V2"     -length  2.427668e+00 -e0  5.498656e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  5.498656e+00
Quadrupole -name "LN3_QD_BP_V02"    -length  4.000000e-02 -e0  5.498656e+00 -strength  1.898195e+00
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  5.498656e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  5.498656e+00
Quadrupole -name "LN3_QD_BP_V02"    -length  4.000000e-02 -e0  5.498656e+00 -strength  1.898195e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  5.498656e+00
Drift      -name "LN3_DR_BP_V3"     -length  1.213336e+00 -e0  5.498656e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  5.498656e+00
Quadrupole -name "LN3_QD_BP_V03"    -length  4.000000e-02 -e0  5.498656e+00 -strength -1.805644e+00
Dipole     -name "LN3_COR"          -length  0.000000e+00 -e0  5.498656e+00
Bpm        -name "LN3_BPM"          -length  0.000000e+00 -e0  5.498656e+00
Quadrupole -name "LN3_QD_BP_V03"    -length  4.000000e-02 -e0  5.498656e+00 -strength -1.805644e+00
Drift      -name "LN3_DR_QD_ED"     -length  4.250000e-02 -e0  5.498656e+00
Drift      -name "LN3_DR_BP_V4"     -length  2.000000e-01 -e0  5.498656e+00
Drift      -name "LN3_DR_BP_0"      -length  2.000000e-01 -e0  5.498656e+00
Drift      -name "LN3_DR_BP_BL"     -length  5.000000e-02 -e0  5.498656e+00
Drift      -name "LN3_DR_SCA_ED"    -length  8.325105e-02 -e0  5.498656e+00
CrabCavity -name "LN3_SCA0"         -length  5.334979e-01 -voltage  0.000000e+00 -phase  9.000000e+01 -frequency  2.997000e+00
SetReferenceEnergy   5.498656e+00
Drift      -name "LN3_DR_SCA_ED"    -length  8.325105e-02 -e0  5.498651e+00
Drift      -name "LN3_DR_BP_BL"     -length  5.000000e-02 -e0  5.498651e+00
Drift      -name "LN3_DR_BP_0"      -length  2.000000e-01 -e0  5.498651e+00
Drift      -name "LN3_DR_BP"        -length  4.000000e-01 -e0  5.498651e+00
Drift      -name "LN3_DR_BP_VS"     -length  1.000000e-01 -e0  5.498651e+00
Drift      -name "LN3_DR_BP"        -length  4.000000e-01 -e0  5.498651e+00
Drift      -name "LN3_DP_SEPM"      -length  4.500000e-01 -e0  5.498651e+00
Drift      -name "LN3_DR_BP"        -length  4.000000e-01 -e0  5.498651e+00
Drift      -name "LN3_DR_BP"        -length  4.000000e-01 -e0  5.498651e+00
Drift      -name "LN3_DR_BP"        -length  4.000000e-01 -e0  5.498651e+00
Drift      -name "LN3_DR_BP"        -length  4.000000e-01 -e0  5.498651e+00
Drift      -name "LN3_DR_BP"        -length  4.000000e-01 -e0  5.498651e+00
Drift      -name "LN3_DR_BP_VS"     -length  1.000000e-01 -e0  5.498651e+00
Drift      -name "LN3_DR_BP"        -length  4.000000e-01 -e0  5.498651e+00
Drift      -name "LN3_BP_WA_OU2_"   -length  0.000000e+00 -e0  5.498651e+00
Drift      -name "TMC_DR_V1"        -length  2.004117e-01 -e0  5.498651e+00
Drift      -name "TMC_DR_QD_ED"     -length  4.250000e-02 -e0  5.498651e+00
Quadrupole -name "TMC_QD_V01"       -length  4.000000e-02 -e0  5.498651e+00 -strength  1.133521e+00
Dipole     -name "TMC_COR"          -length  0.000000e+00 -e0  5.498651e+00
Bpm        -name "TMC_BPM"          -length  0.000000e+00 -e0  5.498651e+00
Quadrupole -name "TMC_QD_V01"       -length  4.000000e-02 -e0  5.498651e+00 -strength  1.133521e+00
Drift      -name "TMC_DR_QD_ED"     -length  4.250000e-02 -e0  5.498651e+00
Drift      -name "TMC_DR_V2"        -length  2.866492e+00 -e0  5.498651e+00
Drift      -name "TMC_DR_QD_ED"     -length  4.250000e-02 -e0  5.498651e+00
Quadrupole -name "TMC_QD_V02"       -length  4.000000e-02 -e0  5.498651e+00 -strength -2.171754e+00
Dipole     -name "TMC_COR"          -length  0.000000e+00 -e0  5.498651e+00
Bpm        -name "TMC_BPM"          -length  0.000000e+00 -e0  5.498651e+00
Quadrupole -name "TMC_QD_V02"       -length  4.000000e-02 -e0  5.498651e+00 -strength -2.171754e+00
Drift      -name "TMC_DR_QD_ED"     -length  4.250000e-02 -e0  5.498651e+00
Drift      -name "TMC_DR_V3"        -length  3.885389e-01 -e0  5.498651e+00
Drift      -name "TMC_DR_QD_ED"     -length  4.250000e-02 -e0  5.498651e+00
Quadrupole -name "TMC_QD_V03"       -length  4.000000e-02 -e0  5.498651e+00 -strength  1.722142e+00
Dipole     -name "TMC_COR"          -length  0.000000e+00 -e0  5.498651e+00
Bpm        -name "TMC_BPM"          -length  0.000000e+00 -e0  5.498651e+00
Quadrupole -name "TMC_QD_V03"       -length  4.000000e-02 -e0  5.498651e+00 -strength  1.722142e+00
Drift      -name "TMC_DR_QD_ED"     -length  4.250000e-02 -e0  5.498651e+00
Drift      -name "TMC_DR_V4"        -length  2.000000e-01 -e0  5.498651e+00
Drift      -name "TMC_DR_20"        -length  5.000000e-01 -e0  5.498651e+00
Sbend      -name "TMC_DP_DIP1"      -length  5.000001e-01 -e0  5.498651e+00 -angle -8.726600e-04 -E1  0.000000e+00 -E2 -8.726600e-04 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  0.000000e+00  -six_dim 1 -csr 1 
Drift      -name "TMC_DR_SIDE_C"    -length  5.000000e-01 -e0  5.498651e+00
Drift      -name "TMC_DR_SIDE"      -length  3.000001e+00 -e0  5.498651e+00
Sbend      -name "TMC_DP_DIP2"      -length  5.000001e-01 -e0  5.498651e+00 -angle  8.726600e-04 -E1  8.726600e-04 -E2  0.000000e+00 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  0.000000e+00  -six_dim 1 -csr 1 
Drift      -name "TMC_DR_CENT_C"    -length  2.500000e-01 -e0  5.498651e+00
Bpm        -name "TMC_BPM"          -length  0.000000e+00 -e0  5.498651e+00
Drift      -name "TMC_DR_CENT"      -length  5.000000e-01 -e0  5.498651e+00
Sbend      -name "TMC_DP_DIP3"      -length  5.000001e-01 -e0  5.498651e+00 -angle  8.726600e-04 -E1  0.000000e+00 -E2  8.726600e-04 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  0.000000e+00  -six_dim 1 -csr 1 
Drift      -name "TMC_DR_SIDE_C"    -length  5.000000e-01 -e0  5.498651e+00
Drift      -name "TMC_DR_SIDE"      -length  3.000001e+00 -e0  5.498651e+00
Sbend      -name "TMC_DP_DIP4"      -length  5.000001e-01 -e0  5.498651e+00 -angle -8.726600e-04 -E1 -8.726600e-04 -E2  0.000000e+00 -hgap  0.000000e+00 -fint  5.000000e-01 -tilt  0.000000e+00  -six_dim 1 -csr 1 
Drift      -name "TMC_DR_20_C"      -length  5.000000e-01 -e0  5.498651e+00
Drift      -name "TMC_WA_OU2"       -length  0.000000e+00 -e0  5.498651e+00
Drift      -name "HXR_DR_V1"        -length  2.000000e-01 -e0  5.498651e+00
Drift      -name "HXR_DR_QD_ED"     -length  5.200000e-02 -e0  5.498651e+00
Quadrupole -name "HXR_QD_V01"       -length  1.300000e-02 -e0  5.498651e+00 -strength  1.971407e+00
Dipole     -name "HXR_COR"          -length  0.000000e+00 -e0  5.498651e+00
Bpm        -name "HXR_BPM"          -length  0.000000e+00 -e0  5.498651e+00
Quadrupole -name "HXR_QD_V01"       -length  1.300000e-02 -e0  5.498651e+00 -strength  1.971407e+00
Drift      -name "HXR_DR_QD_ED"     -length  5.200000e-02 -e0  5.498651e+00
Drift      -name "HXR_DR_V2"        -length  5.892182e-01 -e0  5.498651e+00
Drift      -name "HXR_DR_QD_ED"     -length  5.200000e-02 -e0  5.498651e+00
Quadrupole -name "HXR_QD_V02"       -length  1.300000e-02 -e0  5.498651e+00 -strength -2.144474e+00
Dipole     -name "HXR_COR"          -length  0.000000e+00 -e0  5.498651e+00
Bpm        -name "HXR_BPM"          -length  0.000000e+00 -e0  5.498651e+00
Quadrupole -name "HXR_QD_V02"       -length  1.300000e-02 -e0  5.498651e+00 -strength -2.144474e+00
Drift      -name "HXR_DR_QD_ED"     -length  5.200000e-02 -e0  5.498651e+00
Drift      -name "HXR_DR_V3"        -length  1.144548e+00 -e0  5.498651e+00
Drift      -name "HXR_DR_QD_ED"     -length  5.200000e-02 -e0  5.498651e+00
Quadrupole -name "HXR_QD_V03"       -length  1.300000e-02 -e0  5.498651e+00 -strength  7.026125e-01
Dipole     -name "HXR_COR"          -length  0.000000e+00 -e0  5.498651e+00
Bpm        -name "HXR_BPM"          -length  0.000000e+00 -e0  5.498651e+00
Quadrupole -name "HXR_QD_V03"       -length  1.300000e-02 -e0  5.498651e+00 -strength  7.026125e-01
Drift      -name "HXR_DR_QD_ED"     -length  5.200000e-02 -e0  5.498651e+00
Drift      -name "HXR_DR_V4"        -length  2.759790e+00 -e0  5.498651e+00
Drift      -name "HXR_DR_CT"        -length  1.000000e-01 -e0  5.498651e+00
Drift      -name "HXR_WA_M_OU2"     -length  0.000000e+00 -e0  5.498651e+00
Drift      -name "HXR_DR_QD_ED"     -length  5.200000e-02 -e0  5.498651e+00
Quadrupole -name "HXR_QD_FH"        -length  1.300000e-02 -e0  5.498651e+00 -strength  1.072237e+00
Dipole     -name "HXR_COR"          -length  0.000000e+00 -e0  5.498651e+00
Bpm        -name "HXR_BPM"          -length  0.000000e+00 -e0  5.498651e+00
Quadrupole -name "HXR_QD_FH"        -length  1.300000e-02 -e0  5.498651e+00 -strength  1.072237e+00
Drift      -name "HXR_DR_QD_ED"     -length  5.200000e-02 -e0  5.498651e+00
Drift      -name "HXR_DR_WIG_ED"    -length  1.950000e-01 -e0  5.498651e+00
Drift      -name "HXR_WIG_0"        -length  1.768000e+00 -e0  5.498651e+00
Drift      -name "HXR_DR_WIG_ED"    -length  1.950000e-01 -e0  5.498651e+00
Drift      -name "HXR_DR_QD_ED"     -length  5.200000e-02 -e0  5.498651e+00
Quadrupole -name "HXR_QD_DH"        -length  1.300000e-02 -e0  5.498651e+00 -strength -1.072237e+00
Dipole     -name "HXR_COR"          -length  0.000000e+00 -e0  5.498651e+00
Bpm        -name "HXR_BPM"          -length  0.000000e+00 -e0  5.498651e+00
Quadrupole -name "HXR_QD_DH"        -length  1.300000e-02 -e0  5.498651e+00 -strength -1.072237e+00
Drift      -name "HXR_DR_QD_ED"     -length  5.200000e-02 -e0  5.498651e+00
Drift      -name "HXR_DR_WIG_ED"    -length  1.950000e-01 -e0  5.498651e+00
Drift      -name "HXR_WIG_0"        -length  1.768000e+00 -e0  5.498651e+00
Drift      -name "HXR_DR_WIG_ED"    -length  1.950000e-01 -e0  5.498651e+00
Drift      -name "HXR_DR_QD_ED"     -length  5.200000e-02 -e0  5.498651e+00
Drift      -name "HXR_WA_OU2"       -length  0.000000e+00 -e0  5.498651e+00
